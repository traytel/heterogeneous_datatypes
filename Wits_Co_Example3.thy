theory Wits_Co_Example3
  imports
    "BNF_Nonuniform_Fixpoint" 
    "~~/src/HOL/Library/BNF_Axiomatization"
begin
declare [[bnf_internals,typedef_overloaded]]

bnf_axiomatization ('a0,'a1,'T0,'T1) GG
  [wits: "'a0 \<Rightarrow> 'T1 \<Rightarrow> ('a0,'a1,'T0,'T1) GG" 
    "'a1 \<Rightarrow> 'T0 \<Rightarrow> ('a0,'a1,'T0,'T1) GG"
    (* breaks perfectness, but is better for illustration: *)
    "'a0 \<Rightarrow> 'a1 \<Rightarrow> 'T0 \<Rightarrow> 'T1 \<Rightarrow> ('a0,'a1,'T0,'T1) GG"
    ]
bnf_axiomatization ('a0,'a1) FF1_1 
  [wits: "'a0 \<Rightarrow> ('a0,'a1) FF1_1" "'a1 \<Rightarrow> ('a0,'a1) FF1_1"]
bnf_axiomatization ('a0,'a1) FF1_2
  [wits: "'a0 \<Rightarrow> 'a1 \<Rightarrow> ('a0,'a1) FF1_2"]
bnf_axiomatization ('a0,'a1) FF2_1 
  [wits: "'a0 \<Rightarrow> ('a0,'a1) FF2_1"]
bnf_axiomatization ('a0,'a1) FF2_2 
  [wits: "'a0 \<Rightarrow> ('a0,'a1) FF2_2"]


(*
[
  [
    ([[{0},{1}],[{0,1}]],0),
    ([[{0}],[{0}]],0)
  ]
]
*)

nonuniform_codatatype
  ('a0, 'a1) T = C "('a0, 'a1, (('a0, 'a1) FF1_1, ('a0, 'a1) FF1_2) T, (('a0, 'a1) FF2_1, ('a0, 'a1) FF2_2) T) GG"

print_theorems


  (* Labels: *)
abbreviation "L1 \<equiv> T.L_2_T"
abbreviation "L2 \<equiv> T.L_1_T"

type_synonym depth = "label_T list"

inductive check_shadow :: "bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG1 :: "bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG2 :: "bool \<Rightarrow> bool \<Rightarrow> bool" where
    "check_GG1 v0 v1 \<Longrightarrow> check_GG2 v0 v1 \<Longrightarrow> check_shadow v0 v1"
|   "v0 \<Longrightarrow> check_shadow v0 v0 \<Longrightarrow> check_GG1 v0 v1"
|   "v1 \<Longrightarrow> check_shadow (v0 \<or> v1) (v0 \<and> v1) \<Longrightarrow> check_GG2 v0 v1"

inductive shadow_start :: "bool \<Rightarrow> bool \<Rightarrow> depth \<Rightarrow> bool" where
    "check_shadow v0 v1 \<Longrightarrow> shadow_start v0 v1 []"
|   "shadow_start (v0 \<or> v1) (v0 \<and> v1) l \<Longrightarrow> shadow_start v0 v1 (Cons L1 l)"
|   "shadow_start v0 v0 l \<Longrightarrow> shadow_start v0 v1 (Cons L2 l)"

(*
fun make_true :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> (bool * bool \<Rightarrow> bool)" where
    "make_true f v0 v1 = (\<lambda> vs. if (v0,v1) = vs then True else f (v0,v1))"

inductive check_shadow :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG1 :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG2 :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> bool" where
    "f (v0,v1) \<Longrightarrow> check_shadow f v0 v1"
|   "check_GG1 (make_true f v0 v1) v0 v1 \<Longrightarrow> check_GG2 (make_true f v0 v1) v0 v1 \<Longrightarrow> check_shadow f v0 v1"
|   "v0 \<Longrightarrow> check_shadow f v0 v0 \<Longrightarrow> check_GG1 f v0 v1"
|   "v1 \<Longrightarrow> check_shadow f (v0 \<or> v1) (v0 \<and> v1) \<Longrightarrow> check_GG2 f v0 v1"

inductive shadow_start :: "bool \<Rightarrow> bool \<Rightarrow> depth \<Rightarrow> bool" where
    "check_shadow (\<lambda> v. False) v0 v1 \<Longrightarrow> shadow_start v0 v1 []"
|   "shadow_start (v0 \<or> v1) (v0 \<and> v1) l \<Longrightarrow> shadow_start v0 v1 (Cons L1 l)"
|   "shadow_start v0 v0 l \<Longrightarrow> shadow_start v0 v1 (Cons L2 l)"
*)


    (* THE WITNESS PROOF: *)


    (* Inductive definition of depths matching the transitions between "S"s:  *)

fun get_wit_aux :: "nat set set list \<Rightarrow> nat set \<Rightarrow> nat \<Rightarrow> nat set \<Rightarrow> nat set" where
  "get_wit_aux [] _ _ nats' = nats'"
| "get_wit_aux (natss # l) nats n nats' = (if Bex natss (\<lambda> s. s \<inter> nats = s) then ({n} \<union> nats') else nats')"

fun get_wit :: "nat set set list \<Rightarrow> nat set \<Rightarrow> nat set" where
  "get_wit natss nats = get_wit_aux natss nats 0 {}"



datatype label_GG =
    GG1
  | GG2
  | GG3
(*
inductive is_S_op :: "(label_GG \<Rightarrow> (nat set \<times> label_T set)) \<Rightarrow>((label_T \<Rightarrow> nat set set list \<times> nat) list) \<Rightarrow> nat set \<times> nat \<Rightarrow> nat set \<times> nat \<Rightarrow> label_GG \<Rightarrow> depth \<Rightarrow> bool" where
  "infoGG GG = (nats',Ts) \<Longrightarrow> nats \<inter> nats' = nats' \<Longrightarrow> is_S_op infoGG _ (nats,f) (nats,f) GG []"
| "is_S_op infoGG infoT (nats',f') startst GG' l \<Longrightarrow> 
  infoGG GG = (nats'',Ts) \<Longrightarrow> T \<in> Ts \<Longrightarrow> nats' \<inter> nats'' = nats'' \<Longrightarrow>
  ((infoT ! f') T) = (natss,f) \<Longrightarrow> get_wit natss nats' = nats \<Longrightarrow>  
  is_S_op infoGG infoT (nats,f) startst GG (T # l)"
*)


fun to_depth :: "nat list \<Rightarrow> depth" where
  "to_depth [] = []"
| "to_depth (0 # l) = L2 # to_depth l"
| "to_depth (_ # l) = L1 # to_depth l"

fun to_list :: "depth \<Rightarrow> nat list" where
  "to_list [] = []"
| "to_list (L2 # l) = 0 # to_list l"
| "to_list (L1 # l) = Suc 0 # to_list l"

fun to_nat :: "label_T \<Rightarrow> nat" where
  "to_nat L2 = 0"
| "to_nat L1 = Suc 0"

fun to_T :: "nat \<Rightarrow> label_T" where
  "to_T 0 = L2"
| "to_T _ = L1"

inductive is_S_new :: "(nat set \<times> nat set) list list \<Rightarrow> nat set set list list \<Rightarrow> nat set \<Rightarrow> nat set \<Rightarrow> depth \<Rightarrow> bool" where
  "is_S_new infoGG _ snats snats []"
| "is_S_new infoGG infoT nats startst l \<Longrightarrow> 
  (infoGG ! 0) ! GG = (nats'',Ts) \<Longrightarrow> to_nat T \<in> Ts \<Longrightarrow> nats'' \<subseteq> nats' \<Longrightarrow>
  infoT ! to_nat T = natsss \<Longrightarrow> get_wit natsss nats' = nats \<Longrightarrow>  
  is_S_new infoGG infoT nats' startst (T # l)"

inductive_cases is_S_new_NilE: "is_S_new infoGG infoT nats startst []"
inductive_cases is_S_new_ConsE: "is_S_new infoGG infoT nats startst (T # l)"

inductive build_sh :: "(nat set \<times> nat set) list list \<Rightarrow> nat set set list list \<Rightarrow> nat set \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> bool" where
  "(infoGG ! 0) ! m = (Vset, Fset) \<Longrightarrow> Vset \<subseteq> nats \<Longrightarrow> i \<in> Fset \<Longrightarrow>
    \<exists> f' GG. build_sh infoGG infoT (get_wit (infoT ! f) nats) GG f' \<Longrightarrow>
    build_sh infoGG infoT nats m f"

definition "is_S' infoGG infoT nats l = (\<exists> nats'. is_S_new infoGG infoT nats' nats l)" 

definition infoT' :: "nat set set list list" where
  "infoT' = [[{{0}}, {{0}, {1}}], [{{0}}, {{1, 0}}]]"

definition infoGG' :: "(nat set \<times> nat set) list list" where
  "infoGG' = [[({0},{0}), ({1},{1}), ({1, 0},{1,0})]]"

ML \<open>[[[1]],[[2]]]
 |> map (map (map HOLogic.mk_numeral))
 |> map (map (HOLogic.mk_list @{typ "num"}))
 |> map (HOLogic.mk_list @{typ "num list"})
 |> HOLogic.mk_list @{typ "num list list"}
 |> Syntax.check_term @{context}\<close>

(*
lemma is_S'_Nil: "is_S' infoGG infoT nats []"
  unfolding is_S'_def
  apply(rule exI[where x="GG1"])
  apply(rule exI[where x="nats"])
  by (simp add: is_S_op.intros(1))

lemma is_S'_Nil': "is_S' infoGG infoT ({1},1) []"
  unfolding is_S'_def
  apply(rule exI[where x="GG1"])
  apply(rule exI[where x="({1},1)"])
  by (simp add: is_S_op.intros(1))
*)

inductive
  is_S1 :: "depth \<Rightarrow> bool" and
  is_S0 :: "depth \<Rightarrow> bool" and
  is_S01 :: "depth \<Rightarrow> bool" 
  where 
    "is_S1 []"
  | "is_S1 u \<Longrightarrow> is_S0 (L1 # u)"
  | "is_S0 u \<Longrightarrow> is_S01 (L2 # u)"
  | "is_S01 u \<Longrightarrow> is_S01 (L1 # u)"
  | "is_S01 u \<Longrightarrow> is_S01 (L2 # u)"

definition "is_S u = (is_S1 u \<or> is_S0 u \<or> is_S01 u)"

lemmas is_S_Nil = is_S_def[of "[]", unfolded is_S1.simps simp_thms]
  
context
  fixes dummy :: "'a0 \<times> 'a1"
    and a0 :: 'a0
    and a1 :: 'a1
begin

definition pred1 :: "depth \<Rightarrow> nat set \<Rightarrow> ('a0, 'a1) shape_1_T \<Rightarrow> bool" where
  "pred1 u nats sh1 = (T.invar_shape_1_T u sh1 \<and> pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> nats) (\<lambda>x. x = a1 \<and> 1 \<in> nats) sh1)"
definition pred2 :: "depth \<Rightarrow> nat set  \<Rightarrow> ('a0, 'a1) shape_2_T \<Rightarrow> bool" where
  "pred2 u nats sh2 = (T.invar_shape_2_T u sh2 \<and> pred_shape_2_T (\<lambda>x. x = a0 \<and> 0 \<in> nats) (\<lambda>x. x = a1 \<and> 1 \<in> nats) sh2)"

abbreviation "spred u infoGG infoT nats g \<equiv>
  pred_pre_T (pred1 u nats) (pred2 u nats) (\<lambda>u'. u' = L2 # u \<and> is_S' infoGG infoT nats u') (\<lambda>u'. u' = L1 # u \<and> is_S' infoGG infoT nats u') g"

definition "wcoalg u infoGG infoT nats = Eps (spred u infoGG infoT nats)"

primcorec wit :: "(nat set \<times> nat set) list list \<Rightarrow> nat set set list list \<Rightarrow> nat set \<Rightarrow> depth \<Rightarrow> ('a0,'a1) raw_T" where
  "wit infoGG infoT nats u = T.raw_T.Ctor_T (map_pre_T id id (wit infoGG infoT nats) (wit infoGG infoT nats) (wcoalg u infoGG infoT nats))"
  
  (*
  The statement pred_Si, entirely determined by Si, makes explicit 
  what Si encodes, namely, the existence of witnesses for the given depth. *)

  (* The only inductive proof needed -- shows that if depths follow 
  the transitions encoded in the deep embedding, they achieve what 
  their corresponding sets of states encode. *)
(*
lemma is_S_back: "is_S' infoGG infoT (nats,n) (L # u) \<longrightarrow> is_S' infoGG infoT (nats,n) u"
  unfolding is_S'_def
  apply(rule impI)
  apply(erule exE)+
  apply(simp only: is_S_op.intros)
proof -
  fix GG nats'
  assume "is_S_op infoGG infoT nats' (nats, n) GG (L # u)"
  show "\<exists>GG nats'. is_S_op infoGG infoT nats' (nats, n) GG u"
  apply(cases "is_S_op infoGG infoT nats' (nats, n) GG (L # u)")
*)


lemma is_pred_S:
   "(is_S_new infoGG' infoT' nats snats u \<longrightarrow> 
      (0 \<in> nats \<longrightarrow> (\<exists>sh1::('a0,'a1) shape_1_T. pred1 u snats sh1)) \<and> 
      (Suc 0 \<in> nats \<longrightarrow> (\<exists>sh2::('a0,'a1) shape_2_T. pred2 u snats sh2)))"
  apply (induct u arbitrary : "nats"; unfold pred1_def[abs_def] pred2_def[abs_def])
proof -
  fix nats
  show "is_S_new infoGG' infoT' nats snats [] \<longrightarrow>
       (0 \<in> nats \<longrightarrow>
        (\<exists>sh1. T.invar_shape_1_T [] sh1 \<and>
               pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)) \<and>
       (Suc 0 \<in> nats \<longrightarrow>
        (\<exists>sh2. T.invar_shape_2_T [] sh2 \<and>
               pred_shape_2_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh2))"
    apply((rule impI conjI)+)
    subgoal
      apply(rule exI[of _ "T.shape_1_T.leaf_T _"])
      apply(auto)
      sorry
    apply(rule impI)
    subgoal
      apply(rule exI[of _ "T.shape_2_T.leaf_T _"])
      apply(auto)
      sorry
    done
(*Case n#u*)
  fix a u nats
  show "(\<And>nats.
           is_S_new infoGG' infoT' nats snats u \<longrightarrow>
           (0 \<in> nats \<longrightarrow>
            (\<exists>sh1. T.invar_shape_1_T u sh1 \<and>
                   pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                    (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)) \<and>
           (Suc 0 \<in> nats \<longrightarrow>
            (\<exists>sh2. T.invar_shape_2_T u sh2 \<and>
                   pred_shape_2_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                    (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh2))) \<Longrightarrow>
       is_S_new infoGG' infoT' nats snats (a # u) \<longrightarrow>
       (0 \<in> nats \<longrightarrow>
        (\<exists>sh1. T.invar_shape_1_T (a # u) sh1 \<and>
               pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)) \<and>
       (Suc 0 \<in> nats \<longrightarrow>
        (\<exists>sh2. T.invar_shape_2_T (a # u) sh2 \<and>
               pred_shape_2_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh2))"
    apply(rule impI conjI)+
    subgoal
    proof -
      assume H1 :"(\<And>nats.
        is_S_new infoGG' infoT' nats snats u \<longrightarrow>
        (0 \<in> nats \<longrightarrow>
         (\<exists>sh1. T.invar_shape_1_T u sh1 \<and>
                pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)) \<and>
        (Suc 0 \<in> nats \<longrightarrow>
         (\<exists>sh2. T.invar_shape_2_T u sh2 \<and>
                pred_shape_2_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh2)))"
      assume H2 : "is_S_new infoGG' infoT' nats snats (a # u)"
      assume H3 : "0 \<in> nats"
      obtain nats' GG nats'' natsss Ts where H4: "
          is_S_new infoGG' infoT' nats' snats u \<and>
          infoGG' ! 0 ! GG = (nats'', Ts) \<and>
          to_nat a \<in> Ts \<and>
          nats'' \<subseteq> nats \<and>
          infoT' ! to_nat a = natsss \<and>
          get_wit natsss nats = nats'"
        sorry
        show "\<exists>sh1. T.invar_shape_1_T (a # u) sh1 \<and>
          pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
           (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1"
     apply(cases a)
          subgoal
            apply hypsubst
          proof -
            assume H5: "a = L2"
            have "0 \<in> nats'"
              using H4 H3 H5
              unfolding infoGG'_def infoT'_def
              apply auto
              done
            obtain sh1 where Help : "(T.invar_shape_1_T u sh1 \<and>
                pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)"
              using H1 H4 \<open>0 \<in> nats'\<close> by auto
            show "\<exists>sh1. T.invar_shape_1_T (L2 # u) sh1 \<and>
          pred_shape_1_T
           (\<lambda>x. x = a0 \<and> 0 \<in> snats)
           (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1"
              apply(rule exI[of _ "T.shape_1_T.node_0_T (arg.wit_pre_T_0_0 sh1)"])
              apply(rule conjI)
              using Help
              apply (metis (no_types, lifting) T.invar_shape_depth_iff(2) arg.arg.pre_T_0_0.pred_set arg.arg.pre_T_0_0.wit(1) arg.arg.pre_T_0_0.wit(2))
              using Help
              apply(auto)
              apply(simp only: arg.T_0_0.arg.T_0_0.pre_T_0_0.pred_set)
              apply(auto)
              using arg.T_0_0.arg.T_0_0.pre_T_0_0.wit
              apply metis
              using arg.T_0_0.arg.T_0_0.pre_T_0_0.wit
              by force
          qed
          subgoal
            apply hypsubst
          proof -
            assume H5: "a = L1"
            have "0 \<in> nats'"
              using H4 H3 H5
              unfolding infoGG'_def infoT'_def
              apply auto
              done
            obtain sh1 where Help : "(T.invar_shape_1_T u sh1 \<and>
                pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)"
              using H1 H4 \<open>0 \<in> nats'\<close> by auto
            show " \<exists>sh1. T.invar_shape_1_T (L1 # u) sh1 \<and>
          pred_shape_1_T
           (\<lambda>x. x = a0 \<and> 0 \<in> snats)
           (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1"
              apply(rule exI[of _ "T.shape_1_T.node_1_T (arg.wit1_pre_T_1_0 sh1)"])
              apply(rule conjI)
              using Help
              apply (smt (verit) T.invar_shape_depth_iff(3) arg.arg.pre_T_1_0.pred_True arg.arg.pre_T_1_0.pred_mono_strong arg.arg.pre_T_1_0.wit1(1) arg.arg.pre_T_1_0.wit1(2))
              using Help
              apply(auto)
              apply(simp only: arg.T_1_0.arg.T_1_0.pre_T_1_0.pred_set)
              apply(auto)
              using arg.T_1_0.arg.T_1_0.pre_T_1_0.wit1
              apply metis
              using arg.T_1_0.arg.T_1_0.pre_T_1_0.wit1
              by force
          qed
            done
        qed
(*
        apply(rule impI)
      proof -
      assume H1 :"(\<And>nats.
        is_S_new infoGG' infoT' nats snats u \<longrightarrow>
        (0 \<in> nats \<longrightarrow>
         (\<exists>sh1. T.invar_shape_1_T u sh1 \<and>
                pred_shape_1_T
                 (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats)
                 sh1)) \<and>
        (Suc 0 \<in> nats \<longrightarrow>
         (\<exists>sh2. T.invar_shape_2_T u sh2 \<and>
                pred_shape_2_T
                 (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats)
                 sh2)))"
      assume H2: "is_S_new infoGG' infoT' nats snats
     (a # u)"
      assume H3 : "Suc 0 \<in> nats"
        show "\<exists>sh2. T.invar_shape_2_T (a # u) sh2 \<and>
          pred_shape_2_T
           (\<lambda>x. x = a0 \<and> 0 \<in> snats)
           (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh2"
          apply(cases a)
          subgoal
            apply(hypsubst)
                obtain nats' GG nats'' natsss Ts where H4: "
          is_S_new infoGG' infoT' nats' snats u \<and>
          infoGG' ! 0 ! GG = (nats'', Ts) \<and>
          to_nat a \<in> Ts \<and>
          nats'' \<subseteq> nats \<and>
          infoT' ! to_nat a = natsss \<and>
          get_wit natsss nats = nats'"
        sorry

          
          proof -
            assume H5: "a = L2"
            have "0 \<in> nats'"
              using H4 H3 H5
              unfolding infoGG'_def infoT'_def
              apply auto
              done
            obtain sh1 where Help : "(T.invar_shape_1_T u sh1 \<and>
                pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)"
              using H1 H4 \<open>0 \<in> nats'\<close> by auto
            show "\<exists>sh1. T.invar_shape_1_T (L2 # u) sh1 \<and>
          pred_shape_1_T
           (\<lambda>x. x = a0 \<and> 0 \<in> snats)
           (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1"
              apply(rule exI[of _ "T.shape_1_T.node_0_T (arg.wit_pre_T_0_0 sh1)"])
              apply(rule conjI)
              using Help
              apply (metis (no_types, lifting) T.invar_shape_depth_iff(2) arg.arg.pre_T_0_0.pred_set arg.arg.pre_T_0_0.wit(1) arg.arg.pre_T_0_0.wit(2))
              using Help
              apply(auto)
              apply(simp only: arg.T_0_0.arg.T_0_0.pre_T_0_0.pred_set)
              apply(auto)
              using arg.T_0_0.arg.T_0_0.pre_T_0_0.wit
              apply metis
              using arg.T_0_0.arg.T_0_0.pre_T_0_0.wit
              by force
          qed
          subgoal
            apply hypsubst
          proof -
            assume H5: "a = L1"
            have "0 \<in> nats'"
              using H4 H3 H5
              unfolding infoGG'_def infoT'_def
              apply auto
              done
            obtain sh1 where Help : "(T.invar_shape_1_T u sh1 \<and>
                pred_shape_1_T (\<lambda>x. x = a0 \<and> 0 \<in> snats)
                 (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1)"
              using H1 H4 \<open>0 \<in> nats'\<close> by auto
            show " \<exists>sh1. T.invar_shape_1_T (L1 # u) sh1 \<and>
          pred_shape_1_T
           (\<lambda>x. x = a0 \<and> 0 \<in> snats)
           (\<lambda>x. x = a1 \<and> 1 \<in> snats) sh1"
              apply(rule exI[of _ "T.shape_1_T.node_1_T (arg.wit1_pre_T_1_0 sh1)"])
              apply(rule conjI)
              using Help
              apply (smt (verit) T.invar_shape_depth_iff(3) arg.arg.pre_T_1_0.pred_True arg.arg.pre_T_1_0.pred_mono_strong arg.arg.pre_T_1_0.wit1(1) arg.arg.pre_T_1_0.wit1(2))
              using Help
              apply(auto)
              apply(simp only: arg.T_1_0.arg.T_1_0.pre_T_1_0.pred_set)
              apply(auto)
              using arg.T_1_0.arg.T_1_0.pre_T_1_0.wit1
              apply metis
              using arg.T_1_0.arg.T_1_0.pre_T_1_0.wit1
              by force
          qed
            done
        qed
*)
        sorry
    qed


  
lemmas is_pred_GG = is_pred_S[rule_format]


  (* Now we reify the coalgebra transitions encoded by the deep embedding: *)
lemma is_S_closed:
  "is_S_new infoGG' infoT' nats snats u \<Longrightarrow> build_sh infoGG' infoT' nats GG f \<Longrightarrow> spred u infoGG' infoT' snats (wcoalg u infoGG' infoT' snats)"
  unfolding wcoalg_def
  apply (rule someI_ex)
  apply(frule is_pred_GG)
  apply(erule conjE)
proof -
  assume H1: "build_sh infoGG' infoT' nats GG f"
  assume H2: "0 \<in> nats \<longrightarrow> Ex (local.pred1 u snats)"
  assume H3: "Suc 0 \<in> nats \<longrightarrow> Ex (local.pred2 u snats)"
  obtain Vset Fset f' where Help:
    "infoGG' ! 0 ! GG = (Vset, Fset) \<and>
      Vset \<subseteq> nats \<and>
      (\<exists> GG f'. build_sh infoGG' infoT' (get_wit (infoT' ! f) nats) GG f')"
    sorry
  have H4:"infoGG' ! 0 ! GG = (Vset, Fset) \<and> Vset \<subseteq> nats"
    by (simp add: Help)
  have H5: "\<exists>GG f'. build_sh infoGG' infoT' (get_wit (infoT' ! f) nats) GG f'"
    using Help by auto
  show "\<exists>x. pred_pre_T (local.pred1 u snats)
         (local.pred2 u snats)
         (\<lambda>u'. u' = L2 # u \<and>
               is_S' infoGG' infoT' snats u')
         (\<lambda>u'. u' = L1 # u \<and>
               is_S' infoGG' infoT' snats u')
         x"
    apply(unfold pre_T.pred_set)
    apply(cases GG)
    subgoal
        using H2
        apply(simp)
        apply(erule conjE impE)+
        subgoal
        using H4
        unfolding infoGG'_def infoT'_def
        apply(simp)
        by auto[1]
      apply(erule exE)
    proof - 
      fix x
      assume Goal: "local.pred1 u snats x"
      show "\<exists>x. (\<forall>x\<in>set1_pre_T x. local.pred1 u snats x) \<and>
             (\<forall>x\<in>set2_pre_T x. local.pred2 u snats x) \<and>
             (\<forall>u'\<in>set3_pre_T x.
                 u' = L2 # u \<and> is_S' infoGG' infoT' snats u') \<and>
             (\<forall>u'\<in>set4_pre_T x.
                 u' = L1 # u \<and> is_S' infoGG' infoT' snats u')"
      apply(rule exI[of _ "wit1_pre_T (L2 # u) x"])
      apply(rule conjI)
        subgoal
          using Goal
          apply(auto)
          apply(frule T.pre_T.wit1)
          apply(hypsubst)
          apply(simp)
          done
        apply(rule conjI)
        subgoal
          apply(auto)
          apply(frule T.pre_T.wit1)
          apply(simp)
          done
        apply(rule conjI)
        subgoal
          apply(auto)
          apply(frule T.pre_T.wit1)
           apply(simp)
          apply(frule T.pre_T.wit1)
          apply(hypsubst)
          unfolding is_S'_def

          sorry
        sorry
    qed
    sorry
qed
  (* Everything else follows from the properties we proved about the 
  states and transitions of the coalgebra. *)
  
lemma pred_raw_T_wit: "is_S_new infoGG' infoT' nats snats u \<Longrightarrow> build_sh infoGG' infoT' nats GG f \<Longrightarrow> pred_raw_T (\<lambda>x. x = a0 \<and> 0 \<in> snats) (\<lambda>x. x = a1 \<and> 1 \<in> snats) (wit infoGG' infoT' snats u)"
  apply (rule raw_T.rel_coinduct[THEN iffD2[OF raw_T.pred_rel],
    of "eq_onp (\<lambda>x. \<exists>u. x = wit infoGG' infoT' snats u \<and> (\<exists>nats'. is_S_new infoGG' infoT' nats' snats u))"])
   apply (unfold eq_onp_def simp_thms)
   apply (rule exI conjI refl)+
   apply assumption+
  apply (erule thin_rl)
  apply (erule exE conjE)+
  apply hypsubst_thin
  apply (drule is_S_closed)
  subgoal sorry
  apply (erule thin_rl)
  unfolding is_S'_def
  apply (unfold pre_T.pred_rel shape_1_T.pred_rel shape_2_T.pred_rel wit.sel pre_T.rel_map
    id_apply eq_onp_def pred1_def pred2_def)
  apply (erule pre_T.rel_mono_strong)
     apply (erule conjE)+
     apply hypsubst_thin
     apply (erule shape_1_T.rel_mono_strong; (erule conjunct1 | assumption))
    apply (erule conjE)+
    apply hypsubst_thin
    apply (erule shape_2_T.rel_mono_strong; (erule conjunct1 | assumption))
   apply (erule exE conjE)+
   apply hypsubst_thin
   apply ((rule exI conjI refl | assumption)+) []
  apply (erule exE conjE)+
  apply hypsubst_thin
  apply ((rule exI conjI refl | assumption)+) []
  done

lemma invar_wit: "is_S_new infoGG' infoT' nats snats u \<Longrightarrow> build_sh infoGG' infoT' nats GG f \<Longrightarrow> T.invar_raw_T u (wit infoGG' infoT' snats u)"
  apply (rule invar_raw_T.coinduct[of "\<lambda>u' w. \<exists>u. u' = u \<and> w = wit infoGG' infoT' snats u \<and> is_S' infoGG' infoT' snats u"])
   apply (rule exI conjI refl)+
  unfolding is_S'_def
  apply(rule exI)
   apply assumption
  apply (erule thin_rl)
  apply (erule exE conjE)+
  apply hypsubst_thin
  apply (rule exI conjI refl)+
   apply (rule wit.ctr)
  apply (drule is_S_closed)
  subgoal
    sorry
  apply (unfold pre_T.pred_map)
  apply (erule pre_T.pred_mono_strong)
     apply (unfold id_apply o_apply pred1_def)
     apply (erule conjunct1)
    apply (unfold id_apply o_apply pred2_def)
    apply (erule conjunct1)
   apply (erule conjE)
   apply hypsubst_thin
   apply (rule disjI1)
  unfolding is_S'_def
  apply(erule exE)
   apply (rule exI conjI refl)+
   apply assumption
  apply (erule conjE)
  apply hypsubst_thin
  apply (rule disjI1)
  apply(erule exE)
  apply (rule exI conjI refl)+
  apply assumption
  done

   
   
   
   
   
(*   
   apply (tactic \<open>
         HEADGOAL (fn n => REPEAT_DETERM (resolve_tac @{context} [exI, conjI, refl] n))
    THEN HEADGOAL (assume_tac @{context})
    THEN HEADGOAL (eresolve_tac @{context} [thin_rl])
    THEN HEADGOAL (fn n => REPEAT_DETERM (eresolve_tac @{context} [exE, conjE] n))
    \<close>)
  apply hypsubst_thin
  apply (tactic \<open>
         HEADGOAL (fn n => REPEAT_DETERM (resolve_tac @{context} [exI, conjI, refl] n))
    \<close>)
   apply (rule wit.ctr)
  apply (drule is_S_closed)
  apply (unfold pre_T.pred_map)
  apply (erule pre_T.pred_mono_strong)
     apply (unfold id_apply o_apply pred1_def)
     apply (erule conjunct1)
    apply (unfold id_apply o_apply pred2_def)
    apply (erule conjunct1)
   apply (erule conjE)
   apply hypsubst_thin
   apply (rule disjI1)
   apply (rule exI conjI refl)+
   apply assumption
  apply (erule conjE)
  apply hypsubst_thin
  apply (rule disjI1)
  apply (rule exI conjI refl)+
  apply assumption
  done
*)


lemmas invar_wit_Nil = invar_wit[OF is_S'_Nil]
lemmas T_wit_Nil =
  pred_raw_T_wit[OF is_S'_Nil, unfolded raw_T.pred_set, THEN conjunct1, THEN bspec]
  pred_raw_T_wit[OF is_S'_Nil, unfolded raw_T.pred_set, THEN conjunct2, THEN bspec]

lemmas invar_wit_Nil' = invar_wit[OF is_S'_Nil']
lemmas T_wit_Nil' =
  pred_raw_T_wit[OF is_S'_Nil', unfolded raw_T.pred_set, THEN conjunct1, THEN bspec]
  pred_raw_T_wit[OF is_S'_Nil', unfolded raw_T.pred_set, THEN conjunct2, THEN bspec]



end
thm T_wit_Nil
thm T_wit_Nil'

end
