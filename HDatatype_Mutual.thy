theory HDatatype_Mutual
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

lemma snoc: "snoc x xs = xs @ [x]"
  by (induct xs) auto

lemma rev_Cons: "rev (x # xs) = snoc x (rev xs)"
  by (auto simp: snoc)

lemma rev_snoc: "rev (snoc x xs) = x # rev xs"
  by (auto simp: snoc)

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto

lemma bi_unique_Grp: "bi_unique (BNF_Def.Grp A f) = inj_on f A"
  unfolding bi_unique_def Grp_def inj_on_def by auto

lemma pointfree_idI: "(\<And>x. (f (g x)) = x) \<Longrightarrow> f o g = id"
  unfolding fun_eq_iff by auto
  
lemma reorient_le_o_Abs:
  "(\<And>x. Abs (Rep x) = x) \<Longrightarrow> (A :: _ \<Rightarrow> _ :: order) \<le> B o Abs \<Longrightarrow> A o Rep \<le> B"
  unfolding le_fun_def o_apply
  by (rule allI, rule order_trans, erule spec) auto



section \<open>Input\<close>

declare [[bnf_internals, typedef_overloaded]]

bnf_axiomatization ('a, 'b) F1A
bnf_axiomatization ('a, 'b) F1B
bnf_axiomatization ('a, 'b) F2A
bnf_axiomatization ('a, 'b) F2B
bnf_axiomatization ('a, 'b) E3A
bnf_axiomatization ('a, 'b) E3B

bnf_axiomatization ('a, 'b, 'x, 'y, 'z) GF [wits: "'b \<Rightarrow> ('a, 'b, 'x, 'y, 'z) GF"]
bnf_axiomatization ('a, 'b, 'x, 'y, 'z) GE [wits: "'b \<Rightarrow> 'y \<Rightarrow> ('a, 'b, 'x, 'y, 'z) GE"]

(*
datatype ('a, 'b) bt =
   L 'b | N "('a + 'a, 'b * 'b) bt" 'a "('a * 'a, 'b) bu"
     and ('a, 'b) bu = X 'a 'b | Y "('a list, 'b list) bt"

('a, 'b, 'x, 'y, 'z) GF = 'b + 'x \<times> 'a \<times> 'z
('a, 'b, 'x, 'y, 'z) GE = 'a \<times> 'b + 'y
('a, 'b) F1A = 'a + 'a
('a, 'b) F2A = 'a list
('a, 'b) F1B = 'b * 'b
('a, 'b) F2B = 'b list
('a, 'b) E3A = 'a * 'a
('a, 'b) E3B = 'b

specs = [(GF, [[F1A, F2A], [F1B, F2B]]), (GE, [[E3A], [E3B]])]
*)

section \<open>Raw Type\<close>

datatype (discs_sels) label = F1 | F2 | E3
type_synonym depth = "label list"
datatype ('a, 'b) shape3A = LeafA 'a
                         | Node1A "(('a, 'b) shape3A, ('a, 'b) shape3B) F1A"
                         | Node2A "(('a, 'b) shape3A, ('a, 'b) shape3B) F2A"
                         | Node3A "(('a, 'b) shape3A, ('a, 'b) shape3B) E3A"
     and ('a, 'b) shape3B = LeafB 'b
                         | Node1B "(('a, 'b) shape3A, ('a, 'b) shape3B) F1B"
                         | Node2B "(('a, 'b) shape3A, ('a, 'b) shape3B) F2B"
                         | Node3B "(('a, 'b) shape3A, ('a, 'b) shape3B) E3B"
datatype ('a, 'b) rawF = ConsF "(('a, 'b) shape3A, ('a, 'b) shape3B, ('a, 'b) rawF, ('a, 'b) rawF, ('a, 'b) rawE) GF"
     and ('a, 'b) rawE = ConsE "(('a, 'b) shape3A, ('a, 'b) shape3B, ('a, 'b) rawF, ('a, 'b) rawF, ('a, 'b) rawE) GE"

abbreviation "un_LeafA u \<equiv> case u of LeafA x \<Rightarrow> x"
abbreviation "un_LeafB u \<equiv> case u of LeafB x \<Rightarrow> x"
abbreviation "un_Node1A u \<equiv> case u of Node1A x \<Rightarrow> x"
abbreviation "un_Node1B u \<equiv> case u of Node1B x \<Rightarrow> x"
abbreviation "un_Node2A u \<equiv> case u of Node2A x \<Rightarrow> x"
abbreviation "un_Node2B u \<equiv> case u of Node2B x \<Rightarrow> x"
abbreviation "un_Node3A u \<equiv> case u of Node3A x \<Rightarrow> x"
abbreviation "un_Node3B u \<equiv> case u of Node3B x \<Rightarrow> x"
abbreviation "un_ConsF t \<equiv> case t of ConsF x \<Rightarrow> x"
abbreviation "un_ConsE t \<equiv> case t of ConsE x \<Rightarrow> x"


section \<open>Invariant\<close>

primrec invar_shape3A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> bool" and invar_shape3B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> bool" where
  "invar_shape3A u0 (LeafA u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape3A u0 (Node1A fa) = (case u0 of label # u0 \<Rightarrow> if label = F1 then pred_F1A (invar_shape3A u0) (invar_shape3B u0) fa else False | _ \<Rightarrow> False)"
| "invar_shape3A u0 (Node2A fa) = (case u0 of label # u0 \<Rightarrow> if label = F2 then pred_F2A (invar_shape3A u0) (invar_shape3B u0) fa else False | _ \<Rightarrow> False)"
| "invar_shape3A u0 (Node3A fa) = (case u0 of label # u0 \<Rightarrow> if label = E3 then pred_E3A (invar_shape3A u0) (invar_shape3B u0) fa else False | _ \<Rightarrow> False)"
| "invar_shape3B u0 (LeafB u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape3B u0 (Node1B fb) = (case u0 of label # u0 \<Rightarrow> if label = F1 then pred_F1B (invar_shape3A u0) (invar_shape3B u0) fb else False | _ \<Rightarrow> False)"
| "invar_shape3B u0 (Node2B fb) = (case u0 of label # u0 \<Rightarrow> if label = F2 then pred_F2B (invar_shape3A u0) (invar_shape3B u0) fb else False | _ \<Rightarrow> False)"
| "invar_shape3B u0 (Node3B fb) = (case u0 of label # u0 \<Rightarrow> if label = E3 then pred_E3B (invar_shape3A u0) (invar_shape3B u0) fb else False | _ \<Rightarrow> False)"

primrec invar_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> bool" and invar_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> bool" where
  "invar_rawF u0 (ConsF g) = pred_GF (invar_shape3A u0) (invar_shape3B u0) (invar_rawF (F1 # u0)) (invar_rawF (F2 # u0)) (invar_rawE (E3 # u0)) g"
| "invar_rawE u0 (ConsE g) = pred_GE (invar_shape3A u0) (invar_shape3B u0) (invar_rawF (F1 # u0)) (invar_rawF (F2 # u0)) (invar_rawE (E3 # u0)) g"


section \<open>The Type\<close>

primrec mk_shape :: "depth \<Rightarrow> 'a \<times> 'b \<Rightarrow> ('a, 'b) shape3A \<times> ('a, 'b) shape3B" where
  "mk_shape [] xy = (LeafA (fst xy), LeafB (snd xy))"
| "mk_shape (l # u0) xy = (case l of
    F1 \<Rightarrow> (Node1A (map_F1A (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F1A)),
           Node1B (map_F1B (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F1B)))
  | F2 \<Rightarrow> (Node2A (map_F2A (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F2A)),
           Node2B (map_F2B (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F2B)))
  | E3 \<Rightarrow> (Node3A (map_E3A (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) E3A)),
           Node3B (map_E3B (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) E3B))))"

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_Shape_mk_Shape_tac ctxt P u invar_simps mk_shape_simps pred_maps pred_Trues pred_congs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME [P, u]) @{thm list.induct})) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val simp = full_simp_tac (fold Simplifier.add_cong pred_congs
        (fold Splitter.add_split label_splits
        (ss_only (flat [invar_simps, mk_shape_simps, pred_maps, pred_Trues, IHs,
        @{thms simp_thms list.case fstI sndI if_True o_apply}]) ctxt)));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, simp])
    end) ctxt);
end
\<close>
lemma invar_shape3A_mk_shape_raw:
  "invar_shape3A u0 (fst (mk_shape u0 xy)) \<and>
   invar_shape3B u0 (snd (mk_shape u0 xy))"
  apply (tactic \<open>mk_invar_Shape_mk_Shape_tac @{context}
    @{cterm "\<lambda> u0. invar_shape3A u0 (fst (mk_shape u0 xy)) \<and> invar_shape3B u0 (snd (mk_shape u0 xy))"}
    @{cterm u0}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms mk_shape.simps}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms F1A.pred_True F2A.pred_True E3A.pred_True F1B.pred_True F2B.pred_True E3B.pred_True}
    @{thms F1A.pred_cong F2A.pred_cong E3A.pred_cong F1B.pred_cong F2B.pred_cong E3B.pred_cong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule list.induct[of "\<lambda>u0. invar_shape3A u0 (fst (mk_shape u0 xy)) \<and>
   invar_shape3B u0 (snd (mk_shape u0 xy))" u0])
  apply (auto simp:
    F1A.pred_map F2A.pred_map E3A.pred_map
    F1B.pred_map F2B.pred_map E3B.pred_map
    F1A.pred_True F2A.pred_True E3A.pred_True
    F1B.pred_True F2B.pred_True E3B.pred_True
    cong: F1A.pred_cong F2A.pred_cong E3A.pred_cong F1B.pred_cong F2B.pred_cong E3B.pred_cong
    split: label.splits)
  done*)

lemmas invar_shape3A_mk_shape =
  conjunct1[OF invar_shape3A_mk_shape_raw]
  conjunct2[OF invar_shape3A_mk_shape_raw]

definition "witF y = ConsF (wit_GF (snd (mk_shape [] (undefined, y))))"
definition "witE y = ConsE (wit_GE (snd (mk_shape [] (undefined, y)))
   (ConsF (wit_GF (snd (mk_shape [F2] (undefined, y))))))"
ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_wit_raw_tac ctxt wit_defs invar_raw_simps invar_Shape_simps mk_shape_simps
    label_cases invar_shape_mk_shape pred_maps pred_sets set_maps G_wits =
  ALLGOALS (REPEAT_DETERM_N 100 o
    FIRST' [hyp_subst_tac ctxt,
    resolve_tac ctxt [allI, impI, conjI],
    etac ctxt imageE,
    dresolve_tac ctxt G_wits,
    full_simp_tac (ss_only (flat [wit_defs, invar_raw_simps, invar_Shape_simps, mk_shape_simps,
      label_cases, invar_shape_mk_shape,
      pred_maps, pred_sets, set_maps, @{thms list.case Ball_def sndI fstI if_True o_apply refl}]) ctxt)]);
end
\<close>

lemma invar_witF: "invar_rawF [] (witF y)"
  apply (tactic \<open>mk_invar_wit_raw_tac @{context}
    @{thms witF_def witE_def}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms mk_shape.simps}
    @{thms label.case}
    @{thms invar_shape3A_mk_shape}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_set GE.pred_set}
    @{thms GF.set_map GE.set_map}
    @{thms GE.wit GF.wit}\<close>)
  done
(*
  by (auto simp only: invar_shape3A_mk_shape
    witF_def witE_def invar_rawF.simps invar_rawE.simps
    invar_shape3A.simps invar_shape3B.simps
    GF.pred_map GE.pred_map GF.pred_set GE.pred_set
    GF.set_map GE.set_map list.case dest!: GE.wit GF.wit)*)

lemma invar_witE: "invar_rawE [] (witE y)"
  apply (tactic \<open>mk_invar_wit_raw_tac @{context}
    @{thms witF_def witE_def}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms mk_shape.simps}
    @{thms label.case}
    @{thms invar_shape3A_mk_shape}
    @{thms GF.pred_map GE.pred_map F2B.pred_map}
    @{thms GF.pred_set GE.pred_set F2B.pred_set}
    @{thms GF.set_map GE.set_map F2B.set_map}
    @{thms GE.wit GF.wit}\<close>)
  done
(*
  by (auto simp only: invar_shape3A_mk_shape
    witF_def witE_def invar_rawF.simps invar_rawE.simps
    invar_shape3A.simps invar_shape3B.simps
    GF.pred_map GE.pred_map GF.pred_set GE.pred_set
    GF.set_map GE.set_map list.case dest!: GE.wit GF.wit)
*)

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_typedef_tac ctxt wit_appl invar_wit =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt [NONE, SOME wit_appl] exI) THEN'
    rtac ctxt CollectI THEN' rtac ctxt invar_wit);

end
\<close>
typedef ('a ,'b) T = "{t :: ('a ,'b) rawF. invar_rawF [] t}"
  apply (tactic \<open>mk_typedef_tac @{context}
    @{cterm "witF undefined :: ('a ,'b) rawF"}
    @{thm invar_witF}\<close>)
  done


typedef ('a ,'b) S = "{t :: ('a ,'b) rawE. invar_rawE [] t}"
  apply (tactic \<open>mk_typedef_tac @{context}
    @{cterm "witE undefined :: ('a ,'b) rawE"}
    @{thm invar_witE}\<close>)
  done


section \<open>Flat and Unflat\<close>

primrec (transfer)
  flat_shape1A :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A \<Rightarrow> ('a, 'b) shape3A" and
  flat_shape1B :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B \<Rightarrow> ('a, 'b) shape3B" where
  "flat_shape1A (LeafA f) = Node1A (map_F1A LeafA LeafB f)"
| "flat_shape1A (Node1A fa) = Node1A (map_F1A flat_shape1A flat_shape1B fa)"
| "flat_shape1A (Node2A fa) = Node2A (map_F2A flat_shape1A flat_shape1B fa)"
| "flat_shape1A (Node3A fa) = Node3A (map_E3A flat_shape1A flat_shape1B fa)"
| "flat_shape1B (LeafB f) = Node1B (map_F1B LeafA LeafB f)"
| "flat_shape1B (Node1B fb) = Node1B (map_F1B flat_shape1A flat_shape1B fb)"
| "flat_shape1B (Node2B fb) = Node2B (map_F2B flat_shape1A flat_shape1B fb)"
| "flat_shape1B (Node3B fb) = Node3B (map_E3B flat_shape1A flat_shape1B fb)"

primrec (transfer)
  flat_shape2A :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A \<Rightarrow> ('a, 'b) shape3A" and
  flat_shape2B :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B \<Rightarrow> ('a, 'b) shape3B" where
  "flat_shape2A (LeafA f) = Node2A (map_F2A LeafA LeafB f)"
| "flat_shape2A (Node1A fa) = Node1A (map_F1A flat_shape2A flat_shape2B fa)"
| "flat_shape2A (Node2A fa) = Node2A (map_F2A flat_shape2A flat_shape2B fa)"
| "flat_shape2A (Node3A fa) = Node3A (map_E3A flat_shape2A flat_shape2B fa)"
| "flat_shape2B (LeafB f) = Node2B (map_F2B LeafA LeafB f)"
| "flat_shape2B (Node1B fb) = Node1B (map_F1B flat_shape2A flat_shape2B fb)"
| "flat_shape2B (Node2B fb) = Node2B (map_F2B flat_shape2A flat_shape2B fb)"
| "flat_shape2B (Node3B fb) = Node3B (map_E3B flat_shape2A flat_shape2B fb)"

primrec (transfer)
  flat_shape3A :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A \<Rightarrow> ('a, 'b) shape3A" and
  flat_shape3B :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B \<Rightarrow> ('a, 'b) shape3B" where
  "flat_shape3A (LeafA f) = Node3A (map_E3A LeafA LeafB f)"
| "flat_shape3A (Node1A fa) = Node1A (map_F1A flat_shape3A flat_shape3B fa)"
| "flat_shape3A (Node2A fa) = Node2A (map_F2A flat_shape3A flat_shape3B fa)"
| "flat_shape3A (Node3A fa) = Node3A (map_E3A flat_shape3A flat_shape3B fa)"
| "flat_shape3B (LeafB f) = Node3B (map_E3B LeafA LeafB f)"
| "flat_shape3B (Node1B fb) = Node1B (map_F1B flat_shape3A flat_shape3B fb)"
| "flat_shape3B (Node2B fb) = Node2B (map_F2B flat_shape3A flat_shape3B fb)"
| "flat_shape3B (Node3B fb) = Node3B (map_E3B flat_shape3A flat_shape3B fb)"

primrec (nonexhaustive)
   unflat_shape1A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) shape3A" and
   unflat_shape1B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) shape3B" where
  "unflat_shape1A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_F1A un_LeafA un_LeafB fa)
      | F1 # u0 \<Rightarrow> Node1A (map_F1A (unflat_shape1A u0) (unflat_shape1B u0) fa))"
| "unflat_shape1A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | F2 # u0 \<Rightarrow> Node2A (map_F2A (unflat_shape1A u0) (unflat_shape1B u0) fa))"
| "unflat_shape1A u0 (Node3A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | E3 # u0 \<Rightarrow> Node3A (map_E3A (unflat_shape1A u0) (unflat_shape1B u0) fa))"
| "unflat_shape1B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_F1B un_LeafA un_LeafB fb)
      | F1 # u0 \<Rightarrow> Node1B (map_F1B (unflat_shape1A u0) (unflat_shape1B u0) fb))"
| "unflat_shape1B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | F2 # u0 \<Rightarrow> Node2B (map_F2B (unflat_shape1A u0) (unflat_shape1B u0) fb))"
| "unflat_shape1B u0 (Node3B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | E3 # u0 \<Rightarrow> Node3B (map_E3B (unflat_shape1A u0) (unflat_shape1B u0) fb))"

primrec (nonexhaustive)
   unflat_shape2A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) shape3A" and
   unflat_shape2B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) shape3B" where
  "unflat_shape2A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1A (map_F1A (unflat_shape2A u0) (unflat_shape2B u0) fa))"
| "unflat_shape2A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_F2A un_LeafA un_LeafB fa)
      | _ # u0 \<Rightarrow> Node2A (map_F2A (unflat_shape2A u0) (unflat_shape2B u0) fa))"
| "unflat_shape2A u0 (Node3A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node3A (map_E3A (unflat_shape2A u0) (unflat_shape2B u0) fa))"
| "unflat_shape2B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1B (map_F1B (unflat_shape2A u0) (unflat_shape2B u0) fb))"
| "unflat_shape2B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_F2B un_LeafA un_LeafB fb)
      | _ # u0 \<Rightarrow> Node2B (map_F2B (unflat_shape2A u0) (unflat_shape2B u0) fb))"
| "unflat_shape2B u0 (Node3B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node3B (map_E3B (unflat_shape2A u0) (unflat_shape2B u0) fb))"

primrec (nonexhaustive)
   unflat_shape3A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) shape3A" and
   unflat_shape3B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) shape3B" where
  "unflat_shape3A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1A (map_F1A (unflat_shape3A u0) (unflat_shape3B u0) fa))"
| "unflat_shape3A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2A (map_F2A (unflat_shape3A u0) (unflat_shape3B u0) fa))"
| "unflat_shape3A u0 (Node3A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_E3A un_LeafA un_LeafB fa)
      | _ # u0 \<Rightarrow> Node3A (map_E3A (unflat_shape3A u0) (unflat_shape3B u0) fa))"
| "unflat_shape3B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1B (map_F1B (unflat_shape3A u0) (unflat_shape3B u0) fb))"
| "unflat_shape3B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2B (map_F2B (unflat_shape3A u0) (unflat_shape3B u0) fb))"
| "unflat_shape3B u0 (Node3B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_E3B un_LeafA un_LeafB fb)
      | _ # u0 \<Rightarrow> Node3B (map_E3B (unflat_shape3A u0) (unflat_shape3B u0) fb))"

primrec (transfer) flat1_rawF :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF \<Rightarrow> ('a, 'b) rawF"
               and flat1_rawE :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE \<Rightarrow> ('a, 'b) rawE" where
  "flat1_rawF (ConsF g) = ConsF (map_GF flat_shape1A flat_shape1B flat1_rawF flat1_rawF flat1_rawE g)"
| "flat1_rawE (ConsE g) = ConsE (map_GE flat_shape1A flat_shape1B flat1_rawF flat1_rawF flat1_rawE g)"

primrec (transfer) flat2_rawF :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF \<Rightarrow> ('a, 'b) rawF"
               and flat2_rawE :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE \<Rightarrow> ('a, 'b) rawE" where
  "flat2_rawF (ConsF g) = ConsF (map_GF flat_shape2A flat_shape2B flat2_rawF flat2_rawF flat2_rawE g)"
| "flat2_rawE (ConsE g) = ConsE (map_GE flat_shape2A flat_shape2B flat2_rawF flat2_rawF flat2_rawE g)"

primrec (transfer) flat3_rawF :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF \<Rightarrow> ('a, 'b) rawF"
               and flat3_rawE :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE \<Rightarrow> ('a, 'b) rawE" where
  "flat3_rawF (ConsF g) = ConsF (map_GF flat_shape3A flat_shape3B flat3_rawF flat3_rawF flat3_rawE g)"
| "flat3_rawE (ConsE g) = ConsE (map_GE flat_shape3A flat_shape3B flat3_rawF flat3_rawF flat3_rawE g)"

primrec unflat1_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) rawF"
    and unflat1_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) rawE" where
  "unflat1_rawF u0 (ConsF g) = ConsF (map_GF (unflat_shape1A u0) (unflat_shape1B u0) (unflat1_rawF (F1 # u0)) (unflat1_rawF (F2 # u0)) (unflat1_rawE (E3 # u0)) g)"
| "unflat1_rawE u0 (ConsE g) = ConsE (map_GE (unflat_shape1A u0) (unflat_shape1B u0) (unflat1_rawF (F1 # u0)) (unflat1_rawF (F2 # u0)) (unflat1_rawE (E3 # u0)) g)"

primrec unflat2_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) rawF"
    and unflat2_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) rawE" where
  "unflat2_rawF u0 (ConsF g) = ConsF (map_GF (unflat_shape2A u0) (unflat_shape2B u0) (unflat2_rawF (F1 # u0)) (unflat2_rawF (F2 # u0)) (unflat2_rawE (E3 # u0)) g)"
| "unflat2_rawE u0 (ConsE g) = ConsE (map_GE (unflat_shape2A u0) (unflat_shape2B u0) (unflat2_rawF (F1 # u0)) (unflat2_rawF (F2 # u0)) (unflat2_rawE (E3 # u0)) g)"

primrec unflat3_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) rawF"
    and unflat3_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) rawE" where
  "unflat3_rawF u0 (ConsF g) = ConsF (map_GF (unflat_shape3A u0) (unflat_shape3B u0) (unflat3_rawF (F1 # u0)) (unflat3_rawF (F2 # u0)) (unflat3_rawE (E3 # u0)) g)"
| "unflat3_rawE u0 (ConsE g) = ConsE (map_GE (unflat_shape3A u0) (unflat_shape3B u0) (unflat3_rawF (F1 # u0)) (unflat3_rawF (F2 # u0)) (unflat3_rawE (E3 # u0)) g)"


section \<open>Constructor and Selector\<close>

definition T :: "('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GF \<Rightarrow>
   ('a, 'b) T" where
  "T g = Abs_T (ConsF (map_GF LeafA LeafB (flat1_rawF o Rep_T) (flat2_rawF o Rep_T) (flat3_rawE o Rep_S) g))"

definition un_T :: "('a, 'b) T \<Rightarrow>
  ('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GF" where
  "un_T t = map_GF un_LeafA un_LeafB (Abs_T o unflat1_rawF []) (Abs_T o unflat2_rawF []) (Abs_S o unflat3_rawE []) (un_ConsF (Rep_T t))"

definition S :: "('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GE \<Rightarrow>
   ('a, 'b) S" where
  "S g = Abs_S (ConsE (map_GE LeafA LeafB (flat1_rawF o Rep_T) (flat2_rawF o Rep_T) (flat3_rawE o Rep_S) g))"

definition un_S :: "('a, 'b) S \<Rightarrow>
  ('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GE" where
  "un_S s = map_GE un_LeafA un_LeafB (Abs_T o unflat1_rawF []) (Abs_T o unflat2_rawF []) (Abs_S o unflat3_rawE []) (un_ConsE (Rep_S s))"


section \<open>BNF Instance\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_map_Shape_closed_tac ctxt induct Ps us
    Shape_map_thms invar_simps pred_maps pred_congs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI :: map (fn thm => refl RS thm) pred_congs);
      val elim = eresolve_tac ctxt [conjE, impE, exE];
      val simp = full_simp_tac
        (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits})
        (ss_only (flat [Shape_map_thms, invar_simps, pred_maps,
            @{thms o_apply simp_thms list.inject list.distinct}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma invar_shape_map_closed_raw:
  "(\<forall>u0. invar_shape3A u0 (map_shape3A f g ua) \<longleftrightarrow> invar_shape3A u0 ua) \<and>
   (\<forall>u0. invar_shape3B u0 (map_shape3B f g ub) \<longleftrightarrow> invar_shape3B u0 ub)"
  apply (tactic \<open>mk_invar_map_Shape_closed_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua. \<forall>u0. invar_shape3A u0 (map_shape3A f g ua) \<longleftrightarrow> invar_shape3A u0 ua"},
     @{cterm "\<lambda>ub. \<forall>u0. invar_shape3B u0 (map_shape3B f g ub) \<longleftrightarrow> invar_shape3B u0 ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms shape3A.map shape3B.map}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_cong F1B.pred_cong F2A.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 (map_shape3A f g ua) \<longleftrightarrow> invar_shape3A u0 ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 (map_shape3B f g ub) \<longleftrightarrow> invar_shape3B u0 ub" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits if_splits)
  done*)

lemmas invar_shape_map_closed =
  spec[OF conjunct1[OF invar_shape_map_closed_raw]]
  spec[OF conjunct2[OF invar_shape_map_closed_raw]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_map_raw_closed_tac ctxt induct Ps us
    map_thms invar_simps pred_maps invar_map_Shape_closed_thms pred_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI :: map (fn thm => refl RS thm) pred_congs);
      val simp = full_simp_tac
        (ss_only (flat [map_thms, invar_simps, pred_maps, invar_map_Shape_closed_thms, @{thms o_apply}])
          ctxt);
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
       FIRST' [hyp_subst_tac ctxt, intro, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma invar_map_closed_raw:
  "(\<forall>u0. invar_rawF u0 (map_rawF f g t) \<longleftrightarrow> invar_rawF u0 t) \<and>
   (\<forall>u0. invar_rawE u0 (map_rawE f g s) \<longleftrightarrow> invar_rawE u0 s)"
  apply (tactic \<open>mk_invar_map_raw_closed_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t. \<forall>u0. invar_rawF u0 (map_rawF f g t) \<longleftrightarrow> invar_rawF u0 t"},
     @{cterm "\<lambda>s. \<forall>u0. invar_rawE u0 (map_rawE f g s) \<longleftrightarrow> invar_rawE u0 s"}]
    [@{cterm t}, @{cterm s}]
    @{thms rawF.map rawE.map}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms GF.pred_map GE.pred_map}
    @{thms invar_shape_map_closed}
    @{thms GF.pred_cong GE.pred_cong}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 (map_rawF f g t) \<longleftrightarrow> invar_rawF u0 t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 (map_rawE f g s) \<longleftrightarrow> invar_rawE u0 s" t s])
  apply (simp only:
      rawF.map invar_rawF.simps rawE.map invar_rawE.simps id_apply o_apply
      GF.pred_map GE.pred_map invar_shape_map_closed |
    intro allI GF.pred_cong GE.pred_cong)+
  done*)

lemmas invar_map_closed =
  spec[OF conjunct1[OF invar_map_closed_raw]]
  spec[OF conjunct2[OF invar_map_closed_raw]]

ML \<open>
local
open Ctr_Sugar_Util
in
fun mk_lift_tac ctxt thm =
  HEADGOAL (full_simp_tac (ss_only (thm :: @{thms mem_Collect_eq}) ctxt));
(*call twice with invar_raw_map_closeds and nwits times with invar_wits*)
end
\<close>

lift_bnf ('a, 'b) T [wits: (*"witF :: 'b \<Rightarrow> ('a, 'b) rawF"*)]
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(1)}\<close>)
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(1)}\<close>)
(*
  apply (tactic \<open>mk_lift_tac @{context} @{thms invar_witF}\<close>)
  apply (auto simp only: witF_def witE_def rawF.set shape3B.simps mk_shape.simps snd_conv dest!: GF.wit) []
  apply (auto simp only: witF_def witE_def rawF.set shape3B.simps mk_shape.simps snd_conv dest!: GF.wit) []
*)
  done
(*  apply (simp only:
      invar_shape_map_closed mem_Collect_eq)+
  done*)

(*
TODO for Andrei
When uncommenting the witness below, one gets unprovable goals
(witE is not really a witness; it would need to have type 'a => 'b => ('a, 'b) rawE).
This has to do with the definition of mk_shape and the usage of undefined there.
Instead one would need to carefully use the different witnesses for the inner BNFs Fs (and Es)
in the definition of mk_shape (explore all combinations!?).
*)
lift_bnf ('a, 'b) S (*[wits: "witE :: 'b \<Rightarrow> ('a, 'b) rawE"]*)
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(2)}\<close>)
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(2)}\<close>)
(*
  apply (tactic \<open>mk_lift_tac @{context} @{thms invar_witE}\<close>)
  apply (auto simp only: witF_def witE_def rawE.set rawF.set shape3A.simps shape3B.simps mk_shape.simps fst_conv snd_conv label.case F2B.set_map dest!: GF.wit GE.wit) []
  apply (auto simp only: witF_def witE_def rawE.set shape3B.simps mk_shape.simps snd_conv dest!: GE.wit) []*)
  done


section \<open>Lemmas about Flat, Unflat, Invar\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_Shape_depth_iff_leaf_tac ctxt exhaust P u invar_simps inject distinct =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME [u, P]) exhaust)) THEN
  ALLGOALS
    (let
      val simp = full_simp_tac
        (ss_only (flat [invar_simps, inject, distinct, @{thms simp_thms list.case}]) ctxt);
    in
      REPEAT_DETERM_N 100 o FIRST' [hyp_subst_tac ctxt, simp]
    end);

fun mk_invar_Shape_depth_iff_node_tac ctxt exhaust P u invar_simps inject distinct
    pred_maps label_case label_distinct =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME [u, P]) exhaust)) THEN
  ALLGOALS
    (let
      val simp = full_simp_tac
        (ss_only (flat [invar_simps, inject, distinct, pred_maps, label_case,
        label_distinct, @{thms simp_thms list.case if_True if_False}]) ctxt);
    in
      REPEAT_DETERM_N 100 o FIRST' [hyp_subst_tac ctxt, simp]
    end);
end
\<close>

lemma invar_shape3A_depth_iff:
  "invar_shape3A [] x = (\<exists>a. x = LeafA a)"
  "invar_shape3A (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3A (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3A (E3 # u0) x = (\<exists>y. x = Node3A y \<and> pred_E3A (invar_shape3A u0) (invar_shape3B u0) y)"
  apply (tactic \<open>mk_invar_Shape_depth_iff_leaf_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A [] x = (\<exists>a. x = LeafA a)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}\<close>)
  apply (tactic \<open>mk_invar_Shape_depth_iff_node_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shape3A u0) (invar_shape3B u0) y)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms label.case}
    @{thms label.distinct}\<close>)
  apply (tactic \<open>mk_invar_Shape_depth_iff_node_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shape3A u0) (invar_shape3B u0) y)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms label.case}
    @{thms label.distinct}\<close>)
  apply (tactic \<open>mk_invar_Shape_depth_iff_node_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A (E3 # u0) x = (\<exists>y. x = Node3A y \<and> pred_E3A (invar_shape3A u0) (invar_shape3B u0) y)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms label.case}
    @{thms label.distinct}\<close>)
(*  apply (rule shape3A.exhaust[of x "invar_shape3A [] x = (\<exists>a. x = LeafA a)"];
    simp only: invar_shape3A.simps shape3A.inject shape3A.distinct simp_thms list.case)
  apply (rule shape3A.exhaust[of x "invar_shape3A (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3A.simps F1A.pred_map shape3A.inject shape3A.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3A.exhaust[of x "invar_shape3A (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3A.simps F2A.pred_map shape3A.inject shape3A.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3A.exhaust[of x "invar_shape3A (E3 # u0) x = (\<exists>y. x = Node3A y \<and> pred_E3A (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3A.simps E3A.pred_map shape3A.inject shape3A.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
 *) done

lemma invar_shape3B_depth_iff:
  "invar_shape3B [] x = (\<exists>b. x = LeafB b)"
  "invar_shape3B (F1 # u0) x = (\<exists>y. x = Node1B y \<and> pred_F1B (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3B (F2 # u0) x = (\<exists>y. x = Node2B y \<and> pred_F2B (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3B (E3 # u0) x = (\<exists>y. x = Node3B y \<and> pred_E3B (invar_shape3A u0) (invar_shape3B u0) y)"
  apply (rule shape3B.exhaust[of x "invar_shape3B [] x = (\<exists>a. x = LeafB a)"];
    simp only: invar_shape3B.simps shape3B.inject shape3B.distinct simp_thms list.case)
  apply (rule shape3B.exhaust[of x "invar_shape3B (F1 # u0) x = (\<exists>y. x = Node1B y \<and> pred_F1B (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3B.simps F1B.pred_map shape3B.inject shape3B.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3B.exhaust[of x "invar_shape3B (F2 # u0) x = (\<exists>y. x = Node2B y \<and> pred_F2B (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3B.simps F2B.pred_map shape3B.inject shape3B.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3B.exhaust[of x "invar_shape3B (E3 # u0) x = (\<exists>y. x = Node3B y \<and> pred_E3B (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3B.simps E3B.pred_map shape3B.inject shape3B.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  done

declare
  atomize_all[symmetric, rulify]
  atomize_imp[symmetric, rulify]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_flat_shape_unflat_shape_tac ctxt induct Ps us
    label_exhaust label_splits label_distincts shape_cases shape_injects invar_shape_depth_iffs
    unflat_simps flat_simps invar_simps
    pred_maps map_comps map_cong_preds map_idents pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val case_distinction_label = SOLVED'
        (rtac ctxt label_exhaust THEN_ALL_NEW
          (TRY o (etac ctxt notE THEN_ALL_NEW assume_tac ctxt)));
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) map_cong_preds map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: pred_mono_strongs);
      val simp = full_simp_tac (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits})
        (ss_only (flat [shape_cases, shape_injects, invar_shape_depth_iffs,
            unflat_simps, flat_simps, invar_simps, pred_maps, map_comps, label_distincts,
            @{thms snoc.simps snoc_neq_Nil id_apply o_apply simp_thms
              list.inject de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, case_distinction_label, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma flatUF1_unflatUF1_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> flat_shape1A (unflat_shape1A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> flat_shape1B (unflat_shape1B u0 ub) = ub)"
  apply (tactic \<open>mk_flat_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> flat_shape1A (unflat_shape1A u0 ua) = ua"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> flat_shape1B (unflat_shape1B u0 ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thm label.exhaust}
    @{thms label.splits}
    @{thms label.distinct}
    @{thms shape3A.case shape3B.case}
    @{thms shape3A.inject shape3B.inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms unflat_shape1A.simps unflat_shape1B.simps}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}\<close>)
  done
(*
  apply (auto simp only:
    unflat_shape1A.simps unflat_shape1B.simps flat_shape1A.simps flat_shape1B.simps
    invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
    shape3A.case shape3B.case invar_shape3A_depth_iff invar_shape3B_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits) [2]
  done
*)

lemma flatUF2_unflatUF2_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> flat_shape2A (unflat_shape2A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> flat_shape2B (unflat_shape2B u0 ub) = ub)"
  apply (tactic \<open>mk_flat_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> flat_shape2A (unflat_shape2A u0 ua) = ua"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> flat_shape2B (unflat_shape2B u0 ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thm label.exhaust}
    @{thms label.splits}
    @{thms label.distinct}
    @{thms shape3A.case shape3B.case}
    @{thms shape3A.inject shape3B.inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms unflat_shape2A.simps unflat_shape2B.simps}
    @{thms flat_shape2A.simps flat_shape2B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> flat_shape2A (unflat_shape2A u0 ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> flat_shape2B (unflat_shape2B u0 ub) = ub" ua ub])
  apply (auto simp only:
    unflat_shape2A.simps unflat_shape2B.simps flat_shape2A.simps flat_shape2B.simps
    invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
    shape3A.case shape3B.case invar_shape3A_depth_iff invar_shape3B_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits)
  done
*)

lemma flatUE_unflatUE_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> flat_shape3A (unflat_shape3A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> flat_shape3B (unflat_shape3B u0 ub) = ub)"
  apply (tactic \<open>mk_flat_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> flat_shape3A (unflat_shape3A u0 ua) = ua"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> flat_shape3B (unflat_shape3B u0 ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thm label.exhaust}
    @{thms label.splits}
    @{thms label.distinct}
    @{thms shape3A.case shape3B.case}
    @{thms shape3A.inject shape3B.inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms unflat_shape3A.simps unflat_shape3B.simps}
    @{thms flat_shape3A.simps flat_shape3B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> flat_shape3A (unflat_shape3A u0 ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> flat_shape3B (unflat_shape3B u0 ub) = ub" ua ub])
  apply (auto simp only:
    unflat_shape3A.simps unflat_shape3B.simps flat_shape3A.simps flat_shape3B.simps
    invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
    shape3A.case shape3B.case invar_shape3A_depth_iff invar_shape3B_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits)
  done
*)

lemmas flat_shape_unflat_shape =
  mp[OF spec[OF conjunct1[OF flatUF1_unflatUF1_raw]]]
  mp[OF spec[OF conjunct2[OF flatUF1_unflatUF1_raw]]]
  mp[OF spec[OF conjunct1[OF flatUF2_unflatUF2_raw]]]
  mp[OF spec[OF conjunct2[OF flatUF2_unflatUF2_raw]]]
  mp[OF spec[OF conjunct1[OF flatUE_unflatUE_raw]]]
  mp[OF spec[OF conjunct2[OF flatUE_unflatUE_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in
fun mk_unflat_shape_flat_shape_tac ctxt induct Ps us
    shape_injects unflat_simps flat_simps invar_simps
    F_pred_maps F_map_comps F_pred_Trues Shape_case F_map_cong_preds F_map_idents
    F_pred_mono_strongs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) F_map_cong_preds F_map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: F_pred_mono_strongs);
      val simp = full_simp_tac
        (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits sum.splits})
        (ss_only (flat [Shape_case, shape_injects,
            unflat_simps, flat_simps, invar_simps, F_pred_maps, F_map_comps, F_pred_Trues,
            @{thms id_apply o_apply refl simp_thms list.simps
              list.inject de_Morgan_conj de_Morgan_disj triv_forall_equality thin_rl}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
        FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma unflatUF1_flatUF1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape1A u0 (flat_shape1A ua) = ua) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape1B u0 (flat_shape1B ub) = ub)"
  apply (tactic \<open>mk_unflat_shape_flat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: (('a, 'b) F1A, ('a, 'b) F1B) shape3A. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape1A u0 (flat_shape1A ua) = ua"},
     @{cterm "\<lambda>ub :: (('a, 'b) F1A, ('a, 'b) F1B) shape3B. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape1B u0 (flat_shape1B ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms shape3A.inject shape3B.inject label.distinct}
    @{thms unflat_shape1A.simps unflat_shape1B.simps}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.pred_True F1B.pred_True F2A.pred_True F2B.pred_True E3A.pred_True E3B.pred_True}
    @{thms shape3A.case shape3B.case}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape1A u0 (flat_shape1A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape1B u0 (flat_shape1B ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shape1A.simps unflat_shape1B.simps flat_shape1A.simps flat_shape1B.simps
      invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      shape3A.case shape3B.case
      id_apply o_apply refl
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits)
  done*)

lemma unflatUF2_flatUF2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape2A u0 (flat_shape2A ua) = ua) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape2B u0 (flat_shape2B ub) = ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape2A u0 (flat_shape2A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape2B u0 (flat_shape2B ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shape2A.simps unflat_shape2B.simps flat_shape2A.simps flat_shape2B.simps invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      shape3A.case shape3B.case
      id_apply o_apply refl
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits)
  done

lemma unflatUE_flatUE_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape3A u0 (flat_shape3A ua) = ua) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape3B u0 (flat_shape3B ub) = ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape3A u0 (flat_shape3A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape3B u0 (flat_shape3B ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shape3A.simps unflat_shape3B.simps flat_shape3A.simps flat_shape3B.simps invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      shape3A.case shape3B.case label.distinct
      id_apply o_apply refl if_True
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits)
  done

lemmas unflatU_flatU =
  mp[OF spec[OF conjunct1[OF unflatUF1_flatUF1_raw]]]
  mp[OF spec[OF conjunct2[OF unflatUF1_flatUF1_raw]]]
  mp[OF spec[OF conjunct1[OF unflatUF2_flatUF2_raw]]]
  mp[OF spec[OF conjunct2[OF unflatUF2_flatUF2_raw]]]
  mp[OF spec[OF conjunct1[OF unflatUE_flatUE_raw]]]
  mp[OF spec[OF conjunct2[OF unflatUE_flatUE_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_shape_flat_shape_tac ctxt induct Ps us flat_simps invar_simps F_pred_maps
    F_pred_Trues F_pred_mono_strongs label_splits F_pred_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        F_pred_mono_strongs);
      val simp = full_simp_tac (fold Simplifier.add_cong F_pred_congs
        (fold Splitter.add_split (label_splits @ @{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, invar_simps, F_pred_maps, F_pred_Trues,
            @{thms snoc.simps snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt)));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma invar_shape_flatUF1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F1 u0) (flat_shape1A ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F1 u0) (flat_shape1B ub))"
  apply (tactic \<open>mk_invar_shape_flat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: (('a, 'b) F1A, ('a, 'b) F1B) shape3A. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F1 u0) (flat_shape1A ua)"},
     @{cterm "\<lambda>ub :: (('a, 'b) F1A, ('a, 'b) F1B) shape3B. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F1 u0) (flat_shape1B ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_True F1B.pred_True F2A.pred_True F2B.pred_True E3A.pred_True E3B.pred_True}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms F1A.pred_cong F1B.pred_cong F2A.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F1 u0) (flat_shape1A ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F1 u0) (flat_shape1B ub)" ua ub])
  apply (auto simp only:
      flat_shape1A.simps flat_shape1B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong)
  done*)

lemma invar_shape_flatUF2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F2 u0) (flat_shape2A ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F2 u0) (flat_shape2B ub))"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F2 u0) (flat_shape2A ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F2 u0) (flat_shape2B ub)" ua ub])
  apply (auto simp only:
      flat_shape2A.simps flat_shape2B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong)
  done

lemma invar_shape_flatUE_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc E3 u0) (flat_shape3A ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc E3 u0) (flat_shape3B ub))"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc E3 u0) (flat_shape3A ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc E3 u0) (flat_shape3B ub)" ua ub])
  apply (auto simp only:
      flat_shape3A.simps flat_shape3B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong)
  done

lemmas invar_shape_flatU =
  mp[OF spec[OF conjunct1[OF invar_shape_flatUF1_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_flatUF1_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_flatUF2_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_flatUF2_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_flatUE_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_flatUE_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_raw_flat_raw_tac ctxt induct Ps us flat_simps invar_simps invar_shape_flat_shapes
    G_pred_maps G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ invar_shape_flat_shapes));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, invar_simps, G_pred_maps,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma invar_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F1 u0) (flat1_rawF t)) \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F1 u0) (flat1_rawE s))"
  apply (tactic \<open>mk_invar_raw_flat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F1 u0) (flat1_rawF t)"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F1 u0) (flat1_rawE s)"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms invar_shape_flatU}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F1 u0) (flat1_rawF t)"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F1 u0) (flat1_rawE s)" t s])
  apply (auto simp only:
      flat1_rawF.simps invar_rawF.simps flat1_rawE.simps invar_rawE.simps snoc.simps[symmetric] invar_shape_flatU
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma invar_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F2 u0) (flat2_rawF t)) \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F2 u0) (flat2_rawE s))"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F2 u0) (flat2_rawF t)"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F2 u0) (flat2_rawE s)" t s])
  apply (auto simp only:
      flat2_rawF.simps invar_rawF.simps flat2_rawE.simps invar_rawE.simps snoc.simps[symmetric] invar_shape_flatU
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemma invar_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc E3 u0) (flat3_rawF t)) \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc E3 u0) (flat3_rawE s))"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc E3 u0) (flat3_rawF t)"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc E3 u0) (flat3_rawE s)" t s])
  apply (auto simp only:
      flat3_rawF.simps invar_rawF.simps flat3_rawE.simps invar_rawE.simps snoc.simps[symmetric] invar_shape_flatU
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemmas invar_flat =
  mp[OF spec[OF conjunct1[OF invar_flat1_raw]]]
  mp[OF spec[OF conjunct2[OF invar_flat1_raw]]]
  mp[OF spec[OF conjunct1[OF invar_flat2_raw]]]
  mp[OF spec[OF conjunct2[OF invar_flat2_raw]]]
  mp[OF spec[OF conjunct1[OF invar_flat3_raw]]]
  mp[OF spec[OF conjunct2[OF invar_flat3_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_shape_unflat_shape_tac ctxt induct Ps us unflat_simps invar_simps
    F_pred_maps F_pred_mono_strongs label_splits label_distincts =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN print_tac ctxt "bla" THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: F_pred_mono_strongs);
      val simp = full_simp_tac (fold Splitter.add_split (label_splits @ @{thms list.splits sum.splits if_splits})
        (ss_only (flat [unflat_simps, invar_simps, F_pred_maps, label_distincts,
            @{thms snoc.simps snoc_neq_Nil id_apply o_apply simp_thms list.simps
              de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, elim, intro, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma invar_shape_unflat_shapeF1_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape1A u0 ua)) \<and>
   (\<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape1B u0 ub))"
  apply (tactic \<open>mk_invar_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape1A u0 ua)"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape1B u0 ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms unflat_shape1A.simps unflat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms label.distinct}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape1A u0 ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape1B u0 ub)" ua ub])
  apply (auto simp only:
      unflat_shape1A.simps unflat_shape1B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      id_apply o_apply refl
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits if_splits)
  done*)

lemma invar_shape_unflat_shapeF2_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape2A u0 ua)) \<and>
   (\<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape2B u0 ub))"
  apply (tactic \<open>mk_invar_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape2A u0 ua)"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape2B u0 ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms unflat_shape2A.simps unflat_shape2B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms label.distinct}\<close>)
  done

lemma invar_shape_unflat_shapeE_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape3A u0 ua)) \<and>
   (\<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape3B u0 ub))"
  apply (tactic \<open>mk_invar_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape3A u0 ua)"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape3B u0 ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms unflat_shape3A.simps unflat_shape3B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms label.distinct}\<close>)
  done

lemmas invar_shape_unflat_shape =
  mp[OF spec[OF conjunct1[OF invar_shape_unflat_shapeF1_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_unflat_shapeF1_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_unflat_shapeF2_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_unflat_shapeF2_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_unflat_shapeE_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_unflat_shapeE_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_raw_unflat_raw_tac ctxt induct Ps us unflat_simps invar_simps invar_shape_unflat_shapes
    G_pred_maps G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ invar_shape_unflat_shapes));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [unflat_simps, invar_simps, G_pred_maps,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma invar_unflat1_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
   "(\<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> invar_rawF u0 (unflat1_rawF u0 t)) \<and>
    (\<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> invar_rawE u0 (unflat1_rawE u0 s))"
  apply (tactic \<open>mk_invar_raw_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: ('a, 'b) rawF. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> invar_rawF u0 (unflat1_rawF u0 t)"},
     @{cterm "\<lambda>s :: ('a, 'b) rawE. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> invar_rawE u0 (unflat1_rawE u0 s)"}]
    [@{cterm t}, @{cterm s}]
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms invar_shape_unflat_shape}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> invar_rawF u0 (unflat1_rawF u0 t)"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> invar_rawE u0 (unflat1_rawE u0 s)" t s])
  apply (auto simp only:
      unflat1_rawF.simps invar_rawF.simps unflat1_rawE.simps invar_rawE.simps snoc.simps invar_shape_unflat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma invar_unflat2_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
   "(\<forall>u0. invar_rawF (snoc F2 u0) t \<longrightarrow> invar_rawF u0 (unflat2_rawF u0 t)) \<and>
    (\<forall>u0. invar_rawE (snoc F2 u0) s \<longrightarrow> invar_rawE u0 (unflat2_rawE u0 s))"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F2 u0) t \<longrightarrow> invar_rawF u0 (unflat2_rawF u0 t)"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F2 u0) s \<longrightarrow> invar_rawE u0 (unflat2_rawE u0 s)" t s])
  apply (auto simp only:
      unflat2_rawF.simps invar_rawF.simps unflat2_rawE.simps invar_rawE.simps snoc.simps invar_shape_unflat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemma invar_unflat3_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
   "(\<forall>u0. invar_rawF (snoc E3 u0) t \<longrightarrow> invar_rawF u0 (unflat3_rawF u0 t)) \<and>
    (\<forall>u0. invar_rawE (snoc E3 u0) s \<longrightarrow> invar_rawE u0 (unflat3_rawE u0 s))"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc E3 u0) t \<longrightarrow> invar_rawF u0 (unflat3_rawF u0 t)"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc E3 u0) s \<longrightarrow> invar_rawE u0 (unflat3_rawE u0 s)" t s])
  apply (auto simp only:
      unflat3_rawF.simps invar_rawF.simps unflat3_rawE.simps invar_rawE.simps snoc.simps invar_shape_unflat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemmas invar_unflat =
  mp[OF spec[OF conjunct1[OF invar_unflat1_raw]]]
  mp[OF spec[OF conjunct2[OF invar_unflat1_raw]]]
  mp[OF spec[OF conjunct1[OF invar_unflat2_raw]]]
  mp[OF spec[OF conjunct2[OF invar_unflat2_raw]]]
  mp[OF spec[OF conjunct1[OF invar_unflat3_raw]]]
  mp[OF spec[OF conjunct2[OF invar_unflat3_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_flat_raw_unflat_raw_tac ctxt induct Ps us flat_simps unflat_simps invar_simps raw_injects flat_shape_unflat_shape
    G_pred_maps G_map_comps G_map_cong_preds G_map_idents G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) G_map_cong_preds G_map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ flat_shape_unflat_shape));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, unflat_simps, invar_simps, G_pred_maps, G_map_comps, raw_injects,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma flat1_unflat1_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
  "(\<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> flat1_rawF (unflat1_rawF u0 t) = t) \<and>
   (\<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> flat1_rawE (unflat1_rawE u0 s) = s)"
  apply (tactic \<open>mk_flat_raw_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: ('a, 'b) rawF. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> flat1_rawF (unflat1_rawF u0 t) = t"},
     @{cterm "\<lambda>s :: ('a, 'b) rawE. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> flat1_rawE (unflat1_rawE u0 s) = s"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms rawF.inject rawE.inject}
    @{thms flat_shape_unflat_shape}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_cong_pred GE.map_cong_pred}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> flat1_rawF (unflat1_rawF u0 t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> flat1_rawE (unflat1_rawE u0 s) = s" t s])
  apply (auto simp only:
      unflat1_rawF.simps unflat1_rawE.simps flat1_rawF.simps flat1_rawE.simps invar_rawF.simps invar_rawE.simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply GF.pred_map GF.map_comp GE.pred_map GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma flat2_unflat2_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
  "(\<forall>u0. invar_rawF (snoc F2 u0) t \<longrightarrow> flat2_rawF (unflat2_rawF u0 t) = t) \<and>
   (\<forall>u0. invar_rawE (snoc F2 u0) s \<longrightarrow> flat2_rawE (unflat2_rawE u0 s) = s)"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F2 u0) t \<longrightarrow> flat2_rawF (unflat2_rawF u0 t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F2 u0) s \<longrightarrow> flat2_rawE (unflat2_rawE u0 s) = s" t s])
  apply (auto simp only:
      unflat2_rawF.simps unflat2_rawE.simps flat2_rawF.simps flat2_rawE.simps invar_rawF.simps invar_rawE.simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply GF.pred_map GF.map_comp GE.pred_map GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemma flat3_unflat3_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
  "(\<forall>u0. invar_rawF (snoc E3 u0) t \<longrightarrow> flat3_rawF (unflat3_rawF u0 t) = t) \<and>
   (\<forall>u0. invar_rawE (snoc E3 u0) s \<longrightarrow> flat3_rawE (unflat3_rawE u0 s) = s)"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc E3 u0) t \<longrightarrow> flat3_rawF (unflat3_rawF u0 t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc E3 u0) s \<longrightarrow> flat3_rawE (unflat3_rawE u0 s) = s" t s])
  apply (auto simp only:
      unflat3_rawF.simps unflat3_rawE.simps flat3_rawF.simps flat3_rawE.simps invar_rawF.simps invar_rawE.simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply GF.pred_map GF.map_comp GE.pred_map GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemmas flat_unflat =
  mp[OF spec[OF conjunct1[OF flat1_unflat1_raw]]]
  mp[OF spec[OF conjunct2[OF flat1_unflat1_raw]]]
  mp[OF spec[OF conjunct1[OF flat2_unflat2_raw]]]
  mp[OF spec[OF conjunct2[OF flat2_unflat2_raw]]]
  mp[OF spec[OF conjunct1[OF flat3_unflat3_raw]]]
  mp[OF spec[OF conjunct2[OF flat3_unflat3_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_unflat_raw_flat_raw_tac ctxt induct Ps us flat_simps unflat_simps invar_simps raw_injects unflat_shape_flat_shape
    G_pred_maps G_map_comps G_map_cong_preds G_map_idents G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) G_map_cong_preds G_map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ unflat_shape_flat_shape));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, unflat_simps, invar_simps, G_pred_maps, G_map_comps, raw_injects,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma unflat1_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> unflat1_rawF u0 (flat1_rawF t) = t) \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> unflat1_rawE u0 (flat1_rawE s) = s)"
  apply (tactic \<open>mk_flat_raw_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat1_rawF u0 (flat1_rawF t) = t"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat1_rawE u0 (flat1_rawE s) = s"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms rawF.inject rawE.inject}
    @{thms unflatU_flatU}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_cong_pred GE.map_cong_pred}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat1_rawF u0 (flat1_rawF t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat1_rawE u0 (flat1_rawE s) = s"])
  apply (auto simp only:
      unflat1_rawF.simps unflat1_rawE.simps flat1_rawF.simps flat1_rawE.simps invar_rawF.simps invar_rawE.simps unflatU_flatU
      id_apply o_apply GF.pred_map GE.pred_map GF.map_comp GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma unflat2_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> unflat2_rawF u0 (flat2_rawF t) = t) \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> unflat2_rawE u0 (flat2_rawE s) = s)"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat2_rawF u0 (flat2_rawF t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat2_rawE u0 (flat2_rawE s) = s"])
  apply (auto simp only:
      unflat2_rawF.simps unflat2_rawE.simps flat2_rawF.simps flat2_rawE.simps invar_rawF.simps invar_rawE.simps unflatU_flatU
      id_apply o_apply GF.pred_map GE.pred_map GF.map_comp GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemma unflat3_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> unflat3_rawF u0 (flat3_rawF t) = t) \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> unflat3_rawE u0 (flat3_rawE s) = s)"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat3_rawF u0 (flat3_rawF t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat3_rawE u0 (flat3_rawE s) = s"])
  apply (auto simp only:
      unflat3_rawF.simps unflat3_rawE.simps flat3_rawF.simps flat3_rawE.simps invar_rawF.simps invar_rawE.simps unflatU_flatU
      id_apply o_apply GF.pred_map GE.pred_map GF.map_comp GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done

lemmas unflat_flat =
  mp[OF spec[OF conjunct1[OF unflat1_flat1_raw]]]
  mp[OF spec[OF conjunct2[OF unflat1_flat1_raw]]]
  mp[OF spec[OF conjunct1[OF unflat2_flat2_raw]]]
  mp[OF spec[OF conjunct2[OF unflat2_flat2_raw]]]
  mp[OF spec[OF conjunct1[OF unflat3_flat3_raw]]]
  mp[OF spec[OF conjunct2[OF unflat3_flat3_raw]]]

section \<open>Constructor is Bijection\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_un_T_T_tac ctxt T_ctr_def T_dtr_def T_Abs_inverse T_Reps T_Rep_inverses
    invar_raw_simps raw_cases invar_Shape_simps Shape_cases
    invar_map_raw_closeds invar_flat_raws unflat_flat_raws
    G_pred_maps G_pred_Trues G_map_comps G_map_idents G_pred_congs G_map_congs =
  unfold_tac ctxt [T_ctr_def, T_dtr_def] THEN
  HEADGOAL (full_simp_tac (fold Simplifier.add_cong (G_pred_congs @ G_map_congs)
        (ss_only (flat [[T_Abs_inverse],
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, T_Rep_inverses,
            invar_raw_simps, raw_cases, invar_Shape_simps, Shape_cases,
            invar_map_raw_closeds, invar_flat_raws, unflat_flat_raws,
            G_pred_maps, G_pred_Trues, G_map_comps, G_map_idents,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt)));
end
\<close>
lemma un_T_T: "un_T (T x) = x"
  apply (tactic \<open>mk_un_T_T_tac @{context}
    @{thm T_def}
    @{thm un_T_def}
    @{thm Abs_T_inverse}
    @{thms Rep_T Rep_S}
    @{thms Rep_T_inverse Rep_S_inverse}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms rawF.case rawE.case}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.case shape3B.case}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms unflat_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.map_cong GE.map_cong}\<close>)
  done
(*
  unfolding T_def un_T_def
  apply (simp only:
      Abs_T_inverse Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawF.simps rawF.case
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_shape_map_closed invar_flat unflat_flat
      GF.pred_map GF.pred_True GF.map_comp GF.map_ident
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong GF.map_cong)
  done*)

lemma un_S_S: "un_S (S x) = x"
  apply (tactic \<open>mk_un_T_T_tac @{context}
    @{thm S_def}
    @{thm un_S_def}
    @{thm Abs_S_inverse}
    @{thms Rep_T Rep_S}
    @{thms Rep_T_inverse Rep_S_inverse}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms rawF.case rawE.case}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.case shape3B.case}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms unflat_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.map_cong GE.map_cong}\<close>)
  done
(*
  unfolding S_def un_S_def
  apply (simp only:
      Abs_S_inverse Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawE.simps rawE.case
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_shape_map_closed invar_flat unflat_flat
      GE.pred_map GE.pred_True GE.map_comp GE.map_ident
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong GE.map_cong)
  done*)

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_T_un_T_tac ctxt T_ctr_def T_dtr_def T_Rep t T_Rep_inject raw_exhaust T_Rep_t
    T_Abs_inverses T_Reps T_Rep_inverses invar_raw_simps raw_cases raw_injects
    invar_Shape_simps Shape_cases invar_map_raw_closeds invar_flat_raws
    invar_unflat_raws flat_unflat_raws pred_maps pred_Trues map_comps
    invar_Shape_depth_iffs map_cong_preds map_idents pred_congs pred_mono_strongs =

  unfold_tac ctxt [T_ctr_def, T_dtr_def] THEN
  HEADGOAL (EVERY' [rtac ctxt rev_mp,
    rtac ctxt (infer_instantiate' ctxt [SOME t] (unfold_thms ctxt @{thms mem_Collect_eq} T_Rep)),
    rtac ctxt impI, rtac ctxt (T_Rep_inject RS iffD1),
    rtac ctxt (infer_instantiate' ctxt [SOME T_Rep_t] raw_exhaust)]) THEN
  ALLGOALS
    (let
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) map_cong_preds map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE ::  pred_mono_strongs);
      val dest = dresolve_tac ctxt (map (fn thm => thm RS iffD1) invar_Shape_depth_iffs);
      val simp = asm_full_simp_tac (fold Simplifier.add_cong pred_congs
        (ss_only (flat [T_Abs_inverses,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, T_Rep_inverses,
            invar_raw_simps, raw_cases, raw_injects,
            invar_Shape_simps, Shape_cases, invar_map_raw_closeds, invar_flat_raws,
            invar_unflat_raws, flat_unflat_raws, pred_maps, pred_Trues, map_comps,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt));
    in
      REPEAT_DETERM o FIRST' [hyp_subst_tac ctxt, intro, elim, dest, simp]
    end);
end
\<close>

lemma T_un_T: "T (un_T x) = x"
  apply (tactic \<open>mk_T_un_T_tac @{context}
    @{thm T_def}
    @{thm un_T_def}
    @{thm Rep_T}
    @{cterm x}
    @{thm Rep_T_inject}
    @{thm rawF.exhaust}
    @{cterm "Rep_T x"}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms Rep_T Rep_S}
    @{thms Rep_T_inverse Rep_S_inverse}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms rawF.case rawE.case}
    @{thms rawF.inject rawE.inject}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.case shape3B.case}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms invar_unflat}
    @{thms flat_unflat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms invar_shape3A_depth_iff(1) invar_shape3B_depth_iff(1)}
    @{thms GF.map_cong_pred GE.map_cong_pred}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done
(*
  unfolding T_def un_T_def
  apply (rule rev_mp)
  apply (rule Rep_T[unfolded mem_Collect_eq, of x])
  apply (rule impI)
  apply (rule iffD1[OF Rep_T_inject])
  apply (rule rawF.exhaust[of "Rep_T x"])
  apply (auto simp only:
      Abs_T_inverse Abs_S_inverse
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawF.simps rawF.case rawF.inject
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_shape_map_closed invar_flat invar_unflat flat_unflat
      GF.pred_map GF.pred_True GF.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    dest!: iffD1[OF invar_shape3A_depth_iff(1)] iffD1[OF invar_shape3B_depth_iff(1)]
    intro!: trans[OF GF.map_cong_pred GF.map_ident]
    elim!: GF.pred_mono_strong
    cong: GF.pred_cong)
  done*)

lemma S_un_S: "S (un_S x) = x"
  unfolding S_def un_S_def
  apply (rule rev_mp)
  apply (rule Rep_S[unfolded mem_Collect_eq, of x])
  apply (rule impI)
  apply (rule iffD1[OF Rep_S_inject])
  apply (rule rawE.exhaust[of "Rep_S x"])
  apply (auto simp only:
      Abs_T_inverse Abs_S_inverse
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawE.simps rawE.case rawE.inject
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_map_closed invar_flat invar_unflat flat_unflat
      GE.pred_map GE.pred_True GE.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    dest!: iffD1[OF invar_shape3A_depth_iff(1)] iffD1[OF invar_shape3B_depth_iff(1)]
    intro!: trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GE.pred_mono_strong
    cong: GE.pred_cong)
  done


section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_map_flat_Shape_tac ctxt induct Ps us Shape_injects Shape_maps flat_simps
    map_comps map_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong map_congs
        (ss_only (flat [Shape_maps, flat_simps, map_comps, Shape_injects, IHs, @{thms o_apply}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>
lemma map_flatUF1_raw:
  "map_shape3A f g (flat_shape1A ua) = flat_shape1A (map_shape3A (map_F1A f g) (map_F1B f g) ua) \<and>
   map_shape3B f g (flat_shape1B ub) = flat_shape1B (map_shape3B (map_F1A f g) (map_F1B f g) ub)"
  apply (tactic \<open>mk_map_flat_Shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua. map_shape3A f g (flat_shape1A ua) = flat_shape1A (map_shape3A (map_F1A f g) (map_F1B f g) ua)"},
     @{cterm "\<lambda>ub. map_shape3B f g (flat_shape1B ub) = flat_shape1B (map_shape3B (map_F1A f g) (map_F1B f g) ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.map shape3B.map}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. map_shape3A f g (flat_shape1A ua) = flat_shape1A (map_shape3A (map_F1A f g) (map_F1B f g) ua)"
    "\<lambda>ub. map_shape3B f g (flat_shape1B ub) = flat_shape1B (map_shape3B (map_F1A f g) (map_F1B f g) ub)" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map flat_shape1A.simps flat_shape1B.simps
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0)
  done*)

lemma map_flatUF2_raw:
  "map_shape3A f g (flat_shape2A ua) = flat_shape2A (map_shape3A (map_F2A f g) (map_F2B f g) ua) \<and>
   map_shape3B f g (flat_shape2B ub) = flat_shape2B (map_shape3B (map_F2A f g) (map_F2B f g) ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. map_shape3A f g (flat_shape2A ua) = flat_shape2A (map_shape3A (map_F2A f g) (map_F2B f g) ua)"
    "\<lambda>ub. map_shape3B f g (flat_shape2B ub) = flat_shape2B (map_shape3B (map_F2A f g) (map_F2B f g) ub)" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map flat_shape2A.simps flat_shape2B.simps
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0)
  done

lemma map_flatUE_raw:
  "map_shape3A f g (flat_shape3A ua) = flat_shape3A (map_shape3A (map_E3A f g) (map_E3B f g) ua) \<and>
   map_shape3B f g (flat_shape3B ub) = flat_shape3B (map_shape3B (map_E3A f g) (map_E3B f g) ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. map_shape3A f g (flat_shape3A ua) = flat_shape3A (map_shape3A (map_E3A f g) (map_E3B f g) ua)"
    "\<lambda>ub. map_shape3B f g (flat_shape3B ub) = flat_shape3B (map_shape3B (map_E3A f g) (map_E3B f g) ub)" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map flat_shape3A.simps flat_shape3B.simps
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0)
  done

lemmas map_flatU =
  conjunct1[OF map_flatUF1_raw]
  conjunct2[OF map_flatUF1_raw]
  conjunct1[OF map_flatUF2_raw]
  conjunct2[OF map_flatUF2_raw]
  conjunct1[OF map_flatUE_raw]
  conjunct2[OF map_flatUE_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_map_flat_raw_tac ctxt induct Ps us raw_maps flat_simps map_comps
    map_flat_shapes map_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong map_congs
        (ss_only (flat [raw_maps, flat_simps, map_comps, map_flat_shapes, IHs, @{thms o_apply}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>

lemma map_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "map_rawF f g (flat1_rawF t) = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t) \<and>
   map_rawE f g (flat1_rawE s) = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"
  apply (tactic \<open>mk_map_flat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t. map_rawF f g (flat1_rawF t) = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"},
     @{cterm "\<lambda>s. map_rawE f g (flat1_rawE s) = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"}]
    [@{cterm t}, @{cterm s}]
    @{thms rawF.map rawE.map}
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms GF.map_comp GE.map_comp}
    @{thms map_flatU}
    @{thms GF.map_cong0 GE.map_cong0}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. map_rawF f g (flat1_rawF t) = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"
    "\<lambda>s. map_rawE f g (flat1_rawE s) = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)" t s])
  apply (simp_all only:
      rawF.map rawE.map flat1_rawF.simps flat1_rawE.simps GF.map_comp GE.map_comp map_flatU o_apply
    cong: GF.map_cong0 GE.map_cong0)
  done*)

lemma map_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "map_rawF f g (flat2_rawF t) = flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t) \<and>
   map_rawE f g (flat2_rawE s) = flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. map_rawF f g (flat2_rawF t) = flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t)"
    "\<lambda>s. map_rawE f g (flat2_rawE s) = flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)" t s])
  apply (simp_all only:
      rawF.map rawE.map flat2_rawF.simps flat2_rawE.simps GF.map_comp GE.map_comp map_flatU o_apply
    cong: GF.map_cong0 GE.map_cong0)
  done

lemma map_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "map_rawF f g (flat3_rawF t) = flat3_rawF (map_rawF (map_E3A f g) (map_E3B f g) t) \<and>
   map_rawE f g (flat3_rawE s) = flat3_rawE (map_rawE (map_E3A f g) (map_E3B f g) s)"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. map_rawF f g (flat3_rawF t) = flat3_rawF (map_rawF (map_E3A f g) (map_E3B f g) t)"
    "\<lambda>s. map_rawE f g (flat3_rawE s) = flat3_rawE (map_rawE (map_E3A f g) (map_E3B f g) s)" t s])
  apply (simp_all only:
      rawF.map rawE.map flat3_rawF.simps flat3_rawE.simps GF.map_comp GE.map_comp map_flatU o_apply
    cong: GF.map_cong0 GE.map_cong0)
  done

lemmas map_flat =
  conjunct1[OF map_flat1_raw]
  conjunct2[OF map_flat1_raw]
  conjunct1[OF map_flat2_raw]
  conjunct2[OF map_flat2_raw]
  conjunct1[OF map_flat3_raw]
  conjunct2[OF map_flat3_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_map_T_eq_tac ctxt T_map_defs T_ctr_def T_Abs_inverses
        raw_maps Shape_maps map_flat_raws T_Reps invar_raw_simps
        invar_Shape_simps invar_map_raw_closeds invar_flat_raws
        pred_maps pred_Trues map_comps pred_congs map_congs =
  unfold_tac ctxt (T_ctr_def :: T_map_defs) THEN
  HEADGOAL (full_simp_tac (fold Simplifier.add_cong (pred_congs @ map_congs)
        (ss_only (flat [T_Abs_inverses, raw_maps, Shape_maps, map_flat_raws,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, invar_raw_simps,
            invar_Shape_simps, invar_map_raw_closeds, invar_flat_raws,
            pred_maps, pred_Trues, map_comps,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt)));
end
\<close>

lemma map_T: "map_T f g (T t) =
  T (map_GF f g (map_T (map_F1A f g) (map_F1B f g)) (map_T (map_F2A f g) (map_F2B f g)) (map_S (map_E3A f g) (map_E3B f g)) t)"
  apply (tactic \<open>mk_map_T_eq_tac @{context}
    @{thms map_T_def map_S_def}
    @{thm T_def}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms rawF.map rawE.map}
    @{thms shape3A.map shape3B.map}
    @{thms map_flat}
    @{thms Rep_T Rep_S}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.map_cong0 GE.map_cong0}\<close>)
  done
(*
  unfolding map_T_def map_S_def T_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawF.map rawE.map shape3A.map shape3B.map map_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF.simps
      invar_shape3A.simps invar_shape3B.simps
      invar_shape_map_closed invar_flat
      GF.pred_map GF.pred_True GF.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong GF.map_cong)
  done*)

lemma map_S: "map_S f g (S t) =
  S (map_GE f g (map_T (map_F1A f g) (map_F1B f g)) (map_T (map_F2A f g) (map_F2B f g)) (map_S (map_E3A f g) (map_E3B f g)) t)"
  unfolding map_T_def map_S_def S_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawF.map rawE.map shape3A.map shape3B.map map_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawE.simps
      invar_shape3A.simps invar_shape3B.simps
      invar_map_closed invar_flat
      GE.pred_map GE.pred_True GE.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong GE.map_cong)
  done

subsection \<open>set\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_set_flat_Shape_tac ctxt induct Ps us Shape_sets flat_simps set_maps =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong @{thms SUP_cong}
        (ss_only (flat [Shape_sets, flat_simps, set_maps, IHs,
        @{thms UN_simps UN_singleton UN_insert UN_empty UN_empty2
          UN_Un UN_Un_distrib Un_ac Un_empty_left}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>

lemma set1_flatUF1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(set1_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set1_F1A \<union> UNION (set2_shape3A ua) set1_F1B) \<and>
   (set1_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set1_F1A \<union> UNION (set2_shape3B ub) set1_F1B)"
  apply (tactic \<open>mk_set_flat_Shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: (('a, 'b) F1A, ('a, 'b) F1B) shape3A. set1_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set1_F1A \<union> UNION (set2_shape3A ua) set1_F1B"},
     @{cterm "\<lambda>ub :: (('a, 'b) F1A, ('a, 'b) F1B) shape3B. set1_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set1_F1A \<union> UNION (set2_shape3B ub) set1_F1B"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms shape3A.set shape3B.set}
    @{thms F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set1_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set1_F1A \<union> UNION (set2_shape3A ua) set1_F1B"
    "\<lambda>ub. set1_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set1_F1A \<union> UNION (set2_shape3B ub) set1_F1B" ua ub])
  apply (simp_all only:
      flat_shape1A.simps flat_shape1B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done*)

lemma set1_flatUF2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(set1_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set1_F2A \<union> UNION (set2_shape3A ua) set1_F2B) \<and>
   (set1_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set1_F2A \<union> UNION (set2_shape3B ub) set1_F2B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set1_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set1_F2A \<union> UNION (set2_shape3A ua) set1_F2B"
    "\<lambda>ub. set1_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set1_F2A \<union> UNION (set2_shape3B ub) set1_F2B" ua ub])
  apply (simp_all only:
      flat_shape2A.simps flat_shape2B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set1_flatUE_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(set1_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set1_E3A \<union> UNION (set2_shape3A ua) set1_E3B) \<and>
   (set1_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set1_E3A \<union> UNION (set2_shape3B ub) set1_E3B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set1_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set1_E3A \<union> UNION (set2_shape3A ua) set1_E3B"
    "\<lambda>ub. set1_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set1_E3A \<union> UNION (set2_shape3B ub) set1_E3B" ua ub])
  apply (simp_all only:
      flat_shape3A.simps flat_shape3B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set2_flatUF1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(set2_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set2_F1A \<union> UNION (set2_shape3A ua) set2_F1B) \<and>
   (set2_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set2_F1A \<union> UNION (set2_shape3B ub) set2_F1B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set2_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set2_F1A \<union> UNION (set2_shape3A ua) set2_F1B"
    "\<lambda>ub. set2_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set2_F1A \<union> UNION (set2_shape3B ub) set2_F1B" ua ub])
  apply (simp_all only:
      flat_shape1A.simps flat_shape1B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set2_flatUF2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(set2_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set2_F2A \<union> UNION (set2_shape3A ua) set2_F2B) \<and>
   (set2_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set2_F2A \<union> UNION (set2_shape3B ub) set2_F2B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set2_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set2_F2A \<union> UNION (set2_shape3A ua) set2_F2B"
    "\<lambda>ub. set2_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set2_F2A \<union> UNION (set2_shape3B ub) set2_F2B" ua ub])
  apply (simp_all only:
      flat_shape2A.simps flat_shape2B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set2_flatUE_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(set2_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set2_E3A \<union> UNION (set2_shape3A ua) set2_E3B) \<and>
   (set2_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set2_E3A \<union> UNION (set2_shape3B ub) set2_E3B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set2_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set2_E3A \<union> UNION (set2_shape3A ua) set2_E3B"
    "\<lambda>ub. set2_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set2_E3A \<union> UNION (set2_shape3B ub) set2_E3B" ua ub])
  apply (simp_all only:
      flat_shape3A.simps flat_shape3B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemmas set_flatU =
  conjunct1[OF set1_flatUF1_raw]
  conjunct1[OF set2_flatUF1_raw]
  conjunct2[OF set1_flatUF1_raw]
  conjunct2[OF set2_flatUF1_raw]
  conjunct1[OF set1_flatUF2_raw]
  conjunct1[OF set2_flatUF2_raw]
  conjunct2[OF set1_flatUF2_raw]
  conjunct2[OF set2_flatUF2_raw]
  conjunct1[OF set1_flatUE_raw]
  conjunct1[OF set2_flatUE_raw]
  conjunct2[OF set1_flatUE_raw]
  conjunct2[OF set2_flatUE_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_set_flat_raw_tac ctxt induct Ps us raw_sets flat_simps set_maps set_flat_Shapes =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong @{thms SUP_cong}
        (ss_only (flat [raw_sets, flat_simps, set_maps, set_flat_Shapes, IHs,
        @{thms UN_simps UN_singleton UN_insert UN_empty UN_empty2
          UN_Un UN_Un_distrib Un_ac Un_empty_left}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>
lemma set1_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "set1_rawF (flat1_rawF t) = UNION (set1_rawF t) set1_F1A \<union> UNION (set2_rawF t) set1_F1B \<and>
   set1_rawE (flat1_rawE s) = UNION (set1_rawE s) set1_F1A \<union> UNION (set2_rawE s) set1_F1B"
  apply (tactic \<open>mk_set_flat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. set1_rawF (flat1_rawF t) = UNION (set1_rawF t) set1_F1A \<union> UNION (set2_rawF t) set1_F1B"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. set1_rawE (flat1_rawE s) = UNION (set1_rawE s) set1_F1A \<union> UNION (set2_rawE s) set1_F1B"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms rawF.set rawE.set}
    @{thms GF.set_map GE.set_map}
    @{thms set_flatU}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set1_rawF (flat1_rawF t) = UNION (set1_rawF t) set1_F1A \<union> UNION (set2_rawF t) set1_F1B"
    "\<lambda>s. set1_rawE (flat1_rawE s) = UNION (set1_rawE s) set1_F1A \<union> UNION (set2_rawE s) set1_F1B" t s])
  apply (simp_all only: flat1_rawF.simps flat1_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flatU
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done*)

lemma set2_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "set2_rawF (flat1_rawF t) = UNION (set1_rawF t) set2_F1A \<union> UNION (set2_rawF t) set2_F1B \<and>
   set2_rawE (flat1_rawE s) = UNION (set1_rawE s) set2_F1A \<union> UNION (set2_rawE s) set2_F1B"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set2_rawF (flat1_rawF t) = UNION (set1_rawF t) set2_F1A \<union> UNION (set2_rawF t) set2_F1B"
    "\<lambda>s. set2_rawE (flat1_rawE s) = UNION (set1_rawE s) set2_F1A \<union> UNION (set2_rawE s) set2_F1B" t s])
  apply (simp_all only: flat1_rawF.simps flat1_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flatU
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set1_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "set1_rawF (flat2_rawF t) = UNION (set1_rawF t) set1_F2A \<union> UNION (set2_rawF t) set1_F2B \<and>
   set1_rawE (flat2_rawE s) = UNION (set1_rawE s) set1_F2A \<union> UNION (set2_rawE s) set1_F2B"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set1_rawF (flat2_rawF t) = UNION (set1_rawF t) set1_F2A \<union> UNION (set2_rawF t) set1_F2B"
    "\<lambda>s. set1_rawE (flat2_rawE s) = UNION (set1_rawE s) set1_F2A \<union> UNION (set2_rawE s) set1_F2B" t s])
  apply (simp_all only: flat2_rawF.simps flat2_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flatU
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set2_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "set2_rawF (flat2_rawF t) = UNION (set1_rawF t) set2_F2A \<union> UNION (set2_rawF t) set2_F2B \<and>
   set2_rawE (flat2_rawE s) = UNION (set1_rawE s) set2_F2A \<union> UNION (set2_rawE s) set2_F2B"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set2_rawF (flat2_rawF t) = UNION (set1_rawF t) set2_F2A \<union> UNION (set2_rawF t) set2_F2B"
    "\<lambda>s. set2_rawE (flat2_rawE s) = UNION (set1_rawE s) set2_F2A \<union> UNION (set2_rawE s) set2_F2B" t s])
  apply (simp_all only: flat2_rawF.simps flat2_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flatU
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set1_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "set1_rawF (flat3_rawF t) = UNION (set1_rawF t) set1_E3A \<union> UNION (set2_rawF t) set1_E3B \<and>
   set1_rawE (flat3_rawE s) = UNION (set1_rawE s) set1_E3A \<union> UNION (set2_rawE s) set1_E3B"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set1_rawF (flat3_rawF t) = UNION (set1_rawF t) set1_E3A \<union> UNION (set2_rawF t) set1_E3B"
    "\<lambda>s. set1_rawE (flat3_rawE s) = UNION (set1_rawE s) set1_E3A \<union> UNION (set2_rawE s) set1_E3B" t s])
  apply (simp_all only: flat3_rawF.simps flat3_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flatU
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set2_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "set2_rawF (flat3_rawF t) = UNION (set1_rawF t) set2_E3A \<union> UNION (set2_rawF t) set2_E3B \<and>
   set2_rawE (flat3_rawE s) = UNION (set1_rawE s) set2_E3A \<union> UNION (set2_rawE s) set2_E3B"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set2_rawF (flat3_rawF t) = UNION (set1_rawF t) set2_E3A \<union> UNION (set2_rawF t) set2_E3B"
    "\<lambda>s. set2_rawE (flat3_rawE s) = UNION (set1_rawE s) set2_E3A \<union> UNION (set2_rawE s) set2_E3B" t s])
  apply (simp_all only: flat3_rawF.simps flat3_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flatU
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemmas set_flat =
  conjunct1[OF set1_flat1_raw]
  conjunct1[OF set2_flat1_raw]
  conjunct2[OF set1_flat1_raw]
  conjunct2[OF set2_flat1_raw]
  conjunct1[OF set1_flat2_raw]
  conjunct1[OF set2_flat2_raw]
  conjunct2[OF set1_flat2_raw]
  conjunct2[OF set2_flat2_raw]
  conjunct1[OF set1_flat3_raw]
  conjunct1[OF set2_flat3_raw]
  conjunct2[OF set1_flat3_raw]
  conjunct2[OF set2_flat3_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_set_T_eq_tac ctxt T_set_defs T_ctr_def T_Abs_inverses raw_sets Shape_sets
      set_flat_raw T_Reps invar_raw_simps invar_Shape_simps invar_flat_raws
      pred_maps pred_Trues set_maps pred_congs =
  unfold_tac ctxt (T_ctr_def :: T_set_defs) THEN
  ALLGOALS (full_simp_tac (fold Simplifier.add_cong pred_congs
        (ss_only (flat [T_Abs_inverses, raw_sets, Shape_sets, set_flat_raw,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, invar_raw_simps,
            invar_Shape_simps, invar_flat_raws,
            pred_maps, pred_Trues, set_maps,
            @{thms UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
              list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]}])
          ctxt)));
end
\<close>
lemma set_T: "set1_T (T g) = set1_GF g \<union>
  (\<Union>(set1_F1A ` (\<Union>(set1_T ` (set3_GF g))))) \<union>
  (\<Union>(set1_F1B ` (\<Union>(set2_T ` (set3_GF g))))) \<union>
  (\<Union>(set1_F2A ` (\<Union>(set1_T ` (set4_GF g))))) \<union>
  (\<Union>(set1_F2B ` (\<Union>(set2_T ` (set4_GF g))))) \<union>
  (\<Union>(set1_E3A` (\<Union>(set1_S ` (set5_GF g))))) \<union>
  (\<Union>(set1_E3B` (\<Union>(set2_S ` (set5_GF g)))))"
  "set2_T (T g) = set2_GF g \<union>
  (\<Union>(set2_F1A ` (\<Union>(set1_T ` (set3_GF g))))) \<union>
  (\<Union>(set2_F1B ` (\<Union>(set2_T ` (set3_GF g))))) \<union>
  (\<Union>(set2_F2A ` (\<Union>(set1_T ` (set4_GF g))))) \<union>
  (\<Union>(set2_F2B ` (\<Union>(set2_T ` (set4_GF g))))) \<union>
  (\<Union>(set2_E3A` (\<Union>(set1_S ` (set5_GF g))))) \<union>
  (\<Union>(set2_E3B` (\<Union>(set2_S ` (set5_GF g)))))"
  apply (tactic \<open>mk_set_T_eq_tac @{context}
    @{thms set1_T_def set2_T_def set1_S_def set2_S_def}
    @{thm T_def}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms rawF.set rawE.set}
    @{thms shape3A.set shape3B.set}
    @{thms set_flat}
    @{thms Rep_T Rep_S}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms invar_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.set_map GE.set_map}
    @{thms GF.pred_cong GE.pred_cong}\<close>)
  done
(*
  unfolding set1_T_def set2_T_def set1_S_def set2_S_def T_def
  apply (simp_all only:
      Abs_T_inverse Abs_S_inverse
      rawF.set rawE.set shape3A.set shape3B.set set_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF.simps
      invar_shape3A.simps invar_shape3B.simps
      invar_flat
      GF.pred_map GF.pred_True GF.set_map
      UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong)
  done*)

lemma set_S: "set1_S (S g) = set1_GE g \<union>
  (\<Union>(set1_F1A ` (\<Union>(set1_T ` (set3_GE g))))) \<union>
  (\<Union>(set1_F1B ` (\<Union>(set2_T ` (set3_GE g))))) \<union>
  (\<Union>(set1_F2A ` (\<Union>(set1_T ` (set4_GE g))))) \<union>
  (\<Union>(set1_F2B ` (\<Union>(set2_T ` (set4_GE g))))) \<union>
  (\<Union>(set1_E3A` (\<Union>(set1_S ` (set5_GE g))))) \<union>
  (\<Union>(set1_E3B` (\<Union>(set2_S ` (set5_GE g)))))"
  "set2_S (S g) = set2_GE g \<union>
  (\<Union>(set2_F1A ` (\<Union>(set1_T ` (set3_GE g))))) \<union>
  (\<Union>(set2_F1B ` (\<Union>(set2_T ` (set3_GE g))))) \<union>
  (\<Union>(set2_F2A ` (\<Union>(set1_T ` (set4_GE g))))) \<union>
  (\<Union>(set2_F2B ` (\<Union>(set2_T ` (set4_GE g))))) \<union>
  (\<Union>(set2_E3A` (\<Union>(set1_S ` (set5_GE g))))) \<union>
  (\<Union>(set2_E3B` (\<Union>(set2_S ` (set5_GE g)))))"
  unfolding set1_T_def set2_T_def set1_S_def set2_S_def S_def
  apply (simp_all only:
      Abs_T_inverse Abs_S_inverse
      rawF.set rawE.set shape3A.set shape3B.set set_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawE.simps
      invar_shape3A.simps invar_shape3B.simps
      invar_flat
      GE.pred_map GE.pred_True GE.set_map
      UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong)
  done


subsection \<open>rel\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_flat_Shape_raw_tac ctxt induct Ps us invar_shape_simps flat_simps rel_injects
        invar_Shape_depth_iffs rel_maps pred_sets rel_mono_strongs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IH0s, ...} =>
    let
      val IHs = map (fn thm => thm RS spec RS spec RS mp RS mp RS mp) IH0s;
      val apply_IHs = resolve_tac ctxt IHs THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val dest = dtac ctxt bspec THEN' assume_tac ctxt;
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE ::  rel_mono_strongs);
      val simp = full_simp_tac (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits})
        (ss_only (flat [invar_shape_simps, flat_simps, rel_injects, invar_Shape_depth_iffs,
        rel_maps, pred_sets, @{thms ball_simps id_apply}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
       FIRST' [hyp_subst_tac ctxt, apply_IHs, intro, elim, dest, simp])
    end) ctxt);
end
\<close>

lemma rel_flatUF1_raw:
  fixes R Q
  shows
  "(\<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') \<longrightarrow>
     rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua') \<and>
   (\<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') \<longrightarrow>
     rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub')"
  apply (tactic \<open>mk_rel_flat_Shape_raw_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua. \<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow>
      rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') \<longrightarrow> rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua'"},
     @{cterm "\<lambda>ub. \<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow>
      rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') \<longrightarrow> rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub'"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms shape3A.rel_inject shape3B.rel_inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms F1A.rel_map F2A.rel_map F1B.rel_map F2B.rel_map E3A.rel_map E3B.rel_map}
    @{thms F1A.pred_set F2A.pred_set F1B.pred_set F2B.pred_set E3A.pred_set E3B.pred_set}
    @{thms F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong E3A.rel_mono_strong E3B.rel_mono_strong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') \<longrightarrow>
    rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua'"
    "\<lambda>ub. \<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') \<longrightarrow>
    rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shape3A.simps invar_shape3B.simps flat_shape1A.simps flat_shape1B.simps shape3A.rel_inject shape3B.rel_inject
      invar_shape3A_depth_iff invar_shape3B_depth_iff ball_simps id_apply
      F1A.rel_map F1A.pred_set
      F2A.rel_map F2A.pred_set
      E3A.rel_map E3A.pred_set
      F1B.rel_map F1B.pred_set
      F2B.rel_map F2B.pred_set
      E3B.rel_map E3B.pred_set
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
      E3A.rel_mono_strong E3B.rel_mono_strong
    split: list.splits label.splits if_splits)
  done*)

lemma rel_flatUF2_raw:
  fixes R Q
  shows
  "(\<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape2A ua) (flat_shape2A ua') \<longrightarrow>
     rel_shape3A (rel_F2A R Q) (rel_F2B R Q) ua ua') \<and>
   (\<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape2B ub) (flat_shape2B ub') \<longrightarrow>
     rel_shape3B (rel_F2A R Q) (rel_F2B R Q) ub ub')"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape2A ua) (flat_shape2A ua') \<longrightarrow>
    rel_shape3A (rel_F2A R Q) (rel_F2B R Q) ua ua'"
    "\<lambda>ub. \<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape2B ub) (flat_shape2B ub') \<longrightarrow>
    rel_shape3B (rel_F2A R Q) (rel_F2B R Q) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shape3A.simps invar_shape3B.simps flat_shape2A.simps flat_shape2B.simps shape3A.rel_inject shape3B.rel_inject
      invar_shape3A_depth_iff invar_shape3B_depth_iff ball_simps id_apply
      F1A.rel_map pred_F1A_def
      F2A.rel_map pred_F2A_def
      E3A.rel_map pred_E3A_def
      F1B.rel_map pred_F1B_def
      F2B.rel_map pred_F2B_def
      E3B.rel_map pred_E3B_def
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
      E3A.rel_mono_strong E3B.rel_mono_strong
    split: list.splits label.splits if_splits)
  done

lemma rel_flatUE_raw:
  fixes R Q
  shows
  "(\<forall>ua' u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape3A ua) (flat_shape3A ua') \<longrightarrow>
     rel_shape3A (rel_E3A R Q) (rel_E3B R Q) ua ua') \<and>
   (\<forall>ub' u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape3B ub) (flat_shape3B ub') \<longrightarrow>
     rel_shape3B (rel_E3A R Q) (rel_E3B R Q) ub ub')"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>ua' u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape3A ua) (flat_shape3A ua') \<longrightarrow>
    rel_shape3A (rel_E3A R Q) (rel_E3B R Q) ua ua'"
    "\<lambda>ub. \<forall>ub' u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape3B ub) (flat_shape3B ub') \<longrightarrow>
    rel_shape3B (rel_E3A R Q) (rel_E3B R Q) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shape3A.simps invar_shape3B.simps flat_shape3A.simps flat_shape3B.simps shape3A.rel_inject shape3B.rel_inject
      invar_shape3A_depth_iff invar_shape3B_depth_iff ball_simps id_apply
      F1A.rel_map pred_F1A_def
      F2A.rel_map pred_F2A_def
      E3A.rel_map pred_E3A_def
      F1B.rel_map pred_F1B_def
      F2B.rel_map pred_F2B_def
      E3B.rel_map pred_E3B_def
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
      E3A.rel_mono_strong E3B.rel_mono_strong
    split: list.splits label.splits if_splits)
  done

ML \<open>
local
open Ctr_Sugar_Util
in
fun mk_rel_final_tac ctxt raw_thm transfer =
  ALLGOALS (EVERY' [
    rtac ctxt (transfer RS @{thm rel_funD} RS @{thm iffI[rotated]}),
    assume_tac ctxt,
    rtac ctxt raw_thm] THEN_ALL_NEW
    assume_tac ctxt);
end
\<close>

lemmas rel_flatUF1_raw1 =
  mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flatUF1_raw]]]]]]
lemmas rel_flatUF1_raw2 =
  mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flatUF1_raw]]]]]]
thm flat_shape1A.transfer
lemma rel_flatUF1A:
  fixes R Q
  shows
  "invar_shape3A u0 ua \<Longrightarrow> invar_shape3A u0 ua' \<Longrightarrow>
    rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') = rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua'"
  apply (tactic \<open>mk_rel_final_tac @{context}
    @{thm rel_flatUF1_raw1}
    @{thm flat_shape1A.transfer}\<close>)
  done
lemma rel_flatUF1B:
  fixes R Q
  shows
  "invar_shape3B u0 ub \<Longrightarrow> invar_shape3B u0 ub' \<Longrightarrow>
    rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') = rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub'"
  apply (tactic \<open>mk_rel_final_tac @{context}
    @{thm rel_flatUF1_raw2}
    @{thm flat_shape1B.transfer}\<close>)
  done
(*  apply (rule iffI[rotated, OF rel_funD[OF flat_shape1A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flatUF1_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape1B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flatUF1_raw]]]]]]; assumption)
  done*)

lemma rel_flatUF2:
  fixes R Q
  shows
  "invar_shape3A u0 ua \<Longrightarrow> invar_shape3A u0 ua' \<Longrightarrow>
    rel_shape3A R Q (flat_shape2A ua) (flat_shape2A ua') = rel_shape3A (rel_F2A R Q) (rel_F2B R Q) ua ua'"
  "invar_shape3B u0 ub \<Longrightarrow> invar_shape3B u0 ub' \<Longrightarrow>
    rel_shape3B R Q (flat_shape2B ub) (flat_shape2B ub') = rel_shape3B (rel_F2A R Q) (rel_F2B R Q) ub ub'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape2A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flatUF2_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape2B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flatUF2_raw]]]]]]; assumption)
  done

lemma rel_flatUE:
  fixes R Q
  shows
  "invar_shape3A u0 ua \<Longrightarrow> invar_shape3A u0 ua' \<Longrightarrow>
    rel_shape3A R Q (flat_shape3A ua) (flat_shape3A ua') = rel_shape3A (rel_E3A R Q) (rel_E3B R Q) ua ua'"
  "invar_shape3B u0 ub \<Longrightarrow> invar_shape3B u0 ub' \<Longrightarrow>
    rel_shape3B R Q (flat_shape3B ub) (flat_shape3B ub') = rel_shape3B (rel_E3A R Q) (rel_E3B R Q) ub ub'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape3A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flatUE_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape3B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flatUE_raw]]]]]]; assumption)
  done

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_flat_raw_raw_tac ctxt induct Ps us raw_exhausts invar_raw_simps flat_simps rel_injects
        rel_maps pred_sets rel_mono_strongs rel_flat_Shapes =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct) THEN_ALL_NEW
   (EVERY' [
    rtac ctxt allI,
    Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thm = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           raw_exhausts;
        in HEADGOAL (resolve_tac ctxt thm) end) ctxt, hyp_subst_tac_thin true ctxt,
    Subgoal.FOCUS (fn {context = ctxt, prems = IH0s, ...} =>
    let
      val IHs = map (fn thm => thm RS spec RS spec RS mp RS mp RS mp) IH0s;
      val apply_IHs = resolve_tac ctxt IHs THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val dest = dtac ctxt bspec THEN' assume_tac ctxt;
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE ::  rel_mono_strongs);
      val simp = asm_full_simp_tac (ss_only (flat [rel_flat_Shapes, invar_raw_simps, flat_simps,
        rel_injects, rel_maps, pred_sets, @{thms ball_simps id_apply}])
          ctxt);
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
       FIRST' [hyp_subst_tac ctxt, apply_IHs, intro, elim, dest, simp])
    end) ctxt]));
end
\<close>

lemma rel_flat1_raw:
  fixes R Q and t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "(\<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') \<longrightarrow> rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t') \<and>
   (\<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') \<longrightarrow> rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s')"
  apply (tactic \<open>mk_rel_flat_raw_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') \<longrightarrow> rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t'"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') \<longrightarrow> rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s'"}]
    [@{cterm t}, @{cterm s}]
    @{thms rawF.exhaust rawE.exhaust}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms rawF.rel_inject rawE.rel_inject}
    @{thms GF.rel_map GE.rel_map}
    @{thms GF.pred_set GE.pred_set}
    @{thms GF.rel_mono_strong GE.rel_mono_strong}
    @{thms rel_flatUF1A rel_flatUF1B}\<close>)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') \<longrightarrow> rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t'"
    "\<lambda>s. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') \<longrightarrow> rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s'" t s])
  apply (tactic \<open>
    let open Ctr_Sugar_Util in
      ALLGOALS (rtac @{context} allI THEN' Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thm = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           @{thms rawF.exhaust rawE.exhaust};
        in HEADGOAL (resolve_tac ctxt thm) end) @{context})
    end\<close>)
  apply (auto simp only: rel_flatUF1A rel_flatUF1B ball_simps id_apply
      invar_rawF.simps flat1_rawF.simps rawF.rel_inject GF.rel_map GF.pred_set
      invar_rawE.simps flat1_rawE.simps rawE.rel_inject GE.rel_map GE.pred_set
    elim!: GF.rel_mono_strong GE.rel_mono_strong)
  done*)

lemma rel_flat2_raw:
  fixes R Q and t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "(\<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat2_rawF t) (flat2_rawF t') \<longrightarrow> rel_rawF (rel_F2A R Q) (rel_F2B R Q) t t') \<and>
   (\<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat2_rawE s) (flat2_rawE s') \<longrightarrow> rel_rawE (rel_F2A R Q) (rel_F2B R Q) s s')"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat2_rawF t) (flat2_rawF t') \<longrightarrow> rel_rawF (rel_F2A R Q) (rel_F2B R Q) t t'"
    "\<lambda>s. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat2_rawE s) (flat2_rawE s') \<longrightarrow> rel_rawE (rel_F2A R Q) (rel_F2B R Q) s s'" t s])
  apply (tactic \<open>
    let open Ctr_Sugar_Util in
      ALLGOALS (rtac @{context} allI THEN' Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thm = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           @{thms rawF.exhaust rawE.exhaust};
        in HEADGOAL (resolve_tac ctxt thm) end) @{context})
    end\<close>)
  apply (auto simp only: rel_flatUF2 ball_simps id_apply
      invar_rawF.simps flat2_rawF.simps rawF.rel_inject GF.rel_map GF.pred_set
      invar_rawE.simps flat2_rawE.simps rawE.rel_inject GE.rel_map GE.pred_set
    elim!: GF.rel_mono_strong GE.rel_mono_strong)
  done

lemma rel_flat3_raw:
  fixes R Q and t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "(\<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat3_rawF t) (flat3_rawF t') \<longrightarrow> rel_rawF (rel_E3A R Q) (rel_E3B R Q) t t') \<and>
   (\<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat3_rawE s) (flat3_rawE s') \<longrightarrow> rel_rawE (rel_E3A R Q) (rel_E3B R Q) s s')"
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat3_rawF t) (flat3_rawF t') \<longrightarrow> rel_rawF (rel_E3A R Q) (rel_E3B R Q) t t'"
    "\<lambda>s. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat3_rawE s) (flat3_rawE s') \<longrightarrow> rel_rawE (rel_E3A R Q) (rel_E3B R Q) s s'" t s])
    apply (tactic \<open>
    let open Ctr_Sugar_Util in
      ALLGOALS (rtac @{context} allI THEN' Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thms = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           @{thms rawF.exhaust rawE.exhaust};
        in HEADGOAL (resolve_tac ctxt thms) end) @{context})
    end\<close>)
  apply (auto simp only: rel_flatUE ball_simps id_apply
      invar_rawF.simps flat3_rawF.simps rawF.rel_inject GF.rel_map GF.pred_set
      invar_rawE.simps flat3_rawE.simps rawE.rel_inject GE.rel_map GE.pred_set
    elim!: GF.rel_mono_strong GE.rel_mono_strong)
  done
ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_flat_raw_tac ctxt transfer rel_flat_raw_raw conj =
  ALLGOALS (EVERY' [
    rtac ctxt (transfer RS @{thm rel_funD} RS @{thm iffI[rotated]}),
    assume_tac ctxt,
    rtac ctxt (rel_flat_raw_raw RS conj RS spec RS spec RS mp RS mp RS mp)] THEN_ALL_NEW
    assume_tac ctxt)
end
\<close>
lemma rel_flat1A:
  fixes R Q
  shows
  "invar_rawF u0 t \<Longrightarrow> invar_rawF u0 t' \<Longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') = rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t'"
  apply (tactic \<open>mk_rel_flat_raw_tac @{context}
    @{thm flat1_rawF.transfer}
    @{thm rel_flat1_raw}
    @{thm conjunct1}\<close>)
  done
(*
  apply (rule iffI[rotated, OF rel_funD[OF flat1_rawF.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat1_raw]]]]]]; assumption)
  done*)
lemma rel_flat1B:
  fixes R Q
  shows
  "invar_rawE u0 s \<Longrightarrow> invar_rawE u0 s' \<Longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') = rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s'"
  apply (tactic \<open>mk_rel_flat_raw_tac @{context}
    @{thm flat1_rawE.transfer}
    @{thm rel_flat1_raw}
    @{thm conjunct2}\<close>)
  done
(*
  apply (rule iffI[rotated, OF rel_funD[OF flat1_rawE.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat1_raw]]]]]]; assumption)
  done*)

lemma rel_flat2:
  fixes R Q
  shows
  "invar_rawF u0 t \<Longrightarrow> invar_rawF u0 t' \<Longrightarrow>
   rel_rawF R Q (flat2_rawF t) (flat2_rawF t') = rel_rawF (rel_F2A R Q) (rel_F2B R Q) t t'"
  "invar_rawE u0 s \<Longrightarrow> invar_rawE u0 s' \<Longrightarrow>
   rel_rawE R Q (flat2_rawE s) (flat2_rawE s') = rel_rawE (rel_F2A R Q) (rel_F2B R Q) s s'"
  apply (rule iffI[rotated, OF rel_funD[OF flat2_rawF.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat2_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat2_rawE.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat2_raw]]]]]]; assumption)
  done

lemma rel_flat3:
  fixes R Q
  shows
  "invar_rawF u0 t \<Longrightarrow> invar_rawF u0 t' \<Longrightarrow>
   rel_rawF R Q (flat3_rawF t) (flat3_rawF t') = rel_rawF (rel_E3A R Q) (rel_E3B R Q) t t'"
  "invar_rawE u0 s \<Longrightarrow> invar_rawE u0 s' \<Longrightarrow>
   rel_rawE R Q (flat3_rawE s) (flat3_rawE s') = rel_rawE (rel_E3A R Q) (rel_E3B R Q) s s'"
  apply (rule iffI[rotated, OF rel_funD[OF flat3_rawF.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat3_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat3_rawE.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat3_raw]]]]]]; assumption)
  done

lemmas rel_flat_rawF =
  rel_flat1A
  rel_flat2(1)
  rel_flat3(1)

lemmas rel_flat_rawE =
  rel_flat1B
  rel_flat2(2)
  rel_flat3(2)

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_T_eq_tac ctxt T_rel_defs T_ctr_def T_Abs_inverses
      raw_rel_injects Shape_rel_injects rel_flat_rawss T_Reps invar_raw_simps
      invar_Shape_simps invar_map_raw_closeds invar_flat_raws
      pred_maps pred_Trues rel_maps pred_congs =
  unfold_tac ctxt (T_ctr_def :: @{thm vimage2p_def} :: T_rel_defs) THEN
  HEADGOAL (full_simp_tac (fold Simplifier.add_cong (pred_congs)
        (ss_only (flat [T_Abs_inverses, raw_rel_injects, Shape_rel_injects,
            map2 (fn thms => fn rep =>
              let
                val reps = replicate 2 (unfold_thms ctxt @{thms mem_Collect_eq} rep);
              in
                map (fn thm => thm OF reps) thms
              end) rel_flat_rawss T_Reps |> flat,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps,
            invar_raw_simps, invar_Shape_simps, invar_map_raw_closeds, invar_flat_raws,
            pred_maps, pred_Trues, rel_maps,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt)));
end
\<close>
lemma rel_T: fixes R Q shows "rel_T R Q (T g) (T g') =
  rel_GF R Q
   (rel_T (rel_F1A R Q) (rel_F1B R Q))
   (rel_T (rel_F2A R Q) (rel_F2B R Q))
   (rel_S (rel_E3A R Q) (rel_E3B R Q)) g g'"
thm rel_flat_rawF
thm rel_flat_rawE
thm Rep_T Rep_S rawF.rel_inject
  apply (tactic \<open>mk_rel_T_eq_tac @{context}
    @{thms rel_T_def rel_S_def}
    @{thm T_def}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms rawF.rel_inject rawE.rel_inject}
    @{thms shape3A.rel_inject shape3B.rel_inject}
    [@{thms rel_flat_rawF}, @{thms rel_flat_rawE}]
    @{thms Rep_T Rep_S}
    @{thms invar_rawF.simps invar_rawE.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.rel_map GE.rel_map}
    @{thms GF.pred_cong GF.pred_cong}\<close>)
  done
(*
thm Rep_T[unfolded mem_Collect_eq]
thm rel_flat_rawF[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]]
  unfolding rel_T_def rel_S_def T_def vimage2p_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawF.rel_inject GF.rel_map shape3A.rel_inject shape3B.rel_inject
      rel_flat_rawF[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]]
      rel_flat_rawE[OF Rep_S[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]]
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF.simps invar_shape3A.simps invar_shape3B.simps
      invar_shape_map_closed invar_flat
      GF.pred_map GF.pred_True
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong)
  done*)

lemma rel_S: fixes R Q shows "rel_S R Q (S g) (S g') =
  rel_GE R Q
   (rel_T (rel_F1A R Q) (rel_F1B R Q))
   (rel_T (rel_F2A R Q) (rel_F2B R Q))
   (rel_S (rel_E3A R Q) (rel_E3B R Q)) g g'"
  unfolding rel_T_def rel_S_def S_def vimage2p_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawE.rel_inject GE.rel_map shape3A.rel_inject shape3B.rel_inject
      rel_flat_rawF[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]]
      rel_flat_rawE[OF Rep_S[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]]
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawE.simps invar_shape3A.simps invar_shape3B.simps
      invar_map_closed invar_flat
      GE.pred_map GE.pred_True
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong)
  done

section \<open>Recursion\<close>
(*
(* MIND-FUCK.THY *)

(*normal datatype recursor
  (('b, 'a) G \<Rightarrow> 'a) \<Rightarrow>
  'b T \<Rightarrow> 'a
*)

(*generalized recursor
  (\<forall>'a. ('a L, 'a F K) G \<Rightarrow> 'a K) \<Rightarrow>
  (\<forall>'a. 'a L F \<Rightarrow> 'a F L) \<Rightarrow>
  'b L T \<Rightarrow> 'b K
*)

bnf_axiomatization ('a, 'b, 'c, 'd) RF
bnf_axiomatization ('a, 'b, 'c, 'd) RE
bnf_axiomatization ('e, 'f, 'g) DA
bnf_axiomatization ('h, 'i, 'j) DB

consts defobjF :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB,
  (('a, 'c) F1A, ('b, 'd) F1A, ('a, 'c) F1B, ('b, 'd) F1B) RF,
  (('a, 'c) F2A, ('b, 'd) F2A, ('a, 'c) F2B, ('b, 'd) F2B) RF,
  (('a, 'c) E3A, ('b, 'd) E3A, ('a, 'c) E3B, ('b, 'd) E3B) RE) GF \<Rightarrow> ('a, 'b, 'c, 'd) RF"
consts defobjE :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB,
  (('a, 'c) F1A, ('b, 'd) F1A, ('a, 'c) F1B, ('b, 'd) F1B) RF,
  (('a, 'c) F2A, ('b, 'd) F2A, ('a, 'c) F2B, ('b, 'd) F2B) RF,
  (('a, 'c) E3A, ('b, 'd) E3A, ('a, 'c) E3B, ('b, 'd) E3B) RE) GE \<Rightarrow> ('a, 'b, 'c, 'd) RE"
consts argobj1A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1A \<Rightarrow> (('e, 'h) F1A, ('f, 'i) F1A, ('g, 'j) F1A) DA"
consts argobj1B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1B \<Rightarrow> (('e, 'h) F1B, ('f, 'i) F1B, ('g, 'j) F1B) DB"
consts argobj2A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2A \<Rightarrow> (('e, 'h) F2A, ('f, 'i) F2A, ('g, 'j) F2A) DA"
consts argobj2B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2B \<Rightarrow> (('e, 'h) F2B, ('f, 'i) F2B, ('g, 'j) F2B) DB"
consts argobj3A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3A \<Rightarrow> (('e, 'h) E3A, ('f, 'i) E3A, ('g, 'j) E3A) DA"
consts argobj3B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3B \<Rightarrow> (('e, 'h) E3B, ('f, 'i) E3B, ('g, 'j) E3B) DB"

axiomatization where
  defobjF_transfer: "\<And>RA RB RC RD RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    bi_unique RA \<Longrightarrow> bi_unique RB \<Longrightarrow> bi_unique RC \<Longrightarrow> bi_unique RD \<Longrightarrow>
    rel_fun (rel_GF (rel_DA RE RF RG) (rel_DB RH RI RJ) (rel_RF (rel_F1A RA RC) (rel_F1A RB RD) (rel_F1B RA RC) (rel_F1B RB RD))
    (rel_RF (rel_F2A RA RC) (rel_F2A RB RD) (rel_F2B RA RC) (rel_F2B RB RD)) (rel_RE (rel_E3A RA RC) (rel_E3A RB RD) (rel_E3B RA RC) (rel_E3B RB RD)))
    (rel_RF RA RB RC RD) defobjF defobjF" and
  defobjE_transfer: "\<And>RA RB RC RD RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    bi_unique RA \<Longrightarrow> bi_unique RB \<Longrightarrow> bi_unique RC \<Longrightarrow> bi_unique RD \<Longrightarrow>
    rel_fun (rel_GE (rel_DA RE RF RG) (rel_DB RH RI RJ) (rel_RF (rel_F1A RA RC) (rel_F1A RB RD) (rel_F1B RA RC) (rel_F1B RB RD))
    (rel_RF (rel_F2A RA RC) (rel_F2A RB RD) (rel_F2B RA RC) (rel_F2B RB RD)) (rel_RE (rel_E3A RA RC) (rel_E3A RB RD) (rel_E3B RA RC) (rel_E3B RB RD)))
    (rel_RE RA RB RC RD) defobjE defobjE" and
  argobj1A_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F1A (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DA (rel_F1A RE RH) (rel_F1A RF RI) (rel_F1A RG RJ)) argobj1A argobj1A" and
  argobj1B_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F1B (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DB (rel_F1B RE RH) (rel_F1B RF RI) (rel_F1B RG RJ)) argobj1B argobj1B" and
  argobj2A_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F2A (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DA (rel_F2A RE RH) (rel_F2A RF RI) (rel_F2A RG RJ)) argobj2A argobj2A" and
  argobj2B_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F2B (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DB (rel_F2B RE RH) (rel_F2B RF RI) (rel_F2B RG RJ)) argobj2B argobj2B" and
  argobj3A_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_E3A (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DA (rel_E3A RE RH) (rel_E3A RF RI) (rel_E3A RG RJ)) argobj3A argobj3A" and
  argobj3B_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_E3B (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DB (rel_E3B RE RH) (rel_E3B RF RI) (rel_E3B RG RJ)) argobj3B argobj3B"

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto

lemma RF_pred_mono: "P1 \<le> Q1 \<Longrightarrow> P2 \<le> Q2 \<Longrightarrow> P3 \<le> Q3 \<Longrightarrow> P4 \<le> Q4 \<Longrightarrow> pred_RF P1 P2 P3 P4 \<le> pred_RF Q1 Q2 Q3 Q4"
  using RF.pred_mono_strong[of P1 P2 P3 P4 _ Q1 Q2 Q3 Q4] by force
lemma RE_pred_mono: "P1 \<le> Q1 \<Longrightarrow> P2 \<le> Q2 \<Longrightarrow> P3 \<le> Q3 \<Longrightarrow> P4 \<le> Q4 \<Longrightarrow> pred_RE P1 P2 P3 P4 \<le> pred_RE Q1 Q2 Q3 Q4"
  using RE.pred_mono_strong[of P1 P2 P3 P4 _ Q1 Q2 Q3 Q4] by force

lemma defobjF_invar: "pred_fun (pred_GF (pred_DA RE RF RG) (pred_DB RH RI RJ)
  (pred_RF (pred_F1A RA RC) (pred_F1A RB RD) (pred_F1B RA RC) (pred_F1B RB RD))
  (pred_RF (pred_F2A RA RC) (pred_F2A RB RD) (pred_F2B RA RC) (pred_F2B RB RD))
  (pred_RE (pred_E3A RA RC) (pred_E3A RB RD) (pred_E3B RA RC) (pred_E3B RB RD))) (pred_RF RA RB RC RD) defobjF"
  unfolding fun_pred_rel GF.rel_eq_onp[symmetric]
    F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric] F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric] E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] RF.rel_eq_onp[symmetric] RE.rel_eq_onp[symmetric]
  apply (rule defobjF_transfer) sorry
lemma defobjE_invar: "pred_fun (pred_GE (pred_DA RE RF RG) (pred_DB RH RI RJ)
  (pred_RF (pred_F1A RA RC) (pred_F1A RB RD) (pred_F1B RA RC) (pred_F1B RB RD))
  (pred_RF (pred_F2A RA RC) (pred_F2A RB RD) (pred_F2B RA RC) (pred_F2B RB RD))
  (pred_RE (pred_E3A RA RC) (pred_E3A RB RD) (pred_E3B RA RC) (pred_E3B RB RD))) (pred_RE RA RB RC RD) defobjE"
  unfolding fun_pred_rel GE.rel_eq_onp[symmetric]
    F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric] F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric] E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] RF.rel_eq_onp[symmetric] RE.rel_eq_onp[symmetric]
  apply (rule defobjE_transfer) sorry

lemma argobj1A_invar: "pred_fun (pred_F1A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_F1A RE RH) (pred_F1A RF RI) (pred_F1A RG RJ)) argobj1A"
  unfolding fun_pred_rel F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj1A_transfer) sorry
lemma argobj1B_invar: "pred_fun (pred_F1B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_F1B RE RH) (pred_F1B RF RI) (pred_F1B RG RJ)) argobj1B"
  unfolding fun_pred_rel F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj1B_transfer) sorry
lemma argobj2A_invar: "pred_fun (pred_F2A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_F2A RE RH) (pred_F2A RF RI) (pred_F2A RG RJ)) argobj2A"
  unfolding fun_pred_rel F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj2A_transfer) sorry
lemma argobj2B_invar: "pred_fun (pred_F2B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_F2B RE RH) (pred_F2B RF RI) (pred_F2B RG RJ)) argobj2B"
  unfolding fun_pred_rel F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj2B_transfer) sorry
lemma argobj3A_invar: "pred_fun (pred_E3A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_E3A RE RH) (pred_E3A RF RI) (pred_E3A RG RJ)) argobj3A"
  unfolding fun_pred_rel E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj3A_transfer) sorry
lemma argobj3B_invar: "pred_fun (pred_E3B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_E3B RE RH) (pred_E3B RF RI) (pred_E3B RG RJ)) argobj3B"
  unfolding fun_pred_rel E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj3B_transfer) sorry

lemma GrpD: "BNF_Def.Grp A f x y \<Longrightarrow> f x = y"
  by (rule GrpE)

lemma defobjF_natural:
  assumes
  "inj_on fe (\<Union>(set1_DA ` set1_GF g))" 
  "inj_on ff (\<Union>(set2_DA ` set1_GF g))"
  "inj_on fg (\<Union>(set3_DA ` set1_GF g))"
  "inj_on fh (\<Union>(set1_DB ` set2_GF g))" 
  "inj_on fi (\<Union>(set2_DB ` set2_GF g))"
  "inj_on fj (\<Union>(set3_DB ` set2_GF g))"
  "inj_on fa (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GF g)))"
  "inj_on fb (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GF g)))"
  "inj_on fc (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GF g)))"
  "inj_on fd (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GF g)))"
  shows "defobjF (map_GF (map_DA fe ff fg) (map_DB fh fi fj)
   (map_RF (map_F1A fa fc) (map_F1A fb fd) (map_F1B fa fc) (map_F1B fb fd))
   (map_RF (map_F2A fa fc) (map_F2A fb fd) (map_F2B fa fc) (map_F2B fb fd))
   (map_RE (map_E3A fa fc) (map_E3A fb fd) (map_E3B fa fc) (map_E3B fb fd)) g) = map_RF fa fb fc fd (defobjF g)"
  apply (rule rel_funD[OF defobjF_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_GF g)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_GF g)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_GF g)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_GF g)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_GF g)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_GF g)) fj"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GF g))) fa"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GF g))) fb"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GF g))) fc"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GF g))) fd",
    unfolded bi_unique_Grp, OF assms, unfolded
      F1A.rel_Grp F1B.rel_Grp F2A.rel_Grp F2B.rel_Grp E3A.rel_Grp E3B.rel_Grp
      RF.rel_Grp RE.rel_Grp DA.rel_Grp DB.rel_Grp GF.rel_Grp GE.rel_Grp, THEN GrpD, THEN sym])
  apply (rule GrpI[OF refl])
  apply (blast 12)
  done
lemma defobjE_natural:
  assumes
  "inj_on fe (\<Union>(set1_DA ` set1_GE g))" 
  "inj_on ff (\<Union>(set2_DA ` set1_GE g))"
  "inj_on fg (\<Union>(set3_DA ` set1_GE g))"
  "inj_on fh (\<Union>(set1_DB ` set2_GE g))" 
  "inj_on fi (\<Union>(set2_DB ` set2_GE g))"
  "inj_on fj (\<Union>(set3_DB ` set2_GE g))"
  "inj_on fa (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GE g)))"
  "inj_on fb (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GE g)))"
  "inj_on fc (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GE g)))"
  "inj_on fd (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GE g)))"
  shows  "defobjE (map_GE (map_DA fe ff fg) (map_DB fh fi fj)
   (map_RF (map_F1A fa fc) (map_F1A fb fd) (map_F1B fa fc) (map_F1B fb fd))
   (map_RF (map_F2A fa fc) (map_F2A fb fd) (map_F2B fa fc) (map_F2B fb fd))
   (map_RE (map_E3A fa fc) (map_E3A fb fd) (map_E3B fa fc) (map_E3B fb fd)) g) = map_RE fa fb fc fd (defobjE g)"
  apply (rule rel_funD[OF defobjE_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_GE g)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_GE g)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_GE g)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_GE g)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_GE g)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_GE g)) fj"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GE g))) fa"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GE g))) fb"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GE g))) fc"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GE g))) fd",
    unfolded bi_unique_Grp, OF assms, unfolded
      F1A.rel_Grp F1B.rel_Grp F2A.rel_Grp F2B.rel_Grp E3A.rel_Grp E3B.rel_Grp
      RF.rel_Grp RE.rel_Grp DA.rel_Grp DB.rel_Grp GF.rel_Grp GE.rel_Grp, THEN GrpD, THEN sym])
  apply (rule GrpI[OF refl])
  apply (blast 12)
  done

lemma argobj1A_natural: "inj_on fe (\<Union>(set1_DA ` set1_F1A f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F1A f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F1A f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F1A f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F1A f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F1A f)) \<Longrightarrow>
   argobj1A (map_F1A (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DA (map_F1A fe fh) (map_F1A ff fi) (map_F1A fg fj) (argobj1A f)"
  using rel_funD[OF argobj1A_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F1A f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F1A f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F1A f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F1A f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F1A f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F1A f)) fj"]
  unfolding F1A.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj1B_natural: "inj_on fe (\<Union>(set1_DA ` set1_F1B f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F1B f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F1B f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F1B f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F1B f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F1B f)) \<Longrightarrow>
   argobj1B (map_F1B (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DB (map_F1B fe fh) (map_F1B ff fi) (map_F1B fg fj) (argobj1B f)"
  using rel_funD[OF argobj1B_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F1B f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F1B f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F1B f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F1B f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F1B f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F1B f)) fj"]
  unfolding F1B.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj2A_natural: "inj_on fe (\<Union>(set1_DA ` set1_F2A f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F2A f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F2A f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F2A f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F2A f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F2A f)) \<Longrightarrow>
   argobj2A (map_F2A (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DA (map_F2A fe fh) (map_F2A ff fi) (map_F2A fg fj) (argobj2A f)"
  using rel_funD[OF argobj2A_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F2A f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F2A f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F2A f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F2A f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F2A f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F2A f)) fj"]
  unfolding F2A.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj2B_natural: "inj_on fe (\<Union>(set1_DA ` set1_F2B f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F2B f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F2B f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F2B f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F2B f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F2B f)) \<Longrightarrow>
   argobj2B (map_F2B (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DB (map_F2B fe fh) (map_F2B ff fi) (map_F2B fg fj) (argobj2B f)"
  using rel_funD[OF argobj2B_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F2B f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F2B f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F2B f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F2B f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F2B f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F2B f)) fj"]
  unfolding F2B.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj3A_natural: "inj_on fe (\<Union>(set1_DA ` set1_E3A f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_E3A f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_E3A f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_E3A f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_E3A f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_E3A f)) \<Longrightarrow>
   argobj3A (map_E3A (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DA (map_E3A fe fh) (map_E3A ff fi) (map_E3A fg fj) (argobj3A f)"
  using rel_funD[OF argobj3A_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_E3A f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_E3A f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_E3A f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_E3A f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_E3A f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_E3A f)) fj"]
  unfolding E3A.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj3B_natural: "inj_on fe (\<Union>(set1_DA ` set1_E3B f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_E3B f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_E3B f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_E3B f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_E3B f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_E3B f)) \<Longrightarrow>
   argobj3B (map_E3B (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DB (map_E3B fe fh) (map_E3B ff fi) (map_E3B fg fj) (argobj3B f)"
  using rel_funD[OF argobj3B_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_E3B f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_E3B f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_E3B f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_E3B f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_E3B f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_E3B f)) fj"]
  unfolding E3B.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)


lemma defobjF_flat1_natural:fixes
    u0 :: "label list" and
    gf :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
            ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) gf \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B)
    (map_RF (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B))
    (map_RF (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B))
    (map_RE (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B)) gf) =
    map_RF flat_shape1A flat_shape1A flat_shape1B flat_shape1B (defobjF gf)"
  sorry
lemma defobjF_flat2_natural:fixes
    u0 :: "label list" and
    gf :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
            ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) gf \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B)
    (map_RF (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B))
    (map_RF (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B))
    (map_RE (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B)) gf) =
    map_RF flat_shape2A flat_shape2A flat_shape2B flat_shape2B (defobjF gf)"
  sorry
lemma defobjF_flat3_natural:fixes
    u0 :: "label list" and
    gf :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
            ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) gf \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B)
    (map_RF (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B))
    (map_RF (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B))
    (map_RE (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B)) gf) =
    map_RF flat_shape3A flat_shape3A flat_shape3B flat_shape3B (defobjF gf)"
  sorry
lemma defobjE_flat1_natural:fixes
    u0 :: "label list" and
    ge :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
            ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) ge \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B)
    (map_RF (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B))
    (map_RF (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B))
    (map_RE (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B)) ge) =
    map_RF flat_shape1A flat_shape1A flat_shape1B flat_shape1B (defobjF ge)"
  sorry
lemma defobjE_flat2_natural:fixes
    u0 :: "label list" and
    ge :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
            ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) ge \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B)
    (map_RF (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B))
    (map_RF (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B))
    (map_RE (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B)) ge) =
    map_RF flat_shape2A flat_shape2A flat_shape2B flat_shape2B (defobjF ge)"
  sorry
lemma defobjE_flat3_natural:fixes
    u0 :: "label list" and
    ge :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
            ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) ge \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B)
    (map_RF (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B))
    (map_RF (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B))
    (map_RE (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B)) ge) =
    map_RF flat_shape3A flat_shape3A flat_shape3B flat_shape3B (defobjF ge)"
  sorry


lemma argobj1A_flat1_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F1A"
  shows
    "pred_F1A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1A (map_F1A (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DA (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (argobj1A x)"
  apply (rule argobj1A_natural; rule inj_onI)
  apply(rule box_equals[OF _ unflatU_flatU(1)[of u] unflatU_flatU(1)[of u]])
  apply (auto simp only: unflatU_flatU F1A.pred_set F1B.pred_set F2A.pred_set F2B.pred_set E3A.pred_set E3B.pred_set DA.pred_set DB.pred_set)
  sorry
lemma argobj1A_flat2_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F1A"
  shows
    "pred_F1A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1A (map_F1A (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DA (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (argobj1A x)"
  sorry
lemma argobj1A_flat3_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F1A"
  shows
    "pred_F1A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1A (map_F1A (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DA (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (argobj1A x)"
  sorry
lemma argobj1B_flat1_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F1B"
  shows
    "pred_F1B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1B (map_F1B (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DB (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (argobj1B x)"
  sorry
lemma argobj1B_flat2_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F1B"
  shows
    "pred_F1B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1B (map_F1B (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DB (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (argobj1B x)"
  sorry
lemma argobj1B_flat3_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F1B"
  shows
    "pred_F1B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1B (map_F1B (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DB (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (argobj1B x)"
  sorry
lemma argobj2A_flat1_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F2A"
  shows
    "pred_F2A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2A (map_F2A (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DA (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (argobj2A x)"
  sorry
lemma argobj2A_flat2_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F2A"
  shows
    "pred_F2A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2A (map_F2A (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DA (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (argobj2A x)"
  sorry
lemma argobj2A_flat3_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F2A"
  shows
    "pred_F2A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2A (map_F2A (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DA (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (argobj2A x)"
  sorry
lemma argobj2B_flat1_natural: fixes
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F2B"
  shows
    "pred_F2B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2B (map_F2B (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DB (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (argobj2B x)"
  sorry
lemma argobj2B_flat2_natural: fixes
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F2B"
  shows
    "pred_F2B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2B (map_F2B (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DB (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (argobj2B x)"
  sorry
lemma argobj2B_flat3_natural: fixes
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F2B"
  shows
    "pred_F2B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2B (map_F2B (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DB (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (argobj2B x)"
  sorry
lemma argobj3A_flat1_natural: fixes
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) E3A"
  shows
    "pred_E3A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3A (map_E3A (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DA (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (argobj3A x)"
  sorry
lemma argobj3A_flat2_natural: fixes
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) E3A"
  shows
    "pred_E3A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3A (map_E3A (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DA (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (argobj3A x)"
  sorry
lemma argobj3A_flat3_natural: fixes
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) E3A"
  shows
    "pred_E3A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3A (map_E3A (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DA (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (argobj3A x)"
  sorry
lemma argobj3B_flat1_natural: fixes
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) E3B"
  shows
    "pred_E3B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3B (map_E3B (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DB (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (argobj3B x)"
  sorry
lemma argobj3B_flat2_natural: fixes
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) E3B"
  shows
    "pred_E3B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3B (map_E3B (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DB (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (argobj3B x)"
  sorry
lemma argobj3B_flat3_natural: fixes
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) E3B"
  shows
    "pred_E3B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3B (map_E3B (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DB (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (argobj3B x)"
  sorry


(*
lemma argobj_flat_shape_natural: "pred_F (pred_D (invar_shape u) (invar_shape u) (invar_shape u)) x \<Longrightarrow>
  argobj (map_F (map_D flat_shape flat_shape flat_shape) x) =
  map_D (map_F flat_shape) (map_F flat_shape) (map_F flat_shape) (argobj x)"
  apply (rule argobj_natural; rule inj_onI;
    rule box_equals[OF _ unflat_shape_flat_shape[of u] unflat_shape_flat_shape[of u]])
  apply (auto simp only: F.pred_set D.pred_set)
  done*)


primrec f_shapeA :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shape3A \<Rightarrow> (('e, 'h) shape3A, ('f, 'i) shape3A, ('g, 'j) shape3A) DA"
    and f_shapeB :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shape3B \<Rightarrow> (('e, 'h) shape3B, ('f, 'i) shape3B, ('g, 'j) shape3B) DB" where
  "f_shapeA (LeafA a) = map_DA LeafA LeafA LeafA a"
| "f_shapeA (Node1A fa) = map_DA Node1A Node1A Node1A (argobj1A (map_F1A f_shapeA f_shapeB fa))"
| "f_shapeA (Node2A fa) = map_DA Node2A Node2A Node2A (argobj2A (map_F2A f_shapeA f_shapeB fa))"
| "f_shapeA (Node3A fa) = map_DA Node3A Node3A Node3A (argobj3A (map_E3A f_shapeA f_shapeB fa))"
| "f_shapeB (LeafB b) = map_DB LeafB LeafB LeafB b"
| "f_shapeB (Node1B fb) = map_DB Node1B Node1B Node1B (argobj1B (map_F1B f_shapeA f_shapeB fb))"
| "f_shapeB (Node2B fb) = map_DB Node2B Node2B Node2B (argobj2B (map_F2B f_shapeA f_shapeB fb))"
| "f_shapeB (Node3B fb) = map_DB Node3B Node3B Node3B (argobj3B (map_E3B f_shapeA f_shapeB fb))"

primrec f_rawF :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) rawF \<Rightarrow>
  (('a, 'c) shape3A, ('b, 'd) shape3A, ('a, 'c) shape3B, ('b, 'd) shape3B) RF" and
  f_rawE :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) rawE \<Rightarrow>
  (('a, 'c) shape3A, ('b, 'd) shape3A, ('a, 'c) shape3B, ('b, 'd) shape3B) RE" where
  "f_rawF (ConsF g) = defobjF (map_GF f_shapeA f_shapeB ((map_RF un_Node1A un_Node1A un_Node1B un_Node1B) o f_rawF)
    ((map_RF un_Node2A un_Node2A un_Node2B un_Node2B) o f_rawF) ((map_RE un_Node3A un_Node3A un_Node3B un_Node3B) o f_rawE) g)"
| "f_rawE (ConsE g) = defobjE (map_GE f_shapeA f_shapeB ((map_RF un_Node1A un_Node1A un_Node1B un_Node1B) o f_rawF)
    ((map_RF un_Node2A un_Node2A un_Node2B un_Node2B) o f_rawF) ((map_RE un_Node3A un_Node3A un_Node3B un_Node3B) o f_rawE) g)"

definition f_T :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) T \<Rightarrow> ('a, 'b, 'c, 'd) RF" where
  "f_T t = map_RF un_LeafA un_LeafA un_LeafB un_LeafB (f_rawF (Rep_T t))"
definition f_S :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) S \<Rightarrow> ('a, 'b, 'c, 'd) RE" where
  "f_S t = map_RE un_LeafA un_LeafA un_LeafB un_LeafB (f_rawE (Rep_S t))"

lemma invar_shape_f_shape: fixes ua :: "(('a, 'b, 'c) DA, ('d, 'e, 'f) DB) shape3A" and ub :: "(('a, 'b, 'c) DA, ('d, 'e, 'f) DB) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0) (f_shapeA ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0) (f_shapeB ub))"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0) (f_shapeA ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0) (f_shapeB ub)"
    ua ub])
  apply (auto simp only:
      invar_shape3A.simps invar_shape3B.simps
      f_shapeA.simps f_shapeB.simps
      DA.pred_map DB.pred_map DA.pred_True DB.pred_True
      F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      o_apply simp_thms all_simps True_implies_equals imp_conjL list.inject list.distinct
    intro!: pred_funD[OF argobj1A_invar] pred_funD[OF argobj2A_invar] pred_funD[OF argobj3A_invar]
      pred_funD[OF argobj1B_invar] pred_funD[OF argobj2B_invar] pred_funD[OF argobj3B_invar]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    cong: DA.pred_cong DB.pred_cong imp_cong
    split: list.splits label.splits if_splits)
  done

lemma f_shape_flat1_shape: fixes
      ua :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) shape3A" and 
      ub :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> f_shapeA (flat_shape1A ua) = map_DA flat_shape1A flat_shape1A flat_shape1A (f_shapeA (map_shape3A argobj1A argobj1B ua))) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> f_shapeB (flat_shape1B ub) = map_DB flat_shape1B flat_shape1B flat_shape1B (f_shapeB (map_shape3B argobj1A argobj1B ub)))"
  sorry

lemma f_shape_flat2_shape: fixes
      ua :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) shape3A" and 
      ub :: "((('g, 'h, 'i) DA, ('j, 'k, 'l) DB) F2A, (('g, 'h, 'i) DA, ('j, 'k, 'l) DB) F2B) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> f_shapeA (flat_shape2A ua) = map_DA flat_shape2A flat_shape2A flat_shape2A (f_shapeA (map_shape3A argobj2A argobj2B ua))) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> f_shapeB (flat_shape2B ub) = map_DB flat_shape2B flat_shape2B flat_shape2B (f_shapeB (map_shape3B argobj2A argobj2B ub)))"
  sorry
lemma f_shape_flat3_shape: fixes
      ua :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) shape3A" and 
      ub :: "((('g, 'h, 'i) DA, ('j, 'k, 'l) DB) E3A, (('g, 'h, 'i) DA, ('j, 'k, 'l) DB) E3B) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> f_shapeA (flat_shape3A ua) = map_DA flat_shape3A flat_shape3A flat_shape3A (f_shapeA (map_shape3A argobj3A argobj3B ua))) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> f_shapeB (flat_shape3B ub) = map_DB flat_shape3B flat_shape3B flat_shape3B (f_shapeB (map_shape3B argobj3A argobj3B ub)))"
  sorry

lemma invar_f_raw1: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> pred_RF (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawF ::
  ((('a, 'd) F1A, ('b, 'e) F1A, ('c, 'f) F1A) DA, (('a, 'd) F1B, ('b, 'e) F1B, ('c, 'f) F1B) DB) rawF
   \<Rightarrow> ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3A,
       (('g, 'h) F1A, ('g, 'h) F1B) shape3B, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) RF) (map_rawF argobj1A argobj1B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> pred_RE (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawE ::
  ((('a, 'd) F1A, ('b, 'e) F1A, ('c, 'f) F1A) DA, (('a, 'd) F1B, ('b, 'e) F1B, ('c, 'f) F1B) DB) rawE
   \<Rightarrow> ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3A,
       (('g, 'h) F1A, ('g, 'h) F1B) shape3B, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) RE) (map_rawE argobj1A argobj1B s)))"
  sorry
lemma invar_f_raw2: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> pred_RF (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawF ::
  ((('a, 'd) F2A, ('b, 'e) F2A, ('c, 'f) F2A) DA, (('a, 'd) F2B, ('b, 'e) F2B, ('c, 'f) F2B) DB) rawF
   \<Rightarrow> ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3A,
       (('g, 'h) F2A, ('g, 'h) F2B) shape3B, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) RF) (map_rawF argobj2A argobj2B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> pred_RE (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawE ::
  ((('a, 'd) F2A, ('b, 'e) F2A, ('c, 'f) F2A) DA, (('a, 'd) F2B, ('b, 'e) F2B, ('c, 'f) F2B) DB) rawE
   \<Rightarrow> ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3A,
       (('g, 'h) F2A, ('g, 'h) F2B) shape3B, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) RE) (map_rawE argobj2A argobj2B s)))"
  sorry
lemma invar_f_raw3: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> pred_RF (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawF ::
  ((('a, 'd) E3A, ('b, 'e) E3A, ('c, 'f) E3A) DA, (('a, 'd) E3B, ('b, 'e) E3B, ('c, 'f) E3B) DB) rawF
   \<Rightarrow> ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3A,
       (('g, 'h) E3A, ('g, 'h) E3B) shape3B, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) RF) (map_rawF argobj3A argobj3B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> pred_RE (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawE ::
  ((('a, 'd) E3A, ('b, 'e) E3A, ('c, 'f) E3A) DA, (('a, 'd) E3B, ('b, 'e) E3B, ('c, 'f) E3B) DB) rawE
   \<Rightarrow> ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3A,
       (('g, 'h) E3A, ('g, 'h) E3B) shape3B, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) RE) (map_rawE argobj3A argobj3B s)))"
  sorry

lemma f_raw_flat1: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> f_rawF (flat1_rawF t) = map_RF flat_shape1A flat_shape1A flat_shape1B flat_shape1B (f_rawF (map_rawF argobj1A argobj1B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> f_rawE (flat1_rawE s) = map_RE flat_shape1A flat_shape1A flat_shape1B flat_shape1B (f_rawE (map_rawE argobj1A argobj1B s)))"
  sorry
lemma f_raw_flat2: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> f_rawF (flat2_rawF t) = map_RF flat_shape2A flat_shape2A flat_shape2B flat_shape2B (f_rawF (map_rawF argobj2A argobj2B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> f_rawE (flat2_rawE s) = map_RE flat_shape2A flat_shape2A flat_shape2B flat_shape2B (f_rawE (map_rawE argobj2A argobj2B s)))"
  sorry
lemma f_raw_flat3: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> f_rawF (flat3_rawF t) = map_RF flat_shape3A flat_shape3A flat_shape3B flat_shape3B (f_rawF (map_rawF argobj3A argobj3B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> f_rawE (flat3_rawE s) = map_RE flat_shape3A flat_shape3A flat_shape3B flat_shape3B (f_rawE (map_rawE argobj3A argobj3B s)))"
  sorry

lemma fixes
      g :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB,
        ((('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1A,
         (('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1B) T,
        ((('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2A,
         (('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2B) T,
        ((('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3A,
         (('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3B) S) GF" and
      h :: "(('o, 'p, 'q) DA, ('r, 's, 't) DB,
        ((('o, 'p, 'q) DA, ('r, 's, 't) DB) F1A,
         (('o, 'p, 'q) DA, ('r, 's, 't) DB) F1B) T,
        ((('o, 'p, 'q) DA, ('r, 's, 't) DB) F2A,
         (('o, 'p, 'q) DA, ('r, 's, 't) DB) F2B) T,
        ((('o, 'p, 'q) DA, ('r, 's, 't) DB) E3A,
         (('o, 'p, 'q) DA, ('r, 's, 't) DB) E3B) S) GE"
    shows
 "f_T (T g) = defobjF (map_GF id id (f_T o map_T argobj1A argobj1B) (f_T o map_T argobj2A argobj2B) (f_S o map_S argobj3A argobj3B) g) \<and>
  f_S (S h) = defobjE (map_GE id id (f_T o map_T argobj1A argobj1B) (f_T o map_T argobj2A argobj2B) (f_S o map_S argobj3A argobj3B) h)"
  sorry
*)
section \<open>Induction\<close>

(* The "recursion sizes" of elements of 'a raw -- on the second argument of G *)
datatype szF = SConsF "(unit, unit, szF, szF, szE) GF"
     and szE = SConsE "(unit, unit, szF, szF, szE) GE"

primrec sizeF :: "('a, 'b) rawF \<Rightarrow> szF"
    and sizeE :: "('a, 'b) rawE \<Rightarrow> szE" where
  "sizeF (ConsF g) = SConsF (map_GF (\<lambda>_. ()) (\<lambda>_. ()) sizeF sizeF sizeE g)"
| "sizeE (ConsE g) = SConsE (map_GE (\<lambda>_. ()) (\<lambda>_. ()) sizeF sizeF sizeE g)"

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_sz_of_unflat_raw_tac ctxt raw_induct unflat_raw_simps sz_injects sz_of_simps
    G_map_congs G_map_comps =
  HEADGOAL (rtac ctxt raw_induct) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: G_map_congs);
      val simp = full_simp_tac
        (ss_only (flat [unflat_raw_simps, sz_of_simps, G_map_comps, sz_injects,
          @{thms o_apply}])
            ctxt);
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
        FIRST' [hyp_subst_tac ctxt, intro, apply_IHs, simp])
    end) ctxt);
end
\<close>
thm szF.simps
lemma size_unflat1_raw: 
  "(\<forall>ul. sizeF (unflat1_rawF ul r) = sizeF (r :: ('a, 'b) rawF)) \<and>
   (\<forall>ul. sizeE (unflat1_rawE ul s) = sizeE (s :: ('a, 'b) rawE))"
  apply (tactic \<open>mk_sz_of_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms szF.inject szE.inject}
    @{thms sizeF.simps sizeE.simps}
    @{thms GF.map_cong GE.map_cong}
    @{thms GF.map_comp GE.map_comp}
  \<close>)
  done
(*
  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: unflat1_rawF.simps unflat1_rawE.simps sizeF.simps sizeE.simps
      GF.map_comp GE.map_comp o_apply)
  done
*)

lemmas size_unflat1 =
  spec[OF conjunct1[OF size_unflat1_raw]]
  spec[OF conjunct2[OF size_unflat1_raw]]

lemma size_unflat2_raw: 
  "(\<forall>ul. sizeF (unflat2_rawF ul r) = sizeF (r :: ('a, 'b) rawF)) \<and>
   (\<forall>ul. sizeE (unflat2_rawE ul s) = sizeE (s :: ('a, 'b) rawE))"
  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: unflat2_rawF.simps unflat2_rawE.simps sizeF.simps sizeE.simps
      GF.map_comp GE.map_comp o_apply)
  done

lemmas size_unflat2 =
  spec[OF conjunct1[OF size_unflat2_raw]]
  spec[OF conjunct2[OF size_unflat2_raw]]

lemma size_unflat3_raw: 
  "(\<forall>ul. sizeF (unflat3_rawF ul r) = sizeF (r :: ('a, 'b) rawF)) \<and>
   (\<forall>ul. sizeE (unflat3_rawE ul s) = sizeE (s :: ('a, 'b) rawE))"
  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: unflat3_rawF.simps unflat3_rawE.simps sizeF.simps sizeE.simps
      GF.map_comp GE.map_comp o_apply)
  done

lemmas size_unflat3 =
  spec[OF conjunct1[OF size_unflat3_raw]]
  spec[OF conjunct2[OF size_unflat3_raw]]


ML \<open>
local
open Ctr_Sugar_Util
in

  fun mk_sz_of_map_raw_tac ctxt raw_induct raw_maps sz_injects sz_of_simps
    G_map_congs G_map_comps =
  HEADGOAL (rtac ctxt raw_induct) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt IHs THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt G_map_congs;
      val simp = full_simp_tac
        (ss_only (flat [raw_maps, sz_of_simps, G_map_comps, sz_injects,
            @{thms o_apply}]) ctxt);
    in
      HEADGOAL (REPEAT_DETERM_N 100 o
        FIRST' [hyp_subst_tac ctxt, intro, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma size_map_raw_raw: 
  "sizeF (map_rawF f g r) = sizeF r \<and>
   sizeE (map_rawE f g s) = sizeE s"
  apply (tactic \<open>mk_sz_of_map_raw_tac @{context}
    @{thm rawF_rawE.induct}
    @{thms rawF.map rawE.map}
    @{thms szF.inject szE.inject}
    @{thms sizeF.simps sizeE.simps}
    @{thms GF.map_cong GE.map_cong}
    @{thms GF.map_comp GE.map_comp}
  \<close>)
  done
(*
  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: rawF.map rawE.map sizeF.simps sizeE.simps GF.map_comp GE.map_comp o_apply)
  done
*)
lemmas size_map_raw = conjunct1[OF size_map_raw_raw] conjunct2[OF size_map_raw_raw]


ML \<open>
local
open Ctr_Sugar_Util
open BNF_FP_Rec_Sugar_Util
in

  fun mk_raw_induct_sz_tac ctxt IHs reduce_ctrm sz_induct induct_ctrms sz_cases =
    let
      val n = length (IHs |> map (tap (tracing o Thm.string_of_thm ctxt)));
    in
      HEADGOAL (EVERY' [rtac ctxt (infer_instantiate' ctxt [SOME reduce_ctrm] rev_mp),
        REPEAT_DETERM_N n o rtac ctxt allI,
        rtac ctxt (infer_instantiate' ctxt (map SOME induct_ctrms) sz_induct) THEN_ALL_NEW
        Subgoal.FOCUS (fn {context = ctxt, prems = raw_IHs', ...} =>
        let
          val IHs' = map (fn thm => thm RS spec RS mp) raw_IHs';
          val apply_IHs = resolve_tac ctxt IHs THEN'
            EVERY' (map (fn IH' =>
              rtac ctxt IH' THEN_ALL_NEW asm_full_simp_tac (ss_only sz_cases ctxt)) IHs');
          val intro = resolve_tac ctxt [allI, impI];
        in
          HEADGOAL (REPEAT_DETERM o FIRST' [intro, apply_IHs])
        end) ctxt,
        EVERY' [rtac ctxt impI, REPEAT_DETERM_N n o etac ctxt allE,
          CONJ_WRAP' (fn i => EVERY' [dtac ctxt (mk_conjunctN n i), dtac ctxt spec, etac ctxt mp,
            rtac ctxt refl]) (1 upto n)]])
    end;
end
\<close>
lemma raw_induct_size: 
  assumes "\<And> r :: ('a, 'b) rawF.
    (\<And>r'. sizeF r' \<in> set3_GF (case sizeF r of SConsF x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow>
    (\<And>r'. sizeF r' \<in> set4_GF (case sizeF r of SConsF x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow>
    (\<And>s'. sizeE s' \<in> set5_GF (case sizeF r of SConsF x \<Rightarrow> x) \<Longrightarrow> QQ s') \<Longrightarrow> PP r"
    "\<And> s :: ('a, 'b) rawE.
    (\<And>r'. sizeF r' \<in> set3_GE (case sizeE s of SConsE x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow>
    (\<And>r'. sizeF r' \<in> set4_GE (case sizeE s of SConsE x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow>
    (\<And>s'. sizeE s' \<in> set5_GE (case sizeE s of SConsE x \<Rightarrow> x) \<Longrightarrow> QQ s') \<Longrightarrow> QQ s"
  shows "PP r \<and> QQ s"
  by (tactic \<open>mk_raw_induct_sz_tac @{context}
    @{thms assms}
    @{cterm "\<forall>szF szE. (\<forall>s. sizeF s = szF \<longrightarrow> PP s) \<and> (\<forall>s. sizeE s = szE \<longrightarrow> QQ s)"}
    @{thm szF_szE.induct}
    [@{cterm "\<lambda>szF. \<forall>r. sizeF r = szF \<longrightarrow> PP r"}, @{cterm "\<lambda>szE. \<forall>s. sizeE s = szE \<longrightarrow> QQ s"}]
    @{thms szF.case szE.case}
  \<close>)

(*  THE ASSUMPTIONS: *)

consts P :: "('a, 'b) T \<Rightarrow> bool"
consts Q :: "('a, 'b) S \<Rightarrow> bool"

axiomatization where
P_ind: "\<And>t. (\<And> t'. t' \<in> set3_GF t \<Longrightarrow> P t') \<Longrightarrow> (\<And> t'. t' \<in> set4_GF t \<Longrightarrow> P t') \<Longrightarrow> (\<And> t'. t' \<in> set5_GF t \<Longrightarrow> Q t') \<Longrightarrow> P (T t :: ('a, 'b) T)" and
Q_ind: "\<And>t. (\<And> t'. t' \<in> set3_GE t \<Longrightarrow> P t') \<Longrightarrow> (\<And> t'. t' \<in> set4_GE t \<Longrightarrow> P t') \<Longrightarrow> (\<And> t'. t' \<in> set5_GE t \<Longrightarrow> Q t') \<Longrightarrow> Q (S t :: ('a, 'b) S)" and
P_param: "\<And>R S :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_T R S) (=) P P" and
Q_param: "\<And>R S :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_S R S) (=) Q Q"



ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_P_mono_tac ctxt P_param us T_rel_Grp =
  HEADGOAL (Method.insert_tac ctxt [infer_instantiate' ctxt (map SOME us) P_param]) THEN
  unfold_tac ctxt (T_rel_Grp :: @{thms rel_fun_def bi_unique_Grp}) THEN
  unfold_tac ctxt @{thms Grp_def} THEN
  HEADGOAL (EVERY' [asm_full_simp_tac (ss_only @{thms mem_Collect_eq True_implies_equals} ctxt),
    REPEAT_DETERM o dresolve_tac ctxt [spec, mp], rtac ctxt conjI, rtac ctxt refl,
    CONJ_WRAP' (fn _ => rtac ctxt subset_refl) us, etac ctxt iffD2, assume_tac ctxt])
end
\<close>

lemma P_mono: "inj_on f (set1_T a) \<Longrightarrow> inj_on g (set2_T a) \<Longrightarrow> P (map_T f g a) \<Longrightarrow> P a"
  apply (tactic \<open>mk_P_mono_tac @{context}
    @{thm P_param}
    [@{cterm "BNF_Def.Grp (set1_T a) f"}, @{cterm "BNF_Def.Grp (set2_T a) g"}]
    @{thm T.rel_Grp}
  \<close>)
  done
(*
  using P_param[of "BNF_Def.Grp (set1_T a) f" "BNF_Def.Grp (set2_T a) g"]
  unfolding T.rel_Grp rel_fun_def bi_unique_Grp unfolding Grp_def
  apply (auto simp only:
    simp_thms mem_Collect_eq True_implies_equals implies_True_equals all_simps ex_simps)
  done
*)
lemma Q_mono: "inj_on f (set1_S a) \<Longrightarrow> inj_on g (set2_S a) \<Longrightarrow> Q (map_S f g a) \<Longrightarrow> Q a"
  apply (tactic \<open>mk_P_mono_tac @{context}
    @{thm Q_param}
    [@{cterm "BNF_Def.Grp (set1_S a) f"}, @{cterm "BNF_Def.Grp (set2_S a) g"}]
    @{thm S.rel_Grp}
  \<close>)
  done
(*
  using Q_param[of "BNF_Def.Grp (set1_S a) f" "BNF_Def.Grp (set2_S a) g"]
  unfolding S.rel_Grp rel_fun_def bi_unique_Grp unfolding Grp_def
  apply (auto simp only:
    simp_thms mem_Collect_eq True_implies_equals implies_True_equals all_simps ex_simps)
  done
*)


lemma P_raw: 
  fixes r :: "(('a, 'b) shape3A, ('a, 'b) shape3B) rawF"
    and s :: "(('a, 'b) shape3A, ('a, 'b) shape3B) rawE"
  shows "(invar_rawF [] r \<longrightarrow> P (Abs_T r)) \<and>
         (invar_rawE [] s \<longrightarrow> Q (Abs_S s))"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_Tactics

      fun mk_P_raw_tac ctxt Ps P_inds P_monos T_un_Ts un_T_defs T_Abs_inverses T_map_defs
        Nodess shape_injects raw_cases raw_exhausts raw_induct_sz
        invar_ctrms invar_raw_simps invar_raw_map_closeds invar_raw_unflats
        G_pred_sets G_set_maps
        sz_of_simps sz_cases sz_map_raws sz_unflats =
        let
          val P_arg_cong_insts = map2 (fn P => fn T_un_T => ((T_un_T RS
              (infer_instantiate' ctxt [NONE, NONE, SOME P] arg_cong)) RS iffD1)) Ps T_un_Ts;
          val P_mono_insts = flat (map (fn Nodes => map (fn P_mono =>
            infer_instantiate' ctxt ([SOME (hd Nodes), NONE] @ drop 1 (map SOME Nodes)) P_mono)
            P_monos) Nodess);
          val invar_arg_cong_insts = map (fn invar =>
            infer_instantiate' ctxt [NONE, NONE, SOME invar] arg_cong RS iffD1) invar_ctrms;
          val nr_occ = length Nodess
          val nr_t = length Ps
        in
          HEADGOAL (
            rtac ctxt raw_induct_sz THEN_ALL_NEW
            EVERY' [
              rtac ctxt impI,
              resolve_tac ctxt P_arg_cong_insts,
              resolve_tac ctxt P_inds] THEN'
            REPEAT_DETERM_N nr_t o EVERY' (map (fn i => EVERY' [
              select_prem_tac ctxt nr_occ (dtac ctxt asm_rl) i,
              full_simp_tac (ss_only (mem_Collect_eq :: un_T_defs @ G_set_maps @ T_Abs_inverses) ctxt),
              etac ctxt imageE,
              hyp_subst_tac ctxt,
              SELECT_GOAL (unfold_tac ctxt @{thms o_apply}),
              resolve_tac ctxt P_mono_insts THEN_ALL_NEW
              asm_full_simp_tac (ss_only (shape_injects @ @{thms inj_on_def simp_thms Ball_def}) ctxt),
              resolve_tac ctxt raw_exhausts,
              forward_tac ctxt invar_arg_cong_insts,
              assume_tac ctxt,
              asm_full_simp_tac (ss_only (flat [invar_raw_simps, raw_cases, G_pred_sets, T_map_defs,
                T_Abs_inverses, invar_raw_unflats, @{thms o_apply mem_Collect_eq snoc.simps Ball_def}]) ctxt),
              dtac ctxt @{thm meta_spec},
              etac ctxt (@{thm meta_mp} RS mp) THEN_ALL_NEW
              asm_full_simp_tac (ss_only (flat [sz_of_simps, sz_cases, G_set_maps, sz_map_raws,
                sz_unflats, [imageI]]) ctxt),
              asm_full_simp_tac (ss_only (flat [invar_raw_map_closeds, invar_raw_unflats,
                @{thms snoc.simps}]) ctxt)]) (1 upto nr_occ)))
        end
    in
       mk_P_raw_tac @{context}
        [ @{cterm "P::(('a, 'b) shape3A, ('a, 'b) shape3B) T \<Rightarrow> bool"},
          @{cterm "Q::(('a, 'b) shape3A, ('a, 'b) shape3B) S \<Rightarrow> bool"}]
        @{thms P_ind Q_ind}
        @{thms P_mono Q_mono}
        @{thms T_un_T S_un_S}
        @{thms un_T_def un_S_def}
        @{thms Abs_T_inverse Abs_S_inverse}
        @{thms map_T_def map_S_def}

        [[@{cterm "Node1A :: (('a, 'b) shape3A, ('a, 'b) shape3B) F1A \<Rightarrow> ('a, 'b) shape3A"}, 
          @{cterm "Node1B :: (('a, 'b) shape3A, ('a, 'b) shape3B) F1B \<Rightarrow> ('a, 'b) shape3B"}],
         [@{cterm "Node2A :: (('a, 'b) shape3A, ('a, 'b) shape3B) F2A \<Rightarrow> ('a, 'b) shape3A"}, 
          @{cterm "Node2B :: (('a, 'b) shape3A, ('a, 'b) shape3B) F2B \<Rightarrow> ('a, 'b) shape3B"}],
         [@{cterm "Node3A :: (('a, 'b) shape3A, ('a, 'b) shape3B) E3A \<Rightarrow> ('a, 'b) shape3A"}, 
          @{cterm "Node3B :: (('a, 'b) shape3A, ('a, 'b) shape3B) E3B \<Rightarrow> ('a, 'b) shape3B"}]]
        @{thms shape3A.inject shape3B.inject}
        @{thms rawF.case rawE.case}
        @{thms rawF.exhaust rawE.exhaust}
        @{thm raw_induct_size}

        [ @{cterm "(invar_rawF :: label list \<Rightarrow> (('a, 'b) shape3A, ('a, 'b) shape3B) rawF \<Rightarrow> bool) []"},
          @{cterm "(invar_rawE :: label list \<Rightarrow> (('a, 'b) shape3A, ('a, 'b) shape3B) rawE \<Rightarrow> bool) []"}]
        @{thms invar_rawF.simps invar_rawE.simps}
        @{thms invar_map_closed}
        @{thms invar_unflat}

        @{thms GF.pred_set GE.pred_set}
        @{thms GF.set_map GE.set_map}

        @{thms sizeF.simps sizeE.simps}
        @{thms szF.case szE.case}
        @{thms size_map_raw}
        @{thms size_unflat1 size_unflat2 size_unflat3}
    end\<close>)

(*
  apply (rule raw_induct_size)
  apply (rule impI)
  apply (rule iffD1[OF arg_cong[of _ _ P, OF T_un_T]])
  apply (rule P_ind)

  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (erule thin_rl)
  apply (simp only: un_T_def GF.set_map Abs_T_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule P_mono[of Node1A _ Node1B])
   apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def)
  apply (rule rawF.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar_rawF []"]])
   apply assumption
  apply (simp only: invar_rawF.simps rawF.case GF.pred_set map_T_def o_apply Abs_T_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (simp only: sizeF.simps szF.case GF.set_map size_map_raw size_unflat1 imageI) []
  apply (simp only: invar_shape_map_closed invar_unflat snoc.simps)

  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (simp only: un_T_def GF.set_map Abs_T_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule P_mono[of Node2A _ Node2B])
   apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def)
  apply (rule rawF.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar_rawF []"]])
   apply assumption
  apply (simp only: invar_rawF.simps rawF.case GF.pred_set map_T_def o_apply Abs_T_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (simp only: sizeF.simps szF.case GF.set_map size_map_raw size_unflat2 imageI) []
  apply (simp only: invar_shape_map_closed invar_unflat snoc.simps)

  apply (erule thin_rl)
  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (simp only: un_T_def GF.set_map Abs_T_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule Q_mono[of Node3A _ Node3B])
   apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def)
  apply (rule rawF.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar_rawF []"]])
   apply assumption
  apply (simp only: invar_rawF.simps rawF.case GF.pred_set map_S_def o_apply Abs_S_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (simp only: sizeF.simps szF.case GF.set_map size_map_raw size_unflat3 imageI) []
  apply (simp only: invar_shape_map_closed invar_unflat snoc.simps)

  apply (rule impI)
  apply (rule iffD1[OF arg_cong[of _ _ Q, OF S_un_S]])
  apply (rule Q_ind)

  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (erule thin_rl)
  apply (simp only: un_S_def GE.set_map Abs_S_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule P_mono[of Node1A _ Node1B])
   apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def)
  apply (rule rawE.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar_rawE []"]])
   apply assumption
  apply (simp only: invar_rawE.simps rawE.case GE.pred_set map_T_def o_apply Abs_T_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (simp only: sizeE.simps szE.case GE.set_map size_map_raw size_unflat1 imageI) []
  apply (simp only: invar_shape_map_closed invar_unflat snoc.simps)

  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (simp only: un_S_def GE.set_map Abs_S_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule P_mono[of Node2A _ Node2B])
   apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def)
  apply (rule rawE.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar_rawE []"]])
   apply assumption
  apply (simp only: invar_rawE.simps rawE.case GE.pred_set map_T_def o_apply Abs_T_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (simp only: sizeE.simps szE.case GE.set_map size_map_raw size_unflat2 imageI) []
  apply (simp only: invar_shape_map_closed invar_unflat snoc.simps)

  apply (erule thin_rl)
  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (simp only: un_S_def GE.set_map Abs_S_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule Q_mono[of Node3A _ Node3B])
   apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def)
  apply (rule rawE.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar_rawE []"]])
   apply assumption
  apply (simp only: invar_rawE.simps rawE.case GE.pred_set map_S_def o_apply Abs_S_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (simp only: sizeE.simps szE.case GE.set_map size_map_raw size_unflat3 imageI) []
  apply (simp only: invar_shape_map_closed invar_unflat snoc.simps)
  done
*)

lemma P_end_product: 
  fixes t :: "('a, 'b) T" and s :: "('a, 'b) S"
  shows "P t \<and> Q s"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_Tactics open BNF_FP_Rec_Sugar_Util val ctxt = @{context}
      val P_raw = @{thm P_raw}
      val P_monos = @{thms P_mono Q_mono}
      val Leaves = 
        [@{cterm "LeafA :: 'a \<Rightarrow> ('a, 'b) shape3A"}, 
          @{cterm "LeafB :: 'b \<Rightarrow> ('a, 'b) shape3B"}];
      val shape_injects = @{thms shape3A.inject shape3B.inject}
      val T_map_defs = @{thms map_T_def map_S_def}
      val invar_raw_map_closeds = @{thms invar_map_closed}
      val T_Reps = @{thms Rep_T Rep_S}

      fun mk_P_T_tac ctxt P_raw P_monos Leaves shape_injects T_map_defs
        invar_raw_map_closeds T_Reps =
        let
          val P_mono_insts = map (fn P_mono =>
            infer_instantiate' ctxt ([SOME (hd Leaves), NONE] @ drop 1 (map SOME Leaves)) P_mono)
            P_monos;
          val T_Rep_unfoldeds = map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps
          val n = length T_map_defs;
        in
          HEADGOAL (EVERY' [
            rtac ctxt rev_mp,
            rtac ctxt P_raw,
            rtac ctxt impI,
            CONJ_WRAP' (fn i => EVERY' [dtac ctxt (mk_conjunctN n i),
              resolve_tac ctxt P_mono_insts THEN_ALL_NEW
                full_simp_tac (ss_only (P_raw :: flat [shape_injects,
                  @{thms simp_thms inj_on_def Ball_def}]) ctxt),
              SELECT_GOAL (unfold_tac ctxt [nth T_map_defs (i - 1), @{thm o_apply}]),
              etac ctxt mp,
              full_simp_tac (ss_only (flat [invar_raw_map_closeds, T_Rep_unfoldeds]) ctxt)])
            (1 upto n)])
        end
    in 
      mk_P_T_tac ctxt P_raw P_monos Leaves shape_injects T_map_defs
        invar_raw_map_closeds T_Reps
    end\<close>)
(*
    apply (rule rev_mp)
    apply (rule P_raw)
    apply (rule impI)
    apply (erule conj_forward)

    apply (rule P_mono[of LeafA _ LeafB])
    apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def P_raw)
    apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def P_raw)
    apply (unfold map_T_def o_apply) []
    apply (erule mp)
    apply (simp only: invar_shape_map_closed Rep_T[unfolded mem_Collect_eq])

    apply (rule Q_mono[of LeafA _ LeafB])                    
    apply (simp only: inj_on_def shape3A.inject simp_thms Ball_def P_raw)
    apply (simp only: inj_on_def shape3B.inject simp_thms Ball_def P_raw)
    apply (unfold map_S_def o_apply) []
    apply (erule mp)
    apply (simp only: invar_shape_map_closed Rep_S[unfolded mem_Collect_eq])
  done
*)

(*Another way to do induction*)

abbreviation "immerse1F ul \<equiv> map_rawF Node1A Node1B o unflat1_rawF (rev ul)"
abbreviation "immerse2F ul \<equiv> map_rawF Node2A Node2B o unflat2_rawF (rev ul)"
abbreviation "immerse3F ul \<equiv> map_rawF Node3A Node3B o unflat3_rawF (rev ul)"
abbreviation "immerse1E ul \<equiv> map_rawE Node1A Node1B o unflat1_rawE (rev ul)"
abbreviation "immerse2E ul \<equiv> map_rawE Node2A Node2B o unflat2_rawE (rev ul)"
abbreviation "immerse3E ul \<equiv> map_rawE Node3A Node3B o unflat3_rawE (rev ul)"
  (* cumulative immerse: *)
primrec cimmerse_shape3A where 
  "cimmerse_shape3A [] sh = sh"
| "cimmerse_shape3A (u # ul) sh = cimmerse_shape3A ul
   ((case u of
     F1 \<Rightarrow> \<lambda>ul. map_shape3A Node1A Node1B o unflat_shape1A (rev ul)
   | F2 \<Rightarrow> \<lambda>ul. map_shape3A Node2A Node2B o unflat_shape2A (rev ul)
   | E3 \<Rightarrow> \<lambda>ul. map_shape3A Node3A Node3B o unflat_shape3A (rev ul)) ul sh)"
primrec cimmerse_shape3B where 
  "cimmerse_shape3B [] sh = sh"
| "cimmerse_shape3B (u # ul) sh = cimmerse_shape3B ul
   ((case u of
     F1 \<Rightarrow> \<lambda>ul. map_shape3B Node1A Node1B o unflat_shape1B (rev ul)
   | F2 \<Rightarrow> \<lambda>ul. map_shape3B Node2A Node2B o unflat_shape2B (rev ul)
   | E3 \<Rightarrow> \<lambda>ul. map_shape3B Node3A Node3B o unflat_shape3B (rev ul)) ul sh)"

primrec cimmerseF where 
  "cimmerseF [] r = r"
| "cimmerseF (u # ul) r = cimmerseF ul
   (case u of
     F1 \<Rightarrow> immerse1F ul r
   | F2 \<Rightarrow> immerse2F ul r
   | E3 \<Rightarrow> immerse3F ul r)"
  
primrec cimmerseE where 
  "cimmerseE [] r = r"
| "cimmerseE (u # ul) r = cimmerseE ul
   ((case u of
     F1 \<Rightarrow> immerse1E
   | F2 \<Rightarrow> immerse2E
   | E3 \<Rightarrow> immerse3E) ul r)"

lemma cimmerse_shape3A_inj[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp]:
  "\<forall>sh1 sh2. invar_shape3A (rev ul) sh1 \<longrightarrow> invar_shape3A (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3A ul sh1 = cimmerse_shape3A ul sh2 \<longrightarrow> sh1 = sh2"
  apply (rule list.induct[of "\<lambda>ul. \<forall>sh1 sh2. invar_shape3A (rev ul) sh1 \<longrightarrow> invar_shape3A (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3A ul sh1 = cimmerse_shape3A ul sh2 \<longrightarrow> sh1 = sh2" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerse_shape3A.simps(1) rev.simps(1) id_apply)
  subgoal premises prems for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
    apply (drule prems[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp, rotated -1]
        shape3A.inj_map_strong[rotated -1]
        arg_cong[of _ _ flat_shape1A]
        arg_cong[of _ _ flat_shape2A]
        arg_cong[of _ _ flat_shape3A] |
      simp only: cimmerse_shape3A.simps o_apply rev_Cons shape3A.inject shape3B.inject label.case
        invar_shape_map_closed invar_shape_unflat_shape flat_shape_unflat_shape)+
    done
  done

lemma cimmerse_shape3B_inj[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp]:
  "\<forall>sh1 sh2. invar_shape3B (rev ul) sh1 \<longrightarrow> invar_shape3B (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3B ul sh1 = cimmerse_shape3B ul sh2 \<longrightarrow> sh1 = sh2"
  apply (rule list.induct[of "\<lambda>ul. \<forall>sh1 sh2. invar_shape3B (rev ul) sh1 \<longrightarrow> invar_shape3B (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3B ul sh1 = cimmerse_shape3B ul sh2 \<longrightarrow> sh1 = sh2" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerse_shape3B.simps(1) rev.simps(1) id_apply)
  subgoal premises prems for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
    apply (drule prems[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp, rotated -1]
        shape3B.inj_map_strong[rotated -1]
        arg_cong[of _ _ flat_shape1B]
        arg_cong[of _ _ flat_shape2B]
        arg_cong[of _ _ flat_shape3B] |
      simp only: cimmerse_shape3B.simps o_apply rev_Cons shape3A.inject shape3B.inject label.case
        invar_shape_map_closed invar_shape_unflat_shape flat_shape_unflat_shape)+
    done
  done

lemma invar_cimmerseF[THEN spec, THEN mp]:
  "\<forall>r. invar_rawF (rev ul) r \<longrightarrow> invar_rawF [] (cimmerseF ul r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r. invar_rawF (rev ul) r \<longrightarrow> invar_rawF [] (cimmerseF ul r)" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerseF.simps(1) rev.simps(1) id_apply)
  subgoal for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
      apply (simp only: cimmerseF.simps(2) rev_Cons o_apply invar_map_closed invar_unflat
        label.case)+
    done
  done

lemma invar_cimmerseE[THEN spec, THEN mp]:
  "\<forall>r. invar_rawE (rev ul) r \<longrightarrow> invar_rawE [] (cimmerseE ul r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r. invar_rawE (rev ul) r \<longrightarrow> invar_rawE [] (cimmerseE ul r)" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerseE.simps(1) rev.simps(1) id_apply)
  subgoal for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
      apply (simp only: cimmerseE.simps(2) rev_Cons o_apply invar_map_closed invar_unflat
        label.case)+
    done
  done

lemma un_Cons_cimmerseF[THEN spec]:
  "\<forall>r. map_GF id id (immerse1F []) (immerse2F []) (immerse3E []) (un_ConsF (cimmerseF ul r)) = 
   map_GF (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsF r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r.  map_GF id id (immerse1F []) (immerse2F []) (immerse3E ( [])) (un_ConsF (cimmerseF ul r)) = 
   map_GF (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsF r)" ul])
   apply (rule allI)
   apply (simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps snoc.simps(1) rev.simps(1) id_apply
     label.case
     cong: GF.map_cong)
  apply (rule allI)
  apply (rule allI conjI impI | hypsubst | simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps rev_snoc
    snoc.simps o_apply id_apply GF.map_comp unflat1_rawF.simps unflat2_rawF.simps unflat3_rawF.simps rawF.map
    label.case rawF.inject rawE.inject label.distinct
    cong: GF.map_cong split: rawF.splits label.splits)+
  done

lemma un_Cons_cimmerseE[THEN spec]:
  "\<forall>r. map_GE id id (immerse1F []) (immerse2F []) (immerse3E []) (un_ConsE (cimmerseE ul r)) = 
   map_GE (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsE r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r.  map_GE id id (immerse1F []) (immerse2F []) (immerse3E []) (un_ConsE (cimmerseE ul r)) = 
   map_GE (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsE r)" ul])
   apply (rule allI)
   apply (simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps snoc.simps(1) rev.simps(1) id_apply
     label.case
     cong: GE.map_cong)
  apply (rule allI)
  apply (auto simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps rev_snoc rev.simps
    snoc.simps o_apply id_apply id_o GE.map_comp unflat1_rawE.simps unflat2_rawE.simps unflat3_rawE.simps rawE.map
    label.case
    cong: GE.map_cong split: rawE.splits label.splits)
  done

    (*  THE ASSUMPTIONS: *)
consts Q1 :: "('a, 'b) T \<Rightarrow> bool"
consts Q2 :: "('a, 'b) S \<Rightarrow> bool"
axiomatization where
  Q1_param: "\<And>(R :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (S :: 'b \<Rightarrow> 'b' \<Rightarrow> bool).
    bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_T R S) (=) Q1 Q1" and
  Q1_ind: "pred_GF (\<lambda>_. True) (\<lambda>_. True) Q1 Q1 Q2 \<le> Q1 o T" and
  Q2_param: "\<And>(R :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (S :: 'b \<Rightarrow> 'b' \<Rightarrow> bool).
    bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_S R S) (=) Q2 Q2" and
  Q2_ind: "pred_GE (\<lambda>_. True) (\<lambda>_. True) Q1 Q1 Q2 \<le> Q2 o S"
  
lemma Q1_map_T_inj:
  assumes "inj f" "inj g"
  shows "Q1 (map_T f g t) \<longleftrightarrow> Q1 t"
  using assms Q1_param[of "BNF_Def.Grp UNIV f" "BNF_Def.Grp UNIV g"] unfolding bi_unique_Grp
  unfolding rel_fun_def T.rel_Grp unfolding Grp_def
  apply (simp only: mem_Collect_eq True_implies_equals)
  by (auto simp: inj_on_def)

lemma Q2_map_S_inj:
  assumes "inj f" "inj g"
  shows "Q2 (map_S f g t) \<longleftrightarrow> Q2 t"
  using assms Q2_param[of "BNF_Def.Grp UNIV f" "BNF_Def.Grp UNIV g"] unfolding bi_unique_Grp
  unfolding rel_fun_def S.rel_Grp unfolding Grp_def
  by (auto simp: inj_on_def)
  
abbreviation Q1r where 
  "Q1r ul r \<equiv> Q1 (Abs_T (cimmerseF ul r))"
  
abbreviation Q2r where 
  "Q2r ul r \<equiv> Q2 (Abs_S (cimmerseE ul r))"

theorem Q1r_un_Cons:
  "invar_rawF (rev ul) r \<Longrightarrow> pred_GF (\<lambda>_. True)  (\<lambda>_. True) (\<lambda>x. Q1 (Abs_T x)) (\<lambda>x. Q1 (Abs_T x)) (\<lambda>x. Q2 (Abs_S x))
    (map_GF (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsF r)) \<Longrightarrow>
    Q1r ul r"
  apply (unfold un_Cons_cimmerseF[symmetric] rev.simps(1))
  apply (rule predicate1D[OF reorient_le_o_Abs[OF T_un_T Q1_ind]])
  apply (drule invar_cimmerseF)
  apply (hypsubst_thin |
    rule iffD1[OF Q1_map_T_inj[of Node1A Node1B], rotated -1]
         iffD1[OF Q1_map_T_inj[of Node2A Node2B], rotated -1]
         iffD1[OF Q2_map_S_inj[of Node3A Node3B], rotated -1] |
    drule bspec, assumption |
    erule conjE exE |
    rule allI impI ballI conjI |
    simp only:
      GF.set_map un_T_def Abs_T_inverse Abs_S_inverse map_T_def map_S_def invar_cimmerseF
      mem_Collect_eq ball_simps o_apply id_apply snoc.simps inj_on_def
      invar_unflat rawF.inject shape3A.inject shape3B.inject
      invar_rawF.simps GF.pred_set
      split: rawF.splits)+
  done

theorem Q2r_un_Cons:
  "invar_rawE (rev ul) r \<Longrightarrow> pred_GE (\<lambda>_. True)  (\<lambda>_. True) (\<lambda>x. Q1 (Abs_T x)) (\<lambda>x. Q1 (Abs_T x)) (\<lambda>x. Q2 (Abs_S x))
    (map_GE (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsE r)) \<Longrightarrow>
    Q2r ul r"
  apply (unfold un_Cons_cimmerseE[symmetric] rev.simps(1))
  apply (rule predicate1D[OF reorient_le_o_Abs[OF S_un_S Q2_ind]])
  apply (drule invar_cimmerseE)
  apply (hypsubst_thin |
    rule iffD1[OF Q1_map_T_inj[of Node1A Node1B], rotated -1]
         iffD1[OF Q1_map_T_inj[of Node2A Node2B], rotated -1]
         iffD1[OF Q2_map_S_inj[of Node3A Node3B], rotated -1] |
    drule bspec, assumption |
    erule conjE exE |
    rule allI impI ballI conjI |
    simp only:
      GE.set_map un_S_def Abs_T_inverse Abs_S_inverse map_T_def map_S_def invar_cimmerseE
      mem_Collect_eq ball_simps o_apply id_apply snoc.simps inj_on_def
      invar_unflat rawE.inject shape3A.inject shape3B.inject invar_rawE.simps GE.pred_set
      split: rawE.splits)+
  done

theorem Qrr: 
  fixes r :: "(('a, 'b) shape3A, ('a, 'b) shape3B) rawF" and s :: "(('a, 'b) shape3A, ('a, 'b) shape3B) rawE" 
  shows "(\<forall>ul. invar_rawF (rev ul) r \<longrightarrow> Q1r ul r) \<and> (\<forall>ul. invar_rawE (rev ul) s \<longrightarrow> Q2r ul s)"
  apply (rule rawF_rawE.induct[of "\<lambda>r. \<forall>ul. invar_rawF (rev ul) r \<longrightarrow> Q1r ul r" "\<lambda>s. \<forall>ul. invar_rawE (rev ul) s \<longrightarrow> Q2r ul s"])
  apply (rule allI)
  apply (rule impI)
  apply (rule Q1r_un_Cons, assumption)
  apply (hypsubst_thin |
      rule ballI impI conjI |
      simp only: GF.set_map id_apply invar_rawF.simps GF.pred_set rawF.inject rev_snoc
        ball_simps ball_triv simp_thms split: rawF.splits)+
  apply (rule allI)
  apply (rule impI)
  apply (rule Q2r_un_Cons, assumption)
  using [[simproc del: defined_All HOL.defined_All]]
  apply (hypsubst_thin |
      rule ballI impI conjI |
      simp only: GE.set_map invar_rawE.simps GE.pred_set rawE.inject
         id_apply rev_snoc ball_simps ball_triv simp_thms split: rawE.splits)+
  done

declare [[ML_print_depth = 1000]]
ML \<open>@{simproc HOL.defined_All}\<close>
ML \<open>@{context} |> Simplifier.simpset_of |> dest_ss |> #procs\<close>
thm ball_cong find_theorems "Ball _ _ = Ball _ _"

theorem Q1: "(Q1::('a, 'b) T \<Rightarrow> bool) t"
  apply (insert conjunct1[OF Qrr, THEN spec, THEN mp, of "[]" "Rep_T (map_T LeafA LeafB t)"])
  apply (rule Abs_T_cases[of t])
  apply (simp only: rev.simps
      Rep_T_inverse Rep_T[unfolded mem_Collect_eq] Q1_map_T_inj[of LeafA LeafB]
      inj_on_def shape3A.inject shape3B.inject
      simp_thms ball_simps cimmerseF.simps)
  done

theorem Q2: "(Q2::('a, 'b) S \<Rightarrow> bool) s"
  apply (insert conjunct2[OF Qrr, THEN spec, THEN mp, of "[]" "Rep_S (map_S LeafA LeafB s)"])
  apply (rule Abs_S_cases[of s])
  apply (simp only: rev.simps
      Rep_S_inverse Rep_S[unfolded mem_Collect_eq] Q2_map_S_inj[of LeafA LeafB]
      inj_on_def shape3A.inject shape3B.inject
      simp_thms ball_simps cimmerseE.simps)
  done

end