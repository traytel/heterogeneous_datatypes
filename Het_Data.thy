theory Het_Data
imports Main
begin

  (* 'a T = S | D 'a "('a\<times>'a) T"            *)

  datatype 'a R = Base 'a | Rec "'a R \<times> 'a R"

  datatype 'a T' = S' | D' "'a R" "'a T'"

  datatype shape = sBase | sRec shape

  inductive invarR :: "shape \<Rightarrow> 'a R \<Rightarrow> bool" where
    "invarR sBase (Base _)"
  | "\<lbrakk> invarR s x1; invarR s x2 \<rbrakk> \<Longrightarrow> invarR (sRec s) (Rec (x1,x2))"


  inductive_simps invarR_base[simp]: "invarR sBase x" "invarR s (Base x)"
  inductive_simps invarR_rec[simp]: "invarR (sRec s) x" "invarR s (Rec a)"

  inductive invar :: "shape \<Rightarrow> 'a T' \<Rightarrow> bool" where
    "invar s S'"
  | "\<lbrakk>invarR s x; invar (sRec s) y\<rbrakk> \<Longrightarrow> invar s (D' x y)"  

  fun downR :: "('a\<times>'a) R \<Rightarrow> 'a R" where
    "downR (Base (a,b)) = Rec (Base a, Base b)"
  | "downR (Rec (x,y)) = Rec (downR x, downR y)"
  
  fun upR :: "'a R \<Rightarrow> ('a\<times>'a) R" where
    "upR (Rec (Base a, Base b)) = Base (a,b)"
  | "upR (Rec (x,y)) = Rec (upR x, upR y)"  
  | "upR (Base _) = undefined"

  lemma up_downR_id[simp]: "upR (downR x) = x"
    by (induction x rule: upR.induct) auto

  lemma down_upR_id[simp]: "invarR (sRec s) x \<Longrightarrow> downR (upR x) = x"  
    apply (induction x arbitrary: s rule: upR.induct)
    apply (auto elim: invarR.cases intro: invarR.intros)
    done

  lemma down_invarR[simp]: "invarR (sRec s) (downR x) \<longleftrightarrow> invarR s x"
    apply (induction s arbitrary: x)
    apply (auto)
    apply (case_tac x; auto; case_tac a; auto)
    apply (case_tac x; fastforce)
    done

  lemma up_invarR: "invarR (sRec s) x \<Longrightarrow> invarR s (upR x)"  
    by (induction s arbitrary: x) auto
    
  primrec down :: "('a\<times>'a) T' \<Rightarrow> 'a T'" where
    "down S' = S'"
  | "down (D' x y) = D' (downR x) (down y)"  

  primrec up :: "'a T' \<Rightarrow> ('a\<times>'a) T'" where
    "up S' = S'"
  | "up (D' x y) = D' (upR x) (up y)"  

  lemma up_down_id[simp]: "up (down x) = x"
    by (induction x) auto

  lemma down_up_id[simp]: "invar (sRec s) x \<Longrightarrow> down (up x) = x"
    apply (induction x arbitrary: s)
    apply (auto elim: invar.cases)
    done

  lemma down_invar[simp]: "invar s x \<Longrightarrow> invar (sRec s) (down x)"
  proof (induction x arbitrary: s)
    case S' thus ?case by (auto intro: invar.intros)
  next  
    case (D' x y)
    from D'.prems have 1: "invarR s x" and 2: "invar (sRec s) y"
      by (auto elim: invar.cases)
    from 1 have "invarR (sRec s) (downR x)" 
      by (subst down_invarR)
    moreover note D'.IH[OF 2]
    ultimately show ?case 
      apply simp 
      apply (rule invar.intros)
      apply simp_all
      done
  qed    
  
  lemma up_invar: "invar (sRec s) x \<Longrightarrow> invar s (up x)"  
    apply (induction x arbitrary: s)
    apply (auto intro: invar.intros up_invarR elim: invar.cases)
    done

  lemma [simp]: "invar s S'" by (auto intro: invar.intros)
  lemma [simp]: "invar s (D' x y) \<longleftrightarrow> invarR s x \<and> invar (sRec s) y"
    by (auto intro: invar.intros elim: invar.cases)

  lemma downR_inject[simp]: "downR x = downR y \<longleftrightarrow> x=y"  
    apply auto
    apply (induction x arbitrary: y rule: downR.induct)
    apply (case_tac y; auto; case_tac aa; auto)
    apply (case_tac y; auto; case_tac ya; auto)
    done

  lemma down_inject[simp]: "down x = down y \<longleftrightarrow> x=y"  
    apply auto
    apply (induction x arbitrary: y)
    apply (case_tac y; auto)
    apply (case_tac y; auto)
    done



  (* Type definition *)  
  typedef 'a T = "Collect (invar sBase) :: 'a T' set"
    by (auto intro: exI[where x=S'])
  print_theorems  
  
  lemmas [simp] = Rep_T[simplified]

  definition S :: "'a T" where "S \<equiv> Abs_T S'"
  definition D :: "'a \<Rightarrow> ('a\<times>'a) T \<Rightarrow> 'a T" where
    "D x y \<equiv> Abs_T (D' (Base x) (down (Rep_T y)))"

  lemma D_inject: "D x y = D x' y' \<longleftrightarrow> (x=x' \<and> y=y')"  
    unfolding D_def
    apply auto
    apply (subst (asm) Abs_T_inject)
    apply (auto)
    apply (subst (asm) Abs_T_inject)
    apply (auto simp: Rep_T_inject)
    done

  lemma S_D_neq: "S\<noteq>D x y"
    unfolding S_def D_def
    apply auto
    apply (subst (asm) Abs_T_inject)
    apply auto
    done

  lemma T_exhaust: "a=S \<or> (\<exists>x y. a=D x y)"
    unfolding S_def D_def
    apply (rule Abs_T_cases[of a])
    apply (case_tac y)
    apply auto
    apply (rename_tac y x)
    apply (drule_tac x=x in spec)
    apply (drule_tac x="Abs_T (up y)" in spec)
    apply (subst (asm) Abs_T_inverse)
    apply (auto intro: up_invar)
    done
    
  free_constructors case_T for S | D
    apply -
    using T_exhaust apply blast
    apply (rule D_inject)
    apply (rule S_D_neq)
    done


  (* TODO: It's not completely clear to me how this looks in general *)  
  primrec rec_map_prod :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a R \<Rightarrow> 'b R)" where
    "rec_map_prod f (Base x) = Base (f x)"
  | "rec_map_prod f (Rec x) =Rec (map_prod (rec_map_prod f) (rec_map_prod f) x)"  

  fun map_T' :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a T' \<Rightarrow> 'b T')" where
    "map_T' f S' = S'"
  | "map_T' f (D' x y) = D' (rec_map_prod f x) (map_T' f y)"  

  lemma invarR_rmp[simp]: "invarR s x \<Longrightarrow> invarR s (rec_map_prod f x)"
    by (induction x rule: invarR.induct) auto

  lemma invar_mapT[simp]: "invar s t \<Longrightarrow> invar s (map_T' f t)"
    by (induction t arbitrary: s) auto


  definition map_T :: "('a \<Rightarrow> 'b) \<Rightarrow> ('a T \<Rightarrow> 'b T)" where
    "map_T f x \<equiv> Abs_T (map_T' f (Rep_T x))"

  lemma rmp_down[simp]: "rec_map_prod f (downR x) = downR (rec_map_prod (map_prod f f) x)"
    apply (induction x rule: downR.induct)
    apply auto
    done

  lemma map_T_down[simp]: "map_T' f (down x) = down (map_T' (map_prod f f) x)"
    apply (induction x)
    apply auto
    done


  lemma map_T_S[simp]: "map_T f S = S"
    unfolding map_T_def S_def
    by (auto simp: Abs_T_inverse)

  lemma map_T_D[simp]: "map_T f (D x y) = D (f x) (map_T (map_prod f f) y)"  
    unfolding map_T_def D_def
    apply (rule arg_cong[where f=Abs_T])
    apply (auto simp: Abs_T_inverse)
    done

    

end
