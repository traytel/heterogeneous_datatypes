theory NUDT_Example3
imports "../BNF_Nonuniform_Fixpoint"
begin

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"
lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

datatype (dead 'c, 'a, 'b) F1A = C "'c \<Rightarrow> 'a + 'a"
datatype (dead 'c, 'a, 'b) F1B = C "'b * 'a"
datatype (dead 'c, 'a, 'b) F2A = C "'a list"
datatype (dead 'c, 'a, 'b) F2B = C "'c \<Rightarrow> ('b list)"
datatype (dead 'c, 'a, 'b) F3A = C "'a * ('c \<Rightarrow> 'b)"
datatype (dead 'c, 'a, 'b) F3B = C 'b

datatype (dead 'c, 'a, 'b, 'x, 'y, 'z) G1 = C "('c \<Rightarrow> 'b) + 'x * 'a * 'z"
datatype (dead 'c, 'a, 'b, 'x, 'y, 'z) G2 = C "('c \<Rightarrow> 'a) * 'a + ('c \<Rightarrow> 'y)"



local_setup \<open>
fn lthy =>
  let
    open BNF_Def
    open BNF_Util
    open BNF_FP_Def_Sugar
    open BNF_NU_FP


    val ([a, b, c, x, y, z], _) = mk_TFrees 6 lthy;
    
    val F_namess = [[@{type_name F1A}, @{type_name F1B}],
      [@{type_name F2A}, @{type_name F2B}],
      [@{type_name F3A}, @{type_name F3B}]];
    val F_sugarss = map (map (the o fp_sugar_of lthy)) F_namess;
    val [[F1A, F1B], [F2A, F2B], [F3A, F3B]] = map (map #fp_bnf) F_sugarss;
    val G_names = [@{type_name G1}, @{type_name G2}];
    val G_sugars = map (the o fp_sugar_of lthy) G_names;
    val [G1, G2] = map #fp_bnf G_sugars;

    val specs_G1 = [[([c], F1A), ([c], F2A)], [([c], F1B), ([c], F2B)]];
    val specs_G2 = [[([c], F3A)], [([c], F3B)]];
    val specs = [(([c], G1), specs_G1), (([c], G2), specs_G2)]

    val mixfix = [NoSyn, NoSyn];
    val names = ["T", "S"];
    val bs = map Binding.name names;
    val map_bs = replicate 2 Binding.empty;
    val rel_bs = replicate 2 Binding.empty;
    val pred_bs = replicate 2 Binding.empty;
    val set_bss = replicate 2 (NONE :: replicate 2 (SOME Binding.empty));
    val resBs = map dest_TFree [c, a, b];

    val fp = if false then Greatest_FP else Least_FP;

    val (nu_fp_res, lthy) = construct_nu_fp fp mixfix map_bs rel_bs pred_bs set_bss bs resBs [c] specs [] lthy;


    val lthy = register_nu_fp_result_raw "T" nu_fp_res lthy;

  in lthy end
\<close>

declare [[ML_print_depth = 0]]
ML \<open>
  val nu_fp_res = BNF_NU_FP.nu_fp_result_of @{context} "T" |> the
\<close>
declare [[ML_print_depth = 10000]]


definition P :: "('c, 'a, 'b) T \<Rightarrow> bool" where
  "P \<equiv> \<lambda>t. case T_dtr1 t of G1.C g \<Rightarrow> (case g of Inl f \<Rightarrow> False | Inr (t1, a, t2) \<Rightarrow> True)"

definition Q :: "('c, 'a, 'b) S \<Rightarrow> bool" where
  "Q \<equiv> \<lambda>t. case T_dtr2 t of G2.C g \<Rightarrow> (case g of Inl (f, a) \<Rightarrow> \<exists>c. f c = a | Inr f \<Rightarrow> True)"

axiomatization where
P_ind: "\<And>t. (\<And> t'. t' \<in> set3_G1 t \<Longrightarrow> P t') \<Longrightarrow>
            (\<And> t'. t' \<in> set4_G1 t \<Longrightarrow> P t') \<Longrightarrow>
            (\<And> t'. t' \<in> set5_G1 t \<Longrightarrow> Q t') \<Longrightarrow> P (T_ctr1 t :: ('c, 'a, 'b) T)" and
Q_ind: "\<And>t. (\<And> t'. t' \<in> set3_G2 t \<Longrightarrow> P t') \<Longrightarrow>
            (\<And> t'. t' \<in> set4_G2 t \<Longrightarrow> P t') \<Longrightarrow>
            (\<And> t'. t' \<in> set5_G2 t \<Longrightarrow> Q t') \<Longrightarrow> Q (T_ctr2 t :: ('c, 'a, 'b) S)" and
P_param: "\<And>R S :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_T R S) (=) P P" and
Q_param: "\<And>R S :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_S R S) (=) Q Q"







local_setup \<open>fn lthy => let
  open BNF_Def
  open BNF_Util
  open BNF_NU_FP_Ind

  val AD's = (#As nu_fp_res) @ (#resDs nu_fp_res);
  val (ADs as [a, b, c], _) = mk_TFrees (length AD's) lthy

  val [T1_bnf, T2_bnf] = #bnfs (#fp_result nu_fp_res);
  val P = Const (@{const_name P}, mk_T_of_bnf [c] [a, b] T1_bnf --> HOLogic.boolT);
  val Q = Const (@{const_name Q}, mk_T_of_bnf [c] [a, b] T2_bnf --> HOLogic.boolT);
  val P_thms = (@{thm P_ind}, @{thm P_param});
  val Q_thms = (@{thm Q_ind}, @{thm Q_param});

  val (_, lthy) = construct_nu_ind nu_fp_res [c] [(P, P_thms), (Q, Q_thms)] lthy;
  
in lthy end\<close>















