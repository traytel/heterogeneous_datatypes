theory Wits
imports Main
begin

ML \<open>
local
open Ctr_Sugar_Util
in
fun flip f a b = f b a;

fun intersect a b = duplicates op= (a @ b)
fun union_all [] = [] | union_all xss = fold (union op=) (tl xss) (hd xss);

fun combinations [] = [[]]
  | combinations (xs::xss) = flat (map (fn x => map (cons x) (combinations xss)) xs)

(* removes all sets that are subsets of another one in the collection, removes all but one of equal sets *)
fun mk_subsetfree xss =
  let
    val xss = distinct (eq_set op=) xss;
    val drops = map_index (fn (i, xs) => exists (flip (curry (subset op=)) xs) (nth_drop i xss)) xss;
  in filter_out fst (drops ~~ xss) |> map snd end;

fun mk_Shape_wits F_witsss depth =
  let

    fun all_best_wits_of_Shape dones i = if member ((=)) dones i then [[i]] else
      let
        val F_witss = map (flip nth i) F_witsss; (* wits of relevant Fs for a shape *)
        val F_wits = flat F_witss; (* doesn't matter which F a witnes belongs to *)
        val Shape_witsss = map (map (all_best_wits_of_Shape (i :: dones))) F_wits; (* a list of possible choices to construct new wits, for each wit_F *)
        val F_S_witss = (flat o map (map union_all o combinations)) Shape_witsss; (* combine the shape's witnesses to get witnesses for F *)
        val Clean_wits = mk_subsetfree ([i] :: F_S_witss); (* only take best wits *)
      in Clean_wits end

    fun wits_of_Shape_with_invar depth i = if depth = [] then [[i]] else
      let
        val F_witss = map (flip nth i) F_witsss; (* wits of relevant Fs for a shape i *)
        val F_wits = nth F_witss (hd depth); (* recurse into the correct node *)
        val Shape_witsss = map (map (wits_of_Shape_with_invar (tl depth))) F_wits; (* a list of possible choices to construct new wits, for each wit_F *)
        val F_S_witss = (flat o map (map union_all o combinations)) Shape_witsss; (* combine the shape's witnesses to get witnesses for F *)
        val Clean_wits = mk_subsetfree F_S_witss; (* only take best wits *)
      in Clean_wits end;
  in
    (* map_range (all_best_wits_of_Shape []) (length (hd F_witsss)) *)
    map_range (wits_of_Shape_with_invar depth) (length (hd F_witsss))
  end;

fun mk_raw_wits F_witsss G_witss occs =
  let
    val nr_param = length (hd F_witsss);

    fun wits_of_raw_with_invar dones depth i =
      let
        val G_wits = nth G_witss i; (* the relevant witnesses *)
        val dones = i :: dones; (* don't recurse into this raw again *)
        val G_wits = filter_out (exists (fn j =>
          j >= nr_param andalso member op= dones (nth occs (j - nr_param)))) G_wits; (* No recursion into already handled raws *)
        val Shape_witss = mk_Shape_wits F_witsss depth; (* prepare the wits for the Shapes of right depth*)
        val raw_witsss = map (map (fn j =>
          if j < nr_param then
            nth Shape_witss j
          else
            wits_of_raw_with_invar dones ((j - nr_param) :: depth) (nth occs (j - nr_param)))) G_wits;
        val G_raw_witss = (flat o map (map union_all o combinations)) raw_witsss; (* combine the shape's witnesses to get witnesses for F *)
        val Clean_wits = mk_subsetfree G_raw_witss; (* only take best wits *)
      in Clean_wits end;
  in
    map_range (wits_of_raw_with_invar [] []) (length G_witss)
  end;

  val F_witsss =
    let
      val F1A_wits = [[0]];
      val F1B_wits = [[0, 1], [0, 3]];
      val F1C_wits = [[2, 3], [0]];
      val F1D_wits = [[]];
      val F2A_wits = [[2, 1], [2, 3]];
      val F2B_wits = [[1, 2, 3]];
      val F2C_wits = [[1, 3], [1, 2]];
      val F2D_wits = [[0, 3]];
    in
      [[F1A_wits, F1B_wits, F1C_wits, F1D_wits],
       [F2A_wits, F2B_wits, F2C_wits, F2D_wits]]
    end

  val Shape_wits = mk_Shape_wits F_witsss [1, 0, 1];

  val G_witss =
    let
      val G1_wits = [[0, 1, 2, 3], [5]];
      val G2_wits = [[0, 1, 4], [2, 3]];
    in [G1_wits, G2_wits] end

  val Raw_wits = mk_raw_wits F_witsss G_witss [0, 1];
end
\<close>

end