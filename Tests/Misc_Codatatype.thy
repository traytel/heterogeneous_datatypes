(* Based on

    Title:      HOL/Datatype_Examples/Misc_Codatatype.thy
    Author:     Dmitriy Traytel, TU Muenchen
    Author:     Andrei Popescu, TU Muenchen
    Author:     Jasmin Blanchette, TU Muenchen
    Copyright   2012, 2013

Miscellaneous nonuniform codatatype definitions.
*)

section \<open>Miscellaneous Nonuniform Codatatype Definitions\<close>

theory Misc_Codatatype
imports "../BNF_Nonuniform_Fixpoint" "HOL-Library.FSet"
begin

nonuniform_codatatype 'a stream = Stream (shd: 'a) (stl: "'a stream")

nonuniform_codatatype 'a mylist = MyNil | MyCons (myhd: 'a) (mytl: "'a mylist")

nonuniform_codatatype ('b, 'c :: ord, 'd, 'e) some_passive =
  SP1 "('b, 'c, 'd, 'e) some_passive" | SP2 'b | SP3 'c | SP4 'd | SP5 'e

nonuniform_codatatype 'a multi_live_direct1 = MultiLiveDirect1 'a
nonuniform_codatatype 'a multi_live_direct2 = MultiLiveDirect2 'a 'a
nonuniform_codatatype 'a multi_live_direct3 = MultiLiveDirect3 'a 'a 'a
nonuniform_codatatype 'a multi_live_direct4 = MultiLiveDirect4 'a 'a 'a 'a
nonuniform_codatatype 'a multi_live_direct5 = MultiLiveDirect5 'a 'a 'a 'a 'a
nonuniform_codatatype 'a multi_live_direct6 = MultiLiveDirect6 'a 'a 'a 'a 'a 'a
nonuniform_codatatype 'a multi_live_direct7 = MultiLiveDirect7 'a 'a 'a 'a 'a 'a 'a
nonuniform_codatatype 'a multi_live_direct8 = MultiLiveDirect8 'a 'a 'a 'a 'a 'a 'a 'a
nonuniform_codatatype 'a multi_live_direct9 = MultiLiveDirect9 'a 'a 'a 'a 'a 'a 'a 'a 'a

datatype 'a live_and_fun = LiveAndFun nat "nat \<Rightarrow> 'a"

nonuniform_codatatype 'a par_lambda =
  PVar 'a |
  PApp "'a par_lambda" "'a par_lambda" |
  PAbs 'a "'a par_lambda" |
  PLet "('a \<times> 'a par_lambda) fset" "'a par_lambda"

nonuniform_codatatype 'a p = P "'a + 'a p"

nonuniform_codatatype 'a J1 = J11 'a "'a J1" | J12 'a "'a J2"
and 'a J2 = J21 | J22 "'a J1" "'a J2"

nonuniform_codatatype 'a tree = TEmpty | TNode 'a "'a forest"
and 'a forest = FNil | FCons "'a tree" "'a forest"

nonuniform_codatatype 'a tree' = TEmpty' | TNode' "'a branch" "'a branch"
and 'a branch = Branch 'a "'a tree'"

nonuniform_codatatype 'a bin_rose_tree = BRTree 'a "'a bin_rose_tree mylist" "'a bin_rose_tree mylist"

nonuniform_codatatype ('a, 'b) exp = Term "('a, 'b) trm" | Sum "('a, 'b) trm" "('a, 'b) exp"
and ('a, 'b) trm = Factor "('a, 'b) factor" | Prod "('a, 'b) factor" "('a, 'b) trm"
and ('a, 'b) factor = C 'a | V 'b | Paren "('a, 'b) exp"

nonuniform_codatatype (dead 'a, dead 'b, 'c) some_killing =
  SK "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b, 'c) some_killing + ('a, 'b, 'c) in_here"
and ('a, 'b, 'c) in_here =
  IH1 'b 'a | IH2 'c

nonuniform_codatatype (dead 'a, dead 'b, 'c) some_killing' =
  SK' "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b, 'c) some_killing' + ('a, 'b, 'c) in_here'"
and ('a, 'b, 'c) in_here' =
  IH1' 'b | IH2' 'c

nonuniform_codatatype (dead 'a, 'b, 'c) some_killing'' =
  SK'' "'a \<Rightarrow> ('a, 'b, 'c) in_here''"
and ('a, 'b, 'c) in_here'' =
  IH1'' 'b 'a | IH2'' 'c

nonuniform_codatatype (dead 'b, 'c) less_killing = LK "'b \<Rightarrow> 'c"

nonuniform_codatatype ('a, 'b, 'c) wit3_F1 = W1 'a "('a, 'b, 'c) wit3_F1" "('a, 'b, 'c) wit3_F2"
and ('a, 'b, 'c) wit3_F2 = W2 'b "('a, 'b, 'c) wit3_F2"
and ('a, 'b, 'c) wit3_F3 = W31 'a 'b "('a, 'b, 'c) wit3_F1" | W32 'c 'a 'b "('a, 'b, 'c) wit3_F1"

nonuniform_codatatype ('c, 'e, 'g) coind_wit1 =
       CW1 'c "('c, 'e, 'g) coind_wit1" "('c, 'e, 'g) ind_wit" "('c, 'e, 'g) coind_wit2"
and ('c, 'e, 'g) coind_wit2 =
       CW21 "('c, 'e, 'g) coind_wit2" 'e | CW22 'c 'g
and ('c, 'e, 'g) ind_wit =
       IW1 | IW2 'c

nonuniform_codatatype ('b, dead 'a) bar = BAR "'a \<Rightarrow> 'b"

nonuniform_codatatype ('a, dead 'b, 'c, dead 'd) foo = FOO "'d + 'b \<Rightarrow> 'c + 'a"

nonuniform_codatatype 'a phantom = A
nonuniform_codatatype 'a use_phantom = Y 'a "'a use_phantom phantom"

end
