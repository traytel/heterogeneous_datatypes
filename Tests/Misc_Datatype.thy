(* Based on

    Title:      HOL/Datatype_Examples/Misc_Datatype.thy
    Author:     Dmitriy Traytel, TU Muenchen
    Author:     Andrei Popescu, TU Muenchen
    Author:     Jasmin Blanchette, TU Muenchen
    Copyright   2012, 2013

Miscellaneous nonuniform datatype definitions.
*)

section \<open>Miscellaneous Nonuniform Datatype Definitions\<close>

theory Misc_Datatype
imports "../BNF_Nonuniform_Fixpoint" "HOL-Library.Countable" "HOL-Library.FSet"
begin
  
nonuniform_datatype (discs_sels) 'a mylist = MyNil | MyCons (myhd: 'a) (mytl: "'a mylist")

nonuniform_datatype (discs_sels) ('b, 'c :: ord, 'd, 'e) some_passive =
  SP1 "('b, 'c, 'd, 'e) some_passive" | SP2 'b | SP3 'c | SP4 'd | SP5 'e

nonuniform_datatype (discs_sels) 'a multi_live_direct1 = MultiLiveDirect1 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct2 = MultiLiveDirect2 'a 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct3 = MultiLiveDirect3 'a 'a 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct4 = MultiLiveDirect4 'a 'a 'a 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct5 = MultiLiveDirect5 'a 'a 'a 'a 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct6 = MultiLiveDirect6 'a 'a 'a 'a 'a 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct7 = MultiLiveDirect7 'a 'a 'a 'a 'a 'a 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct8 = MultiLiveDirect8 'a 'a 'a 'a 'a 'a 'a 'a
nonuniform_datatype (discs_sels) 'a multi_live_direct9 = MultiLiveDirect9 'a 'a 'a 'a 'a 'a 'a 'a 'a

nonuniform_datatype (dead 'a, 'b) ite = ITE "'a \<Rightarrow> bool" "'a \<Rightarrow> 'b" "'a \<Rightarrow> 'b"

nonuniform_datatype 'a live_and_fun = LiveAndFun nat "nat \<Rightarrow> 'a"

nonuniform_datatype (discs_sels) 'a par_lambda =
  PVar 'a |
  PApp "'a par_lambda" "'a par_lambda" |
  PAbs 'a "'a par_lambda" |
  PLet "('a \<times> 'a par_lambda) fset" "'a par_lambda"

locale loc =
  fixes c :: 'a and d :: 'a
  assumes "c \<noteq> d"
begin

nonuniform_datatype (discs_sels) 'b I1 = I11 'b "'b I1" | I12 'b "'b I2"
and 'b I2 = I21 | I22 "'b I1" "'b I2"

nonuniform_datatype (discs_sels) 'b tree = TEmpty | TNode 'b "'b forest"
and 'b forest = FNil | FCons "'b tree" "'b forest"

end

nonuniform_datatype (discs_sels) 'a tree' = TEmpty' | TNode' "'a branch" "'a branch"
and 'a branch = Branch 'a "'a tree'"

nonuniform_datatype (discs_sels) 'a bin_rose_tree = BRTree 'a "'a bin_rose_tree mylist" "'a bin_rose_tree mylist"

nonuniform_datatype (discs_sels) ('a, 'b) exp = Term "('a, 'b) trm" | Sum "('a, 'b) trm" "('a, 'b) exp"
and ('a, 'b) trm = Factor "('a, 'b) factor" | Prod "('a, 'b) factor" "('a, 'b) trm"
and ('a, 'b) factor = C 'a | V 'b | Paren "('a, 'b) exp"

nonuniform_datatype (discs_sels) (dead 'a, dead 'b, 'c) some_killing =
  SK "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b, 'c) some_killing + ('a, 'b, 'c) in_here"
and ('a, 'b, 'c) in_here =
  IH1 'b 'a | IH2 'c

nonuniform_datatype (discs_sels) 'b nofail1 = NF11 "'b nofail1" 'b | NF12 'b
nonuniform_datatype (discs_sels) 'b nofail2 = NF2 "('b nofail2 \<times> 'b \<times> 'b nofail2 \<times> 'b) list"
nonuniform_datatype (discs_sels) 'b nofail3 = NF3 'b "('b nofail3 \<times> 'b \<times> 'b nofail3 \<times> 'b) fset"
nonuniform_datatype (discs_sels) 'b nofail4 = NF4 "('b nofail4 \<times> ('b nofail4 \<times> 'b \<times> 'b nofail4 \<times> 'b) fset) list"

nonuniform_datatype (discs_sels) ('a, dead 'b) bar = Bar "'b \<Rightarrow> 'a"

nonuniform_datatype (discs_sels) ('a, dead 'b, 'c, dead 'd) foo = Foo "'d + 'b \<Rightarrow> 'c + 'a"

nonuniform_datatype (discs_sels) 'a phantom = A
nonuniform_datatype (discs_sels) 'a use_phantom = Y 'a "'a use_phantom phantom"

nonuniform_datatype (dead 't, 'id) dead_sum_fun = Dead_sum_fun "('t list \<Rightarrow> 't) + 't" | Bar (bar: 'id)

end
