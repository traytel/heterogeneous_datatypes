theory NUDT_Example1
imports "../BNF_Nonuniform_Fixpoint"
begin

datatype 'a F = C "'a * 'a"

datatype ('a,'x) G = C "unit + 'a * 'x"

declare [[ML_exception_trace]]
(*ML \<open>parallel_proofs := 0\<close>*)



















(*
local_setup \<open>
fn lthy =>
  let
    open BNF_Def
    open BNF_Util
    open BNF_FP_Def_Sugar
    open BNF_NU_FP
    open BNF_NU_FP_Def_Sugar

    val ([a, x], _) = mk_TFrees 2 lthy;
    
    val F_namess = [[@{type_name F}]];
    val F_sugarss = map (map (the o fp_sugar_of lthy)) F_namess;
    val [[F]] = map (map #fp_bnf) F_sugarss;
    val G_names = [@{type_name G}];
    val G_sugars = map (the o fp_sugar_of lthy) G_names;
    val [G] = map #fp_bnf G_sugars;

    val specs_G = [[([], F)]];
    val specs = [(([], G), specs_G)]

    val mixfix = [NoSyn];
    val names = ["plist"];
    val bs = map Binding.name names;
    val map_bs = replicate 1 Binding.empty;
    val rel_bs = replicate 1 Binding.empty;
    val pred_bs = replicate 1 Binding.empty;
    val set_bss = replicate 1 (replicate 1 (SOME Binding.empty));
    val resBs = map dest_TFree [a];
    
    val fp = if true then Greatest_FP else Least_FP;

    val f = nonuniform_co_datatypes fp (construct_nu_fp fp)

    val (nu_fp_res, lthy) = construct_nu_fp fp mixfix map_bs rel_bs pred_bs set_bss bs resBs [] specs [] lthy;

    val _ = register_nu_fp_sugars (K true)
fun a n = let
in 0 end
  in lthy end
\<close>
*)
nonuniform_datatype 'a plist = C "('a, 'a F plist) G"

declare [[ML_print_depth = 3]]
ML \<open>
val a = BNF_NU_FP_Def_Sugar.nu_fp_sugars_of @{context} |> @{print}

val nu_fp_res = BNF_NU_FP_Def_Sugar.nu_fp_sugar_of @{context} @{type_name plist} |> the |> #nu_fp_res\<close>
declare [[ML_print_depth = 10000]]


consts P :: "('a) plist \<Rightarrow> bool" (*where
  "P \<equiv> \<lambda>t. case T_dtr1 t of G1.C g \<Rightarrow> (case g of Inl f \<Rightarrow> False | Inr (t1, a, t2) \<Rightarrow> True)"*)

axiomatization where
P_ind: "\<And>t. (\<And> t'. t' \<in> set2_G t \<Longrightarrow> P t') \<Longrightarrow>
        P (T_ctr t :: ('a) plist)" and
P_param: "\<And>R :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> rel_fun (rel_pList R) (=) P P"


local_setup \<open>fn lthy => let
  open BNF_Def
  open BNF_Util
  open BNF_NU_FP_Ind

  val AD's = (#As nu_fp_res) @ (#resDs nu_fp_res);
  val (ADs as [a], _) = mk_TFrees (length AD's) lthy;

  val [T1_bnf] = #bnfs (#fp_res nu_fp_res);
  val P = Const (@{const_name P}, mk_T_of_bnf [] [a] T1_bnf --> HOLogic.boolT);
  val P_thms = (@{thm P_ind}, @{thm P_param});

  val (_, lthy) = construct_nu_ind nu_fp_res [] [(P, P_thms)] lthy;
  
in lthy end\<close>















