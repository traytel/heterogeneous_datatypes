theory Bushy
imports "HOL-Library.BNF_Axiomatization"
begin

declare [[typedef_overloaded, bnf_internals]]

(* datatype 'a T = ('a, 'a H T G T) F*)

bnf_axiomatization ('a, 'b) F [wits: "('a, 'b) F"]
bnf_axiomatization 'a G
bnf_axiomatization 'a H

datatype shadow = S | G | H

datatype 'a outer = Outer "('a, 'a outer) F"

datatype 'a elem = Leaf 'a | Self "'a elem outer" | NodeG "'a elem G" | NodeH "'a elem H"

type_synonym 'a raw = "'a elem outer"

inductive ok_raw and ok_elem where
  "pred_F (ok_elem u) (ok_raw (G # S # H # u)) f \<Longrightarrow> ok_raw u (Outer f)"
| "ok_elem [] (Leaf x)"
| "pred_G (ok_elem u) g \<Longrightarrow> ok_elem (G # u) (NodeG g)"
| "pred_H (ok_elem u) h \<Longrightarrow> ok_elem (H # u) (NodeH h)"
| "ok_raw u t \<Longrightarrow> ok_elem (S # u) (Self t)"

primrec flat_outer where
  "flat_outer flat (Outer f) = Outer (map_F flat (flat_outer flat) f)"

primrec flat_elemH where
  "flat_elemH (Leaf x) = NodeH (map_H Leaf x)"
| "flat_elemH (NodeG g) = NodeG (map_G flat_elemH g)"
| "flat_elemH (NodeH h) = NodeH (map_H flat_elemH h)"
| "flat_elemH (Self t) = Self (flat_outer id (map_outer flat_elemH t))"

primrec flat_elemG where
  "flat_elemG (Leaf x) = NodeG (map_G Leaf x)"
| "flat_elemG (NodeG g) = NodeG (map_G flat_elemG g)"
| "flat_elemG (NodeH h) = NodeH (map_H flat_elemG h)"
| "flat_elemG (Self t) = Self (flat_outer id (map_outer flat_elemG t))"

primrec flat_elemS where
  "flat_elemS (Leaf x) = Self x"
| "flat_elemS (NodeG g) = NodeG (map_G flat_elemS g)"
| "flat_elemS (NodeH h) = NodeH (map_H flat_elemS h)"
| "flat_elemS (Self t) = Self (flat_outer id (map_outer flat_elemS t))"

abbreviation "flat_rawH \<equiv> flat_outer flat_elemH"
abbreviation "flat_rawG \<equiv> flat_outer flat_elemG"
abbreviation "flat_rawS \<equiv> flat_outer flat_elemS"

typedef 'a T = "{xs :: 'a raw. ok_raw [] xs}"
  by (auto simp: pred_F_def dest: F.wit
    intro!: ok_raw_ok_elem.intros exI[of _ "Outer wit_F"])

setup_lifting type_definition_T

lift_definition Cons :: "('a, 'a H T G T) F \<Rightarrow> 'a T" is
  "\<lambda>f :: ('a, 'a H raw G raw) F. Outer (map_F Leaf (flat_rawH o flat_rawS o flat_rawG) f)"
  apply (auto simp: F.pred_map intro!: ok_raw_ok_elem.intros)
  oops

end