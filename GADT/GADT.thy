theory GADT
  imports Main
begin

declare [[typedef_overloaded, bnf_internals]]

(*
term "C :: int \<Rightarrow> int Exp"
term "B :: bool \<Rightarrow> bool Exp"
term "U :: 'a \<Rightarrow> 'a Exp"
term "Add :: int Exp \<Rightarrow> int Exp \<Rightarrow> int Exp"
term "If :: bool Exp \<Rightarrow> 'a Exp \<Rightarrow> 'a Exp \<Rightarrow> 'a Exp"
term "Eq :: 'a Exp \<Rightarrow> 'a Exp \<Rightarrow> bool Exp"
*)
(*
datatype 'a exp = Add "'a exp" "'a exp" | C int
  | If "'a exp" "'a exp" "'a exp" | U 'a
  | Eq "'a exp" "'a exp" | B bool

datatype foo = Foo

consts type :: "'a \<Rightarrow> string"
overloading type_int \<equiv> "type :: int \<Rightarrow> string" begin
definition type_int :: "int \<Rightarrow> string" where "type_int i = ''int''"
end
overloading type_bool \<equiv> "type :: bool \<Rightarrow> string" begin
definition type_bool :: "bool \<Rightarrow> string" where "type_bool i = ''bool''"
end
(*
overloading type_exp \<equiv> "type :: 'a exp \<Rightarrow> string" begin
definition type_exp :: "'a exp \<Rightarrow> string" where "type_exp i = ''('' @ type @ '')exp''"
end
*)

inductive int_exp and bool_exp and a_exp where
  iC: "int_exp (C i)"
| iA: "int_exp a \<Longrightarrow> int_exp b \<Longrightarrow> int_exp (Add a b)"
| iU: "type(a) = ''int'' \<Longrightarrow> int_exp (U a)"
| iI: "bool_exp c \<Longrightarrow> int_exp t \<Longrightarrow> int_exp e \<Longrightarrow> int_exp (If c t e)"
| bB: "bool_exp (B b)"
| bU: "type(a) = ''bool'' \<Longrightarrow> bool_exp (U a)"
| bE: "type(a) = type(b) \<Longrightarrow> a_exp a \<Longrightarrow> a_exp b \<Longrightarrow> bool_exp (Eq a b)"
| bI: "bool_exp c \<Longrightarrow> bool_exp t \<Longrightarrow> bool_exp e \<Longrightarrow> bool_exp (If c t e)"
| aU: "a_exp (U a)"
| aI: "type(t) = type(e) \<Longrightarrow> bool_exp c \<Longrightarrow> a_exp t \<Longrightarrow> a_exp e \<Longrightarrow> a_exp (If c t e)"
| ia: "int_exp x \<Longrightarrow> a_exp x"
| ba: "bool_exp x \<Longrightarrow> a_exp x"

typedef 'a iExp = "{x :: 'a exp. a_exp x}"
  by (metis CollectI a_exp.simps)

typedef 'a Exp = "{x :: 'a exp. a_exp x}"
  by (metis CollectI a_exp.simps)

setup_lifting type_definition_Exp
*)
(*
lift_definition Add :: "int Exp \<Rightarrow> int Exp \<Rightarrow> int Exp" is exp.Add
  subgoal for a b
  apply (drule ai)
    subgoal by (rule type_int_def) thm type_int_def

datatype 'a iexp = Add "'a iexp" "'a iexp" | C int
     and 'a aexp = If "'a bexp" "'a aexp + 'a bexp + 'a iexp" "'a aexp + 'a bexp + 'a iexp" | U 'a
     and 'a bexp = Eq "'a aexp + 'a bexp + 'a iexp" "'a aexp + 'a bexp + 'a iexp" | B bool
*)

datatype 'a iexp = Add "'a iexp" "'a iexp" | C int | U int | If "'a bexp" "'a iexp" "'a iexp"
     and 'a aexp = If "'a bexp" "'a aexp" "'a aexp" | U 'a
                 | Add "'a iexp" "'a iexp" | C int
                 | Eq "'a aexp" "'a aexp" | B bool
     and 'a bexp = Eq "'a aexp" "'a aexp" | B bool | U bool | If "'a bexp" "'a bexp" "'a bexp"

fun embed_ia :: "int iexp \<Rightarrow> int aexp"  where
  "embed_ia (iexp.Add a b) = aexp.Add a b"
| "embed_ia (iexp.C i) = aexp.C i"
| "embed_ia (iexp.U i) = aexp.U i"
| "embed_ia (iexp.If c t e) = aexp.If c (embed_ia t) (embed_ia e)"

fun embed_ba :: "bool bexp \<Rightarrow> bool aexp" where
  "embed_ba (bexp.Eq a b) = aexp.Eq a b"
| "embed_ba (bexp.B c) = aexp.B c"
| "embed_ba (bexp.U i) = aexp.U i"
| "embed_ba (bexp.If c t e) = aexp.If c (embed_ba t) (embed_ba e)"

quotient_type 'a Exp = "'a iexp + ('a aexp + 'a bexp)" / "\<lambda>x y. case (x, y) of
    (Inl x, Inr (Inl y)) \<Rightarrow> embed_ia x = y
  | (Inr (Inl y), Inr (Inr z)) \<Rightarrow> embed_ba z = y"

typedecl ('b, 'a) Exp (* (int, 'a) Exp := 'a iexp, (bool, 'a) Exp := 'a bexp, ('a, 'a) Exp := 'a aexp *)

end