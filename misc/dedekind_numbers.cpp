#include <iostream>
#include <set>
using namespace std;

// calculation of https://oeis.org/A014466

template <typename T>
ostream& operator<<(ostream& os, const set<T> s) {
	os << "{";
	for (typename set<T>::iterator i = s.begin(); i != s.end(); i++) {
		if (i != s.begin())
			os << ", ";
		os << *i;
	}
    return os << "}";
}

template <typename T>
bool subset(set<T> sub, set<T> super) {
	typename set<T>::iterator i = sub.begin(), j = super.begin();
	while (j != super.end()) {
		if (*i == *j)
			i++;
		j++;
		if (i == sub.end())
			return true;
	}
	return false;
}

template <typename T>
set<set<T>> minimize_wits(set<set<T>> witss) {
	set<set<T>> min;
	for (typename set<set<T>>::iterator i = witss.begin(); i != witss.end(); i++) {
		bool ok = true;
		for (typename set<set<T>>::iterator j = witss.begin(); j != witss.end(); j++) {
			if (i != j && subset(*j, *i)) {
				ok = false;
				break;
			}
		}
		if (ok)
			min.insert(*i);
	}
	return min;
}

template <typename T>
set<set<T>> powerset(set<T> s) {
	set<set<T>> pset;
	pset.insert(set<T>());
	for (typename set<T>::iterator i = s.begin(); i != s.end(); i++) {
		set<set<T>> newset;
		for (typename set<set<T>>::iterator j = pset.begin(); j != pset.end(); j++) {
			set<T> temp(*j);
			temp.insert(*i);
			newset.insert(temp);
		}
		pset.insert(newset.begin(), newset.end());
	}
	return pset;
}


int main() {
	set<int> s;
	int n = 5;
	
	for (int i = 1; i <= n; i++) {
		set<set<set<int>>> sss = powerset(powerset(s)), result;
		sss.erase(sss.begin());
		
		for (typename set<set<set<int>>>::iterator i = sss.begin(); i != sss.end(); i++) {
			result.insert(minimize_wits(*i));
		}
		
		cout << result.size() << endl << result << endl;
		s.insert(i);
	}
	
	cout << endl << "calculation of https://oeis.org/A014466" << endl;
	cout << "1, 2, 5, 19, 167, 7580, 7828353, 2414682040997, 56130437228687557907787" << endl;
	
	return 0;
}
















