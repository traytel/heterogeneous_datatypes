theory Hall_of_Fame
imports "../BNF_Nonuniform_Fixpoint"
begin

nonuniform_datatype ('a, dead 'b) w1 = W 'a 'b

nonuniform_codatatype 'a llist = LNil | LCons 'a "'a llist"
nonuniform_codatatype 'a pllist = LNil | PLCons 'a "('a \<times> 'a) pllist"

nonuniform_datatype 'a x = E | C "'a * 'a option x"
nonuniform_datatype 'a y = E | C "'a y option"
  
nonuniform_codatatype ('a1, 'a2) T =
  C 'a1 'a2 "('a1, 'a2) T" "('a1, 'a2) T"

datatype ('a, 'b) F = Foo 'a 'b
datatype ('a, 'b, 'c, 'd) G = Goo 'a 'b 'c 'd
nonuniform_codatatype ('a1, 'a2) U =
  C "('a1, 'a2, (('a1, 'a2) F, ('a1, 'a2) F) U, (('a1, 'a2) F, ('a1, 'a2) F) U) G"


end
