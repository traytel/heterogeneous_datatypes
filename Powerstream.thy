theory Powerstream
imports "../BNF_Nonuniform_Fixpoint" "~~/src/HOL/Eisbach/Eisbach"
begin

method parametricity_prover =
  (match premises in
     bi_unique[transfer_rule]: "bi_unique P" and
     left_total[transfer_rule]: "left_total P" for P \<Rightarrow> \<open>transfer_prover\<close>)
  
nonuniform_codatatype 'a pstream = PSCons (pshd: 'a) (pstl: "('a \<times> 'a) pstream")

declare pstream.map_sel[simp] pstream.map_cong[intro!]

(*
nonuniform_primcorecursive psconst :: "'a \<Rightarrow> 'a pstream" where
  "psconst x = PSCons x (psconst (x, x))"
*)

consts psconst :: "'a \<Rightarrow> 'a pstream"
axiomatization where
  psconst_ctr: "\<And>x. psconst x = PSCons x (psconst (x, x))" and
  psconst_transfer[transfer_rule]: "\<And>R. bi_unique R \<Longrightarrow> rel_fun R (rel_pstream R) psconst psconst"
  
lemma psconst_sel[simp]:
  "pshd (psconst x) = x"
  "pstl (psconst x) = psconst (x, x)"
  by (subst psconst_ctr, auto)+
  
nonuniform_coinduct "\<lambda>l r :: 'a pstream. \<exists>x xs. l = psconst x \<and> r = map_pstream (\<lambda>_ :: 'a. x) xs" in
  "(psconst x :: 'a pstream) = map_pstream (\<lambda>_ :: 'a. x) xs"
  by parametricity_prover force+
  
  
end