(*  Title:      HOL/Tools/BNF/bnf_nu_fp_ind.ML
    Author:     Dmitriy Traytel, ETH Zuerich
    Author:     Fabian Meier, ETH Zuerich
    Copyright   2016

(Co)induction over nonuniform datatypes.
*)

signature BNF_NU_FP_IND =
sig
  type nu_fp_ind_result =
    {co_ind_thms: thm list}

  val morph_nu_fp_ind_result: morphism -> nu_fp_ind_result -> nu_fp_ind_result

  val construct_nu_co_ind: BNF_Util.fp_kind -> BNF_NU_FP_Util.nu_fp_result -> typ list ->
    term list -> thm list -> (thm list -> thm list) -> local_theory ->
    nu_fp_ind_result * local_theory
end;

structure BNF_NU_FP_Ind : BNF_NU_FP_IND =
struct

local
  open BNF_Def
  open BNF_Util
  open BNF_Tactics
  open BNF_Comp
  open BNF_FP_Util
  open BNF_FP_Def_Sugar
  open BNF_LFP
  open BNF_LFP_Util
  open BNF_LFP_Tactics
  open BNF_LFP_Rec_Sugar
  open Ctr_Sugar
  open BNF_Tactics
  open BNF_NU_FP_Util
  open BNF_NU_FP_Tactics
in

type nu_fp_ind_result =
  {
    co_ind_thms: thm list
  }

fun morph_nu_fp_ind_result phi {co_ind_thms} =
  {
    co_ind_thms = map (Morphism.thm phi) co_ind_thms
  };

val mk_const_val = Const oo pair o fst o dest_Const;
fun name_of (Const (x, _)) = x
  | name_of (Free (x, _)) = x
  | name_of _ = error "not const or free";

fun target_ctr_sugar_of_fp_sugar lthy fpT ({T, fp_ctr_sugar = {ctr_sugar, ...}, ...} : fp_sugar) =
  let
    val thy = Proof_Context.theory_of lthy;
    val rho = Vartab.fold (cons o apsnd snd) (Sign.typ_match thy (T, fpT) Vartab.empty) [];
    val phi = Morphism.term_morphism "BNF" (Term.subst_TVars rho);
  in
    morph_ctr_sugar phi ctr_sugar
  end;

fun construct_nu_co_ind fp nu_fp_res resDs Ps P_param_thms get_P_co_ind_thms0 lthy =
  let
    val arity = case_fp fp 1 2;
    val co_swap = case_fp fp I swap;
    (* Store sizes *)

    val nr_Ts = length Ps;
    val nr_param = length (#As nu_fp_res);
    val nr_occs = #nr_occs nu_fp_res
    val nr_occ = Integer.sum nr_occs;
    fun rep_occs xs = @{map 2} replicate nr_occs xs;
    fun rest_occs xs = flat (rep_occs xs);

    val _ = if length Ps = nr_Ts then 0 else
      error "Number of predicates must be same as number of Ts";

    (* Prepare *)

    val inst_deads = map (Term.typ_subst_atomic (#resDs nu_fp_res ~~ resDs));
    val Dsss = transpose (#Dsss nu_fp_res) |> map (map inst_deads);
    val Dss = #Dss nu_fp_res |> map inst_deads;

    val names = fold (union (op =)) (Dss @ flat Dsss) resDs;
    val names_lthy = fold Variable.declare_typ names lthy;

    val ((As, A's), _) = names_lthy
      |> mk_TFrees nr_param
      ||>> mk_TFrees nr_param;

    (* Extract types *)

    val Gs = #Gs nu_fp_res;
    val shape_sugars = #shape_sugars (#shape_res nu_fp_res);
    val raw_sugars = #raw_sugars nu_fp_res;
    val labelT = #labelT (#shape_res nu_fp_res);
    val invar_raw_vals = #invar_raws nu_fp_res;
    val T_bnfs = #bnfs (#fp_res nu_fp_res);
    val T_infos = #T_infos nu_fp_res;

    (* All the relevant types and none more... *)

    val depthT = HOLogic.listT labelT;
    val shape_As = map (mk_T_of_bnf resDs As o #fp_bnf) shape_sugars;
    val raw_As = map (mk_T_of_bnf resDs As o #fp_bnf) raw_sugars;
    val raw_shape_As = map (mk_T_of_bnf resDs shape_As o #fp_bnf) raw_sugars;
    val T_As = map (mk_T_of_bnf resDs As) T_bnfs;
    val T_A's = map (mk_T_of_bnf resDs A's) T_bnfs;

    val shape_ctr_sugars = @{map 2} (target_ctr_sugar_of_fp_sugar lthy) shape_As shape_sugars;
    val raw_ctr_sugars = @{map 2} (target_ctr_sugar_of_fp_sugar lthy) raw_As raw_sugars;

    (* Create Frees and Consts *)

    val ((((f_vals, t_valss), raw_shape_valss), u0), _) = lthy
      |> mk_Frees "f" (@{map 2} (curry op -->) As A's)
      ||>> mk_Freess "t" (map (replicate arity) T_As)
      ||>> mk_Freess "raw_shape" (map (replicate arity) raw_shape_As)
      ||>> yield_singleton (mk_Frees "u0") depthT;


    (* Most useful theorems *)

    val co_induct_thm = (hd raw_sugars) |> #fp_co_induct_sugar |> the |> #common_co_inducts |> hd;
    val shape_inject_thms = map #injects shape_ctr_sugars |> flat;
    val shape_case_thms = map #case_thms shape_ctr_sugars |> flat;
    val raw_inj_map_strong_thms =  map (inj_map_strong_of_bnf o #fp_bnf) raw_sugars;
    val unCons_defs = maps #sel_defs raw_ctr_sugars;
    val raw_split_thmss = map (fn sugar => [#split sugar, #split_asm sugar]) raw_ctr_sugars;
    val raw_inject_thms = map (the_single o #injects) raw_ctr_sugars;
    val G_pred_set_thms = map pred_set_of_bnf Gs;
    val G_set_map_thmss = map set_map_of_bnf Gs;
    val G_rel_map_thmss = map rel_map_of_bnf Gs;
    val G_rel_mono_strong_thms = map rel_mono_strong_of_bnf Gs;
    val invar_shape_depth_iff_thms = #invar_shape_depth_iff_thms (#shape_res nu_fp_res);
    val cimmerse_shape_inj_thms = #cimmerse_shape_inj_thms (#shape_res nu_fp_res);
    val invar_raw_simps = #invar_raw_simps nu_fp_res;
    val cimmerse_raw_simpss = #cimmerse_raw_simpss nu_fp_res;
    val invar_map_raw_closed_thms = #invar_map_raw_closed_thms nu_fp_res;
    val invar_cimmerse_raw_thms = #invar_cimmerse_raw_thms nu_fp_res;
    val invar_raw_unflat_thms = flat (#invar_raw_unflat_raw_thmss nu_fp_res);
    val T_rel_Grp_thms = map rel_Grp_of_bnf T_bnfs;
    val T_un_T_thms = #ctor_dtors (#fp_res nu_fp_res);
    val T_dtor_defs = #T_dtor_defs nu_fp_res;
    val T_Abs_cases = map (#Abs_cases o snd) T_infos;
    val T_Abs_injects = map (#Abs_inject o snd) T_infos;
    val T_Abs_inverses = map (#Abs_inverse o snd) T_infos;
    val T_Abs_names = map (#Abs_name o fst) T_infos;
    val T_Rep_names = map (#Rep_name o fst) T_infos;
    val T_Rep_inverses = map (#Rep_inverse o snd) T_infos;
    val T_Rep_thms = map (#Rep o snd) T_infos;
    val T_map_defs = map map_def_of_bnf T_bnfs;

    val Leaves = @{map 2} (mk_const_val o hd o #ctrs) shape_ctr_sugars
      (@{map 2} (curry op -->) As shape_As);

    val Nodess =
      @{map 2} (map (Syntax.check_term lthy) oo @{map 2} mk_const_val o tl o #ctrs)
        shape_ctr_sugars (map (fn U => replicate nr_occ (dummyT --> U)) shape_As);

    (* P_mono *)

    val P_Ts = map (fn T => replicate arity T ---> HOLogic.boolT) T_As;
    val P_vals = @{map 2} mk_const_val Ps P_Ts;
    val P'_Ts = map (fn T => replicate arity T ---> HOLogic.boolT) T_A's;
    val P'_vals = @{map 2} mk_const_val Ps P'_Ts;
    val mapTs = map (mk_map_of_bnf resDs As A's) T_bnfs;
    val mapT_shapes = map (mk_map_of_bnf resDs As shape_As) T_bnfs;
    val grp_Ts = @{map 2} (fn A => fn A' =>
      HOLogic.mk_setT A --> (A --> A') --> A --> A' --> HOLogic.boolT) As A's;
    val grp_vals = map (fn t => Const (@{const_name BNF_Def.Grp}, t)) grp_Ts;

    fun mk_P_mono_cterm grp f =
      Thm.cterm_of lthy (grp $ HOLogic.mk_UNIV (domain_type (fastype_of f)) $ f);
    val P_mono_ctermss = map (fn _ => @{map 2} mk_P_mono_cterm grp_vals f_vals) t_valss;

    fun mk_P_mono fs ts P P' mapT = Logic.list_implies
      (map (fn f => HOLogic.mk_Trueprop (mk_inj f)) fs,
        HOLogic.mk_Trueprop (HOLogic.mk_imp (co_swap
          (list_comb (P', map (fn t => list_comb (mapT, fs) $ t) ts), list_comb (P, ts)))));

    val P_mono_goals = @{map 4} (mk_P_mono f_vals) t_valss P_vals P'_vals mapTs;
    val P_mono_vars = map name_of (flat t_valss @ f_vals);

    val P_mono_tactics = @{map 3} (fn P_param_thm => fn us => fn T_rel_Grp_thm =>
      fn {context = ctxt, prems = _} => mk_P_mono_tac ctxt nr_param P_param_thm us T_rel_Grp_thm)
      P_param_thms P_mono_ctermss T_rel_Grp_thms;

    val P_mono_thms = @{map 2} (fn goal => fn tac =>
      Goal.prove_sorry lthy P_mono_vars [] goal tac
      |> Thm.close_derivation \<^here>)
      P_mono_goals P_mono_tactics;

    fun reorient T_un_T thm = @{thm reorient_le_o_Abs} OF [T_un_T, thm]
    val P_co_ind_thms = @{map 2} (fn T_un_T => case_fp fp (reorient T_un_T) I)
      T_un_T_thms (get_P_co_ind_thms0 P_mono_thms);

    val shape_subst = As ~~ shape_As;
    val cimmerse_raw_shape_Ts = map (fn raw => depthT --> raw --> raw) raw_shape_As;
    val shape_shape_As = map (Term.typ_subst_atomic shape_subst) shape_As;
    val T_shape_As = map (Term.typ_subst_atomic shape_subst) T_As;
    val Abs_shape_vals =
      @{map 3} (fn abs => fn T => fn U => Const (abs, T --> U)) T_Abs_names raw_shape_As T_shape_As;
    val cimmerse_raw_vals = @{map 2} mk_const_val (#cimmerse_raws nu_fp_res) cimmerse_raw_shape_Ts;
    val P_shape_vals = map (Term.subst_atomic_types shape_subst) P_vals;
    fun mk_PP abs P cimmerse rs = list_comb (P, map (fn r => abs $ (cimmerse $ u0 $ r)) rs);
    val PPs = @{map 4} mk_PP Abs_shape_vals P_shape_vals cimmerse_raw_vals raw_shape_valss;

    (* PP_unCons *)

    val invar_raw_shape_Ts = map (fn raw => depthT --> raw --> HOLogic.boolT) raw_shape_As;
    val invar_raw_shape_vals = @{map 2} mk_const_val invar_raw_vals invar_raw_shape_Ts;

    val invarss = @{map 2} (fn invar => fn rs => map (fn r => invar $ (mk_rev u0) $ r) rs)
      invar_raw_shape_vals raw_shape_valss;

    val PP_unCons_thms =
      let
        val unCons_cimmerse_raw_thms = #unCons_cimmerse_raw_thms nu_fp_res;
        val map_argss = @{map 2} (fn rs => fn thm => map (fn r =>
            infer_instantiate' lthy (map (SOME o Thm.cterm_of lthy) [u0, r]) thm
            |> Thm.prop_of |> HOLogic.dest_Trueprop |> HOLogic.dest_eq |> snd) rs)
          raw_shape_valss unCons_cimmerse_raw_thms;

        val mk_passive_arg = case_fp fp (fn T => absdummy T @{term True}) HOLogic.eq_const
        val passive_args = map mk_passive_arg shape_shape_As;
        val active_args = rest_occs (@{map 3} (fn abs => fn P => fn rs =>
            fold_rev lambda rs (list_comb (P, map (fn r => abs $ r) rs)))
          Abs_shape_vals P_shape_vals raw_shape_valss);

        val Ts = shape_shape_As @ rest_occs raw_shape_As;
        val assms =
          @{map 3} (fn bnf => fn Ds => fn map_args => HOLogic.mk_Trueprop (list_comb
            (list_comb (case_fp fp (mk_pred_of_bnf Ds Ts bnf) (mk_rel_of_bnf Ds Ts Ts bnf),
              passive_args @ active_args), map_args)))
          Gs Dss map_argss;

        fun mk_goal invars assm PP =
          Logic.list_implies (map HOLogic.mk_Trueprop invars,
            Logic.mk_implies (co_swap (assm, HOLogic.mk_Trueprop PP)));
        val goals = @{map 3} mk_goal invarss assms PPs;

        val tactics =
          @{map 9} (fn unCons_cimmerse => fn P_co_ind => fn invar_cimmerse =>
              fn raw_inject => fn raw_splits => fn set_maps => fn rel_maps => fn pred_set =>
              fn rel_mono_strong => fn ctxt =>
            mk_PP_unCons_co_tac fp ctxt unCons_cimmerse P_co_ind
              P_mono_thms rel_mono_strong (map (map (Thm.cterm_of ctxt)) Nodess)
              raw_splits raw_inject set_maps rel_maps pred_set
              shape_inject_thms shape_case_thms invar_shape_depth_iff_thms
              invar_raw_unflat_thms invar_raw_simps
              invar_cimmerse T_dtor_defs T_Abs_inverses T_map_defs)
          unCons_cimmerse_raw_thms P_co_ind_thms invar_cimmerse_raw_thms
          raw_inject_thms raw_split_thmss G_set_map_thmss G_rel_map_thmss G_pred_set_thms
          G_rel_mono_strong_thms
      in
        @{map 2} (fn goal => fn tac =>
          let
            val vars = Variable.add_free_names lthy goal [];
          in
            Goal.prove_sorry lthy vars [] goal (fn {context = ctxt, prems = _} => tac ctxt)
            |> Thm.close_derivation \<^here>
          end)
        goals tactics
      end;

    (* PP *)

    val PP_thms =
      let
        val mk_pre_goals = case_fp fp list_all_free list_exists_free [u0] o
          case_fp fp (Library.foldr HOLogic.mk_imp) (Library.foldr HOLogic.mk_conj)
        val pre_goals = @{map 2} (curry mk_pre_goals) invarss PPs;
        fun mk_goal pre_goal T rs = case_fp fp pre_goal
          (HOLogic.mk_imp (pre_goal, list_comb (HOLogic.eq_const T, rs)));
        val goals = @{map 3} mk_goal pre_goals raw_shape_As raw_shape_valss;
        val goal = Library.foldr1 HOLogic.mk_conj goals;
        val vars = Variable.add_free_names lthy goal [];
      in
        Goal.prove_sorry lthy vars [] (HOLogic.mk_Trueprop goal) (fn {context = ctxt, prems = _} =>
          mk_PP_co_tac fp ctxt co_induct_thm
            (@{map 2} (fn rs => Thm.cterm_of ctxt o fold_rev lambda rs) raw_shape_valss pre_goals)
            (map (Thm.cterm_of ctxt) (flat raw_shape_valss)) unCons_defs PP_unCons_thms
            G_rel_mono_strong_thms cimmerse_shape_inj_thms (flat raw_split_thmss)
            (flat G_set_map_thmss) (flat G_rel_map_thmss) invar_raw_simps G_pred_set_thms
            raw_inject_thms)
        |> Thm.close_derivation \<^here>
        |> split_conj_thm
      end;

    (* P *)

    val P_goals =
      case_fp fp (@{map 2} (curry list_comb) P_vals t_valss)
        (@{map 2} (fn P => mk_leq P o HOLogic.eq_const) P_vals T_As);

    val depth_Nil = HOLogic.nil_const labelT;
    val Rep_shape_vals =
     @{map 3} (fn rep => fn T => fn U => Const (rep, T --> U)) T_Rep_names T_shape_As raw_shape_As;

    val P_tactics = @{map 13} (fn PP => fn Rep => fn mapT => fn ts =>
        fn Abs_T_inverse => fn Abs_T_inject => fn Abs_T_cases => fn Rep_T_inverse => fn Rep_T =>
        fn map_T_def => fn P_mono => fn raw_inj_map_strong => fn cimmerse_simps =>
      case_fp fp
        (fn ctxt => mk_P_tac ctxt PP (Thm.cterm_of ctxt depth_Nil) (Thm.cterm_of ctxt Rep)
          (map (Thm.cterm_of ctxt) Leaves) (Thm.cterm_of ctxt mapT) (Thm.cterm_of ctxt (hd ts))
          Abs_T_cases Rep_T_inverse Rep_T P_mono shape_inject_thms cimmerse_simps)
        (fn ctxt => mk_P_cotac ctxt (map (Thm.cterm_of ctxt) Leaves) Abs_T_inject Abs_T_cases
          Abs_T_inverse PP invar_map_raw_closed_thms
          raw_inj_map_strong P_mono map_T_def shape_inject_thms cimmerse_simps))
      PP_thms Rep_shape_vals mapT_shapes t_valss T_Abs_inverses T_Abs_injects T_Abs_cases
        T_Rep_inverses T_Rep_thms T_map_defs P_mono_thms raw_inj_map_strong_thms cimmerse_raw_simpss;

    val P_thms =
      @{map 2} (fn goal => fn tac =>
        let
          val vars = Variable.add_free_names lthy goal [];
        in
          Goal.prove_sorry lthy vars [] (HOLogic.mk_Trueprop goal)
            (fn {context = ctxt, prems = _} => tac ctxt)
          |> Thm.close_derivation \<^here>
          |> case_fp fp I (fn thm => thm RS @{thm predicate2D})
        end)
      P_goals P_tactics;

    val nu_fp_ind_res = {
        co_ind_thms = P_thms
      };
  in
    (nu_fp_ind_res, lthy)
  end;

end;

end;
