theory TryNonUniformInduction imports "~~/src/HOL/Library/BNF_Axiomatization"
begin


datatype ('a,'b) F = F "unit + 'a \<times> 'b"
datatype 'b G = G "'b \<times> 'b"

typedecl 'a I

consts dtor :: "'a I \<Rightarrow> ('a, 'a G I) F"
consts ctor :: "('a, 'a G I) F \<Rightarrow> 'a I"
lemma dtor_ctor[simp]: "dtor (ctor f) = f"
  sorry

consts P :: "'a I \<Rightarrow> bool"
axiomatization where
   P_def:
   "P (xs :: 'a I) = (case dtor xs of
      F (Inl ()) \<Rightarrow> card (UNIV :: 'a set) < 5
    | F (Inr (x, xs)) \<Rightarrow> P xs)"

theorem I_induct: 
fixes i :: "'a I"
assumes "\<And> i :: ('a, 'a G I) F. (\<And> j. j \<in> set2_F i \<Longrightarrow> P j) \<Longrightarrow> P (ctor i)"
shows "P i"
sorry

lemma 1: "P (xs :: bool I)"
  apply (rule I_induct)
  apply (case_tac i)
  apply (subst P_def)
  apply (simp split: sum.splits unit.splits)
  done

abbreviation "nil \<equiv> ctor (F (Inl ()))"
abbreviation "cons x xs \<equiv> ctor (F (Inr (x, xs)))"

lemma *: "(UNIV :: 'a G set) = image G ((UNIV :: 'a set) \<times> (UNIV :: 'a set))"
  apply (auto simp: image_iff)
  apply (rename_tac x; case_tac x)
  apply (rename_tac x p; case_tac p)
  apply simp
  done

lemma **: "card (UNIV :: 'a G set) = card (UNIV :: 'a set) ^ 2"
  unfolding *
  apply (subst card_image)
  apply (simp add: inj_on_def )
  apply (subst card_cartesian_product)
  apply (rule semiring_normalization_rules(29))
  done
  
(*
lemma 2: "\<not> P (cons True (cons (G (True, True)) nil))"
  apply (subst P_def)
  apply simp
  apply (subst P_def)
  apply simp
  apply (subst P_def)
  apply (simp add: ** split: unit.splits)
  done

lemma False
  by (metis 1 2)
*)

(* Jasmin induction: *)
axiomatization where 
P : "\<And> i :: ('a, 'a G I) F. (\<And> j. j \<in> set2_F i \<Longrightarrow> P j) \<Longrightarrow> P (ctor i)"

theorem Jasmin_I_induct: 
fixes i :: "'a I"
shows "P i"
sorry


(* Dmitriy induction: *)

consts map_I :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a I \<Rightarrow> 'b I"
consts set_I :: "'a I \<Rightarrow> 'a set"
consts bd_I :: "nat rel"
consts rel_I :: "('a \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a I \<Rightarrow> 'b I \<Rightarrow> bool"

bnf "'a I"
 map: map_I
 sets: set_I
 bd: bd_I
 rel: rel_I
sorry

term rel_F

term set2_F

term ctor

axiomatization where 
  set2_F_param: 
  "\<And> (p :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (q :: 'b \<Rightarrow> 'b' \<Rightarrow> bool) (x :: ('a,'b) F) (x' :: ('a','b') F). 
  rel_F p q x x' \<Longrightarrow> (\<forall> b \<in> set2_F x. \<exists> b' \<in> set2_F x'. q b b') \<and> 
                     (\<forall> b' \<in> set2_F x'. \<exists> b \<in> set2_F x. q b b')"
and 
  ctor_param: 
  "\<And> (p :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (x:: ('a, 'a G I) F) (x' :: ('a', 'a' G I) F).
     rel_F p (rel_I (rel_G p)) x x' \<Longrightarrow> rel_I p (ctor x) (ctor x')"
and 
  dtor_param: 
  "\<And> (p :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (i::'a I) (i'::'a' I).
     rel_I p i i' \<Longrightarrow> rel_F p (rel_I (rel_G p)) (dtor i) (dtor i')"

(* The next two hold for any BNF: *)
lemma rel_I_True_lift:
defines "p \<equiv> \<lambda> (a::'a) (a'::'a'). True"
fixes i' :: "'a I"
shows "\<exists> i'. rel_I p i i'" 
proof
  def i' \<equiv> "map_I undefined i :: 'a' I"
  have "rel_I (BNF_Def.Grp UNIV undefined) i i'" 
  unfolding I.rel_Grp i'_def unfolding BNF_Def.Grp_def by auto
  moreover have "rel_I (BNF_Def.Grp UNIV undefined) \<le> rel_I p"
  using I.rel_mono unfolding p_def by fastforce
  ultimately show "rel_I p i i'" by auto
qed

lemma rel_I_True_lift2:
defines "p \<equiv> \<lambda> (a::'a) (a'::'a'). True"
fixes i :: "'a I"
shows "\<exists> i. rel_I p i i'" 
proof-
  have pm: "p\<inverse>\<inverse> = (\<lambda> (a::'a') (a'::'a). True)" unfolding p_def using conversep_iff by auto
  then obtain i where "rel_I p\<inverse>\<inverse> i' i" using rel_I_True_lift by auto 
  thus ?thesis using I.rel_flip by auto
qed

consts Q :: "'a I \<Rightarrow> bool"

axiomatization where 
Q_param : "\<And> (r :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (i :: 'a I) (i' :: 'a' I). rel_I r i i' \<Longrightarrow> Q i \<longleftrightarrow> Q i'"

lemma D_implies_J: 
assumes 1: "\<And> x :: ('a, 'a G I) F. (\<And> j. j \<in> set2_F x \<Longrightarrow> Q j) \<Longrightarrow> Q (ctor x)"
fixes x' :: "('a', 'a' G I) F"
assumes 2: "\<And> j'. j' \<in> set2_F x' \<Longrightarrow> Q j'"
shows "Q (ctor x')"
proof- (* probably lifting and transfer does this better: *)
  def p \<equiv> "\<lambda> (a::'a) (a'::'a'). True" 
  obtain i where ix': "rel_I p i (ctor x')" unfolding p_def using rel_I_True_lift2 by auto
  def x \<equiv> "dtor i" 
  have xx': "rel_F p (rel_I (rel_G p)) x x'" unfolding x_def using dtor_param ix' by fastforce
  {fix j assume "j \<in> set2_F x"
   then obtain j' where "j' \<in> set2_F x'" and jj': "rel_I (rel_G p) j j'"
   using set2_F_param xx' by blast
   hence "Q j'" using 2 by simp
   hence "Q j" using Q_param jj' by blast
  }
  hence "Q (ctor x)" using 1 by auto
  thus ?thesis using xx' Q_param ctor_param by blast
qed  

theorem D_induct: 
fixes i :: "'a I"
assumes "\<And> i :: ('a, 'a G I) F. (\<And> j. j \<in> set2_F i \<Longrightarrow> Q j) \<Longrightarrow> Q (ctor i)"
shows "Q i"
sorry


(* The difficulty essence: cross-type well-founded induction *)
typedecl exp
consts ord :: "exp rel"  
theorem "wf ord"
sorry

consts meas :: "'a I \<Rightarrow> exp"
consts red :: "'a I \<Rightarrow> ('a \<times> 'a) I"

theorem "(meas i, meas (red i)) \<in> ord"
sorry

consts R :: "'a I \<Rightarrow> bool"

theorem IH: "R i \<Longrightarrow> R (red i)"
sorry


theorem "R i" (*  ?   *)
sorry







 

end