% !TeX root = thesis.tex
% !TeX spellcheck = en_US

\chapter{Preliminaries}
\label{prelim}

For the convenience of the reader we repeat the relevant material from \cite{blanchette2014truly} to briefly introduce Isabelle's ecosystem of types in this chapter.

\section{Bounded Natural Functors}
\label{BNF}
We work with the following base types in this thesis:
\begin{description}[leftmargin=!,labelwidth=\widthof{$\alpha \to \beta$}]
	\item[$\alpha\times\beta$] -- the `Tuple' type with constructor $(\alpha, \beta)$,
	\item[$\alpha + \beta$] -- the `Either' type with constructors $In_L\ \alpha$ and $In_R\ \beta$,
	\item[$\alpha \to \beta$] -- the type of functions from $\alpha$ to $\beta$,
	\item[$\alpha\ list$] -- the type of lists containing elements of type $\alpha$,
	\item[$\alpha\ set$] -- the type of sets containing elements of type $\alpha$,
	\item[$nat$] -- the type of natural numbers,
	\item[$bool$] -- the type of logical values with instances $True$ and $False$, and
	\item[$unit$] -- the singleton type with instance $()$.
\end{description}
Datatypes can be represented as single unary constructors composed of these base types. We model a curried constructor with multiple arguments with $\times$ and multiple constructors are represented with $+$.

A datatype definition corresponds to a fix-point equation. For example, the equation $\beta = unit + \alpha \times \beta$ specifies either (finite) lists or lazy lists, depending on which fix-point is chosen. Bounded natural functors (BNFs) are a semantic criterion for where (co)recursion may appear on the right-hand side of an equation. In this thesis we will only discuss least fixed point BNFs and thus only work with types with finite instances.

An $n$-ary BNF $F$ is a type constructor with $n$ live type arguments. Dead type arguments are trivial to handle and irrelevant for this thesis. We frequently use the four following functions on $t::F$:

\begin{description}

\item[$map_F$]$ :: (\alpha_1 \to \beta_1) \to ... \to (\alpha_n \to \beta_n) \to (\alpha_1, ..., \alpha_n)\ F \to (\beta_1, ..., \beta_n)\ F$
is the functorial action of the BNF $F$, where $map_F\ f_1\ ...\ t$ applies the function $f_1$ to each element of $t$ with type $\alpha_1$, and the same for the other $n$ types.

\item[$set_{F, \alpha_i}$]$ :: (\alpha_1, ..., \alpha_n)\ F \to \alpha_i\ set$
are the $n$ natural transformations of $F$, where $set_{F, \alpha_1}\ t$ extracts all elements of type $\alpha_1$ from $t::F$.

\item[$rel_F$]$ :: (\alpha_1 \to \beta_1 \to bool) \to ... \to (\alpha_n \to \beta_n \to bool) \to (\alpha_1, ..., \alpha_n)\ F \to (\beta_1, ..., \beta_n)\ F \to bool$
can be used to compare two instances of $F$, where $rel_F\ R_1\ ...\ t\ t'$ accepts if the shapes of $t$ and $t'$ are equal and each element of $t$ with type $\alpha_1$ relates with $R_1$ to each corresponding element of $t'$ with type $\beta_1$, etc.

\item[$pred_F$]$ :: (\alpha_1 \to bool) \to ... \to (\alpha_n \to bool) \to (\alpha_1, ..., \alpha_n)\ F \to bool$
is the predicator of $F$ and satisfies the equation
$$pred\ P_1\ P_2\ ...\ t \iff \forall a_1\in set_{F, \alpha_1}\ t.\ P_1\ a_1\wedge \forall a_2\in set_{F, \alpha_2}\ t.\ P_2\ a_2\wedge ...$$

\end{description}

The constants of a BNF are subject to some properties, e.g. $map$ preserves identity and composition. Those theorems and many of their straightforward consequences are stored as theorems in Isabelle's BNF database.

For datatypes some additional theorems, such as the induction principle and injectivity of constructors, are stored. We reuse all of this proof infrastructure in our internal construction of non-uniform datatypes.



