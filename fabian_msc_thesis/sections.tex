\chapter{Preliminaries}

In this chapter we briefly introduce Isabelle's ecosystem of types.

\section{Datatypes}
\label{datatypes}

A simple list example illustrates the translation of a datatype which we do to implement non-uniform datatypes in Isabelle:

$\keyw{datatype}\ \alpha\ list = Nil\ |\ Cons\ \alpha\ (\alpha\ list)$

Isabelle transforms this dype declaration into an isomorphic representation which separates the recursion and Type definition as follows:

$(\alpha, \sigma)\ \keyw{G} = unit + \alpha * \sigma$\\
$\alpha~ \keyw{raw} = In ((\alpha, \alpha~ \keyw{raw})\ \keyw{G})$

We separate the datastructure represented by G from the recursion in raw.The recursive occurrence was replaced in $G$ with a new type variable $\sigma$ and assign the recursion in the definition of Raw. Both types together are isomorphic to list, in fact this separation of the recursive part can be made with all datatypes and raw is the same for any datatype with one type argument and one recursive call. G is what changes for different datatypes, for lists it is as above, but for finitely branching trees it would be $(\alpha, \beta)~ G = unit + \alpha * \beta~list$.

In addition to the constructor $In :: (\alpha, \alpha~ raw) G \rightarrow \alpha~ raw$ of raw we also define the destructor

$Out :: \alpha~ raw \rightarrow (\alpha, \alpha~ raw) G$\\
$Out~(In~g) = g$

The usual fold operation on list $fold :: (\alpha~ \rightarrow \beta~ \rightarrow \alpha~) \rightarrow \alpha~ \rightarrow \beta~ list \rightarrow \alpha~$ can be transformed into

$fold :: ((\alpha, \beta)~ G \rightarrow \beta~) \rightarrow \alpha~ raw \rightarrow \beta~$\\
$fold f = f * (map_G~id~(fold~f)) * Out$

where $map_G$ is the map function $map_G :: (\alpha \rightarrow \beta) \rightarrow (\sigma \rightarrow \tau) \rightarrow \alpha~ \sigma~ G \rightarrow \beta~ \tau~ G$.

\section{BNFs}
\label{BNF}

Datatypes can be represented as single unary constructors. Multiple curried constructors are modeled by disjoint sums of products. A datatype definition corresponds to a fixpoint equation. For example, the equation $\beta = unit + \alpha \times \beta$ specifies either (finite) lists or lazy lists, depending on which fixpoint is chosen. Bounded natural functors (BNFs) are a semantic criterion for where (co)recursion may appear on the right-hand side of an equation. The theory of BNFs is described in Traytel’s M.Sc. thesis \cite{traytel} and (previous paper). We refer to either of these for a discussion of related work.

BNFs are equiped with several operations and lemmas. Recursive functions on BNFs are supported in isabelle with the $\keyw{primrec}$ command and \cite{bnf} defines an automatic induction method for proving these recursive equations. 



pred, map (functorial action)

For the Functor $+$ we use the Constructors $In_L\ (.)$ and $In_R\ (.)$ to pattern-match either the left or right side of an instance. The tuple Functor $*$ is matched as usual with the $(.,.)$ constructor.



\chapter{Low-Level Construction}

Our construction can be best understood with examples showing each step. In Section~\ref{datastructure} we introduce a simple power-list and construct a larger type in the existing Isabelle $\keyw{datatype}$ framework. This type is then lifted in Section~\ref{typedef} with an invariant explained in Section~\ref{invar}. Type constructors using a flat and unflat operation are shown in Section~\ref{flat}. Then we expand the example of the power-list to have multiple type arguments (Section~\ref{nr_param}), multiple recursive occurrences (Section~\ref{nr_occ}) and multiple mutual types (Section~\ref{nr_Ts}). Finally we present the general construction in Section~\ref{generalized} and by providing witnesses we show non-emptyness of the type in Section~\ref{wit}.

\section{Construction by Example}
\label{datastructure}

Isabelle's $\keyw{datatype}$ command does not support non-uniform recursive datatypes as of yet, e.g. recursively defined types in which the type arguments of the recursive occurrences may vary. We construct a larger type and an invariant with the existing infrastructure.
Consider this definition of power lists:

$\alpha~ plist = Nil\ |\ Cons~\alpha~ ((\alpha~ * \alpha~) plist)$

Note that the type argument changes as we go down the recursion. The values stored in a power list $\alpha~ plist$ are essentially fully balanced binary trees with values of type $\alpha~$ in their leaves, e.g. the value of the first element has type $\alpha$, the second element has type $(\alpha, \alpha)$ which is a tuple of $\alpha$ and the third is of type $((\alpha, \alpha), (\alpha, \alpha))$, etc.
The basic idea is to separate the evolution of the type argument from the type definition, resulting in an isomorphic type that can be defined with $\keyw{datatype}$. We first define a recursive type Shape representing the change of the type and then use this type in the outer type called raw as follows:

$\alpha~ Shape = Leaf\ \alpha\ |\ Node\ ((\alpha\ Shape)*(\alpha\ Shape))$

$\alpha~ raw = In\ (unit + (\alpha\ Shape) * (\alpha\ raw))$

The $Node$ constructor of $Shape$ has recursions according to the change of the argument of the recursive occurrence of plist. The constructors of $plist$ can be modelled in BNF fashion as the single constructor $In$ of $raw$. It is worth to point out that the recursive occurrence of $raw$ has no change of the type argument as the change is fully handled by the $Shape$ type.

In this example $\alpha~ raw$ is not isomorphic to $\alpha~ plist$, since the $Shape$ type is not restricted, e.g. one can attach a large tree of $Shape$s to the first element of the list $raw$. Therefore valid instances of $raw$ must satisfy an invariant of the tree depth equal to the recursion depth. We define $invar_{raw}$ as the invariant depending on $invar_{Shape}$, which checks wether the shapes form a balanced tree of a certain depth.

$invar_{Shape} : \keyw{int} \rightarrow \alpha\ Shape \rightarrow \keyw{bool}\\
invar_{Shape}\ 0\ (Leaf\ \_) = \keyw{True}\\
invar_{Shape}\ n\ (Node\ (s_1, s_2)) = invar_{Shape}\ (n-1)\ s_1 \wedge invar_{Shape}\ (n-1)\ s_2\\
invar_{Shape}\ \_\ \_ = \keyw{False}$

We define $invar_{raw}$ to be true when the height of each Shape tree matches its recursion depth.

$invar_{raw} : \keyw{int} \rightarrow \alpha\ raw \rightarrow \keyw{bool}\\
invar_{raw}\ \_\ (In (In_L ())) = \keyw{True}\\
invar_{raw}\ n\ (In (In_R (s, r))) = invar_{Shape}\ n\ s\wedge invar_{raw}\ (n+1)\ r$

\subsection{Type Definition}
\label{fold}

We define the type $plist$ as the subset of $raw$ where the invariant $invar_{raw}$ holds, e.g. $\alpha\ plist = \{r:\alpha\ raw\ |\ invar_{raw}\ 0\ r\}$. We can reconstruct the original constructors of plist using $raw$, note that the constructor $Cons$ has type $\alpha \rightarrow (\alpha, \alpha)~plist \rightarrow \alpha~plist$.

 $Nil = abs_{plist} (In (In_L ())) \text{ and}\\
Cons\ a\ ls = abs_{plist} (In (In_R (Leaf\ a, flat_{raw}\ (rep_{plist}\ ls))))$

Where $abs_{plist}$ is the abstracting function of type $\alpha\ raw \rightarrow \alpha\ plist$ and $rep_{plist}$ is the representative function $rep_{plist} : \alpha\ plist \rightarrow \alpha\ raw$. The $+$ operator has the two constructors $In_L$ and $In_R$ and $flat_{raw}$ transforms a list of touples into a list of $Shapes$.

$flat_{Shape} : (\alpha, \alpha)\ Shape \rightarrow \alpha\ Shape\\
flat_{Shape} (Leaf\ (v_1, v_2)) = Node\ (Leaf\ v_1, Leaf\ v_2)\\
flat_{Shape} (Node\ (s_1, s_2)) = Node\ (flat_{Shape}\ s_1, flat_{Shape}\ s_2)$

$flat_{raw} : (\alpha, \alpha)\ raw \rightarrow \alpha\ raw\\
flat_{raw}\ (In (In_L ())) = In (In_L ())\\
flat_{raw}\ (in (In_R (s, r))) = In (In_R (flat_{Shape}\ s, flat_{raw}\ r))$

Assuming $r$ satisfies $invar_{raw}\ 0$, then $flat_{raw} r$ will satisfy $invar_{raw}\ 1$. We also define an unflat operation inverse to flat.

As mentioned before, in the next three Sections we show how to adapt the datastructures when there are more type arguments, multiple recursive occurrences or multiple mutual types respectively.

\subsection{Number of Type Arguments}
\label{nr_param}

To support more than one type argument for a non-uniform datatype we need to adapt the underlying infrastructure. We demonstrate the changes on another list example with two values per element. Both types of these values can individually change along the recursion:

$(\alpha, \beta)\ plist_2 = Nil\ |\ Cons\ \alpha\ \beta\ ((\alpha * \beta, \alpha + \beta)\ plist_2)$

With multiple type arguments it is also possible to have multiple different changes of type arguments. Also the change of an argument can depend on any other argument. Let's have a look at the types of the first few elements of this list.

$\alpha \text{ and } \beta
\\\alpha * \beta \text{ and } \alpha + \beta
\\(\alpha * \beta) * (\alpha + \beta) \text{ and } (\alpha * \beta) + (\alpha + \beta)
%\\((\alpha * \beta) * (\alpha + \beta)) * ((\alpha * \beta) + (\alpha + \beta)) \text{ and } ((\alpha * \beta) * (\alpha + \beta)) + ((\alpha * \beta) + (\alpha + \beta))
$

The first type argument depends on the second and vice versa. This behaviour can be imitated in Isabelle by two mutually recursively defined Shape types -- the shapes can recurse into each other.

$(\alpha, \beta)\ Shape_A = Leaf\ \alpha\ |\ Node\ (((\alpha, \beta)\ Shape_A) * ((\alpha, \beta)\ Shape_B)))
\\(\alpha, \beta)\ Shape_B = Leaf\ \beta\ |\ Node\ (((\alpha, \beta)\ Shape_A) + ((\alpha, \beta)\ Shape_B))$

$
\alpha~ \beta~ raw = In\ (unit + ((\alpha, \beta)\ Shape_A) * ((\alpha, \beta)\ Shape_B) * ((\alpha, \beta)\ raw))$

The two shape types are sufficient to create all the occurring types by recursing through the structure until a Shape tree of the requred height is formed. The type Raw has no significant changes, notable is that .

The invariant and fold operations can easily be extended to this case. We define the invariant mutually recursive for the mutually defined Shapes. The functions are named $invar_{Shape_A}$ and $invar_{Shape_B}$ and they recursvely call each other. The exact definition of the invariant and also the flat operations will be shown at the end of this Section.

\subsection{Number of occurrences}
\label{nr_occ}

Type definitions can have more than one recursive occurrence. The infrastrtucture can't handle this case (yet), we present another simple example to present the changes. This is entirely compatible with the previous multiple parameters adapthion but this example has again only one type argument.

$\alpha\ plist_3 = Nil\ |\ Cons_1\ \alpha\ ((\alpha * \alpha)\ plist_3)\ |\ Cons_2\ \alpha\ ((\alpha + \alpha)\ plist_3)$

This list features two $Cons$ constructors, both with one recursive occurrence, each has different recursive type arguments. We can determine the evolution of the type arguments on each step by choosing to either use $Cons_1$ or $Cons_2$ to continue the list. For example the type

$((\alpha + \alpha) * (\alpha + \alpha)) * ((\alpha + \alpha) * (\alpha + \alpha))$

can occur in $plist_3$ if we construct it using the sequence $\langle Cons_2, Cons_1, Cons_1\rangle$. It is worth pointing out that the type evolves the same over one `level' of the type tree. We adapt the construction in the following way:

$\alpha\ Shape = Leaf\ \alpha\\ |\ Node_1\ ((\alpha\ Shape) * (\alpha\ Shape))\\ |\ Node_2\ ((\alpha\ Shape) + (\alpha\ Shape))$

$\alpha~ raw = In (unit + (\alpha\ Shape) * (\alpha\ raw) + (\alpha\ Shape) * (\alpha\ raw))$

Here again we can build all the needed types with Shapes and the List is constructed with Raw. The important thing to note is that the Shapes can construct more types than there can be in $plist_3$, even when the Shapes satisfy our current invariant of fixed height. For example we can construct the Type

$((\alpha + \alpha) * (\alpha * \alpha)) * ((\alpha + \alpha) + (\alpha + \alpha))$

which can't be constructed with any sequence of $Cons_1$ and $Cons_2$ constructors, despite the fact that this example is fully balanced and satisfies the current invariant. We adapt the invariant by replacing the integer depth with a list of labels $\lst{L}$ which specifies the occurrence we recursed into. Let these labels be called $L_1$ and $L_2$, indicating a recursion into the recursive occurrence of $Cons_1$ or $Cons_2$ respectively. $invar_{Shape}$ validates that a tree of Shapes has the structure specified by a given list of labels by pattern-matching the label to the used $Node$ constructor. We need two equations in the $L_2$ case on a technicality (the $+$ Functor does not have a single constructor):

$invar_{Shape} :: [label] \rightarrow Shape \rightarrow bool\\
invar_{Shape}\  [] (Leaf~a) = true\\
invar_{Shape}\ (L_1 : \lst{L}) (Node_1\ (s_1 * s_2)) = (invar_{Shape}\ \lst{L}\ s_1) \wedge (invar_{Shape}\ \lst{L}\ s_2)\\
invar_{Shape}\ (L_2 : \lst{L}) (Node_2\ (In_L\ s)) = invar_{Shape}\ \lst{L}\ s\\
invar_{Shape}\ (L_2 : \lst{L}) (Node_2\ (In_R\ s)) = invar_{Shape}\ \lst{L}\ s\\
invar_{Shape}\ \_\ \_ = false$

$invar_{raw}$ validates the structure by invoking $invar_{Shape}$ on on each level of recursion. The label list is extended whenever we recurse into an occurrence by appending the corresponding label to the list, starting with an empty list.

$invar_{raw} :: [label] \rightarrow raw \rightarrow bool\\
invar_{raw} \lst{L} (In (In_L ())) = true\\
invar_{raw}\ \lst{L} (In (In_R (In_L (s, r)))) = (invar_{Shape}\ \lst{L}\ s) \wedge (invar_{raw}\ (L_1 : \lst{L})\ r)\\
invar_{raw}\ \lst{L} (In (In_R (In_R (s, r)))) = (invar_{Shape}\ \lst{L}\ s) \wedge (invar_{raw}\ (L_2 : \lst{L})\ r)$

There are three equations in this example, one for $Nil$ and two for the Constructors $Cons_1$ and  $Cons_2$. At each recursion we append a different label to the list of labels.

\subsection{Number of Mutual Types}
\label{nr_ts}

Our construction also supports mutually recursive datatypes. We naturally apply the principles shown in the previous Section since there are always multiple recursive occurrences in mutually recursive definitions. We introduce our construction again on an example, this time two mutually recursive lists:

$\alpha\ plist_{4a} = Nil\ |\ Cons\ \alpha\ ((\alpha * \alpha)\ plist_{4b})\\
\alpha\ plist_{4b} = Cons\ \alpha\ ((\alpha + \alpha)\ plist_{4a})$

Here $\alpha~ plist_{4a}$ is a list of even length, where the type argument alternates between two different changes. The list is of even length since it can only end in the Nil~constructor of $plist_{4a}$. Both types depend on each other, so they are mutually recursive and there are two recursive occurrences, one for each type.

$\alpha\ Shape = Leaf\ \alpha\\ |\ Node_1\ ((\alpha\ Shape) * (\alpha\ Shape))\\ |\ Node_2\ ((\alpha\ Shape) + (\alpha\ Shape))$

$\alpha\ raw_1 = In\ (unit + (\alpha\ Shape) * (\alpha\ raw_2) )\\
\alpha\ raw_2 = In\ ((\alpha\ Shape) * (\alpha\ raw_1))$

We have two raw datastructures representing the two types $plist_{4a}$ and $plist_{4b}$. The Shapes behave the same as in the previous section with multiple occurances, since they have the same change of the arguments. As you can see $raw_1$ recurses into $raw_2$ and vice versa, as in the definiton of $plist$.

As for the Shapes, $invar_{Shape}$ remains the same, but we now have two $invar_{raw}$s. They recurse as you would expect - $invar_{raw_1}$ into $invar_{raw_2}$ and vice versa. 

\section{Generalized Construction}

\subsection{The Generalized Datatype}
\label{generalized}

As promised we present the generalized infrastructure in this Section. We have to consider variable number of parameters, occurrences and multiple mutually recursive types. Assume our input is a non-uniform datatype with $n$ type arguments, $t$ mutual types with totally $m$ occurrences of recursion distributed among the $t$ mutual types such that the $i$-th type has $m_i$ occurrences and $\sum{m_1 ... m_t} = m$.

\begin{itemize}
\item First we extract the changes of the arguments from the user's type definition. We define $F_{i, j}$ as the change of the $j$-th argument of the $i$-th occurrence of a recursion. Thus, $F$ defines a Matrix of dimension $m\times n$. In example $plist_2$ (repeated below) we have two changes $F_{1, 1}$ and $F_{1,2}$, and in example $plist_3$ we have $F_{1,1}$ and $F_{2,1}$.

$(\alpha, \beta)\ plist_2 = Nil\ |\ Cons\ \alpha\ \beta\ ((\alpha * \beta, \alpha + \beta)\ plist_2)$

$(\alpha, \beta)\ F_{1, 1} = \alpha * \beta \text{ and } (\alpha, \beta)\ F_{1,2} = \alpha + \beta$

$\alpha\ plist_3 = Nil\ |\ Cons_1\ \alpha\ ((\alpha * \alpha)\ plist_3)\ |\ Cons_2\ \alpha\ ((\alpha + \alpha)\ plist_3)$

$\alpha\ F_{1, 1} = \alpha * \alpha \text{ and } \alpha\ F_{2,1} = \alpha + \alpha$

Each $F_{i,j}$ is a BNF with $n$ type arguments. 

\item For each mutually defined datatype we also extract the non-recursive structure as a vector $G$ of length $t$, each $G_k$ representing the constructors of the $k$-th type. We substitute each recursive occurrence with a new type argument $\tau$, $\sigma$, etc. . Thus, each $G_k$ has $n+m$ type arguments divided into two parts: first $n$ for the type arguments of the user's type, followed by $m$ for each occurrence of a recursion. In example $plist_{4a}, plist_{4b}$ we have two Gs:

$\alpha\ plist_{4a} = Nil\ |\ Cons\ \alpha\ ((\alpha * \alpha)\ plist_{4b})\\
\alpha\ plist_{4b} = Cons\ \alpha\ ((\alpha + \alpha)\ plist_{4a})$

$\alpha\ \tau\ \sigma\ G_1 = unit + \alpha * \sigma\\
\alpha\ \tau\ \sigma\ G_2 = \alpha * \tau$

Note the order of the secondary type arguments: we sort them by the type they represent, e.g. $\tau$ is a recursion into $plist_{4a}$ which comes before the argument(s) representing $plist_{4b}$. In general we first have $m_k$ arguments representing recursions into the $k$-th type.

\begin{remark}
The parsing and extraction of $F$ and $G$ of a type declaration was not part of this thesis, rather a matrix $F$ and vector $G$ are the input of the program.
\end{remark}

\item We define $n$ mutual $Shape$ types, one for each type argument. As discussed before, those Shapes handle the change of the type and utilise the $F$ BNFs. Each $Shape_i$ has one $Leaf$ constructor $Leaf_i$ which instanitates the $i$-th type argument and $m$ Node constructors $Node_{i,j}$, having $F_{j,i}$, instantiated with shapes as type arguments, as single constructor argument. The Shape type definition ends up looking as follows:

$$\begin{aligned}
\keyw{datatype}\quad (\alpha_1, \alpha_2, ...)\ Shape_1 =&\ ...\\
\keyw{and}\quad (\alpha_1, \alpha_2, ...)\ Shape_i =&\ Leaf_i\ \alpha_i\\
&\ ...\\
|&\ Node_{i, j}\ (((\alpha_1, \alpha_2, ...)\ Shape_1,\\
&\quad\quad\quad\ \ \quad(\alpha_1, \alpha_2, ...)\ Shape_2, ...)\ F_{j, i})\\
&\ ...
\end{aligned}$$

\item There are $t$ raw types, each with $n$ type arguments and a singular constructor $In_k$, for $k\in\{1..t\}$. Each constructor $In_k$ has one type argument, the corresponding $G_k$ with its arguments instantiated with $n$ Shape types followed by $m$ raw types, where $raw_k$ is repeated $m_k$ times to fill all $m$ arguments.


$$\begin{aligned}
\keyw{datatype}\quad (\alpha_1, \alpha_2, ...)\ raw_k =&\ In_k\ (((\alpha_1, \alpha_2, ...)\ Shape_1, (\alpha_1, \alpha_2, ...)\ Shape_2, ...,\\
&(\alpha_1, \alpha_2, ...)\ raw_{k_1}, (\alpha_1, \alpha_2, ...)\ raw_{k_2}, ...)\ G_k)
\end{aligned}$$

Where the function $k_l = max\left\{i | \sum_{j=1}^{i-1}{m_j} < l\right\}$ maps the index of the recursive occurrence to the associated raw type.
\end{itemize}

\subsection{The Invariant}
\label{invariant}

As mentioned before, the types build in the previous Section are supertypes of the intended non-uniform datatype. We define an invariant which restricts the Shapes to the growth-pattern of the non-uniform datatype. This is done in two steps: $invar_{Shape_i}$ to validate the tree of Shapes and $invar_{raw_k}$ to define the required structure of Shapes at each point of the recursion with a list of labels (as seen in Section~\ref{nr_occ}). We can use the predicators $pred_{F_{j,i}}$ and $pred_{G_k}$ seen in Section~\ref{BNF} to recurse through the $F$ and $G$ BNFs. 

The functions $invar_{Shape}$ are $n$-ary mutually recursive -- they all call each other recursively. Each takes a label list and a $Shape$ and returns a boolean indicating the well-formedness of the Shape tree. It does so by pattern-matching the pivotal label $L_j$ with the $Node_{i, j}$ constructor before recursing into $F$. Once it arrives at a $Leaf$ and the label list is empty the function accepts; if the list is not empty or a label on the way does not mach the $Node$ the function aborts.

$invar_{Shape_i} :: [label] \rightarrow Shape_i \rightarrow bool\\
invar_{Shape_i}\ []\ (Leaf_i~a) = true\\
invar_{Shape_i}\ (L_1 : \lst{L})\ (Node_{i,1}\ f) = pred_{F_{1, i}}\ (invar_{Shape_1} \lst{L})\ (invar_{Shape_2} \lst{L})\ ...\ f\\
invar_{Shape_i}\ (L_2 : \lst{L})\ (Node_{i,2}\ f) = pred_{F_{2, i}}\ (invar_{Shape_1} \lst{L})\ (invar_{Shape_2} \lst{L})\ ...\ f\\
...\\
invar_{Shape_i}\ \_\ \_ = false$

The $invar_{raw}$ are $t$ mutually recursively defined functions that maintain a list of labels representing the recursive occurrences recursed into at any point in time. Using $pred_{G_k}$ we can apply the invariants of the Shapes and recurse into $invar_{raw_{k_j}}$ while appending the associated label $L_j$ to the label list $\lst{L}$.

$\ invar_{raw_k} :: [label] \rightarrow raw_k \rightarrow bool\\
\begin{aligned}
invar_{raw_k} \lst{L} (In\ g) = pred_{G_k}\ &(invar_{Shape_1}\ \lst{L})\ (invar_{Shape_2}\ \lst{L})\ ...\\
&(invar_{raw_{k_1}}\ (L_1 : \lst{L}))\ (invar_{raw_{k_2}}\ (L_2 : \lst{L}))\ ...\ g
\end{aligned}$

Here we have the same index-mapping function $k_l$ as defined in Section~\ref{generalized}.


\subsection{Type Definition}
\label{abstraction}

As discussed in Section~\ref{fold} we hide the underlying low-level construction behind the lifted types $(\alpha_1, \alpha_2, ...)\ T_k = \left\{r :: (\alpha_1, \alpha_2, ...)\ raw_k\ |\ invar_{raw_k}\ []\ r\right\}$ for $1\leq k\leq t$. The general constructor $T_k$ has the type:

$(\alpha_1, \alpha_2, ..., ((\alpha_1, \alpha_2, ...)\ F_{1,1}, (\alpha_1, \alpha_2, ...)\ F_{1,2},...)\ T_{k_1}, (...) T_{k_2}, ...)\ G_k \rightarrow (\alpha_1, \alpha_2, ...)\ T$

Revisiting the $plist$ example, we see that this is an unification of all $Nil$ and $Cons$ constructors into one singular constuctor. Note that $Cons$ takes an argument of type $(\alpha, \alpha)\ T$ and as before we need to flatten this to represent the tupled type with Shapes. In the general case we need to get from aFT to aT with a flat operation.
the constructor is defined to flatten the recursive arguments.

$T_k\ g = abs_{T_k}\ (In_k\ (map_{G_k}\ Leaf_1\ Leaf_2\ ...\ (flat^1_{raw_{k_1}} \circ rep_{T_{k_1}})\ (flat^2_{raw_{k_2}} \circ rep_{T_{k_2}})\ ...\ g))$

wrapps the values in leaves and flattens the recursive occs to use Shapes instead of directly have a different type. 

$flat^j_{raw_r}\ (In_k g) = In_k\ (map_{G_k}\ flat^j_{Shape_1}\ flat^j_{Shape_2}\ ...\ flat^j_{raw_{k_1}}\ flat^j_{raw_{k_2}}\ g)$


We also define a destructor $T^{-1}$ inverse to the constructor. 

$T^{-1} :: \alpha~T \rightarrow (\alpha, \alpha~F~T)~G$

where T is the abstraction of raw. The implementation of T makes use of a flat function defined on raw to map from $\alpha~F~T$ to $\alpha~T$ (as seen in the exuation above). Similar $T^{-1}$ uses an unflat function. On our plist example these are defined as:

$flat :: \alpha~F~raw \rightarrow \alpha~raw\\
flat (Cons g) = Cons (map_G flat_{Shape} flat g)$

$unflat :: [label] \rightarrow \alpha~raw \rightarrow \alpha~F~raw\\
unflat \lst{L} (Cons g) = Cons (map_G (unflat_{Shape} \lst{L}) (unflat (L_1 : \lst{L})) g)$

Where the same operations on the shapes are defined as:

$
flat_{Shape} :: \alpha~F~Shape \rightarrow \alpha~Shape\\
flat~(Leaf f) = Node~(map_F Leaf f)\\
flat~(Node f) = Node~(map_F flat_{Shape} f)$

$unflat :: [label] \rightarrow \alpha~Shape \rightarrow \alpha~F~Shape\\
unflat~\lst{L}~(Node f) = 
\begin{cases}
    Leaf~(map_F~Leaf^{-1}~f), & \lst{L} = []\\
    Node~(map_F~(unflat_{Shape}~\lst{L}')~f), & \lst{L} = L_1 : \lst{L}'
\end{cases}$

\section{Witnesses and Theorems}

\subsection{Witnesses}
\label{wit}

Non-emptyness witnesses prove that there exists at least one instance of raw satisfying the invariant. Witnesses of our datastructure need to satisfy the depth invariant, thus their generation is nontrivial. Witnesses of raw depend on the witnesses of the Gs and Shapes, and those depend on the witnesses of the F BNFs. Since they need to comply with the invariant, the shape witnesses also have to be of certain depth according to some labels.

Witnesses can be partially ordered depending on the arguments they take, e.g. a witness that takes one argument is worse than a witness that requires none. Thus a witness can be represented by the set of arguments $I_{wit} \subset \{1..N\}$ it takes. A witness $I$ is minimal if there is no other witness that takes a subset of its arguments, e.g. $\forall I'. I\not\subset I'$, and the set of all of these witnesses for a datatype $\alpha$ are simply called the witnesses $\mathbb{I}_\alpha$ of that datatype. 

To get the witnesses of raw we need to be able to get the witnesses for a shape with a specific depth. Given the witnesses $\{\mathbb{I}_F\}_{\forall Fs}$ of all Fs we can compute the witnesses $wit_{Shape_i}(d)$ of $Shape_i$ restricted by depth $d$ recursively by getting the witnesses of the shapes of the witnesses of the F indicated by the frontmost label of $d$. these can be computed recursively with the tail of $d$. 

TODO in detail

\subsection{Theorems}

In addition to witnesses of our datastructure we would also like to prove the bijectivity of the constructor $T$ and destructor $T^{-1}$ defined in Section~\ref{abstraction} and the behaviour of the characteristic functions map, set and rel.

\subsubsection{Bijection of the Constructor}

For the bijection proof we need some additional lemmas about the functions flat and unflat used in the constructors. There are four such lemmas for both, Shapes and raw, two stating that flat and unflat are a bijection and two showing that the corresponding invariant still holds after such an operation. These lemmas are generated along with the datastructure and proven automatically with a generated tactic. We demonstrate these lemmas on the plist example:

$
\forall \lst{L}.\ invar_{Shape}~(snoc~L_1~\lst{L})~u \implies flat_{Shape}~(unflat_{Shape}~\lst{L}~u) = u\\
\forall \lst{L}.\ invar_{Shape}~\lst{L}~u \implies unflat_{Shape}~\lst{L}~(flat_{Shape}~u) = u\\
\forall \lst{L}.\ invar_{Shape}~\lst{L}~u \implies invar_{Shape}~(snoc~F~\lst{L})~(flat_{Shape}~u)\\
\forall \lst{L}.\ invar_{Shape}~(snoc~L_1~\lst{L})~u \implies invar_{Shape}~\lst{L}~(unflat_{Shape}~\lst{L}~u)\\$

$
\forall \lst{L}.\ invar~(snoc~L_1~\lst{L})~t \implies flat~(unflat~\lst{L}~t) = t\\
\forall \lst{L}.\ invar~\lst{L}~t \implies unflat~\lst{L}~(flat~t) = t\\
\forall \lst{L}.\ invar~\lst{L}~t \implies invar~(snoc~F~\lst{L})~(flat~t)\\
\forall \lst{L}.\ invar~(snoc~L_1~\lst{L})~t \implies invar~\lst{L}~(unflat~\lst{L}~t)\\$

Using these lemmas the bijection $T~(T^{-1}~x) = x$ and $T^{-1}~(T~x) = x$ can be proven automatically.

\subsubsection{Characteristic Theorems}

We also prove that the characteristic functions do the same wether you apply them to the abstract datatype or our construction underneath.

In the plist example $map: (\alpha \rightarrow \beta) \rightarrow \alpha~ raw \rightarrow \beta~ raw$ has to satisfy the theorem:

$\forall f.\ map_T~f~(T~g) = T~(map_G~f~(map_T~(map_F~f)) g)$

E.g.\ it doesn't matter wether you apply the constructor $T$ before or after $map$ing. To prove this theorem automatically we first prove similar lemmas about $map_{Shape}$ and $map_{raw}$. We do the same for the $set$ function, which extracts a set of all occurrences of a type from a larger type, and for the $rel$ function, which allows comparison of two objectsbasd on its fields.

$
set_\alpha: \alpha~ raw \rightarrow \alpha~set\\
rel: (\alpha \rightarrow \beta \rightarrow bool) \rightarrow \alpha~ raw \rightarrow \beta~ raw \rightarrow bool$

\chapter{General Fold Combinators in Isabelle}

\section{Theorems}

\chapter{Conclusion}

\section{Further Work}

\begin{itemize}
\item non-uniform codatatypes
\item Nested recursion $\alpha~bush = Nil~ | Cons~ \alpha~((\alpha~bush)~bush)$
\end{itemize}