% !TeX root = thesis.tex
% !TeX spellcheck = en_US

\chapter{Non-Uniform Primitive Recursion}
\label{gfold}

As discussed in the introduction, traditional fold operators are not expressive enough to handle non-uniform datatypes. Our tool generates an extended fold operator with an additional type parameter to handle the changing type argument. We demonstrate the construction of primitive, non-uniform recursive functions on two examples. We start with a recursive $sum$ function which sums the elements of a power-list and show how its recursor is built in Section~\ref{plist_sum}. Then we extend the solution to handle more type parameters with another example in Section~\ref{plist_flat}. The infrastructure we implemented is fully parameterizable and shown in Section~\ref{grec}. Finally we validate our construction in Section~\ref{grec_thm}.

\section{Construction by Example}
\label{plist_sum}
The sum function on a power-list containing integer elements is defined as follows:

$
sum :: nat\ plist \to nat\\
sum\ (Nil) = 0\\
sum\ (Cons\ x\ xs) = x + sum\ (map_{plist}\ (\lambda (v_1,v_2).\ v_1 + v_2)\ xs)$

The emphasis here lies on the type of $xs :: (nat\times nat)\ plist$. $sum$ cannot be called on that subtree of tuples, so we map it to the addition of the tuple fields. Still, this definition must be rejected by Isabelle's $\keyw{primrec}$ command, since the argument of the recursive call is not a proper subtree. Isabelle's $\keyw{function}$ command cannot prove termination and rejects the this declaration. It is missing the $size$ function \cite{krauss2010partial} which requires the $sum$ function internally.

We circumvent this limitation by separating the function into two parts and applying them to our internal infrastructure. The \emph{reducing} function is the first part and handles the changing arguments. We can extract it from the the definition above by looking at the argument of the $map_{plist}$ function and obtain:

$rdc :: nat\times nat \to nat\\
rdc\ (v_1, v_2) = v_1 + v_2$

The second function handles non-recursive part of the original definition and is called the \emph{folding} function. It is non-recursive and expects the result of the recursive call as an argument:

$fld :: unit + nat \times nat \to nat\\
fld\ (In_L\ ()) = 0\\
fld\ (In_R\ (x, xs_{result})) = x + xs_{result}
$

Our tool receives both, $fld$ and $rdc$, as input and requires them to be parametric in their type variables. In this case this is given since there are no type variables. Additionally, we only allow primitive recursive functions to be declared.

We define a general recursor by applying the $rdc$ function to the $shape$ types and let $fld$ handle the rest of the function. The generalized recursor $grec_{raw}$ is primitively recursive on $raw$ and defined as follows:

$grec_{raw} :: nat\ raw \to nat\\
grec_{raw}\ (In\ (In_L\ ())) = fld\ (In_L ())\\
grec_{raw}\ (In\ (In_R\ (s, r)) = fld\ (In_R\ (grec_{shape}\ s, grec_{raw}\ r))$

$grec_{shape} ::nat\ shape \to nat\\
grec_{shape}\ (Leaf\ v) = v\\
grec_{shape}\ (Node\ (s_1, s_2)) = rdc\ (grec_{shape}\ s_1, grec_{shape}\ s_2)$

$grec_{raw}$ invokes the $fld$ function on the reduced $shape$s and the result on the recursive call. $grec_{shape}$ recursively applies $rdc$ to the shape tree. Both functions do not depend on the implementation of $rdc$ and $fld$, but can vary for different domain and range types. Both are primitively recursive and can be defined in the Isabelle environment.

The user obtains the recursive function $sum$, which simply calls $grec_{raw}$ on the representation of the input:

$sum :: nat\ plist \to nat\\
sum\ p = grec_{raw}\ (rep_{plist}\ p)$

\paragraph{Theorem}

In addition to this output we also prove Theorem~\ref{thm:grec_plist}, stating that our definition of $sum$ satisfies its specification. For better readability we work with the general constructor $T$  defined in Section~\ref{generalized}.

\begin{equation}\label{thm:grec_plist}
sum\ (T\ g) = fld\ (map_G\ id\ (sum\circ map_{plist}\ rdc)\ g)
\end{equation}

Theorem~\ref{thm:grec_plist} can be proven automatically with the help of some auxiliary lemmas. Section~\ref{grec_thm} presents this in detail for the general case.

\subsection{Live Domain and Range Types}
\label{plist_flat}

Let us call the inner type of the non-uniform datatype the \emph{domain} type. In the previous example neither domain nor range type had live type arguments (both were $nat$). In this second example we adapt our infrastructure to handle the function $pflat$, which flattens a power-list into a normal list. Here we have domain and range types with live type arguments:

$pflat :: \alpha\ plist \to \alpha\ list\\
pflat\ Nil = []\\
pflat\ (Cons\ x\ xs) = x : (append\ (unzip\ (pflat\ (map_{plist}\ id\ xs))))$

We write the equation in expanded form to empathize the $rdc$ function, here just an identity function. The other used functions are what you expect, $unzip$ splits a list of tuples into two lists and $append$ appends a list to the end of the other. In this example the domain and range types do have live type variables $\alpha\ D = \alpha$ and $\alpha\ R = \alpha\ list$. Beside the identity function $rdc$ we have the folding function is defined as:

$fld :: unit + \alpha \times ((\alpha\times\alpha)\ list) \to \alpha\ list\\
fld\ (In_L ()) = []\\
fld\ (In_R\ (x, xs_{result})) = x : (append\ (unzip\ xs_{result}))$

We require the definitions of $rdc$ and $flt$ to be parametric. In this case they are, $rdc$ trivially since it is the identity function and $fld$ satisfies
$$ \forall R.\ \forall x,y.\ rel_{G'}\ R\ x\ y\implies rel_{list}\ R\ (fld\ x)\ (fld\ y)$$
Where $\alpha\ G' = unit + \alpha \times ((\alpha\times\alpha)\ list)$. Our tool requires this as a fact and generates the following functions: 

$grec_{raw} :: \alpha\ raw \to (\alpha\ shape)\ list\\
grec_{raw}\ (In\ (In_L\ ())) = fld\ (In_L\ ())\\
grec_{raw}\ (In\ (In_R\ (s, r))) = fld\ (In_R\ (grec_{shape}\ s, map_{list}\ unNode\ (grec_{raw}\ r)))
$

$grec_{shape} :: \alpha\ shape \to \alpha\ shape\\
grec_{shape}\ (Leaf\ v) = Leaf\ v\\
grec_{shape}\ (Node\ (s_1, s_2)) =Node\ (rdc\ (grec_{shape}\ s_1, grec_{shape}\ s_2))$

The definitions here are very similar to those of the $sum$ function. The notable difference is that we pack the results of the reduce functions into shapes and $unNode$ them in $grec_{raw}$. This allows us to define functions for larger types where we have multiple parameters or recursive occurrences. Finally to deliver the function to the user we apply th $unLeaf$ function:

$pflat :: \alpha\ plist\to \alpha\ list\\
pflat\ p = map_{list}\ unLeaf\ (grec_{raw}\ (rep_{plist}\ p))$

\section{Generalized Construction}
\label{grec}

There are no intuitive examples of functions on non-uniform datatypes with multiple parameters, occurrences or mutually recursive types, thus we will introduce the general construction here. Reviewing the infrastructure of Section~\ref{generalized} where we have $t$ mutually recursive $n$-ary non-uniform datatypes with $m$ recursive occurrences, we have the following additional structure:

\begin{itemize}
\item Let $D_i$ be the instantiated type arguments of the non-uniform datatype the recursive function is defined on. These \emph{domain} types themselves can all have different type arguments, but we require them to all have the same number $x \geq 0$ of live type arguments. This is not a restriction, since one can add unused type arguments to normalize them.

\item If the non-uniform datatype is mutually recursive, the function must also be mutually recursively defined. Each of those functions can have a different range type $R_k$ of which there are $t$. Each range has the same type arguments, but out of a technicality they can only have a number of type arguments that is a multiple of the number of type arguments the non-uniform datatype takes, e.g. we define a constant $y$ such that $R$ has $n*y$ type arguments, we denote them as $\gamma_{i,j}$.

\item We extract $m \times n$ reduction functions $rdc_{j, i}$ arranged in a matrix. Each $rdc_{j, i}$ reduces one change of argument $F_{j,i}$ of the non-uniform datatype and has the type:

$$\begin{aligned}
((\alpha_1, \alpha_2, ...)\ D_1, (\beta_1, \beta_2, ...)\ D_2, ...)\ F_{j, i} \to \\
((\alpha_1, \beta_1, ...)\ F_{j,i}, (\alpha_2, \beta_2, ...)\ F_{j,i}, ...) D_i
\end{aligned}$$

In the sum example this type reduced to $nat \times nat \to nat$ and in the pflat example we had an identity function of type $\alpha \times \alpha \to \alpha \times \alpha$.

\item There is one fold function $fld_k$ for each of the mutually recursively defined functions. These functions take the results of the reductions and the results of the recursive occurrences to produce the final result. The function $fld_k$ is supplied with these arguments by having a specialized input of type $G_k$. The first $n$ type arguments of $G_k$ are domain types $D_i$ for $1 \leq i\leq n$, followed up by $m$ range types $R_{k_j}$ with $F$ types as inner types. The function produces a result in the range $R_{k}$ as follows:

$$\begin{aligned}
((\alpha_1, \alpha_2, ...)\ D_1, (\beta_1, \beta_2, ...)\ D_2, ..., ((\gamma_{1,1}, \gamma_{2,1}, ...)\ F_{1,k_1}, ...)\ R_{k_1}, ...)\ G_k \to\\ (\gamma_{1,1}, \gamma_{1,2}, ...)\ R_k
\end{aligned}$$

\item We define $n$ mutually recursive functions $grec_{shape_i}$. Each is supplied with $m$ $rdc_{j, i}$ functions, for all $1\leq j \leq m$. We apply these functions by recursively mapping over the inner $F$ types of the $Node$s and applying the reduce functions.

$\begin{aligned}
grec_{shape_i}::\ &((\alpha_1, \alpha_2, ...)\ D_1, (\beta_1, \beta_2, ...)\ D_2, ...)\ shape_i \to\\
&((\alpha_1, \beta_1, ...)\ shape_i, (\alpha_2, \beta_2, ...)\ shape_i, ...) D_i
\end{aligned}\\
grec_{shape_i}\ (Leaf_i\ v) = map_{D_i}\ Leaf_i\ Leaf_i\ ...\ v\\...\\
grec_{shape_i}\ (Node_{i,j}\ f) = map_{D_i}\ Node_{i, j}\ ...\ (rdc_{j, i}\ (map_{F_{j,i}}\ grec_{shape_1}\ ...\ f))$

\item These $grec_{shape}$ functions are invoked by the $grec_{raw_k}$ general recursors. They depend on all $rdc_{j, i}$ and $fld_k$ functions and as expected they are defined mutually recursively.

$\begin{aligned}grec_{raw_k} ::\ &((\alpha_1, \alpha_2, ...)\ D_1, (\beta_1, \beta_2, ...)\ D_2, ...)\ raw_i \to\\
&((\gamma_{1,1}, \gamma_{2,1}, ...)\ shape_i, (\gamma_{1,2}, \gamma_{2,2}, ...)\ shape_i, ...)\ R_k
\end{aligned}\\
grec_{raw_k}\ (In_k\ g) = fld_k\ (map_{G_k}\ grec_{shape_1}\ ...\ (unNodeR_1 \circ grec_{raw_{k_1}})\ ...\ g)$

We define $unNodeR_j$ as the function that maps the destructors of $Node_{j,i}$ to all $n*y$ type arguments of $R_{k_j}$ (each is repeated $y$ times).

\item We can reconstruct the user's function by applying the recursor $grec_{raw_k}$ to the abstraction of $T_k$:

$grec_k :: ((\alpha_1, \alpha_2, ...)\ D_1, (\beta_1, \beta_2, ...)\ D_2, ...)\ T_i \to (\gamma_{1,1}, \gamma_{1,2}, ..., \gamma_{2,1}, \gamma_{2,2})\ R_k\\
grec_k\ t = unLeafR_k\ (grec_{raw_k} (rep_k\ t))$

Similarly as before, $unLeafR_k$ maps the correct $unLeaf$ destructors over the range type $R_k$.

\end{itemize}

\section{Theorems}
\label{grec_thm}

We state that the function $grec_k$ applied to our infrastructure satisfies its specification with Theorem~\ref{thm:grec_thm}:
\begin{equation}\label{thm:grec_thm}
grec_k\ (T_k\ g) = fld_k\ (map_{G_k}\ id\ id\ ...\ (grec_{k_1}\circ map_{T_{k_1}}\ rdc_{1,1}\ rdc_{1,2}\ ...)\ ...\ g)
\end{equation}
For each $1\leq k\leq t$ the function $grec_k$ applied to the constructed type is equal to the $fld_k$ function applied to the results of the recursive calls, where we reduce the arguments first. This theorem can be automatically proven in our infrastructure, provided we have the following four lemmas:
\begin{equation}\label{thm:grec_1}
grec_{shape_i}\ (flat^j_{shape_i}\ s) = map_{D_i}\ flat^j_{shape_i}\ ...\ (grec_{shape_i}\ (map_{Shpae_i}\ rdc_{j,1}\ ...\ s))
\end{equation}
\begin{equation}\label{thm:grec_2}
invar_{shape_i}\ \lst{L}\ s \implies pred_{D_i}\ (invar_{shape_i}\ \lst{L})\ ...\ (grec_{shape_i}\ s)
\end{equation}
\begin{equation}\label{thm:grec_3}\begin{aligned}
invar&_{raw_k}\ \lst{L}\ r \implies\\
&grec_{raw_k}\ (flat^j_{raw_k}\ r) = map_{R_k}\ flat^j_{shape_1}\ ...\ (grec_{raw_k}\ (map_{raw_k}\ rdc_{j,1}\ ...\ r))
\end{aligned}\end{equation}
\begin{equation}\label{thm:grec_4}
invar_{raw_k}\ \lst{L}\ r \implies pred_{R_k}\ (invar_{shape_1}\lst{L})\ ...\ (grec_{raw_k}\ (map_{raw_k}\ rdc_{j,1}\ ...\ 
r))
\end{equation}
In Lemma~\ref{thm:grec_1} we repeat $flat^j_{shape_i}$ for each type parameter of $D_i$, similarly we repeat $(invar_{shape_i}\ \lst{L})$ in Lemma~\ref{thm:grec_2}. The first of these lemmas provides a rewrite rule and the second one asserts the invariant after an application of $grec_{shape_i}$. We can use these two lemmas to prove the same statements for $raw$. In Lemma~\ref{thm:grec_3} we repeat $flat^j_{shape_i}$ for each $shape$ type $y$ times and the same for $(invar_{shape_1}\lst{L})$ in Lemma~\ref{thm:grec_4}.
Lemmas \ref{thm:grec_1} - \ref{thm:grec_4} can be proven automatically and suffice to prove Theorem~\ref{thm:grec_thm} automatically in our infrastructure.

