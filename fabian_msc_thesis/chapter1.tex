% !TeX root = thesis.tex
% !TeX spellcheck = en_US

\chapter{Low-Level Construction}
\label{low_level}

Our construction can be best understood with examples showing each step. In Section~\ref{datastructure} we introduce our infrastructure with a simple power-list example. Then we expand the example to have multiple type arguments (Section~\ref{nr_param}), multiple recursive occurrences (Section~\ref{nr_occ}) and multiple mutually recursive types (Section~\ref{nr_ts}). Finally we present the general construction in Section~\ref{generalized}. We prove non-emptiness of the type by providing witnesses in Section~\ref{wit} and prove additional theorems to integrate our solution into the Isabelle environment in Section~\ref{theorems}.

\section{Construction by Example}
\label{datastructure}

Our running example will be the a power-list already seen in the introduction:

$\alpha\ plist = Nil\ |\ Cons\ \alpha\ ((\alpha \times \alpha)\ plist)$

When we inspect the structure of this type we note that the values stored in the power-list $\alpha~ plist$ are fully balanced tuples of type $\alpha$. These binary trees have height according to their position in the list, e.g. the value of the first element has type $\alpha$, the second~---~$(\alpha, \alpha)$, the third~---~$((\alpha, \alpha), (\alpha, \alpha))$ and so on.

Such changing type arguments are not supported by the current Isabelle $\keyw{datatype}$ command. As we mentioned in the Introduction we can circumvent this limitation by creating a larger abstract supertype within Isabelle's type system.

The main idea of this thesis is to separate the evolution of the type argument from the rest of the type definition. We define a recursive datatype $\alpha\ shape$ handling the change of the type argument.

$\keyw{datatype}\quad \alpha~ shape = Leaf\ \alpha\ |\ Node\ ((\alpha\ shape)\times(\alpha\ shape))$

The $Node$ constructor of $\alpha\ shape$ has recursions according to the change of the argument $\alpha\times\alpha$ of the recursive occurrence in $plist$. We can build trees with $Node$ and $Leaf$ constructors to form all relevant types that occur in a power-list. Now we replace all fields of type $\alpha$ in $plist$ with our new $\alpha\ shape$ type. Hence we can replace the non-uniform recursive occurrence with a normal, \emph{uniform} one. We call this new outer type $raw$ and obtain the following definition:

$\keyw{datatype}\quad \alpha~ raw = In\ (unit + (\alpha\ shape) \times (\alpha\ raw))$

To simplify reasoning we write $raw$ in BNF-fashion with a singular constructor $In$. As mentioned the recursive occurrence of $raw$ has no change of its type arguments since the change is fully handled by $\alpha\ shape$.

\subsubsection{Invariant}

In this example $\alpha~ raw$ is \emph{not} isomorphic to $\alpha~ plist$ -- the value of the first element of the list defined by $\alpha\ raw$ can have the type $(\alpha, (\alpha, \alpha))$ -- built with $\alpha\ shape$s -- clearly incorrect for a power-list. Accordingly, valid instances of $\alpha\ raw$ must restrict the trees of $\alpha\ shape$s, such that they are balanced and of depth equal to the recursion depth. We define $invar_{raw}$ as the invariant depending on $invar_{shape}$, which checks whether the shapes form a balanced tree of a certain depth.

$invar_{shape} :: nat \to \alpha\ shape \to bool\\
invar_{shape}\ 0\ (Leaf\ \_) = True\\
invar_{shape}\ n\ (Node\ (s_1, s_2)) = invar_{shape}\ (n-1)\ s_1 \wedge invar_{shape}\ (n-1)\ s_2\\
invar_{shape}\ \_\ \_ = False$

And we define $invar_{raw}$ to be $true$ when the height of each $shape$ tree matches the current recursion depth.

$invar_{raw} :: nat \to \alpha\ raw \to bool\\
invar_{raw}\ \_\ (In\ (In_L\ ())) = True\\
invar_{raw}\ n\ (In\ (In_R\ (s, r))) = invar_{shape}\ n\ s\wedge invar_{raw}\ (n+1)\ r$

Here $In_L$ and $In_R$ are the constructors of the Functor $+$ as discussed in Chapter~\ref{prelim}. We use them here to do the equivalent of matching the $Nil$ and $Cons$ constructors of $plist$. There exists a bijection from power-lists to instances of $\alpha\ raw$ satisfying the invariant $invar_{raw}\ 0$.

\subsubsection{Type Definition}
\label{fold}

We define the type $\alpha\ plist$ as the subset of $\alpha\ raw$ where the invariant $invar_{raw}\ 0$ holds, e.g. $\alpha\ plist = \{r::\alpha\ raw\ |\ invar_{raw}\ 0\ r\}$. By \cite{biendarra2015} we must prove the closed-ness of the $map_{raw}$ function in order to lift $\alpha\ raw$. The idea of the proof is that the invariant argues about the structure and the map function does not change the structure. 

Henceforth we will use the function $abs_{plist}::\alpha\ raw \to \alpha\ plist$ to abstract our type and its inverse, $rep_{plist} :: \alpha\ plist \to \alpha\ raw$, to get back the representation. We can reconstruct the original constructors of $\alpha\ plist$ for $\alpha\ raw$. 

 $Nil = abs_{plist} (In (In_L ()))\\
Cons\ x\ xs = abs_{plist} (In (In_R (Leaf\ x, flat_{raw}\ (rep_{plist}\ xs))))$

It is worth pointing out that the constructor $Cons$ has type $\alpha \to (\alpha\times \alpha)\ plist \to \alpha~plist$. To transform its second argument into the usual internal representation with $\alpha\ shape$s we define the function $flat_{raw}$, which replaces the tuples in the list with $Node$s.

$flat_{raw} :: (\alpha\times \alpha)\ raw \to \alpha\ raw\\
flat_{raw}\ (In\ (In_L\ ())) = In\ (In_L\ ())\\
flat_{raw}\ (In\ (In_R\ (s, r))) = In\ (In_R\ (flat_{shape}\ s, flat_{raw}\ r))$

$flat_{shape} :: (\alpha\times \alpha)\ shape \to \alpha\ shape\\
flat_{shape}\ (Leaf\ (v_1, v_2)) = Node\ (Leaf\ v_1, Leaf\ v_2)\\
flat_{shape}\ (Node\ (s_1, s_2)) = Node\ (flat_{shape}\ s_1, flat_{shape}\ s_2)$

This flat operation will not produce an $\alpha\ raw$ satisfying the invariant $invar_{raw}\ 0$, but one satisfying $invar_{raw}\ 1$. This is exactly what we require, since the $Cons$ constructor itself provides the first element.

We also define destructor functions as the inverse of the constructors. Similarly to before the inverse of $flat$ is used to $unflat$ the tail of the input list $\alpha\ raw$ into an $(\alpha\times\alpha)\ raw$ list. We define them in great detail in Section~\ref{generalized}.

\subsection{Number of Type Arguments}
\label{nr_param}

Up to now our infrastructure can only support one type argument. We demonstrate the changes to our infrastructure for multiple arguments on another example $plist_2$. This list has two values of different types per element and both types can change individually along the recursion:

$(\alpha, \beta)\ plist_2 = Nil\ |\ Cons\ \alpha\ \beta\ ((\alpha \times \beta, \alpha + \beta)\ plist_2)$

In particular, not both type arguments have to change in the same way and they can depend on all available types. Observing the types of the first few values of the list yields the following:

\begin{itemize}
\item $\alpha$ and $\beta$
\item $\alpha \times \beta$ and $\alpha + \beta$
\item $(\alpha \times \beta) \times (\alpha + \beta)$ and $(\alpha \times \beta) + (\alpha + \beta)$
\end{itemize}

%\\((\alpha * \beta) * (\alpha + \beta)) * ((\alpha * \beta) + (\alpha + \beta)) \text{ and } ((\alpha * \beta) * (\alpha + \beta)) + ((\alpha * \beta) + (\alpha + \beta))

Note that the first type argument depends on the second and vice versa. This behavior can be modeled in Isabelle with two mutually recursively defined datatypes $shape_A$ and $shape_B$:

$(\alpha, \beta)\ shape_A = Leaf\ \alpha\ |\ Node\ (((\alpha, \beta)\ shape_A) \times ((\alpha, \beta)\ shape_B))
\\(\alpha, \beta)\ shape_B = Leaf\ \beta\ |\ Node\ (((\alpha, \beta)\ shape_A) + ((\alpha, \beta)\ shape_B))$

$
(\alpha, \beta)\ raw = In\ (unit + ((\alpha, \beta)\ shape_A) \times ((\alpha, \beta)\ shape_B) \times ((\alpha, \beta)\ raw))$

By allowing the shapes to recurse mutually they are sufficient to create all the types required for $plist_2$. The type Raw has no significant changes.

\paragraph{Invariant and Constructors}

We can easily extend the invariant and fold functions for the constructors to handle multiple type arguments. The new invariant will be two mutually recursive functions called $invar_{shape_A}$ and $invar_{shape_B}$, each can validate one $shape$ type. The fold functions can be adapted in a similar way. We omit their specifications here and present their generalized form in Section~\ref{generalized}.

\subsection{Number of Recursive Occurrences}
\label{nr_occ}

The examples processed so far had only one recursive occurrence. In this subsection we introduce a new example $plist_3$, with multiple such occurrences, and adapt our infrastructure to handle it. This is entirely compatible with the changes introduced previously for $plist_2$.

$\alpha\ plist_3 = Nil\ |\ Cons_1\ \alpha\ ((\alpha \times \alpha)\ plist_3)\ |\ Cons_2\ \alpha\ ((\alpha + \alpha)\ plist_3)$

This list features two $Cons$ constructors, both with one recursive occurrence with a different recursive type argument each. The user can determine the evolution of the type arguments on each step by choosing to either use $Cons_1$ or $Cons_2$ to continue the list. For example the type

$((\alpha + \alpha) \times (\alpha + \alpha)) \times ((\alpha + \alpha) \times (\alpha + \alpha))$

can occur in $plist_3$ by using the constructor sequence $\langle Cons_2, Cons_1, Cons_1\rangle$. It is worth pointing out that the type evolves in the same way on one `level' of the type tree. We adapt our infrastructure as follows:

$\begin{aligned}
\alpha\ shape =\ &Leaf\ \alpha
\\ |\ &Node_1\ ((\alpha\ shape) \times (\alpha\ shape))
\\ |\ &Node_2\ ((\alpha\ shape) + (\alpha\ shape))
\end{aligned}$

$\alpha~ raw = In (unit + (\alpha\ shape) \times (\alpha\ raw) + (\alpha\ shape) \times (\alpha\ raw))$

This $shape$ datatype allows us to choose which way the argument type evolves and all types required for $plist_3$ can be built. The important thing to note is that it allows us to construct more types than there can be in $plist_3$. For example this fully balanced type 

$((\alpha + \alpha) \times (\alpha \times \alpha)) \times ((\alpha + \alpha) + (\alpha + \alpha))$

satisfying the current invariant can be formed, despite it being impossible to be constructed with any sequence of $Cons_1$ and $Cons_2$ constructors. 

\paragraph{Invariant}

We adapt the invariant by replacing the integer depth with a list of labels $\lst{L}$ which specifies the occurrence we recursed into. Let $label$ be a datatype with the constructors $L_1$ and $L_2$, indicating a recursion into the recursive occurrence of $Cons_1$ or $Cons_2$ respectively. $invar_{shape}$ validates that a tree of $shape$ types has the structure specified by a given list of labels by pattern-matching the label to the used $Node$ constructor. We require two equations in the $L_2$ case on a technicality (the $+$ Functor does not have a single constructor):

$invar_{shape} :: label\ list \to \alpha\ shape \to bool\\
invar_{shape}\ []\ (Leaf\ a) = True\\
invar_{shape}\ (L_1 : \lst{L})\ (Node_1\ (s_1, s_2)) = (invar_{shape}\ \lst{L}\ s_1) \wedge (invar_{shape}\ \lst{L}\ s_2)\\
invar_{shape}\ (L_2 : \lst{L})\ (Node_2\ (In_L\ s_1)) = invar_{shape}\ \lst{L}\ s_1\\
invar_{shape}\ (L_2 : \lst{L})\ (Node_2\ (In_R\ s_2)) = invar_{shape}\ \lst{L}\ s_2\\
invar_{shape}\ \_\ \_ = False$

$invar_{raw}$ validates the structure by invoking $invar_{shape}$ on each level of recursion. The label list is extended whenever we recurse into an occurrence by appending the corresponding label to the list, starting with an empty list.

$invar_{raw} :: label\ list \to \alpha\ raw \to bool\\
invar_{raw} \lst{L} (In (In_L ())) = True\\
invar_{raw}\ \lst{L} (In (In_R (In_L (s, r)))) = (invar_{shape}\ \lst{L}\ s) \wedge (invar_{raw}\ (L_1 : \lst{L})\ r)\\
invar_{raw}\ \lst{L} (In (In_R (In_R (s, r)))) = (invar_{shape}\ \lst{L}\ s) \wedge (invar_{raw}\ (L_2 : \lst{L})\ r)$

There are three equations in this example, one for $Nil$ and two for the Constructors $Cons_1$ and  $Cons_2$. At each recursion we append a different label to the list of labels.

\paragraph{Constructors}

We require two $flat$ functions to define the constructors of $plist_3$ within our infrastructure. For $Cons_1$ we define $flat_{raw}^1 :: (\alpha\times \alpha)\ raw \to \alpha\ raw$ and for $Cons_2$ we have $flat_{raw}^2 :: (\alpha+ \alpha)\ raw \to \alpha\ raw$. These functions still have the same behavior and will require two different underlying $flat_{shape}$ functions. We omit the exact definition here in favor of a general definition in Section~\ref{generalized}.

\subsection{Number of Mutual Types}
\label{nr_ts}

Our construction also supports mutually recursive datatypes. We naturally apply the principles shown in the previous Section since there are always multiple recursive occurrences in mutually recursive definitions. The example studied in this Section are two mutually recursive lists $plist_{4a}$ and $plist_{4b}$ defined as

$\alpha\ plist_{4a} = Nil\ |\ Cons\ \alpha\ ((\alpha \times \alpha)\ plist_{4b})\\
\alpha\ plist_{4b} = Cons\ \alpha\ ((\alpha + \alpha)\ plist_{4a})$

Here $\alpha~ plist_{4a}$ is a list of even length, where the type argument alternates between two different changes. The list is of even length since it can only end in the Nil~constructor of $plist_{4a}$. Both types depend on each other, so they are mutually recursive and there are two recursive occurrences, one for each type.

In this case our construction consists of two $raw$ datatypes -- representing the two types $plist_{4a}$ and $plist_{4b}$:

$\begin{aligned}
\alpha\ shape =\ &Leaf\ \alpha
\\ |\ &Node_1\ ((\alpha\ shape) \times (\alpha\ shape))
\\ |\ &Node_2\ ((\alpha\ shape) + (\alpha\ shape))
\end{aligned}$

$\alpha\ raw_1 = In\ (unit + (\alpha\ shape) \times (\alpha\ raw_2) )\\
\alpha\ raw_2 = In\ ((\alpha\ shape) \times (\alpha\ raw_1))$

The $shape$ types behave the same as in the previous section with multiple occurrences, they even represent the same change of arguments. The two $raw$ datatypes hold no surprises -- we observe that $raw_1$ recurses into $raw_2$ and vice versa, as in $plist$'s definition.

\paragraph{Invariant and Constructors}

$invar_{shape}$ remains unchanged from the previous Section, but we require two $invar_{raw}$ functions to handle both $raw$ types. They recurse as you would expect -- $invar_{raw_1}$ into $invar_{raw_2}$ and vice versa. The constructor definition also depends the same two $flat$ functions as before, this becomes apparent in the next Section where we present the general case.

\section{Generalized Construction}
\label{generalized}

\subsection{The Generalized Datatype}
\label{generalized_datatype}

The generalization of our infrastructure is presented in this Section. Let us consider a specification of a mutually recursive non-uniform datatype with $n$ type arguments, $t$ mutual types with totally $m$ recursive occurrences, distributed among the $t$ types such that the $k$-th mutual type occurs $m_k$ times, satisfying $\sum_{k=1}^t m_k = m$. The specification consists of the following:

\begin{itemize}
\item The extracted changes of the arguments from the user's specification. We define $F_{j, i}$ as the change of the $i$-th argument of the $j$-th recursive occurrence. Hence $F$ is a Matrix of $n$-ary BNFs of dimension $m\times n$. In example $plist_2$ we had two changes $F_{1, 1}$ and $F_{1,2}$ distributed over two type arguments, and in example $plist_3$ were two changes $F_{1,1}$ and $F_{2,1}$ distributed over two recursive occurrences:
\begin{description}
\item[$plist_2$:] $(\alpha, \beta)\ F_{1, 1} = \alpha \times \beta \text{ and } (\alpha, \beta)\ F_{1,2} = \alpha + \beta$

\item[$plist_3$:] $\alpha\ F_{1, 1} = \alpha \times \alpha \text{ and } \alpha\ F_{2,1} = \alpha + \alpha$
\end{description}

\item For each mutually defined datatype we also extract the non-recursive structure in a vector of BNFs called $G$. $G$ has length $t$ and each $G_k$ represents the constructors of the $k$-th type. As we have done in the previous Sections each recursive occurrence is substituted with a new type argument $\tau$, $\sigma$, etc.. Thus, each $G_k$ has $n+m$ type arguments divided into two parts: first $n$ for the type arguments of the user's type, followed by $m$ for each recursive occurrence. In example $plist_{4}$ we have two $G$s:

\begin{description}
\item[$plist_{4}$:]
$\begin{aligned}
\alpha\ \tau\ \sigma\ G_1 = &unit + \alpha \times \sigma\\
\alpha\ \tau\ \sigma\ G_2 = &\alpha * \tau
\end{aligned}$
\end{description}

We order the secondary type arguments according to the type they represent, e.g. $\tau$ is a recursion into $plist_{4a}$ which comes before the argument(s) representing $plist_{4b}$. In general we first have $m_k$ arguments representing recursions into the $k$-th type.

\end{itemize}
\begin{remark}
The parsing and extraction of $F$ and $G$ from a type declaration was not part of this thesis, rather a matrix $F$ and vector $G$ are the input of the program.
\end{remark}

We can define our infrastructure using this specification. It consists of the following:


\begin{itemize}
\item We define $n$ mutual $shape$ types, one for each type argument. We rewrite the $shape$ datatypes defined previously utilizing the $F$ BNFs. Each $shape_i$ has one $Leaf$ constructor $Leaf_i$ which instantiates the $i$-th type argument and $m$ Node constructors $Node_{i,j}$, having $F_{j,i}$ instantiated with shapes as type arguments, as single constructor argument. The $shape$ type definition ends up looking as follows:

$$\begin{aligned}
\keyw{datatype}\quad (\alpha_1, \alpha_2, ...)\ shape_1 =&\ ...\\
\keyw{and}\quad (\alpha_1, \alpha_2, ...)\ shape_i =&\ Leaf_i\ \alpha_i\\
&\ ...\\
|&\ Node_{i, j}\ (((\alpha_1, \alpha_2, ...)\ shape_1,\\
&\quad\quad\quad\ \ \quad(\alpha_1, \alpha_2, ...)\ shape_2, ...)\ F_{j, i})\\
&\ ...
\end{aligned}$$

For each constructor defined we denote the corresponding destructor as $unLeaf_i$, respectively $unNode_{i,j}$.

\item There are $t$ raw types, each with $n$ type arguments and a singular constructor $In_k$, for $k\in\{1..t\}$. Each constructor $In_k$ has one type argument, the corresponding $G_k$ with its arguments instantiated with $n$ $shape$ types followed by $m$ raw types, where $raw_k$ is repeated $m_k$ times to fill all $m$ arguments.

$$\begin{aligned}
\keyw{datatype}\ (\alpha_1, \alpha_2, ...)\ raw_k =In_k&\ (((\alpha_1, \alpha_2, ...)\ shape_1, (\alpha_1, \alpha_2, ...)\ shape_2, ...,\\
&\ (\alpha_1, \alpha_2, ...)\ raw_{k_1}, (\alpha_1, \alpha_2, ...)\ raw_{k_2}, ...)\ G_k)
\end{aligned}$$

Where the indexing function $k_l = max\left\{i | \sum_{j=1}^{i-1}{m_j} < l\right\}$ maps the index of the recursive occurrence to the associated raw type.

For each $raw_k$ type we define its destructor as $Out_k$.

\end{itemize}

\subsection{The Invariant}
\label{invariant}

As you might expect there are $t$ invariants $invar_{raw_k}$. They restrict the $shape$ trees to the growth-pattern of the non-uniform datatype. This is done by invoking the $invar_{shape_i}$ functions with a depth condition represented by a list of labels (as seen in Section~\ref{nr_occ}). Let $label$ be the following datatype:

$
\keyw{datatype}\quad label =L_1\ |\ L_2\ |\ ...\ |\ L_m
$

The functions $invar_{shape}$ are $n$-ary mutually recursive. Each takes a label list and a $shape$ and returns a boolean indicating the well-formedness of the $shape$ tree. It does so by pattern-matching the head-label $L_j$ with the $Node_{i, j}$ constructor before recursing into the $Node$s. Once it arrives at a $Leaf$ and the label list is empty the function accepts; if the list is not empty or a label on the way does not match the $Node$ the function rejects. We make use the predicators $pred_{F_{j,i}}$ seen in Chapter~\ref{prelim} to recurse through the $F$ BNFs. 

$invar_{shape_i} :: label\ list \to shape_i \to bool\\
invar_{shape_i}\ []\ (Leaf_i~a) = True\\
invar_{shape_i}\ (L_1 : \lst{L})\ (Node_{i,1}\ f) = pred_{F_{1, i}}\ (invar_{shape_1}\ \lst{L})\ (invar_{shape_2}\ \lst{L})\ ...\ f\\
invar_{shape_i}\ (L_2 : \lst{L})\ (Node_{i,2}\ f) = pred_{F_{2, i}}\ (invar_{shape_1}\ \lst{L})\ (invar_{shape_2}\ \lst{L})\ ...\ f\\
...\\
invar_{shape_i}\ \_\ \_ = False$

The $invar_{raw}$ functions maintain the current path through the recursive occurrences as a list of labels. Using $pred_{G_k}$ we can apply the $invar_{shape}$ functions to the $shape$ trees and recurse into the recursive occurrences with $invar_{raw_{k_j}}$ while appending the associated label $L_j$ to the label list $\lst{L}$.

$\ invar_{raw_k} :: label\ list \to raw_k \to bool\\
\begin{aligned}
invar_{raw_k}\ \lst{L}\ (In\ g) = pred_{G_k}\ &(invar_{shape_1}\ \lst{L})\ (invar_{shape_2}\ \lst{L})\ ...\\
&(invar_{raw_{k_1}}\ (L_1 : \lst{L}))\ (invar_{raw_{k_2}}\ (L_2 : \lst{L}))\ ...\ g
\end{aligned}$

Here we have the same index-mapping function $k_l$ as defined in Section~\ref{generalized}.

\subsection{Type Definition}
\label{abstraction}

As done for the examples we define the non-uniform types as
$$(\alpha_1, \alpha_2, ...)\ T_k = \left\{r :: (\alpha_1, \alpha_2, ...)\ raw_k\ |\ invar_{raw_k}\ []\ r\right\}$$
for $1\leq k\leq t$. We obtain the abstracting functions $abs_k :: (\alpha_1, \alpha_2, ...)\ raw_k \to (\alpha_1, \alpha_2, ...)\ T_k$ and their inverses, $rep_k :: (\alpha_1, \alpha_2, ...)\ T_k \to (\alpha_1, \alpha_2, ...)\ raw_k$.

\paragraph{Constructors}

For simplicity of notation we use the same letter $T$ for the constructors of our types. A constructor $T_k$ has the type

$(\alpha_1, \alpha_2, ..., ((\alpha_1, \alpha_2, ...)\ F_{1,1}, (\alpha_1, \alpha_2, ...)\ F_{1,2},...)\ T_{k_1}, (...) T_{k_2}, ...)\ G_k \to (\alpha_1, \alpha_2, ...)\ T$

Note that the arguments of type $((\alpha_1, \alpha_2, ...)\ F_{1,1}, (\alpha_1, \alpha_2, ...)\ F_{1,2},...)\ T_{k_1}$ need to be flattened to fit into the infrastructure. The constructor takes the representations of the recursive arguments and flattens them, then uses the $In$ constructor to form a $raw$ and returns its abstraction to the user:

$T_k\ g = abs_{T_k}\ (In_k\ (map_{G_k}\ Leaf_1\ ...\ (flat^1_{raw_{k_1}} \circ rep_{k_1})\ (flat^2_{raw_{k_2}} \circ rep_{k_2})\ ...\ g))$

We define $t * m$ $flat^j_{raw_k}$ functions for $1\leq k \leq t$ and $1 \leq j \leq m$ where the functions for a fixed $j$ are mutually recursive. The $flat_{raw}$ functions invoke $flat_{shape}$ functions on each $shape$ tree:

$flat^j_{raw_k}:: ((\alpha_1, \alpha_2, ...)\ F_{j, 1}, (\alpha_1, \alpha_2, ...)\ F_{j, 2}, ...)\ raw_k \to (\alpha_1, \alpha_2, ...)\ raw_k\\
flat^j_{raw_k}\ (In_k g) = In_k\ (map_{G_k}\ flat^j_{shape_1}\ flat^j_{shape_2}\ ...\ flat^j_{raw_{k_1}}\ flat^j_{raw_{k_2}}\ ...\ g)$

And each function $flat^j_{shape_i}$ unpacks the $F_{j,i}$ of a $Leaf_i$ into the corresponding $Node_{i,j}$:

$flat^j_{shape_i}:: ((\alpha_1, \alpha_2, ...)\ F_{j, 1}, (\alpha_1, \alpha_2, ...)\ F_{j, 2}, ...)\ shape_i \to (\alpha_1, \alpha_2, ...)\ shape_i\\
flat^j_{shape_i}\ (Leaf_i\ f) = Node_{i, j}\ (map_{F_{j,i}}\ Leaf_1\ Leaf_2\ ...\ f)$

And recurses when it encounters a $Node_{i, j'}$ constructor for $1\leq j'\leq m$. For this the $flat_{shape}$ functions are defined mutually recursive:

$flat^j_{shape_i}\ (Node_{i,j'}\ f') = Node_{i,j'}\ (map_{F_{j',i}}\ flat^j_{shape_1}\ flat^j_{shape_2}\ ...\ f'))$

A flattened $raw_k$ satisfies the invariant $invar_{raw_k}\ [L_j]$ and can thus be embedded into a valid surrounding $raw$.

\paragraph{Destructors}

The created constructors help the user getting an instance of the created non-uniform type. The user also requires ways to read the fields of the type with destructors. For each constructor we define its inverse $T^{-1}_k$ as the function that takes the representation of the input, unpacks it and returns the $unLeaf$ed values and the unflattened recursive occurrences to the user:

$T^{-1}_k\ t =  map_{G_k}\ unLeaf_1\ ...\ (abs_{k_1} \circ unflat^1_{raw_{k_1}}\ [])\ ...\ (Out_k\ (rep_k\ t))$

As one might expect, the $unflat_{raw}$ functions are the inverse functions of the $flat$ functions. They maintain a $label$-list indicating on which level the current $shape$ trees have to be unflattened. This $label$-list is given to the $unflat_{shape}$ functions which do the actual transformation:

$unflat^j_{raw_k} :: label\ list \to (\alpha_1, \alpha_2, ...)\ raw_k \to ((\alpha_1, \alpha_2, ...)\ F_{j, 1}, ...)\ raw_k\\
unflat^j_{raw_k}\ \lst{L}\ (In_k g) = In_k\ (map_{G_k}\ (unflat^j_{shape_1}\ \lst{L})\ ...\ (unflat^j_{raw_{k_1}}\ (L_1:\lst{L}))\ ...\ g)$

$unflat^j_{shape_i} :: label\ list \to (\alpha_1, \alpha_2, ...)\ shape_i \to ((\alpha_1, \alpha_2, ...)\ F_{j, 1}, ...)\ shape_i\\
unflat^j_{shape_i}\ []\ (Node_{i, j}\ f) = Leaf_i\ (map_{F_{j,i}}\ unLeaf_1\ unLeaf_2\ ...\ f)\\
unflat^j_{shape_i}\ (\_:\lst{L})\ (Node_{i, j'}\ f') = Node_{i,j'}\ (map_{F_{j',i}}\ (unflat^j_{shape_1}\ \lst{L})\ ...\ f')$

The $unflat_{shape}$ functions do not match for $Leaf$ constructors -- when they reach one it's already too late to unflat and the function will abort. When the function $unflat^j_{shape_i}$ reaches a $Node_i,j$ and the $label$-list is empty it will create a $Leaf$ on that level, otherwise it will recurse into any $Node_{i, j'}$ for all $1\leq j'\leq m$ until the list is empty.

The unflattened structure satisfies the invariant $invar_{raw_k}\ []$ and can be abstracted and returned to the user.

\section{Witnesses}
\label{wit}


Non-emptiness witnesses prove that there exists at least one instance of raw satisfying the invariant. In our infrastructure witnesses of the $raw$ types depend on the witnesses of the $G$ BNFs and $shape$ types, which themselves depend on the witnesses of the $F$ BNFs. The construction of witnesses is non-trivial since we want witnesses that satisfy the invariant, e.g. the witnesses of the $shape$ types have to be of certain depth according to the invariant.

Witnesses can be partially ordered depending on the arguments they take (see \cite{blanchette2015witnessing} for a deeper theory on witnesses), e.g. a witness that takes one argument is `worse' than a witness that requires none. A witness can be identified by the set of arguments $wit \subset \{1..n\}$ it takes. A witness $wit$ is minimal if there is no other witness that takes a subset of its arguments, e.g. $\forall wit'.\ wit'\not\subset wit$, and the set of all of these minimal witnesses for a datatype $F$ are simply called the witnesses $I_F$ of that datatype.

To get the witnesses of raw satisfying the invariant we require $shape$ witnesses which satisfy the invariant $invar_{shape_i}\ (L_j:\lst{L})$. Therefore we use the $Node_{i,j}$ constructor which as per definition has a single field with $F_{j, i}$ type (remember that the type arguments of $F_{j,i}$ are $shape$ types). To instantiate the witnesses in $I_{F_{j,i}}$ we supply them with witnesses of $shape$ types which satisfy $invar_{shape_{i'}}\ \lst{L}$. We can recursively find those witnesses and combine them to get witnesses for $F_{j, i}$. The $Leaf_i$ constructor provides a witness that satisfies $invar_{shape_i}\ []$. Finally we instantiate our $Node_i$ constructor to get witnesses for $shape_i$ which satisfy the invariant. Of the generated witnesses we only take the minimal ones. This process can be written as the recursive function below:

$wits_{shape_i} :: label\ list \to nat\ list\ list\\
wits_{shape_i}\ [] = \left[\left[i\right]\right]\\
wits_{shape_i}\ (L_j:\lst{L}) = minimize\ \left(\bigcup\limits_{wit \in I_{F_{j,i}}} \left(combine\ (map\ (\lambda i.\ wits_{shape_i}\ \lst{L})\ wit)\right)\right)$

Where $combine$ computes a set of all combinations of witnesses and $minimize$ filters for all the minimal witnesses.

In comparison, generating the witnesses for $raw$ becomes trivial with the help of the $wits_{shape}$ function and is omitted. An implementation of this process can be found in the repository under $\const{/wits.thy}$.

\section{Theorems}
\label{theorems}

To strengthen the integrity of out infrastructure we formally prove the bijectivity of constructor and destructor, and the behavior of the characteristic functions $map$, $set$ and $rel$ on the non-uniform datatype. All lemmas and theorems presented in this Section are generated and proven automatically for a non-uniform datatype.

\subsection{Bijection of the Constructor}

We assert the bijectivity of the constructor and destructor by formally proving the theorems for all $1\leq k \leq t$:
\begin{equation}\label{thm:bijection}
T_k\ (T_k^{-1}\ t) = t \text{ and } T_k^{-1}\ (T_k\ g) = g\text{.}
\end{equation}
They can easily be shown after we prove the following auxiliary lemmas. We present them for the $plist$ example as the general lemmas are too big to fit this thesis:
\begin{equation}\label{thm:shape1}
\forall \lst{L}.\ invar_{shape}~(snoc~L_1~\lst{L})~u \implies flat_{shape}~(unflat_{shape}~\lst{L}~u) = u
\end{equation}
\begin{equation}\label{thm:shape2}
\forall \lst{L}.\ invar_{shape}~\lst{L}~u \implies unflat_{shape}~\lst{L}~(flat_{shape}~u) = u
\end{equation}
\begin{equation}\label{thm:shape3}
\forall \lst{L}.\ invar_{shape}~\lst{L}~u \implies invar_{shape}~(snoc~L_1~\lst{L})~(flat_{shape}~u)
\end{equation}
\begin{equation}\label{thm:shape4}
\forall \lst{L}.\ invar_{shape}~(snoc~L_1~\lst{L})~u \implies invar_{shape}~\lst{L}~(unflat_{shape}~\lst{L}~u)
\end{equation}
With the $snoc$ function, an operation that appends an element to the end of a list, these lemmas state the following:
\ref{thm:shape1} and \ref{thm:shape2} combined state that the $unflat_{shape}$ function is the inverse of $flat_{shape}$. Provided that the input satisfies the invariant $invar_{shape}\ \lst{L}$, \ref{thm:shape3} states that the result of the $flat$ operation will satisfy $invar_{shape}\ (snoc\ \lst{L}\ L_1)$ and \ref{thm:shape4} has the assumption and assertion swapped for the $unflat$ function. These lemmas can easily be discharged with induction proves over the $shape$ type. In fact, they are proven automatically via ML tactics in our infrastructure (see \cite{NipkowPaulsonWenzel2002LNCS} for a reference on ML tactics).

We continue by stating the same facts about the $raw$ type:
\begin{equation}\label{thm:raw1}
\forall \lst{L}.\ invar~(snoc~L_1~\lst{L})~t \implies flat~(unflat~\lst{L}~t) = t
\end{equation}
\begin{equation}\label{thm:raw2}
\forall \lst{L}.\ invar~\lst{L}~t \implies unflat~\lst{L}~(flat~t) = t
\end{equation}
\begin{equation}\label{thm:raw3}
\forall \lst{L}.\ invar~\lst{L}~t \implies invar~(snoc~F~\lst{L})~(flat~t)
\end{equation}
\begin{equation}\label{thm:raw4}
\forall \lst{L}.\ invar~(snoc~L_1~\lst{L})~t \implies invar~\lst{L}~(unflat~\lst{L}~t)
\end{equation}
Again, these lemmas are automatically proven in our infrastructure with induction proves over raw and lemmas \ref{thm:shape1} - \ref{thm:shape4}. Using lemmas \ref{thm:raw1} - \ref{thm:raw4} the prover verifies the Theorems \ref{thm:bijection}.

\subsection{Characteristic Theorems}

Furthermore, we prove that the characteristic functions do the same whether applied to the abstract datatype or our construction underneath. We again show the relevant lemmas and theorems for the $plist$ example.

\paragraph{Map}

Theorem \ref{thm:map1} states that it doesn't matter whether you apply the constructor $T$ before or after you apply $map$. 
\begin{equation}\label{thm:map1}
\forall f.\ map_T~f~(T~g) = T~(map_G\ f\ (map_T~(map_F~f)) g)
\end{equation}
The automatic proof assistant can verify this theorem when supplied with the following two lemmas about the $shape$ and $raw$ type:
\begin{equation}\label{thm:map2}
\forall f.\ map_{shape}\ f\ (flat_{shape}\ u) = flat_{shape}\ (map_{shape}\ (map_F\ f)\ u)
\end{equation}
\begin{equation}\label{thm:map3}
\forall f.\ map_{raw}\ f\ (flat_{raw}\ r) = flat_{raw}\ (map_{raw}\ (map_F\ f)\ r)
\end{equation}
Lemmas \ref{thm:map2} and \ref{thm:map3} can both be proven with induction proves over the $shape$ type, respectively the $raw$ type.

\paragraph{Set and Rel}

Similarly, we prove that the $set$ operation has the same result whether we apply it to the constructed type or to the parts used to construct the type:
\begin{equation*}
set_{\alpha,T}\ (T\ g) = set_{\alpha, G}\ g \cup \left(\bigcup_{f\in F_{set}(g)}set_{\alpha, F}\ f\right)\text{ where }F_{set}(g) = \bigcup_{t\in (set_{\tau, G}\ g)}set_{\alpha, T}\ t
\end{equation*}
It is irrelevant for $rel$ whether we apply the constructor before comparing two instances:
\begin{equation*}
\forall R.\ rel_T\ R\ (T\ g)\ (T\ g') = rel_G\ R\ (rel_T\ (rel_F\ R))\ g\ g'
\end{equation*}
Both theorems can be proven automatically by first establishing the relation between the $set$/$rel$ function and $flat$ for the $shape$ and $raw$ types as seen for the $map$ function.
