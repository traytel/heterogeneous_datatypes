% !TeX root = thesis.tex
% !TeX spellcheck = en_US

\chapter{Introduction}
\label{intro}

Non-uniform datatypes (also known as nested or heterogeneous datatypes) are recursively defined types in which the type arguments of the recursive occurrences may vary. The `hello world!' example of non-uniform datatypes is the type of power-lists: lists in which the first entry contains one element, the second -- two, the third -- four, the fourth -- eight, and so on (all arranged in fully balanced tuples). In Isabelle \cite{NipkowPaulsonWenzel2002LNCS} syntax:

\begin{equation}\label{intro_ex}
\keyw{datatype}\quad \alpha\ PList = Nil\ |\ Cons\ \alpha\ ((\alpha\times\alpha)\ PList)
\end{equation}

Non-uniform datatypes have been profitably used to perform tricky performance optimizations. Some prominent examples include the bootstrapping and implicit recursive slowdown techniques introduced by Okazaki \cite{Okasaki} and the purely functional data structure of finger trees by Hinze and Paterson \cite{HinzeP06}.

\paragraph{Non-Uniform Datatypes}

Non-uniform datatypes cannot be defined normally with Isabelle's $\keyw{datatype}$ command and it must reject such declarations as Equation~\ref{intro_ex}. Obtaining a type according to a non-uniform specification requires a considerable amount of manual work in the current Isabelle environment. We aim to add a new command $\keyw{nu\text{-}datatype}$ to Isabelle which simplifies the construction of non-uniform datatypes.

We generate an abstract supertype in the Isabelle environment with an internal structure similar to the non-uniform specification. This type is then lifted with an invariant restricting it to a type isomorph to the non-uniform type. Our construction is parametrized with the number of type arguments, number of recursive occurrences and it supports mutually recursive datatypes. We assert the integrity of our construction by proving various Lemmas on it automatically. The user is presented with an abstraction of the constructed type with suitable constructors and destructors.

\paragraph{Recursive Functions}

Bird and Paterson \cite{BirdP99} have studied the question of how to define recursive functions on non-uniform datatypes in a generic fashion. The traditional fold operators, which are commonly used for recursive datatypes, are not expressive enough -- they fail to define even simple functions like summing up the elements of a power-list. Bird and Paterson's solution are \emph{generalized folds} -- an extension of the usual fold-combinators with a further polymorphic parameter, which accounts for the change of the type argument.

We generate such generalized fold combinators and show how a user-defined recursive function on a non-uniform datatype is applied to our abstraction. We split the user's function into several functions, some handling the change of the type arguments and others the general function. Then we combine them and formally prove that the created function is isomorph to the original specification.

This thesis is set up as follows: first we briefly introduce Isabelle's type infrastructure and bounded natural Functors (abbreviated with BNF) in Chapter~\ref{prelim}. In Chapter~\ref{low_level} we present our internal construction to create non-uniform datatypes and show how recursive equations on these datatypes can be defined in Chapter~\ref{gfold}. Chapter~\ref{conclusion} concludes this thesis.

\section{Contributions}
\label{contrib}

The contributions of this thesis are:
\begin{itemize}
\item An implementation of low-level constructions for nonuniform datatypes in Isabelle. The internal infrastructure consists of several types and functions. Lemmas supporting this infrastructure are generated as terms, including Lemmas which prove the bijectivity of the constructor(s) and define the behavior of the characteristic functions. The construction supports a variable number of parameters and mutually recursive definitions.

\item Each Lemma generated during the construction of the datatype is proven automatically with a formal proof via ML tactics.

\item A small Theory on how to create non-emptiness witnesses for the new datatypes when given the witnesses for the inputs and incorporated it into the datatype generation. A small example in Isabelle confirms the concept.

\item The theory for generalized folds on non-uniform datatypes was extended from single parameter and occurence to multi parameter and multi occurrence including mutual datatypes (except the ML tactics). The implementation of the theory in ML can generate recursive functions on non-uniform datatypes given a user-specification. Lemmas formally proving the isomorphism to the specification are generated as terms.

\item Examples testing the infrastructure in ML.

\end{itemize}






