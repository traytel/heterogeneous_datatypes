theory ML_Powerlist
  imports Main
begin

ML {*
datatype 'a plist = Nil | Cons of 'a * ('a * 'a) plist;
*}

ML {*
fun len Nil = 0
  | len (Cons (x, xs)) = 1 + len xs;
*}

ML {*
fun str f Nil = "[]"
  | str f (Cons (x, xs)) = f x ^ " # " ^ str (fn (x1, x2) => "(" ^ x1 ^ ", " ^ x2 ^ ")") xs;
*}

end
