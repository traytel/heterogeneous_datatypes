\appendix
\CitationPrefix{A}
\section{}

\subsection{(Weak) Parametricity and Naturality}
\label{app-param}

We define and connect the various parametricity notions used in the paper. We fix two $n$-ary BNFs $\F$ and $\G$.

A polymorphic constant $\mathsf{c} : \ov{\alpha}\;\F$ is \emph{parametric} if $\rel{\F}\;\ov{R}\;(\mathsf{c} :  : \ov{\alpha}\;\F)\;(\mathsf{c} : \ov{\beta}\;\F)$ holds for all $R_i : \alpha_i \to \beta_i$.
%
We generalize this notion from BNFs to the full function space. (Note that the function space $\alpha \to \beta$ is only a BNF in $\beta$, since BNFs are strictly positive functors.) A function $\mathsf{f} : \ov{\alpha}\;\F \to \ov{\alpha}\;\G$ between two BNFs is \emph{parametric} if $(\rel{\F}\;\ov{R} \Mapsto \rel{\G}\;\ov{R})\;\mathsf{f}\;\mathsf{f}$ holds for all $\ov{R}$.
%
Here we use the \emph{function space relator} $R \Mapsto S$ defined as $\lambda f\;g.\;(\forall a\;b.\;R\;a\;b \iff S\;(f\;a)\;(g\;b))$. In other words, two functions are related by $R \Mapsto S$ if for all $R$-related inputs, the outputs are $S$-related. Using the function space relator parametricity can be defined for higher-order functions of arbitrary arity as well.

Parametricity is a strong requirement. For example, polymorphic equality is not parametric. A weaker requirement is \emph{injective-parametricity}: $(\rel{\F}\;\ov{R} \Mapsto \rel{\G}\;\ov{R})\;\mathsf{f}\;\mathsf{f}$ must hold only for those $\ov{R}$ that are graphs of injective functions, i.e., left-total, single-valued relations. Polymorphic equality is injective-parametric, i.e. $(R \Mapsto R \Mapsto (\iff))\;(=)\;(=)$ for all left-total, single-valued $R$ (where $(\iff)$ is the trivial relator for $\boolT$).

For some constants, in particular the existential and universal quantifiers, injective-parametricity is still too strong.
In general, on predicates $\mathsf{P} : \ov{\alpha}\;\F \to \boolT$, (injective-)parametricity requires $(\rel{\F}\;\ov{R} \Mapsto (\iff))\;\mathsf{P}\;\mathsf{P}$ to hold for all $\ov{R}$ (that are graphs of injective functions). By refining the $\iff$ relation, we obtain two new notions: \emph{injective-monotone parametricity} (IMP) when $\iff$ is replaced with $\implies$ and
\emph{injective-antitone parametricity} (IAP) when $\iff$ is replaced with $\Medleftarrow$. The existential quantifier is IMP in that $((R \Mapsto (\iff)) \Mapsto (\implies))\;\exists\;\exists$ holds for all left-total, single-valued $R$. Dually, we have $((R \Mapsto (\iff)) \Mapsto (\Medleftarrow))\;\forall\;\forall$ for all left-total, single-valued $R$. Hence, the universal quantifier is IAP. Note that a predicate that is IAP and IMP is also injective-parametric.

Finally, we rephrase the above relational parametricity notions in terms of functions. The rich BNF structure %, in particular weak pullback preservation,
allows us to connect those two worlds.
The key notion in the functional world is that of a \emph{natural transformation}: The function
$\mathsf{f} : \ov{\alpha}\;\F \to \ov{\alpha}\;\G$ is a natural transformation if $\mathsf{f} \circ \map{\F}\;\ov{g} = \map{\G}\;\ov{g}\;\circ\;\mathsf{f}$ holds for all $\ov{g}$. On BNFs, this notion is equivalent to parametricity of $\mathsf{f}$. While it is clear that parametricity implies naturality (by instantiating each $R_i$ with the graph of the corresponding $g_i$), we sketch a proof of the converse direction.

Let $\mathsf{f} : \ov{\alpha}\;\F \to \ov{\alpha}\;\G$ be a
natural transformation between two BNFs.
We first note that, by naturality of both $\mathsf{f}$ and $\Gseti$ for all $i \in [n]$, $\mathsf{f}$ never increases the set of components of an~$x : \ov{\alpha}\;\F$:~ %%% ~ is a typesetting hack
%
\begin{equation}\label{eq:set}
\forall x : \ov{\alpha}\;\F.\;\forall i \in [n].\; \Gseti\;(\mathsf{f}\;x) \subseteq \Fseti\;x\tag{$\star$}
\end{equation}
%
Now we prove that $\mathsf{f}$ is parametric. For this, let $R_i : \alpha_i \to \beta_i \to \boolT$ for $i \in [n]$, $x : \ov{\alpha}\;\F$ and $y : \ov{\beta}\;\F$ such that $\rel{\F}\;\ov{R}\;x\;y$. From the definition of $\rel{\F}$, we obtain $z \in \ov{\alpha\times\beta}\;\F$ such that (1) $x = \map{\F}\;\ov{\fst}\;z$, (2) $y = \map{\F}\;\ov{snd}\;z$,
and (3) $\Fseti\;z\subseteq\{(x,\,y).\;R_i\;x\;y\}$ for all $i \in [n]$.
To show $\rel{\G}\;\ov{R}\;(\mathsf{f}\;x)\;(\mathsf{f}\;y)$, we unfold the definition of $\rel{\G}$ and prove:
\begin{itemize}
    \item $\map{\G}\;\ov{\fst}\;(\mathsf{f}\;z) = \mathsf{f}\;x$ using (1) and the naturality of $\mathsf{f}$,
    \item $\map{\G}\;\ov{\snd}\;(\mathsf{f}\;z) = \mathsf{f}\;y$ using (2) and the naturality of $\mathsf{f}$,
    \item $\Gseti\;(\mathsf{f}\;z)\subseteq\{(x,\,y).\;R_i\;x\;y\}$ for all $i \in [n]$ using (3) and~$\eqref{eq:set}$.
\end{itemize}

Using similar arguments, we obtain equivalent functional characterizations for the weaker parametricity notions as well:
\begin{itemize}
   \item $\mathsf{f}$ is injective-parametric iff $\mathsf{f} \circ \map{\F}\;\ov{g} = \map{\G}\;\ov{g}\;\circ\;\mathsf{f}$ holds for all injective functions $\ov{g}$.
   \item $\mathsf{P}$ is IMP iff $\mathsf{P}\;x \implies \mathsf{P}\;(\map{\F}\;\ov{g}\;x)$ holds for all injective functions $\ov{g}$.
   \item $\mathsf{P}$ is IAP iff $\mathsf{P}\;(\map{\F}\;\ov{g}\;x) \implies \mathsf{P}\;x$ holds for all injective functions $\ov{g}$.
\end{itemize}

\subsection{Proof Sketch of Theorem \ref{thm-wit}}
\label{app-wit-proof}


%As usual, 
Since the types $\T_\imut$ do not exist yet, 
we need to work on the representation types $\raw_\imut$, and prove a property about the 
predicates $\ok_\imut$ that represent the to-be-defined types $\T_\imut$. For this, we introduce 
a refinement of the notion of witness: Given a shadow $\ul : \depthT$, 
a set $I \su [\nmut]$ is an \emph{$\ul$-witness}
for $\raw_\imut$ if, for all sets $\ov{A}$, $\forall \iarg \in I.\;A_\iarg \not=\emptyset$ 
implies $\exists r \in \raw_\imut\;\ov{A}.\;\ok_\imut\;\ul\;r$. 
We prove:

(1) $\Langimut(\Gr)$ (or $\Langinfimut(\Gr)$) is a perfect sets of $[]$-witnesses for 
the (co)datatype $\raw_\imut$. 

%Assuming (3) is proved, we easily infer our desired facts (1) and (2), by reifying 
%the $(\ok_\imut\;[])$-witness for $\raw_i$ into regular witnesses for $\T_i$. 
%: First, 
%having $\emptyset$ as a perfect set of $\ok$-witnesses means 
%there is no $(\ok_\imut\;[])$-witness, which is equivalent to $(\ok_\imut\;[])$ being empty 
%(i.e., $\T_\imut$ would be empty). Moreover, thanks to the definition of the set functions 
%for $\T_\imut$ as copies of those for $\raw_\imut$, we have that the $(\ok_\imut\;[])$-witnesses for $\raw_\imut$ 
%coincide with the (regular) witnesses for $\T_\imut$. 
%So let us focus on (3). 
%Since the grammar was designed having in mind the (high-level) definition of the $\T_\imut$'s, 
%we need to see how this looks from the $\raw_\imut$'s point of view. 
%The destructors $\unT_i$ were defined 
%as copies of the functions 
%
%We first establish a connection between the productions of the grammar and the low-level 
%destructor:
To prove (1), 
we are looking for a connection between the grammar $\Gr$ and $\raw_\imut$'s (co)recursive specification, 
in the direction of the destructor 
$$
\ov{\alpha}\;\raw_\imut \stackrel{\unRaw_i}{\longrightarrow} 
(\ov{\ov{\alpha}\;\shape},\,\ov{\alpha}\;\raw_{\sigma(1)},\,\cdots,\,\ov{\alpha}\;\raw_{\sigma(\nocc)})\;\G_\imut
$$
%
By the definition of $\ok_\imut$, 
if $r : \ov{\alpha}\;\raw_\imut$ is such that $\ok_\imut\;\ul\;r$, 
then $\Dtor_\imut\;r$ has, for each $\iocc \in [\nocc]$, its $\iocc$-components 
$r':\ov{\alpha}\;\raw_{\sigma(\iocc)}$ satisfying $\ok_\imut\;(\listCons{\iocc}{\ul})\;r'$.  
As discussed in Section \ref{subsec-datatype}, the shadow increment between $\ul$ and $\listCons{\iocc}{\ul}$ 
encodes the application of the components 
$\ov{\F_{\iocc}} = (\F_{\iocc 1}$, $\ldots$, $\F_{\iocc \narg})$, reflected in the grammar's productions of type 2. 
This suggests %the following 
a recursive translation of shadows into polywits: 
\begin{itemize}
\item $\gamma_{\iarg}\;[] = \{\{\iarg\}\}$;
\item $\gamma_{\iarg}\;(\listCons{\iocc}{\ul}) = (\gamma_{1}\;\ul,\ldots,\gamma_{\narg}\;\ul) \cdot \II(\F_{\iocc\iarg})$.
\end{itemize}
%
Now we can formulate a generalization of (1), taking into account arbitrary shadows, not just $[]$. 
For each nonterminal $x$, we write $\Lang_{x}(\Gr)$ (or $\Lang_{\infty,x}(\Gr)$) for 
the language (co)generated by $x$. 

(2) For all $u : \depthT$, $\Lang_{(\gamma_{\iarg}\;\ul,\ldots,\gamma_{\iarg}\;\ul)\,t_\imut}(\Gr)$ 
(respectively $\Lang_{\infty,(\gamma_{\iarg}\;\ul,\allowbreak\ldots,\gamma_{\iarg}\;\ul)\,t_\imut}(\Gr)$) 
is a perfect set of $\ul$-witnesses for 
the (co)datatype $\raw_\imut$. 

Finally, (2) can be proved using standard (uniform) (co)induction and (co)recursion, 
moving back and forth 
between $\Gr$-derivation trees and the $\raw_\imut$'s. 
%The proof proceeds as follows: 

\paragraph*{For datatypes}  
That every $I \in \Lang_{(\gamma_{\iarg}\;\ul,\ldots,\gamma_{\iarg}\;\ul)\,t_\imut}(\Gr)$ 
is an $\ul$-witnesses follows by structural induction on its derivation tree in $\Gr$.  
Conversely, that for every $\ul$-witness $J$, we have $I \subseteq J$ for 
some $I \in \Lang_{(\gamma_{\iarg}\;\ul,\ldots,\gamma_{\iarg}\;\ul)\,t_\imut}(\Gr)$ follows by induction 
on the definition of $\ok_\imut$.

\paragraph*{For codatatypes} 
To prove that every $I_0 \in \allowbreak\Lang_{\infty,(\gamma_{\iarg}\;\ul_0,\ldots,\gamma_{\iarg}\;\ul_0)\,t_{\imut_0}}(\Gr)$ 
is an $\ul_0$-witness, we let $\ov{A}$ be such that $\forall i \in I_0.\;A_i \not=\emptyset$.
With $\ul_0,\imut_0,I_0$ and $\ov{A}$ fixed, let $\Tr$ be a (possibly infinite) derivation tree of 
$(\gamma_{\iarg}\;\ul_0,\ldots,\gamma_{\iarg}\;\ul_0)\,t_{\imut_0}$ in $\Gr$---thus having $I_0$ 
as the set of terminals on its frontier. Let $\depthT_{\ul_0,\imut}$ consist of all shadows having $\ul_0$ as a prefix 
and such that the nonterminal $(\ov{\gamma}\;\ul)\,t_{\imut}$ occurs in the tree $\Tr$, where we write 
$\ov{\gamma}\;\ul$ for $(\gamma_{1}\;\ul,\ldots,\gamma_{\narg}\;\ul)$.

%From the form 
%of the productions in $\Gr$ and the definition of the $\gamma_\iarg$'s, we see that all the nonterminals in $\Tr$ 
%have either 
%the form $\gamma_{\iarg}\;\ul$ or the form $(\gamma_{1}\;\ul,\ldots,\gamma_{\narg}\;\ul)\,t_{\imut_0}$, 
%where in both cases $\ul \in \depth_{\ul_0}$.    
Mutually corecursively 
(by primitive $\raw$-corecursion), 
we define the functions 
$w_\imut : \depthT_{\ul_0,\imut} \ra \ov{\alpha}\;\raw_\imut$, 
%from shadows having $u$ as a prefix to the codatatypes, 
by 
%
$$w_\imut\;\ul = \map{\G_i}\;\ov{\id}\;(w_{\sigma(1)},\ldots,w_{\sigma(\nocc)})\;g_\ul$$
%
where the element $g_\ul \in (\ov{\ov{\alpha}\;\shape},\,\depthT_{\ul_0,\sigma(1)},\,\cdots,\,\depthT_{\ul_0,\sigma(\nocc)})\;\G_\imut$ 
is defined as follows:  
Let $(\ov{\gamma}\;\ul)\,t_{\imut} \longrightarrow \Gamma_I$ be the (type 2) 
production in $\Tr$ corresponding to the terminal $(\ov{\gamma}\;\ul)\,t_{\imut}$. 
\begin{itemize}
\item From the definition of $\Gamma_J$ it follows that, for each $\iocc$ such that $\narg + \iocc \in J$, the nonterminal 
$((\ov{\gamma}\;\ul) \cdot \II(\F_{\iocc 1}), \ldots, (\ov{\gamma}\;\ul) \cdot \II(\F_{\iocc\narg}))\,t_{\sigma(\iocc)}$, 
i.e., $(\gamma_1(\listCons{\iocc}{\ul}), \ldots, \gamma_\narg(\listCons{\iocc}{\ul}))\,t_{\sigma(\iocc)}$, 
is also in $\Tr$; hence $\listCons{\iocc}{\ul} \in \depthT_{\ul_0,\sigma(\iocc)}$. 
%
\item Also from the definition of $\Gamma_J$ it follows that, for each $\iarg \in [\narg] \cap J$, 
the nonterminal $\gamma_k\;\ul$ is also in $\Tr$. Since only type 1 productions are applicable 
to polywit nonterminals, let $\gamma_k\;\ul \ra I$ be the production from $\Tr$ applied to $\gamma_k\;\ul$. 
Then $I$ is included in $\Tr$'s frontier, i.e., $I \su I_0$. Then, picking some elements $a_i \in A_i$ 
for $i\in I$, we can define shapes $s_\iarg \in \shape_\iarg\;\ov{A}$ for each $\iarg \in [\narg]$ that are full trees, 
i.e., such that $\full_\ul\;s_i$ holds. 
\end{itemize}
Thus, we have constructed the elements $\listCons{\iocc}{\ul} \in \depthT_{\ul_0,\sigma(\iocc)}$ 
for each $\iocc$ such that $\narg + \iocc \in J$ and $s_i \in \shape_\iarg\;\ov{A}$ (in particular, 
$s_i : \ov{\alpha}\;\shape_\iarg$) for each $\iarg \in [\narg]$. Since $J$ is a witness for $\G_i$, 
we obtain our desired element $g_\ul \in (\ov{\ov{\alpha}\;\shape},\,\depthT_{\ul_0,\sigma(1)},\,\cdots,\,\depthT_{\ul_0,\sigma(\nocc)})\;\G_\imut$, 
which concludes the definition of the $w_\imut$'s.  
Because of our choices in the definition, it is now routine to prove the following:
\begin{itemize}
\item by rule coinduction 
on the definition of the $\ok_\imut$'s, that $\ok_\imut\;\ul\;(w_\imut\;\ul)$ holds; 
\item by rule induction on the definition of the set operators for $\raw$, 
that $\setOp_{\raw,\iarg}(w_\imut\;\ul) \su A_\imut$ holds, which means that $w_\imut\;\ul \in \raw_\imut\;\ov{A}$ holds. %for all $\imut$ and $\ul$. 
\end{itemize}
%
This concludes the proof that $I_0$ is a witness. 

Conversely, to prove that for every $\ul$-witness $J$, we have $I \subseteq J$ for 
some $I \in \Lang_{\infty,(\gamma_{\iarg}\;\ul,\ldots,\gamma_{\iarg}\;\ul)\,t_\imut}(\Gr)$, 
we construct a (possibly infinite) derivation tree whose frontier includes $J$. 
The construction proceeds corecursively, by extracting each time the next production 
to be applied from $J$ and the $\G_\imut$'s witnesses. \qed



\leftOut{
\subsection{An Axiomatic Extension of HOL}
\label{app-extensions}

As illustrated throughout this paper, we had to walk a very tight rope 
in oder to integrate nonuniform datatypes in HOL---for defining the types 
to proving them nonempty to proving induction and recursion principles. 
Each time, we fought with lack of expressiveness of HOL, where type 
quantification is restricted to the top level. But in the end all 
the constructions have been entirely reduced to the HOL kernel. This 
foundational approach, of not adding new axioms but only definitions 
and proofs in the minimal logic, is a major strength of the HOL community, because it guards against 
inconsistencies. 

On the other hand, our results have limitations: We can only prove injective-antitone-parametric 
predicates for induction and employ injective-parametric terms in the recursion. As it turns out, 
%a gentle, 
%provably consistent -- I no longer say this, now that's in the appendix. 
axiomatic extension of HOL would remove these restrictions. 
The axioms do not refer to nonuniform datatypes, or the intricate constructions leading to them, or even to BNFs. 
Rather, they are general-purpose axioms for cross-type well-founded induction and recursion. 
HOL seems to have a blind spot on these topics, and therefore we propose a gentle eye surgery. 
}% end leftOut


\subsection{Fusion Laws} \label{app-fusion}

We fix a nonuniform datatype $\alpha\;\T \eqLFP (\alpha,\alpha\;\F\;T)\;\G$. 
Let us write $\Rec(\Dom,\Ran,\V,\argobj,\defobj)$ 
for the polymorphic function 
$\recfun : \alpha\;\Dom\;\T \to \alpha\;\Ran$ defined by nonuniform 
recursion from  
a blueprint $\defobj : (\alpha\;\Dom,\,\alpha\;\V\;\Ran)\;\G \to \alpha\;\Ran$ and a swapper
$\argobj : \alpha\;\Dom\;\F \to \alpha\;\V\;\Dom $, as in Section \ref{subsec-genPrimRec}. 
($\Rec$ cannot be a HOL combinator; it is simply a meta-level notation.)

\begin{theorem}
The following hold: 
\begin{description}
\item{Fold fusion:} If $\kappa : \alpha\;\Ran \ra \alpha\;\Ran'$ is such that 
$\kappa \circ \defobj = \defobj' \circ \Gmap\;\id\;\kappa$, 
then
$\kappa \circ \Rec(\Dom,\Ran,\V,\argobj,\defobj) = \Rec(\Dom,\Ran',\V,\argobj,\defobj')$.
%
\item{Map fusion:} If $\kappa : \alpha\;\Dom' \ra \alpha\;\Dom$ is such that 
$\kappa \circ \argobj' = a \circ \Fmap\;\kappa$, 
then 
$\Rec(\Dom,\Ran,\V,\argobj,\defobj) \circ \Tmap\;\kappa = \Rec(\Dom',\Ran,\V,\argobj',\defobj \circ \Gmap\;\kappa\;\id)$.
\end{description}
\end{theorem}
%
\begin{proofx}
For fold fusion, 
we let $\recfun = \Rec(\Dom,\Ran,\V,\argobj,\defobj)$ and $\recfun' = \Rec(\Dom,\Ran',\V,\argobj,\defobj')$.
Defining the predicate $\PP$ by $\PP\;g \iff \recfun\;g = \recfun'\;g$, 
we have that $\PP$ is IAP (since $\recfun$ and $\recfun'$ are IAP, and so is the equality). 
Then $\PP$ follows by nonuniform induction (rule schema $\Induct_{\T}^{\PP}$),  
chasing the left diagram of Figure \ref{fig-fusion} (where, as usual, we omit the mapping function 
for $\G$)---induction essentially allows one to assume 
that $\kappa \circ \recfun = \recfun'$ holds on the right of the diagram (inside the $\Gsett$-componenents) and 
requires one to prove that it holds for the left of the diagram---this is trivial from 
the assumption $\kappa \circ \defobj = \defobj' \circ \Gmap\;\id\;\kappa$ and the functoriality 
of the involved BNFs. 

The case of map fusion is similar---depicted in the right diagram of Figure \ref{fig-fusion}. 
As can be seen in the upper sub-diagram, here, besides functoriality, we also need that 
the destructor is a natural transformation (which is guaranteed by our construction and 
automatically delivered 
as a theorem when the type $\alpha\;\T$ is defined).  
\end{proofx}


\begin{figure*}[t]\centering
    %
    \centering
 $\xymatrix@C=0.5pc@R=4pc{
        \alpha\;\Dom\;\T  \ar^{\Dtor\;\;\;\;}[rr] \ar_{\recfun}[dd] \ar_{\recfun'}[dddr] &  & 
            (\alpha\;\Dom,\alpha\;\Dom\;\F\;\T)\,\G  \ar_{\id}^{\Tmap\;\argobj}[d] & 
      \\  
     & &  
     (\alpha\;\Dom,\alpha\;\V\;\Dom\;\T)\,\G \ar_{\id}^{\recfun}[d] \ar_{\id}^{\recfun'}@/^1.5pc/[ddr] &
      \\
     \alpha\;\Ran  \ar_{\kappa}[dr] &  & (\alpha\;\Dom,\alpha\;\V\;\Ran)\,\G \ar_{\id}^{\kappa}[dr] \ar_{\defobj}[ll]  &
      \\
      &  \alpha\;\Ran'  & & (\alpha\;\Dom,\alpha\;\V\;\Ran')\,\G \ar_{\defobj'}[ll] 
    }$
%%%%%%%%%%
\hspace*{4ex}
%%%%%%%%%%
    $\xymatrix@C=0.5pc@R=4pc{
      & \alpha\;\Dom'\;\T  \ar^{\Dtor\;\;\;\;}[rr] \ar_{\Tmap\;\kappa}[dl] \ar_{\recfun'}[dddl]   &  & 
            (\alpha\;\Dom',\alpha\;\Dom'\;\F\;\T)\,\G  \ar_{\id}^{\map{\F\;\T}\;\kappa}[dl]   \ar_{\id}^{\Tmap\;\argobj'}[d]
      \\ 
        \alpha\;\Dom\;\T  \ar^{\Dtor\;\;\;\;}[rr] \ar_{\recfun}[dd]  &  & 
            (\alpha\;\Dom,\alpha\;\Dom\;\F\;\T)\,\G  \ar_{\id}^{\Tmap\;\argobj}[d] & 
             (\alpha\;\Dom',\alpha\;\V\;\Dom'\;\T)\,\G \ar_{\id}^{\recfun'}[d] \ar_{\kappa}^{\Tmap\;\kappa}[dl]
      \\  
     & &  
     (\alpha\;\Dom,\alpha\;\V\;\Dom\;\T)\,\G \ar_{\id}^{\recfun}[d]  &  
        (\alpha\;\Dom',\alpha\;\V\;\Ran)\,\G  \ar_{\kappa}^{\id}[dl]
      \\
     \alpha\;\Ran   &  & (\alpha\;\Dom,\alpha\;\V\;\Ran)\,\G  \ar_{\defobj}[ll]  &
    }$
    \caption{\,Proof of the fusion laws: fold (left) and map (right)}
    \label{fig-fusion}
\end{figure*}


The duals of the funsion laws hold when $\alpha\;\T$ 
is a nonuniform codatatype as in Section \ref{subsec-genPrimCoRec}.
\leftOut{ and we write 
us write $\Corec(\Dom,\Ran,\V,\argobj,\defobj)$ 
for the polymorphic function 
$\recfun : \alpha\;\Ran \to \alpha\;\Dom\;\T$ defined by nonuniform 
corecursion from  
a blueprint $\defobj :
\alpha\;\Ran \to \defobj : (\alpha\;\Dom,\,\alpha\;\V\;\Ran)\;\G$ and a swapper
$\argobj : \alpha\;\V\;\Dom \to \alpha\;\Dom\;\F$. 
} %end leftOut





\subsection{Cross-Type Induction Schema} \label{sec-crossTypeInd}

As discussed in the paper, a main restriction of our work is induction for nonuniform types, 
where we require IA-parametricity of the predicate. 
Here, we show how a gentle, 
provably consistent 
axiomatic extension of HOL removes this restriction. 
The axiom does not refer to nonuniform datatypes, or the intricate construction leading to them, or even to BNFs. 
Rather, it is a general-purpose axiom for cross-type well-founded induction and recursion. 
%HOL seems to have a blind spot on these topics, and therefore we propose a gentle eye surgery.

We fix the types $\alpha\;\T$, 
$\alpha\;\F$ and $\M$ (with the notations $\T$ and $\F$ not %necessarily 
connected to nonuniform datatypes).

Let $\PP : \alpha\;\T \ra \boolT$ be a polymorphic predicate, for which we want to prove 
%
$\forall \alpha.\;\forall t : \alpha\;\T.\;\PP\;t$. %  \;\;\;\;\;\;\;\mbox{(*)}$
%
%i.e., making the type quantification explicit,
%
%$$\forall \ov{p},\alpha.\;\forall t : (\ov{\beta},\alpha)\T.\;\PP\;t  \;\;\;\mbox{(*)}$$
%
A natural approach would be induction using a measure 
$\meas : \alpha\;\T \ra \M$ which decreases w.r.t.\ a well-founded relation 
$\rell : \M\;\setT \ra \M\;\setT \ra \boolT$. But what if the measure decreases by changing the type, 
as in $\rell\;(\meas\;t')\;(\meas\;t)$, where $t:\alpha\;\T$ and $t':\alpha\;\F\;\T$? 
This is still acceptable, since well-foundedness should still operate  
across the types $\alpha\;\F^n\;\T$. Formally, we would like to have the following rule, 
where $\wf\;\rell$ states that $\rell$ is well-founded:
%
$$
\rl{$\WFInd_{\T,\F,\rell,\meas,\PP}$}
{
    \begin{array}{l}
    \forall \alpha.\;\forall t : \alpha\;\T.\;
    \\
    \quad
    \wf\;\rell  \,\wedge\, (\forall t': \alpha\;\F\;\T.\;\rell\;(\meas\;t')\;(\meas\;t) \lra \PP\;t')
    \\
    \quad
    \lra \PP\;t  
    \end{array} 
}
{
    \forall \alpha.\;\forall t : \alpha\,\T.\;\PP\;t
}
$$ 
%
%
In HOL, this kind of induction across types is impossible to justify 
(without requiring parametricity). 
%Yet, 
Yet, %it is easy to see that 
it is safe:

\balance

\begin{theorem} \label{prop-generic-induct-axiom}
    The rule schema $\WFInd$ 
    is sound in the standard models of HOL \cite{pitts1993} 
    and in the ground models of Isabelle\slash HOL \cite{kuncar-popescu-itp2015}.
    Hence it is consistent with HOL and Isabelle\slash HOL.
\end{theorem}

We treat Isabelle\slash HOL specially because it allows ad hoc overloading
%of constants
intertwined with type definitions, which is problematic in the
standard HOL semantics \cite{kuncar-popescu-itp2015,kp-esop-2017}.

%
\begin{proofx}
In short, the schema is consistent with HOL because it is clearly sound in the set-theoretic interpretation of HOL. 
The following argument elaborates this idea.

A standard model of HOL fixes a universe $\UU$, i.e., a set of sets 
with some closure properties (e.g., closure under function spaces). 
It interprets a type constructor such as $\T$ 
as a function on this universe, $[\T] : \UU \ra \UU$, a type such as $\M$ as an element of the universe, $[\M] \in \UU$, etc.  
Moreover, a polymorphic constant such 
as $\meas : \alpha\;\T \ra \M$ is interpreted as a $\UU$-indexed family $([\meas]_{A})_{A \in \UU}$ where $[\meas](A) : \T(A) \ra [\M]$. 
Crucially, it interprets the function-space type constructor as 
the set of {\em all} functions between the interpretation of its arguments and 
the type $\natT$ as a countable set $[\natT]$, which with $[0]$ and $[\Suc]$ 
is isomorphic to the natural numbers.

This means that the scheme $\WFInd$ can be justified 
inside $\UU$ as follows: 
Assuming its conclusion is false and repeatedly using its hypothesis, we obtain 
the infinite seqeuences $(A_i)_{i}$ and $(b_i)_{i}$ such that $A_{i+1} = [\F](A_i)$, 
$b_i \in [\T](A_i)$ and $[\rell]\,([\meas](A_{i+1})(b_{i+1}))\,([\meas](A_i)(b_i)) = [\True]$. 
Taking $c_i = [\meas](A_i)(b_i)$, this gives us an infinite sequence $(c_i)_{i}$ such that $c_i \in [\M]$ and  
$[\rell]\,(c_{i+1})\,(c_i) = [\True]$. Thanks to standardness, $(c_i)_{i}$ yields a witness for 
the formula $\exists c':\natT\ra\M.\;\forall i:\natT.\;\rell\,(c'\;i)\,(c'\;i)$, which therefore holds in the model. 
This contradicts the fact that $[\wf\;\rell]$ also holds in the model. 

A ground model of Isabelle\slash HOL only interprets the ground (monomorphic) types and terms, 
again with a standard interpretation for functions and numbers. A formula is true in such a model 
iff all its ground substitutions are true. For example, $\forall x:\alpha.\;x = x$ is %deemed
true 
because, for all ground types $\K$, the ground formula $\forall x:\K.\;x = x$ is true. % in the model.  
%
The argument for why the schema $\WFInd$ is sound is similar to the case of standard HOL models, 
but employing ground types $\K_i$ instead of the sets $A_i$. 
%
\end{proofx}

With this addition, we can remove the parametricity requirement from Theorem\ \ref{prop-induct-param}: 
%
\begin{theorem} \label{prop-generic-induct}
    The $\Induct$ schema is derivable in HOL enriched with the $\WFInd$ schema. 
\end{theorem}
%
\begin{proofx}
%
The derivation takes place by instantiating the parameters of $\WFInd_{\T,\F,\rell,\meas,\PP}$ using 
those of $\Induct_{\T}^{\PP}$. 
We take 
\begin{itemize}
    \item $\T$ and $\F$ as in the nonuniform datatype definition $\alpha\;\T \eqLFP (\alpha,\,\alpha\;\F\;\T)\;\G$;
    \item $\M$ to be the uniform datatype
    %
    $%\textsc{datatype}\;\; 
    \M \eqLFP \MCons\;((\unitT,\,\M)\;\G)$;
    %
    \item $\rell$ to be the immediate subterm relation associated to $\M$, 
    namely $\rell\;m\;m' \iff  m' \in \Gsett\,(\MCons\;m)$;
    %
    \item $\meas$ to be the composition $\rawmeas \circ \Rep_\T$, where 
    $\Rep_T : \alpha\;\T \ra \alpha\;\raw$ is the representation function for $\T$ and  
    $\rawmeas : \alpha\;\raw \ra \M$ sends any $r$ to its recursive depth:
    %
    $$\rawmeas\;(\Raw\;g) = \MCons\;(\Gmap\;(\lambda\_.\;())\;\rawmeas\;g)$$
\end{itemize}   
%
With these parameters in place, 
it is not hard to verify that the assumptions of $\WFInd_{\T,\F,\rell,\meas,\PP}$ hold. 
\end{proofx}

In summary, the unrestricted %lighter and 
version of nonuniform induction %and recursion 
is available in a consistent axiomatic 
extension of HOL. 
Users can choose between enabling this axiom or using the more restricted 
rule that depends on parametricity.

\leftOut{
\subsection{Cross-Type Recursion Schema}\label{subsec-crossTypeRec}

We can play a similar game to leverage nonuniform recursion.
%to remove the parametricity assumptions, and in fact 
%even the BNF requirement for the target type, from Section \ref{sec:co-recursion-principles} recursion principle. 
In addition to the parameters in the previous section, types $\alpha\;\T$, 
$\alpha\;\F$ and $\M$ and constants $\meas : \alpha\;\T \ra \M$ and  
$\rell : \M^2\;\setT$, we fix the type $\alpha\;\Ran$. 

The goal is to define 
a polymorphic function $f : \alpha\;\T \ra \alpha\;\Ran$ by well-founded recursion using 
$\meas$ and $\rell$---namely, by defining $f\;(t:\alpha\;\T)$ in terms of elements 
$t' : \alpha\;\F\;\T$ such that $(\meas\;t',\meas\;t) \in \rell$. To achieve this, 
we assume a fixpoint combinator $\cmb : (\alpha\;\F\;\T \ra \alpha\;\F\;\Ran) \ra \alpha\;\T \ra \alpha\;\Ran$ 
assumed to make progress by only analyzing smaller values, i.e., satisfy the property $\prg_\cmb$ defined as follows:   
%
$$
\begin{array}{l}
\forall \alpha.\;\forall f,g : \alpha\;\F\;\T \ra \alpha\;\F\;\Ran.\;\forall t:\alpha\;\T.\;
\\
\hspace*{2ex}
(\forall t'.\;(\meas\;t',\meas\;t) \in \rell \implies f\;t' = g\;t') 
\implies \cmb\;f\;t = \cmb\;g\;t
\end{array}$$
%
This is very similar to the combinators for well-founded recursion usually defined in HOL, 
save for shift between $\alpha\;\T$, the type of $t$, and $\alpha\;\F\;\T$, the type of the smaller elements $t'$. 
We would like to perform the fixpoint definition 
$f\;t = \cmb\;f\;t$. So the definitional principle, which we call $\Ddef_{\T,\F,\rell,\meas,\PP,\Ran,\cmb}$, 
proceeds as follows: 
%
\begin{quote}
    Given $\T,\F,\rell,\meas,\PP,\Ran,\cmb$ such that $\prg_\cmb$ holds, 
    introduce a new polymorphic constant $f : \alpha\;\T \ra \alpha\;\Ran$ 
    and the axiom $f = \cmb\;f$.
\end{quote}
%
This style of definition is of course outside the scope of HOL. %, since HOL does not allow polymorphic recursion. 
But, again, it it safe:

\begin{theorem} \label{prop-generic-rec}
    The definition schema $\Ddef$ is consistent with HOL. 
\end{theorem}

This schema allows us remove a lot of the recursion overhead: 
the BNF structure on the involved type constructors and the parametricity 
requirements for the blueprint and the swapper. 

With this addition, we can remove the parametricity requirement from Theorem\ \ref{prop-induct-param}: 
%
\begin{theorem} \label{prop-generic-rec-proc}
    In HOL enriched with $\Ddef$, the recursion procedure works 
    for any types $\alpha\;\V$ and $\alpha\;\Ran$ 
    and any polymorphic functions $\defobj : (\alpha\;\Dom,\,\alpha\;\V\;\Ran)\;\G \to \alpha\;\Ran$
    and $\argobj : \alpha\;\Dom\;\F \to \alpha\;\V\;\Dom$.  
\end{theorem}

The prove this, we take $\alpha\;\T'$ to be $\alpha\;\Dom\;\T$, $\alpha\;\F'$ to be $\alpha\;\V$, 
and define $\cmb : (\alpha\;\F'\;\T' \ra \alpha\;\F'\;\Ran) \ra \alpha\;\T' \ra \alpha\;\Ran$ 
by $\cmb\;f\;t = \defobj\;(\Gmap\;\id\;(f \circ \Tmap\;\argobj)\;(\Dtor\;t))$. 
Moreover, $\rell$ and $\meas$ are defined as for induction in the proof of Theorem \ref{prop-generic-induct}. 
%
Then the function $f$ defined by $\Ddef_{\T',\F',\rell,\meas,\Ran,\cmb}$ satisfies the 
desired characteristic equation. 
} % end leftOut




%TODO:
%\\- talk about the tradeoffs; J, in the implementation section, talk about the 
%parametricity-free mode.
%\\- proof sketch for the consistency theorems

%%% Trick to get proper alignment
\let\oldthebibliography=\thebibliography
\let\endoldthebibliography=\endthebibliography
\renewenvironment{thebibliography}[1]{\begin{oldthebibliography}{A#1}}{\end{oldthebibliography}}

\nocite{gordon-melham-1993}
\bibliographystyle{myabbrv}
\bibliography{tex}
