This archive contains supplementary material related to the paper draft

  Foundational Nonuniform (Co)datatypes in Higher-Order Logic

The material consists of the Isabelle/HOL formalizations of the main
constructions from the paper together with a tool implementation and examples.
The Isabelle theory files can be processed with a recent development version
of Isabelle (e.g., Mercurial changeset 54f5afc9c413). The development version
can be obtained following the instructions at

    http://isabelle.in.tum.de/repos/isabelle/file/54f5afc9c413/README_REPOSITORY

In addition to the source files, we provide browsable HTML pages that were
generated from the formalization. The entry page is "html/index.html". The
page structure is similar to what is described below.

---

The main constructions are shown in the "formalization" folder.

The files "Nonuniform_Datatype.thy" and "Nonuniform_Codatatype.thy" contain
the abstract manual (co)datatype constructions from Section III (for the
simple i = j = k = 1 case and arbitrary but fixed BNFs F and G), an abstract
(co)induction example as presented in Section V (for an arbitrary but fixed
predicate P or bisimulation Q), and an abstract (co)recursion example as
presented in Section VI (for arbitrary but fixed blueprint and swapper, called
defobj and argobj in the formalization).

The files "Codatatype_Witnesses.thy" and "Recursion_With_Parameters.thy" show
a concrete construction for coinductive nonemptiness witnesses for a
nontrivial scenario and generalize the recursion construction to handle
additional function arguments (as mentioned at the end of Section VI).

---

The "code" folder contains the implementation of the six commands referred to
in Section VII. The implementation of the commands
"nonuniform_prim(co)recursion" and the construction of nonemptiness witnesses
for codatatypes are currently incomplete.

---

The "examples" folder contains a selection of examples using the above
implementation. "Misc_Types.thy" contains all type definitions used in the
paper and additional ones such as finger trees or bootstrapped and catenable
queues. The "Lambda_Term.thy" and "Powerstream.thy" files contain the examples
from Section VII.
