theory Parametric_General_Induction_Recursion
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

declare [[bnf_internals, typedef_overloaded]]


(* SETTING FOR INDUCTION AND GENERAL RECURSION *)


(* Parameters common to induction and recursion: *)
(* p is a list of 'parameter' type variables *)
bnf_axiomatization 'a T
bnf_axiomatization 'a F 
(* we need the BNF structure for both *)
typedecl M
consts rel :: "M rel"
consts meas :: "'a T \<Rightarrow>  M"

(* Induction: *)
consts P :: "'a T \<Rightarrow> bool"

(* ASSUMPTION: *)
axiomatization where
P_wf_F: 
"\<And> t :: 'a T. (\<And> t' :: ('a F) T. (meas t', meas t) \<in> rel \<Longrightarrow> P t') 
 \<Longrightarrow> P t"
and 
P_injective_semi_param: 
"\<And> (f::'a T \<Rightarrow> 'a' T) t. inj f \<Longrightarrow> P (f t) \<Longrightarrow> P t"
and 
meas_injective_parametric: 
"\<And> (f::'a T \<Rightarrow> 'a' T) t. inj f \<Longrightarrow> meas (f t) =  meas t"
and 
wf_rel: "wf (rel::M rel)" 
  
datatype 'a shape = Leaf 'a | Node "'a shape F"
abbreviation "un_Leaf u \<equiv> case u of Leaf x \<Rightarrow> x"
abbreviation "un_Node u \<equiv> case u of Node x \<Rightarrow> x"
  
lemma inj_map_T_Leaf: "inj (map_T Leaf)"
and inj_map_T_Node: "inj (map_T Node)"
by (meson T.inj_map injI shape.inject)+

(* END PRODUCT (detailed proof for the sake of documentation): *)
lemma P: "P (t::'a T)"
proof-
  have "P (map_T Leaf t)"
  proof(rule wf_induct[OF wf_inv_image[OF wf_rel, of meas]],clarsimp)  
    fix t::"'a shape T" 
    assume 1: "\<forall> t' :: 'a shape T. (meas t', meas t) \<in> rel \<longrightarrow> P t'"
    show "P t"  
    proof(rule P_wf_F)
      fix t'' :: "'a shape F T" assume "(meas t'', meas t) \<in> rel"
      hence "(meas (map_T Node t''), meas t) \<in> rel"
      by (simp add: inj_map_T_Node meas_injective_parametric)
      hence "P (map_T Node t'')" using 1 by simp
      thus "P t''" using P_injective_semi_param inj_map_T_Node by blast      
    qed
  qed
  thus ?thesis using P_injective_semi_param inj_map_T_Leaf by blast
qed
    
(* Recursion: *)
bnf_axiomatization 'a R

consts recdef :: "('a F T \<Rightarrow> 'a F R) \<Rightarrow> 'a T \<Rightarrow> 'a R"

definition recdef_sh :: "('a shape T \<Rightarrow> 'a shape R) \<Rightarrow> 'a shape T \<Rightarrow> 'a shape R"
  where "recdef_sh g \<equiv> recdef (map_R un_Node o g o map_T Node)"
  
(* ASSUMPTION: *)
axiomatization where
 adm_recdef_meas_rel: 
"\<And>f g t. 
   (\<And>t'. (meas t', meas t) \<in> rel \<Longrightarrow> f t' = g t') 
   \<Longrightarrow> recdef f t = recdef g t"
  
 
definition g :: "'a shape T \<Rightarrow> 'a shape R" where 
"g \<equiv> wfrec (inv_image rel meas) recdef_sh"
  
lemma g: "g = recdef_sh g"
unfolding g_def proof(rule wfrec_fixpoint)
  show "wf (inv_image rel meas)" using wf_rel by simp
next
  show "adm_wf (inv_image rel meas) recdef_sh"
  by (auto simp: adm_wf_def recdef_sh_def inj_map_T_Node meas_injective_parametric
           intro!: adm_recdef_meas_rel R.map_cong)  
qed

definition f :: "'a T \<Rightarrow> 'a R" where "f = map_R un_Leaf o g o map_T Leaf"

(* END PRODUCT: *)
lemma f: "f t = recdef f t"
  apply(subst f_def) apply simp
  apply(subst g) unfolding f_def recdef_sh_def 
 (* works for parametric recdef, but we can probably do better *)
  
  term

 recdef_sh_def apply simp


(*  *)

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

lemma bi_unique_Grp: "bi_unique (BNF_Def.Grp A f) = inj_on f A"
  unfolding bi_unique_def Grp_def inj_on_def by auto

lemma bi_unique_eq_onp: "bi_unique (eq_onp P)"
  unfolding eq_onp_def bi_unique_def by simp

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto

section \<open>Input\<close>

declare [[bnf_internals, typedef_overloaded]]

(*
datatype 'a l = N | C 'a "('a * 'a) l"

('a, 'x) G = unit + 'a * 'x
'a F = 'a * 'a

specs = [(G, [[F]])]
*)

bnf_axiomatization 'a F

bnf_axiomatization 'b V  (* only needed for recursion *)

bnf_axiomatization ('a, 'x) G [wits: "'a \<Rightarrow> ('a, 'x) G"]

section \<open>Raw Type\<close>

datatype label = F
type_synonym depth = "label list"
datatype 'a shape = Leaf 'a | Node "'a shape F"
datatype 'a shapeV = LeafV 'a | NodeV "'a shapeV V"
datatype 'a raw = Cons "('a shape, 'a raw) G"

abbreviation "un_Leaf u \<equiv> case u of Leaf x \<Rightarrow> x"
abbreviation "un_Node u \<equiv> case u of Node x \<Rightarrow> x"
abbreviation "un_LeafV u \<equiv> case u of LeafV x \<Rightarrow> x"
abbreviation "un_NodeV u \<equiv> case u of NodeV x \<Rightarrow> x"

abbreviation "un_Cons t \<equiv> case t of Cons x \<Rightarrow> x"


section \<open>Invariant\<close>

fun invar_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> bool" where
  "invar_shape u0 (Leaf u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape u0 (Node f1) = (case u0 of F # u0 \<Rightarrow> pred_F (invar_shape u0) f1 | _ \<Rightarrow> False)"

lemma invar_shape_induct[consumes 1, case_names Leaf Node, induct pred: invar_shape]: 
assumes i: "invar_shape u0 s"
and leaf: "\<And>u. PP [] (Leaf u)" 
and node: "\<And>u0 f1. pred_F (invar_shape u0) f1 \<Longrightarrow> pred_F (PP u0) f1 \<Longrightarrow> PP (F # u0) (Node f1)"
shows "PP u0 s"
using assms by (induct s arbitrary: u0) (auto simp: Cons pred_F_def split: list.splits label.splits)

fun invar_shapeV :: "depth \<Rightarrow> 'a shapeV \<Rightarrow> bool" where
  "invar_shapeV u0 (LeafV u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shapeV u0 (NodeV f1) = (case u0 of F # u0 \<Rightarrow> pred_V (invar_shapeV u0) f1 | _ \<Rightarrow> False)"

lemma G_pred_mono: 
"a \<le> a1 \<Longrightarrow> pred_G p a \<le> pred_G p a1"
  by (simp add: G.pred_mono)

inductive invar :: "depth \<Rightarrow> 'a raw \<Rightarrow> bool" where
  "pred_G (invar_shape u0) (invar (F # u0)) g \<Longrightarrow> invar u0 (Cons g)"
monos G_pred_mono

lemmas invar_simps = invar.simps[of _ "Cons _", unfolded simp_thms(39,40) ex_simps raw.inject]

section \<open>The Type\<close>

definition "wit x = Cons (wit_G (Leaf x))"

lemma invar_wit: "invar [] (wit x)"
  by (auto simp only:
    wit_def invar_simps invar_shape.simps G.pred_map o_def id_apply G.pred_set
    G.set_map list.case dest: G.wit)


typedef 'a T = "{t :: 'a raw. invar [] t}"
  by (rule exI[of _ "wit undefined"]) (auto simp only: invar_wit)

section \<open>Flat and Unflat\<close>

primrec (transfer)
  flat_shape :: "'a F shape \<Rightarrow> 'a shape" where
  "flat_shape (Leaf f1) = Node (map_F Leaf f1)"
| "flat_shape (Node f1) = Node (map_F flat_shape f1)"

primrec (transfer)
  flat_shapeV :: "'a V shapeV \<Rightarrow> 'a shapeV" where
  "flat_shapeV (LeafV f1) = NodeV (map_V LeafV f1)"
| "flat_shapeV (NodeV f1) = NodeV (map_V flat_shapeV f1)"
    
primrec (nonexhaustive)
   unflat_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> 'a F shape" where
  "unflat_shape u0 (Node f1) =
      (case u0 of
        [] \<Rightarrow> Leaf (map_F un_Leaf f1)
      | _ # u0 \<Rightarrow> Node (map_F (unflat_shape u0) f1))"

primrec (nonexhaustive)
   unflat_shapeV :: "depth \<Rightarrow> 'a shapeV \<Rightarrow> 'a V shapeV" where
  "unflat_shapeV u0 (NodeV f1) =
      (case u0 of
        [] \<Rightarrow> LeafV (map_V un_LeafV f1)
      | _ # u0 \<Rightarrow> NodeV (map_V (unflat_shapeV u0) f1))"

primrec (transfer) flat :: "'a F raw \<Rightarrow> 'a raw" where
  "flat (Cons g) = Cons (map_G flat_shape flat g)"

primrec unflat :: "depth \<Rightarrow> 'a raw \<Rightarrow> 'a F raw" where
  "unflat u0 (Cons g) = Cons (map_G (unflat_shape u0) (unflat (F # u0)) g)"


section \<open>Constructor and Selector\<close>

definition T :: "('a, 'a F T) G \<Rightarrow> 'a T" where
  "T g = Abs_T (Cons (map_G Leaf (flat o Rep_T) g))"

definition un_T :: "'a T \<Rightarrow> ('a, 'a F T) G" where
  "un_T t = map_G un_Leaf (Abs_T o unflat []) (un_Cons (Rep_T t))"


section \<open>BNF Instance\<close>

lemma invar_shape_map_closed_raw:
  "\<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u" u])
  apply (auto simp only:
      shape.map invar_shape.simps
      F.pred_map
      o_apply
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invar_shape_map_closed =
  spec[OF invar_shape_map_closed_raw]

lemma invar_shapeV_map_closed_raw:
  "\<forall>u0. invar_shapeV u0 (map_shapeV f u) \<longleftrightarrow> invar_shapeV u0 u"
  apply (rule shapeV.induct[of
    "\<lambda>u. \<forall>u0. invar_shapeV u0 (map_shapeV f u) \<longleftrightarrow> invar_shapeV u0 u" u])
  apply (auto simp only:
      shapeV.map invar_shapeV.simps
      V.pred_map
      o_apply
    elim!: V.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invar_shapeV_map_closed =
  spec[OF invar_shapeV_map_closed_raw]

lemma invar_map_closed_raw:
  "\<forall>u0. invar u0 (map_raw f t) \<longleftrightarrow> invar u0 t"
  apply (induct t)
  apply (auto simp only:
      raw.map invar_simps id_apply o_apply
      G.pred_map invar_shape_map_closed
    elim!: G.pred_mono_strong)
  done
lemmas invar_map_closed =
  spec[OF invar_map_closed_raw]


lift_bnf 'a T
  apply (auto simp only:
      invar_map_closed)
  done




section \<open>Lemmas about Flat, Unflat, Invar\<close>

lemma invar_shape_depth_iff:
  "invar_shape [] x = (\<exists>a. x = Leaf a)"
  "invar_shape (F # u0) x = (\<exists>y. x = Node y \<and> pred_F (invar_shape u0) y)"
  by (cases x; simp add: F.pred_map)+


lemma invar_shapeV_depth_iff:
  "invar_shapeV [] x = (\<exists>a. x = LeafV a)"
  "invar_shapeV (F # u0) x = (\<exists>y. x = NodeV y \<and> pred_V (invar_shapeV u0) y)"
  by (cases x; simp add: F.pred_map)+


lemma flat_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u" u])
  apply (auto simp only:
    unflat_shape.simps flat_shape.simps
    invar_shape.simps F.pred_map F.map_comp
    shape.case invar_shape_depth_iff snoc.simps snoc_neq_Nil
    id_apply o_apply
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas flat_shape_unflat_shape =
  mp[OF spec[OF flat_shape_unflat_shape_raw]]

lemma flat_shape_unflat_shapeV_raw:
  fixes u :: "'a shapeV"
  shows
  "\<forall>u0. invar_shapeV (snoc F u0) u \<longrightarrow> flat_shapeV (unflat_shapeV u0 u) = u"
  apply (rule shapeV.induct[of
    "\<lambda>u. \<forall>u0. invar_shapeV (snoc F u0) u \<longrightarrow> flat_shapeV (unflat_shapeV u0 u) = u" u])
  apply (auto simp only:
    unflat_shapeV.simps flat_shapeV.simps
    invar_shapeV.simps V.pred_map V.map_comp
    shapeV.case invar_shapeV_depth_iff snoc.simps snoc_neq_Nil
    id_apply o_apply
    intro!: trans[OF V.map_cong_pred V.map_ident]
    elim!: V.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas flat_shape_unflat_shapeV =
  mp[OF spec[OF flat_shape_unflat_shapeV_raw]]

lemma unflat_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u" u])
  apply (auto simp only:
      unflat_shape.simps flat_shape.simps invar_shape.simps
      F.pred_map F.map_comp F.pred_True
      shape.case
      id_apply o_apply refl
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas unflat_shape_flat_shape =
  mp[OF spec[OF unflat_shape_flat_shape_raw]]

lemma unflat_shapeV_flat_shapeV_raw:
  fixes u :: "'a V shapeV"
  shows
  "\<forall>u0. invar_shapeV u0 u \<longrightarrow> unflat_shapeV u0 (flat_shapeV u) = u"
  apply (rule shapeV.induct[of
    "\<lambda>u. \<forall>u0. invar_shapeV u0 u \<longrightarrow> unflat_shapeV u0 (flat_shapeV u) = u" u])
  apply (auto simp only:
      unflat_shapeV.simps flat_shapeV.simps invar_shapeV.simps
      V.pred_map V.map_comp V.pred_True
      shapeV.case
      id_apply o_apply refl
    intro!: trans[OF V.map_cong_pred V.map_ident]
    elim!: V.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas unflat_shapeV_flat_shapeV =
  mp[OF spec[OF unflat_shapeV_flat_shapeV_raw]]

lemma invar_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)" u])
  apply (auto simp only:
      flat_shape.simps invar_shape.simps snoc.simps
      F.pred_map F.pred_True
      id_apply o_apply
    elim!: F.pred_mono_strong
    intro: F.pred_mono_strong[OF iffD2[OF fun_cong[OF F.pred_True] TrueI]]
    split: list.splits label.splits)
  done


lemmas invar_shape_flat_shape =
  mp[OF spec[OF invar_shape_flat_shape_raw]]

lemma invar_shapeV_flat_shapeV_raw:
  fixes u :: "'a V shapeV"
  shows
  "\<forall>u0. invar_shapeV u0 u \<longrightarrow> invar_shapeV (snoc F u0) (flat_shapeV u)"
apply (rule shapeV.induct[of
    "\<lambda>u. \<forall>u0. invar_shapeV u0 u \<longrightarrow> invar_shapeV (snoc F u0) (flat_shapeV u)" u])
  apply (auto simp only:
      flat_shapeV.simps invar_shapeV.simps snoc.simps
      V.pred_map V.pred_True
      id_apply o_apply
    elim!: V.pred_mono_strong
    intro: V.pred_mono_strong[OF iffD2[OF fun_cong[OF V.pred_True] TrueI]]
    split: list.splits label.splits)
  done

lemmas invar_shapeV_flat_shapeV =
  mp[OF spec[OF invar_shapeV_flat_shapeV_raw]]

lemma invar_flat_raw: "\<forall>u0. invar u0 x \<longrightarrow> invar (snoc F u0) (flat x)"
  apply (induct x)
  apply (auto simp only:
      flat.simps invar_simps snoc.simps[symmetric] invar_shape_flat_shape id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done


lemmas invar_flat = mp[OF spec[OF invar_flat_raw]]

lemma invar_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)" u])
  apply (auto simp only:
      unflat_shape.simps invar_shape.simps snoc.simps snoc_neq_Nil
      F.pred_map id_apply o_apply refl
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invar_shape_unflat_shape =
  mp[OF spec[OF invar_shape_unflat_shape_raw]]

lemma invar_shapeV_unflat_shapeV_raw:
  fixes u :: "'a shapeV"
  shows
  "\<forall>u0. invar_shapeV (snoc F u0) u \<longrightarrow> invar_shapeV u0 (unflat_shapeV u0 u)"
  apply (rule shapeV.induct[of
    "\<lambda>u. \<forall>u0. invar_shapeV (snoc F u0) u \<longrightarrow> invar_shapeV u0 (unflat_shapeV u0 u)" u])
  apply (auto simp only:
      unflat_shapeV.simps invar_shapeV.simps snoc.simps snoc_neq_Nil
      V.pred_map id_apply o_apply refl
    elim!: V.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invar_shapeV_unflat_shapeV =
  mp[OF spec[OF invar_shapeV_unflat_shapeV_raw]]

lemma invar_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> invar u0 (unflat u0 t)"
  apply (induct t)
  apply (auto simp only:
      unflat.simps invar_simps snoc.simps invar_shape_unflat_shape id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemmas invar_unflat = mp[OF spec[OF invar_unflat_raw]]

lemma flat_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> flat (unflat u0 t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar_simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done


lemmas flat_unflat = mp[OF spec[OF flat_unflat_raw]]

lemma unflat_flat_raw: "\<forall>u0. invar u0 t \<longrightarrow> unflat u0 (flat t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar_simps unflat_shape_flat_shape id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done


lemmas unflat_flat = mp[OF spec[OF unflat_flat_raw]]


section \<open>Constructor is Bijection\<close>

lemma un_T_T: "un_T (T x) = x"
  unfolding T_def un_T_def
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
    intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.case shape.case Rep_T_inverse o_apply
      G.map_comp G.map_ident Rep_T[unfolded mem_Collect_eq] unflat_flat
    intro!: trans[OF G.map_cong G.map_ident]) []
  done


lemma T_un_T: "T (un_T x) = x"
  unfolding T_def un_T_def G.map_comp o_def
  apply (rule iffD1[OF Rep_T_inject])
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (insert Rep_T[simplified, of x])
  apply (rule raw.exhaust[of "Rep_T x"])
  apply (auto simp only:
      raw.case shape.case invar_simps invar_shape_depth_iff
       snoc.simps(1)[of F, symmetric]
      G.pred_map Abs_T_inverse invar_unflat flat_unflat id_apply o_apply mem_Collect_eq
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong) []
  done



section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

lemma flat_shape_map:
  "map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)"
  apply (rule shape.induct[of
    "\<lambda>u. map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)" u])
  apply (auto simp only:
      shape.map flat_shape.simps F.map_comp o_apply
    intro!: F.map_cong0)
  done

lemma map_raw_flat: "map_raw f (flat t) = flat (map_raw (map_F f) t)"
  apply (induct t)
  apply (auto simp only:
      raw.map flat.simps G.map_comp flat_shape_map o_apply
    intro!: G.map_cong0)
  done

lemma map_T: "map_T f (T t) = T (map_G f (map_T (map_F f)) t)"
  unfolding map_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.map shape.map G.map_comp o_apply mem_Collect_eq
     invar_map_closed Abs_T_inverse Rep_T[unfolded mem_Collect_eq] map_raw_flat
    intro!: arg_cong[of _ _ "\<lambda>x. Abs_T (raw.Cons x)"] G.map_cong0) []
  done


subsection \<open>set\<close>

lemma flat_shape_set:
  fixes u :: "'a F shape"
  shows
  "set_shape (flat_shape u) = UNION (set_shape u) set_F"
  apply (rule shape.induct[of
    "\<lambda>u. set_shape (flat_shape u) = UNION (set_shape u) set_F" u])
  apply (auto simp only:
      flat_shape.simps shape.set F.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set_raw_flat:
  "set_raw (flat t) = UNION (set_raw t) set_F"
  apply (induct t)
  apply (auto simp only:
      flat.simps raw.set shape.set G.set_map flat_shape_set
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set_T: "set_T (T g) = set1_G g \<union>
  (\<Union>(set_F ` (\<Union>(set_T ` (set2_G g)))))"
  unfolding set_T_def T_def o_apply
  apply -
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.set shape.set G.set_map set_raw_flat o_apply
        UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong) []
  done


subsection \<open>rel\<close>

lemma flat_shape_rel_raw:
  "(\<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
     rel_shape (rel_F R) u u')"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
    rel_shape (rel_F R) u u'"
    u])
   apply (auto 0 4 simp only:
     invar_shape.simps flat_shape.simps shape.rel_inject
     invar_shape_depth_iff ball_simps id_apply
     F.rel_map pred_F_def F.set_map
     elim!: F.rel_mono_strong
     split: list.splits label.splits)
  done

lemma flat_shape_rel:
  "invar_shape u0 u \<Longrightarrow> invar_shape u0 u' \<Longrightarrow>
    rel_shape R (flat_shape u) (flat_shape u') = rel_shape (rel_F R) u u'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF flat_shape_rel_raw]]]]]; assumption)
  done

lemma rel_raw_flat_raw:
  "\<forall>t' u0. invar u0 t \<longrightarrow> invar u0 t' \<longrightarrow>
   rel_raw R (flat t) (flat t') \<longrightarrow> rel_raw (rel_F R) t t'"
  apply (induct t)
  apply (rule allI)
  apply (case_tac t')
  apply (auto simp only:
      invar_simps flat.simps raw.rel_inject G.rel_map G.pred_set flat_shape_rel G.set_map ball_simps id_apply
    elim!: G.rel_mono_strong)
  done

lemma rel_raw_flat:
  "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
   rel_raw R (flat t) (flat t') = rel_raw (rel_F R) t t'"
  apply (rule iffI[rotated, OF rel_funD[OF flat.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF rel_raw_flat_raw]]]]]; assumption)
  done

lemma rel_T: "rel_T R (T g) (T g') = rel_G R (rel_T (rel_F R)) g g'"
  unfolding rel_T_def T_def vimage2p_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) [2]
  apply (simp only:
    raw.rel_inject G.rel_map shape.rel_inject o_apply
    rel_raw_flat[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]])
  done


section \<open>Induction\<close>

(* Preliminaries *)

(* The "recursion sz_ofs" of elements of 'a raw -- on the second argument of G *)
datatype sz = SCons (un_SCons:"(unit, sz) G")

definition rel :: "sz rel" where "rel = {(sz',sz) . sz' \<in> set2_G (un_SCons sz)}"
  
lemma wf_rel: "wf rel"
  unfolding wf_def by clarify (induct_tac x, auto simp: rel_def)

primrec sz_of :: "'a raw \<Rightarrow> sz" where
  "sz_of (Cons g) = SCons (map_G (\<lambda>_. ()) sz_of g)"

lemma sz_of_unflat: 
  "sz_of (unflat ul r) = sz_of (r :: 'a raw)"
  apply (induct r arbitrary: ul)
  apply (auto intro: G.map_cong simp only: unflat.simps sz_of.simps G.map_comp o_apply)
  done

lemma sz_of_map_raw: 
  "sz_of (map_raw f r) = sz_of r"
  apply (induct r)
  apply (auto intro: G.map_cong simp only: raw.map sz_of.simps G.map_comp o_apply)
  done

lemma un_SCons_sz_of: "un_SCons (sz_of x) = map_G (\<lambda>_. ()) sz_of (un_Cons x)"
  by (cases x) auto

definition meas :: "'a T \<Rightarrow> sz" where "meas = sz_of o Rep_T"
    
lemma sz_of_set_G:
  assumes "r' \<in> set2_G (un_Cons r)"
  shows "sz_of r' \<in> set2_G (un_SCons (sz_of r))"
  using assms unfolding un_SCons_sz_of G.set_map by auto
  
lemma meas_set2_G:
assumes "t' \<in> set2_G (un_T t)"
shows "meas t' \<in> set2_G (un_SCons (meas t))"
proof-
  define r where r: "r = Rep_T t"
  hence t: "t = Abs_T r" by (simp add: Rep_T_inverse) 
  obtain r' where t': "t' = Abs_T (unflat [] r')"
  and r': "r' \<in> set2_G (un_Cons r)" 
    using assms unfolding un_T_def G.set_map r by auto
  have "invar [] r" unfolding r using Rep_T by blast
  hence "invar [F] r'" using r' by (metis axiom12_G invar.simps raw.case)
  hence "invar [] (unflat [] r')" by (simp add: invar_unflat)
  hence ur': "unflat [] r' = Rep_T t'" by (simp add: Abs_T_inverse t')
  have "sz_of (unflat [] r') \<in> set2_G (un_SCons (sz_of r))"
    using sz_of_set_G[OF r'] unfolding sz_of_unflat .  
  thus ?thesis unfolding meas_def r ur' by simp
qed


(*  THE ASSUMPTIONS: *)

consts P :: "'a T \<Rightarrow> bool"

axiomatization where 
P_ind: "\<And>g. (\<And> t'. t' \<in> set2_G g \<Longrightarrow> P t') \<Longrightarrow> P (T g :: 'a T)" 
(* No longer needed: 
   P_param: "\<And>R :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> rel_fun (rel_T R) (=) P P" 
*)
  
lemma P_wf_F:
fixes t :: "'a T"
assumes "\<And> t' :: 'a F T. (meas t', meas t) \<in> rel \<Longrightarrow> P t'" 
shows "P t" 
proof-
  obtain g where t: "t = T g" by (metis T_un_T)
  hence g: "g = un_T t" by (simp add: un_T_T)
  show ?thesis unfolding t apply(rule P_ind)
  by (auto intro!: assms meas_set2_G simp: rel_def g)
qed

(* Applying the rule for multi-type induction (with M = sz), we obtain: *)
lemma wf_rel_P: "wf (rel::sz rel) \<Longrightarrow> P (t::'a T)"
  sorry
  
(* Now, since rel is a well-order, we obtain: *)
lemma P: "P (t::'a T)"
  using wf_rel_P[OF wf_rel] .

(* PIPELINE: User states P; in response, system defines the polymorphic P (as before) 
and asks the user to prove P_ind; after user proves this, system applies axiom 
to mobtain wf_rel_O, and then proves P. *)
  


section \<open>Recursion\<close>

(*normal datatype recursor
  (('b, 'a) G \<Rightarrow> 'a) \<Rightarrow>
  'b T \<Rightarrow> 'a
*)

(*generalized recursor
  (\<forall>'a. ('a D, 'b F R) G \<Rightarrow> 'b R) \<Rightarrow>
  (\<forall>'a. 'a D F \<Rightarrow> 'a F D) \<Rightarrow>
  (\<forall>'a. 'a D T \<Rightarrow> 'b R) 
*)

typedecl 'a R 

(* bnf_axiomatization 'a D *)
typedecl 'a D

consts defobj :: "('a D, 'a V R) G \<Rightarrow> 'a R"
consts argobj :: "'a D F \<Rightarrow> 'a V D"

(* axiomatization where
  argobj_transfer: "\<And>A.
    bi_unique A \<Longrightarrow>
    rel_fun (rel_F (rel_D A)) (rel_D (rel_V A)) argobj argobj"
*)

lemma eq_onpE: "eq_onp R x y \<Longrightarrow> (R x \<Longrightarrow> x = y \<Longrightarrow> thesis) \<Longrightarrow> thesis"
  unfolding eq_onp_def by auto

lemma eq_onpI: "R x \<Longrightarrow> x = y \<Longrightarrow> eq_onp R x y"
  unfolding eq_onp_def by auto

lemma rel_fun_mono_strong:
  "rel_fun X A f g \<Longrightarrow> (\<And>x y. Y x y \<Longrightarrow> X x y) \<Longrightarrow>
   (\<And>x y. (x, y) \<in> {(f a, g b) | a b. Y a b} \<Longrightarrow> A x y \<Longrightarrow> B x y) \<Longrightarrow> rel_fun Y B f g"
  unfolding rel_fun_def by force
    
(* lemma argobj_invar: 
"pred_fun (pred_F (pred_D A)) (pred_D (pred_V A)) argobj"
  unfolding fun_pred_rel F.rel_eq_onp[symmetric] V.rel_eq_onp[symmetric] D.rel_eq_onp[symmetric]
  by (rule argobj_transfer; rule bi_unique_eq_onp)


lemma argobj_pred: 
assumes "pred_F (pred_D A) x" 
shows "(pred_D (pred_V A)) (argobj x)"
using assms argobj_invar by auto
*)

lemma V_map_cong_id: 
  assumes "\<And> a. a \<in> set_V r \<Longrightarrow> ff a = a"
  shows "map_V ff r = r"
  by (metis V.map_cong0 V.map_id0 assms id_apply)

(* lemma argobj_natural:
  "inj_on a (\<Union>(set_D ` set_F x)) \<Longrightarrow>
  argobj (map_F (map_D a) x) = map_D (map_V a) (argobj x)"
  using rel_funD[OF argobj_transfer, of "BNF_Def.Grp (\<Union>(set_D ` set_F x)) a"]
  unfolding F.rel_Grp V.rel_Grp D.rel_Grp bi_unique_Grp
  by (auto simp add: Grp_def)
*)

(* lemma argobj_flat_shape_natural: 
fixes x :: "('a V shapeV) D F"
  shows
    "pred_F (pred_D (invar_shapeV u)) x \<Longrightarrow>
     argobj (map_F (map_D flat_shapeV) x) =
     map_D (map_V flat_shapeV) (argobj x)"
apply(rule argobj_natural)
unfolding pred_F_def pred_D_def inj_on_def  
by simp (metis unflat_shapeV_flat_shapeV)
*)

definition recdef :: "('a V D T \<Rightarrow> ('a V) R) \<Rightarrow> 'a D T \<Rightarrow> 'a R"
  where "recdef f t = defobj (map_G id (f o map_T argobj) (un_T t))"
  
lemma Rep_T_map_T: "Rep_T (map_T f t) = map_raw f (Rep_T t)"
  by (simp only:
    map_T_def o_apply Abs_T_inverse invar_map_closed mem_Collect_eq Rep_T[unfolded mem_Collect_eq])
  
lemma meas_map_R_argobj: 
"meas (map_T argobj t) = meas t"
by (simp add: meas_def sz_of_map_raw Rep_T_map_T)
  
lemma adm_recdef_meas_rel: 
assumes "\<And>t'. (meas t', meas t) \<in> rel \<Longrightarrow> f t' = g t'"
shows "recdef f t = recdef g t" 
unfolding recdef_def apply(rule cong[of defobj])
by (auto intro!: G.map_cong assms 
         simp: rel_def meas_set2_G meas_map_R_argobj)

(* APPLY GENERIC RECURSION RULE FOR 
  'a T := 'a D T and 'a F := 'a V, 
  obtaining: *)
consts f :: "'a D T \<Rightarrow> 'a R"
axiomatization where f_def: "\<And> t::'a D T. f t = recdef f t"

(* The infer the nonuniform primitive recursion theorem: *) 
theorem "f (T g) = defobj (map_G id (f o map_T argobj) g)"
  using f_def[of "T g"] unfolding recdef_def un_T_T .

end