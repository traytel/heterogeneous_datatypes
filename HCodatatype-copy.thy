theory HCodatatype-copy
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

lemma bi_unique_Grp: "bi_unique (BNF_Def.Grp A f) = inj_on f A"
  unfolding bi_unique_def Grp_def inj_on_def by auto

lemma bi_unique_eq_onp: "bi_unique (eq_onp P)"
  unfolding eq_onp_def bi_unique_def by simp

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto

section \<open>Input\<close>

declare [[bnf_internals, typedef_overloaded]]

(*
datatype 'a l = N | C 'a "('a * 'a) l"

('a, 'x) G = unit + 'a * 'x
'a F = 'a * 'a

specs = [(G, [[F]])]
*)

bnf_axiomatization 'a F [wits: "'a \<Rightarrow> 'a F"]

bnf_axiomatization ('a, 'x) G [wits: "'a \<Rightarrow> 'x \<Rightarrow> ('a, 'x) G"]

section \<open>Raw Type\<close>

datatype label = F
type_synonym depth = "label list"
datatype 'a shape = Leaf 'a | Node "'a shape F"
codatatype 'a raw = Cons (unCons: "('a shape, 'a raw) G")

abbreviation "un_Leaf u \<equiv> case u of Leaf x \<Rightarrow> x"
abbreviation "un_Node u \<equiv> case u of Node x \<Rightarrow> x"
abbreviation "un_Cons t \<equiv> case t of Cons x \<Rightarrow> x"


section \<open>Invariant\<close>

primrec invar_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> bool" where
  "invar_shape u0 (Leaf u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape u0 (Node f1) = (case u0 of F # u0 \<Rightarrow> pred_F (invar_shape u0) f1 | _ \<Rightarrow> False)"

coinductive invar :: "depth \<Rightarrow> 'a raw \<Rightarrow> bool" where
  "pred_G (invar_shape u0) (\<lambda>x. invar (F # u0) x) g \<Longrightarrow> invar u0 (Cons g)"
monos G.pred_mono

lemmas invar_simps = invar.simps[of _ "Cons _", unfolded simp_thms(39,40) ex_simps raw.inject]

section \<open>The Type\<close>

primrec mk_shape where
  "mk_shape [] x = Leaf x"
| "mk_shape (_ # u0) x = Node (wit_F (mk_shape u0 x))"

primcorec wit :: "label list \<Rightarrow> 'a \<Rightarrow> 'a raw" where
  "wit u0 x = Cons (map_G id (case_prod wit) (wit_G (mk_shape u0 x) (F # u0, x)))"

lemma invar_shape_mk_shape: "invar_shape u0 (mk_shape u0 x)"
  apply (induct u0)
   apply (auto split: label.splits simp: F.pred_set dest!: F.wit)
  done
  
lemma invar_wit: "invar u0 (wit u0 x)"
  apply (coinduction arbitrary: u0)
  apply (subst wit.ctr)
  apply (auto simp: G.set_map G.pred_set invar_shape_mk_shape dest!: G.wit)
  done

typedef 'a T = "{t :: 'a raw. invar [] t}"
  by (rule exI[of _ "wit [] undefined"]) (auto simp only: invar_wit)


section \<open>Flat and Unflat\<close>

primrec (transfer)
  flat_shape :: "'a F shape \<Rightarrow> 'a shape" where
  "flat_shape (Leaf f1) = Node (map_F Leaf f1)"
| "flat_shape (Node f1) = Node (map_F flat_shape f1)"

primrec (nonexhaustive)
   unflat_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> 'a F shape" where
  "unflat_shape u0 (Node f1) =
      (case u0 of
        [] \<Rightarrow> Leaf (map_F un_Leaf f1)
      | _ # u0 \<Rightarrow> Node (map_F (unflat_shape u0) f1))"

primcorec (transfer) flat :: "'a F raw \<Rightarrow> 'a raw" where
  "flat r = Cons (map_G flat_shape flat (un_Cons r))"

lemmas flat_simps = flat.ctr[of "Cons z" for z, unfolded raw.sel]

primcorec unflat :: "depth \<Rightarrow> 'a raw \<Rightarrow> 'a F raw" where
  "unflat u0 r = Cons (map_G (unflat_shape u0) (unflat (F # u0)) (un_Cons r))"

lemmas unflat_simps = unflat.ctr[of _ "Cons z" for z, unfolded raw.sel]


section \<open>Constructor and Selector\<close>

definition T :: "('a, 'a F T) G \<Rightarrow> 'a T" where
  "T g = Abs_T (Cons (map_G Leaf (flat o Rep_T) g))"

definition un_T :: "'a T \<Rightarrow> ('a, 'a F T) G" where
  "un_T t = map_G un_Leaf (Abs_T o unflat []) (un_Cons (Rep_T t))"


section \<open>BNF Instance\<close>

lemma invar_shape_map_closed_raw:
  "\<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u" u])
  apply (auto simp only:
      shape.map invar_shape.simps
      F.pred_map
      o_apply
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invar_shape_map_closed =
  spec[OF invar_shape_map_closed_raw]

lemma invar_map_closed:
  "invar u0 (map_raw f t) = invar u0 (t :: 'a raw)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_invar_map_raw_closed_cotac ctxt Ps Qs induct raw_exhausts raw_injects raw_maps
          invar_simps invar_shape_map_closed
          G_pred_maps G_pred_mono_strongs =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (rtac ctxt iffI),
              rtac ctxt (infer_instantiate' ctxt (map SOME Ps) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 1)] exhaust;
                  val elim = eresolve_tac ctxt G_pred_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_injects, raw_maps, invar_simps,
                    invar_shape_map_closed, G_pred_maps, @{thms simp_thms ex_simps o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM_N 100 o
                    FIRST' [hyp_subst_tac ctxt, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (K all_tac),
              rtac ctxt (infer_instantiate' ctxt (map SOME Qs) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (conjI :: exI :: disjI1 :: []);
                  val elim = eresolve_tac ctxt G_pred_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_maps, invar_simps,
                    invar_shape_map_closed, G_pred_maps, @{thms o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            case_mut ( unfold_tac ctxt @{thms ex_simps simp_thms(39,40)} THEN
            HEADGOAL (EVERY' [rtac ctxt impI, rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i),
                dtac ctxt (mk_conjunctN n i),
                rtac ctxt iffI,

                etac ctxt thin_rl,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt,

                dtac ctxt asm_rl,
                etac ctxt thin_rl,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)])) (all_tac)
          end
      in
        mk_invar_map_raw_closed_cotac @{context}
          [@{cterm "\<lambda>x. \<lambda>y :: 'a raw. \<exists>u0 t. x = u0 \<and> y = t \<and> invar u0 (map_raw f t)"}]
          [@{cterm "\<lambda>x. \<lambda>y :: 'b raw. \<exists>u0 t. x = u0 \<and> y = map_raw f t \<and> invar u0 t"}]
          @{thm invar.coinduct}
          @{thms raw.exhaust}
          @{thms raw.inject}
          @{thms raw.map}
          @{thms invar_simps}
          @{thms invar_shape_map_closed}
          @{thms G.pred_map}
          @{thms G.pred_mono_strong}
      end\<close>)
(*
  apply (rule iffI)
  apply (rule invar.coinduct[of "\<lambda>x. \<lambda>y :: 'b raw. \<exists>u0 t. x = u0 \<and> y = t \<and> invar u0 (map_raw f t)"])
  apply (rule exI conjI refl)+
  apply assumption
  
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ _ t
    apply (rule raw.exhaust[of t])
    apply (auto simp only: invar_simps G.pred_map invar_shape_map_closed
      ex_simps simp_thms(39,40) raw.inject raw.map o_apply elim!: G.pred_mono_strong) []
    done

  apply (rule invar.coinduct[of "\<lambda>x. \<lambda>y :: 'a raw. \<exists>u0 t. x = u0 \<and> y = map_raw f t \<and> invar u0 t"])
  apply (rule exI conjI refl)+
  apply assumption
  
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ _ t
    apply (rule raw.exhaust[of t])
    apply (auto simp only: invar_simps G.pred_map invar_shape_map_closed
      ex_simps simp_thms(39,40) raw.inject raw.map o_apply elim!: G.pred_mono_strong) [] 
    done
  done*)

lift_bnf 'a T
  apply (auto simp only:
      invar_map_closed)
  done


section \<open>Lemmas about Flat, Unflat, Invar\<close>

lemma invar_shape_depth_iff:
  "invar_shape [] x = (\<exists>a. x = Leaf a)"
  "invar_shape (F # u0) x = (\<exists>y. x = Node y \<and> pred_F (invar_shape u0) y)"
  by (cases x; simp add: F.pred_map)+

lemma flat_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u" u])
  apply (auto simp only:
    unflat_shape.simps flat_shape.simps
    invar_shape.simps F.pred_map F.map_comp
    shape.case invar_shape_depth_iff snoc.simps snoc_neq_Nil
    id_apply o_apply
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas flat_shape_unflat_shape =
  mp[OF spec[OF flat_shape_unflat_shape_raw]]

lemma unflat_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u" u])
  apply (auto simp only:
      unflat_shape.simps flat_shape.simps invar_shape.simps
      F.pred_map F.map_comp F.pred_True
      shape.case
      id_apply o_apply refl
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas unflat_shape_flat_shape =
  mp[OF spec[OF unflat_shape_flat_shape_raw]]

lemma invar_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)" u])
  apply (auto simp only:
      flat_shape.simps invar_shape.simps snoc.simps
      F.pred_map F.pred_True
      id_apply o_apply
    elim!: F.pred_mono_strong
    intro: F.pred_mono_strong[OF iffD2[OF fun_cong[OF F.pred_True] TrueI]]
    split: list.splits label.splits)
  done

lemmas invar_shape_flat_shape =
  mp[OF spec[OF invar_shape_flat_shape_raw]]

lemma invar_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)" u])
  apply (auto simp only:
      unflat_shape.simps invar_shape.simps snoc.simps snoc_neq_Nil
      F.pred_map id_apply o_apply refl
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invar_shape_unflat_shape =
  mp[OF spec[OF invar_shape_unflat_shape_raw]]


lemma invar_flat: "invar u0 t \<longrightarrow> invar (snoc F u0) (flat t)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_invar_raw_flat_raw_cotac ctxt Ps induct raw_exhausts raw_injects raw_maps
          raw_sels invar_simps flat_simps invar_shape_flat_shape
          G_pred_maps G_rel_mono_strongs =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (rtac ctxt impI),
              rtac ctxt (infer_instantiate' ctxt (map SOME Ps) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (conjI :: exI :: disjI1 :: []);
                  val elim = eresolve_tac ctxt G_rel_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_injects, raw_maps, raw_sels, invar_simps,
                    flat_simps, invar_shape_flat_shape, G_pred_maps, @{thms snoc.simps[symmetric] o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (case_mut (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)]) (K all_tac))
          end
      in
        mk_invar_raw_flat_raw_cotac @{context}
          [@{cterm "\<lambda>x. \<lambda>y :: 'a raw. \<exists>u0 t. x = (snoc F u0) \<and> y = (flat t) \<and> invar u0 t"}]
          @{thm invar.coinduct}
          @{thms raw.exhaust}
          @{thms raw.inject}
          @{thms raw.map}
          @{thms raw.sel}
          @{thms invar_simps}
          @{thms flat_simps}
          @{thms invar_shape_flat_shape}
          @{thms G.pred_map}
          @{thms G.pred_mono_strong}
      end\<close>)
(*
  apply (rule impI)
  apply (rule invar.coinduct[of "\<lambda>x y. \<exists>u0 t. x = (snoc F u0) \<and> y = (flat t) \<and> invar u0 t"])
  apply (rule exI conjI refl)+
  apply assumption
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule raw.exhaust[of t])
    apply (auto simp only: raw.inject
        flat_simps invar_simps invar_shape_flat_shape
        G.pred_map
        id_apply o_apply snoc.simps[symmetric] ex_simps simp_thms(39,40)
      elim!: G.pred_mono_strong |
      ((rule exI)+, rule conjI[OF refl]))+
    done
  done*)

lemma invar_unflat: "invar (snoc F u0) t \<longrightarrow> invar u0 (unflat u0 t)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_invar_raw_unflat_raw_cotac ctxt Ps induct raw_exhausts raw_injects raw_maps
          raw_sels invar_simps unflat_simps invar_shape_unflat_shape
          G_pred_maps G_rel_mono_strongs =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (rtac ctxt impI),
              rtac ctxt (infer_instantiate' ctxt (map SOME Ps) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (conjI :: exI :: disjI1 :: []);
                  val elim = eresolve_tac ctxt G_rel_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_injects, raw_maps, raw_sels, invar_simps,
                    unflat_simps, invar_shape_unflat_shape, G_pred_maps, @{thms snoc.simps[symmetric] o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (case_mut (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)]) (K all_tac))
          end
      in
        mk_invar_raw_unflat_raw_cotac @{context}
          [@{cterm "\<lambda>x. \<lambda>y :: ('a F) raw. \<exists>u0 t. x = u0 \<and> y = (unflat u0 t) \<and> invar (snoc F u0) t"}]
          @{thm invar.coinduct}
          @{thms raw.exhaust}
          @{thms raw.inject}
          @{thms raw.map}
          @{thms raw.sel}
          @{thms invar_simps}
          @{thms unflat_simps}
          @{thms invar_shape_unflat_shape}
          @{thms G.pred_map}
          @{thms G.pred_mono_strong}
      end\<close>)
(*
  apply (rule impI)
  apply (rule invar.coinduct[of "\<lambda>x y. \<exists>u0 t. x = u0 \<and> y = (unflat u0 t) \<and> invar (snoc F u0) t"])
  apply (rule exI conjI refl)+
  apply assumption
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule raw.exhaust[of t])
    apply (auto simp only: raw.inject
        unflat_simps invar_simps invar_shape_unflat_shape
        G.pred_map
        id_apply o_apply snoc.simps[symmetric] ex_simps simp_thms(39,40)
      elim!: G.pred_mono_strong |
      ((rule exI)+, rule conjI[OF refl]))+
    done
  done*)

lemma flat_unflat: "invar (snoc F u0) t \<longrightarrow> flat (unflat u0 t) = t"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_flat_raw_unflat_raw_cotac ctxt Ps xs ys induct raw_exhausts raw_sels
          flat_simps unflat_simps invar_simps flat_shape_unflat_shapes
          G_rel_maps G_pred_rels G_rel_mono_strongs =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (rtac ctxt impI),
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            HEADGOAL (RANGE (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (allI :: impI :: conjI :: exI :: []);
                  val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) :: G_rel_mono_strongs);
                  val simp = asm_full_simp_tac (ss_only (flat [raw_sels, flat_simps, unflat_simps, invar_simps,
                    flat_shape_unflat_shapes, G_rel_maps, G_pred_rels, @{thms snoc.simps[symmetric] eq_onp_def
                    simp_thms snoc_neq_Nil}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts)) THEN
            HEADGOAL (case_mut (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)]) (K all_tac))
          end
      in
        mk_flat_raw_unflat_raw_cotac @{context}
          [@{cterm "\<lambda>l r :: 'a raw. \<exists>u0 t. l = flat (unflat u0 t) \<and> r = t \<and> invar (snoc F u0) t"}]
          [@{cterm "flat (unflat u0 t)"}]
          [@{cterm t}]
          @{thm raw.coinduct}
          @{thms raw.exhaust}
          @{thms raw.sel}
          @{thms flat_simps}
          @{thms unflat_simps}
          @{thms invar_simps}
          @{thms flat_shape_unflat_shape}
          @{thms G.rel_map}
          @{thms G.pred_rel}
          @{thms G.rel_mono_strong}
      end\<close>)
(*
lemma flat_unflat: "invar (snoc F u0) t \<longrightarrow> flat (unflat u0 t) = t"
  apply (rule impI)
  apply (rule raw.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = flat (unflat u0 t) \<and> r = t \<and> invar (snoc F u0) t"
    "flat (unflat u0 t)" t])
  apply ((rule exI conjI refl | assumption)+)
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule raw.exhaust[of t])
    apply (auto simp only: raw.sel unflat_simps flat_simps G.rel_map invar_simps G.pred_rel
      flat_shape_unflat_shape snoc.simps[symmetric] eq_onp_def elim!: G.rel_mono_strong)
    done
  done*)

lemma unflat_flat: "invar u0 t \<longrightarrow> unflat u0 (flat t) = t"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_unflat_raw_flat_raw_cotac ctxt Ps xs ys induct raw_exhausts raw_sels
          flat_simps unflat_simps invar_simps flat_shape_unflat_shapes
          G_rel_maps G_pred_rels G_rel_mono_strongs =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (rtac ctxt impI),
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (exI :: []);
                  val elim = eresolve_tac ctxt (conjE :: G_rel_mono_strongs);
                  val simp = asm_full_simp_tac (ss_only (flat [raw_sels, flat_simps, unflat_simps, invar_simps,
                    flat_shape_unflat_shapes, G_rel_maps, G_pred_rels, @{thms eq_onp_def}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (case_mut (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)]) (K all_tac))
          end
      in
        mk_unflat_raw_flat_raw_cotac @{context}
          [@{cterm "\<lambda>l r :: ('a F) raw. \<exists>u0 t. l = unflat u0 (flat t) \<and> r = t \<and> invar u0 t"}]
          [@{cterm "unflat u0 (flat t)"}]
          [@{cterm t}]
          @{thm raw.coinduct}
          @{thms raw.exhaust}
          @{thms raw.sel}
          @{thms flat_simps}
          @{thms unflat_simps}
          @{thms invar_simps}
          @{thms unflat_shape_flat_shape}
          @{thms G.rel_map}
          @{thms G.pred_rel}
          @{thms G.rel_mono_strong}
      end\<close>)


section \<open>Constructor is Bijection\<close>

lemma un_T_T: "un_T (T x) = x"
  unfolding T_def un_T_def
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
    intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.case shape.case Rep_T_inverse o_apply
      G.map_comp G.map_ident Rep_T[unfolded mem_Collect_eq] unflat_flat
    intro!: trans[OF G.map_cong G.map_ident]) []
  done

lemma T_un_T: "T (un_T x) = x"
  unfolding T_def un_T_def G.map_comp o_def
  apply (rule iffD1[OF Rep_T_inject])
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (insert Rep_T[simplified, of x])
  apply (rule raw.exhaust[of "Rep_T x"])
  apply (auto simp only:
      raw.case shape.case invar_simps invar_shape_depth_iff
       snoc.simps(1)[of F, symmetric]
      G.pred_map Abs_T_inverse invar_unflat flat_unflat id_apply o_apply mem_Collect_eq
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong) []
  done


section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

lemma flat_shape_map:
  "map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)"
  apply (rule shape.induct[of
    "\<lambda>u. map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)" u])
  apply (auto simp only:
      shape.map flat_shape.simps F.map_comp o_apply
    intro!: F.map_cong0)
  done

lemma map_raw_flat: "map_raw f (flat t) = flat (map_raw (map_F f) t)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
      fun mk_map_flat_raw_cotac ctxt Ps xs ys induct raw_exhausts raw_sels
        raw_maps flat_simps map_flat_shapes
        G_rel_maps G_rel_refl =
        let
          val n = length Ps
          fun case_mut a b = if n <> 1 then a else b;
          val inst_cterms = Ps @ ([xs, ys] |> transpose |> flat);
        in
          HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (K all_tac),
            rtac ctxt (infer_instantiate' ctxt (map SOME inst_cterms) induct),
            case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]))]) THEN
          (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
            REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
            hyp_subst_tac ctxt,
            Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
              let
                val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 2)] exhaust;
                val intro = resolve_tac ctxt (exI :: G_rel_refl);
                val simp = asm_full_simp_tac (ss_only (flat [raw_sels, raw_maps, flat_simps,
                  map_flat_shapes, G_rel_maps, @{thms snoc.simps[symmetric] eq_onp_def}]) ctxt);
              in
                HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                  FIRST' [hyp_subst_tac ctxt, intro, simp]])
              end) ctxt])) raw_exhausts) THEN
          HEADGOAL (case_mut (
            rtac ctxt impI THEN'
            CONJ_WRAP' (fn i => EVERY' [
              dtac ctxt (mk_conjunctN n i),
              etac ctxt mp,
              REPEAT_DETERM o FIRST' [
                resolve_tac ctxt [exI, conjI, refl],
                assume_tac ctxt]]) (1 upto n)) (K all_tac))
        end
      in
        mk_map_flat_raw_cotac @{context}
          [@{cterm "\<lambda>l r. \<exists>t. l = map_raw f (flat t) \<and> r = flat (map_raw (map_F f) t)"}]
          [@{cterm "map_raw f (flat t)"}]
          [@{cterm "flat (map_raw (map_F f) t)"}]
          @{thm raw.coinduct}
          @{thms raw.exhaust}
          @{thms raw.sel}
          @{thms raw.map}
          @{thms flat_simps}
          @{thms flat_shape_map}
          @{thms G.rel_map}
          @{thms G.rel_refl}
      end\<close>)
(*
  apply (coinduction arbitrary: t rule: raw.coinduct)
  apply (rename_tac t)
  apply (case_tac t)
  apply (auto simp only: raw.sel G.rel_map
      raw.map flat_simps flat_shape_map o_apply
    intro!: G.rel_refl)
  done*)

lemma map_T: "map_T f (T t) = T (map_G f (map_T (map_F f)) t)"
  unfolding map_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.map shape.map G.map_comp o_apply mem_Collect_eq
     invar_map_closed Abs_T_inverse Rep_T[unfolded mem_Collect_eq] map_raw_flat
    intro!: arg_cong[of _ _ "\<lambda>x. Abs_T (raw.Cons x)"] G.map_cong0) []
  done


subsection \<open>set\<close>

lemma flat_shape_set:
  fixes u :: "'a F shape"
  shows
  "set_shape (flat_shape u) = UNION (set_shape u) set_F"
  apply (rule shape.induct[of
    "\<lambda>u. set_shape (flat_shape u) = UNION (set_shape u) set_F" u])
  apply (auto simp only:
      flat_shape.simps shape.set F.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma subset_UN_I:
  "(\<And>x y. x \<in> C y \<Longrightarrow> \<forall>t. fl t = y \<longrightarrow> x \<in> (\<Union>x\<in>C' t. B x)) \<Longrightarrow> C (fl t) \<subseteq> (\<Union>x\<in>C' t. B x)"
  by blast

lemma UN_E': "b \<in> (\<Union>x\<in>A. B x) \<Longrightarrow> (\<And>x. x \<in> A \<Longrightarrow> \<forall>b. b \<in> B x \<longrightarrow> b \<in> C) \<Longrightarrow> b \<in> C"
  by blast


lemma set_raw_flat_with_orig_proof:
  "set_raw (flat t) = UNION (set_raw t) set_F"
  apply (rule equalityI)
  apply (rule subset_UN_I[of set_raw flat set_F set_raw t])
  apply (erule raw.set_induct)
  apply (rule allI impI)+
  subgoal for _ _ g sh a t
    apply (rule raw.exhaust[of t])
    apply (auto 0 4 simp only:
        flat_simps raw.set shape.set G.set_map flat_shape_set
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong) []
    done
  apply (rule allI impI)+
  subgoal for _ _ g sh a t
    apply (rule raw.exhaust[of t])
    apply (auto 0 4 simp only:
        flat_simps raw.set G.set_map
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong) []
    done

  apply (rule subsetI)
  apply (erule UN_E')
  apply (erule raw.set_induct)
  apply (rule allI impI)+
  apply (auto simp only:
      flat_simps raw.set shape.set G.set_map flat_shape_set
      UN_simps UN_Un UN_Un_distrib Un_ac) []
  apply (rule allI impI)+
  apply (auto simp only:
      flat_simps raw.set G.set_map
      UN_simps UN_Un UN_Un_distrib Un_ac) []
  done

(* set_induct thm with same form as in mutual case *)
lemma raw_set_induct: 
  "(\<And>z xa xb. xa \<in> set1_G z \<Longrightarrow> xb \<in> set_shape xa \<Longrightarrow> P xb (raw.Cons z)) \<Longrightarrow>
   (\<And>z xc xd. xc \<in> set2_G z \<Longrightarrow> xd \<in> set_raw xc \<Longrightarrow> P xd xc \<Longrightarrow> P xd (raw.Cons z)) \<Longrightarrow>
   \<forall>x \<in> set_raw A. P x A"
  apply (rule ballI)
  apply (rule raw.set_induct[of _ A P]; assumption)
  done

lemma set_raw_flat_with_mutual_apply_proof:
  "set_raw (flat t) = UNION (set_raw t) set_F"
  apply (rule rev_mp[of "\<forall>t :: 'a raw. (\<forall>y \<in> set_raw t. \<forall>t'. flat t' = t \<longrightarrow> y \<in> UNION (set_raw t') set_F)"])
   apply (rule allI)
  apply (rule ballI)
   apply (erule raw.set_induct; (rule allI impI)+)
  subgoal for _ _ _ _ _ t
    apply (rule raw.exhaust[of t])
    apply (auto 0 4 simp only:
        flat_simps raw.set G.set_map flat_shape_set
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong)
  done
  subgoal for _ _ _ _ _ t
    apply (rule raw.exhaust[of t])
    apply (auto 0 4 simp only:
        flat_simps raw.set G.set_map flat_shape_set
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong)
    done
  
  apply (rule impI)
  apply (rule equalityI)
  apply (rule subsetI)
  apply (erule allE)
  apply (drule bspec)
  apply (assumption)
  apply (erule allE)
  apply (drule mp)
  apply (rule refl)
   apply assumption
  
  
  apply (rule subsetI)
  apply (erule UN_E)
  apply (rule rev_mp)
   apply (erule raw.set_induct[of
      _ t "\<lambda>f t :: 'a F raw. \<forall>a \<in> set_F f. a \<in> set_raw (flat t)"];
    auto simp only: flat_simps raw.set G.set_map flat_shape_set
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply (rule impI)
    apply (drule bspec, assumption)+
    apply assumption
  done


lemma set_raw_flat_with_tac:
  "set_raw (flat t) = UNION (set_raw t) set_F"
by (tactic \<open>let open Ctr_Sugar_Util open BNF_Util open BNF_FP_Rec_Sugar_Util
        fun mk_set_flat_raw_cotac ctxt lemma Pss us inducts nr_occ raw_exhausts raw_injects raw_sets
          flat_simps set_flat_shapes G_set_maps =
          let
            val nr_par = (length Pss);
            val nr_Ts = length (hd Pss);
            val nr_lives = nr_par + nr_occ;
            fun case_mut a b = if nr_Ts <> 1 then a else b;
            val rep_raw_exhausts = map (replicate nr_lives) raw_exhausts |> flat;
            val UnI_pars = map (mk_UnIN nr_Ts) (1 upto nr_par);
            val UnI_Gs = map (mk_UnIN nr_lives) (1 upto nr_lives);
            val solve = EVERY' [rtac ctxt equalityI, rtac ctxt subsetI,
              etac ctxt allE, dtac ctxt bspec, assume_tac ctxt,
              etac ctxt allE, dtac ctxt mp, rtac ctxt refl, assume_tac ctxt];
          in
            HEADGOAL (EVERY' [rtac ctxt (infer_instantiate' ctxt [SOME lemma] rev_mp),
              REPEAT_DETERM_N nr_Ts o rtac ctxt allI,
              case_mut (rtac ctxt (hd inducts))
              (rtac ctxt ballI THEN' etac ctxt (hd inducts)) THEN_ALL_NEW
              REPEAT_DETERM o resolve_tac ctxt [allI, impI],
              RANGE (map (fn exhaust =>
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd o List.last) params] exhaust;
                in
                  HEADGOAL (rtac ctxt exhaust_inst)
                end) ctxt THEN' REPEAT_DETERM_N 100 o
                let
                  val elim = eresolve_tac ctxt (imageE :: UnE :: @{thms UN_E});
                  val dest_IH = etac ctxt allE THEN' dtac ctxt mp THEN' rtac ctxt refl;
                  val solve = EVERY' [resolve_tac ctxt UnI_pars, etac ctxt @{thm UN_I[rotated]},
                    resolve_tac ctxt UnI_Gs, etac ctxt @{thm UN_I[rotated]}, assume_tac ctxt];
                  val simp = CHANGED o asm_full_simp_tac
                    (ss_only (flat [raw_sets, raw_injects, flat_simps, set_flat_shapes,
                      G_set_maps]) ctxt);
                in
                  FIRST' [hyp_subst_tac ctxt, elim, solve, dest_IH, simp]
                end) rep_raw_exhausts)]) THEN
            HEADGOAL (case_mut (K all_tac) (EVERY' [
              rtac ctxt impI,
              solve,
              rtac ctxt subsetI,
              etac ctxt @{thm UN_E}])) THEN
            HEADGOAL (EVERY' (map2 (fn induct => fn Ps =>
              EVERY' [rtac ctxt rev_mp,
                let
                  val instantiate_args = case_mut (map SOME (Ps @ us)) (NONE :: map SOME (us @ Ps));
                  val dest_IH = dtac ctxt bspec THEN' assume_tac ctxt;
                  val solve = EVERY' [resolve_tac ctxt UnI_Gs, etac ctxt @{thm UN_I}] THEN'
                    TRY o EVERY' [resolve_tac ctxt UnI_pars,
                    etac ctxt @{thm UN_I[rotated]}, assume_tac ctxt];
                  val intro = resolve_tac ctxt (ballI :: []);
                  val simp = CHANGED o asm_full_simp_tac (ss_only (flat [raw_sets, flat_simps,
                    set_flat_shapes, G_set_maps, @{thms UN_simps}]) ctxt);
                in
                  rtac ctxt (infer_instantiate' ctxt instantiate_args induct) THEN_ALL_NEW
                  REPEAT_DETERM o FIRST' [intro, solve, dest_IH, simp]
                end]) inducts Pss)) THEN
            case_mut (unfold_tac ctxt @{thms all_simps} THEN
            HEADGOAL (EVERY' [REPEAT_DETERM o rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN nr_Ts i),
                EVERY' (replicate nr_par (dtac ctxt (mk_conjunctN nr_Ts i))),
                solve,
                rtac ctxt subsetI,
                REPEAT_DETERM o etac ctxt UnE,
                EVERY' (replicate nr_par (EVERY' [
                  etac ctxt @{thm UN_E},
                  REPEAT_DETERM o (dtac ctxt bspec THEN' assume_tac ctxt),
                  assume_tac ctxt]))
              ]) (1 upto nr_Ts)])) (HEADGOAL (REPEAT_DETERM_N nr_par o EVERY' [
                rtac ctxt impI,
                REPEAT_DETERM o (dtac ctxt bspec THEN' assume_tac ctxt),
                assume_tac ctxt]))
          end
      in
        mk_set_flat_raw_cotac @{context}
          @{cterm "\<forall>t :: 'a raw.
            (\<forall>y \<in> set_raw t. \<forall>t'. flat t' = t \<longrightarrow> y \<in> UNION (set_raw t') set_F)"}
          [[@{cterm "\<lambda>f :: 'a F. \<lambda>t :: ('a F) raw. \<forall>a \<in> set_F f. a \<in> set_raw (flat t)"}]]
          [@{cterm t}]
          @{thms raw.set_induct}
          1
          @{thms raw.exhaust}
          @{thms raw.inject}
          @{thms raw.set}
          @{thms flat_simps}
          @{thms flat_shape_set}
          @{thms G.set_map}
      end\<close>)

(* 
lemma set_raw_flat:
  "set_raw (flat t) = UNION (set_raw t) set_F"
  apply (rule equalityI)
   apply (rule subsetI)
   apply (rule UN_I[of _ _])
    apply (erule raw.set_induct)

  find_theorems "?x \<in> UNION _ _"
  
thm raw.set_induct
thm raw.set_induct[of _ _ "\<lambda>y t. \<exists>t'. flat t' = t \<and> y \<in> UNION (set_raw t') set_F"]
  
  thm UN_I
  thm UN_Un
  thm UN_Un_distrib
  thm Un_ac
  thm mem_Collect_eq
  thm rev_mp[of "\<forall>t' :: 'a raw.
            (\<forall>y \<in> set_raw t'. \<forall>t. flat t = t' \<longrightarrow> y \<in> UNION (set_raw t) set_F)"]
  apply (rule rev_mp[of "\<forall>t' :: 'a raw. (\<forall>y \<in> set_raw t'. \<forall>t. flat t = t' \<longrightarrow> y \<in> UNION (set_raw t) set_F)"])
   apply (rule allI ballI impI)+
  thm raw.set_induct
  thm raw.set_induct[of _ _ "\<lambda>y t. \<exists>t'. flat t' = t \<and> y \<in> UNION (set_raw t') set_F"]
   apply (erule raw.set_induct)
  (*subgoal for t' _ t
    apply (rule raw.exhaust[of t'])
    apply (rule raw.exhaust[of t])
    apply (auto simp only:
        flat_simps raw.set shape.set G.set_map flat_shape_set
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong)
    done
  subgoal for _ _ t
    apply (rule raw.exhaust[of t])
    apply (auto simp only:
        flat_simps raw.set G.set_map flat_shape_set
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong) []
  done*)
  sorry


lemma set_T: "set_T (T g) = set1_G g \<union>
  (\<Union>(set_F ` (\<Union>(set_T ` (set2_G g)))))"
  unfolding set_T_def T_def o_apply
  apply -
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.set shape.set G.set_map set_raw_flat o_apply
        UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong) []
  done
*)

subsection \<open>rel\<close>

lemma flat_shape_rel_raw:
  "(\<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
     rel_shape (rel_F R) u u')"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
    rel_shape (rel_F R) u u'"
    u])
   apply (auto 0 4 simp only:
     invar_shape.simps flat_shape.simps shape.rel_inject
     invar_shape_depth_iff ball_simps id_apply
     F.rel_map pred_F_def F.set_map
     elim!: F.rel_mono_strong
     split: list.splits label.splits)
  done

lemma flat_shape_rel:
  "invar_shape u0 u \<Longrightarrow> invar_shape u0 u' \<Longrightarrow>
    rel_shape R (flat_shape u) (flat_shape u') = rel_shape (rel_F R) u u'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF flat_shape_rel_raw]]]]]; assumption)
  done

lemma rel_raw_flat:
  "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
   rel_raw R (flat t) (flat t') = rel_raw (rel_F R) t t'"
  apply (rule iffI[rotated, OF rel_funD[OF flat.transfer]], assumption)
  apply (coinduction arbitrary: u0 t t' rule: raw.rel_coinduct)
  apply (rename_tac t t')
  apply (case_tac t)
  apply (case_tac t')
  apply (auto simp only: raw.sel
      invar_simps flat_simps raw.rel_inject G.rel_map G.pred_set flat_shape_rel G.set_map ball_simps id_apply
    elim!: G.rel_mono_strong |
    ((rule exI)+, rule conjI[OF refl]))+
  done
thm raw.rel_coinduct

lemma rel_raw_flat':
  "invar u0 t \<longrightarrow> invar u0 t' \<longrightarrow>
   rel_raw R (flat t) (flat t') \<longrightarrow> rel_raw (rel_F R) t t'"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
      fun mk_rel_flat_raw_raw_cotac ctxt Ps Rs xs ys induct raw_exhausts raw_sels raw_rel_injects
        flat_simps invar_raw_simps rel_flat_shapes
        G_rel_maps G_pred_sets G_rel_refl G_rel_congs G_rel_mono_strongs =
        let
          val n = length Ps;
          fun case_mut a b = if n <> 1 then a else b;
          val infer_args = case_mut ((hd Ps :: Rs @ tl Ps) @ ([xs, ys] |> transpose |> flat))
            (hd Ps :: ([xs, ys] |> transpose |> flat) @ Rs);
        in
          HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (REPEAT_DETERM o (rtac ctxt impI)),
            rtac ctxt (infer_instantiate' ctxt (map SOME infer_args) induct),
            case_mut (K all_tac) (REPEAT_DETERM o FIRST' [resolve_tac ctxt [exI, conjI], assume_tac ctxt])]) THEN
          (HEADGOAL o RANGE) (map (fn exhaust => EVERY' [
            REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
            Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
              let
                val exhaust_tacs = map (fn param =>
                  rtac ctxt (infer_instantiate' ctxt ((single o SOME o snd) param) exhaust)) (take 2 params);
                val intro = resolve_tac ctxt (exI :: G_rel_refl);
                val elim = eresolve_tac ctxt (conjE :: G_rel_mono_strongs);
                val dest_bspec = dtac ctxt bspec THEN' assume_tac ctxt;
                val rel_flat_shapesI = map (fn thm => thm RS iffD1) rel_flat_shapes;
                val apply_rel_flat = resolve_tac ctxt rel_flat_shapesI THEN_ALL_NEW assume_tac ctxt;
                val simp = asm_full_simp_tac (fold Simplifier.add_cong G_rel_congs
                  (ss_only (flat [raw_sels, raw_rel_injects, flat_simps,
                    invar_raw_simps, G_rel_maps, G_pred_sets]) ctxt));
              in
                HEADGOAL (EVERY' exhaust_tacs THEN' REPEAT_DETERM o
                  FIRST' [hyp_subst_tac ctxt, intro, elim, dest_bspec, apply_rel_flat, simp])
              end) ctxt]) raw_exhausts) THEN
          HEADGOAL (case_mut (
            rtac ctxt impI THEN'
            CONJ_WRAP' (fn i => EVERY' [
              dtac ctxt (mk_conjunctN n i),
              REPEAT_DETERM o resolve_tac ctxt [allI, impI],
              etac ctxt mp,
              REPEAT_DETERM o FIRST' [
                resolve_tac ctxt [exI, conjI],
                assume_tac ctxt]]) (1 upto n)) (K all_tac))
        end
      in
        mk_rel_flat_raw_raw_cotac @{context}
          [@{cterm "\<lambda>t t'. \<exists>u0. invar u0 t \<and> invar u0 t' \<and> rel_raw R (flat t) (flat t')"}]
          [@{cterm "rel_F R"}]
          [@{cterm "t"}]
          [@{cterm "t'"}]
          @{thm raw.rel_coinduct}
          @{thms raw.exhaust}
          @{thms raw.sel}
          @{thms raw.rel_inject}
          @{thms flat_simps}
          @{thms invar_simps}
          @{thms flat_shape_rel}
          @{thms G.rel_map}
          @{thms G.pred_set}
          @{thms G.rel_refl}
          @{thms G.rel_cong}
          @{thms G.rel_mono_strong}
      end\<close>)
(*
  apply (rule impI)+
  apply (rule raw.rel_coinduct[of "\<lambda>t t'. \<exists>u0. invar u0 t \<and> invar u0 t' \<and> rel_raw R (flat t) (flat t')" t t' "rel_F R"])
  apply ((rule conjI exI | assumption))+
  subgoal for raw raw'
    apply (rule raw.exhaust[of raw])
    apply (rule raw.exhaust[of raw'])
    apply (auto simp only: raw.sel G.rel_map
      raw.rel_inject flat_shape_rel flat_simps invar_simps G.pred_set
      o_apply simp_thms
      intro: G.rel_refl
      cong: G.rel_cong
      elim!: G.rel_mono_strong)
    done
  done*)

lemma rel_T: "rel_T R (T g) (T g') = rel_G R (rel_T (rel_F R)) g g'"
  unfolding rel_T_def T_def vimage2p_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) [2]
  apply (simp only:
    raw.rel_inject G.rel_map shape.rel_inject o_apply
    rel_raw_flat[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]])
  done


section \<open>Size\<close>



(* The "recursion sz_ofs" of elements of 'a raw -- on the second argument of G *)
codatatype sz = SCons "(unit, sz) G"

primcorec sz_of :: "'a raw \<Rightarrow> sz" where
  "sz_of t = SCons (map_G (\<lambda>_. ()) sz_of (un_Cons t))"

lemmas sz_of_simps = sz_of.ctr[of "Cons _", unfolded raw.case]

lemma sz_of_unflat: 
  "sz_of (unflat u0 t) = sz_of (t :: 'a raw)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun sz_of_unflat_raw_cotac ctxt Ps xs ys induct raw_exhausts sz_sels
          unflat_simps sz_of_simps G_rel_maps G_rel_refls =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (K all_tac),
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]))]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (exI :: G_rel_refls);
                  val simp = asm_full_simp_tac (ss_only (flat [sz_sels, unflat_simps, sz_of_simps, G_rel_maps]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (case_mut (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), etac ctxt mp,
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl])
              ]) (1 upto n)]) (K all_tac))
          end
      in
        sz_of_unflat_raw_cotac @{context}
          [@{cterm "\<lambda>l r. \<exists>u0 t. l = sz_of (unflat u0 t) \<and> r = sz_of (t :: 'a raw)"}]
          [@{cterm "sz_of (unflat u0 t)"}]
          [@{cterm "sz_of t"}]
          @{thm sz.coinduct}
          @{thms raw.exhaust}
          @{thms sz.sel}
          @{thms unflat_simps}
          @{thms sz_of_simps}
          @{thms G.rel_map}
          @{thms G.rel_refl}
      end\<close>)
(*
  apply (coinduction arbitrary: ul r rule: sz.coinduct)
  apply (rename_tac u0 t)
  apply (case_tac t)
  apply (auto simp only: unflat_simps sz_of_simps sz.sel G.rel_map
    intro: G.rel_refl)
  done*)

lemma sz_of_map_raw: 
  "sz_of (map_raw f t) = sz_of t"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun sz_of_map_raw_cotac ctxt Ps xs ys induct raw_exhausts sz_sels
          raw_maps sz_of_simps G_rel_maps G_rel_refls =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (K all_tac),
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]))]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 2)] exhaust;
                  val intro = resolve_tac ctxt (exI :: G_rel_refls);
                  val simp = asm_full_simp_tac (ss_only (flat [sz_sels, raw_maps, sz_of_simps, G_rel_maps]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (case_mut (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), etac ctxt mp,
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl])
              ]) (1 upto n)]) (K all_tac))
          end
      in
        sz_of_map_raw_cotac @{context}
          [@{cterm "\<lambda>l r. \<exists>t. l = sz_of (map_raw f t) \<and> r = sz_of t"}]
          [@{cterm "sz_of (map_raw f t)"}]
          [@{cterm "sz_of t"}]
          @{thm sz.coinduct}
          @{thms raw.exhaust}
          @{thms sz.sel}
          @{thms raw.map}
          @{thms sz_of_simps}
          @{thms G.rel_map}
          @{thms G.rel_refl}
      end\<close>)
(*
  apply (coinduction arbitrary: r rule: sz.coinduct)
  apply (rename_tac t)
  apply (case_tac t)
  apply (auto simp only: raw.map sz_of_simps sz.sel G.rel_map
    intro: G.rel_refl)
  done




ML \<open>
local
open Ctr_Sugar_Util
open BNF_FP_Rec_Sugar_Util
in

  fun mk_raw_induct_sz_tac ctxt IHs reduce_ctrm sz_induct induct_ctrms sz_cases =
    let
      val n = length (IHs |> map (tap (tracing o Thm.string_of_thm ctxt)));
    in
      HEADGOAL (EVERY' [rtac ctxt (infer_instantiate' ctxt [SOME reduce_ctrm] rev_mp),
        REPEAT_DETERM_N n o rtac ctxt allI,
        rtac ctxt (infer_instantiate' ctxt (map SOME induct_ctrms) sz_induct) THEN_ALL_NEW
        Subgoal.FOCUS (fn {context = ctxt, prems = raw_IHs', ...} =>
        let
          val IHs' = map (fn thm => thm RS spec RS mp) raw_IHs';
          val apply_IHs = resolve_tac ctxt IHs THEN'
            EVERY' (map (fn IH' =>
              rtac ctxt IH' THEN_ALL_NEW asm_full_simp_tac (ss_only sz_cases ctxt)) IHs');
          val intro = resolve_tac ctxt [allI, impI];
        in
          HEADGOAL (REPEAT_DETERM o FIRST' [intro, apply_IHs])
        end) ctxt,
        EVERY' [rtac ctxt impI, REPEAT_DETERM_N n o etac ctxt allE,
          CONJ_WRAP' (fn i => EVERY' [dtac ctxt (mk_conjunctN n i), dtac ctxt spec, etac ctxt mp,
            rtac ctxt refl]) (1 upto n)]])
    end;
end
\<close>

lemma raw_induct_sz_of: 
  assumes "\<And> r :: 'a raw. (\<And>r'. sz_of r' \<in> set2_G (case sz_of r of SCons x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
  shows "PP (r ::'a raw)"
  by (tactic \<open>mk_raw_induct_sz_tac @{context}
    @{thms assms}
    @{cterm "\<forall>sz r. sz_of r = sz \<longrightarrow> PP r "}
    @{thm sz.induct}
    [@{cterm "\<lambda>sz. \<forall>r. sz_of r = sz \<longrightarrow> PP r"}]
    @{thms sz.case}
  \<close>)*)















section \<open>Corecursion\<close>
(*
(*normal datatype recursor
  (('b, 'a) G \<Rightarrow> 'a) \<Rightarrow>
  'b T \<Rightarrow> 'a
*)

(*generalized recursor
  (\<forall>'a. ('a D, ('b F, 'c F) R) G \<Rightarrow> ('b, 'c) R) \<Rightarrow>
  (\<forall>'a. 'a D F \<Rightarrow> 'a F D) \<Rightarrow>
  'a D T \<Rightarrow> ('b, 'c) R 
*)


bnf_axiomatization ('b, 'c) R
bnf_axiomatization ('a, 'd, 'e) D

consts defobj :: "(('a, 'd, 'e) D, ('b F, 'c F) R) G \<Rightarrow> ('b, 'c) R"
consts argobj :: "('a, 'd, 'e) D F \<Rightarrow> ('a F, 'd F, 'e F) D"

axiomatization where
  defobj_transfer: "\<And>A B C D E.
    bi_unique A \<Longrightarrow> bi_unique B \<Longrightarrow> bi_unique C \<Longrightarrow> bi_unique D \<Longrightarrow> bi_unique E \<Longrightarrow>
    rel_fun (rel_G (rel_D A D E) (rel_R (rel_F B) (rel_F C))) (rel_R B C) defobj defobj" and
  argobj_transfer: "\<And>A D E.
    bi_unique A \<Longrightarrow> bi_unique D \<Longrightarrow> bi_unique E \<Longrightarrow>
    rel_fun (rel_F (rel_D A D E)) (rel_D (rel_F A) (rel_F D) (rel_F E)) argobj argobj"

lemma defobj_invar: "pred_fun (pred_G (pred_D A D E) (pred_R (pred_F B) (pred_F C))) (pred_R B C) defobj"
  unfolding fun_pred_rel G.rel_eq_onp[symmetric] F.rel_eq_onp[symmetric]
    D.rel_eq_onp[symmetric] R.rel_eq_onp[symmetric]
  by (rule defobj_transfer; rule bi_unique_eq_onp)

lemma argobj_invar: "pred_fun (pred_F (pred_D A D E)) (pred_D (pred_F A) (pred_F D) (pred_F E)) argobj"
  unfolding fun_pred_rel F.rel_eq_onp[symmetric] D.rel_eq_onp[symmetric]
  by (rule argobj_transfer; rule bi_unique_eq_onp)

lemma defobj_natural:
  "inj_on a (\<Union>(set1_D ` set1_G x)) \<Longrightarrow>
   inj_on b (\<Union>(set2_D ` set1_G x)) \<Longrightarrow>
   inj_on c (\<Union>(set3_D ` set1_G x)) \<Longrightarrow>
   inj_on d (\<Union>(set_F ` \<Union>(set1_R ` set2_G x))) \<Longrightarrow>
   inj_on e (\<Union>(set_F ` \<Union>(set2_R ` set2_G x))) \<Longrightarrow>
  defobj (map_G (map_D a b c) (map_R (map_F d) (map_F e)) x) = map_R d e (defobj x)"
  using rel_funD[OF defobj_transfer, of "BNF_Def.Grp (\<Union>(set1_D ` set1_G x)) a" "BNF_Def.Grp (\<Union>(set_F ` \<Union>(set1_R ` set2_G x))) d" "BNF_Def.Grp (\<Union>(set_F ` \<Union>(set2_R ` set2_G x))) e" "BNF_Def.Grp (\<Union>(set2_D ` set1_G x)) b" "BNF_Def.Grp (\<Union>(set3_D ` set1_G x)) c"]
  unfolding F.rel_Grp R.rel_Grp D.rel_Grp G.rel_Grp bi_unique_Grp
  by (auto simp add: Grp_def)

lemma argobj_natural:
  "inj_on a (\<Union>(set1_D ` set_F x)) \<Longrightarrow>
   inj_on d (\<Union>(set2_D ` set_F x)) \<Longrightarrow>
   inj_on e (\<Union>(set3_D ` set_F x)) \<Longrightarrow>
  argobj (map_F (map_D a d e) x) = map_D (map_F a) (map_F d) (map_F e) (argobj x)"
  using rel_funD[OF argobj_transfer, of "BNF_Def.Grp (\<Union>(set1_D ` set_F x)) a" "BNF_Def.Grp (\<Union>(set2_D ` set_F x)) d" "BNF_Def.Grp (\<Union>(set3_D ` set_F x)) e"]
  unfolding F.rel_Grp R.rel_Grp D.rel_Grp bi_unique_Grp
  by (auto simp add: Grp_def)

lemma argobj_flat_shape_natural: fixes x :: "('a F shape, 'b F shape, 'c F shape) D F"
  shows
    "pred_F (pred_D (invar_shape u) (invar_shape u) (invar_shape u)) x \<Longrightarrow>
    argobj (map_F (map_D flat_shape flat_shape flat_shape) x) =
    map_D (map_F flat_shape) (map_F flat_shape) (map_F flat_shape) (argobj x)"
  apply (rule argobj_natural; rule inj_onI;
    rule box_equals[OF _ unflat_shape_flat_shape[of u] unflat_shape_flat_shape[of u]])
  apply (auto simp only: F.pred_set D.pred_set)
  done

lemma defobj_flat_shape_natural: fixes x :: "(('a F shape, 'b F shape, 'c F shape) D, ('d F shape F, 'e F shape F) R) G"
  shows
    "pred_G (pred_D (invar_shape u) (invar_shape u) (invar_shape u)) (pred_R (pred_F (invar_shape u)) (pred_F (invar_shape u))) x \<Longrightarrow>
    defobj (map_G (map_D flat_shape flat_shape flat_shape) (map_R (map_F flat_shape) (map_F flat_shape)) x) = map_R flat_shape flat_shape (defobj x)"
  apply (rule defobj_natural; rule inj_onI;
    rule box_equals[OF _ unflat_shape_flat_shape[of u] unflat_shape_flat_shape[of u]])
  apply (auto simp only: F.pred_set G.pred_set D.pred_set R.pred_set)
  done

primrec f_shape :: "('a, 'd, 'e) D shape \<Rightarrow> ('a shape, 'd shape, 'e shape) D" where
  "f_shape (Leaf a) = map_D Leaf Leaf Leaf a"
| "f_shape (Node f) = map_D Node Node Node (argobj (map_F f_shape f))"

primcorec f_raw :: "('a, 'd, 'e) D raw \<Rightarrow> ('b shape, 'c shape) R" where
  "f_raw (Cons g) = defobj (map_G f_shape (map_R un_Node un_Node o f_raw) g)"

definition f :: "('a, 'd, 'e) D T \<Rightarrow> ('b, 'c) R" where
  "f t = map_R un_Leaf un_Leaf (f_raw (Rep_T t))"

lemma invar_shape_f_shape[THEN spec, THEN mp]:
  "\<forall>u0. invar_shape u0 u \<longrightarrow> pred_D (invar_shape u0) (invar_shape u0) (invar_shape u0) (f_shape u)"
  apply (rule shape.induct[of "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> pred_D (invar_shape u0) (invar_shape u0) (invar_shape u0) (f_shape u)" u])
  apply (auto simp only: invar_shape.simps f_shape.simps D.pred_map D.pred_True o_apply
    simp_thms all_simps list.inject list.distinct F.pred_map
    intro!: pred_funD[OF argobj_invar]
    elim!: F.pred_mono_strong
    cong: D.pred_cong
    split: list.splits label.splits)
  done

lemma f_shape_flat_shape[THEN spec, THEN mp]: "\<forall>u. invar_shape u x \<longrightarrow>
  f_shape (flat_shape x) = map_D flat_shape flat_shape flat_shape (f_shape (map_shape argobj x))"
  apply (rule shape.induct[of "\<lambda>x. \<forall>u. invar_shape u x \<longrightarrow> f_shape (flat_shape x) = map_D flat_shape flat_shape flat_shape (f_shape (map_shape argobj x))" x])
   apply (simp only: flat_shape.simps f_shape.simps shape.map D.map_comp F.map_comp argobj_natural inj_on_def shape.inject
     o_apply simp_thms Ball_def cong: F.map_cong D.map_cong)
  apply (rule allI)
  subgoal premises IH for f u0
    apply (rule impI)
    apply (unfold flat_shape.simps f_shape.simps shape.map D.map_comp o_def)
    apply (auto simp only: D.map_comp[unfolded o_def, symmetric]
        F.set_map F.map_comp F.pred_set invar_shape.simps list.sel o_apply
        invar_shape_map_closed invar_shape_f_shape
      intro!: arg_cong[of _ _ argobj] D.map_cong F.map_cong
         IH[THEN spec, THEN mp, of _ "tl u0"]
        trans[OF _ argobj_flat_shape_natural[of "tl u0"]]
      elim!: F.pred_mono_strong
      split: list.splits label.splits)
    done
  done

lemma invar_f_raw[THEN spec, THEN mp]:
  "\<forall>u0. invar u0 x \<longrightarrow> pred_R (invar_shape u0) (invar_shape u0) ((f_raw :: ('a F, 'b F, 'c F) D raw \<Rightarrow> (('d F) shape, ('e F) shape) R) (map_raw argobj x))"
  apply (rule raw.induct[of "\<lambda>x. \<forall>u0. invar u0 x \<longrightarrow> pred_R (invar_shape u0) (invar_shape u0) (f_raw (map_raw argobj x))" x])
  apply (rule allI)
  subgoal premises IH for g u0
    apply (auto simp only: invar_simps raw.map f_raw.simps G.map_comp o_apply G.pred_map R.pred_map
    invar_shape_map_closed invar_shape_depth_iff shape.case
    intro!: pred_funD[OF defobj_invar[of "invar_shape u0" "invar_shape u0" "invar_shape u0"]] invar_shape_f_shape
      R.pred_mono_strong[OF mp[OF spec[OF IH, of _ "F # u0"]]]
    elim!: G.pred_mono_strong
    cong: G.map_cong)
    done
  done

lemma f_raw_flat[THEN spec, THEN mp]:
  "\<forall>u0. invar u0 x \<longrightarrow> f_raw (flat x) = map_R flat_shape flat_shape (f_raw (map_raw argobj x))"
  apply (rule raw.induct[of "\<lambda>x. \<forall>u0. invar u0 x \<longrightarrow> f_raw (flat x) = map_R flat_shape flat_shape (f_raw (map_raw argobj x))" x])
  apply (rule allI)
  subgoal premises IH for g u0
    apply (auto simp only: flat.simps f_raw.simps G.map_comp o_apply
      f_shape_flat_shape raw.map
      invar_simps G.pred_set G.set_map mp[OF spec[OF IH, of _ "F # u0"]]
      R.map_comp invar_shape_depth_iff shape.case flat_shape.simps invar_shape_map_closed R.pred_map
      intro!: arg_cong[of _ _ defobj] G.map_cong0 R.map_cong_pred invar_shape_f_shape
        R.pred_mono_strong[OF invar_f_raw[of "F # u0"]]
        trans[OF _ defobj_flat_shape_natural[of u0]] f_shape_flat_shape[of u0])
    done
  done

lemma "f (T g) = defobj (map_G id (f o map_T argobj) g)"
  apply (simp only: f_def[abs_def] map_T_def T_def o_apply cong: G.map_cong)
  apply (subst (1 2) Abs_T_inverse)
    apply (simp only: mem_Collect_eq invar_map_closed_raw Rep_T[unfolded mem_Collect_eq])
   apply (simp only: mem_Collect_eq invar_simps G.pred_map G.pred_True o_apply
     invar_shape.simps list.case invar_flat Rep_T[unfolded mem_Collect_eq] snoc.simps[symmetric]
     cong: G.pred_cong)
  apply (auto simp only:
    G.map_comp F.map_comp D.map_comp R.map_comp
    F.map_ident D.map_ident
    G.set_map F.set_map D.set_map R.set_map
    f_raw.simps f_shape.simps shape.case flat_shape.simps invar_shape_depth_iff
    f_raw_flat[OF Rep_T[unfolded mem_Collect_eq]]
    inj_on_def o_apply id_apply
    intro!: arg_cong[of _ _ defobj] G.map_cong R.map_cong
      trans[OF defobj_natural[of un_Leaf _ un_Leaf un_Leaf, symmetric]]
    dest!: bspec[OF conjunct1[OF invar_f_raw[OF Rep_T[unfolded mem_Collect_eq], unfolded R.pred_set]]]
           bspec[OF conjunct2[OF invar_f_raw[OF Rep_T[unfolded mem_Collect_eq], unfolded R.pred_set]]]
    cong: F.map_cong D.map_cong)
  done

hide_const (open) f
*)
section \<open>Coinduction\<close>

(* Preliminaries *)


definition pred_onp where
  "pred_onp R P Q x y = (R x y \<and> P x \<and> Q y)"

lemma eq_pred_onp: "(=) = pred_onp ((=)) (\<lambda>_. True) (\<lambda>_. True)"
  unfolding fun_eq_iff pred_onp_def by simp

lemma G_rel_pred_onp: "rel_G (pred_onp R1 P1 Q1) (pred_onp R2 P2 Q2) =
  pred_onp (rel_G R1 R2) (pred_G P1 P2) (pred_G Q1 Q2)"
  unfolding G.in_rel fun_eq_iff pred_onp_def[abs_def] G.pred_set
  by (fastforce simp: G.set_map)


(* Andrei preliminaries: *)

(* Preliminaries *)

(* lemma raw_induct_un_Cons: 
assumes "\<And> r :: 'a raw. (\<And>r'. r' \<in> set2_G (un_Cons r) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
shows "PP (r ::'a raw)"
apply(induct r) using assms
using raw.case by fastforce
*)

lemmas invar_map_raw[simp] = invar_map_closed 
 
lemma map_T_Abs_T: "invar [] r \<Longrightarrow> map_T f (Abs_T r) = Abs_T (map_raw f r)" 
unfolding map_T_def by (simp add: Abs_T_inverse)

(* The "co-recursion sizes" of elements of 'a raw -- on the second argument of G *)


hide_const size
abbreviation unit where "unit \<equiv> \<lambda> _. ()"
abbreviation "size \<equiv> sz_of"  

(* lemma size: "size t = Cons (map_G (map_shape unit) size (un_Cons t))"
by (metis raw.exhaust_sel raw.map_sel unCons_def)
     *)
lemmas size_unflat = sz_of_unflat

lemmas size_map_raw[simp] = sz_of_map_raw 

(* Coinduction with invariant: *)  
lemma raw_coinduct_inv: 
assumes R: "R r1 r2" "invr r1" "invr r2"
and invr: "\<And> r. invr r \<Longrightarrow> pred_G (\<lambda> _. True) invr (unCons r)"  
and coind: "\<And>r1 r2. invr r1 \<Longrightarrow> invr r2 \<Longrightarrow> R r1 r2 \<Longrightarrow> rel_G op= R (unCons r1) (unCons r2)"
shows "r1 = r2"
proof-
  define RR where "RR \<equiv> \<lambda> r1 r2. R r1 r2 \<and> invr r1 \<and> invr r2"
  {fix r1 r2 
   assume "RR r1 r2"
   hence "pred_G (\<lambda> _. True) invr (unCons r1)" "pred_G (\<lambda> _. True) invr (unCons r2)"
         "rel_G op= R (unCons r1) (unCons r2)" using invr coind unfolding RR_def by auto
   hence "rel_G op= RR (unCons r1) (unCons r2)" 
   unfolding RR_def rel_G_def pred_G_def BNF_Def.Grp_def OO_def apply auto
   by (smt Collect_split_mono_strong G.set_map(2))   
  }
  hence "RR \<le> op=" by (simp add: predicate2I raw.coinduct)
  thus ?thesis using R unfolding RR_def by auto
qed
  
(*
(* Size-coinduction with invariant: *)
lemma raw_coinduct_inv_size: 
fixes R :: "'a raw \<Rightarrow> 'a raw \<Rightarrow> bool"
assumes R: "R r1 r2" 
shows "r1 = r2" 
proof- 
  define Rs :: "'a raw \<Rightarrow> 'a raw \<Rightarrow> bool" where 
  "Rs \<equiv> \<lambda> r1 r2. \<exists> r1' r2'. size r1' = size r1 \<and> size r2' = size r2 \<and> R r1' r2'"
  have "Rs r1 r2" using R unfolding Rs_def by auto
  thus "r1 = r2"
  proof(coinduct rule: raw.coinduct)
    case (Eq_raw r1 r2)
    then obtain r1' r2' where s: "size r1' = size r1" "size r2' = size r2" 
    and R: "R r1' r2'" unfolding Rs_def by auto 
    let ?Trp = "Trp::'a shape \<Rightarrow> 'a shape \<Rightarrow> bool" 
    obtain g1 g2 :: "('a shape, 'a raw)G" where   
    "map_G unit size (unCons r1') = map_G unit size g1"
    "map_G unit size (unCons r2') = map_G unit size g2" 
    and Rg: "rel_G op= R g1 g2" using coind[OF R] by auto
    hence m1: "map_G unit size (unCons r1) = map_G unit size g1" and 
          m2: "map_G unit size (unCons r2) = map_G unit size g2"
    using s by (metis sz_of.simps unCons_def)+
    have "rel_G ((Grp unit) OO (Grp unit)\<inverse>\<inverse>) 
                 (Grp size OO (Grp size)\<inverse>\<inverse>) (unCons r1) g1"
    using m1 unfolding G.rel_compp G.rel_Grp G.rel_conversep 
    unfolding OO_def Grp_def by auto 
    hence "rel_G Trp (Grp size OO (Grp size)\<inverse>\<inverse>) (unCons r1) g1"
    using G.rel_mono_strong by fastforce
    hence 12: "rel_G (?Trp OO op=) ((Grp size OO (Grp size)\<inverse>\<inverse>) OO R) (unCons r1) g2"
    using Rg unfolding G.rel_compp unfolding OO_def by auto
    have "rel_G ((Grp unit) OO (Grp unit)\<inverse>\<inverse>) 
                 (Grp size OO (Grp size)\<inverse>\<inverse>) g2 (unCons r2)"
    using m2 unfolding G.rel_compp G.rel_Grp G.rel_conversep 
    unfolding OO_def Grp_def by auto 
    hence 22: "rel_G ?Trp (Grp size OO (Grp size)\<inverse>\<inverse>) g2 (unCons r2)"
    using G.rel_mono_strong by fastforce
    have "rel_G ((?Trp OO op=) OO ?Trp) 
                (((Grp size OO (Grp size)\<inverse>\<inverse>) OO R) OO (Grp size OO (Grp size)\<inverse>\<inverse>)) 
          (unCons r1) (unCons r2)"  
    using 12 22 unfolding G.rel_compp unfolding OO_def by auto
  
      
    
    
    unfolding OO_def find_theorems conversep
      unfolding OO_def apply 
      
      find_theorems "rel_G _ _\<inverse>\<inverse>"
      find_theorems rel_G "_ OO _"
      using G.rel_Grp sledgehammer
      
      unfolding BNF_Def.Grp_def OO_def apply auto
    hence m: "rel_G (\<lambda> sh sh'. True) (\<lambda> r r'. size r = size r') (unCons r1) g1"
      unfolding rel_G_def BNF_Def.Grp_def OO_def apply auto
    apply(rule exI[of _ "(unCons r1, g1)"])  
     find_theorems rel_G 
    thus ?case using Rg unfolding Rs_def 
    (* unfolding rel_G_def BNF_Def.Grp_def OO_def apply auto *)
  qed
qed
    
  
  
assumes "\<And> r :: 'a raw. (\<And>r'. size r' \<in> set2_G (un_SCons (size r)) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
shows "PP (r ::'a raw)"


lemma raw_induct_size: 
assumes "\<And> r :: 'a raw. (\<And>r'. size r' \<in> set2_G (un_SCons (size r)) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
shows "PP (r ::'a raw)"
proof-
  {fix sz and r :: "'a raw" assume "sz = size r"
   hence "PP r" apply(induct sz arbitrary: r)
   using assms by simp (metis sz.sel)
  }
  thus ?thesis by simp
qed

lemma invar_set2_G_un_Leaf: 
assumes r: "invar [] r" and r': "r' \<in> set2_G (map_G un_Leaf (unflat []) (un_Cons r))"
(is "r' \<in> ?A")
shows "invar [] r'"  
proof-
  have "?A = unflat [] ` set2_G (un_Cons r)" unfolding G.set_map(2) ..
  then obtain r1 where r1: "r1 \<in> set2_G (un_Cons r)" and r': "r' = unflat [] r1"
  using r' by auto
  from r1 r have "invar [F] r1" by (metis invar.elims(2) pred_G_def raw.case)
  thus ?thesis unfolding r' by (simp add: invar_unflat)
qed
*)

(*  THE ASSUMPTIONS: *)
consts Q :: "'a T \<Rightarrow> 'a T \<Rightarrow> bool"
axiomatization where 
Q_param: "\<And>(R :: 'a \<Rightarrow> 'a' \<Rightarrow> bool).
  bi_unique R \<Longrightarrow> rel_fun (rel_T R) (rel_fun (rel_T R) (=)) Q Q" and
Q_coind: "Q \<le> BNF_Def.vimage2p un_T un_T (rel_G (=) Q)"
  
(* 
lemma Q_param2: False
  using Q_param unfolding rel_fun_def 
*)

lemma less_vimage2p_Rep_iff:
  assumes "type_definition Rep Abs A" "type_definition Rep' Abs' B"
  shows "R \<le> BNF_Def.vimage2p Rep Rep' Y \<longleftrightarrow> pred_onp (BNF_Def.vimage2p Abs Abs' R) (\<lambda>x. x \<in> A) (\<lambda>x. x \<in> B) \<le> Y"
proof -
  interpret A: type_definition Rep Abs A by (rule assms)
  interpret B: type_definition Rep' Abs' B by (rule assms)
  show ?thesis
  unfolding vimage2p_def fun_eq_iff le_fun_def le_bool_def pred_onp_def
  by (force simp: A.Rep_inverse B.Rep_inverse A.Abs_inverse B.Abs_inverse A.Rep B.Rep)
qed

ML \<open>BNF_FP_Def_Sugar.fp_sugar_of @{context} @{type_name raw} |> the |> #fp_co_induct_sugar\<close>

lemma *: "type_definition un_T T UNIV"
  by unfold_locales (auto simp: T_un_T un_T_T)

lemma [simp]: "in_rel UNIV = top"
  unfolding in_rel_def[abs_def] by auto


(* Andrei *)

lemma Q_rel_G: 
  assumes "Q t1 t2"  
  shows "rel_G op= Q (un_T t1) (un_T t2)" 
  using assms Q_coind unfolding vimage2p_def by blast

find_theorems invar name: simp
find_theorems invar_shape name: simp
find_theorems unflat_shape name: simp
find_theorems unflat_shape
term unflat_shape
find_theorems unflat name: simp

find_theorems snoc

lemma snoc[simp]: "snoc x xs = xs @ [x]"
  by (induct xs) auto

abbreviation "forget_shape ul \<equiv> map_shape Node o unflat_shape ul"
abbreviation "forget ul \<equiv> map_raw Node o unflat ul"

(* cumulative forget: *)  
function cforget_shape :: "depth \<Rightarrow> 'a shape shape \<Rightarrow> 'a shape shape" where 
 "cforget_shape [] sh = sh"
|"cforget_shape (ul @ [u1]) sh = cforget_shape ul (forget_shape ul sh)"
using rev_exhaust by auto (meson rev_exhaust)
termination by (relation "measure (length o fst)") auto

function cforget :: "depth \<Rightarrow> 'a shape raw \<Rightarrow> 'a shape raw" where 
 "cforget [] r = r"
|"cforget (ul @ [u1]) r = cforget ul (forget ul r)"
using rev_exhaust by auto (meson rev_exhaust)
termination by (relation "measure (length o fst)") auto

lemma cforget_shape_Nil[simp]: "cforget_shape [] = id" by auto
lemma cforget_shape_snoc[simp]: 
"cforget_shape (ul @ [u1]) = cforget_shape ul o forget_shape ul"
by (rule ext) auto
lemma cforget_shape_Singl[simp]: "cforget_shape [u] = forget_shape []" 
using cforget_shape_snoc[of "[]" u, simplified] by blast


lemma cforget_Nil[simp]: "cforget [] = id" by auto
lemma cforget_snoc[simp]: 
"cforget (ul @ [u1]) = cforget ul o forget ul"
by (rule ext) auto
lemma cforget_Singl[simp]: "cforget [u] = forget []" 
using cforget_snoc[of "[]" u, simplified] by blast

lemma invar_forget_shape: 
assumes "invar_shape (ul @ [F]) sh"
shows "invar_shape ul (forget_shape ul sh)"
  using assms 
  by (simp add: invar_shape_map_closed_raw invar_shape_unflat_shape_raw)

lemma forget_shape_inj: 
assumes i: "invar_shape (ul @ [u]) sh1" "invar_shape (ul @ [u]) sh2"
and f: "forget_shape ul sh1 = forget_shape ul sh2"
shows "sh1 = sh2" 
proof-
  have "unflat_shape ul sh1 = unflat_shape ul sh2"
    using f using shape.inj_map_strong by fastforce
  thus ?thesis
    by (cases u) (metis flat_shape_unflat_shape_raw i snoc)
qed

theorem cforget_shape_inj:  (* crucial for handling equality! *)
assumes i: "invar_shape ul sh1" "invar_shape ul sh2"
and f: "cforget_shape ul sh1 = cforget_shape ul sh2"
shows "sh1 = sh2" 
using assms apply(induction ul arbitrary: sh1 sh2 rule: rev_induct) 
  by simp_all (metis (mono_tags, lifting) comp_def 
     forget_shape_inj invar_forget_shape label.exhaust)

lemma invar_forget: 
assumes "invar (ul @ [F]) r"
shows "invar ul (forget ul r)"
  using assms by (simp add: invar_unflat)

lemma invar_cforget_shape:
assumes "invar_shape ul sh"
shows "invar_shape [] (cforget_shape ul sh)" 
  using assms apply(induction ul arbitrary: sh rule: rev_induct)
  apply simp_all
  by (metis invar_shape_map_closed_raw invar_shape_unflat_shape_raw label.exhaust snoc) 

lemma invar_cforget:
assumes "invar ul r"
shows "invar [] (cforget ul r)" 
  using assms apply(induction ul arbitrary: r rule: rev_induct)
  by simp_all (metis invar_map_closed invar_unflat label.exhaust snoc)

lemma un_Cons_forget:
"un_Cons (forget ul r) = 
 map_G (forget_shape ul) (forget (F # ul)) (un_Cons r)"
apply simp unfolding unCons_def[symmetric] raw.map_sel unflat.sel G.map_comp ..

theorem un_Cons_cforget:
"map_G id (forget []) (un_Cons (cforget ul r)) = 
 map_G (cforget_shape ul) (cforget (F # ul)) (un_Cons r)"
  apply(induction ul arbitrary: r rule: rev_induct) apply auto
  unfolding append.append_Cons[symmetric] cforget_snoc 
  unfolding G.map_comp[symmetric]
  apply(rule G.map_cong) apply auto
  unfolding G.map_comp 
  using un_Cons_forget by auto
  
definition Qr where "Qr r1 r2 \<equiv> Q (Abs_T r1) (Abs_T r2)" 
 
definition Qrr where 
"Qrr r1 r2 \<equiv> \<exists> ul. invar ul r1 \<and> invar ul r2 \<and> Qr (cforget ul r1) (cforget ul r2)" 

abbreviation "Grp \<equiv> BNF_Def.Grp UNIV"
lemmas Grp_def = BNF_Def.Grp_def

lemma Q_map_T_inj: 
assumes "inj f"
shows "Q (map_T f t1) (map_T f t2) \<longleftrightarrow> Q t1 t2"
using assms Q_param[of "Grp f"] unfolding bi_unique_Grp 
unfolding rel_fun_def T.rel_Grp  unfolding Grp_def by auto

lemma Q_map_T_Leaf[simp]: 
"Q (map_T Leaf f1) (map_T Leaf f2) \<longleftrightarrow> Q f1 f2"
using Q_map_T_inj by (metis injI shape.inject)

lemma Q_map_T_Node[simp]: 
"Q (map_T Node f1) (map_T Node f2) \<longleftrightarrow> Q f1 f2"
using Q_map_T_inj by (metis injI shape.inject)

lemma rel_G_Q: 
"rel_G op= Q (map_G Leaf (map_T Node) g1) (map_G Leaf (map_T Node) g2) \<longleftrightarrow> 
 rel_G op= Q g1 g2"
unfolding G.rel_map by simp
  
theorem Qr_un_Cons:
assumes i: "invar ul r1" "invar ul r2" 
and Qr: "Qr (cforget ul r1) (cforget ul r2)" (is "Qr ?r1' ?r2'")
shows "rel_G op= Qr
           (map_G id (forget []) (un_Cons (cforget ul r1)))
           (map_G id (forget []) (un_Cons (cforget ul r2)))"
proof-
  have i': "invar [] ?r1'" "invar [] ?r2'" using i invar_cforget by auto 
  have Q: "Q (Abs_T ?r1') (Abs_T ?r2')" using Qr unfolding Qr_def by auto
  have "rel_G op= Q (un_T (Abs_T ?r1')) (un_T (Abs_T ?r2'))" using Q_rel_G[OF Q] .
  hence "rel_G op= Q 
           (map_G id Abs_T (map_G un_Leaf (unflat []) (un_Cons ?r1'))) 
           (map_G id Abs_T (map_G un_Leaf (unflat []) (un_Cons ?r2')))"
    (is "rel_G op= Q ?A1 ?A2")
  unfolding G.map_comp by (simp add: Abs_T_inverse i' un_T_def)
  hence "rel_G op= Q (map_G Leaf (map_T Node) ?A1) 
                     (map_G Leaf (map_T Node) ?A2)"
  unfolding rel_G_Q .
  hence "rel_G op= Q 
   (map_G (Leaf o un_Leaf) (map_T Node o Abs_T o unflat []) (un_Cons ?r1')) 
   (map_G (Leaf o un_Leaf) (map_T Node o Abs_T o unflat []) (un_Cons ?r2'))"
  (is ?C)
    unfolding G.map_comp o_assoc by simp    
  moreover have "?C \<longleftrightarrow> rel_G op= Q 
  (map_G id Abs_T (map_G id (forget []) (un_Cons ?r1'))) 
  (map_G id Abs_T (map_G id (forget []) (un_Cons ?r2')))"  (is "_ \<longleftrightarrow> ?D")
    unfolding G.map_comp unfolding id_o 
    unfolding G.rel_map apply simp
    apply(rule G.rel_cong) apply auto  
      apply (metis axiom12_G i' invar.cases invar_shape_depth_iff(1) 
              raw.sel shape.simps(5) unCons_def)
     apply (metis Q_map_T_Node append_Nil axiom12_G i'
         invar.cases invar_unflat map_T_Abs_T raw.sel snoc unCons_def)
    by (smt Q_map_T_Node axiom12_G i'(1) i'(2) invar.simps invar_unflat map_T_Abs_T raw.case snoc.simps(1))
  finally have ?D . (* TODO: clean-up mess *)
  thus ?thesis unfolding G.rel_map Qr_def by simp
qed

theorem Qrr: 
fixes r1 r2 :: "'a shape raw"  
assumes "Qrr r1 r2" 
shows "r1 = r2"    
using assms proof(coinduct rule: raw.coinduct)
  case (Eq_raw r1 r2)
  then obtain ul where i: "invar ul r1" "invar ul r2" and 
  Qr: "Qr (cforget ul r1) (cforget ul r2)" unfolding Qrr_def by auto 
  have 0: "rel_G op= Qr
           (map_G (cforget_shape ul) (cforget (F # ul)) (un_Cons r1))
           (map_G (cforget_shape ul) (cforget (F # ul)) (un_Cons r2))"
    using Qr_un_Cons[OF i Qr] unfolding un_Cons_cforget .
  
  show ?case unfolding unCons_def apply(rule G.rel_mono_strong0[OF 0[unfolded G.rel_map]])
  unfolding Qrr_def apply auto
  apply (metis axiom12_G cforget_shape_inj i invar.cases raw.sel unCons_def)  
  by (metis axiom12_G i invar.cases raw.sel unCons_def)
qed

theorem T_coind: "(Q::'a T \<Rightarrow> 'a T \<Rightarrow> bool) \<le> (=)"
proof clarify
  fix t1 t2 :: "'a T" assume Q: "Q t1 t2"
  define t1' and t2' where "t1' \<equiv> map_T Leaf t1" and "t2' \<equiv> map_T Leaf t2"  
    (* implicit use of parametricity of Q for Leaf: *)
  have Q: "Q t1' t2'" using Q unfolding t1'_def t2'_def by simp 
  hence "Qrr (Rep_T t1') (Rep_T t2')" unfolding Qrr_def Qr_def 
  using Rep_T by (intro exI[of _ "[]"]) (auto simp: Rep_T_inverse)
  hence "Rep_T t1' = Rep_T t2'" using  Qrr by auto
  hence "t1' = t2'" by (metis Rep_T_inverse)
  thus "t1 = t2" unfolding t1'_def t2'_def using T.inj_map_strong by fastforce
qed

end