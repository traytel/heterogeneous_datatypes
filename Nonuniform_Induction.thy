theory Nonuniform_Induction
imports Main
keywords "nonuniform_induction" :: prf_block % "proof"
begin

ML \<open>
fun nonuniform_induct_cmd state1 =
  let
    val _ = Proof.assert_backward state1;

    val ctxt = Proof.context_of state1;
    val (A, names_ctxt) = ctxt
      |> BNF_Util.mk_TFrees 1 |>> hd;
    val (x, names_ctxt) = names_ctxt
      |> yield_singleton (BNF_Util.mk_Frees "x") A;

    val ((P, (_, P_def)), (ctxt, old_ctxt)) = ctxt
      |> Local_Theory.open_target |> snd
      |> Local_Theory.define ((@{binding PP}, NoSyn), ((Binding.empty, []),
        absdummy A @{term True}))
      ||> `Local_Theory.close_target;

    val phi = Proof_Context.export_morphism old_ctxt ctxt;
    val P = Morphism.term phi P |> @{print};
    val P_def = Morphism.thm phi P_def |> @{print}

    val goal = Proof.simple_goal state1 |> #goal |> Thm.concl_of |> Logic.unprotect;

    fun after_qed (ctxt', thmss) state =
      let
        val step = flat thmss |> Proof_Context.export ctxt' ctxt |> hd |> @{print} (*truly polymorphic*);
        val thm = Goal.prove_sorry ctxt [] [] goal (fn {context = ctxt, ...} => Skip_Proof.cheat_tac ctxt 1);
      in
        state
        |> Proof.refine_primitive (fn ctxt => resolve_tac ctxt [thm] 1 #> Seq.hd)
        |> Proof.reset_facts
        |> Proof.enter_backward
      end;
  in
    state1
    |> Proof.map_context (K ctxt)
    |> Proof.enter_forward
    |> Proof.internal_goal (fn _ => fn _ => ()) Proof_Context.mode_default true "nonuniform_induction"
        NONE after_qed [] [] [(Binding.empty_atts, [(Logic.all x (BNF_Util.mk_Trueprop_eq (x, x)), [])])] |> #2
  end;

Outer_Syntax.command @{command_keyword nonuniform_induction} "proof by nonuniform induction"
  (Scan.succeed (Toplevel.proof nonuniform_induct_cmd));
\<close>

consts P :: "'a \<Rightarrow> bool"
lemma "P i"  
nonuniform_induction proof - --\<open>Induction step for a fixed type variable (here refl)\<close>
  fix x :: 'b
  show "x = x" .. 
qed --\<open>Proves the original goal "P i" using the polymorphic step (above using cheat_tac)\<close>
qed

end