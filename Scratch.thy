theory Scratch
imports BNF_Nonuniform_Fixpoint "HOL-Library.Multiset" "HOL-Library.FSet"
begin

nonuniform_datatype 'a plist = PNil | PCons 'a "('a \<times> 'a) plist"

term Rep_plist_pre_plist
thm wit_raw_plist_def
term Scratch.arg.plist_0_0.Rep_arg_plist_0_0_pre_plist_0_0

nonuniform_datatype 'a biplist = Nil | Cons\<^sub>1 'a "('a list) biplist"
                                     | Cons\<^sub>2 'a "('a \<times> 'a) biplist"

nonuniform_datatype ('a, 'b) tplist = Nil 'b | Cons 'a "('a \<times> 'a, 'b option) tplist"

term Scratch.arg.tplist_0_0.Rep_arg_tplist_0_0_pre_tplist_0_0
term "Scratch.arg.tplist_0_1.Rep_arg_tplist_0_1_pre_tplist_0_1"

nonuniform_codatatype 'a ptree = Node 'a "'a pforest"
                and 'a pforest = Nil | Cons "'a ptree" "('a \<times> 'a) pforest"

nonuniform_codatatype ('a, 'b) ptree2 = Node 'a "('a, 'b option) pforest2"
                and ('a, 'b) pforest2 = Nil | Cons "('a, 'b list) ptree2" "('a \<times> 'a, 'b) pforest2"

term Scratch.arg.ptree2_0_0.Rep_arg_ptree2_0_0_pre_ptree2_0_0
term Scratch.arg.ptree2_0_1.Rep_arg_ptree2_0_1_pre_ptree2_0_1

term Scratch.ptree.Rep_ptree_pre_ptree
term Scratch.pforest.Rep_pforest_pre_pforest

nonuniform_datatype ('a, 'b) pforest2 = Nil | Cons "('a, 'b list) ptree2" "('a \<times> 'a, 'b option) pforest2"
                and ('a, 'b) ptree2 = Node 'a "('a, 'b) pforest2"

nonuniform_codatatype 'a pstream = PSCons 'a "('a list) pstream"

nonuniform_datatype 'a crazy = Crazy 'a "'a pstream fset crazy multiset list"

declare bi_unique_alt_def[THEN iffD1, THEN conjunct1, transfer_rule]
declare bi_unique_alt_def[THEN iffD1, THEN conjunct2, transfer_rule]

nonuniform_induct xs in
  not_PCons_self: "\<forall>f x. xs \<noteq> PCons x (map_plist f xs)"
subgoal premises prems [transfer_rule] thm prems by transfer_prover
proof -
  show "\<forall>f x. PNil \<noteq> PCons x (map_plist f PNil)"
    by simp
next
  fix y and ys :: "('c \<times> 'c) plist"
  assume IH: "\<forall>f x. ys \<noteq> PCons x (map_plist f ys)"
  show "\<forall>f x. PCons y ys \<noteq> PCons x (map_plist f (PCons y ys))"
    by (simp add: IH)
qed

thm not_PCons_self[rule_format]
thm not_PCons_self[rule_format, where f="\<lambda>x. (x, x)"]


nonuniform_primrec split :: "('a \<times> 'b) plist \<Rightarrow> 'a plist \<times> 'b plist" where
  "split PNil = (PNil, PNil)"
| "split (PCons ab xs) =
     (let (as, bs) = split (map_plist (\<lambda>((a,b),(a',b')). ((a,a'),(b,b'))) xs)
     in (PCons (fst ab) as, PCons (snd ab) bs))"

(*
nonuniform_datatype 'a tm = Var 'a | App "'a tm" "'a tm" | Lam "('a option) tm"
*)








(*
declare [[bnf_internals]]
nonuniform_datatype 'a T = T 'a

nonuniform_datatype ('a, 'b) X = E 'b | T 'a "('a , 'b) X" | U 'a "('a + 'a, 'b) Y"
                and ('a, 'b) Y = VY "('a * 'a, 'b) X"
find_theorems name: cimmerse name: X
find_theorems name: cimmerse name: shape name: X
find_theorems name: flat name: X
find_theorems name: invar name: X
thm X_Y.invar_cimmerse_raw
thm X_Y.unCons_cimmerse_raw
*)
end