theory Het_Data3
imports Main
begin

  datatype typename = T string "typename list"

  consts TNAME :: "'a itself \<Rightarrow> typename"

  (*defs (overloaded) 
    fun_TNAME_def: "TNAME (x:: ('a \<Rightarrow> 'b) itself) \<equiv> T ''fun'' [TNAME TYPE('a), TNAME TYPE('b)]"
    prod_TNAME_def: "TNAME (x:: ('a \<times> 'b) itself) \<equiv> T ''fun'' [TNAME TYPE('a), TNAME TYPE('b)]"
*)
  ML_val \<open>Binding.make ("foo",Position.none)\<close>  

overloading "TNAME (x:: ('a \<Rightarrow> 'b) itself) \<equiv> T ''fun'' [TNAME TYPE('a), TNAME TYPE('b)]"

  ML {*
    let
      val thy = @{theory}

      val tsig = Sign.tsig_of thy;

      val dt = @{cpat "TNAME \<equiv> \<lambda>_::?'a itself. T ?name ?list"}
        |> Thm.term_of
      val tt = @{cpat "TNAME TYPE (?'a)"} |> Thm.term_of 

      val tfrees = map (fn v => TFree (v, []));

      fun mtfrees n = tfrees (Name.invent Name.context Name.aT n)
      fun mtype t args = (Type (t,args))

      fun mtname tfree = Term.subst_TVars [(("'a",0),tfree)] tt
      fun mtnames args = map mtname args 
        |> HOLogic.mk_list @{typ "typename"}

      fun mdef_eq (tname, arity) = let
        val args = mtfrees arity
        val ty = mtype tname args
        val name = HOLogic.mk_string tname
        val list = mtnames args
      in  
        Term.subst_vars ([(("'a",0),ty)],[(("name",0),name),(("list",0),list)]) dt
      end  

      val leqs = Type.logical_types tsig 
        |> map (fn x => (x,Type.arity_number tsig x))
        |> map mdef_eq
        |> Name.invent_names Name.context "TNAME_def"
        |> map (fn (n,eq) => ((Binding.make (n,Position.none),eq),[]))

      val thy = Global_Theory.add_defs true leqs thy

    in
      thy
    end  
  *}



  declare Basic_BNFs.setl.simps[termination_simp]
  declare Basic_BNFs.setr.simps[termination_simp]

  lemma elim_refl_vars[simp]: 
    "(\<And>a. x=a \<Longrightarrow> PROP (P a)) \<equiv> (PROP (P x))"
    "(\<And>a. a=x \<Longrightarrow> PROP (P a)) \<equiv> (PROP (P x))"
    apply rule
    apply (drule meta_spec[where x=x])
    apply simp_all[2]
    apply rule
    apply (drule meta_spec[where x=x])
    apply simp_all[2]
    done

  (*
    C :: 'a T
    D :: int T \<Rightarrow> int T
  *)

  datatype typename = T string "typename list"

  class typename = fixes TNAME :: "'a itself \<Rightarrow> typename"

  instantiation int :: typename begin
    definition [simp]: "TNAME \<equiv> \<lambda>_::int itself. T ''int'' []"
    instance ..
  end  

  instantiation prod :: (typename,typename) typename begin
    definition [simp]: "TNAME \<equiv> \<lambda>_::('a\<times>'b)itself. T ''prod'' [TNAME TYPE('a), TNAME TYPE('b)]"
    instance ..
  end  

  datatype 'a univ = 
    rC
  | rD "'a univ"

  inductive isT :: "'a::typename univ \<Rightarrow> bool" where
    "isT rC"
  | "TNAME TYPE('a) = TNAME TYPE(int) \<Longrightarrow> isT x1 \<Longrightarrow> isT (rD x1)"  

  typedef 'a::typename T = "{x::'a univ. isT x}"
    by (auto intro: isT.intros)

  definition C :: "'a::typename T" where "C \<equiv> Abs_T rC"  
  definition D :: "int T \<Rightarrow> int T" where "D x \<equiv> Abs_T (rD (Rep_T x))"
  
  lemma T_ne: "C \<noteq> D x"
    unfolding C_def D_def
    apply safe
    apply (subst (asm) Abs_T_inject)
    using Rep_T[of x] apply (auto intro!: isT.intros)
    done

  lemma D_inject: "D x = D y \<Longrightarrow> x=y"  
    unfolding D_def
    apply (subst (asm) Abs_T_inject)
    prefer 3
    apply (simp add: Rep_T_inject)
    using Rep_T apply (auto intro!: isT.intros)
    done

  lemma T_exhaust: "x = C \<or> (\<exists>i. x = D i)"  
    apply (cases x; simp)
    apply (simp_all add: C_def D_def)
    apply (subst Abs_T_inject)
    apply simp
    apply (simp add: isT.intros)
    apply (erule isT.cases)
    apply simp_all
    apply (rule_tac x="Abs_T x1" in exI)
    apply (subst Abs_T_inject)
    apply (simp add: isT.intros)
    apply (simp_all add: Abs_T_inverse isT.intros)
    done    

  lemma T_exhaust': "TNAME TYPE('a::typename) \<noteq> TNAME TYPE(int) \<Longrightarrow> (x::'a T) = C"  
    apply (cases x; simp)
    apply (simp_all add: C_def D_def)
    apply (subst Abs_T_inject)
    apply simp
    apply (simp add: isT.intros)
    apply (erule isT.cases)
    apply simp_all
    done    

end
