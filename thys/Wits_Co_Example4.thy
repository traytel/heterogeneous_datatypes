theory Wits_Co_Example4
  imports
    "../BNF_Nonuniform_Fixpoint" 
    "~~/src/HOL/Library/BNF_Axiomatization"
begin
declare [[bnf_internals,typedef_overloaded]]

bnf_axiomatization ('a0,'a1,'T0,'T1) GG
  [wits: "'a0 \<Rightarrow> 'T1 \<Rightarrow> ('a0,'a1,'T0,'T1) GG" 
    "'a1 \<Rightarrow> 'T0 \<Rightarrow> ('a0,'a1,'T0,'T1) GG"
    (* breaks perfectness, but is better for illustration: *)
    "'a0 \<Rightarrow> 'a1 \<Rightarrow> 'T0 \<Rightarrow> 'T1 \<Rightarrow> ('a0,'a1,'T0,'T1) GG"
    ]



bnf_axiomatization ('a0,'a1) FF1_1
  [wits: "'a0 \<Rightarrow> ('a0,'a1) FF1_1" "'a1 \<Rightarrow> ('a0,'a1) FF1_1"]
bnf_axiomatization ('a0,'a1) FF1_2
  [wits: "'a0 \<Rightarrow> 'a1 \<Rightarrow> ('a0,'a1) FF1_2"]
bnf_axiomatization ('a0,'a1) FF2_1 
  [wits: "'a0 \<Rightarrow> ('a0,'a1) FF2_1"]
bnf_axiomatization ('a0,'a1) FF2_2 
  [wits: "'a0 \<Rightarrow> ('a0,'a1) FF2_2"]

(*
nonuniform_codatatype
  ('a,'b) ptree = Node "'a" "('a,'b) pforest" and
  ('a,'b) pforest = Nil "'b" | Cons "('b,'b) ptree" "(('a \<times> 'a),'a) pforest"
  

nonuniform_codatatype
  'a ptree' = Node "'a" "'a pforest'" and
  'a pforest' = Nil | Cons "'a ptree'" "('a \<times> 'a) pforest'"
*)

nonuniform_codatatype
  ('a0, 'a1) T = C "('a0, 'a1, (('a0, 'a1) FF1_1, ('a0, 'a1) FF1_2) T, (('a0, 'a1) FF2_1, ('a0, 'a1) FF2_2) T) GG"

print_theorems


  (* Labels: *)
abbreviation "L1 \<equiv> T.L_2_T"
abbreviation "L2 \<equiv> T.L_1_T"

type_synonym depth = "label_T list"

inductive check_shadow :: "bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG1 :: "bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG2 :: "bool \<Rightarrow> bool \<Rightarrow> bool" where
    "check_GG1 v0 v1 \<Longrightarrow> check_GG2 v0 v1 \<Longrightarrow> check_shadow v0 v1"
|   "v0 \<Longrightarrow> check_shadow v0 v0 \<Longrightarrow> check_GG1 v0 v1"
|   "v1 \<Longrightarrow> check_shadow (v0 \<or> v1) (v0 \<and> v1) \<Longrightarrow> check_GG2 v0 v1"

inductive shadow_start :: "bool \<Rightarrow> bool \<Rightarrow> depth \<Rightarrow> bool" where
    "check_shadow v0 v1 \<Longrightarrow> shadow_start v0 v1 []"
|   "shadow_start (v0 \<or> v1) (v0 \<and> v1) l \<Longrightarrow> shadow_start v0 v1 (Cons L1 l)"
|   "shadow_start v0 v0 l \<Longrightarrow> shadow_start v0 v1 (Cons L2 l)"

(*
fun make_true :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> (bool * bool \<Rightarrow> bool)" where
    "make_true f v0 v1 = (\<lambda> vs. if (v0,v1) = vs then True else f (v0,v1))"

inductive check_shadow :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG1 :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> bool" and
    check_GG2 :: "(bool * bool \<Rightarrow> bool) \<Rightarrow> bool \<Rightarrow> bool \<Rightarrow> bool" where
    "f (v0,v1) \<Longrightarrow> check_shadow f v0 v1"
|   "check_GG1 (make_true f v0 v1) v0 v1 \<Longrightarrow> check_GG2 (make_true f v0 v1) v0 v1 \<Longrightarrow> check_shadow f v0 v1"
|   "v0 \<Longrightarrow> check_shadow f v0 v0 \<Longrightarrow> check_GG1 f v0 v1"
|   "v1 \<Longrightarrow> check_shadow f (v0 \<or> v1) (v0 \<and> v1) \<Longrightarrow> check_GG2 f v0 v1"

inductive shadow_start :: "bool \<Rightarrow> bool \<Rightarrow> depth \<Rightarrow> bool" where
    "check_shadow (\<lambda> v. False) v0 v1 \<Longrightarrow> shadow_start v0 v1 []"
|   "shadow_start (v0 \<or> v1) (v0 \<and> v1) l \<Longrightarrow> shadow_start v0 v1 (Cons L1 l)"
|   "shadow_start v0 v0 l \<Longrightarrow> shadow_start v0 v1 (Cons L2 l)"
*)


    (* THE WITNESS PROOF: *)

    (* Inductive definition of depths matching the transitions between "S"s:  *)
inductive
  is_S1 :: "depth \<Rightarrow> bool" and
  is_S0 :: "depth \<Rightarrow> bool" and
  is_S01 :: "depth \<Rightarrow> bool" 
  where 
    "is_S1 []"
  | "is_S1 u \<Longrightarrow> is_S0 (L1 # u)"
  | "is_S0 u \<Longrightarrow> is_S01 (L2 # u)"
  | "is_S01 u \<Longrightarrow> is_S01 (L1 # u)"
  | "is_S01 u \<Longrightarrow> is_S01 (L2 # u)"

(*
inductive isS and isS0 and isS1 and isS01 where
  "isS0 []"
| "False \<Longrightarrow> isS (L1 # u)"
| "False \<Longrightarrow> isS (L2 # u)"
| "isS1 u \<Longrightarrow> isS0 (L1 # u)"
| "XXX \<Longrightarrow> isS0 (L2 # u)"
| "XXX \<Longrightarrow> isS1 (L1 # u)"
| "XXX \<Longrightarrow> isS1 (L2 # u)"
| "isS01 u \<Longrightarrow> isS01 (L1 # u)"
| "isS0 u \<or> isS01 u \<Longrightarrow> isS01 (L2 # u)"
*)

definition "is_S u = (is_S1 u \<or> is_S0 u \<or> is_S01 u)"

lemmas is_S_Nil = is_S_def[of "[]", unfolded is_S1.simps simp_thms]
  
context
  fixes dummy :: "'a0 \<times> 'a1"
    and a1 :: 'a1
begin

definition pred1 :: "depth \<Rightarrow> ('a0, 'a1) shape_1_T \<Rightarrow> bool" where
  "pred1 u sh1 = (T.invar_shape_1_T u sh1 \<and> pred_shape_1_T (\<lambda>_. False) (\<lambda>x. x = a1) sh1)"
definition pred2 :: "depth \<Rightarrow> ('a0, 'a1) shape_2_T \<Rightarrow> bool" where
  "pred2 u sh2 = (T.invar_shape_2_T u sh2 \<and> pred_shape_2_T (\<lambda>_. False) (\<lambda>x. x = a1) sh2)"

abbreviation "spred u g \<equiv>
  pred_pre_T (pred1 u) (pred2 u) (\<lambda>u'. u' = L2 # u \<and> is_S u') (\<lambda>u'. u' = L1 # u \<and> is_S u') g"

definition "wcoalg u = Eps (spred u)"

primcorec wit :: "depth \<Rightarrow> ('a0,'a1) raw_T" where 
  "wit u = T.raw_T.Ctor_T (map_pre_T id id wit wit (wcoalg u))"
  
  (*
  The statement pred_Si, entirely determined by Si, makes explicit 
  what Si encodes, namely, the existence of witnesses for the given depth. *)

  (* The only inductive proof needed -- shows that if depths follow 
  the transitions encoded in the deep embedding, they achieve what 
  their corresponding sets of states encode. *)

lemma is_pred_S:
  "(is_S1 u \<longrightarrow>(\<exists>sh2::('a0,'a1) shape_2_T. pred2 u sh2)) \<and> 
   (is_S0 u \<longrightarrow> (\<exists>sh1::('a0,'a1) shape_1_T. pred1 u sh1)) \<and>
  (is_S01 u \<longrightarrow> (\<exists>sh1::('a0,'a1) shape_1_T. pred1 u sh1) \<and> (\<exists>sh2::('a0,'a1) shape_2_T. pred2 u sh2))"
  apply (rule is_S1_is_S0_is_S01.induct; unfold pred1_def[abs_def] pred2_def[abs_def];
      ((erule exE conjE)+)?; (rule conjI)?)
         apply (rule
             exI[of _ "T.shape_1_T.leaf_T _"]
             exI[of _ "T.shape_2_T.leaf_T _"]
             exI[of _ "T.shape_1_T.node_1_T (arg.wit1_pre_T_1_0 _)"]
             exI[of _ "T.shape_1_T.node_1_T (arg.wit2_pre_T_1_0 _)"]
             exI[of _ "T.shape_2_T.node_1_T (arg.wit_pre_T_1_1 _ _)"]
             exI[of _ "T.shape_1_T.node_0_T (arg.wit_pre_T_0_0 _)"]
             exI[of _ "T.shape_2_T.node_0_T (arg.wit_pre_T_0_1 _)"],
          ((hypsubst | assumption | drule
          arg.arg.pre_T_0_0.wit
          arg.arg.pre_T_0_1.wit
          arg.arg.pre_T_1_0.wit1
          arg.arg.pre_T_1_0.wit2
          arg.arg.pre_T_1_1.wit | (rule conjI ballI)+ | simp only:
          list.case simp_thms if_True if_False label_T.distinct
          T.invar_shape_1_T.simps T.invar_shape_2_T.simps
          shape_1_T.pred_inject shape_2_T.pred_inject
          arg.arg.pre_T_0_0.pred_set
          arg.arg.pre_T_0_1.pred_set
          arg.arg.pre_T_1_0.pred_set
          arg.arg.pre_T_1_1.pred_set)+; fail) [])+
  done

lemmas is_pred_S1 = is_pred_S[THEN conjunct1, rule_format]
lemmas is_pred_S0 = is_pred_S[THEN conjunct2, THEN conjunct1, rule_format]
lemmas is_pred_S01 = is_pred_S[THEN conjunct2, THEN conjunct2, rule_format]

ML \<open>
fun map f [] = []
  | map f (x :: xs) = f x :: map f xs
\<close>

ML \<open>
local
open Ctr_Sugar_Util
in
val _ = rtac
end
\<close>

  (* Now we reify the coalgebra transitions encoded by the deep embedding: *)
lemma is_S_closed:
  "is_S u \<Longrightarrow> spred u (wcoalg u)"
  unfolding is_S_def wcoalg_def
  apply (tactic \<open>HEADGOAL (Ctr_Sugar_Util.rtac @{context} @{thm someI_ex})\<close>)
(*
  apply (tactic \<open>
    HEADGOAL (eresolve_tac @{context} [disjE] THEN_ALL_NEW
      EVERY' [SELECT_GOAL (print_tac @{context} "foo"), K all_tac])
    \<close>)
*)
  apply (elim disjE; frule is_pred_S1 is_pred_S0 is_pred_S01; unfold pre_T.pred_set; (erule conjE exE)+)
    apply (rule exI[of _ "wit1_pre_T _ _"] exI[of _ "wit2_pre_T _ _"] exI[of _ "wit3_pre_T _ _"],
      ((hypsubst | assumption | drule pre_T.wit1 pre_T.wit2 pre_T.wit3 | rule conjI ballI |
          simp only: is_S1_is_S0_is_S01.intros list.simps simp_thms)+; fail))+
  done
  
  (* Everything else follows from the properties we proved about the 
  states and transitions of the coalgebra. *)
  
lemma pred_raw_T_wit: "is_S u \<Longrightarrow> pred_raw_T (\<lambda>_. False) (\<lambda>x. x = a1) (wit u)"
  apply (rule raw_T.rel_coinduct[THEN iffD2[OF raw_T.pred_rel],
    of "eq_onp (\<lambda>x. \<exists>u. x = wit u \<and> is_S u)"])
   apply (unfold eq_onp_def simp_thms)
   apply (tactic \<open>
         HEADGOAL (fn n => REPEAT_DETERM (resolve_tac @{context} [exI, conjI, refl] n))
    THEN HEADGOAL (assume_tac @{context})
    THEN HEADGOAL (eresolve_tac @{context} [thin_rl])
    THEN HEADGOAL (fn n => REPEAT_DETERM (eresolve_tac @{context} [exE, conjE] n))
    THEN HEADGOAL (hyp_subst_tac_thin true @{context})
    \<close>)
  apply (drule is_S_closed)
  apply (unfold pre_T.pred_rel shape_1_T.pred_rel shape_2_T.pred_rel wit.sel pre_T.rel_map
    id_apply eq_onp_def pred1_def pred2_def)
  apply (erule pre_T.rel_mono_strong)
     apply (erule conjE)+
     apply hypsubst_thin
     apply (erule shape_1_T.rel_mono_strong; (erule conjunct1 | assumption))
    apply (erule conjE)+
    apply hypsubst_thin
    apply (erule shape_2_T.rel_mono_strong; (erule conjunct1 | assumption))
   apply (tactic \<open>
         HEADGOAL (REPEAT_DETERM o (eresolve_tac @{context} [exE, conjE]))
    THEN HEADGOAL (hyp_subst_tac_thin true @{context})
    THEN SELECT_GOAL (HEADGOAL (REPEAT_DETERM o
        ((resolve_tac @{context} [exI, conjI, refl] ) ORELSE'
          (assume_tac @{context} )))) 1
    \<close>)
(*
    apply (erule exE conjE)+
   apply hypsubst_thin
*)
  apply (erule exE conjE)+
  apply hypsubst_thin
  apply ((rule exI conjI refl | assumption)+) []
  done

lemma invar_wit: "is_S u \<Longrightarrow> T.invar_raw_T u (wit u)"
  apply (rule invar_raw_T.coinduct[of "\<lambda>u' w. \<exists>u. u' = u \<and> w = wit u \<and> is_S u"])
   apply (tactic \<open>
         HEADGOAL (fn n => REPEAT_DETERM (resolve_tac @{context} [exI, conjI, refl] n))
    THEN HEADGOAL (assume_tac @{context})
    THEN HEADGOAL (eresolve_tac @{context} [thin_rl])
    THEN HEADGOAL (fn n => REPEAT_DETERM (eresolve_tac @{context} [exE, conjE] n))
    THEN HEADGOAL (hyp_subst_tac_thin true @{context})
    THEN HEADGOAL (fn n => REPEAT_DETERM (resolve_tac @{context} [exI, conjI, refl] n))
    \<close>)
   apply (rule wit.ctr)
  apply (drule is_S_closed)
  apply (unfold pre_T.pred_map)
  apply (erule pre_T.pred_mono_strong)
     apply (unfold id_apply o_apply pred1_def)
     apply (erule conjunct1)
    apply (unfold id_apply o_apply pred2_def)
    apply (erule conjunct1)
   apply (tactic \<open>
     HEADGOAL (eresolve_tac @{context} [conjE])
    THEN HEADGOAL (hyp_subst_tac_thin true @{context})
    THEN HEADGOAL (resolve_tac @{context} [disjI1])
    THEN HEADGOAL (fn n => REPEAT_DETERM (resolve_tac @{context} [exI, conjI, refl] n))
    THEN HEADGOAL (assume_tac @{context})
    THEN HEADGOAL (eresolve_tac @{context} [conjE])
    THEN HEADGOAL (hyp_subst_tac_thin true @{context})
    THEN HEADGOAL (resolve_tac @{context} [disjI1])
    THEN HEADGOAL (fn n => REPEAT_DETERM (resolve_tac @{context} [exI, conjI, refl] n))
    THEN HEADGOAL (assume_tac @{context})
    \<close>)
  done

lemmas invar_wit_Nil = invar_wit[OF is_S_Nil]
lemmas T_wit_Nil =
  pred_raw_T_wit[OF is_S_Nil, unfolded raw_T.pred_set, THEN conjunct1, THEN bspec]
  pred_raw_T_wit[OF is_S_Nil, unfolded raw_T.pred_set, THEN conjunct2, THEN bspec]

end

end
