theory List_Example
imports Main
begin
declare [[ML_print_depth = 10000]]                 
local_setup \<open>
fn lthy => 
  let
    open Ctr_Sugar
    open BNF_Def
    open BNF_Util
    open BNF_Tactics
    open BNF_Comp
    open BNF_FP_Util
    open BNF_FP_Def_Sugar
    open BNF_LFP
    open BNF_LFP_Util
    open BNF_LFP_Tactics
    open BNF_LFP_Rec_Sugar
    open HOLogic


    (* ----- List Example ----- *)

    val ([a], _) = mk_TFrees 1 lthy;
    val tyargs = [(SOME Binding.empty, (a, @{sort type}))];

    val T_b = @{binding lis};
    val T_name = Local_Theory.full_name lthy T_b;
    val T =  Type (T_name, [a]);

    val ctr_specs = [(((Binding.empty, @{binding Nil}), []), NoSyn), 
                     (((Binding.empty, @{binding Cons}), [(@{binding Hd}, a), (@{binding Tl}, T)]), NoSyn)];

    val spec = (((((tyargs, T_b), NoSyn), ctr_specs), (Binding.empty, Binding.empty, Binding.empty)), []);

    val plugins = Plugin_Name.default_filter;
    val discs_sels = true;

    val lthy = lthy |>
        (co_datatypes Least_FP construct_lfp ((plugins, discs_sels), [spec]));


    (* ----- Length Function Definition ----- *)

    val len_b = Binding.name "length";

    val thy = Proof_Context.theory_of lthy;

    fun target_ctr_sugar_of_fp_sugar fpT ({T, fp_ctr_sugar = {ctr_sugar, ...}, ...} : fp_sugar) =
      let
        val rho = Vartab.fold (cons o apsnd snd) (Sign.typ_match thy (T, fpT) Vartab.empty) [];
        val phi = Morphism.term_morphism "BNF" (Term.subst_TVars rho);
      in
        morph_ctr_sugar phi ctr_sugar
      end;

    
    val ctrs = BNF_FP_Def_Sugar.fp_sugar_of lthy T_name
      |> the
      |> target_ctr_sugar_of_fp_sugar T
      |> #ctrs;
    
    val [nil', cons'] = ctrs;
    val len' = Free ("length", T --> natT);
    val x = Free ("x", a);
    val xs = Free ("xs", T);

    val len_nil = len' $ nil' : term;
    val eq1 = mk_Trueprop_eq (len_nil, zero);
    val cons_x_xs = cons' $ x $ xs : term;
    val len_cons = len' $ cons_x_xs : term;
    val len_xs = len' $ xs : term;
    val one_plus_len = mk_Suc len_xs : term;
    val eq2 = mk_Trueprop_eq (len_cons, one_plus_len);

    val eqs = [eq1, eq2];
    
    val ((_, _, simps), lthy) = primrec [(len_b, NONE, NoSyn)] (map (pair Attrib.empty_binding) eqs) lthy;
    val _ = simps |> @{print}
  in
    lthy
  end
\<close>

term lis.Cons

primrec len :: "'a lis \<Rightarrow> nat" where
  "len Nil = 0"
| "len (Cons x xs) = 1 + len xs"

lemma const_len: "len (Cons x Nil) = 1"
by simp

term Nil
term length
thm length.simps

lemma len2: "length (Cons x Nil) = 1"
by simp

end