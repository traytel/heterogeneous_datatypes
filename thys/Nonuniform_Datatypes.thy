theory Nonuniform_Datatypes
imports "../BNF_Nonuniform_Fixpoint"
keywords "nonuniform_datatype" :: thy_decl
begin
declare [[ML_print_depth = 10000]]
(*
nonuniform_datatype 'a list = Nil | Cons 'a "'a list"
nonuniform_datatype 'a plist = Nil | Cons 'a "('a \<times> 'a) plist"
nonuniform_datatype ('a, 'b) tplist = Nil 'b | Cons 'a "('a \<times> 'a, 'b option) tplist"
nonuniform_datatype ('a, 'b) tplist' = Nil 'b | Cons 'a "('a \<times> 'a, 'b option) tplist'" "('a + 'a, 'b) tplist'"
nonuniform_datatype ('a, 'b) tplist1 = Nil 'b | Cons 'a "('a \<times> 'a, 'b option) tplist1" "('a + 'a, 'b) tplist2"
                and ('a, 'b) tplist2 = Nil 'b | Cons 'a "('a \<times> 'b, 'a + 'b) tplist1"
*)














fun flat where
  "flat [] = []"
| "flat ((x, y) # xs) = x # y # flat xs"
consts defobj_pflat :: 'a (*where
  "defobj_pflat \<equiv> \<lambda>g. case g of Inl () \<Rightarrow> [] | Inr (x :: 'a, r) \<Rightarrow> x # flat r"*)

consts defobj_sum :: 'a argobj_sum :: 'a

consts defobj_idF :: 'a defobj_idE :: 'a
  argobj_id1A :: 'a argobj_id1B :: 'a
  argobj_id2A :: 'a argobj_id2B :: 'a
  argobj_id3A :: 'a argobj_id3B :: 'a

consts defobj_sumF :: 'a defobj_sumE :: 'a
  argobj_sum1A :: 'a argobj_sum1B :: 'a
  argobj_sum2A :: 'a argobj_sum2B :: 'a
  argobj_sum3A :: 'a argobj_sum3B :: 'a

consts P1 :: 'a P2 :: 'a

local_setup \<open>
fn lthy =>
  let
    open BNF_Def
    open BNF_Util
    open BNF_Tactics
    open BNF_Comp
    open BNF_FP_Util
    open BNF_FP_Def_Sugar
    open BNF_LFP
    open BNF_LFP_Util
    open BNF_LFP_Tactics
    open BNF_LFP_Rec_Sugar
    open Ctr_Sugar
    open BNF_Tactics
    open BNF_NU_FP
    open BNF_NU_FP_Rec
    open BNF_NU_FP_Ind


    (* ----- Debug data generation ----- *)
    fun composition_bnf lthy (inline_opt, (b, str)) =
      let
        val T = Syntax.read_typ lthy str;
        fun qualify b' = if length (Binding.prefix_of b') <= 2 then b
          else Binding.qualify true (Binding.name_of b) b';
        val inline = (case inline_opt of true => BNF_Def.Dont_Inline | _ => BNF_Def.Do_Inline);
        val ((bnf, (Ds, _)), ((_, unfolds), lthy')) =
          BNF_Comp.bnf_of_typ true inline qualify (distinct (=) o flat) [] [] T
            ((BNF_Comp.empty_comp_cache, BNF_Comp.empty_unfolds), lthy)
      in
        ((bnf, Ds), lthy')
      end;

    fun compose_normalize_seal_bnfs inline_opt defs Ass Ds Xf lthy =
      let
        val (bnf'n'Ds, lthy) = fold (fn def => fn (res, lthy) =>
          composition_bnf lthy (inline_opt, def)
          |>> append res o single)
          defs ([], lthy);
        fun norm_qualify i = Binding.qualify true (nth ["a", "b", "c", "d", "e", "f", "g"] (Int.max (0, i - 1)))
          #> Binding.concealed;
        val (_, (BNFs, ((_, unfolds), lthy))) = normalize_bnfs norm_qualify Ass Ds Xf (map fst bnf'n'Ds)
          ((empty_comp_cache, empty_unfolds), lthy);
        val (bnf_qs, lthy) = @{fold 3} (fn b => fn bnf => fn (_, Ds)=> fn (res, lthy) =>
          seal_bnf I unfolds (Binding.qualify false "seal" b) true Ds Ds bnf lthy
          |>> append res o single)
          (map fst defs) BNFs bnf'n'Ds ([], lthy);
      in (map fst bnf_qs, lthy) end

    (* ----- Easy Example ----- *)
    fun Ex1 lthy = let
      val F_defs = [(Binding.name "F1A", "'a * 'a")];
      val ([a, x], _) = mk_TFrees 2 lthy;
      val ([F1], lthy) = compose_normalize_seal_bnfs true F_defs [[a]] [] (K [a]) lthy;
      val Fss = [[([], F1)]];
      val G_def = [(Binding.name "G", "unit + 'a * 'x")];
      val ([G], lthy) = compose_normalize_seal_bnfs true G_def [[a, x]] [] (K [a, x]) lthy;

      val specs = [(([], G), Fss)];
      val mixfix = [NoSyn];
      val bs = [Binding.name "plist"];
      val map_bs = replicate 1 Binding.empty;
      val rel_bs = replicate 1 Binding.empty;
      val pred_bs = replicate 1 Binding.empty;
      val set_bss = replicate 1 (replicate 1 (SOME Binding.empty));
      val resBs = map dest_TFree [a];

      val (nu_fp_res, lthy) = construct_nu_fp BNF_Util.Least_FP mixfix map_bs rel_bs pred_bs set_bss bs resBs [] specs [] lthy;

      val lthy = register_nu_fp_result_raw "plist" nu_fp_res lthy;

      fun ex1a lthy =
        let
          val f_names = ["pflat"];
          val f_bs = map Binding.name f_names;
          val Ran_bnf = the (bnf_of lthy @{type_name list});
          val Dom_bnf = ID_bnf;
          val defobj = Const (@{const_name defobj_pflat}, mk_T_of_bnf [] [a, mk_T_of_bnf [] [mk_T_of_bnf [] [a] F1] Ran_bnf] G --> HOLogic.listT a);
          val argobj = HOLogic.id_const (mk_T_of_bnf [] [a] F1);
          val defobj_transfer = refl;
          val argobj_transfer = refl;
          val (_, lthy) = construct_nu_rec mixfix f_bs nu_fp_res
            [Dom_bnf] [[]] [[a]]
            [Ran_bnf] [[]] [[a]]
            [((defobj, defobj_transfer), [[(argobj, argobj_transfer)]])] lthy;
        in lthy end

      fun ex1b lthy =
        let
          val f_names = ["sum"];
          val f_bs = map Binding.name f_names;
          val Ran_bnf = DEADID_bnf;
          val Dom_bnf = DEADID_bnf;
          val defobj = Const (@{const_name defobj_sum}, mk_T_of_bnf [] [HOLogic.natT, HOLogic.natT] G --> HOLogic.natT);
          val argobj = Const (@{const_name argobj_sum}, mk_T_of_bnf [] [HOLogic.natT] F1 --> HOLogic.natT);
          (*val defobj = @{term "\<lambda>g. case g of Inl () \<Rightarrow> (0 :: nat) | Inr (x :: nat, r) \<Rightarrow> x + r"};
          val argobj = @{term "\<lambda>(x, y :: nat). x + y"};*)
          val defobj_transfer = refl;
          val argobj_transfer = refl;
          val (_, lthy) = construct_nu_rec mixfix f_bs nu_fp_res
            [Dom_bnf] [[HOLogic.natT]] [[]]
            [Ran_bnf] [[HOLogic.natT]] []
            [((defobj, defobj_transfer), [[(argobj, argobj_transfer)]])] lthy;
        in lthy end

      fun ex1c lthy = 
        let
          val [T_bnf] = #bnfs (#fp_result nu_fp_res);
          val P1 = Const (@{const_name P1}, mk_T_of_bnf [] [a] T_bnf --> HOLogic.boolT);

          val _ = @{print warning} "No P_param and P_ind theorems available!";
          val (_, lthy) = construct_nu_ind nu_fp_res
            [(P1, (refl, refl))] lthy;
        in lthy end

      val lthy = ex1c lthy;
    in lthy end


    (* ----- Multiparam Multi Occurence Example ----- *)
    fun Ex2 lthy = let
      val F_defs = map (apfst Binding.name) [
        ("F1A", "'a + 'b"),
        ("F2A", "'a * 'a"),
        ("F1B", "'b"),
        ("F2B", "'a * 'b")];

      val G_def = [(Binding.name "G", "'b + 'a * 'x * 'y")];
      val ([a, b, x, y], _) = mk_TFrees 4 lthy;

      val Ass = [[a, b],[a],[b],[a, b]];
      val Xs = [a, b];
      val Ds = [];
  
      val (F_bnfs, lthy) = compose_normalize_seal_bnfs true F_defs Ass Ds (K Xs) lthy;
      val Fss = Library.unflat [[0, 0], [0, 0]] (map (pair []) F_bnfs);
  
      val Ass = [[b, a, x, y]];
      val Xs = [a, b, x, y];
  
      val ([G], lthy) = compose_normalize_seal_bnfs true G_def Ass Ds (K Xs) lthy;

      
      val specs = [(([], G), Fss)];
      val mixfix = [NoSyn];
      val bs = [Binding.name "myType"];
      val map_bs = replicate 1 Binding.empty;
      val rel_bs = replicate 1 Binding.empty;
      val pred_bs = replicate 1 Binding.empty;
      val set_bss = replicate 1 (replicate 2 Binding.empty);

      val resBs = map dest_TFree [a, b]
      
      val (_, lthy) = construct_nu_fp mixfix map_bs rel_bs pred_bs set_bss bs resBs [] specs [] lthy;

    in lthy end

    (* ----- Mutual Example ----- *)
    fun Ex3 lthy = let
      val F_defs = map (apfst Binding.name) [
        ("F1A", "'c \<Rightarrow> 'a + 'a"),
        ("F1B", "'b * 'a"),
        ("F2A", "'a list"),
        ("F2B", "'c \<Rightarrow> 'b list"),
        ("E3A", "'a * ('c \<Rightarrow> 'b)"),
        ("E3B", "'b")];

      val G_defs = map (apfst Binding.name) [
        ("G1", "('c \<Rightarrow> 'b) + 'x * 'a * 'z"),
        ("G2", "'c \<Rightarrow> 'a * 'b + ('c \<Rightarrow> 'y)")];

      val names = ["myTypeA", "myTypeB"];

      val ([a, b, c, x, y, z], _) = mk_TFrees 6 lthy;

      val Ass = [[a], [b, a], [a], [b], [a, b], [b]];
      val Dss = [[c], [], [], [c], [c], []];
      val Xs = [a, b];
      val Ds = [c];
      val (F_bnfs, lthy) = compose_normalize_seal_bnfs true F_defs Ass Ds (K Xs) lthy;
      val Fss = Library.unflat [[0, 0], [0, 0]] (take 4 (Dss ~~ F_bnfs)); (* transpose when using *)
      val Ess = Library.unflat [[0, 0]] (drop 4 (Dss ~~ F_bnfs)); (* transpose when using *)

      val Ass = [[b, x, a, z], [a, b, y]];
      val Xs = [a, b, x, y, z];

      val ([G1, G2], lthy) = compose_normalize_seal_bnfs true G_defs Ass Ds (K Xs) lthy;

      val specs = [(([c], G1), transpose Fss), (([c], G2), transpose Ess)];
      val mixfix = [NoSyn, NoSyn] : mixfix list;
      val bs = map Binding.name names;
      val map_bs = replicate 2 Binding.empty;
      val rel_bs = replicate 2 Binding.empty;
      val pred_bs = replicate 2 Binding.empty;
      val set_bss = replicate 2 (replicate 2 Binding.empty);

      val resBs = map dest_TFree [c, a, b]
      
      val (nu_fp_res, lthy) = construct_nu_fp mixfix map_bs rel_bs pred_bs set_bss bs resBs [c] specs [] lthy;

      fun ex3a lthy =
        let
          val f_names = ["multi_id_G1", "multi_id_G2"];
          val f_bs = map Binding.name f_names;
          val Ran_bnfs = #bnfs (#fp_result nu_fp_res);
          val (RF, RE) = (hd Ran_bnfs, hd (tl Ran_bnfs));
          val Ran_Dss = [[c], [c]];
          val Ran_As = [a, b];
          val Dom_bnfs = replicate 2 ID_bnf;
          val Dom_As = [[a],[b]];
          val Fss = Library.unflat [[0, 0], [0, 0], [0, 0]] (Dss ~~ F_bnfs);

          val defobj_names = [@{const_name defobj_idF},
                          @{const_name defobj_idE}];
          val argobj_namess = [
            [@{const_name argobj_id1A}, @{const_name argobj_id1B}],
            [@{const_name argobj_id2A}, @{const_name argobj_id2B}],
            [@{const_name argobj_id3A}, @{const_name argobj_id3B}]];

          val F_Ass = map (map (fn (Ds, F) => mk_T_of_bnf Ds [a, b] F)) Fss;
          val R_F_As = @{map 2} (mk_T_of_bnf [c]) F_Ass [RF, RF, RE];
          val D_As = @{map 2} (mk_T_of_bnf []) Dom_As Dom_bnfs;
          val G_DR_As = map (mk_T_of_bnf [c] (D_As @ R_F_As)) [G1, G2];
          val R_As = map (mk_T_of_bnf [c] Ran_As) Ran_bnfs;
          val F_Dss = map (map (fn (Ds, F) => mk_T_of_bnf Ds D_As F)) Fss;
          val D_Fss = map (@{map 2} (fn D => fn F => mk_T_of_bnf [] [F] D) Dom_bnfs) F_Ass;

          val defobj_transfer = refl;
          val argobj_transfer = refl;
          val [defobjF, defobjE] = @{map 3} (fn name => fn G => fn R => Const (name, G --> R))
            defobj_names G_DR_As R_As |> map (rpair defobj_transfer);
          val [[argobj1A, argobj1B], [argobj2A, argobj2B], [argobj3A, argobj3B]] =
            @{map 3} (@{map 3} (fn name => fn F => fn D => Const (name, F --> D)))
            argobj_namess F_Dss D_Fss |> map (map (rpair argobj_transfer));

          val (_, lthy) = construct_nu_rec mixfix f_bs nu_fp_res
            Dom_bnfs [[], []] [[a], [b]]
            Ran_bnfs [[c], [c]] [[a, b]]
            [(defobjF, [[argobj1A, argobj2A], [argobj1B, argobj2B]]),
             (defobjE, [[argobj3A], [argobj3B]])] lthy;
        in lthy end

      fun ex3b lthy =
        let
          val f_names = ["sum_T1", "sum_T2"];
          val f_bs = map Binding.name f_names;
          val (RF, RE) = (DEADID_bnf, DEADID_bnf);
          val Ran_bnfs = [RF, RE];
          val Ran_Dss = [[HOLogic.natT], [HOLogic.natT]];
          val Ran_As = [];
          val Dom_bnfs = replicate 2 DEADID_bnf;
          val Dom_Dss = [[HOLogic.natT], [HOLogic.natT]];
          val Dom_As = [[],[]];
          val Fss = Library.unflat [[0, 0], [0, 0], [0, 0]] (Dss ~~ F_bnfs);

          val defobj_names = [@{const_name defobj_sumF},
                          @{const_name defobj_sumE}];
          val argobj_namess = [
            [@{const_name argobj_sum1A}, @{const_name argobj_sum1B}],
            [@{const_name argobj_sum2A}, @{const_name argobj_sum2B}],
            [@{const_name argobj_sum3A}, @{const_name argobj_sum3B}]];

          val Gs = map (mk_T_of_bnf [c] (replicate 5 HOLogic.natT)) [G1, G2];
          val Rs = replicate 2 HOLogic.natT;
          val Fss = map (map (fn (Ds, F) => mk_T_of_bnf Ds (replicate 2 HOLogic.natT) F)) Fss;
          val Dss = map (map (fn F => HOLogic.natT)) Fss;

          val defobj_transfer = refl;
          val argobj_transfer = refl;
          val [defobjF, defobjE] = @{map 3} (fn name => fn G => fn R => Const (name, G --> R))
            defobj_names Gs Rs |> map (rpair defobj_transfer);
          val [[argobj1A, argobj1B], [argobj2A, argobj2B], [argobj3A, argobj3B]] =
            @{map 3} (@{map 3} (Const oo rpair oo curry op -->))
            Fss Dss argobj_namess |> map (map (rpair argobj_transfer));

          val (_, lthy) = construct_nu_rec mixfix f_bs nu_fp_res
            Dom_bnfs Dom_Dss Dom_As
            Ran_bnfs Ran_Dss Ran_As
            [(defobjF, [[argobj1A, argobj2A], [argobj1B, argobj2B]]),
             (defobjE, [[argobj3A], [argobj3B]])] lthy;
        in lthy end

      fun ex3c lthy = 
        let
          val [T1_bnf, T2_bnf] = #bnfs (#fp_result nu_fp_res);
          val P1 = Const (@{const_name P1}, mk_T_of_bnf [c] [a, b] T1_bnf --> HOLogic.boolT);
          val P2 = Const (@{const_name P2}, mk_T_of_bnf [c] [a, b] T2_bnf --> HOLogic.boolT);
          
          val _ = @{print warning} "No P_param and P_ind theorems available!";
          val (_, lthy) = construct_nu_ind nu_fp_res
            [(P1, (refl, refl)), (P2, (refl, refl))] lthy;
        in lthy end

      val lthy = ex3c lthy;
      val lthy = register_nu_fp_result_raw "myType1" nu_fp_res lthy

    in lthy end

    val lthy = Ex1 lthy

    fun a b = let
    in 0 end
  in
    lthy
  end
\<close>

declare [[ML_print_depth = 0]]
ML \<open>BNF_NU_FP.nu_fp_result_of @{context} "myType1" |> the\<close>
declare [[ML_print_depth = 10000]]


term T_ctr
term T_dtr
term map_plist
term "map_plist (\<lambda>(v1, v2). v1 + (v2::nat))"
term rec_list
term rec_raw_0
typ "nat raw_0"
term Cons
term Leaf
term Label_0
term Node_0
term Shape_0.Leaf
term Shape_1.Node_0
term Shape_1.Node_1
term invar_shape1
term flat_shape11
term unflat_shape11
term T_ctr1

thm invar_shape1.simps
thm flat_shape11.simps
thm unflat_shape11.simps
thm Shape_0.map
thm myTypeA.map_comp
term map_myTypeA
print_bnfs
term map_raw_0
term map_raw_1
term map_myTypeB
thm "Shape_0.set"

term f_T

end