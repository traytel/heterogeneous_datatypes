theory Wits_Co_Example
imports "../BNF_Nonuniform_Fixpoint" 
begin
declare [[bnf_internals]]
nonuniform_codatatype ('a, 'b) s =
    Cons1 'a "('a * 'b, 'b) s"
  | Cons2 'b "('a, 'a + 'b) s"

(*
  G = 'a * 'x + 'b * 'y

  FA1 = 'a * 'b
    [1, 2]
  FB1 = 'b
    [2]
  FA2 = 'a
    [1]
  FB2 = 'a + 'b
    [1], [2]
*)
datatype label = L1 | L2
consts Leaf :: 'a Node :: 'a
  wit1_FA1 :: 'a wit1_FB1 :: 'a wit1_FA2 :: 'a wit1_FB2 :: 'a wit2_FB2 :: 'a
  wit1_G :: 'a wit2_G :: 'a
  unfold :: 'a

(* recursing only through Cons1 *)
primrec mkA1 :: "label list \<Rightarrow> 'a \<Rightarrow> 'b \<Rightarrow> 'c" and mkB1 where
    "mkA1 [] a b = Leaf a"
  | "mkA1 (L1 # u0) a b = Node (wit1_FA1 (mkA1 u0 a b) (mkB1 u0 a b))"
  | "mkB1 [] a b = Leaf b"
  | "mkB1 (L1 # u0) a b = Node (wit1_FB1 (mkB1 u0 a b))"
(* unfold term for this wit *)
term "unfold (\<lambda>u0. wit1_G (mkA1 u0 a b) (L1 # u0)) []"(* :: 'a \<Rightarrow> 'b \<Rightarrow> raw*)

(* recursing only through Cons2 with wit1_FB2 *)
primrec mkA2 and mkB2 where
    "mkA2 [] a b = Leaf a"
  | "mkA2 (L2 # u0) a b = Node (wit1_FA2 (mkA2 u0 a b))"
  | "mkB2 [] a b = Leaf b"
  | "mkB2 (L2 # u0) a b = Node (wit1_FB2 (mkA2 u0 a b))"
(* unfold term for this wit *)
term "unfold (\<lambda>u0. wit2_G (mkB2 u0 a b) (L2 # u0)) []"(* :: 'a \<Rightarrow> 'b \<Rightarrow> raw*)

(* recursing only through Cons2 with wit2_FB2 *)
primrec mkB3 where
    "mkB3 [] b = Leaf b"
  | "mkB3 (L2 # u0) b = Node (wit2_FB2 (mkB3 u0 b))"
(* unfold term for this wit *)
term "unfold (\<lambda>u0. wit2_G (mkB3 u0 b) (L2 # u0)) []"(* :: 'b \<Rightarrow> raw*)



ML \<open>

local
open BNF_Util
open BNF_Def
open BNF_FP_Util
open BNF_GFP_Util
open BNF_GFP_Tactics
open BNF_LFP_Rec_Sugar
in
fun mk_undefined T = Const (@{const_name undefined}, T);
val mk_Frees_of = map2 (Free oo pair o Binding.name_of);
fun mk_decl b t = (b, SOME t, NoSyn);
val mk_const_val = Const oo pair o fst o dest_Const;
fun mk_tree_args (I, T) (I', Ts) = (sort_distinct int_ord (I @ I'), T :: Ts);

val nr_param = 2;
val nr_Ts = 1;
val nr_occ = 2;

(* calculates the possible infinite witnesses
   depending on the possible labels and the
   invokations *)
fun mk_coind_shape_wits G_wits F_witss i =
  let
    val _ = F_witss: (int list * term) list list;
    fun get_As (I, _) = filter (fn i => i <= nr_param) I;
    fun get_Xs (I, _) = filter (fn i => i > nr_param) I;
    
    
    
  in [] end


(* produces a finite shape witness satisfying the depth invariant *)
fun mk_shape_terms [] ys leaves _ _ i = [([i], nth leaves i $ nth ys i)]
  | mk_shape_terms (label :: depth) ys leaves nodess F_witsss i =
  let
    val F_wits = nth (nth F_witsss label) i;
    val node = nth (nth nodess label) i;
    fun mk_wit F_wit xs = node $ (list_comb (F_wit, xs));
  in
    map (fn (I, F_wit) =>
      let
        val xs = map (mk_shape_terms depth ys leaves nodess F_witsss) I;
        val inner_wits = fold_rev (map_product mk_tree_args) xs [([], [])]
          |> minimize_wits;
      in
        map (apsnd (mk_wit F_wit)) inner_wits
      end) F_wits
    |> flat |> minimize_wits
  end

(*  defines a recursive function that generates shapes
    satisfying a certain depth. The witnesses for F are fixed.
    "I" are the arguments this function will have and also the
    number of functions generated
    "labels" are the occuring labels
    "F_witss" has dimensions <length I> x <length labels>,
    e.g. the exact witness to use for each F.  *)
fun mk_shape_fun bs I labels F_witss As shape_As labelT leaves nodess label_case lthy =
  let
    val nr_occ = length (hd nodess);
    val As = map (nth As) I;
    val shape_As = map (nth shape_As) I;
    val leaves = map (nth leaves) I;
    val nodess = map (nth (map (nth nodess) labels)) I;
    val depthT = HOLogic.listT labelT;
    val shape_fun_Ts = map (fn s => depthT --> (As ---> s)) shape_As;
    val shape_fun_vals = mk_Frees_of bs shape_fun_Ts;
    val (((A_vals, l_val), u0), _) = lthy
      |> mk_Frees "x" As
      ||>> yield_singleton (mk_Frees "label") labelT
      ||>> yield_singleton (mk_Frees "u0") depthT;

    fun mk_leaf_eq xs shape_fun leaf x = HOLogic.mk_eq (list_comb (
      shape_fun $ HOLogic.nil_const labelT, xs), leaf $ x);

    fun mk_node_eq labels xs shape_funs shape_fun shape_A nodes F_wits =
      let
        val nth_shape_fun = AList.lookup (=) (labels ~~ shape_funs) #> the;
        val rhs_defineds = map2 (fn node => fn F_wit =>
          node $ (list_comb o swap o apfst (map (fn i => list_comb (nth_shape_fun i $ u0, xs)))) F_wit)
          nodes F_wits;
        val rhs_alls = map (AList.lookup (=) (labels ~~ rhs_defineds)
          #> the_default (mk_undefined shape_A)) (1 upto nr_occ);
      in
        HOLogic.mk_eq (list_comb (shape_fun $ (HOLogic.cons_const labelT $ l_val $ u0), xs),
        list_comb (label_case shape_A $ l_val, rhs_alls))
      end

    val leaf_eqs = @{map 3} (mk_leaf_eq A_vals) shape_fun_vals A_vals leaves;
    val node_eqs = @{map 4} (mk_node_eq labels A_vals shape_fun_vals)
      shape_fun_vals shape_As nodess F_witss;
    val decls = map2 mk_decl bs shape_fun_Ts;

    val ((shape_fun_frees, _, shape_fun_simps_free), (lthy, lthy_old)) = lthy
      |> Local_Theory.begin_nested |> snd
      |> primrec false [] decls (map (fn eq => ((Binding.empty_atts, eq), [], [])) (leaf_eqs @ node_eqs))
      ||> `Local_Theory.end_nested;

    val phi = Proof_Context.export_morphism lthy_old lthy;
    val shape_fun_vals = map2 mk_const_val shape_fun_frees shape_fun_Ts;
    val shape_fun_simps = map (Morphism.fact phi) shape_fun_simps_free |> flat;
  in (shape_fun_vals, shape_fun_simps) end



fun mk_any_wit G_wits F_witss As shape_As labelT raw_unfolds leaves nodess u0 label_ctors label_case lthy =
  let
    val _ = F_witss: (int list * term) list list;
    val I = 1 upto nr_param;
    val bs = map (Binding.name o prefix "mk_shape" o string_of_int) I;
    val labels = 1 upto nr_occ;
    val shape_funs = mk_shape_fun bs I labels F_witss As shape_As labelT leaves nodess label_case lthy
      |> fst;
    val shape_fun_vals = map (fn (I, wit_val) =>
      lambda u0 (list_comb (wit_val, map (fn i => 
      if i <= nr_param then nth shape_funs i
      else HOLogic.cons_const labelT $ (nth label_ctors (i - nr_param)) $ u0) I))) G_wits;
  in
    map (fn unfold => list_comb (unfold, shape_fun_vals) $ HOLogic.nil_const labelT) raw_unfolds
  end



datatype wit_tree = Wit_Leaf of int | Wit_Node of (int * int * int list) * wit_tree list;
                                              (* <nr_Ts wit_id Is *)

fun mk_tree_args (I, T) (I', Ts) = (sort_distinct int_ord (I @ I'), T :: Ts);

fun finish Iss m seen i (nwit, I) =
  let
    val treess = map (fn j =>
        if j < m orelse member ((=)) seen j then [([j], Wit_Leaf j)]
        else
          map_index (finish Iss m (insert ((=)) j seen) j) (nth Iss (j - m))
          |> flat
          |> minimize_wits)
      I;
  in
    map (fn (I, t) => (I, Wit_Node ((i - m, nwit, filter (fn i => i < m) I), t)))
      (fold_rev (map_product mk_tree_args) treess [([], [])])
    |> minimize_wits
  end;

fun tree_to_ctor_wit vars _ _ (Wit_Leaf j) = ([j], nth vars j)
  | tree_to_ctor_wit vars ctors witss (Wit_Node ((i, nwit, I), subtrees)) =
     (I, nth ctors i $ (Term.list_comb (snd (nth (nth witss i) nwit),
       map (snd o tree_to_ctor_wit vars ctors witss) subtrees)));

fun tree_to_coind_wits _ (Wit_Leaf _) = []
  | tree_to_coind_wits lwitss (Wit_Node ((i, nwit, I), subtrees)) =
     ((i, I), nth (nth lwitss i) nwit) :: maps (tree_to_coind_wits lwitss) subtrees;

fun mk_ctor_witss decode mk_arg m Dss As Ts pre_bnfs ctors mk_unfold unfold_thms setss_by_range
    set_inducts lthy =
  let
    val n = length pre_bnfs;
    val live = n + m;
    val ks = 1 upto n;
    val ls = 1 upto m;
    val set_maps = maps set_map_of_bnf pre_bnfs;

    val ((((ys, ys'), Jzs), (ys_copy, ys'_copy)), _) = lthy
      |> mk_Frees' "y" As
      ||>> mk_Frees "z" Ts
      ||>> mk_Frees' "y" As;

    fun close_wit I wit = (I, fold_rev Term.absfree (map (nth ys') I) wit);

    val all_unitTs = replicate live HOLogic.unitT;
    val unitTs = replicate n HOLogic.unitT;
    val unit_funs = replicate n (Term.absdummy HOLogic.unitT HOLogic.unit);
    fun mk_map_args I =
      map (fn i =>
        if member ((=)) I i then Term.absdummy HOLogic.unitT (nth ys i)
        else mk_undefined (HOLogic.unitT --> nth As i))
      (0 upto m - 1);

    fun mk_nat_wit Ds pre_bnf (I, wit) () =
      let
        val passiveI = filter (fn i => i < m) I;
        val map_args = mk_map_args passiveI;
      in
        Term.absdummy HOLogic.unitT (Term.list_comb
          (mk_map_of_bnf Ds all_unitTs (As @ unitTs) pre_bnf, map_args @ unit_funs) $ wit)
      end;

    fun mk_dummy_wit Ds pre_bnf I =
      let
        val map_args = mk_map_args I;
      in
        Term.absdummy HOLogic.unitT (Term.list_comb
          (mk_map_of_bnf Ds all_unitTs (As @ unitTs) pre_bnf, map_args @ unit_funs) $
          mk_undefined (mk_T_of_bnf Ds all_unitTs pre_bnf))
      end;

    val nat_witss =
      map2 (fn Ds => fn pre_bnf => mk_wits_of_bnf (replicate (nwits_of_bnf pre_bnf) Ds)
        (replicate (nwits_of_bnf pre_bnf) (replicate live HOLogic.unitT)) pre_bnf
        |> map (fn (I, wit) =>
          (I, Lazy.lazy (mk_nat_wit Ds pre_bnf (I, Term.list_comb (wit, map (K HOLogic.unit) I))))))
      Dss pre_bnfs;

    val nat_wit_thmss = map2 (curry op ~~) nat_witss (map wit_thmss_of_bnf pre_bnfs)

    val Iss = map (map fst) nat_witss;

    fun filter_wits (I, wit) =
      let val J = filter (fn i => i < m) I;
      in (J, (length J < length I, wit)) end;

    val wit_treess = map_index (fn (i, Is) =>
      map_index (finish Iss m [i+m] (i+m)) Is) Iss
      |> map (minimize_wits o map filter_wits o minimize_wits o flat);

    val coind_wit_argsss =
      map (map (tree_to_coind_wits nat_wit_thmss o snd o snd) o filter (fst o snd)) wit_treess;

    val nonredundant_coind_wit_argsss =
      fold (fn i => fn argsss =>
        nth_map (i - 1) (filter_out (fn xs =>
          exists (fn ys =>
            let
              val xs' = (map (fst o fst) xs, snd (fst (hd xs)));
              val ys' = (map (fst o fst) ys, snd (fst (hd ys)));
            in
              eq_pair (subset ((=))) (eq_set ((=))) (xs', ys') andalso not (fst xs' = fst ys')
            end)
          (flat argsss)))
        argsss)
      ks coind_wit_argsss;

    fun prepare_args args =
      let
        val I = snd (fst (hd args));
        val (dummys, args') =
          map_split (fn i =>
            (case find_first (fn arg => fst (fst arg) = i - 1) args of
              SOME (_, ((_, wit), thms)) => (NONE, (Lazy.force wit, thms))
            | NONE =>
              (SOME (i - 1), (mk_dummy_wit (nth Dss (i - 1)) (nth pre_bnfs (i - 1)) I, []))))
          ks;
      in
        ((I, dummys), apsnd flat (split_list args'))
      end;

    fun mk_coind_wits ((I, dummys), (args, thms)) =
      ((I, dummys), (map (fn i => mk_unfold Ts args i $ HOLogic.unit) ks, thms));

    val coind_witss =
      maps (map (mk_coind_wits o prepare_args)) nonredundant_coind_wit_argsss;

    val witss = map2 (fn Ds => fn pre_bnf => mk_wits_of_bnf
      (replicate (nwits_of_bnf pre_bnf) Ds)
      (replicate (nwits_of_bnf pre_bnf) (As @ Ts)) pre_bnf) Dss pre_bnfs;

    val ctor_witss =
      map (map (uncurry close_wit o tree_to_ctor_wit ys ctors witss o snd o snd) o
        filter_out (fst o snd)) wit_treess;

    fun mk_coind_wit_thms ((I, dummys), (wits, wit_thms)) =
      let
        fun mk_goal sets y y_copy y'_copy j =
          let
            fun mk_conjunct set z dummy wit =
              mk_Ball (set $ z) (Term.absfree y'_copy
                (if dummy = NONE orelse member ((=)) I (j - 1) then
                  HOLogic.mk_imp (HOLogic.mk_eq (z, wit),
                    if member ((=)) I (j - 1) then HOLogic.mk_eq (y_copy, y)
                    else @{term False})
                else @{term True}));
          in
            HOLogic.mk_Trueprop
              (Library.foldr1 HOLogic.mk_conj (@{map 4} mk_conjunct sets Jzs dummys wits))
          end;
        val goals = @{map 5} mk_goal setss_by_range ys ys_copy ys'_copy ls;
      in
        map2 (fn goal => fn induct =>
          Variable.add_free_names lthy goal []
          |> (fn vars => Goal.prove_sorry lthy vars [] goal
            (fn {context = ctxt, prems = _} => mk_coind_wit_tac ctxt induct unfold_thms set_maps
              wit_thms))
          |> Thm.close_derivation Position.none)
        goals set_inducts
        |> map split_conj_thm
        |> transpose
        |> map (map_filter (try (fn thm => thm RS bspec RS mp)))
        |> curry op ~~ (map_index Library.I (map (close_wit I) wits))
        |> filter (fn (_, thms) => length thms = m)
      end;

    val coind_wit_thms = maps mk_coind_wit_thms coind_witss;

    val (wit_thmss, all_witss) =
      fold (fn ((i, wit), thms) => fn witss =>
        nth_map i (fn (thms', wits) => (thms @ thms', wit :: wits)) witss)
      coind_wit_thms (map (pair []) ctor_witss)
      |> map (apsnd (map snd o minimize_wits))
      |> split_list
  in
    (wit_thmss, all_witss)
end;

end
\<close>

end