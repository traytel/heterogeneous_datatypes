theory "HDatatype-copy"
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin
  
primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

section \<open>Input\<close>

declare [[bnf_internals, typedef_overloaded]]

(*
datatype 'a l = N | C 'a "('a * 'a) l"

('a, 'x) G = unit + 'a * 'x
'a F = 'a * 'a

specs = [(G, [[F]])]
*)

bnf_axiomatization 'a F

bnf_axiomatization ('a, 'x) G [wits: "'a \<Rightarrow> ('a, 'x) G"]

section \<open>Raw Type\<close>

datatype label = F
type_synonym depth = "label list"
datatype 'a shape = Leaf 'a | Node "'a shape F"
datatype 'a raw = Cons "('a shape, 'a raw) G"

abbreviation "un_Leaf u \<equiv> case u of Leaf x \<Rightarrow> x"
abbreviation "un_Node u \<equiv> case u of Node x \<Rightarrow> x"
abbreviation "un_Cons t \<equiv> case t of Cons x \<Rightarrow> x"


section \<open>Invariant\<close>

fun invar_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> bool" where
  "invar_shape u0 (Leaf u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape u0 (Node f1) = (case u0 of F # u0 \<Rightarrow> pred_F (invar_shape u0) f1 | _ \<Rightarrow> False)"

function invar :: "depth \<Rightarrow> 'a raw \<Rightarrow> bool" where
  "invar u0 (Cons g) = pred_G (invar_shape u0) (invar (F # u0)) g"
sorry
termination sorry


section \<open>The Type\<close>

definition "wit x = Cons (wit_G (Leaf x))"

lemma invar_wit: "invar [] (wit x)"
(*  by (auto simp only:
    wit_def invar.simps invar_shape.simps G.pred_map o_def id_apply G.pred_set
    G.set_map list.case dest: G.wit) *)
sorry

typedef 'a T = "{t :: 'a raw. invar [] t}"
  by (rule exI[of _ "wit undefined"]) (auto simp only: invar_wit)


section \<open>Flat and Unflat\<close>

primrec (transfer)
  flat_shape :: "'a F shape \<Rightarrow> 'a shape" where
  "flat_shape (Leaf f1) = Node (map_F Leaf f1)"
| "flat_shape (Node f1) = Node (map_F flat_shape f1)"

primrec (nonexhaustive)
   unflat_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> 'a F shape" where
  "unflat_shape u0 (Node f1) =
      (case u0 of
        [] \<Rightarrow> Leaf (map_F un_Leaf f1)
      | _ # u0 \<Rightarrow> Node (map_F (unflat_shape u0) f1))"

primrec (transfer) flat :: "'a F raw \<Rightarrow> 'a raw" where
  "flat (Cons g) = Cons (map_G flat_shape flat g)"

primrec unflat :: "depth \<Rightarrow> 'a raw \<Rightarrow> 'a F raw" where
  "unflat u0 (Cons g) = Cons (map_G (unflat_shape u0) (unflat (F # u0)) g)"


section \<open>Constructor and Selector\<close>

definition T :: "('a, 'a F T) G \<Rightarrow> 'a T" where
  "T g = Abs_T (Cons (map_G Leaf (flat o Rep_T) g))"

definition un_T :: "'a T \<Rightarrow> ('a, 'a F T) G" where
  "un_T t = map_G un_Leaf (Abs_T o unflat []) (un_Cons (Rep_T t))"


section \<open>BNF Instance\<close>

lemma invar_shape_map_closed_raw:
  "\<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u"
(* 
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u" u])
  apply (auto simp only:
      shape.map invar_shape.simps
      F.pred_map
      o_apply
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas invar_shape_map_closed =
  spec[OF invar_shape_map_closed_raw]

lemma invar_map_closed_raw:
  "\<forall>u0. invar u0 (map_raw f t) \<longleftrightarrow> invar u0 t"
(* 
  apply (induct t)
  apply (auto simp only:
      raw.map invar.simps id_apply o_apply
      G.pred_map invar_shape_map_closed
    elim!: G.pred_mono_strong)
  done
*) sorry
lemmas invar_map_closed =
  spec[OF invar_map_closed_raw]

lift_bnf 'a T
  apply (auto simp only:
      invar_map_closed)
  done


section \<open>Lemmas about Flat, Unflat, Invar\<close>

lemma invar_shape_depth_iff:
  "invar_shape [] x = (\<exists>a. x = Leaf a)"
  "invar_shape (F # u0) x = (\<exists>y. x = Node y \<and> pred_F (invar_shape u0) y)"
(* 
  by (cases x; simp add: F.pred_map)+
*)
sorry

lemma flat_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u" u])
  apply (auto simp only:
    unflat_shape.simps flat_shape.simps
    invar_shape.simps F.pred_map F.map_comp
    shape.case invar_shape_depth_iff snoc.simps snoc_neq_Nil
    id_apply o_apply
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas flat_shape_unflat_shape =
  mp[OF spec[OF flat_shape_unflat_shape_raw]]

lemma unflat_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u" u])
  apply (auto simp only:
      unflat_shape.simps flat_shape.simps invar_shape.simps
      F.pred_map F.map_comp F.pred_True
      shape.case
      id_apply o_apply refl
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas unflat_shape_flat_shape =
  mp[OF spec[OF unflat_shape_flat_shape_raw]]

lemma invar_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)" u])
  apply (auto simp only:
      flat_shape.simps invar_shape.simps snoc.simps
      F.pred_map F.pred_True
      id_apply o_apply
    elim!: F.pred_mono_strong
    intro: F.pred_mono_strong[OF iffD2[OF fun_cong[OF F.pred_True] TrueI]]
    split: list.splits label.splits)
  done
*) sorry

lemmas invar_shape_flat_shape =
  mp[OF spec[OF invar_shape_flat_shape_raw]]

lemma invar_flat_raw: "\<forall>u0. invar u0 x \<longrightarrow> invar (snoc F u0) (flat x)"
  apply (induct x)
  apply (auto simp only:
      flat.simps invar.simps snoc.simps[symmetric] invar_shape_flat_shape id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemmas invar_flat = mp[OF spec[OF invar_flat_raw]]

lemma invar_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)" u])
  apply (auto simp only:
      unflat_shape.simps invar_shape.simps snoc.simps snoc_neq_Nil
      F.pred_map id_apply o_apply refl
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas invar_shape_unflat_shape =
  mp[OF spec[OF invar_shape_unflat_shape_raw]]

lemma invar_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> invar u0 (unflat u0 t)"
(*
  apply (induct t)
  apply (auto simp only:
      unflat.simps invar.simps snoc.simps invar_shape_unflat_shape id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done
*)
sorry

lemmas invar_unflat = mp[OF spec[OF invar_unflat_raw]]

lemma flat_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> flat (unflat u0 t) = t"
(*
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar.simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done
*) sorry

lemmas flat_unflat = mp[OF spec[OF flat_unflat_raw]]

lemma unflat_flat_raw: "\<forall>u0. invar u0 t \<longrightarrow> unflat u0 (flat t) = t"
(*
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar.simps unflat_shape_flat_shape id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done
*) sorry

lemmas unflat_flat = mp[OF spec[OF unflat_flat_raw]]


section \<open>Constructor is Bijection\<close>

lemma un_T_T: "un_T (T x) = x"
(*
  unfolding T_def un_T_def
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
    intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.case shape.case Rep_T_inverse o_apply
      G.map_comp G.map_ident Rep_T[unfolded mem_Collect_eq] unflat_flat
    intro!: trans[OF G.map_cong G.map_ident]) []
  done
*) sorry

lemma T_un_T: "T (un_T x) = x"
(*
  unfolding T_def un_T_def G.map_comp o_def
  apply (rule iffD1[OF Rep_T_inject])
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (insert Rep_T[simplified, of x])
  apply (rule raw.exhaust[of "Rep_T x"])
  apply (auto simp only:
      raw.case shape.case invar.simps invar_shape_depth_iff
       snoc.simps(1)[of F, symmetric]
      G.pred_map Abs_T_inverse invar_unflat flat_unflat id_apply o_apply mem_Collect_eq
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong) []
  done
*) sorry


section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

lemma flat_shape_map:
  "map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)"
  apply (rule shape.induct[of
    "\<lambda>u. map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)" u])
  apply (auto simp only:
      shape.map flat_shape.simps F.map_comp o_apply
    intro!: F.map_cong0)
  done

lemma map_raw_flat: "map_raw f (flat t) = flat (map_raw (map_F f) t)"
  apply (induct t)
  apply (auto simp only:
      raw.map flat.simps G.map_comp flat_shape_map o_apply
    intro!: G.map_cong0)
  done

lemma map_T: "map_T f (T t) = T (map_G f (map_T (map_F f)) t)"
(*
  unfolding map_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.map shape.map G.map_comp o_apply mem_Collect_eq
     invar_map_closed Abs_T_inverse Rep_T[unfolded mem_Collect_eq] map_raw_flat
    intro!: arg_cong[of _ _ "\<lambda>x. Abs_T (raw.Cons x)"] G.map_cong0) []
  done
*) sorry


subsection \<open>set\<close>

lemma flat_shape_set:
  fixes u :: "'a F shape"
  shows
  "set_shape (flat_shape u) = UNION (set_shape u) set_F"
  apply (rule shape.induct[of
    "\<lambda>u. set_shape (flat_shape u) = UNION (set_shape u) set_F" u])
  apply (auto simp only:
      flat_shape.simps shape.set F.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set_raw_flat:
  "set_raw (flat t) = UNION (set_raw t) set_F"
  apply (induct t)
  apply (auto simp only:
      flat.simps raw.set shape.set G.set_map flat_shape_set
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set_T: "set_T (T g) = set1_G g \<union>
  (\<Union>(set_F ` (\<Union>(set_T ` (set2_G g)))))"
(*
  unfolding set_T_def T_def o_apply
  apply -
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.set shape.set G.set_map set_raw_flat o_apply
        UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong) []
  done
*)
sorry

subsection \<open>rel\<close>

lemma flat_shape_rel_raw:
  "(\<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
     rel_shape (rel_F R) u u')"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
    rel_shape (rel_F R) u u'"
    u])
   apply (auto 0 4 simp only:
     invar_shape.simps flat_shape.simps shape.rel_inject
     invar_shape_depth_iff ball_simps id_apply
     F.rel_map pred_F_def F.set_map
     elim!: F.rel_mono_strong
     split: list.splits label.splits)
  done

lemma flat_shape_rel:
  "invar_shape u0 u \<Longrightarrow> invar_shape u0 u' \<Longrightarrow>
    rel_shape R (flat_shape u) (flat_shape u') = rel_shape (rel_F R) u u'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF flat_shape_rel_raw]]]]]; assumption)
  done

lemma rel_raw_flat_raw:
  "\<forall>t' u0. invar u0 t \<longrightarrow> invar u0 t' \<longrightarrow>
   rel_raw R (flat t) (flat t') \<longrightarrow> rel_raw (rel_F R) t t'"
(*
  apply (induct t)
  apply (rule allI)
  apply (case_tac t')
  apply (auto simp only:
      invar.simps flat.simps raw.rel_inject G.rel_map G.pred_set flat_shape_rel G.set_map ball_simps id_apply
    elim!: G.rel_mono_strong)
  done
*) sorry

lemma rel_raw_flat:
  "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
   rel_raw R (flat t) (flat t') = rel_raw (rel_F R) t t'"
  apply (rule iffI[rotated, OF rel_funD[OF flat.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF rel_raw_flat_raw]]]]]; assumption)
  done

lemma rel_T: "rel_T R (T g) (T g') = rel_G R (rel_T (rel_F R)) g g'"
(*
  unfolding rel_T_def T_def vimage2p_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) [2]
  apply (simp only:
    raw.rel_inject G.rel_map shape.rel_inject o_apply
    rel_raw_flat[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]])
  done
*)
sorry

(*normal datatype recursor
  (('b, 'a) G \<Rightarrow> 'a) \<Rightarrow>
  'b T \<Rightarrow> 'a
*)

(*generalized recursor
  (\<forall>'a. ('a D, ('b F, 'c F) R) G \<Rightarrow> ('b, 'c) R) \<Rightarrow>
  (\<forall>'a. 'a D F \<Rightarrow> 'a F D) \<Rightarrow>
  'a D T \<Rightarrow> ('b, 'c) R 
*)

(*
  pflat 

  



*)


bnf_axiomatization ('b, 'c) R
bnf_axiomatization ('a, 'd, 'e) D

ML \<open>
local

open Ctr_Sugar_Util
open BNF_Util

in
fun mk_arg_cong ctxt n t =
  let
    val Us = fastype_of t |> strip_typeN n |> fst;
    val ((xs, ys), _) = ctxt
      |> mk_Frees "x" Us
      ||>> mk_Frees "y" Us;
    val goal = Logic.list_implies (@{map 2} (curry mk_Trueprop_eq) xs ys,
      mk_Trueprop_eq (list_comb (t, xs), list_comb (t, ys)));
    val vars = Variable.add_free_names ctxt goal [];
  in
    Goal.prove_sorry ctxt vars [] goal (fn {context = ctxt, prems = _} =>
      HEADGOAL (hyp_subst_tac ctxt THEN' rtac ctxt refl))
    |> Thm.close_derivation
  end;

end
\<close>

local_setup \<open>fn lthy =>
  let
    val n = 3;
    val map_D_cong = mk_arg_cong lthy (n + 1) @{term map_D} OF (replicate n ext);
    val pred_D_cong = mk_arg_cong lthy (n + 1) @{term pred_D} OF (replicate n ext);
  in
    lthy
    |> Local_Theory.note ((@{binding map_D_cong}, []), [map_D_cong]) |> snd
    |> Local_Theory.note ((@{binding pred_D_cong}, []), [pred_D_cong]) |> snd
  end
\<close>

consts defobj :: "(('a, 'd, 'e) D, ('b F, 'c F) R) G \<Rightarrow> ('b, 'c) R"
consts argobj :: "('a, 'd, 'e) D F \<Rightarrow> ('a F, 'd F, 'e F) D"

axiomatization where
  defobj_transfer: "rel_fun (rel_G (rel_D A D E) (rel_R (rel_F B) (rel_F C))) (rel_R B C) defobj defobj" and
  argobj_transfer: "rel_fun (rel_F (rel_D A D E)) (rel_D (rel_F A) (rel_F D) (rel_F E)) argobj argobj"

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
(*
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto
*) sorry

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto

lemma R_pred_mono: "P1 \<le> Q1 \<Longrightarrow> P2 \<le> Q2 \<Longrightarrow> pred_R P1 P2 \<le> pred_R Q1 Q2"
  using R.pred_mono_strong[of P1 P2 _ Q1 Q2] by auto

lemma defobj_invar: "pred_fun (pred_G (pred_D A D E) (pred_R (pred_F B) (pred_F C))) (pred_R B C) defobj"
  unfolding fun_pred_rel G.rel_eq_onp[symmetric] F.rel_eq_onp[symmetric]
    D.rel_eq_onp[symmetric] R.rel_eq_onp[symmetric] by (rule defobj_transfer)

lemma argobj_invar: "pred_fun (pred_F (pred_D A D E)) (pred_D (pred_F A) (pred_F D) (pred_F E)) argobj"
  unfolding fun_pred_rel F.rel_eq_onp[symmetric] D.rel_eq_onp[symmetric] by (rule argobj_transfer)

lemma defobj_natural: "defobj (map_G (map_D a d e) (map_R (map_F b) (map_F c)) x) = map_R b c (defobj x)"
  using rel_funD[OF defobj_transfer, of "BNF_Def.Grp UNIV a" "BNF_Def.Grp UNIV d" "BNF_Def.Grp UNIV e" "BNF_Def.Grp UNIV b" "BNF_Def.Grp UNIV c"]
  unfolding F.rel_Grp R.rel_Grp D.rel_Grp G.rel_Grp by (simp add: Grp_def)

lemma argobj_natural: "argobj (map_F (map_D a d e) x) = map_D (map_F a) (map_F d) (map_F e) (argobj x)"
  using rel_funD[OF argobj_transfer, of "BNF_Def.Grp UNIV a" "BNF_Def.Grp UNIV d" "BNF_Def.Grp UNIV e"]
  unfolding F.rel_Grp R.rel_Grp D.rel_Grp by (simp add: Grp_def)

primrec f_shape :: "('a, 'd, 'e) D shape \<Rightarrow> ('a shape, 'd shape, 'e shape) D" where
  "f_shape (Leaf a) = map_D Leaf Leaf Leaf a"
| "f_shape (Node f) = map_D Node Node Node (argobj (map_F f_shape f))"

primrec f_raw :: "('a, 'd, 'e) D raw \<Rightarrow> ('b shape, 'c shape) R" where
  "f_raw (Cons g) = defobj (map_G f_shape (map_R un_Node un_Node o f_raw) g)"

definition f :: "('a, 'd, 'e) D T \<Rightarrow> ('b, 'c) R" where
  "f t = map_R un_Leaf un_Leaf (f_raw (Rep_T t))"

lemma f_shape_flat_shape: "f_shape (flat_shape x) = map_D flat_shape flat_shape flat_shape (f_shape (map_shape argobj x))"
  apply (rule shape.induct[of "\<lambda>x. f_shape (flat_shape x) = map_D flat_shape flat_shape flat_shape (f_shape (map_shape argobj x))" x])
   apply (simp only: flat_shape.simps f_shape.simps shape.map D.map_comp F.map_comp argobj_natural
     o_apply cong: F.map_cong map_D_cong)
  apply (simp only: flat_shape.simps f_shape.simps shape.map F.map_comp D.map_comp o_apply
    D.map_comp[unfolded o_def, symmetric] argobj_natural[symmetric]
    cong: F.map_cong map_D_cong)
  done

lemma invar_shape_f_shape[THEN spec, THEN mp]:
  "\<forall>u0. invar_shape u0 u \<longrightarrow> pred_D (invar_shape u0) (invar_shape u0) (invar_shape u0) (f_shape u)"
  apply (rule shape.induct[of "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> pred_D (invar_shape u0) (invar_shape u0) (invar_shape u0) (f_shape u)" u])
   apply (simp only: invar_shape.simps f_shape.simps D.pred_map D.pred_True o_apply
     cong: pred_D_cong)
  apply (auto simp only: invar_shape.simps f_shape.simps D.pred_map D.pred_True o_apply
    simp_thms all_simps list.inject list.distinct F.pred_map
    intro!: pred_funD[OF argobj_invar]
    elim!: F.pred_mono_strong
    cong: pred_D_cong
    split: list.splits label.splits)
  done

lemma invar_f_raw[THEN spec, THEN mp]:
  "\<forall>u0. invar u0 x \<longrightarrow> pred_R (invar_shape u0) (invar_shape u0) ((f_raw :: ('a F, 'b F, 'c F) D raw \<Rightarrow> (('d F) shape, ('e F) shape) R) (map_raw argobj x))"
  apply (rule raw.induct[of "\<lambda>x. \<forall>u0. invar u0 x \<longrightarrow> pred_R (invar_shape u0) (invar_shape u0) (f_raw (map_raw argobj x))" x])
  apply (rule allI)
  subgoal premises IH for g u0
    apply (auto simp only: invar.simps raw.map f_raw.simps G.map_comp o_apply G.pred_map R.pred_map
    invar_shape_map_closed invar_shape_depth_iff shape.case
    intro!: pred_funD[OF defobj_invar[of "invar_shape u0" "invar_shape u0" "invar_shape u0"]] invar_shape_f_shape
      predicate1D[OF R_pred_mono, rotated 2, OF mp[OF spec[OF IH, of _ "F # u0"]]]
    elim!: G.pred_mono_strong
    cong: G.map_cong)
    done
  done

lemma f_raw_flat[THEN spec, THEN mp]:
  "\<forall>u0. invar u0 x \<longrightarrow> f_raw (flat x) = map_R flat_shape flat_shape (f_raw (map_raw argobj x))"
  apply (rule raw.induct[of "\<lambda>x. \<forall>u0. invar u0 x \<longrightarrow> f_raw (flat x) = map_R flat_shape flat_shape (f_raw (map_raw argobj x))" x])
  apply (rule allI)
  subgoal premises IH for g u0
    apply (auto simp only: flat.simps f_raw.simps G.map_comp o_apply
      f_shape_flat_shape raw.map defobj_natural[of flat_shape flat_shape flat_shape, symmetric]
      invar.simps G.pred_set mp[OF spec[OF IH, of _ "F # u0"]]
      R.map_comp invar_shape_depth_iff shape.case flat_shape.simps
      intro!: arg_cong[of _ _ defobj] G.map_cong0 R.map_cong_pred
        predicate1D[OF R_pred_mono, rotated 2, OF invar_f_raw[of "F # u0"]])
    done
  done

lemma "f (T g) = defobj (map_G id (f o map_T argobj) g)"
  apply (simp only: f_def[abs_def] map_T_def T_def o_apply cong: G.map_cong)
  apply (subst (1 2) Abs_T_inverse)
    apply (simp only: mem_Collect_eq invar_map_closed_raw Rep_T[unfolded mem_Collect_eq])
   apply (simp only: mem_Collect_eq invar.simps G.pred_map G.pred_True o_apply
     invar_shape.simps list.case invar_flat Rep_T[unfolded mem_Collect_eq] snoc.simps[symmetric]
     cong: G.pred_cong)
  apply (auto simp only: G.map_comp F.map_comp F.map_ident D.map_comp D.map_ident R.map_comp
    f_raw.simps f_shape.simps shape.case flat_shape.simps
    defobj_natural[of un_Leaf un_Leaf un_Leaf, symmetric] invar_shape_depth_iff
    f_raw_flat[OF Rep_T[unfolded mem_Collect_eq]] o_apply id_apply
    intro!: arg_cong[of _ _ defobj] G.map_cong0 R.map_cong_pred
      predicate1D[OF R_pred_mono, rotated 2, OF invar_f_raw[OF Rep_T[unfolded mem_Collect_eq]]]
    cong: F.map_cong map_D_cong)
  done

hide_const (open) f

section \<open>Induction\<close>

(* Preliminaries *)

lemma raw_induct_un_Cons: 
assumes "\<And> r :: 'a raw. (\<And>r'. r' \<in> set2_G (un_Cons r) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
shows "PP (r ::'a raw)"
apply(induct r) using assms
using raw.case by fastforce

lemma invar_map_raw[simp]: 
assumes "invar ul r"
shows "invar ul (map_raw f r)"
using assms apply(induct r arbitrary: ul)
by simp (metis invar.simps invar_map_closed raw.map)

lemma map_T_Abs_T: "invar [] r \<Longrightarrow> map_T f (Abs_T r) = Abs_T (map_raw f r)" 
unfolding map_T_def by (simp add: Abs_T_inverse)

(* The "recursion sizes" of elements of 'a raw -- on the second argument of G *)
hide_const size
abbreviation unit where "unit \<equiv> \<lambda> _. ()"
datatype sz = SCons (un_SCons:"(unit, sz) G")

primrec size :: "'a raw \<Rightarrow> sz" where
"size (Cons g) = SCons (map_G unit size g)"

lemma size_unflat: 
"size (unflat ul r) = size (r :: 'a raw)"
by (induct r arbitrary: ul) (auto intro: G.map_cong simp: G.map_comp)

lemma size_map_raw[simp]: 
"size (map_raw Node r) = size r"
by (induct r) (auto intro: G.map_cong simp: G.map_comp)

lemma raw_induct_size: 
assumes "\<And> r :: 'a raw. (\<And>r'. size r' \<in> set2_G (un_SCons (size r)) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
shows "PP (r ::'a raw)"
proof-
  {fix sz and r :: "'a raw" assume "sz = size r"
   hence "PP r" apply(induct sz arbitrary: r)
   using assms by simp (metis sz.sel)
  }
  thus ?thesis by simp
qed

lemma invar_set2_G_un_Leaf: 
assumes r: "invar [] r" and r': "r' \<in> set2_G (map_G un_Leaf (unflat []) (un_Cons r))"
(is "r' \<in> ?A")
shows "invar [] r'"  
proof-
  have "?A = unflat [] ` set2_G (un_Cons r)" unfolding G.set_map(2) ..
  then obtain r1 where r1: "r1 \<in> set2_G (un_Cons r)" and r': "r' = unflat [] r1"
  using r' by auto
  from r1 r have "invar [F] r1" by (metis invar.elims(2) pred_G_def raw.case)
  thus ?thesis unfolding r' by (simp add: invar_unflat)
qed


(*  THE ASSUMPTIONS: *)

consts P :: "'a T \<Rightarrow> bool"

axiomatization where 
P_ind: "\<And> t :: 'a T. (\<And> t'. t' \<in> set2_G (un_T t) \<Longrightarrow> P t') \<Longrightarrow> P t"
and 
(* Type-monotonicity a la J (but in the reverse direction, from large to small): 
a very weak form of (one-way) parametricity, which *is* satisfied 
by equality and others:  *)
P_mono: "inj f \<Longrightarrow> P (map_T f t) \<Longrightarrow> P t"
(* For example: universally quantified equalities of polymorphic functions 
(arguably covering 90% of interesting statements)
are type-monotonic. 

TOCHECK: How many statements from the List library are compositionally type-monotonic? 

Note: Nominal equivariance is weaker: it refers bijections, not injections.
*)

(* Note: 
The majority of the interesting polymorphic predicates we want to prove are 
not a priori parametric, i.e., do not consist of parametric-only components. 

The main reason is that equality of polymorphic (co)datatypes is not 
parametric (because uses the nonparametric equality on the type variables), 
and many concepts use equality, including set operations (e.g., union is parametric, 
but intersection is not). 

To see how restricted is the full parametricity condition for predicates: 
a parametric property on usual lists 
is essentially a property of numbers! Things like "distinct" and many others are not 
in this category...

Of course, a property that is proved true is trivially parametric, 
but this is discovered after the fact (and not by a compositional analysis).

Note that assumption P_ind follows easily from a local version of P_ind + full parametricity 
(or rather parametricity w.r.t. total relation) -- but in the light of the above discussion this 
no longer looks very useful.  
*)


lemma P_raw: 
fixes r :: "'a shape raw" assumes "invar [] r" shows "P (Abs_T r)"
using assms proof(induct r rule: raw_induct_size)
  fix r::"'a shape raw" 
  assume 0: "\<And>r':: 'a shape raw. size r' \<in> set2_G (un_SCons (size r)) \<Longrightarrow> invar [] r' \<Longrightarrow> P (Abs_T r')"
  assume r: "invar [] r"
  show "P (Abs_T r)" proof(rule P_ind)
    fix t' assume t': "t' \<in> set2_G (un_T (Abs_T r))" (is "t' \<in> ?A")
    have "?A = Abs_T ` (set2_G (map_G un_Leaf (unflat []) (un_Cons r)))" (is "_ = _ ` ?B")
    unfolding un_T_def by (simp add: Abs_T_inverse G.set_map(2) image_comp r)
    then obtain r' where r': "r' \<in> ?B" and t': "t' = Abs_T r'" using t' by auto
    hence ir': "invar [] r'" using r by (simp add: invar_set2_G_un_Leaf)
    have "\<And> r''. r'' \<in> set2_G (un_Cons r) \<Longrightarrow> invar [F] r''"using r 
    by (metis invar.elims(2) pred_G_def raw.case)
    hence size_r': "size r' \<in> set2_G (un_SCons (size r))" 
    using r' unfolding G.set_map(2) 
    by auto (smt size_unflat G.set_map(2) imageI invar.elims(2) r raw.case size.simps sz.sel)
    def r'' \<equiv> "map_raw Node r'"
    have size_r'': "size r' = size r''" unfolding r''_def by simp
    have ir'': "invar [] r''" unfolding r''_def using ir' by simp 
    have r'': "P (Abs_T r'')" using 0[OF size_r'[unfolded size_r''] ir''] .
    have 1: "Abs_T r'' = map_T Node (Abs_T r')" unfolding r''_def map_T_Abs_T[OF ir', symmetric] .. 
    have 2: "inj Node" unfolding inj_on_def by auto
    show "P t'" using r'' 2 P_mono unfolding t' 1 by auto
  qed
qed

lemma P_end_product: 
fixes t :: "'a T" shows "P t"
proof-
  def t' \<equiv> "map_T Leaf t" 
  obtain r :: "'a shape raw" where r: "invar [] r" and t': "t' = Abs_T r"
  by (metis Rep_T Rep_T_inverse mem_Collect_eq) 
  have P: "P t'" using P_raw[OF r] unfolding t' .
  have 1: "inj Leaf" unfolding inj_on_def by auto
  show ?thesis using P_mono[OF 1 P[unfolded t'_def]] .
qed

end