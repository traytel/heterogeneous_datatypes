theory HDatatype_anotherCopy
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

(* 
(* Parameters common to induction and recursion: *)
(* p is a list of 'parameter' type variables *)
typedecl ('p,'a) T
typedecl ('p,'a) F
typedecl 'p M
consts r :: "'p M rel"
consts m :: "('p,'a) T \<Rightarrow> 'p M"

(* Induction: *)

consts P :: "('p,'a) T \<Rightarrow> bool"

(* ASSUMPTION: *)
axiomatization where
  P_wf_F: "\<forall> t :: ('p,'a) T. (ALL t' :: ('p, ('p,'a)F) T. (m t', m t) \<in> r \<longrightarrow> P t') \<Longrightarrow> P t"

(* CONCLUSION: *)
lemma assumes "wf (r::'p M rel)" shows "P (t::('p,'a) T)"
using P_wf_F
sorry

typedecl ('p,'a) R

(* Recursion: *)  
consts recdef :: "(('p, ('p,'a)F) T \<Rightarrow> ('p, ('p,'a)F) R) \<Rightarrow> ('p,'a) T \<Rightarrow> ('p,'a) R"

(* ASSUMPTION: *)
axiomatization where
 adm_recdef_m: "\<And>f g t. (\<forall>t'. (m t', m t) \<in> r \<longrightarrow> f t' = g t') \<Longrightarrow> recdef f t = recdef g t"

(* CONCLUSION: *)
consts f :: "('p,'a) T \<Rightarrow> ('p,'a) R"

axiomatization where f_def: "f t = recdef f t"

(* Now can easily define things like map and zip; no parametricity requirement, 
  no BNF or relator requirement for the target type. *)
*)

(*  *)

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

lemma bi_unique_Grp: "bi_unique (BNF_Def.Grp A f) = inj_on f A"
  unfolding bi_unique_def Grp_def inj_on_def by auto

lemma bi_unique_eq_onp: "bi_unique (eq_onp P)"
  unfolding eq_onp_def bi_unique_def by simp

(*
lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  (* unfolding pred_fun_def rel_fun_def eq_onp_def by auto *)
sorry

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto
*)

section \<open>Input\<close>

declare [[bnf_internals, typedef_overloaded]]

(*
datatype 'a l = N | C 'a "('a * 'a) l"

('a, 'x) G = unit + 'a * 'x
'a F = 'a * 'a

specs = [(G, [[F]])]
*)

bnf_axiomatization 'a F

bnf_axiomatization 'b V  (* only needed for recursion *)

bnf_axiomatization ('a, 'x) G [wits: "'a \<Rightarrow> ('a, 'x) G"]

section \<open>Raw Type\<close>

datatype label = F
type_synonym depth = "label list"
datatype 'a shape = Leaf 'a | Node "'a shape F"
datatype 'a shapeV = LeafV 'a | NodeV "'a shapeV V"
datatype 'a raw = Cons "('a shape, 'a raw) G"

abbreviation "un_Leaf u \<equiv> case u of Leaf x \<Rightarrow> x"
abbreviation "un_Node u \<equiv> case u of Node x \<Rightarrow> x"
abbreviation "un_LeafV u \<equiv> case u of LeafV x \<Rightarrow> x"
abbreviation "un_NodeV u \<equiv> case u of NodeV x \<Rightarrow> x"

abbreviation "un_Cons t \<equiv> case t of Cons x \<Rightarrow> x"


section \<open>Invariant\<close>

fun invar_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> bool" where
  "invar_shape u0 (Leaf u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape u0 (Node f1) = (case u0 of F # u0 \<Rightarrow> pred_F (invar_shape u0) f1 | _ \<Rightarrow> False)"

lemma invar_shape_induct[consumes 1, case_names Leaf Node, induct pred: invar_shape]: 
assumes i: "invar_shape u0 s"
and leaf: "\<And>u. PP [] (Leaf u)" 
and node: "\<And>u0 f1. pred_F (invar_shape u0) f1 \<Longrightarrow> pred_F (PP u0) f1 \<Longrightarrow> PP (F # u0) (Node f1)"
shows "PP u0 s"
using assms by (induct s arbitrary: u0) (auto simp: Cons pred_F_def split: list.splits label.splits)

fun invar_shapeV :: "depth \<Rightarrow> 'a shapeV \<Rightarrow> bool" where
  "invar_shapeV u0 (LeafV u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shapeV u0 (NodeV f1) = (case u0 of F # u0 \<Rightarrow> pred_V (invar_shapeV u0) f1 | _ \<Rightarrow> False)"

lemma invar_shapeV_induct[consumes 1, case_names LeafV NodeV, induct pred: invar_shapeV]: 
assumes i: "invar_shapeV u0 s"
and leaf: "\<And>u. PP [] (LeafV u)" 
and node: "\<And>u0 f1. pred_V (invar_shapeV u0) f1 \<Longrightarrow> pred_V (PP u0) f1 \<Longrightarrow> PP (F # u0) (NodeV f1)"
shows "PP u0 s"
using assms by (induct s arbitrary: u0) (auto simp: Cons pred_V_def split: list.splits label.splits)

lemma G_pred_mono: 
"a \<le> a1 \<Longrightarrow> pred_G p a \<le> pred_G p a1"
sorry

inductive invar :: "depth \<Rightarrow> 'a raw \<Rightarrow> bool" where
  "pred_G (invar_shape u0) (invar (F # u0)) g \<Longrightarrow> invar u0 (Cons g)"
monos G_pred_mono

lemmas invar_simps = invar.simps[of _ "Cons _", unfolded simp_thms(39,40) ex_simps raw.inject]

section \<open>The Type\<close>

definition "wit x = Cons (wit_G (Leaf x))"

lemma invar_wit: "invar [] (wit x)"
(*
  by (auto simp only:
    wit_def invar_simps invar_shape.simps G.pred_map o_def id_apply G.pred_set
    G.set_map list.case dest: G.wit)
*)
sorry

typedef 'a T = "{t :: 'a raw. invar [] t}"
  by (rule exI[of _ "wit undefined"]) (auto simp only: invar_wit)


section \<open>Flat and Unflat\<close>

primrec (transfer)
  flat_shape :: "'a F shape \<Rightarrow> 'a shape" where
  "flat_shape (Leaf f1) = Node (map_F Leaf f1)"
| "flat_shape (Node f1) = Node (map_F flat_shape f1)"

primrec (transfer)
  flat_shapeV :: "'a V shapeV \<Rightarrow> 'a shapeV" where
  "flat_shapeV (LeafV f1) = NodeV (map_V LeafV f1)"
| "flat_shapeV (NodeV f1) = NodeV (map_V flat_shapeV f1)"

lemma inj_LeafV: "inj LeafV"
and inj_NodeV: "inj NodeV"
by (meson injI shapeV.inject)+

lemma inj_map_V_LeafV: "inj (map_V LeafV)"
and inj_map_V_NodeV: "inj (map_V NodeV)"
by (auto simp: V.inj_map inj_LeafV inj_NodeV)

(*
lemma inj_flat_shapeV: "inj (flat_shapeV :: 'a V shapeV \<Rightarrow> 'a shapeV)"
unfolding inj_on_def proof safe
  fix s s' :: "'a V shapeV"
  assume "flat_shapeV s = flat_shapeV s'"
  thus "s = s'"
  proof(induct s arbitrary: s') 
    case (LeafV u) note L = LeafV
    thus ?case
    proof(induct s')
      case (LeafV u') thus ?case 
      by simp (meson injD inj_map_V_LeafV)
    next
      case (NodeV f1)
      {assume "set_V (map_V LeafV u) = set_V (map_V flat_shapeV f1)"
       hence False unfolding V.set_map using NodeV apply auto sledgehammer
      }
      thus ?case using NodeV by auto
    qed
*)  
    
primrec (nonexhaustive)
   unflat_shape :: "depth \<Rightarrow> 'a shape \<Rightarrow> 'a F shape" where
  "unflat_shape u0 (Node f1) =
      (case u0 of
        [] \<Rightarrow> Leaf (map_F un_Leaf f1)
      | _ # u0 \<Rightarrow> Node (map_F (unflat_shape u0) f1))"

primrec (nonexhaustive)
   unflat_shapeV :: "depth \<Rightarrow> 'a shapeV \<Rightarrow> 'a V shapeV" where
  "unflat_shapeV u0 (NodeV f1) =
      (case u0 of
        [] \<Rightarrow> LeafV (map_V un_LeafV f1)
      | _ # u0 \<Rightarrow> NodeV (map_V (unflat_shapeV u0) f1))"

primrec (transfer) flat :: "'a F raw \<Rightarrow> 'a raw" where
  "flat (Cons g) = Cons (map_G flat_shape flat g)"

primrec unflat :: "depth \<Rightarrow> 'a raw \<Rightarrow> 'a F raw" where
  "unflat u0 (Cons g) = Cons (map_G (unflat_shape u0) (unflat (F # u0)) g)"


section \<open>Constructor and Selector\<close>

definition T :: "('a, 'a F T) G \<Rightarrow> 'a T" where
  "T g = Abs_T (Cons (map_G Leaf (flat o Rep_T) g))"

definition un_T :: "'a T \<Rightarrow> ('a, 'a F T) G" where
  "un_T t = map_G un_Leaf (Abs_T o unflat []) (un_Cons (Rep_T t))"


section \<open>BNF Instance\<close>

lemma invar_shape_map_closed_raw:
  "\<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 (map_shape f u) \<longleftrightarrow> invar_shape u0 u" u])
  apply (auto simp only:
      shape.map invar_shape.simps
      F.pred_map
      o_apply
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas invar_shape_map_closed =
  spec[OF invar_shape_map_closed_raw]

lemma invar_shapeV_map_closed_raw:
  "\<forall>u0. invar_shapeV u0 (map_shapeV f u) \<longleftrightarrow> invar_shapeV u0 u"
sorry

lemmas invar_shapeV_map_closed =
  spec[OF invar_shapeV_map_closed_raw]

lemma invar_map_closed_raw:
  "\<forall>u0. invar u0 (map_raw f t) \<longleftrightarrow> invar u0 t"
(*
  apply (induct t)
  apply (auto simp only:
      raw.map invar_simps id_apply o_apply
      G.pred_map invar_shape_map_closed
    elim!: G.pred_mono_strong)
  done
lemmas invar_map_closed =
  spec[OF invar_map_closed_raw]
*)
sorry

lift_bnf 'a T
(*
  apply (auto simp only:
      invar_map_closed)
  done
*)
sorry



section \<open>Lemmas about Flat, Unflat, Invar\<close>

lemma invar_shape_depth_iff:
  "invar_shape [] x = (\<exists>a. x = Leaf a)"
  "invar_shape (F # u0) x = (\<exists>y. x = Node y \<and> pred_F (invar_shape u0) y)"
(*
  by (cases x; simp add: F.pred_map)+
*)
sorry

lemma invar_shapeV_depth_iff:
  "invar_shapeV [] x = (\<exists>a. x = LeafV a)"
  "invar_shapeV (F # u0) x = (\<exists>y. x = NodeV y \<and> pred_V (invar_shapeV u0) y)"
(*
  by (cases x; simp add: F.pred_map)+
*)
sorry

lemma flat_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> flat_shape (unflat_shape u0 u) = u" u])
  apply (auto simp only:
    unflat_shape.simps flat_shape.simps
    invar_shape.simps F.pred_map F.map_comp
    shape.case invar_shape_depth_iff snoc.simps snoc_neq_Nil
    id_apply o_apply
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas flat_shape_unflat_shape =
  mp[OF spec[OF flat_shape_unflat_shape_raw]]

lemma flat_shape_unflat_shapeV_raw:
  fixes u :: "'a shapeV"
  shows
  "\<forall>u0. invar_shapeV (snoc F u0) u \<longrightarrow> flat_shapeV (unflat_shapeV u0 u) = u"
sorry

lemmas flat_shape_unflat_shapeV =
  mp[OF spec[OF flat_shape_unflat_shapeV_raw]]

lemma unflat_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> unflat_shape u0 (flat_shape u) = u" u])
  apply (auto simp only:
      unflat_shape.simps flat_shape.simps invar_shape.simps
      F.pred_map F.map_comp F.pred_True
      shape.case
      id_apply o_apply refl
    intro!: trans[OF F.map_cong_pred F.map_ident]
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas unflat_shape_flat_shape =
  mp[OF spec[OF unflat_shape_flat_shape_raw]]

lemma unflat_shapeV_flat_shapeV_raw:
  fixes u :: "'a V shapeV"
  shows
  "\<forall>u0. invar_shapeV u0 u \<longrightarrow> unflat_shapeV u0 (flat_shapeV u) = u"
sorry

lemmas unflat_shapeV_flat_shapeV =
  mp[OF spec[OF unflat_shapeV_flat_shapeV_raw]]

lemma invar_shape_flat_shape_raw:
  fixes u :: "'a F shape"
  shows
  "\<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape u0 u \<longrightarrow> invar_shape (snoc F u0) (flat_shape u)" u])
  apply (auto simp only:
      flat_shape.simps invar_shape.simps snoc.simps
      F.pred_map F.pred_True
      id_apply o_apply
    elim!: F.pred_mono_strong
    intro: F.pred_mono_strong[OF iffD2[OF fun_cong[OF F.pred_True] TrueI]]
    split: list.splits label.splits)
  done
*)
sorry

lemmas invar_shape_flat_shape =
  mp[OF spec[OF invar_shape_flat_shape_raw]]

lemma invar_shapeV_flat_shapeV_raw:
  fixes u :: "'a V shapeV"
  shows
  "\<forall>u0. invar_shapeV u0 u \<longrightarrow> invar_shapeV (snoc F u0) (flat_shapeV u)"
sorry

lemmas invar_shapeV_flat_shapeV =
  mp[OF spec[OF invar_shapeV_flat_shapeV_raw]]

lemma invar_flat_raw: "\<forall>u0. invar u0 x \<longrightarrow> invar (snoc F u0) (flat x)"
(*
  apply (induct x)
  apply (auto simp only:
      flat.simps invar_simps snoc.simps[symmetric] invar_shape_flat_shape id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done
*)
sorry

lemmas invar_flat = mp[OF spec[OF invar_flat_raw]]

lemma invar_shape_unflat_shape_raw:
  fixes u :: "'a shape"
  shows
  "\<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)"
(*
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0. invar_shape (snoc F u0) u \<longrightarrow> invar_shape u0 (unflat_shape u0 u)" u])
  apply (auto simp only:
      unflat_shape.simps invar_shape.simps snoc.simps snoc_neq_Nil
      F.pred_map id_apply o_apply refl
    elim!: F.pred_mono_strong
    split: list.splits label.splits)
  done
*)
sorry

lemmas invar_shape_unflat_shape =
  mp[OF spec[OF invar_shape_unflat_shape_raw]]

lemma invar_shapeV_unflat_shapeV_raw:
  fixes u :: "'a shapeV"
  shows
  "\<forall>u0. invar_shapeV (snoc F u0) u \<longrightarrow> invar_shapeV u0 (unflat_shapeV u0 u)"
sorry

lemmas invar_shapeV_unflat_shapeV =
  mp[OF spec[OF invar_shapeV_unflat_shapeV_raw]]

lemma invar_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> invar u0 (unflat u0 t)"
(*
  apply (induct t)
  apply (auto simp only:
      unflat.simps invar_simps snoc.simps invar_shape_unflat_shape id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done
*)
sorry

lemmas invar_unflat = mp[OF spec[OF invar_unflat_raw]]

lemma flat_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> flat (unflat u0 t) = t"
(*
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar_simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done
*)
sorry

lemmas flat_unflat = mp[OF spec[OF flat_unflat_raw]]

lemma unflat_flat_raw: "\<forall>u0. invar u0 t \<longrightarrow> unflat u0 (flat t) = t"
(*
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar_simps unflat_shape_flat_shape id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done
*)
sorry

lemmas unflat_flat = mp[OF spec[OF unflat_flat_raw]]


section \<open>Constructor is Bijection\<close>

lemma un_T_T: "un_T (T x) = x"
(*
  unfolding T_def un_T_def
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
    intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.case shape.case Rep_T_inverse o_apply
      G.map_comp G.map_ident Rep_T[unfolded mem_Collect_eq] unflat_flat
    intro!: trans[OF G.map_cong G.map_ident]) []
  done
*)
sorry

lemma T_un_T: "T (un_T x) = x"
(*
  unfolding T_def un_T_def G.map_comp o_def
  apply (rule iffD1[OF Rep_T_inject])
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (insert Rep_T[simplified, of x])
  apply (rule raw.exhaust[of "Rep_T x"])
  apply (auto simp only:
      raw.case shape.case invar_simps invar_shape_depth_iff
       snoc.simps(1)[of F, symmetric]
      G.pred_map Abs_T_inverse invar_unflat flat_unflat id_apply o_apply mem_Collect_eq
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong) []
  done
*)
sorry


section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

lemma flat_shape_map:
  "map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)"
  apply (rule shape.induct[of
    "\<lambda>u. map_shape f (flat_shape u) = flat_shape (map_shape (map_F f) u)" u])
  apply (auto simp only:
      shape.map flat_shape.simps F.map_comp o_apply
    intro!: F.map_cong0)
  done

lemma map_raw_flat: "map_raw f (flat t) = flat (map_raw (map_F f) t)"
  apply (induct t)
  apply (auto simp only:
      raw.map flat.simps G.map_comp flat_shape_map o_apply
    intro!: G.map_cong0)
  done

lemma map_T: "map_T f (T t) = T (map_G f (map_T (map_F f)) t)"
(*
  unfolding map_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.map shape.map G.map_comp o_apply mem_Collect_eq
     invar_map_closed Abs_T_inverse Rep_T[unfolded mem_Collect_eq] map_raw_flat
    intro!: arg_cong[of _ _ "\<lambda>x. Abs_T (raw.Cons x)"] G.map_cong0) []
  done
*)
sorry


subsection \<open>set\<close>

lemma flat_shape_set:
  fixes u :: "'a F shape"
  shows
  "set_shape (flat_shape u) = UNION (set_shape u) set_F"
  apply (rule shape.induct[of
    "\<lambda>u. set_shape (flat_shape u) = UNION (set_shape u) set_F" u])
  apply (auto simp only:
      flat_shape.simps shape.set F.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set_raw_flat:
  "set_raw (flat t) = UNION (set_raw t) set_F"
  apply (induct t)
  apply (auto simp only:
      flat.simps raw.set shape.set G.set_map flat_shape_set
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set_T: "set_T (T g) = set1_G g \<union>
  (\<Union>(set_F ` (\<Union>(set_T ` (set2_G g)))))"
  unfolding set_T_def T_def o_apply
(*
  apply -
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) []
  apply (auto simp only:
      raw.set shape.set G.set_map set_raw_flat o_apply
        UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong) []
  done
*)
sorry


subsection \<open>rel\<close>

lemma flat_shape_rel_raw:
  "(\<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
     rel_shape (rel_F R) u u')"
  apply (rule shape.induct[of
    "\<lambda>u. \<forall>u0 u'. invar_shape u0 u \<longrightarrow> invar_shape u0 u' \<longrightarrow> rel_shape R (flat_shape u) (flat_shape u') \<longrightarrow>
    rel_shape (rel_F R) u u'"
    u])
   apply (auto 0 4 simp only:
     invar_shape.simps flat_shape.simps shape.rel_inject
     invar_shape_depth_iff ball_simps id_apply
     F.rel_map pred_F_def F.set_map
     elim!: F.rel_mono_strong
     split: list.splits label.splits)
  done

lemma flat_shape_rel:
  "invar_shape u0 u \<Longrightarrow> invar_shape u0 u' \<Longrightarrow>
    rel_shape R (flat_shape u) (flat_shape u') = rel_shape (rel_F R) u u'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF flat_shape_rel_raw]]]]]; assumption)
  done

lemma rel_raw_flat_raw:
  "\<forall>t' u0. invar u0 t \<longrightarrow> invar u0 t' \<longrightarrow>
   rel_raw R (flat t) (flat t') \<longrightarrow> rel_raw (rel_F R) t t'"
(*
  apply (induct t)
  apply (rule allI)
  apply (case_tac t')
  apply (auto simp only:
      invar_simps flat.simps raw.rel_inject G.rel_map G.pred_set flat_shape_rel G.set_map ball_simps id_apply
    elim!: G.rel_mono_strong)
  done
*)
sorry

lemma rel_raw_flat:
  "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
   rel_raw R (flat t) (flat t') = rel_raw (rel_F R) t t'"
  apply (rule iffI[rotated, OF rel_funD[OF flat.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF rel_raw_flat_raw]]]]]; assumption)
  done

lemma rel_T: "rel_T R (T g) (T g') = rel_G R (rel_T (rel_F R)) g g'"
(*
  unfolding rel_T_def T_def vimage2p_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only:
       invar_simps invar_shape.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[unfolded mem_Collect_eq] invar_flat
     intro!: G.pred_mono_strong[OF iffD2[OF fun_cong[OF G.pred_True] TrueI]]) [2]
  apply (simp only:
    raw.rel_inject G.rel_map shape.rel_inject o_apply
    rel_raw_flat[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]])
  done
*)
sorry


section \<open>Induction\<close>

(* Preliminaries *)

(* The "recursion sz_ofs" of elements of 'a raw -- on the second argument of G *)
datatype sz = SCons (un_SCons:"(unit, sz) G")

primrec sz_of :: "'a raw \<Rightarrow> sz" where
  "sz_of (Cons g) = SCons (map_G (\<lambda>_. ()) sz_of g)"

lemma sz_of_unflat: 
  "sz_of (unflat ul r) = sz_of (r :: 'a raw)"
  apply (induct r arbitrary: ul)
  apply (auto intro: G.map_cong simp only: unflat.simps sz_of.simps G.map_comp o_apply)
  done

lemma sz_of_map_raw: 
  "sz_of (map_raw f r) = sz_of r"
  apply (induct r)
  apply (auto intro: G.map_cong simp only: raw.map sz_of.simps G.map_comp o_apply)
  done

lemma un_SCons_sz_of: "un_SCons (sz_of x) = map_G (\<lambda>_. ()) sz_of (un_Cons x)"
by (cases x) auto

ML \<open>
local
open Ctr_Sugar_Util
open BNF_FP_Rec_Sugar_Util
in

  fun mk_raw_induct_sz_tac ctxt IHs reduce_ctrm sz_induct induct_ctrms sz_cases =
    let
      val n = length (IHs |> map (tap (tracing o Thm.string_of_thm ctxt)));
    in
      HEADGOAL (EVERY' [rtac ctxt (infer_instantiate' ctxt [SOME reduce_ctrm] rev_mp),
        REPEAT_DETERM_N n o rtac ctxt allI,
        rtac ctxt (infer_instantiate' ctxt (map SOME induct_ctrms) sz_induct) THEN_ALL_NEW
        Subgoal.FOCUS (fn {context = ctxt, prems = raw_IHs', ...} =>
        let
          val IHs' = map (fn thm => thm RS spec RS mp) raw_IHs';
          val apply_IHs = resolve_tac ctxt IHs THEN'
            EVERY' (map (fn IH' =>
              rtac ctxt IH' THEN_ALL_NEW asm_full_simp_tac (ss_only sz_cases ctxt)) IHs');
          val intro = resolve_tac ctxt [allI, impI];
        in
          HEADGOAL (REPEAT_DETERM o FIRST' [intro, apply_IHs])
        end) ctxt,
        EVERY' [rtac ctxt impI, REPEAT_DETERM_N n o etac ctxt allE,
          CONJ_WRAP' (fn i => EVERY' [dtac ctxt (mk_conjunctN n i), dtac ctxt spec, etac ctxt mp,
            rtac ctxt refl]) (1 upto n)]])
    end;
end
\<close>

lemma raw_induct_sz_of: 
  assumes "\<And> r :: 'a raw. (\<And>r'. sz_of r' \<in> set2_G (case sz_of r of SCons x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
  shows "PP (r ::'a raw)"
  by (tactic \<open>mk_raw_induct_sz_tac @{context}
    @{thms assms}
    @{cterm "\<forall>sz r. sz_of r = sz \<longrightarrow> PP r "}
    @{thm sz.induct}
    [@{cterm "\<lambda>sz. \<forall>r. sz_of r = sz \<longrightarrow> PP r"}]
    @{thms sz.case}
  \<close>)


(*  THE ASSUMPTIONS: *)

consts P :: "'a T \<Rightarrow> bool"

axiomatization where 
P_ind: "\<And>t. (\<And> t'. t' \<in> set2_G t \<Longrightarrow> P t') \<Longrightarrow> P (T t :: 'a T)" and
P_param: "\<And>R :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> rel_fun (rel_T R) (=) P P"

lemma P_mono: "inj_on f (set_T a) \<Longrightarrow> P (map_T f a) \<Longrightarrow> P a"
  by (tactic \<open>let open Ctr_Sugar_Util
    fun mk_P_mono_tac ctxt P_param us T_rel_Grp =
      HEADGOAL (Method.insert_tac ctxt [infer_instantiate' ctxt (map SOME us) P_param]) THEN
      unfold_tac ctxt (T_rel_Grp :: @{thms rel_fun_def bi_unique_Grp}) THEN
      unfold_tac ctxt @{thms Grp_def} THEN
      HEADGOAL (EVERY' [asm_full_simp_tac (ss_only @{thms mem_Collect_eq True_implies_equals} ctxt),
        REPEAT_DETERM o dresolve_tac ctxt [spec, mp], rtac ctxt conjI, rtac ctxt refl,
        CONJ_WRAP' (fn _ => rtac ctxt subset_refl) us, etac ctxt iffD2, assume_tac ctxt])
  in mk_P_mono_tac @{context}
    @{thm P_param}
    [@{cterm "BNF_Def.Grp (set_T a) f"}]
    @{thm T.rel_Grp}
  end\<close>)

lemma P_raw: 
  fixes r :: "'a shape raw"
  shows "invar [] r \<longrightarrow> P (Abs_T r)"
sorry

lemma P_end_product: 
  fixes t :: "'a T"
  shows "P t"
sorry

section \<open>Recursion\<close>

(*normal datatype recursor
  (('b, 'a) G \<Rightarrow> 'a) \<Rightarrow>
  'b T \<Rightarrow> 'a
*)

(*generalized recursor
  (\<forall>'a. ('a D, 'b F R) G \<Rightarrow> 'b R) \<Rightarrow>
  (\<forall>'a. 'a D F \<Rightarrow> 'a F D) \<Rightarrow>
  (\<forall>'a. 'a D T \<Rightarrow> 'b R) 
*)


bnf_axiomatization 'b R
bnf_axiomatization 'a D


consts defobj :: "('a D, 'a V R) G \<Rightarrow> 'a R"
consts argobj :: "'a D F \<Rightarrow> 'a V D"

axiomatization where
  defobj_transfer: "\<And>A.
    bi_unique A \<Longrightarrow> 
    rel_fun (rel_G (rel_D A) (rel_R (rel_V A))) (rel_R A) defobj defobj" 
and
  argobj_transfer: "\<And>A.
    bi_unique A \<Longrightarrow>
    rel_fun (rel_F (rel_D A)) (rel_D (rel_V A)) argobj argobj"

(*
lemma defobj_invar: "pred_fun (pred_G (pred_D A) (pred_R (pred_V B))) (pred_R B) defobj"

  unfolding fun_pred_rel G.rel_eq_onp[symmetric] F.rel_eq_onp[symmetric]
    D.rel_eq_onp[symmetric] R.rel_eq_onp[symmetric]
  by (rule defobj_transfer; rule bi_unique_eq_onp)

lemma argobj_invar: "pred_fun (pred_F (pred_D A D E)) (pred_D (pred_F A) (pred_F D) (pred_F E)) argobj"
  unfolding fun_pred_rel F.rel_eq_onp[symmetric] D.rel_eq_onp[symmetric]
  by (rule argobj_transfer; rule bi_unique_eq_onp)
*)

(* variant of the above: *)
lemma defobj_pred: 
assumes "pred_G (pred_D A) (pred_R (pred_V A)) x"
shows "(pred_R A) (defobj x)"
sorry

lemma argobj_pred: 
assumes "pred_F (pred_D A) x" 
shows "(pred_D (pred_V A)) (argobj x)"
sorry

lemma defobj_natural:
assumes 
"inj_on a (\<Union>(set_D ` set1_G x) \<union> \<Union>(set_V ` \<Union>(set_R ` set2_G x)))"
shows "defobj (map_G (map_D a) (map_R (map_V a)) x) = map_R a (defobj x)"
using assms rel_funD[OF defobj_transfer, of 
    "BNF_Def.Grp (\<Union>(set_D ` set1_G x) \<union> \<Union>(set_V ` \<Union>(set_R ` set2_G x))) a" 
   ]
unfolding V.rel_Grp F.rel_Grp R.rel_Grp D.rel_Grp G.rel_Grp bi_unique_Grp
by (fastforce simp add: Grp_def)

lemma argobj_natural:
  "inj_on a (\<Union>(set_D ` set_F x)) \<Longrightarrow>
  argobj (map_F (map_D a) x) = map_D (map_V a) (argobj x)"
  using rel_funD[OF argobj_transfer, of "BNF_Def.Grp (\<Union>(set_D ` set_F x)) a"]
  unfolding F.rel_Grp V.rel_Grp R.rel_Grp D.rel_Grp bi_unique_Grp
  by (auto simp add: Grp_def)

lemma argobj_flat_shape_natural: 
fixes x :: "('a V shapeV) D F"
  shows
    "pred_F (pred_D (invar_shapeV u)) x \<Longrightarrow>
     argobj (map_F (map_D flat_shapeV) x) =
     map_D (map_V flat_shapeV) (argobj x)"
apply(rule argobj_natural)
unfolding pred_F_def pred_D_def inj_on_def  
  by simp (metis unflat_shapeV_flat_shapeV)

find_theorems flat_shapeV unflat_shapeV term flat_shapeV
find_theorems 

lemma defobj_flat_shape_natural: 
fixes x :: "(('a V shapeV) D, ('a V shapeV V) R) G"
shows
"pred_G (pred_D (invar_shapeV u)) 
        (pred_R (pred_V (invar_shapeV u))) x 
\<Longrightarrow>
defobj (map_G (map_D flat_shapeV) 
              (map_R (map_V flat_shapeV)) x) = 
map_R flat_shapeV (defobj x)"
apply(rule defobj_natural)
apply simp_all unfolding pred_G_def pred_R_def pred_D_def pred_V_def inj_on_def 
using unflat_shapeV_flat_shapeV 
by auto (smt unflat_shapeV_flat_shapeV)+
 
primrec f_shape :: "'a D shape \<Rightarrow> 'a shapeV D" where
  "f_shape (Leaf a) = map_D LeafV a"
| "f_shape (Node f) = map_D NodeV (argobj (map_F f_shape f))"

primrec f_raw :: "'a D raw \<Rightarrow> 'a shapeV R" where
  "f_raw (Cons x) = defobj (map_G f_shape (map_R un_NodeV o f_raw) x)"

definition f :: "'a D T \<Rightarrow> 'a R" where
  "f t = map_R un_LeafV (f_raw (Rep_T t))"
  
lemma f_def2: "f = map_R un_LeafV o f_raw o Rep_T"
unfolding f_def[abs_def] by auto

(* Important for compositional reasoning with predicators: *)
lemma F_pred_True: "pred_F (\<lambda>a. True) x"
unfolding pred_F_def by simp

lemma F_pred_map: "pred_F (\<lambda>a. PP (ff a)) x = pred_F PP (map_F ff x)"
unfolding pred_F_def by (simp add: F.set_map)

lemma F_pred_map_comp: "pred_F (PP o ff) x = pred_F PP (map_F ff x)"
using F_pred_map unfolding o_def .

(* Important for compositional reasoning with predicators: *)
lemma D_pred_True: "pred_D (\<lambda>a. True) x"
unfolding pred_D_def by simp

lemma D_pred_map: "pred_D (\<lambda>a. PP (ff a)) x = pred_D PP (map_D ff x)"
unfolding pred_D_def by (simp add: D.set_map)

lemma D_pred_map_comp: "pred_D (PP o ff) x = pred_D PP (map_D ff x)"
using D_pred_map unfolding o_def .

lemma invar_shape_f_shape:
assumes "invar_shape u0 u" shows "pred_D (invar_shapeV u0) (f_shape u)"
using assms apply (induct) 
using F_pred_map D_pred_map argobj_pred D_pred_True by fastforce+

lemma F_map_cong_pred: 
assumes "pred_F (\<lambda>a. ff a = gg a) x"
shows "map_F ff x = map_F gg x"
using assms unfolding pred_F_def by (intro F.map_cong) auto

find_theorems f_shape invar_shape

find_theorems un_NodeV invar_shapeV

lemma f_shape_flat_shape: 
assumes "invar_shape u x" 
shows "f_shape (flat_shape x) = map_D flat_shapeV (f_shape (map_shape argobj x))"
using assms proof induct 
  case Leaf
  thus ?case
  apply simp unfolding D.map_comp o_def apply simp
  unfolding o_def[symmetric] D.map_comp[symmetric]
  apply(rule D.map_cong)
  apply simp_all unfolding F.map_comp o_def apply simp
  apply(rule argobj_natural) 
unfolding inj_on_def by auto
next
  case (Node u0 f1)
  hence 1: 
    "map_F (\<lambda>a. f_shape (flat_shape a)) f1 = 
     map_F (map_D flat_shapeV o (\<lambda>a. f_shape (map_shape argobj a))) f1"
  by (intro F_map_cong_pred) simp
  show ?case apply simp
  unfolding D.map_comp o_def apply simp
  unfolding o_def[symmetric] D.map_comp[symmetric]
  apply(rule D.map_cong) apply simp_all
  unfolding F.map_comp o_def unfolding 1 unfolding F.map_comp[symmetric]
  apply(rule argobj_flat_shape_natural[of u0])
  using Node unfolding F_pred_map[symmetric]
  unfolding pred_F_def pred_D_def
  by simp (meson invar_shape_f_shape invar_shape_map_closed pred_D_def)
qed


(*
lemma f_shape_flat_shape[THEN spec, THEN mp]: "\<forall>u. invar_shape u x \<longrightarrow>
  f_shape (flat_shape x) = map_D flat_shapeV (f_shape (map_shape argobj x))"
  apply (rule shape.induct[of "\<lambda>x. \<forall>u. invar_shape u x \<longrightarrow> f_shape (flat_shape x) = map_D flat_shape flat_shape flat_shape (f_shape (map_shape argobj x))" x])
   apply (simp only: flat_shape.simps f_shape.simps shape.map D.map_comp F.map_comp argobj_natural inj_on_def shape.inject
     o_apply simp_thms Ball_def cong: F.map_cong D.map_cong)
  apply (rule allI)
  subgoal premises IH for f u0
    apply (rule impI)
    apply (unfold flat_shape.simps f_shape.simps shape.map D.map_comp o_def)
    apply (auto simp only: D.map_comp[unfolded o_def, symmetric]
        F.set_map F.map_comp F.pred_set invar_shape.simps list.sel o_apply
        invar_shape_map_closed invar_shape_f_shape
      intro!: arg_cong[of _ _ argobj] D.map_cong F.map_cong
         IH[THEN spec, THEN mp, of _ "tl u0"]
        trans[OF _ argobj_flat_shape_natural[of "tl u0"]]
      elim!: F.pred_mono_strong
      split: list.splits label.splits)
    done
  done
*)

(*
lemma invar_f_raw[THEN spec, THEN mp]:
  "\<forall>u0. invar u0 x \<longrightarrow> pred_R (invar_shapeV u0)  
         ((f_raw :: 'a V D raw \<Rightarrow> 'd V shapeV R) (map_raw argobj x))"
apply (rule raw.induct[of "\<lambda>x. \<forall>u0. invar u0 x \<longrightarrow> pred_R (invar_shape u0) (invar_shape u0) (f_raw (map_raw argobj x))" x])
  apply (rule allI)
  subgoal premises IH for g u0
    apply (auto simp only: invar_simps raw.map f_raw.simps G.map_comp o_apply G.pred_map R.pred_map
    invar_shape_map_closed invar_shape_depth_iff shape.case
    intro!: pred_funD[OF defobj_invar[of "invar_shape u0" "invar_shape u0" "invar_shape u0"]] invar_shape_f_shape
      R.pred_mono_strong[OF mp[OF spec[OF IH, of _ "F # u0"]]]
    elim!: G.pred_mono_strong
    cong: G.map_cong)
    done
  done
*)

term "argobj :: 'a D F \<Rightarrow> 'a V D"
term "defobj :: ('a D, 'a V R) G \<Rightarrow> 'a R"

(* both follouu by a simple argument on natural trans uu.r.t. injective functions *)
lemma set_argobj:
assumes "v \<in> set_D (argobj ff)" and "a \<in> set_V v"  
shows "\<exists> d \<in> set_F ff. a \<in> set_D d"
sorry

lemma set_defobj:
assumes "b \<in> set_R (defobj gg)"  
shows "\<exists> r v. r \<in> set2_G gg \<and> v \<in> set_R r \<and> b \<in> set_V v"
sorry

lemma invar_shapeV_set_V_un_NodeV: 
assumes "invar_shapeV (F # u0) ss" and "s \<in> set_V (un_NodeV ss)"
shows "invar_shapeV u0 s"
  using assms by (metis invar_shapeV_depth_iff(2) pred_V_def shapeV.simps(6))

term f_raw

lemma invar_f_raw:
assumes "invar u0 (x::'a D raw)"
shows "pred_R (invar_shapeV u0) (f_raw x :: 'a shapeV R)"
using assms proof induct
  case (1 u0 g)  
  let ?fr = "f_raw :: 'a D raw \<Rightarrow> 'a shapeV R"
  show ?case unfolding pred_R_def proof safe
    fix s :: "'a shapeV" assume "s \<in> set_R (f_raw (Cons g))" 
    hence s: "s \<in> set_R (defobj (map_G id (map_R un_NodeV) (map_G f_shape f_raw g)))"
    unfolding G.map_comp by auto
    def g1: g1 \<equiv> "map_G id (map_R un_NodeV) (map_G f_shape ?fr g)"
    obtain r v where r: "r \<in> set2_G g1" and v: "v \<in> set_R r" and s: "s \<in> set_V v"
    using s set_defobj unfolding g1[symmetric] by metis
    obtain rr where r: "r = map_R un_NodeV (?fr rr)" and rr: "rr \<in> set2_G g"
    using r unfolding g1 "G.set_map" by auto  
    then obtain ss where v: "v = un_NodeV ss" and ss: "ss \<in> set_R (f_raw rr)" using v unfolding r R.set_map by auto
    (* have "invar (F # u0) rr" using 1 rr unfolding pred_G_def pred_R_def by auto  *)
    have ss: "invar_shapeV (F # u0) ss" using 1 rr ss unfolding pred_G_def pred_R_def by auto  
    show "invar_shapeV u0 s" using invar_shapeV_set_V_un_NodeV ss s unfolding v by auto
  qed
qed

(* D, the proof of this uuas nouu split in tuuo: *)
lemma invar_f_raw_map_raw_argobj:
assumes "invar u0 x"
shows "pred_R (invar_shapeV u0) ((f_raw :: 'a V D raw \<Rightarrow> 'a V shapeV R) (map_raw argobj x))"
  by (simp add: assms invar_f_raw invar_map_closed_raw)

lemma un_NodeV_flat_shapeV_Nil:
assumes "invar_shapeV [] vv"
shows "un_NodeV (flat_shapeV vv) = map_V LeafV (un_LeafV vv)" 
  by (metis (no_types, lifting) assms flat_shapeV.simps(1) 
   invar_shapeV_depth_iff(1) shapeV.simps(5) shapeV.simps(6))


lemma un_NodeV_flat_shapeV:
assumes "invar_shapeV (F # u0) vv"
shows "un_NodeV (flat_shapeV vv) = map_V flat_shapeV (un_NodeV vv)" 
by (metis assms flat_shapeV.simps(2) invar_shapeV_depth_iff(2) shapeV.simps(6))
  

lemma f_raw_flat:
assumes "invar u0 (r::'a D F raw)" 
shows "(f_raw (flat r)::'a shapeV R) = map_R flat_shapeV (f_raw (map_raw argobj r))"
using assms proof induct 
  case (1 u0 g) 
  hence IH1: "\<forall>sh\<in>set1_G g. invar_shape u0 sh" and 
  IH2: "\<forall>r \<in>set2_G g. invar (F # u0) r \<and>
              (f_raw (flat r)::'a shapeV R) = map_R flat_shapeV (f_raw (map_raw argobj r))"  
    unfolding pred_G_def by auto
  (* Preparations: *)
  let ?x = "map_G (f_shape o map_shape argobj)
                  (map_R un_NodeV o f_raw o map_raw argobj) g :: 
            ('a V shapeV D, 'a V shapeV V R) G" 
  let ?y = "map_G (map_shape argobj) (map_raw argobj) g"
  (* *) 
  have x: "pred_G (pred_D (invar_shapeV u0))
                  (pred_R (pred_V (invar_shapeV u0))) ?x" 
    using IH1 IH2 unfolding pred_G_def pred_R_def pred_D_def pred_V_def 
    apply (auto simp: G.set_map R.set_map)
    apply (meson D.pred_set invar_shape_f_shape invar_shape_map_closed)
    by (metis R.pred_set invar_f_raw_map_raw_argobj invar_shapeV_set_V_un_NodeV)
  (* The diagram chase: *)
  have "(f_raw (flat (raw.Cons g))::'a shapeV R) =
         f_raw (Cons (map_G flat_shape flat g))"
    unfolding flat.simps ..
  also have "\<dots> = defobj (map_G (f_shape \<circ> flat_shape)
                                (map_R un_NodeV \<circ> f_raw \<circ> flat)
                                g)" 
    unfolding f_raw.simps G.map_comp ..
  also have "\<dots> = 
    defobj (map_G (map_D flat_shapeV o f_shape o map_shape argobj)
                  (map_R (un_NodeV \<circ> flat_shapeV) o f_raw o map_raw argobj)
                  g)" 
    apply(rule cong[of defobj], simp) apply(rule G.map_cong, simp)
    using IH1 IH2 f_shape_flat_shape by (auto simp: R.map_comp)
  also have "\<dots> = 
    defobj (map_G (map_D flat_shapeV o f_shape o map_shape argobj)
                  (map_R (map_V flat_shapeV o un_NodeV) o f_raw o map_raw argobj)
                  g)"
    apply(rule cong[of defobj], simp) apply(rule G.map_cong, simp)
    using un_NodeV_flat_shapeV IH1 IH2 
         invar_f_raw[unfolded pred_R_def] invar_map_closed_raw
    by (fastforce intro!: R.map_cong)+
  also have "\<dots> = map_R flat_shapeV (defobj ?x)"
    unfolding defobj_flat_shape_natural[OF x, symmetric]
    unfolding G.map_comp
    apply(rule cong[of defobj], simp) apply(rule G.map_cong, simp)
    by (auto simp: R.map_comp)
  also have "\<dots> = map_R flat_shapeV (f_raw (Cons ?y))" 
    unfolding f_raw.simps G.map_comp ..
  also have "\<dots> = map_R flat_shapeV (f_raw (map_raw argobj (Cons g)))"
  unfolding raw.simps ..
  finally show ?case .
qed


(*
lemma f_raw_flat:
assumes "invar [] (r::'a D F raw)" 
shows "(f_raw (flat r)::'a shapeV R) = map_R flat_shapeV (f_raw (map_raw argobj r))"

  using assms proof (induct r rule: raw.induct)  
  case (Cons g) 
  hence IH1: "\<forall>sh\<in>set1_G g. invar_shape [] sh" and 
  IH2: "\<forall>r \<in>set2_G g. invar [F] r \<and>
              (f_raw (flat r)::'a shapeV R) = map_R flat_shapeV (f_raw (map_raw argobj r))"  
    unfolding pred_G_def apply auto 
      apply (simp add: G.pred_set invar_simps)
     apply (simp add: G.pred_set invar_simps)
    sledgehammer
  (* Preparations: *)
  let ?x = "map_G (f_shape o map_shape argobj)
                  (map_R un_NodeV o f_raw o map_raw argobj) g :: 
            ('a V shapeV D, 'a V shapeV V R) G" 
  let ?y = "map_G (map_shape argobj) (map_raw argobj) g"
  (* *) 
  have x: "pred_G (pred_D (invar_shapeV u0))
                  (pred_R (pred_V (invar_shapeV u0))) ?x" 
    using IH1 IH2 unfolding pred_G_def pred_R_def pred_D_def pred_V_def 
    apply (auto simp: G.set_map R.set_map)
    apply (meson D.pred_set invar_shape_f_shape invar_shape_map_closed)
    by (metis R.pred_set invar_f_raw_map_raw_argobj invar_shapeV_set_V_un_NodeV)
  (* The diagram chase: *)
  have "(f_raw (flat (raw.Cons g))::'a shapeV R) =
         f_raw (Cons (map_G flat_shape flat g))"
    unfolding flat.simps ..
  also have "\<dots> = defobj (map_G (f_shape \<circ> flat_shape)
                                (map_R un_NodeV \<circ> f_raw \<circ> flat)
                                g)" 
    unfolding f_raw.simps G.map_comp ..
  also have "\<dots> = 
    defobj (map_G (map_D flat_shapeV o f_shape o map_shape argobj)
                  (map_R (un_NodeV \<circ> flat_shapeV) o f_raw o map_raw argobj)
                  g)" 
    apply(rule cong[of defobj], simp) apply(rule G.map_cong, simp)
    using IH1 IH2 f_shape_flat_shape by (auto simp: R.map_comp)
  also have "\<dots> = 
    defobj (map_G (map_D flat_shapeV o f_shape o map_shape argobj)
                  (map_R (map_V flat_shapeV o un_NodeV) o f_raw o map_raw argobj)
                  g)"
    apply(rule cong[of defobj], simp) apply(rule G.map_cong, simp)
    using un_NodeV_flat_shapeV IH1 IH2 
         invar_f_raw[unfolded pred_R_def] invar_map_closed_raw
    by (fastforce intro!: R.map_cong)+
  also have "\<dots> = map_R flat_shapeV (defobj ?x)"
    unfolding defobj_flat_shape_natural[OF x, symmetric]
    unfolding G.map_comp
    apply(rule cong[of defobj], simp) apply(rule G.map_cong, simp)
    by (auto simp: R.map_comp)
  also have "\<dots> = map_R flat_shapeV (f_raw (Cons ?y))" 
    unfolding f_raw.simps G.map_comp ..
  also have "\<dots> = map_R flat_shapeV (f_raw (map_raw argobj (Cons g)))"
  unfolding raw.simps ..
  finally show ?case .
qed
*)
sorry (* will need depth induct *)
    
lemma argobj_Rep_T:
"argobj (map_F (map_D Rep_T) t) = map_D (map_V Rep_T) (argobj t)"
  by (simp add: argobj_natural Rep_T_inject inj_on_def)

lemma argobj_Rep_T_Abs_T:
"argobj (map_F (map_D Rep_T) t) = map_D (map_V Rep_T) (argobj t)"
  by (simp add: argobj_natural Rep_T_inject inj_on_def)

lemma map_raw_Rep_T: "map_raw ff (Rep_T t) = Rep_T (map_T ff t)"
  by (simp only: map_T_def invar_map_closed_raw
    Abs_T_inverse[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq] o_apply)

lemma map_raw_o_Rep_T: "map_raw ff o Rep_T = Rep_T o map_T ff"
by (auto simp: map_raw_Rep_T)

lemma f_shape_o_Leaf: "f_shape o Leaf = map_D LeafV"
  by auto

lemma f_shape_o_Noce: "f_shape o Node = map_D NodeV o argobj o map_F f_shape"
  by auto

lemma map_V_cong_id: 
assumes "\<And> a. a \<in> set_V vv \<Longrightarrow> ff a = a"
shows "map_V ff vv = vv"
by (metis V.map_cong0 V.map_id0 assms id_apply)

theorem "f (T g) = defobj (map_G id (f o map_T argobj) g)"
  (is "?A = ?B")
proof-
  (* Preparations: *)
  have 1: "invar [] (Cons (map_G Leaf (flat \<circ> Rep_T) g))"
    unfolding invar_simps pred_G_def using Rep_T invar_flat_raw
    by (fastforce simp: G.set_map Rep_T invar_flat_raw)
  (*  *)
  have "?A = map_R un_LeafV (f_raw (Rep_T (T g)))"
    unfolding f_def ..
  also have "\<dots> = 
  map_R un_LeafV (f_raw ((Cons (map_G Leaf (flat \<circ> Rep_T) g))))"
    unfolding T_def using T.Abs_T_inverse[simplified, OF 1] by simp
  also have "\<dots> = 
  map_R un_LeafV (defobj
     (map_G (f_shape \<circ> Leaf)
            (map_R un_NodeV \<circ> f_raw \<circ> (flat \<circ> Rep_T)) 
            g))" 
    unfolding f_raw.simps G.map_comp ..
  also have "\<dots> = 
  map_R un_LeafV (defobj
     (map_G (f_shape \<circ> Leaf)
            (map_R (un_NodeV \<circ> flat_shapeV) o f_raw o map_raw argobj \<circ> Rep_T)
            g))" 
    apply(rule cong[of "map_R un_LeafV"], simp)  
    apply(rule cong[of defobj], simp) 
    apply(rule G.map_cong, simp) apply auto
    unfolding R.map_comp[symmetric]
    using Rep_T by (auto intro!: R.map_cong f_raw_flat) 
  also have "\<dots> = 
  map_R un_LeafV (defobj
     (map_G (f_shape \<circ> Leaf)
            (map_R (un_NodeV \<circ> flat_shapeV) \<circ> f_raw \<circ> Rep_T \<circ> map_T argobj)
            g))" 
    unfolding o_assoc[symmetric] unfolding map_raw_o_Rep_T unfolding o_assoc ..  
  also have "\<dots> = 
  map_R un_LeafV (defobj
     (map_G (f_shape \<circ> Leaf)
            (map_R (map_V LeafV o un_LeafV) \<circ> f_raw \<circ> Rep_T \<circ> map_T argobj)
            g))"
    apply(rule cong[of "map_R un_LeafV"], simp)  
    apply(rule cong[of defobj], simp) 
    apply(rule G.map_cong, simp)
    apply (auto intro!: R.map_cong un_NodeV_flat_shapeV_Nil) 
    by (metis R.pred_set Rep_T invar_f_raw mem_Collect_eq)
  also have "\<dots> = 
  defobj (map_G (map_D un_LeafV) (map_R (map_V un_LeafV))
     (map_G (f_shape \<circ> Leaf)
            (map_R (map_V LeafV o un_LeafV) \<circ> f_raw \<circ> Rep_T \<circ> map_T argobj)
            g))" 
    apply(rule defobj_natural[symmetric]) 
    unfolding inj_on_def by (auto simp: G.set_map D.set_map R.set_map V.set_map)
  also have "\<dots> = defobj (map_G id (f \<circ> map_T argobj) g)" 
    unfolding G.map_comp o_assoc R.axiom2_R[symmetric] V.axiom2_V[symmetric] 
    unfolding f_def2 
    unfolding o_assoc[symmetric] unfolding f_shape_o_Leaf unfolding o_assoc
    unfolding D.axiom2_D[symmetric]
    apply(rule cong[of defobj], simp) 
    apply(rule G.map_cong, simp)
     apply(subst D.axiom1_D[symmetric]) apply(rule D.map_cong, simp) apply auto[]
     (* *)
    apply simp apply(rule R.map_cong, simp) 
    apply simp apply(rule map_V_cong_id) by auto
  finally show ?thesis .
qed
 


hide_const (open) f

end