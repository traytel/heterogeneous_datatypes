theory GeneralNonuniform imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

declare [[bnf_internals, typedef_overloaded]]

bnf_axiomatization ('a,'b,'c) F   
bnf_axiomatization ('a,'b) G
bnf_axiomatization 'a H



(* Want: 
typedecl 'a I
consts dtor :: "'a I \<Rightarrow> ('a I, 'a, ('a I, 'a I H I) G) F"

An arbitrary expression consisting of BNFs, including I, is allowed on the right. 
Guardedness (i.e., non-I at the top) is not required, but in that case no witness 
can be produced. 
*)

(* Raw type iterates all possible combinations of te involved types constructors: *)
datatype 'a raw = 
  Leaf 'a | 
  Fnode "('a raw, 'a raw, 'a raw) F" | 
  Gnode "('a raw, 'a raw) G" | 
  Hnode "'a raw H"  

(* Syntactic representation of the type expressions involved in the definition:  *)
datatype exp = var | i exp | f exp exp exp | g exp exp | h exp

(* The following matches the type expression in the datatype declaration: *)
fun unfold_i :: "exp \<Rightarrow> exp" where 
"unfold_i exp = f (i exp) 
                  (g (i exp) (i (h (i exp))))
                  (i exp)"

lemma [mono]: "P1 \<le> Q1 \<Longrightarrow> P2 \<le> Q2 \<Longrightarrow> P3 \<le> Q3 \<Longrightarrow> pred_F P1 P2 P3 \<le> pred_F Q1 Q2 Q3"
  using F.pred_mono_strong[of P1 P2 P3 _ Q1 Q2 Q3] unfolding le_fun_def le_bool_def by blast
lemma [mono]: "P1 \<le> Q1 \<Longrightarrow> P2 \<le> Q2 \<Longrightarrow> pred_G P1 P2 \<le> pred_G Q1 Q2"
  using G.pred_mono_strong[of P1 P2 _ Q1 Q2] unfolding le_fun_def le_bool_def by blast
lemma [mono]: "P1 \<le> Q1 \<Longrightarrow> pred_H P1 \<le> pred_H Q1"
  using H.pred_mono_strong[of P1 _ Q1] unfolding le_fun_def le_bool_def by blast

inductive good :: "exp \<Rightarrow> 'a raw \<Rightarrow> bool" where 
  var: "good var (Leaf a)"
| i: "good (unfold_i exp) x \<Longrightarrow> good (i exp) x"
| f: "pred_F (good e1) (good e2) (good e3) x \<Longrightarrow> good (f e1 e2 e3) (Fnode x)"
| g: "pred_G (good e1) (good e2) x \<Longrightarrow> good (g e1 e2) (Gnode x)"
| h: "pred_H (good e1) x \<Longrightarrow> good (h e1) (Hnode x)"

lemma good_map1: "good e (map_raw gg x) \<Longrightarrow> good e x"
  apply (induct "map_raw gg x" arbitrary: x rule: good.induct)
  apply (auto simp only: intro: good.intros)
  apply (case_tac x; auto simp only: raw.map intro: good.intros)
  apply (case_tac xa; auto simp only: F.pred_map raw.map o_apply elim!: F.pred_mono_strong intro!: good.intros)
  apply (case_tac xa; auto simp only: G.pred_map raw.map o_apply elim!: G.pred_mono_strong intro!: good.intros)
  apply (case_tac xa; auto simp only: H.pred_map raw.map o_apply elim!: H.pred_mono_strong intro!: good.intros)
  done

lemma good_map2: "good e x \<Longrightarrow> good e (map_raw gg x)"
  apply (induct x rule: good.induct)
  apply (auto simp only: F.pred_map G.pred_map H.pred_map raw.map o_apply
    elim!: F.pred_mono_strong G.pred_mono_strong H.pred_mono_strong
    intro!: good.intros)
  done

lemma good_map_closed: "good e (map_raw gg x) = good e x"
  using good_map1 good_map2 by blast

typedef 'a I = "{x :: 'a raw. good (i var) x}"
sorry (* need witnesses here *)

lift_bnf 'a I
  by (simp_all only: good_map_closed mem_Collect_eq)

definition T :: "('a I, 'a, ('a I, 'a I H I) G) F \<Rightarrow> 'a I" where
  "T x = Abs_I (ggg (map_F Rep_I id (map_G Rep_I (map_raw (map_H Rep_I) o Rep_I)) x))"


 

end