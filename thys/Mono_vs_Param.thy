theory Mono_vs_Param
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

lemma bi_unique_alt:
  "bi_unique R \<longleftrightarrow> (\<exists>f. inj_on f (Collect (Domainp R)) \<and> R = BNF_Def.Grp (Collect (Domainp R)) f)"
  unfolding bi_unique_def inj_on_def Grp_def
  by (auto simp: fun_eq_iff the_equality intro!: exI[of _ "\<lambda>x. The (\<lambda>y. R x y)"])

bnf_axiomatization 'a T

consts P :: "'a T \<Rightarrow> bool"

lemma
  fixes f :: "'a \<Rightarrow> 'b"
  assumes param: "\<And>R :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> rel_fun (rel_T R) (=) P P"
  shows "inj_on f (set_T a) \<Longrightarrow> P (map_T f a) \<Longrightarrow> P a"
using param[of "BNF_Def.Grp (set_T a) f"]
unfolding T.rel_Grp rel_fun_def
unfolding Grp_def bi_unique_def inj_on_def by auto

axiomatization where
  P_mono: "\<And>(f :: 'a \<Rightarrow> 'b) a. inj_on f (set_T a) \<Longrightarrow> P (map_T f a) \<Longrightarrow> P a"

lemma P_antimono: "inj_on f (set_T a) \<Longrightarrow> P a \<Longrightarrow> P (map_T f a)"
  apply (rule P_mono[where f = "the_inv_into (set_T a) f"])
  apply (auto simp: T.map_comp T.map_ident the_inv_into_f_f cong: T.map_cong)
  apply (auto simp: inj_on_def T.set_map the_inv_into_f_f)
  done

lemma
  fixes R :: "'a \<Rightarrow> 'b \<Rightarrow> bool"
  shows "bi_unique R \<Longrightarrow> rel_fun (rel_T R) (=) P P"
  unfolding bi_unique_alt
  apply safe
  apply (rule ssubst[of R])
  apply assumption
  unfolding T.rel_Grp rel_fun_def
  unfolding Grp_def inj_on_def apply auto
  apply (rule P_antimono; force simp: inj_on_def)
  apply (rule P_mono; force simp: inj_on_def)
  done

end