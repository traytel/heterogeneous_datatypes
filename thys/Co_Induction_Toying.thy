theory Co_Induction_Toying
  imports Main
begin

declare [[bnf_internals]]

datatype 'a plist = PNil | PCons 'a "'a plist"

consts P :: "'a plist \<Rightarrow> bool"

thm plist.ctor_induct

lemma P_ctor: "(\<And>z. z \<in> set2_pre_plist x \<Longrightarrow> P z) \<Longrightarrow> P (ctor_plist x)"
  sorry

lemma P_ind: "pred_pre_plist (\<lambda>_. True) P \<le> P o plist.ctor_plist"
  apply (rule predicate1I)
  apply (unfold pre_plist.pred_set Ball_def simp_thms o_apply)
  apply (simp add: P_ctor)
  done 


method_setup def_Q = \<open>
  fn (context, toks) =>
  let
(*
    val context' = context
      |> Context.map_proof (
        Local_Theory.define ((@{binding Q}, NoSyn), ((Thm.def_binding @{binding Q}, []), @{term undefined}))
        #> snd)
*)
    val context' = context
      |> Context.map_theory (
        Global_Theory.add_defs true [((@{binding Q}, @{term undefined}), [])]
        #> snd)
  in
    (fn thms => Method.succeed, (context', toks))
  end
\<close> "define a polymorphic constant called Q"


lemma "hd xs = hd (id xs)"
  apply def_Q


  apply simp
  done


