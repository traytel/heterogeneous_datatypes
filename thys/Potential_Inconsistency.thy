theory Potential_Inconsistency
imports "../BNF_Nonuniform_Fixpoint"
begin

nonuniform_datatype 'a olist =
  ONil 'a
| OCons "('a + 'a) olist"

definition P :: "'a olist \<Rightarrow> bool" where
  "P xs = (\<exists>x. set_olist xs = {x})"

nonuniform_induct xs in
  "P xs"
  subgoal sorry
proof -
  fix x :: 'a
  show "P (ONil x)" unfolding P_def apply auto sorry
next
  fix xs :: "('a + 'a) olist"
  assume "P xs"
  then show "P (OCons xs)"
    unfolding P_def apply (auto simp: setl.simps setr.simps) sorry
qed

end
