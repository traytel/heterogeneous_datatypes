theory FingerTree
imports "../BNF_Nonuniform_Fixpoint"
begin
declare [[ML_print_depth = 10000]]

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

datatype 'a Node = Node2 'a 'a | Node3 'a 'a 'a
datatype 'a Digit = One 'a | Two 'a 'a | Three 'a 'a 'a | Four 'a 'a 'a 'a
datatype ('a, 'g) FingerTreeG = Empty | Single 'a | Depth "'a Digit" 'g "'a Digit"

fun toDigit :: "'a Node \<Rightarrow> 'a Digit" where
  "toDigit (Node2 a b) = Two a b"
| "toDigit (Node3 a b c) = Three a b c"

fun cons :: "'a \<Rightarrow> 'a Digit \<Rightarrow> 'a Digit" where
  "cons a (One b) = Two a b"
| "cons a (Two b c) = Three a b c"
| "cons a (Three b c d) = Four a b c d"
| "cons _ _ = undefined"

fun head :: "'a Digit \<Rightarrow> 'a" where
  "head (One a) = a"
| "head (Two a _) = a"
| "head (Three a _ _) = a"
| "head (Four a _ _ _) = a"

fun tail :: "'a Digit \<Rightarrow> 'a Digit" where
  "tail (One a) = undefined"
| "tail (Two _ a) = One a"
| "tail (Three _ a b) = Two a b"
| "tail (Four _ a b c) = Three a b c"

fun split :: "'a Digit \<Rightarrow> ('a Digit * 'a Digit)" where
  "split (One a) = undefined"
| "split (Two a b) = (One a, One b)"
| "split (Three a b c) = (Two a b, One c)"
| "split (Four a b c d) = (Two a b, Two c d)"

fun toList :: "'a Digit \<Rightarrow> 'a list" where
  "toList (One a) = [a]"
| "toList (Two a b) = [a, b]"
| "toList (Three a b c) = [a, b, c]"
| "toList (Four a b c d) = [a, b, c, d]"

fun nodify' :: "'a list \<Rightarrow> 'a Node Digit" where
  "nodify' [a, b] = One (Node2 a b)"
| "nodify' [a, b, c] = One (Node3 a b c)"
| "nodify' [a, b, c, d] = Two (Node2 a b) (Node2 a b)"
| "nodify' (a#b#c#ds) = cons (Node3 a b c) (nodify' ds)"
| "nodify' _ = undefined"

fun nodify :: "'a Digit \<Rightarrow> 'a Digit \<Rightarrow> 'a Digit \<Rightarrow> 'a Node Digit" where
  "nodify a b c = nodify' ((toList a) @ (toList b) @ (toList c))"

fun reduce_sum :: "nat Node \<Rightarrow> nat" where
  "reduce_sum (Node2 a b) = a + b"
| "reduce_sum (Node3 a b c) = a + b + c"

fun sum_digit :: "nat Digit \<Rightarrow> nat" where
  "sum_digit (One a) = a"
| "sum_digit (Two a b) = a + b"
| "sum_digit (Three a b c) = a + b + c"
| "sum_digit (Four a b c d) = a + b + c + d"

fun fold_sum :: "(nat, nat) FingerTreeG \<Rightarrow> nat" where
  "fold_sum (Empty) = 0"
| "fold_sum (Single a) = a"
| "fold_sum (Depth as t bs) = sum_digit as + t + sum_digit bs"


local_setup \<open>
fn lthy => 
  let
    open BNF_Def
    open BNF_Util
    open BNF_Tactics
    open BNF_Comp
    open BNF_FP_Util
    open BNF_FP_Def_Sugar
    open BNF_LFP
    open BNF_LFP_Util
    open BNF_LFP_Tactics
    open BNF_LFP_Rec_Sugar
    open Ctr_Sugar
    open BNF_Tactics
    open BNF_NU_FP
    open BNF_NU_LFP_Rec

    (* Isabelle to ML *)

    val ([a, x], _) = mk_TFrees 2 lthy;

    val Node_sugar = the (fp_sugar_of lthy @{type_name Node}) : fp_sugar;
    val Node = Type (@{type_name Node}, [a]);
    val Node_Bnf = #fp_bnf Node_sugar;
    val Digit_sugar = the (fp_sugar_of lthy @{type_name Digit});
    val Digit = Type (@{type_name Digit}, [a]);
    val FingerTreeG_sugar = the (fp_sugar_of lthy @{type_name FingerTreeG});
    val FingerTreeG_bnf = #fp_bnf FingerTreeG_sugar;

    val toDigit = Const (@{const_name toDigit}, Node --> Digit);
    val cons = Const (@{const_name cons}, a --> Digit --> Digit);
    val head = Const (@{const_name head}, Digit --> a);
    val tail = Const (@{const_name tail}, Digit --> Digit);
    val split = Const (@{const_name split}, Digit --> HOLogic.mk_prodT (Digit, Digit));

    (* Type definition *)

    val Fss = [[([], Node_Bnf)]];
    val specs = [(([], FingerTreeG_bnf), Fss)];
    val mixfix = [NoSyn];
    val bs = [Binding.name "FingerTree"];
    val map_bs = replicate 1 Binding.empty;
    val rel_bs = replicate 1 Binding.empty;
    val pred_bs = replicate 1 Binding.empty;
    val set_bss = replicate 1 (replicate 1 Binding.empty);
    val resBs = map dest_TFree [a];

    val (nu_fp_res, lthy) = construct_nu_fp mixfix map_bs rel_bs pred_bs set_bss bs resBs [] specs [] lthy;

    (* Functions *)

    (* sum *)

    val f_names = ["sum"];
    val f_bs = map Binding.name f_names;
    val Ran_bnf = DEADID_bnf;
    val Dom_bnf = DEADID_bnf;
    val fld = Const (@{const_name fold_sum}, mk_T_of_bnf [] [HOLogic.natT, HOLogic.natT] FingerTreeG_bnf --> HOLogic.natT);
    val rdc = Const (@{const_name reduce_sum}, mk_T_of_bnf [] [HOLogic.natT] Node_Bnf --> HOLogic.natT);

    val fld_transfer = refl;
    val rdc_transfer = refl;
    val (_, lthy) = construct_nu_rec mixfix f_bs nu_fp_res
      [Dom_bnf] [[HOLogic.natT]] [[]]
      [Ran_bnf] [[HOLogic.natT]] []
      [((fld, fld_transfer), [[(rdc, rdc_transfer)]])] lthy;
  in
    lthy
  end
\<close>


term T_ctr
term f_T
term map_FingerTree

term "T_ctr (Depth (One 1) (T_ctr (Single (Node3 2 3 4))) (Two 2 2))"
term "f_T (T_ctr (Depth (One 1) (T_ctr (Single (Node3 2 3 4))) (Two 2 2)))"

definition "t = f_T (T_ctr (Depth (One 1) (T_ctr (Single (Node3 2 3 4))) (Two 2 2)))"


local_setup \<open>
fn lthy => let
  val t = Const (@{const_name t}, HOLogic.natT)
    |> Syntax.string_of_term lthy |> writeln
in lthy end
\<close>

function push :: "'a \<Rightarrow> 'a FingerTree \<Rightarrow> 'a FingerTree" where
  "push a (T_ctr Empty) = T_ctr (Single a)"
| "push a (T_ctr (Single b)) = T_ctr (Depth (One a) (T_ctr Empty) (One b))"
| "push a (T_ctr (Depth (Four b c d e) t f)) = T_ctr (Depth (Two a b) (push (Node3 c d e) t) f)"
| "push a (T_ctr (Depth b t c)) = T_ctr (Depth (cons a b) t c)"

end






