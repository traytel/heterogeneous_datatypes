theory Concrete_Co_Induction
imports "../BNF_Nonuniform_Fixpoint"
begin

section \<open>Induction\<close>

declare [[bnf_internals]]

nonuniform_datatype 'a plist = PNil | PCons 'a "('a \<times> 'a) plist"

nonuniform_induct xs in
  map_plist_id[symmetric]: "map_plist id xs = xs"
  sorry


thm map_plist_id


subsection \<open>Prove @{term "map_plist id xs = xs"} by nonuniform induction on xs\<close>
  
definition P where
  "P xs = (map_plist id xs = xs)"


subsection \<open>3 Polymorphic facts to be proved by the user\<close>

theorem P_parametric:
  assumes [transfer_rule]: "bi_unique R" "left_total R"
  shows "rel_fun (rel_plist R) (\<longleftarrow>) P P"
  unfolding P_def apply transfer_prover_start
    apply transfer_step
    apply transfer_step
   defer
   apply (rule refl)
  apply (auto simp: rel_fun_def Rel_def)
    by (meson assms(1) bi_uniqueDl plist.bi_unique_rel)

theorem P_PNil: "P PNil"
  unfolding P_def by simp

theorem P_PCons:
  fixes x xs
  assumes "P xs"
  shows "P (PCons x xs)"
  using assms unfolding P_def by (simp add: prod.map_id0)


subsection \<open>Rest derived automatically\<close>

(*something similar is derived in the call to the ML code below; reuse!*)
lemma P_inj_mono: "inj f \<Longrightarrow> P (map_plist f t) \<longrightarrow> P t"
  using P_parametric[of "BNF_Def.Grp UNIV f"] unfolding bi_unique_Grp left_total_Grp plist.rel_Grp
  by (auto simp: Grp_def rel_fun_def)

lemma P_ind: "pred_pre_plist (\<lambda>_. True) P \<le> P o plist.Ctor_plist"
  apply (rule predicate1I)
  apply (unfold pre_plist.pred_set Ball_def simp_thms o_apply)
  subgoal for t
    apply (tactic \<open>resolve_tac @{context}
      [BNF_NU_FP_Def_Sugar.nu_fp_sugar_of @{context} @{type_name plist} |> the
      |> #combined_absT_info |> #type_definition RS @{thm type_definition.Abs_cases}
      |> infer_instantiate' @{context} [NONE, NONE, NONE, SOME @{cterm t}]] 1\<close>)
    subgoal for y
      apply (cases y)
       apply (simp add: P_PNil[unfolded PNil_def o_apply])
      apply (auto simp add: Abs_plist_pre_plist_inverse)
      apply (rule P_PCons[unfolded PCons_def o_apply])
      apply (auto simp: map_pre_plist_def set2_pre_plist_def
        Abs_plist_pre_plist_inverse inj_on_def
        arg.Abs_arg_plist_0_0_pre_plist_0_0_inject dest: mp[OF P_inj_mono, rotated])
      done
    done
  done

lemma type_def_plist_pre_plist: "type_definition Rep_plist_pre_plist Abs_plist_pre_plist UNIV"
  sorry

lemma P_Ctor0: "(\<And>z. z \<in> set2_pre_plist x \<Longrightarrow> P z) \<Longrightarrow>
  P (plist.Ctor_plist (map_pre_plist id (map_plist arg.Abs_arg_plist_0_0_pre_plist_0_0) x))"
  apply (insert P_PNil P_PCons)
  apply (rotate_tac -2)
  apply (unfold PNil_def PCons_def)
  apply (tactic \<open>HEADGOAL (resolve_tac @{context}
    [BNF_FP_Util.mk_absumprodE @{thm type_def_plist_pre_plist} [0, 2]])\<close>)
  apply (drule DEADID.rel_mono_strong[of x])
   apply hypsubst_thin
   apply (unfold comp_apply)
   apply assumption
  apply hypsubst_thin
  apply (simp add: map_pre_plist_def Abs_plist_pre_plist_inverse set2_pre_plist_def)
  apply (drule meta_spec)
  apply (rotate_tac -1)
  apply (drule meta_spec)
  apply (erule meta_mp)
  apply (drule meta_spec)
  apply (erule meta_mp)
  apply (rule refl)
  done

lemma P_Ctor: "(\<And>z. z \<in> set2_pre_plist x \<Longrightarrow> P z) \<Longrightarrow> P (plist.Ctor_plist x)"
  apply (rule P_Ctor0[of "map_pre_plist id (map_plist arg.Rep_arg_plist_0_0_pre_plist_0_0) xs" for xs,
    unfolded pre_plist.map_comp plist.map_comp id_def o_def pre_plist.map_ident plist.map_ident
      arg.Rep_arg_plist_0_0_pre_plist_0_0_inverse])
  apply (rule P_inj_mono[THEN mp, of arg.Abs_arg_plist_0_0_pre_plist_0_0])
   apply (simp add: inj_on_def arg.Abs_arg_plist_0_0_pre_plist_0_0_inject)
  apply (drule meta_spec)
  apply (erule meta_mp)
  apply (simp only: pre_plist.set_map)
  apply (subst inj_image_mem_iff[of "map_plist arg.Rep_arg_plist_0_0_pre_plist_0_0", symmetric])
   apply (rule plist.inj_map)
   apply (simp add: plist.inj_map inj_on_def arg.Rep_arg_plist_0_0_pre_plist_0_0_inject)
  apply (simp only: plist.map_comp o_def arg.Abs_arg_plist_0_0_pre_plist_0_0_inverse[simplified]
      plist.map_ident)
  done

lemma P_ind': "pred_pre_plist (\<lambda>_. True) P \<le> P o plist.Ctor_plist"
  apply (rule predicate1I)
  apply (unfold pre_plist.pred_set Ball_def simp_thms o_apply)
  subgoal for t
    apply (tactic \<open>resolve_tac @{context}
      [BNF_NU_FP_Def_Sugar.nu_fp_sugar_of @{context} @{type_name plist} |> the
      |> #combined_absT_info |> #type_definition RS @{thm type_definition.Abs_cases}
      |> infer_instantiate' @{context} [NONE, NONE, NONE, SOME @{cterm t}]] 1\<close>)
    apply (rule P_Ctor)
    apply fast
    done
  done

local_setup \<open> fn lthy =>
  let
  val (res, lthy) = BNF_NU_FP_Ind.construct_nu_co_ind BNF_Util.Least_FP
      (BNF_NU_FP_Def_Sugar.nu_fp_sugar_of lthy @{type_name plist} |> the |> #nu_fp_res) []
      [@{term P}] @{thms P_parametric} (K @{thms P_ind}) lthy
  in
    lthy
    |> Local_Theory.note ((@{binding result}, []), #co_ind_thms res) |> snd
  end\<close>

theorem "map_plist id xs = xs"
  by (rule result[unfolded P_def])


section \<open>Coinduction\<close>


nonuniform_codatatype 'a pllist = PLNil | PLCons (plhd: 'a) (pltl: "('a \<times> 'a) pllist")

nonuniform_coinduct "\<lambda>l r. \<exists>xs. l = map_pllist id xs \<and> r = xs" in
  map_pllist_id[symmetric]: "map_pllist id xs = xs"
  sorry

subsection \<open>Prove @{term "map_pllist id xs = xs"} by nonuniform coinduction\<close>
  
definition Q where
  "Q l r = (\<exists>xs. l = map_pllist id xs \<and> r = xs)"


subsection \<open>3 Polymorphic facts to be proved by the user\<close>

theorem Q_parametric:
  assumes [transfer_rule]: "bi_unique R" "left_total R"
  shows "rel_fun (rel_pllist R) (rel_fun (rel_pllist R)(\<longrightarrow>)) Q Q"
  unfolding Q_def apply simp
  apply transfer_prover_start
     apply transfer_step+
   defer
   apply (rule refl)
  apply (auto simp: Rel_def rel_fun_def)
    by (meson assms(1) bi_uniqueDr pllist.bi_unique_rel)

theorem Q_ctr_bisim:
  shows "Q r s \<Longrightarrow> (r = PLNil \<longleftrightarrow> s = PLNil) \<and>
    (r \<noteq> PLNil \<longrightarrow> s \<noteq> PLNil \<longrightarrow> (plhd r = plhd s \<and> Q (pltl r) (pltl s)))"
  unfolding Q_def by (auto simp: pllist.map_sel prod.map_id0)


subsection \<open>Rest derived automatically\<close>

(*something similar is derived in the call to the ML code below; reuse!*)
lemma Q_inj_mono: "inj f \<Longrightarrow>
  Q t u \<longrightarrow> Q (map_pllist f t) (map_pllist f u)"
  using Q_parametric[of "BNF_Def.Grp UNIV f"] unfolding bi_unique_Grp left_total_Grp pllist.rel_Grp
  by (auto simp: Grp_def rel_fun_def)

ML {*
open BNF_Util
open BNF_Tactics
open BNF_FP_Def_Sugar_Tactics
*}

ML {*
fun hhf_concl_conv cv ctxt ct =
  (case Thm.term_of ct of
    Const (@{const_name Pure.all}, _) $ Abs _ =>
    Conv.arg_conv (Conv.abs_conv (hhf_concl_conv cv o snd) ctxt) ct
  | _ => Conv.concl_conv ~1 cv ct);
*}

lemma Q_bisim0:
  "\<forall>z z'. Q z z' \<longrightarrow> rel_pre_pllist (=) Q
     (map_pre_pllist id (map_pllist arg.Rep_arg_pllist_0_0_pre_pllist_0_0) (pllist.Dtor_pllist z))
     (map_pre_pllist id (map_pllist arg.Rep_arg_pllist_0_0_pre_pllist_0_0) (pllist.Dtor_pllist z'))"
  apply (insert Q_ctr_bisim)
  apply (tactic \<open>
    HEADGOAL (mk_coinduct_discharge_prem_tac @{context}
      @{thms id_def o_def pre_pllist.map_comp pllist.map_comp
          pre_pllist.map_ident pllist.map_ident
          arg.Abs_arg_pllist_0_0_pre_pllist_0_0_inverse[OF UNIV_I]}
       [] (* rel_eqs' FIXME *)
       1 1 2
       @{thm rel_pre_pllist_def}
       @{thm Abs_pllist_pre_pllist_inverse[OF UNIV_I]}
       @{thm Abs_pllist_pre_pllist_inverse[OF UNIV_I]}
       @{thm pllist.dtor_ctor}
       @{thm pllist.exhaust}
       @{thms PLNil_def PLCons_def}
       [@{thms pllist.distinct(1)}, @{thms pllist.distinct(2)}] [[], @{thms pllist.sel}])
  \<close>)
  done

lemma Q_bisim0_alt_proof:
  "\<forall>z z'. Q z z' \<longrightarrow> rel_pre_pllist (=) Q
     (map_pre_pllist id (map_pllist arg.Rep_arg_pllist_0_0_pre_pllist_0_0) (pllist.Dtor_pllist z))
     (map_pre_pllist id (map_pllist arg.Rep_arg_pllist_0_0_pre_pllist_0_0) (pllist.Dtor_pllist z'))"
  apply (insert Q_ctr_bisim)
  apply (rule allI)
  apply (rule allI)
  apply (rule impI)
  apply (drule meta_spec)
  apply (drule meta_spec)
  apply (drule meta_mp)
   apply assumption
  apply (rule pllist.exhaust)
   apply (tactic \<open>BNF_FP_Def_Sugar_Tactics.co_induct_inst_as_projs_tac @{context} 0\<close>)
   apply hypsubst
   apply (rule pllist.exhaust)
    apply (tactic \<open>BNF_FP_Def_Sugar_Tactics.co_induct_inst_as_projs_tac @{context} 1\<close>)
    apply hypsubst
    apply (tactic \<open>HEADGOAL (CONVERSION (hhf_concl_conv
      (Conv.top_conv (K (Conv.try_conv (Conv.rewr_conv @{thm PLNil_def}))) @{context}) @{context}))\<close>)
    apply (unfold rel_pre_pllist_def pllist.dtor_ctor)
    apply (unfold
      rel_pre_pllist_def Abs_pllist_pre_pllist_inverse[OF UNIV_I] pllist.dtor_ctor pllist.sel
      rel_sum_simps rel_prod_inject prod.inject id_apply conj_assoc
      (* begin new *)
      pre_pllist.map_comp pllist.map_comp
      pre_pllist.map_ident pllist.map_ident
      arg.Abs_arg_pllist_0_0_pre_pllist_0_0_inverse[OF UNIV_I]
      (* end new *)
      o_apply vimage2p_def)
    apply (rule refl)
   apply hypsubst
   apply (elim conjE)
   apply (simp only: refl pllist.distinct simp_thms)

  apply (rule pllist.exhaust)
   apply (tactic \<open>BNF_FP_Def_Sugar_Tactics.co_induct_inst_as_projs_tac @{context} 1\<close>)
   apply hypsubst
   apply (elim conjE)
   apply (simp only: refl pllist.distinct simp_thms)
  apply hypsubst
  apply (tactic \<open>HEADGOAL (CONVERSION (hhf_concl_conv
    (Conv.top_conv (K (Conv.try_conv (Conv.rewr_conv @{thm PLCons_def}))) @{context}) @{context}))\<close>)
  apply (unfold rel_pre_pllist_def pllist.dtor_ctor pllist.sel)
  apply (unfold
     (* begin new *)
     pre_pllist.map_comp pllist.map_comp
     pre_pllist.map_ident pllist.map_ident
     arg.Abs_arg_pllist_0_0_pre_pllist_0_0_inverse[OF UNIV_I]
     (* end new *)
     rel_pre_pllist_def Abs_pllist_pre_pllist_inverse[OF UNIV_I] pllist.dtor_ctor pllist.sel rel_sum_simps rel_prod_inject prod.inject id_apply conj_assoc o_apply vimage2p_def)
  apply (simp only: pllist.distinct simp_thms)
  done

lemma Q_bisim:
  "\<forall>z z'. Q z z' \<longrightarrow> rel_pre_pllist (=) Q (pllist.Dtor_pllist z) (pllist.Dtor_pllist z')"
  apply (intro allI impI)
  apply (drule Q_bisim0[rule_format])
  apply (unfold pre_pllist.rel_map id_apply)
  apply (erule pre_pllist.rel_mono_strong)
   apply assumption
  apply (drule Q_inj_mono[THEN mp, rotated -1])
   apply (rule injI)
   apply (erule arg.Abs_arg_pllist_0_0_pre_pllist_0_0_inject[OF UNIV_I UNIV_I, THEN iffD1])
   apply (simp only: pllist.map_comp pllist.map_ident o_apply arg.Rep_arg_pllist_0_0_pre_pllist_0_0_inverse cong: pllist.map_cong)
  done
(*
ML {*
fun mk_nonuniform_induct_Q_dtor_tac ctxt live_arg_rep_injects pre_rel_maps Q_dtor0 Q_mono =
  HEADGOAL (forward_tac ctxt [Q_dtor0] THEN'
    SELECT_GOAL (unfold_thms_tac ctxt (id_apply :: pre_rel_maps)) THEN'
    subst_asm_tac ctxt NONE [Q_mono] THEN'
    rtac ctxt @{thm inj_on_def[THEN iffD2]} THEN'
    rtac ctxt ballI THEN' rtac ctxt ballI THEN' rtac ctxt impI THEN'
    eresolve_tac ctxt (map (fn thm => thm RS iffD1) live_arg_rep_injects) THEN'
    assume_tac ctxt);
*}

lemma Q_bisim_alt_proof:
  "\<forall>z z'. Q z z' \<longrightarrow> rel_pre_pllist (=) Q (pllist.Dtor_pllist z) (pllist.Dtor_pllist z')"
  apply (intro allI impI)
  apply (tactic \<open>mk_nonuniform_induct_Q_dtor_tac @{context}
    @{thms arg.Rep_arg_pllist_0_0_pre_pllist_0_0_inject}
    @{thms pre_pllist.rel_map}
    @{thm Q_bisim0[rule_format]} @{thm Q_inj_mono}\<close>)
  done
    sorry

thm Q_bisim[THEN predicate2I_obj, folded BNF_Def.vimage2p_def]
*)
lemma Q_coind:
  "Q \<le> BNF_Def.vimage2p pllist.Dtor_pllist pllist.Dtor_pllist (rel_pre_pllist (=) Q)"
  apply (rule predicate2I)
  apply (unfold vimage2p_def)
  by (simp only: Q_bisim)
(*
  apply (frule Q_bisim0[rule_format])
  apply (simp add: pre_pllist.rel_map)
  apply (subst (asm) Q_inj_mono)
   apply (unfold inj_on_def)
   apply (intro ballI impI)
   apply (erule arg.Rep_arg_pllist_0_0_pre_pllist_0_0_inject[THEN iffD1])
  apply assumption
  done
    sorry
*)

local_setup \<open> fn lthy =>
  let
  val (res, lthy) = BNF_NU_FP_Ind.construct_nu_co_ind BNF_Util.Greatest_FP
      (BNF_NU_FP_Def_Sugar.nu_fp_sugar_of lthy @{type_name pllist} |> the |> #nu_fp_res) []
      [@{term Q}] @{thms Q_parametric} (K @{thms Q_coind}) lthy
  in
    lthy
    |> Local_Theory.note ((@{binding co_result}, []), #co_ind_thms res) |> snd
  end\<close>

theorem "map_pllist id xs = xs"
  apply (rule co_result)
  apply (rule Q_def[THEN iffD2])
  apply (intro exI)
  apply (rule conjI)
   apply (rule refl)
  apply (rule refl)
  done


section \<open>Mutual Tests\<close>

nonuniform_datatype (dead 'c, 'a, 'b) X = E | X 'a 'b "('c, 'a + 'a, 'b * 'a) Y" "('c, 'a * 'a, 'b + 'a) X"
                and ('c, 'a, 'b) Y = Y 'a 'b "('c, 'a * 'a, 'b * 'a) Z" "('c, 'a, 'a * 'b * 'a) Z"
                and ('c, 'a, 'b) Z = Z 'a 'b "('c, 'a, 'b) X"

nonuniform_induct x y z in
  map_X_id[symmetric]: "map_X id id x = x"
| map_Y_id: "map_Y id id y = y"
| map_Z_id[symmetric]: "map_Z id id z = z"
  sorry

thm map_X_id map_Y_id map_Z_id

definition P1 where
  "P1 xs = (map_X id id xs = xs)"
definition P2 where
  "P2 xs = (map_Y id id xs = xs)"
definition P3 where
  "P3 xs = (map_Z id id xs = xs)"

theorem P1_parametric:
  assumes [transfer_rule]: "bi_unique R" "bi_unique S"
  shows "rel_fun (rel_X R S) (\<longleftarrow>) P1 P1"
  sorry

theorem P2_parametric:
  assumes [transfer_rule]: "bi_unique R" "bi_unique S"
  shows "rel_fun (rel_Y R S) (\<longleftarrow>) P2 P2"
  sorry

theorem P3_parametric:
  assumes [transfer_rule]: "bi_unique R" "bi_unique S"
  shows "rel_fun (rel_Z R S) (\<longleftarrow>) P3 P3"
  sorry

lemma P1_ind:
  "pred_pre_X (\<lambda>_. True) (\<lambda>_. True) P1 P1 P2 P3 P3 \<le> P1 o X_Y_Z.Ctor_X"
  sorry

lemma P2_ind:
  "pred_pre_Y (\<lambda>_. True) (\<lambda>_. True) P1 P1 P2 P3 P3 \<le> P2 o X_Y_Z.Ctor_Y"
  sorry

lemma P3_ind:
  "pred_pre_Z (\<lambda>_. True) (\<lambda>_. True) P1 P1 P2 P3 P3 \<le> P3 o X_Y_Z.Ctor_Z"
  sorry

local_setup \<open> fn lthy =>
  let
  val (res, lthy) = BNF_NU_FP_Ind.construct_nu_co_ind BNF_Util.Least_FP
      (BNF_NU_FP_Def_Sugar.nu_fp_sugar_of lthy @{type_name X} |> the |> #nu_fp_res) [@{typ 'c}]
      [@{term P1}, @{term P2}, @{term P3}] @{thms P1_parametric P2_parametric P3_parametric}
      (K @{thms P1_ind P2_ind P3_ind}) lthy
  in
    lthy
    |> Local_Theory.note ((@{binding XYZ_result}, []), #co_ind_thms res) |> snd
  end\<close>

theorem "map_X id id x = x" "map_Y id id y = y" "map_Y id id z = z"
  by (rule XYZ_result[unfolded P1_def P2_def P3_def])+

nonuniform_codatatype (dead 'c, 'a, 'b) XX = E | XX 'a 'b "('c, 'a + 'a, 'b * 'a) YY" "('c, 'a * 'a, 'b + 'a) XX"
                  and ('c, 'a, 'b) YY = YY 'a 'b "('c, 'a * 'a, 'b * 'a) ZZ" "('c, 'a, 'a * 'b * 'a) ZZ"
                  and ('c, 'a, 'b) ZZ = ZZ 'a 'b "('c, 'a, 'b) XX"

(* FIXME: make the command work without type annotations *)
nonuniform_coinduct
  "\<lambda>l r :: ('a, 'b, 'c) XX. \<exists>xs. l = map_XX id id xs \<and> r = xs"
  "\<lambda>l r :: ('a, 'b, 'c) YY. \<exists>xs. l = map_YY id id xs \<and> r = xs"
  "\<lambda>l r :: ('a, 'b, 'c) ZZ. \<exists>xs. l = map_ZZ id id xs \<and> r = xs"
in
  map_XX_id[symmetric]: "map_XX id id x = (x :: ('a, 'b, 'c) XX)"
| map_YY_id: "map_YY id id y = (y :: ('a, 'b, 'c) YY)"
| map_ZZ_id[symmetric]: "map_ZZ id id z = (z :: ('a, 'b, 'c) ZZ)"
  sorry

thm map_XX_id map_YY_id map_ZZ_id

definition Q1 where
  "Q1 l r = (\<exists>xs. l = map_XX id id xs \<and> r = xs)"
definition Q2 where
  "Q2 l r = (\<exists>xs. l = map_YY id id xs \<and> r = xs)"
definition Q3 where
  "Q3 l r = (\<exists>xs. l = map_ZZ id id xs \<and> r = xs)"

theorem Q1_parametric:
  assumes [transfer_rule]: "bi_unique R" "bi_unique S"
  shows "rel_fun (rel_XX R S) (rel_fun (rel_XX R S) (\<longrightarrow>)) Q1 Q1"
  sorry

theorem Q2_parametric:
  assumes [transfer_rule]: "bi_unique R" "bi_unique S"
  shows "rel_fun (rel_YY R S) (rel_fun (rel_YY R S) (\<longrightarrow>)) Q2 Q2"
  sorry

theorem Q3_parametric:
  assumes [transfer_rule]: "bi_unique R" "bi_unique S"
  shows "rel_fun (rel_ZZ R S) (rel_fun (rel_ZZ R S) (\<longrightarrow>)) Q3 Q3"
  sorry

lemma Q1_coind:
  "Q1 \<le> BNF_Def.vimage2p XX_YY_ZZ.Dtor_XX XX_YY_ZZ.Dtor_XX (rel_pre_XX (=) (=) Q1 Q1 Q2 Q3 Q3)"
  sorry

lemma Q2_coind:
  "Q2 \<le> BNF_Def.vimage2p XX_YY_ZZ.Dtor_YY XX_YY_ZZ.Dtor_YY (rel_pre_YY (=) (=) Q1 Q1 Q2 Q3 Q3)"
  sorry

lemma Q3_coind:
  "Q3 \<le> BNF_Def.vimage2p XX_YY_ZZ.Dtor_ZZ XX_YY_ZZ.Dtor_ZZ (rel_pre_ZZ (=) (=) Q1 Q1 Q2 Q3 Q3)"
  sorry

local_setup \<open> fn lthy =>
  let
  val (res, lthy) = BNF_NU_FP_Ind.construct_nu_co_ind BNF_Util.Greatest_FP
      (BNF_NU_FP_Def_Sugar.nu_fp_sugar_of lthy @{type_name XX} |> the |> #nu_fp_res) [@{typ 'c}]
      [@{term Q1}, @{term Q2}, @{term Q3}] @{thms Q1_parametric Q2_parametric Q3_parametric}
      (K @{thms Q1_coind Q2_coind Q3_coind}) lthy
  in
    lthy
    |> Local_Theory.note ((@{binding XXYYZZ_co_result}, []), #co_ind_thms res) |> snd
  end\<close>

theorem "map_XX id id x = x" "map_YY id id y = y" "map_YY id id z = z"
  by (rule XXYYZZ_co_result[unfolded Q1_def Q2_def Q3_def]; simp)+

end
