theory HDatatype_Multiparam_Multiocc
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

section \<open>Input\<close>

declare [[bnf_internals, typedef_overloaded]]

(*
datatype ('a, 'b) bt = L 'b | N "('a + 'a, 'b * 'b) bt" 'a "('a * 'a, 'a + 'b) bt"

('a, 'b, 'x, 'y) G = 'b + x * 'a * 'y
('a, 'b) F1A = 'a + 'a
('a, 'b) F2A = 'a * 'a
('a, 'b) F1B = 'b * 'b
('a, 'b) F2B = 'a + 'b

specs = [(G, [[F1A, F1B], [F2A, F2B]])]
*)

bnf_axiomatization ('a, 'b) F1A
bnf_axiomatization ('a, 'b) F2A
bnf_axiomatization ('a, 'b) F1B
bnf_axiomatization ('a, 'b) F2B

bnf_axiomatization ('a, 'b, 'x, 'y) G [wits: "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b, 'x, 'y) G"]


section \<open>Raw Type\<close>

datatype label = F1 | F2
type_synonym depth = "label list"
datatype ('a, 'b) shapeA = LeafA 'a
                         | Node1A "(('a, 'b) shapeA, ('a, 'b) shapeB) F1A"
                         | Node2A "(('a, 'b) shapeA, ('a, 'b) shapeB) F2A"
     and ('a, 'b) shapeB = LeafB 'b
                         | Node1B "(('a, 'b) shapeA, ('a, 'b) shapeB) F1B"
                         | Node2B "(('a, 'b) shapeA, ('a, 'b) shapeB) F2B"
datatype ('a, 'b) raw = Cons "(('a, 'b) shapeA, ('a, 'b) shapeB, ('a, 'b) raw, ('a, 'b) raw) G"

abbreviation "un_LeafA u \<equiv> case u of LeafA x \<Rightarrow> x"
abbreviation "un_LeafB u \<equiv> case u of LeafB x \<Rightarrow> x"
abbreviation "un_Node1A u \<equiv> case u of Node1A x \<Rightarrow> x"
abbreviation "un_Node1B u \<equiv> case u of Node1B x \<Rightarrow> x"
abbreviation "un_Node2A u \<equiv> case u of Node2A x \<Rightarrow> x"
abbreviation "un_Node2B u \<equiv> case u of Node2B x \<Rightarrow> x"
abbreviation "un_Cons t \<equiv> case t of Cons x \<Rightarrow> x"


section \<open>Invariant\<close>

primrec invar_shapeA :: "depth \<Rightarrow> ('a, 'b) shapeA \<Rightarrow> bool" and invar_shapeB :: "depth \<Rightarrow> ('a, 'b) shapeB \<Rightarrow> bool" where
  "invar_shapeA u0 (LeafA u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shapeA u0 (Node1A fa) = (case u0 of F1 # u0 \<Rightarrow> pred_F1A (invar_shapeA u0) (invar_shapeB u0) fa | _ \<Rightarrow> False)"
| "invar_shapeA u0 (Node2A fa) = (case u0 of F2 # u0 \<Rightarrow> pred_F2A (invar_shapeA u0) (invar_shapeB u0) fa | _ \<Rightarrow> False)"
| "invar_shapeB u0 (LeafB u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shapeB u0 (Node1B fb) = (case u0 of F1 # u0 \<Rightarrow> pred_F1B (invar_shapeA u0) (invar_shapeB u0) fb | _ \<Rightarrow> False)"
| "invar_shapeB u0 (Node2B fb) = (case u0 of F2 # u0 \<Rightarrow> pred_F2B (invar_shapeA u0) (invar_shapeB u0) fb | _ \<Rightarrow> False)"

primrec invar :: "depth \<Rightarrow> ('a, 'b) raw \<Rightarrow> bool" where
  "invar u0 (Cons g) = pred_G (invar_shapeA u0) (invar_shapeB u0) (invar (F1 # u0)) (invar (F2 # u0)) g"


section \<open>The Type\<close>

definition "wit x y = Cons (wit_G (LeafA x) (LeafB y))"

lemma invar_wit: "invar [] (wit x y)"
  by (auto simp only:
    wit_def invar.simps invar_shapeA.simps invar_shapeB.simps G.pred_map o_def id_apply pred_G_def
    G.set_map list.case dest: G.wit)

typedef ('a ,'b) T = "{t :: ('a ,'b) raw. invar [] t}"
  by (rule exI[of _ "wit undefined undefined"]) (auto simp only: invar_wit)


section \<open>Flat and Unflat\<close>

primrec (transfer)
  flatNode1A :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeA \<Rightarrow> ('a, 'b) shapeA" and
  flatNode1B :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeB \<Rightarrow> ('a, 'b) shapeB" where
  "flatNode1A (LeafA f) = Node1A (map_F1A LeafA LeafB f)"
| "flatNode1A (Node1A fa) = Node1A (map_F1A flatNode1A flatNode1B fa)"
| "flatNode1A (Node2A fa) = Node2A (map_F2A flatNode1A flatNode1B fa)"
| "flatNode1B (LeafB f) = Node1B (map_F1B LeafA LeafB f)"
| "flatNode1B (Node1B fb) = Node1B (map_F1B flatNode1A flatNode1B fb)"
| "flatNode1B (Node2B fb) = Node2B (map_F2B flatNode1A flatNode1B fb)"

primrec (transfer)
  flatNode2A :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeA \<Rightarrow> ('a, 'b) shapeA" and
  flatNode2B :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeB \<Rightarrow> ('a, 'b) shapeB" where
  "flatNode2A (LeafA f) = Node2A (map_F2A LeafA LeafB f)"
| "flatNode2A (Node1A fa) = Node1A (map_F1A flatNode2A flatNode2B fa)"
| "flatNode2A (Node2A fa) = Node2A (map_F2A flatNode2A flatNode2B fa)"
| "flatNode2B (LeafB f) = Node2B (map_F2B LeafA LeafB f)"
| "flatNode2B (Node1B fb) = Node1B (map_F1B flatNode2A flatNode2B fb)"
| "flatNode2B (Node2B fb) = Node2B (map_F2B flatNode2A flatNode2B fb)"

primrec (nonexhaustive)
   unflatNode1A :: "depth \<Rightarrow> ('a, 'b) shapeA \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) shapeA" and
   unflatNode1B :: "depth \<Rightarrow> ('a, 'b) shapeB \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) shapeB" where
  "unflatNode1A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_F1A un_LeafA un_LeafB fa)
      | _ # u0 \<Rightarrow> Node1A (map_F1A (unflatNode1A u0) (unflatNode1B u0) fa))"
| "unflatNode1A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2A (map_F2A (unflatNode1A u0) (unflatNode1B u0) fa))"
| "unflatNode1B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_F1B un_LeafA un_LeafB fb)
      | _ # u0 \<Rightarrow> Node1B (map_F1B (unflatNode1A u0) (unflatNode1B u0) fb))"
| "unflatNode1B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2B (map_F2B (unflatNode1A u0) (unflatNode1B u0) fb))"

primrec (nonexhaustive)
   unflatNode2A :: "depth \<Rightarrow> ('a, 'b) shapeA \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) shapeA" and
   unflatNode2B :: "depth \<Rightarrow> ('a, 'b) shapeB \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) shapeB" where
  "unflatNode2A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1A (map_F1A (unflatNode2A u0) (unflatNode2B u0) fa))"
| "unflatNode2A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_F2A un_LeafA un_LeafB fa)
      | _ # u0 \<Rightarrow> Node2A (map_F2A (unflatNode2A u0) (unflatNode2B u0) fa))"
| "unflatNode2B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1B (map_F1B (unflatNode2A u0) (unflatNode2B u0) fb))"
| "unflatNode2B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_F2B un_LeafA un_LeafB fb)
      | _ # u0 \<Rightarrow> Node2B (map_F2B (unflatNode2A u0) (unflatNode2B u0) fb))"

primrec (transfer) flat1 :: "(('a, 'b) F1A, ('a, 'b) F1B) raw \<Rightarrow> ('a, 'b) raw" where
  "flat1 (Cons g) = Cons (map_G flatNode1A flatNode1B flat1 flat1 g)"

primrec (transfer) flat2 :: "(('a, 'b) F2A, ('a, 'b) F2B) raw \<Rightarrow> ('a, 'b) raw" where
  "flat2 (Cons g) = Cons (map_G flatNode2A flatNode2B flat2 flat2 g)"

primrec unflat1 :: "depth \<Rightarrow> ('a, 'b) raw \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) raw" where
  "unflat1 u0 (Cons g) = Cons (map_G (unflatNode1A u0) (unflatNode1B u0) (unflat1 (F1 # u0)) (unflat1 (F2 # u0)) g)"

primrec unflat2 :: "depth \<Rightarrow> ('a, 'b) raw \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) raw" where
  "unflat2 u0 (Cons g) = Cons (map_G (unflatNode2A u0) (unflatNode2B u0) (unflat2 (F1 # u0)) (unflat2 (F2 # u0)) g)"


section \<open>Constructor and Selector\<close>

definition T :: "('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T) G \<Rightarrow>
  ('a, 'b) T" where
  "T g = Abs_T (Cons (map_G LeafA LeafB (flat1 o Rep_T) (flat2 o Rep_T) g))"

definition un_T :: "('a, 'b) T \<Rightarrow>
  ('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T) G" where
  "un_T t = map_G un_LeafA un_LeafB (Abs_T o unflat1 []) (Abs_T o unflat2 []) (un_Cons (Rep_T t))"


section \<open>BNF Instance\<close>

lemma invarU_map_closed_raw:
  "(\<forall>u0. invar_shapeA u0 (map_shapeA f g ua) \<longleftrightarrow> invar_shapeA u0 ua) \<and>
   (\<forall>u0. invar_shapeB u0 (map_shapeB f g ub) \<longleftrightarrow> invar_shapeB u0 ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 (map_shapeA f g ua) \<longleftrightarrow> invar_shapeA u0 ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 (map_shapeB f g ub) \<longleftrightarrow> invar_shapeB u0 ub" ua ub])
  apply (auto simp only:
      shapeA.map shapeB.map invar_shapeA.simps invar_shapeB.simps
      F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map
      o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invarU_map_closed =
  spec[OF conjunct1[OF invarU_map_closed_raw]]
  spec[OF conjunct2[OF invarU_map_closed_raw]]

lemma invar_map_closed_raw:
  "\<forall>u0. invar u0 (map_raw f g t) \<longleftrightarrow> invar u0 t"
  apply (induct t)
  apply (auto simp only:
      raw.map invar.simps id_apply o_apply
      G.pred_map invarU_map_closed
    elim!: G.pred_mono_strong)
  done

lemmas invar_map_closed = spec[OF invar_map_closed_raw]

lift_bnf ('a, 'b) T (*TODO wits*)
  apply (auto simp only:
      invar_map_closed)
  done


section \<open>Lemmas about Flat, Unflat, Invar\<close>

lemma invar_shapeA_depth_iff:
  "invar_shapeA [] x = (\<exists>a. x = LeafA a)"
  "invar_shapeA (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shapeA u0) (invar_shapeB u0) y)"
  "invar_shapeA (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shapeA u0) (invar_shapeB u0) y)"
  apply (rule shapeA.exhaust[of x "invar_shapeA [] x = (\<exists>a. x = LeafA a)"];
    simp only: invar_shapeA.simps shapeA.inject shapeA.distinct simp_thms list.case)
  apply (rule shapeA.exhaust[of x "invar_shapeA (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shapeA u0) (invar_shapeB u0) y)"];
    simp only: invar_shapeA.simps F1A.pred_map shapeA.inject shapeA.distinct label.case id_o simp_thms list.case)
  apply (rule shapeA.exhaust[of x "invar_shapeA (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shapeA u0) (invar_shapeB u0) y)"];
    simp only: invar_shapeA.simps F2A.pred_map shapeA.inject shapeA.distinct label.case id_o simp_thms list.case)
  done

lemma invar_shapeB_depth_iff:
  "invar_shapeB [] x = (\<exists>b. x = LeafB b)"
  "invar_shapeB (F1 # u0) x = (\<exists>y. x = Node1B y \<and> pred_F1B (invar_shapeA u0) (invar_shapeB u0) y)"
  "invar_shapeB (F2 # u0) x = (\<exists>y. x = Node2B y \<and> pred_F2B (invar_shapeA u0) (invar_shapeB u0) y)"
  apply (rule shapeB.exhaust[of x "invar_shapeB [] x = (\<exists>a. x = LeafB a)"];
    simp only: invar_shapeB.simps shapeB.inject shapeB.distinct simp_thms list.case)
  apply (rule shapeB.exhaust[of x "invar_shapeB (F1 # u0) x = (\<exists>y. x = Node1B y \<and> pred_F1B (invar_shapeA u0) (invar_shapeB u0) y)"];
    simp only: invar_shapeB.simps F1B.pred_map shapeB.inject shapeB.distinct label.case id_o simp_thms list.case)
  apply (rule shapeB.exhaust[of x "invar_shapeB (F2 # u0) x = (\<exists>y. x = Node2B y \<and> pred_F2B (invar_shapeA u0) (invar_shapeB u0) y)"];
    simp only: invar_shapeB.simps F2B.pred_map shapeB.inject shapeB.distinct label.case id_o simp_thms list.case)
  done

lemma flatU1_unflatU1_raw:
  fixes ua :: "('a, 'b) shapeA" and ub :: "('a, 'b) shapeB"
  shows
  "(\<forall>u0. invar_shapeA (snoc F1 u0) ua \<longrightarrow> flatNode1A (unflatNode1A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shapeB (snoc F1 u0) ub \<longrightarrow> flatNode1B (unflatNode1B u0 ub) = ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA (snoc F1 u0) ua \<longrightarrow> flatNode1A (unflatNode1A u0 ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB (snoc F1 u0) ub \<longrightarrow> flatNode1B (unflatNode1B u0 ub) = ub" ua ub])
  apply (auto simp only:
    unflatNode1A.simps unflatNode1B.simps flatNode1A.simps flatNode1B.simps
    invar_shapeA.simps invar_shapeB.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp
    shapeA.case shapeB.case invar_shapeA_depth_iff invar_shapeB_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits)
  done

lemma flatU2_unflatU2_raw:
  fixes ua :: "('a, 'b) shapeA" and ub :: "('a, 'b) shapeB"
  shows
  "(\<forall>u0. invar_shapeA (snoc F2 u0) ua \<longrightarrow> flatNode2A (unflatNode2A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shapeB (snoc F2 u0) ub \<longrightarrow> flatNode2B (unflatNode2B u0 ub) = ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA (snoc F2 u0) ua \<longrightarrow> flatNode2A (unflatNode2A u0 ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB (snoc F2 u0) ub \<longrightarrow> flatNode2B (unflatNode2B u0 ub) = ub" ua ub])
  apply (auto simp only:
    unflatNode2A.simps unflatNode2B.simps flatNode2A.simps flatNode2B.simps
    invar_shapeA.simps invar_shapeB.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp
    shapeA.case shapeB.case invar_shapeA_depth_iff invar_shapeB_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas flatU_unflatU =
  mp[OF spec[OF conjunct1[OF flatU1_unflatU1_raw]]]
  mp[OF spec[OF conjunct2[OF flatU1_unflatU1_raw]]]
  mp[OF spec[OF conjunct1[OF flatU2_unflatU2_raw]]]
  mp[OF spec[OF conjunct2[OF flatU2_unflatU2_raw]]]

lemma unflatU1_flatU1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeA" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeB"
  shows
  "(\<forall>u0. invar_shapeA u0 ua \<longrightarrow> unflatNode1A u0 (flatNode1A ua) = ua) \<and>
   (\<forall>u0. invar_shapeB u0 ub \<longrightarrow> unflatNode1B u0 (flatNode1B ub) = ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 ua \<longrightarrow> unflatNode1A u0 (flatNode1A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 ub \<longrightarrow> unflatNode1B u0 (flatNode1B ub) = ub" ua ub])
  apply (auto simp only:
      unflatNode1A.simps unflatNode1B.simps flatNode1A.simps flatNode1B.simps invar_shapeA.simps invar_shapeB.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True
      shapeA.case shapeB.case
      id_apply o_apply refl
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits sum.splits)
  done

lemma unflatU2_flatU2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeA" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeB"
  shows
  "(\<forall>u0. invar_shapeA u0 ua \<longrightarrow> unflatNode2A u0 (flatNode2A ua) = ua) \<and>
   (\<forall>u0. invar_shapeB u0 ub \<longrightarrow> unflatNode2B u0 (flatNode2B ub) = ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 ua \<longrightarrow> unflatNode2A u0 (flatNode2A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 ub \<longrightarrow> unflatNode2B u0 (flatNode2B ub) = ub" ua ub])
  apply (auto simp only:
      unflatNode2A.simps unflatNode2B.simps flatNode2A.simps flatNode2B.simps invar_shapeA.simps invar_shapeB.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True
      shapeA.case shapeB.case
      id_apply o_apply refl
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits sum.splits)
  done

lemmas unflatU_flatU =
  mp[OF spec[OF conjunct1[OF unflatU1_flatU1_raw]]]
  mp[OF spec[OF conjunct2[OF unflatU1_flatU1_raw]]]
  mp[OF spec[OF conjunct1[OF unflatU2_flatU2_raw]]]
  mp[OF spec[OF conjunct2[OF unflatU2_flatU2_raw]]]

lemma invarU_flatU1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeA" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeB"
  shows
  "(\<forall>u0. invar_shapeA u0 ua \<longrightarrow> invar_shapeA (snoc F1 u0) (flatNode1A ua)) \<and>
   (\<forall>u0. invar_shapeB u0 ub \<longrightarrow> invar_shapeB (snoc F1 u0) (flatNode1B ub))"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 ua \<longrightarrow> invar_shapeA (snoc F1 u0) (flatNode1A ua)"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 ub \<longrightarrow> invar_shapeB (snoc F1 u0) (flatNode1B ub)" ua ub])
  apply (auto simp only:
      flatNode1A.simps flatNode1B.simps invar_shapeA.simps invar_shapeB.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits sum.splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong)
  done

lemma invarU_flatU2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeA" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeB"
  shows
  "(\<forall>u0. invar_shapeA u0 ua \<longrightarrow> invar_shapeA (snoc F2 u0) (flatNode2A ua)) \<and>
   (\<forall>u0. invar_shapeB u0 ub \<longrightarrow> invar_shapeB (snoc F2 u0) (flatNode2B ub))"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 ua \<longrightarrow> invar_shapeA (snoc F2 u0) (flatNode2A ua)"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 ub \<longrightarrow> invar_shapeB (snoc F2 u0) (flatNode2B ub)" ua ub])
  apply (auto simp only:
      flatNode2A.simps flatNode2B.simps invar_shapeA.simps invar_shapeB.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits sum.splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong)
  done

lemmas invarU_flatU =
  mp[OF spec[OF conjunct1[OF invarU_flatU1_raw]]]
  mp[OF spec[OF conjunct2[OF invarU_flatU1_raw]]]
  mp[OF spec[OF conjunct1[OF invarU_flatU2_raw]]]
  mp[OF spec[OF conjunct2[OF invarU_flatU2_raw]]]

lemma invar_flat1_raw: "\<forall>u0. invar u0 t \<longrightarrow> invar (snoc F1 u0) (flat1 t)"
  apply (induct t)
  apply (auto simp only:
      flat1.simps invar.simps snoc.simps[symmetric] invarU_flatU id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemma invar_flat2_raw: "\<forall>u0. invar u0 t \<longrightarrow> invar (snoc F2 u0) (flat2 t)"
  apply (induct t)
  apply (auto simp only:
      flat2.simps invar.simps snoc.simps[symmetric] invarU_flatU id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemmas invar_flat =
  mp[OF spec[OF invar_flat1_raw]]
  mp[OF spec[OF invar_flat2_raw]]

lemma invarU_unflatU1_raw:
  fixes ua :: "('a, 'b) shapeA" and ub :: "('a, 'b) shapeB"
  shows
  "(\<forall>u0. invar_shapeA (snoc F1 u0) ua \<longrightarrow> invar_shapeA u0 (unflatNode1A u0 ua)) \<and>
   (\<forall>u0. invar_shapeB (snoc F1 u0) ub \<longrightarrow> invar_shapeB u0 (unflatNode1B u0 ub))"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA (snoc F1 u0) ua \<longrightarrow> invar_shapeA u0 (unflatNode1A u0 ua)"
    "\<lambda>ub. \<forall>u0. invar_shapeB (snoc F1 u0) ub \<longrightarrow> invar_shapeB u0 (unflatNode1B u0 ub)" ua ub])
  apply (auto simp only:
      unflatNode1A.simps unflatNode1B.simps invar_shapeA.simps invar_shapeB.simps snoc.simps snoc_neq_Nil
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map
      id_apply o_apply refl
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits)
  done

lemma invarU_unflatU2_raw:
  fixes ua :: "('a, 'b) shapeA" and ub :: "('a, 'b) shapeB"
  shows
  "(\<forall>u0. invar_shapeA (snoc F2 u0) ua \<longrightarrow> invar_shapeA u0 (unflatNode2A u0 ua)) \<and>
   (\<forall>u0. invar_shapeB (snoc F2 u0) ub \<longrightarrow> invar_shapeB u0 (unflatNode2B u0 ub))"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA (snoc F2 u0) ua \<longrightarrow> invar_shapeA u0 (unflatNode2A u0 ua)"
    "\<lambda>ub. \<forall>u0. invar_shapeB (snoc F2 u0) ub \<longrightarrow> invar_shapeB u0 (unflatNode2B u0 ub)" ua ub])
  apply (auto simp only:
      unflatNode2A.simps unflatNode2B.simps invar_shapeA.simps invar_shapeB.simps snoc.simps snoc_neq_Nil
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map
      id_apply o_apply refl
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
    split: list.splits label.splits)
  done

lemmas invarU_unflatU =
  mp[OF spec[OF conjunct1[OF invarU_unflatU1_raw]]]
  mp[OF spec[OF conjunct2[OF invarU_unflatU1_raw]]]
  mp[OF spec[OF conjunct1[OF invarU_unflatU2_raw]]]
  mp[OF spec[OF conjunct2[OF invarU_unflatU2_raw]]]

lemma invar_unflat1_raw: "\<forall>u0. invar (snoc F1 u0) t \<longrightarrow> invar u0 (unflat1 u0 t)"
  apply (induct t)
  apply (auto simp only:
      unflat1.simps invar.simps snoc.simps invarU_unflatU id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemma invar_unflat2_raw: "\<forall>u0. invar (snoc F2 u0) t \<longrightarrow> invar u0 (unflat2 u0 t)"
  apply (induct t)
  apply (auto simp only:
      unflat2.simps invar.simps snoc.simps invarU_unflatU id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemmas invar_unflat =
  mp[OF spec[OF invar_unflat1_raw]]
  mp[OF spec[OF invar_unflat2_raw]]

lemma flat1_unflat1_raw: "\<forall>u0. invar (snoc F1 u0) t \<longrightarrow> flat1 (unflat1 u0 t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat1.simps flat1.simps invar.simps snoc.simps
      flatU_unflatU id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done

lemma flat2_unflat2_raw: "\<forall>u0. invar (snoc F2 u0) t \<longrightarrow> flat2 (unflat2 u0 t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat2.simps flat2.simps invar.simps snoc.simps
      flatU_unflatU id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done

lemmas flat_unflat =
  mp[OF spec[OF flat1_unflat1_raw]]
  mp[OF spec[OF flat2_unflat2_raw]]

lemma unflat1_flat1_raw: "\<forall>u0. invar u0 t \<longrightarrow> unflat1 u0 (flat1 t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat1.simps flat1.simps invar.simps unflatU_flatU id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done

lemma unflat2_flat2_raw: "\<forall>u0. invar u0 t \<longrightarrow> unflat2 u0 (flat2 t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat2.simps flat2.simps invar.simps unflatU_flatU id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done

lemmas unflat_flat =
  mp[OF spec[OF unflat1_flat1_raw]]
  mp[OF spec[OF unflat2_flat2_raw]]


section \<open>Constructor is Bijection\<close>

lemma un_T_T: "un_T (T x) = x"
  unfolding T_def un_T_def
  apply (subst Abs_T_inverse)
   apply (auto simp only: 
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F1, symmetric] snoc.simps(1)[of F2, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat invar_map_closed
     cong: G.pred_cong) []
  apply (auto simp only:
      raw.case shapeA.case shapeB.case Rep_T_inverse o_apply sum.case 
      G.map_comp G.map_ident Rep_T[simplified] unflat_flat
      raw.map_comp raw.map_ident invar_map_closed 
    cong: G.map_cong raw.map_cong) []
  done

lemma T_un_T: "T (un_T x) = x"
  unfolding T_def un_T_def G.map_comp o_def
  apply (rule iffD1[OF Rep_T_inject])
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F1, symmetric] snoc.simps(1)[of F2, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat invar_map_closed
     cong: G.pred_cong) []
  apply (insert Rep_T[simplified, of x])
  apply (rule raw.exhaust[of "Rep_T x"])
  apply (auto simp only:
      raw.case shapeA.case shapeB.case invar.simps invar_shapeA_depth_iff invar_shapeB_depth_iff
      snoc.simps(1)[of F1, symmetric] snoc.simps(1)[of F2, symmetric]
      G.pred_map Abs_T_inverse invar_unflat flat_unflat
      id_apply o_apply mem_Collect_eq
      raw.map_comp invar_map_closed
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong
    cong: raw.map_cong) []
  done


section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

lemma flatU1_map_raw:
  "map_shapeA f g (flatNode1A ua) = flatNode1A (map_shapeA (map_F1A f g) (map_F1B f g) ua) \<and>
   map_shapeB f g (flatNode1B ub) = flatNode1B (map_shapeB (map_F1A f g) (map_F1B f g) ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. map_shapeA f g (flatNode1A ua) = flatNode1A (map_shapeA (map_F1A f g) (map_F1B f g) ua)"
    "\<lambda>ub. map_shapeB f g (flatNode1B ub) = flatNode1B (map_shapeB (map_F1A f g) (map_F1B f g) ub)" ua ub])
  apply (auto simp only:
      shapeA.map shapeB.map flatNode1A.simps flatNode1B.simps F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0)
  done

lemma flatU2_map_raw:
  "map_shapeA f g (flatNode2A ua) = flatNode2A (map_shapeA (map_F2A f g) (map_F2B f g) ua) \<and>
   map_shapeB f g (flatNode2B ub) = flatNode2B (map_shapeB (map_F2A f g) (map_F2B f g) ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. map_shapeA f g (flatNode2A ua) = flatNode2A (map_shapeA (map_F2A f g) (map_F2B f g) ua)"
    "\<lambda>ub. map_shapeB f g (flatNode2B ub) = flatNode2B (map_shapeB (map_F2A f g) (map_F2B f g) ub)" ua ub])
  apply (auto simp only:
      shapeA.map shapeB.map flatNode2A.simps flatNode2B.simps F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0)
  done

lemmas flatU_map =
  conjunct1[OF flatU1_map_raw]
  conjunct2[OF flatU1_map_raw]
  conjunct1[OF flatU2_map_raw]
  conjunct2[OF flatU2_map_raw]

lemma map_raw_flat1:
  "map_raw f g (flat1 t) = flat1 (map_raw (map_F1A f g) (map_F1B f g) t)"
  apply (induct t)
  apply (simp only:
      raw.map flat1.simps G.map_comp flatU_map o_apply
    cong: G.map_cong0)
  done

lemma map_raw_flat2:
  "map_raw f g (flat2 t) = flat2 (map_raw (map_F2A f g) (map_F2B f g) t)"
  apply (induct t)
  apply (simp only:
      raw.map flat2.simps G.map_comp flatU_map o_apply
    cong: G.map_cong0)
  done

lemmas map_raw_flat = map_raw_flat1 map_raw_flat2

lemma map_T: "map_T f g (T t) =
  T (map_G f g (map_T (map_F1A f g) (map_F1B f g)) (map_T (map_F2A f g) (map_F2B f g)) t)"
  unfolding map_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F1, symmetric] snoc.simps(1)[of F2, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) []
  apply (auto simp only:
      raw.map shapeA.map shapeB.map G.map_comp o_apply mem_Collect_eq
     invar_map_closed Abs_T_inverse Rep_T[simplified] map_raw_flat
    cong: G.map_cong) []
  done

subsection \<open>set\<close>

lemma flatU1_set1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeA" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeB"
  shows
  "(set1_shapeA (flatNode1A ua) = UNION (set1_shapeA ua) set1_F1A \<union> UNION (set2_shapeA ua) set1_F1B) \<and>
   (set1_shapeB (flatNode1B ub) = UNION (set1_shapeB ub) set1_F1A \<union> UNION (set2_shapeB ub) set1_F1B)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. set1_shapeA (flatNode1A ua) = UNION (set1_shapeA ua) set1_F1A \<union> UNION (set2_shapeA ua) set1_F1B"
    "\<lambda>ub. set1_shapeB (flatNode1B ub) = UNION (set1_shapeB ub) set1_F1A \<union> UNION (set2_shapeB ub) set1_F1B" ua ub])
  apply (simp_all only:
      flatNode1A.simps flatNode1B.simps shapeA.set shapeB.set F1A.set_map F2A.set_map F1B.set_map F2B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma flatU2_set1_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeA" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeB"
  shows
  "(set1_shapeA (flatNode2A ua) = UNION (set1_shapeA ua) set1_F2A \<union> UNION (set2_shapeA ua) set1_F2B) \<and>
   (set1_shapeB (flatNode2B ub) = UNION (set1_shapeB ub) set1_F2A \<union> UNION (set2_shapeB ub) set1_F2B)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. set1_shapeA (flatNode2A ua) = UNION (set1_shapeA ua) set1_F2A \<union> UNION (set2_shapeA ua) set1_F2B"
    "\<lambda>ub. set1_shapeB (flatNode2B ub) = UNION (set1_shapeB ub) set1_F2A \<union> UNION (set2_shapeB ub) set1_F2B" ua ub])
  apply (simp_all only:
      flatNode2A.simps flatNode2B.simps shapeA.set shapeB.set F1A.set_map F2A.set_map F1B.set_map F2B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma flatU1_set2_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeA" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shapeB"
  shows
  "(set2_shapeA (flatNode1A ua) = UNION (set1_shapeA ua) set2_F1A \<union> UNION (set2_shapeA ua) set2_F1B) \<and>
   (set2_shapeB (flatNode1B ub) = UNION (set1_shapeB ub) set2_F1A \<union> UNION (set2_shapeB ub) set2_F1B)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. set2_shapeA (flatNode1A ua) = UNION (set1_shapeA ua) set2_F1A \<union> UNION (set2_shapeA ua) set2_F1B"
    "\<lambda>ub. set2_shapeB (flatNode1B ub) = UNION (set1_shapeB ub) set2_F1A \<union> UNION (set2_shapeB ub) set2_F1B" ua ub])
  apply (simp_all only:
      flatNode1A.simps flatNode1B.simps shapeA.set shapeB.set F1A.set_map F2A.set_map F1B.set_map F2B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma flatU2_set2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeA" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shapeB"
  shows
  "(set2_shapeA (flatNode2A ua) = UNION (set1_shapeA ua) set2_F2A \<union> UNION (set2_shapeA ua) set2_F2B) \<and>
   (set2_shapeB (flatNode2B ub) = UNION (set1_shapeB ub) set2_F2A \<union> UNION (set2_shapeB ub) set2_F2B)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. set2_shapeA (flatNode2A ua) = UNION (set1_shapeA ua) set2_F2A \<union> UNION (set2_shapeA ua) set2_F2B"
    "\<lambda>ub. set2_shapeB (flatNode2B ub) = UNION (set1_shapeB ub) set2_F2A \<union> UNION (set2_shapeB ub) set2_F2B" ua ub])
  apply (simp_all only:
      flatNode2A.simps flatNode2B.simps shapeA.set shapeB.set F1A.set_map F2A.set_map F1B.set_map F2B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemmas flatU_set =
  conjunct1[OF flatU1_set1_raw]
  conjunct1[OF flatU1_set2_raw]
  conjunct2[OF flatU1_set1_raw]
  conjunct2[OF flatU1_set2_raw]
  conjunct1[OF flatU2_set1_raw]
  conjunct1[OF flatU2_set2_raw]
  conjunct2[OF flatU2_set1_raw]
  conjunct2[OF flatU2_set2_raw]


lemma set_raw_flat1:
  "set1_raw (flat1 t) = UNION (set1_raw t) set1_F1A \<union> UNION (set2_raw t) set1_F1B"
  "set2_raw (flat1 t) = UNION (set1_raw t) set2_F1A \<union> UNION (set2_raw t) set2_F1B"
  apply (induct t)
  apply (simp_all only: flat1.simps raw.set shapeA.set G.set_map flatU_set
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set_raw_flat2:
  "set1_raw (flat2 t) = UNION (set1_raw t) set1_F2A \<union> UNION (set2_raw t) set1_F2B"
  "set2_raw (flat2 t) = UNION (set1_raw t) set2_F2A \<union> UNION (set2_raw t) set2_F2B"
  apply (induct t)
  apply (simp_all only: flat2.simps raw.set shapeA.set G.set_map flatU_set
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemmas set_raw_flat = set_raw_flat1 set_raw_flat2

lemma set_T: "set1_T (T g) = set1_G g \<union>
  (\<Union>(set1_F1A ` (\<Union>(set1_T ` (set3_G g))))) \<union>
  (\<Union>(set1_F1B ` (\<Union>(set2_T ` (set3_G g))))) \<union>
  (\<Union>(set1_F2A ` (\<Union>(set1_T ` (set4_G g))))) \<union>
  (\<Union>(set1_F2B ` (\<Union>(set2_T ` (set4_G g)))))"
  "set2_T (T g) = set2_G g \<union>
  (\<Union>(set2_F1A ` (\<Union>(set1_T ` (set3_G g))))) \<union>
  (\<Union>(set2_F1B ` (\<Union>(set2_T ` (set3_G g))))) \<union>
  (\<Union>(set2_F2A ` (\<Union>(set1_T ` (set4_G g))))) \<union>
  (\<Union>(set2_F2B ` (\<Union>(set2_T ` (set4_G g)))))"
  unfolding set1_T_def set2_T_def T_def o_apply
  sorry

lemma flatU1_rel_raw:
  "(\<forall>u0 ua'. invar_shapeA u0 ua \<longrightarrow> invar_shapeA u0 ua' \<longrightarrow> rel_shapeA R S (flatNode1A ua) (flatNode1A ua') \<longrightarrow>
     rel_shapeA (rel_F1A R S) (rel_F1B R S) ua ua') \<and>
   (\<forall>u0 ub'. invar_shapeB u0 ub \<longrightarrow> invar_shapeB u0 ub' \<longrightarrow> rel_shapeB R S (flatNode1B ub) (flatNode1B ub') \<longrightarrow>
     rel_shapeB (rel_F1A R S) (rel_F1B R S) ub ub')"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0 ua'. invar_shapeA u0 ua \<longrightarrow> invar_shapeA u0 ua' \<longrightarrow> rel_shapeA R S (flatNode1A ua) (flatNode1A ua') \<longrightarrow>
    rel_shapeA (rel_F1A R S) (rel_F1B R S) ua ua'"
    "\<lambda>ub. \<forall>u0 ub'. invar_shapeB u0 ub \<longrightarrow> invar_shapeB u0 ub' \<longrightarrow> rel_shapeB R S (flatNode1B ub) (flatNode1B ub') \<longrightarrow>
    rel_shapeB (rel_F1A R S) (rel_F1B R S) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shapeA.simps invar_shapeB.simps flatNode1A.simps flatNode1B.simps shapeA.rel_inject shapeB.rel_inject
      invar_shapeA_depth_iff invar_shapeB_depth_iff ball_simps id_apply
      F1A.rel_map pred_F1A_def F1A.set_map
      F2A.rel_map pred_F2A_def F2A.set_map
      F1B.rel_map pred_F1B_def F1B.set_map
      F2B.rel_map pred_F2B_def F2B.set_map
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
    split: list.splits label.splits)
  done

lemma flatU2_rel_raw:
  "(\<forall>u0 ua'. invar_shapeA u0 ua \<longrightarrow> invar_shapeA u0 ua' \<longrightarrow> rel_shapeA R S (flatNode2A ua) (flatNode2A ua') \<longrightarrow>
     rel_shapeA (rel_F2A R S) (rel_F2B R S) ua ua') \<and>
   (\<forall>u0 ub'. invar_shapeB u0 ub \<longrightarrow> invar_shapeB u0 ub' \<longrightarrow> rel_shapeB R S (flatNode2B ub) (flatNode2B ub') \<longrightarrow>
     rel_shapeB (rel_F2A R S) (rel_F2B R S) ub ub')"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0 ua'. invar_shapeA u0 ua \<longrightarrow> invar_shapeA u0 ua' \<longrightarrow> rel_shapeA R S (flatNode2A ua) (flatNode2A ua') \<longrightarrow>
    rel_shapeA (rel_F2A R S) (rel_F2B R S) ua ua'"
    "\<lambda>ub. \<forall>u0 ub'. invar_shapeB u0 ub \<longrightarrow> invar_shapeB u0 ub' \<longrightarrow> rel_shapeB R S (flatNode2B ub) (flatNode2B ub') \<longrightarrow>
    rel_shapeB (rel_F2A R S) (rel_F2B R S) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shapeA.simps invar_shapeB.simps flatNode2A.simps flatNode2B.simps shapeA.rel_inject shapeB.rel_inject
      invar_shapeA_depth_iff invar_shapeB_depth_iff ball_simps id_apply
      F1A.rel_map pred_F1A_def F1A.set_map
      F2A.rel_map pred_F2A_def F2A.set_map
      F1B.rel_map pred_F1B_def F1B.set_map
      F2B.rel_map pred_F2B_def F2B.set_map
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
    split: list.splits label.splits)
  done


subsection \<open>rel\<close>

lemma flatU1_rel:
  "invar_shapeA u0 ua \<Longrightarrow> invar_shapeA u0 ua' \<Longrightarrow>
    rel_shapeA R S (flatNode1A ua) (flatNode1A ua') = rel_shapeA (rel_F1A R S) (rel_F1B R S) ua ua'"
  "invar_shapeB u0 ub \<Longrightarrow> invar_shapeB u0 ub' \<Longrightarrow>
    rel_shapeB R S (flatNode1B ub) (flatNode1B ub') = rel_shapeB (rel_F1A R S) (rel_F1B R S) ub ub'"
  apply (rule iffI[rotated, OF rel_funD[OF flatNode1A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF flatU1_rel_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flatNode1B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF flatU1_rel_raw]]]]]]; assumption)
  done

lemma flatU2_rel:
  "invar_shapeA u0 ua \<Longrightarrow> invar_shapeA u0 ua' \<Longrightarrow>
    rel_shapeA R S (flatNode2A ua) (flatNode2A ua') = rel_shapeA (rel_F2A R S) (rel_F2B R S) ua ua'"
  "invar_shapeB u0 ub \<Longrightarrow> invar_shapeB u0 ub' \<Longrightarrow>
    rel_shapeB R S (flatNode2B ub) (flatNode2B ub') = rel_shapeB (rel_F2A R S) (rel_F2B R S) ub ub'"
  apply (rule iffI[rotated, OF rel_funD[OF flatNode2A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF flatU2_rel_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flatNode2B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF flatU2_rel_raw]]]]]]; assumption)
  done

lemma rel_raw_flat1_raw:
  "\<forall>t' u0. invar u0 t \<longrightarrow> invar u0 t' \<longrightarrow>
   rel_raw R S (flat1 t) (flat1 t') \<longrightarrow> rel_raw (rel_F1A R S) (rel_F1B R S) t t'"
  apply (induct t)
  apply (rule allI)
  apply (case_tac t')
  apply (auto simp only:
      invar.simps flat1.simps raw.rel_inject G.rel_map pred_G_def flatU1_rel G.set_map ball_simps id_apply
    elim!: G.rel_mono_strong)
  done

lemma rel_raw_flat1:
  "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
   rel_raw R S (flat1 t) (flat1 t') = rel_raw (rel_F1A R S) (rel_F1B R S) t t'"
  apply (rule iffI[rotated, OF rel_funD[OF flat1.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF rel_raw_flat1_raw]]]]]; assumption)
  done

lemma rel_raw_flat2_raw:
  "\<forall>t' u0. invar u0 t \<longrightarrow> invar u0 t' \<longrightarrow>
   rel_raw R S (flat2 t) (flat2 t') \<longrightarrow> rel_raw (rel_F2A R S) (rel_F2B R S) t t'"
  apply (induct t)
  apply (rule allI)
  apply (case_tac t')
  apply (auto simp only:
      invar.simps flat2.simps raw.rel_inject G.rel_map pred_G_def flatU2_rel G.set_map ball_simps id_apply
    elim!: G.rel_mono_strong)
  done

lemma rel_raw_flat2:
  "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
   rel_raw R S (flat2 t) (flat2 t') = rel_raw (rel_F2A R S) (rel_F2B R S) t t'"
  apply (rule iffI[rotated, OF rel_funD[OF flat2.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF rel_raw_flat2_raw]]]]]; assumption)
  done

lemma rel_T: "rel_T R S (T g) (T g') = rel_G R S (rel_T (rel_F1A R S) (rel_F1B R S)) (rel_T (rel_F2A R S) (rel_F2B R S)) g g'"
  unfolding rel_T_def T_def vimage2p_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F1, symmetric] snoc.simps(1)[of F2, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) [2]
  apply (simp only:
    raw.rel_inject G.rel_map shapeA.rel_inject shapeB.rel_inject o_apply
    rel_raw_flat1[OF Rep_T[simplified] Rep_T[simplified]]
    rel_raw_flat2[OF Rep_T[simplified] Rep_T[simplified]])
  done

(*
bnf_axiomatization ('a, 'b) F1A
bnf_axiomatization ('a, 'b) F2A
bnf_axiomatization ('a, 'b) F1B
bnf_axiomatization ('a, 'b) F2B

bnf_axiomatization ('a, 'b, 'x, 'y) G [wits: "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b, 'x, 'y) G"]

datatype label = F1 | F2
type_synonym depth = "label list"
datatype ('a, 'b) shapeA = LeafA 'a
                         | Node1A "(('a, 'b) shapeA, ('a, 'b) shapeB) F1A"
                         | Node2A "(('a, 'b) shapeA, ('a, 'b) shapeB) F2A"
     and ('a, 'b) shapeB = LeafB 'b
                         | Node1B "(('a, 'b) shapeA, ('a, 'b) shapeB) F1B"
                         | Node2B "(('a, 'b) shapeA, ('a, 'b) shapeB) F2B"
datatype ('a, 'b) raw = Cons "(('a, 'b) shapeA, ('a, 'b) shapeB, ('a, 'b) raw, ('a, 'b) raw) G"

abbreviation "un_LeafA u \<equiv> case u of LeafA x \<Rightarrow> x"
abbreviation "un_LeafB u \<equiv> case u of LeafB x \<Rightarrow> x"
abbreviation "un_Node1A u \<equiv> case u of Node1A x \<Rightarrow> x"
abbreviation "un_Node1B u \<equiv> case u of Node1B x \<Rightarrow> x"
abbreviation "un_Node2A u \<equiv> case u of Node2A x \<Rightarrow> x"
abbreviation "un_Node2B u \<equiv> case u of Node2B x \<Rightarrow> x"
abbreviation "un_Cons t \<equiv> case t of Cons x \<Rightarrow> x"*)

bnf_axiomatization ('a, 'b, 'c, 'd) R
bnf_axiomatization ('e, 'f, 'g) DA
bnf_axiomatization ('h, 'i, 'j) DB

consts defobj :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB,
  (('a, 'c) F1A, ('b, 'd) F1A, ('a, 'c) F1B, ('b, 'd) F1B) R,
  (('a, 'c) F2A, ('b, 'd) F2A, ('a, 'c) F2B, ('b, 'd) F2B) R) G \<Rightarrow> ('a, 'b, 'c, 'd) R"
consts argobj1A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1A \<Rightarrow> (('e, 'h) F1A, ('f, 'i) F1A, ('g, 'j) F1A) DA"
consts argobj1B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1B \<Rightarrow> (('e, 'h) F1B, ('f, 'i) F1B, ('g, 'j) F1B) DB"
consts argobj2A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2A \<Rightarrow> (('e, 'h) F2A, ('f, 'i) F2A, ('g, 'j) F2A) DA"
consts argobj2B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2B \<Rightarrow> (('e, 'h) F2B, ('f, 'i) F2B, ('g, 'j) F2B) DB"

axiomatization where
  defobj_transfer: "rel_fun (rel_G (rel_DA RE RF RG) (rel_DB RH RI RJ) (rel_R (rel_F1A RA RC) (rel_F1A RB RD) (rel_F1B RA RC) (rel_F1B RB RD))
    (rel_R (rel_F2A RA RC) (rel_F2A RB RD) (rel_F2B RA RC) (rel_F2B RB RD))) (rel_R RA RB RC RD) defobj defobj" and
  argobj1A_transfer: "rel_fun (rel_F1A (rel_DA RE RF RG) (rel_DB RH RI RJ)) (rel_DA (rel_F1A RE RH) (rel_F1A RF RI) (rel_F1A RG RJ)) argobj1A argobj1A" and
  argobj2A_transfer: "rel_fun (rel_F2A (rel_DA RE RF RG) (rel_DB RH RI RJ)) (rel_DA (rel_F2A RE RH) (rel_F2A RF RI) (rel_F2A RG RJ)) argobj2A argobj2A" and
  argobj1B_transfer: "rel_fun (rel_F1B (rel_DA RE RF RG) (rel_DB RH RI RJ)) (rel_DB (rel_F1B RE RH) (rel_F1B RF RI) (rel_F1B RG RJ)) argobj1B argobj1B" and
  argobj2B_transfer: "rel_fun (rel_F2B (rel_DA RE RF RG) (rel_DB RH RI RJ)) (rel_DB (rel_F2B RE RH) (rel_F2B RF RI) (rel_F2B RG RJ)) argobj2B argobj2B"

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto

lemma R_pred_mono: "P1 \<le> Q1 \<Longrightarrow> P2 \<le> Q2 \<Longrightarrow> P3 \<le> Q3 \<Longrightarrow> P4 \<le> Q4 \<Longrightarrow> pred_R P1 P2 P3 P4 \<le> pred_R Q1 Q2 Q3 Q4"
  using R.pred_mono_strong[of P1 P2 P3 P4 _ Q1 Q2 Q3 Q4] sorry

lemma defobj_invar: "pred_fun (pred_G (pred_DA RE RF RG) (pred_DB RH RI RJ)
  (pred_R (pred_F1A RA RC) (pred_F1A RB RD) (pred_F1B RA RC) (pred_F1B RB RD))
  (pred_R (pred_F2A RA RC) (pred_F2A RB RD) (pred_F2B RA RC) (pred_F2B RB RD))) (pred_R RA RB RC RD) defobj"
  unfolding fun_pred_rel G.rel_eq_onp[symmetric]
    F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric] F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] R.rel_eq_onp[symmetric] by (rule defobj_transfer)

lemma argobj1A_invar: "pred_fun (pred_F1A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_F1A RE RH) (pred_F1A RF RI) (pred_F1A RG RJ)) argobj1A"
  unfolding fun_pred_rel F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] by (rule argobj1A_transfer)
lemma argobj1B_invar: "pred_fun (pred_F1B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_F1B RE RH) (pred_F1B RF RI) (pred_F1B RG RJ)) argobj1B"
  unfolding fun_pred_rel F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] by (rule argobj1B_transfer)
lemma argobj2A_invar: "pred_fun (pred_F2A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_F2A RE RH) (pred_F2A RF RI) (pred_F2A RG RJ)) argobj2A"
  unfolding fun_pred_rel F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] by (rule argobj2A_transfer)
lemma argobj2B_invar: "pred_fun (pred_F2B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_F2B RE RH) (pred_F2B RF RI) (pred_F2B RG RJ)) argobj2B"
  unfolding fun_pred_rel F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] by (rule argobj2B_transfer)

lemma defobj_natural: "defobj (map_G (map_DA fe ff fg) (map_DB fh fi fj)
   (map_R (map_F1A fa fc) (map_F1A fb fd) (map_F1B fa fc) (map_F1B fb fd))
   (map_R (map_F2A fa fc) (map_F2A fb fd) (map_F2B fa fc) (map_F2B fb fd)) x) = map_R fa fb fc fd (defobj x)"
  using rel_funD[OF defobj_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"
    "BNF_Def.Grp UNIV fa" "BNF_Def.Grp UNIV fc" "BNF_Def.Grp UNIV fb" "BNF_Def.Grp UNIV fd"]
  unfolding F1A.rel_Grp F1B.rel_Grp F2A.rel_Grp F2B.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp G.rel_Grp by (auto simp: Grp_def)

lemma argobj1A_natural: "argobj1A (map_F1A (map_DA fe ff fg) (map_DB fh fi fj) x) = map_DA (map_F1A fe fh) (map_F1A ff fi) (map_F1A fg fj) (argobj1A x)"
  using rel_funD[OF argobj1A_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"]
  unfolding F1A.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp by (auto simp: Grp_def)
lemma argobj2A_natural: "argobj2A (map_F2A (map_DA fe ff fg) (map_DB fh fi fj) x) = map_DA (map_F2A fe fh) (map_F2A ff fi) (map_F2A fg fj) (argobj2A x)"
  using rel_funD[OF argobj2A_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"]
  unfolding F2A.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp by (auto simp: Grp_def)
lemma argobj1B_natural: "argobj1B (map_F1B (map_DA fe ff fg) (map_DB fh fi fj) x) = map_DB (map_F1B fe fh) (map_F1B ff fi) (map_F1B fg fj) (argobj1B x)"
  using rel_funD[OF argobj1B_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"]
  unfolding F1B.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp by (auto simp: Grp_def)
lemma argobj2B_natural: "argobj2B (map_F2B (map_DA fe ff fg) (map_DB fh fi fj) x) = map_DB (map_F2B fe fh) (map_F2B ff fi) (map_F2B fg fj) (argobj2B x)"
  using rel_funD[OF argobj2B_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"]
  unfolding F2B.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp by (auto simp: Grp_def)

primrec f_shapeA :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shapeA \<Rightarrow> (('e, 'h) shapeA, ('f, 'i) shapeA, ('g, 'j) shapeA) DA"
    and f_shapeB :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shapeB \<Rightarrow> (('e, 'h) shapeB, ('f, 'i) shapeB, ('g, 'j) shapeB) DB" where
  "f_shapeA (LeafA a) = map_DA LeafA LeafA LeafA a"
| "f_shapeA (Node1A fa) = map_DA Node1A Node1A Node1A (argobj1A (map_F1A f_shapeA f_shapeB fa))"
| "f_shapeA (Node2A fa) = map_DA Node2A Node2A Node2A (argobj2A (map_F2A f_shapeA f_shapeB fa))"
| "f_shapeB (LeafB b) = map_DB LeafB LeafB LeafB b"
| "f_shapeB (Node1B fb) = map_DB Node1B Node1B Node1B (argobj1B (map_F1B f_shapeA f_shapeB fb))"
| "f_shapeB (Node2B fb) = map_DB Node2B Node2B Node2B (argobj2B (map_F2B f_shapeA f_shapeB fb))"

primrec f_raw :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) raw \<Rightarrow>
  (('a, 'c) shapeA, ('b, 'd) shapeA, ('a, 'c) shapeB, ('b, 'd) shapeB) R" where
  "f_raw (Cons g) = defobj (map_G f_shapeA f_shapeB ((map_R un_Node1A un_Node1A un_Node1B un_Node1B) o f_raw)
    ((map_R un_Node2A un_Node2A un_Node2B un_Node2B) o f_raw) g)"

definition f :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) T \<Rightarrow> ('a, 'b, 'c, 'd) R" where
  "f t = map_R un_LeafA un_LeafA un_LeafB un_LeafB (f_raw (Rep_T t))"

lemma f_shapeA_flatNode1A: "f_shapeA (flatNode1A u) = map_DA flatNode1A flatNode1A flatNode1A (f_shapeA (map_shapeA argobj1A argobj1B u))"
  sorry
lemma f_shapeA_flatNode2A: "f_shapeA (flatNode2A u) = map_DA flatNode2A flatNode2A flatNode2A (f_shapeA (map_shapeA argobj2A argobj2B u))"
  sorry
lemma f_shapeB_flatNode1B: "f_shapeB (flatNode1B u) = map_DB flatNode1B flatNode1B flatNode1B (f_shapeB (map_shapeB argobj1A argobj1B u))"
  sorry
lemma f_shapeB_flatNode2B: "f_shapeB (flatNode2B u) = map_DB flatNode2B flatNode2B flatNode2B (f_shapeB (map_shapeB argobj2A argobj2B u))"
  sorry

lemma invar_shapeA_f_shapeA: "invar_shapeA u0 u \<Longrightarrow> pred_DA (invar_shapeA u0) (invar_shapeA u0) (invar_shapeA u0) (f_shapeA u)"
  sorry
lemma invar_shapeB_f_shapeB: "invar_shapeB u0 u \<Longrightarrow> pred_DB (invar_shapeB u0) (invar_shapeB u0) (invar_shapeB u0) (f_shapeB u)"
  sorry

lemma f_raw_flat1: "invar u0 t \<Longrightarrow> f_raw (flat1 t) = map_R flatNode1A flatNode1A flatNode1B flatNode1B (f_raw (map_raw argobj1A argobj1B t))"
  sorry
lemma f_raw_flat2: "invar u0 t \<Longrightarrow> f_raw (flat2 t) = map_R flatNode2A flatNode2A flatNode2B flatNode2B (f_raw (map_raw argobj2A argobj2B t))"
  sorry

lemma invar_f_raw1: "invar u0 x \<Longrightarrow> pred_R (invar_shapeA u0) (invar_shapeA u0) (invar_shapeB u0) (invar_shapeB u0) (f_raw (map_raw argobj1A argobj1B x))"
  sorry
lemma invar_f_raw2: "invar u0 x \<Longrightarrow> pred_R (invar_shapeA u0) (invar_shapeA u0) (invar_shapeB u0) (invar_shapeB u0) (f_raw (map_raw argobj2A argobj2B x))"
  sorry

lemma "f (T g) = defobj (map_G id id (f o map_T argobj1A argobj1B) (f o map_T argobj2A argobj2B) g)"
  unfolding f_def[abs_def] map_T_def T_def o_def
  sorry

hide_const (open) f

section \<open>Induction\<close>

(* Preliminaries *)

lemma bi_unique_Grp: "bi_unique (BNF_Def.Grp A f) = inj_on f A"
  unfolding bi_unique_def Grp_def inj_on_def by auto

(* The "recursion sizes" of elements of 'a raw -- on the second argument of G *)
datatype sz = SCons "(unit, unit, sz, sz) G"

primrec size :: "('a, 'b) raw \<Rightarrow> sz" where
  "size (Cons g) = SCons (map_G (\<lambda>_. ()) (\<lambda>_. ()) size size g)"

lemma size_unflat1: 
  "size (unflat1 ul r) = size (r :: ('a, 'b) raw)"
  apply (induct r arbitrary: ul)
  apply (auto intro: G.map_cong simp only: unflat1.simps size.simps G.map_comp o_apply)
  done

lemma size_unflat2: 
  "size (unflat2 ul r) = size (r :: ('a, 'b) raw)"
  apply (induct r arbitrary: ul)
  apply (auto intro: G.map_cong simp only: unflat2.simps size.simps G.map_comp o_apply)
  done

lemma size_map_raw: 
  "size (map_raw f g r) = size r"
  apply (induct r)
  apply (auto intro: G.map_cong simp only: raw.map size.simps G.map_comp o_apply)
  done

lemma raw_induct_size: 
  assumes "\<And> r :: ('a, 'b) raw.
    (\<And>r'. size r' \<in> set3_G (case size r of SCons x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow>
    (\<And>r'. size r' \<in> set4_G (case size r of SCons x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
  shows "PP (r :: ('a, 'b) raw)"
  apply (induct "size r" arbitrary: r)
  apply (rule assms)
  apply (drule sym)
  apply (simp only: sz.case)
  apply (drule sym)
  apply (simp only: sz.case)
  done


(*  THE ASSUMPTIONS: *)

consts P :: "('a, 'b) T \<Rightarrow> bool"

axiomatization where 
P_ind: "\<And>t. (\<And> t'. t' \<in> set3_G t \<Longrightarrow> P t') \<Longrightarrow> (\<And> t'. t' \<in> set4_G t \<Longrightarrow> P t') \<Longrightarrow> P (T t :: ('a, 'b) T)" and
P_param: "\<And>R S :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_T R S) (=) P P"

lemma P_mono: "inj_on f (set1_T a) \<Longrightarrow> inj_on g (set2_T a) \<Longrightarrow> P (map_T f g a) \<Longrightarrow> P a"
  using P_param[of "BNF_Def.Grp (set1_T a) f" "BNF_Def.Grp (set2_T a) g"]
  unfolding T.rel_Grp rel_fun_def bi_unique_Grp unfolding Grp_def
  apply (auto simp only:
    simp_thms mem_Collect_eq True_implies_equals implies_True_equals all_simps ex_simps)
  done

lemma P_raw: 
  fixes r :: "(('a, 'b) shapeA, ('a, 'b) shapeB) raw"
  shows "invar [] r \<longrightarrow> P (Abs_T r)"
  apply (rule raw_induct_size)
  apply (rule impI)
  apply (rule iffD1[OF arg_cong[of _ _ P, OF T_un_T]])
  apply (rule P_ind)

  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (simp only: un_T_def G.set_map Abs_T_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule P_mono[of Node1A _ Node1B])
   apply (simp only: inj_on_def shapeA.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shapeB.inject simp_thms Ball_def)
  apply (rule raw.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar []"]])
   apply assumption
  apply (simp only: invar.simps raw.case G.pred_set map_T_def o_apply Abs_T_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (auto simp only: size.simps sz.case G.set_map size_map_raw size_unflat1) []
  apply (simp only: invar_map_closed invar_unflat snoc.simps)

  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (simp only: un_T_def G.set_map Abs_T_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule P_mono[of Node2A _ Node2B])
   apply (simp only: inj_on_def shapeA.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shapeB.inject simp_thms Ball_def)
  apply (rule raw.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar []"]])
   apply assumption
  apply (simp only: invar.simps raw.case G.pred_set map_T_def o_apply Abs_T_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (auto simp only: size.simps sz.case G.set_map size_map_raw size_unflat2) []
  apply (simp only: invar_map_closed invar_unflat snoc.simps)
  done

lemma P_end_product: 
  fixes t :: "('a, 'b) T"
  shows "P t"
  apply (rule P_mono[of LeafA _ LeafB])
   apply (simp only: inj_on_def shapeA.inject simp_thms Ball_def P_raw)
   apply (simp only: inj_on_def shapeB.inject simp_thms Ball_def P_raw)
  unfolding map_T_def o_apply
  apply (rule mp[OF P_raw])
  apply (simp only: invar_map_closed Rep_T[unfolded mem_Collect_eq])
  done

end