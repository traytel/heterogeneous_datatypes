theory HPrimrec_Examples
imports Main
begin

section \<open>Some examples\<close>

typedecl 'a plist
consts Cons_plist :: "unit + 'a \<times> ('a \<times> 'a) plist \<Rightarrow> 'a plist"
consts pmap :: "('a \<Rightarrow> 'b) \<Rightarrow> 'a plist \<Rightarrow> 'b plist"
axiomatization where pmap_id: "pmap id = id" 
(*
'a G = unit + 'a \<times> 'b
'a F = 'a \<times> 'a
*)

definition "PNil = Cons_plist (Inl ())"
definition "PCons x xs = Cons_plist (Inr (x, xs))"

(*
hprimrec sum :: "nat plist \<Rightarrow> nat" where
  sum_PNil:  "sum PNil = 0"
| sum_PCons: "sum (PCons x xs) = x + sum (pmap (\<lambda>(x,y). x + y) xs)"
'a D = nat
'b R = nat
*)

definition defobj_sum :: "unit + nat \<times> nat \<Rightarrow> nat" where
  "defobj_sum = case_sum (\<lambda>_. 0) (\<lambda>(x, y). x + y)"
definition argobj_sum :: "nat \<times> nat \<Rightarrow> nat" where
  "argobj_sum = (\<lambda>(x, y). x + y)"
consts sum :: "nat plist \<Rightarrow> nat"
axiomatization where
  sum_rec: "sum (Cons_plist g) = defobj_sum (map_pre_list id (sum o pmap argobj_sum) g)"

lemma sum_PNil:  "sum PNil = 0"
  unfolding PNil_def by (subst sum_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_sum_def o_apply id_apply map_sum.simps sum.case)

lemma sum_PCons:  "sum (PCons x xs) = x + sum (pmap (\<lambda>(x,y). x + y) xs)"
  unfolding PCons_def by (subst sum_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_sum_def argobj_sum_def o_apply id_apply map_sum.simps map_prod_def prod.case sum.case)

(*
hprimrec len :: "'a plist \<Rightarrow> nat" where
  len_PNil:  "len PNil = 0"
| len_PCons: "len (PCons x xs) = 1 + len xs"
'a D = 'a
'b R = nat
*)

definition defobj_len :: "unit + 'a \<times> nat \<Rightarrow> nat" where
  "defobj_len = case_sum (\<lambda>_. 0) (\<lambda>(x, y). 1 + y)"
definition argobj_len :: "'a \<times> 'a \<Rightarrow> 'a \<times> 'a" where
  "argobj_len = id"
consts len :: "'a plist \<Rightarrow> nat"
axiomatization where
  len_rec: "len (Cons_plist g) = defobj_len (map_pre_list id (len o pmap argobj_len) g)"

lemma len_PNil:  "len PNil = 0"
  unfolding PNil_def by (subst len_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_len_def o_apply id_apply map_sum.simps sum.case)

lemma len_PCons: "len (PCons x xs) = 1 + len xs"
  unfolding PCons_def by (subst len_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_len_def argobj_len_def o_apply id_apply map_sum.simps map_prod_def prod.case sum.case pmap_id)

(*
hprimrec size :: "'a plist \<Rightarrow> ('a \<Rightarrow> nat) \<Rightarrow>  nat" where
  size_PNil:  "size PNil s = 0"
| size_PCons: "size (PCons x xs) s = s x + size xs (\<lambda>(x,y). s x + s y)"
'a D = 'a
'b R = ('a \<Rightarrow> nat) \<Rightarrow> nat
*)

definition defobj_size :: "unit + 'a \<times> (('a \<times> 'a \<Rightarrow> nat) \<Rightarrow> nat) \<Rightarrow> ('a \<Rightarrow> nat) \<Rightarrow> nat" where
  "defobj_size = case_sum (\<lambda>_ _. 0) (\<lambda>(x, y) s. s x + y (\<lambda>(x,y). s x + s y))"
definition argobj_size :: "'a \<times> 'a \<Rightarrow> 'a \<times> 'a" where
  "argobj_size = id"
consts size :: "'a plist \<Rightarrow> ('a \<Rightarrow> nat) \<Rightarrow>  nat"
axiomatization where
  size_rec: "size (Cons_plist g) = defobj_size (map_pre_list id (size o pmap argobj_size) g)"

lemma size_PNil:  "size PNil s = 0"
  unfolding PNil_def by (subst size_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_size_def o_apply id_apply map_sum.simps sum.case)

lemma size_PCons: "size (PCons x xs) s = s x + size xs (\<lambda>(x,y). s x + s y)"
  unfolding PCons_def by (subst size_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_size_def argobj_size_def o_apply id_apply map_sum.simps map_prod_def prod.case sum.case pmap_id)

(*
hprimrec mypmap :: "'a plist \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'b plist" where
  mypmap_PNil:  "mypmap PNil f = PNil"
| mypmap_PCons: "mypmap (PCons x xs) f = PCons (f x) (mypmap xs (\<lambda>(x,y). (f x, f y)))"
'a D = 'a
'b R = ('a \<Rightarrow> 'b) \<Rightarrow> 'b plist
*)

definition defobj_mypmap :: "unit + 'a \<times> (('a \<times> 'a \<Rightarrow> 'b \<times> 'b) \<Rightarrow> ('b \<times> 'b) plist) \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'b plist" where
  "defobj_mypmap = case_sum (\<lambda>_ _. PNil) (\<lambda>(x, y) f. PCons (f x) (y (\<lambda>(x,y). (f x, f y))))"
definition argobj_mypmap :: "'a \<times> 'a \<Rightarrow> 'a \<times> 'a" where
  "argobj_mypmap = id"
consts mypmap :: "'a plist \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'b plist"
axiomatization where
  mypmap_rec: "mypmap (Cons_plist g) = defobj_mypmap (map_pre_list id (mypmap o pmap argobj_mypmap) g)"

lemma mypmap_PNil:  "mypmap PNil f = PNil"
  unfolding PNil_def by (subst mypmap_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_mypmap_def o_apply id_apply map_sum.simps sum.case PNil_def)

lemma mypmap_PCons: "mypmap (PCons x xs) f = PCons (f x) (mypmap xs (\<lambda>(x,y). (f x, f y)))"
  unfolding PCons_def by (subst mypmap_rec) (simp only: map_pre_list_def BNF_Composition.id_bnf_def
    defobj_mypmap_def argobj_mypmap_def o_apply id_apply map_sum.simps map_prod_def prod.case sum.case pmap_id PCons_def)

end