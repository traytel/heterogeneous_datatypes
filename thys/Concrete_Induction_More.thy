theory Concrete_Induction_More
imports "../BNF_Nonuniform_Fixpoint"
begin

declare [[bnf_internals]]

ML {*
open BNF_Util
open BNF_Tactics
*}

ML {*
fun mk_ctor_case_prem_tac ctxt P_mono live_arg_abs_inject live_arg_rep_inject live_arg_abs_inverse
    fp_map_comp fp_map_ident fp_inj_map =
  HEADGOAL (rtac ctxt (unfold_thms ctxt @{thms inj_on_def} P_mono RS mp) THEN'
    rtac ctxt ballI THEN' rtac ctxt ballI THEN' rtac ctxt impI THEN'
    etac ctxt (live_arg_abs_inject OF @{thms UNIV_I UNIV_I} RS iffD1) THEN'
    dtac ctxt meta_spec THEN' etac ctxt meta_mp THEN'
    rtac ctxt @{thm inj_image_mem_iff[THEN iffD1]} THEN'
    rtac ctxt fp_inj_map THEN'
    SELECT_GOAL (unfold_thms_tac ctxt @{thms inj_on_def}) THEN'
    rtac ctxt ballI THEN' rtac ctxt ballI THEN' rtac ctxt impI THEN'
    etac ctxt (live_arg_rep_inject RS iffD1) THEN'
    SELECT_GOAL (unfold_thms_tac ctxt [@{thm o_def}, live_arg_abs_inverse OF [UNIV_I], fp_map_comp,
      fp_map_ident]) THEN'
    assume_tac ctxt);
*}

ML {*
fun mk_nonuniform_ind0_tac ctxt pre_pred_set P_ctor =
  HEADGOAL (rtac ctxt @{thm predicate1I} THEN'
    SELECT_GOAL (unfold_thms_tac ctxt (pre_pred_set :: @{thms o_apply Ball_def})) THEN'
    rtac ctxt P_ctor THEN'
    REPEAT_DETERM o clean_blast_tac ctxt);
*}


subsection \<open>Power Lists with Option\<close>

datatype 'a opt = None | Some 'a

nonuniform_datatype 'a polist = PONil | POCons 'a "('a \<times> 'a) polist opt"

definition P1 where
  "P1 xs = (map_polist id xs = xs)"

theorem P1_parametric:
  assumes [transfer_rule]: "bi_unique R" "left_total R"
  shows "rel_fun (rel_polist R) (\<longleftarrow>) P1 P1"
  unfolding P1_def apply transfer_prover_start apply transfer_step+ prefer 2 apply (rule refl)
    apply (auto simp: rel_fun_def Rel_def)
    by (meson assms(1) bi_uniqueDl polist.bi_unique_rel)

theorem P1_PONil: "P1 PONil"
  unfolding P1_def by simp

theorem P1_POCons:
  fixes x xs
  assumes "\<And>x2a. x2a \<in> set_opt xs \<Longrightarrow> P1 x2a"
  shows "P1 (POCons x xs)"
  unfolding P1_def by (simp add: polist.map_id0)

lemma P1_inj_mono: "inj f \<Longrightarrow> P1 (map_polist f t) \<longrightarrow> P1 t"
  using P1_parametric[of "BNF_Def.Grp UNIV f"]
  unfolding bi_unique_Grp left_total_Grp polist.rel_Grp
  by (auto simp: Grp_def rel_fun_def)

lemma type_def_polist_pre_polist: "type_definition Rep_polist_pre_polist Abs_polist_pre_polist UNIV"
  sorry

lemma P1_Ctor0: "(\<And>z. z \<in> set2_pre_polist x \<Longrightarrow> P1 z) \<Longrightarrow>
  P1 (polist.Ctor_polist (map_pre_polist id (map_polist arg.Abs_arg_polist_0_0_pre_polist_0_0) x))"
  apply (insert P1_PONil P1_POCons)
  apply (rotate_tac -2)
  apply (unfold PONil_def POCons_def)
  apply (tactic \<open>HEADGOAL (resolve_tac @{context}
    [BNF_FP_Util.mk_absumprodE @{thm type_def_polist_pre_polist} [0, 2]])\<close>)
  apply (drule DEADID.rel_mono_strong[of x])
   apply hypsubst_thin
   apply (unfold o_def)
   apply assumption
  apply hypsubst_thin
  apply (simp add: map_pre_polist_def Abs_polist_pre_polist_inverse set2_pre_polist_def)
  apply (drule meta_spec)
  apply (rotate_tac -1)
  apply (drule meta_spec)
  apply (erule meta_mp)
  apply (drule meta_spec)
  apply (erule meta_mp)
  apply assumption (* instead of (rule refl) *)
  done

lemma P1_Ctor: "(\<And>z. z \<in> set2_pre_polist x \<Longrightarrow> P1 z) \<Longrightarrow> P1 (polist.Ctor_polist x)"
  apply (rule P1_Ctor0[of "map_pre_polist id (map_polist arg.Rep_arg_polist_0_0_pre_polist_0_0) xs" for xs,
    unfolded pre_polist.map_comp polist.map_comp id_def o_def pre_polist.map_ident polist.map_ident
     arg.Rep_arg_polist_0_0_pre_polist_0_0_inverse pre_polist.set_map])
  apply (tactic \<open>mk_ctor_case_prem_tac @{context} @{thm P1_inj_mono}
    @{thm arg.Abs_arg_polist_0_0_pre_polist_0_0_inject}
    @{thm arg.Rep_arg_polist_0_0_pre_polist_0_0_inject}
    @{thm arg.Abs_arg_polist_0_0_pre_polist_0_0_inverse}
    @{thm polist.map_comp}
    @{thm polist.map_ident}
    @{thm polist.inj_map}\<close>)
  done

lemma P1_ind: "pred_pre_polist (\<lambda>_. True) P1 \<le> P1 o polist.Ctor_polist"
  by (tactic \<open>mk_nonuniform_ind0_tac @{context} @{thm pre_polist.pred_set} @{thm P1_Ctor}\<close>)

local_setup \<open> fn lthy =>
  let
  val (res, lthy) = BNF_NU_FP_Ind.construct_nu_co_ind BNF_Util.Least_FP
      (BNF_NU_FP_Def_Sugar.nu_fp_sugar_of lthy @{type_name polist} |> the |> #nu_fp_res) []
      [@{term P1}] @{thms P1_parametric} (K @{thms P1_ind}) lthy
  in
    lthy
    |> Local_Theory.note ((@{binding result_polist}, []), #co_ind_thms res) |> snd
  end\<close>

theorem "map_polist id xs = xs"
  by (rule result_polist[unfolded P1_def])


subsection \<open>Two Different Instances of Same Type on RHS\<close>

nonuniform_datatype 'a potree = POEmp | PONode 'a "'a potree opt" "('a \<times> 'a) potree opt"

definition P2 where
  "P2 xs = (map_potree id xs = xs)"

theorem P2_parametric:
  assumes [transfer_rule]: "bi_unique R" "left_total R"
  shows "rel_fun (rel_potree R) (\<longleftarrow>) P2 P2"
  unfolding P2_def apply transfer_prover_start apply transfer_step+ prefer 2 apply (rule refl)
    apply (auto simp: rel_fun_def Rel_def)
    by (meson assms(1) bi_uniqueDl potree.bi_unique_rel)

theorem P2_POEmp: "P2 POEmp"
  unfolding P2_def by simp

theorem P2_PONode:
  fixes x xs ys
  assumes "\<And>x2a. x2a \<in> set_opt xs \<Longrightarrow> P2 x2a"
  assumes "\<And>x2a. x2a \<in> set_opt ys \<Longrightarrow> P2 x2a"
  shows "P2 (PONode x xs ys)"
  unfolding P2_def by (simp add: potree.map_id0)

lemma P2_inj_mono: "inj f \<Longrightarrow> P2 (map_potree f t) \<longrightarrow> P2 t"
  using P2_parametric[of "BNF_Def.Grp UNIV f"] unfolding bi_unique_Grp left_total_Grp potree.rel_Grp
  by (auto simp: Grp_def rel_fun_def)

lemma type_def_potree_pre_potree: "type_definition Rep_potree_pre_potree Abs_potree_pre_potree UNIV"
  sorry

lemma P2_Ctor0:
  "(\<And>z. z \<in> set2_pre_potree x \<Longrightarrow> P2 z) \<Longrightarrow>
   (\<And>z. z \<in> set3_pre_potree x \<Longrightarrow> P2 z) \<Longrightarrow>
   P2 (potree.Ctor_potree (map_pre_potree id (map_potree arg.Abs_arg_potree_0_0_pre_potree_0_0)
     (map_potree arg.Abs_arg_potree_1_0_pre_potree_1_0) x))"
  apply (insert P2_POEmp P2_PONode)
  apply (rotate_tac -2)
  apply (unfold POEmp_def PONode_def)
  apply (tactic \<open>HEADGOAL (resolve_tac @{context}
    [BNF_FP_Util.mk_absumprodE @{thm type_def_potree_pre_potree} [0, 3]])\<close>)
  apply (drule DEADID.rel_mono_strong[of x])
   apply hypsubst_thin
   apply (unfold o_def)
   apply assumption
  apply hypsubst_thin
  apply (simp add: map_pre_potree_def Abs_potree_pre_potree_inverse set2_pre_potree_def set3_pre_potree_def)
  apply (drule meta_spec)
  apply (rotate_tac -1)
  apply (drule meta_spec)
  apply (rotate_tac -1)
  apply (drule meta_spec)
  apply (thin_tac "P2 (Concrete_Induction_More.potree.Ctor_potree (Abs_potree_pre_potree (Inl ())))")
  apply (drule meta_mp)
   apply (thin_tac "\<And>z. z \<in> set_opt b \<Longrightarrow> P2 z")
   apply (drule meta_spec, erule meta_mp, assumption)
  apply (drule meta_mp)
   apply (drule meta_spec)
   apply (erule meta_mp)
   apply assumption
  apply assumption (* instead of (rule refl) *)
  done

lemma P2_Ctor:
  "(\<And>z. z \<in> set2_pre_potree x \<Longrightarrow> P2 z) \<Longrightarrow>
   (\<And>z. z \<in> set3_pre_potree x \<Longrightarrow> P2 z) \<Longrightarrow>
   P2 (potree.Ctor_potree x)"
  apply (rule P2_Ctor0[of "map_pre_potree id (map_potree arg.Rep_arg_potree_0_0_pre_potree_0_0)
    (map_potree arg.Rep_arg_potree_1_0_pre_potree_1_0) xs" for xs,
    unfolded pre_potree.map_comp potree.map_comp id_def o_def pre_potree.map_ident potree.map_ident
    arg.Rep_arg_potree_0_0_pre_potree_0_0_inverse arg.Rep_arg_potree_1_0_pre_potree_1_0_inverse
    pre_potree.set_map])
   apply (tactic \<open>mk_ctor_case_prem_tac @{context} @{thm P2_inj_mono}
     @{thm arg.Abs_arg_potree_0_0_pre_potree_0_0_inject}
     @{thm arg.Rep_arg_potree_0_0_pre_potree_0_0_inject}
     @{thm arg.Abs_arg_potree_0_0_pre_potree_0_0_inverse}
     @{thm potree.map_comp}
     @{thm potree.map_ident}
     @{thm potree.inj_map}\<close>)
  apply (tactic \<open>mk_ctor_case_prem_tac @{context} @{thm P2_inj_mono}
    @{thm arg.Abs_arg_potree_1_0_pre_potree_1_0_inject}
    @{thm arg.Rep_arg_potree_1_0_pre_potree_1_0_inject}
    @{thm arg.Abs_arg_potree_1_0_pre_potree_1_0_inverse}
    @{thm potree.map_comp}
    @{thm potree.map_ident}
    @{thm potree.inj_map}\<close>)
  done

lemma P2_ind: "pred_pre_potree (\<lambda>_. True) P2 P2 \<le> P2 o potree.Ctor_potree"
  by (tactic \<open>mk_nonuniform_ind0_tac @{context} @{thm pre_potree.pred_set} @{thm P2_Ctor}\<close>)

local_setup \<open> fn lthy =>
  let
  val (res, lthy) = BNF_NU_FP_Ind.construct_nu_co_ind BNF_Util.Least_FP
      (BNF_NU_FP_Def_Sugar.nu_fp_sugar_of lthy @{type_name potree} |> the |> #nu_fp_res) []
      [@{term P2}] @{thms P2_parametric} (K @{thms P2_ind}) lthy
  in
    lthy
    |> Local_Theory.note ((@{binding result_potree}, []), #co_ind_thms res) |> snd
  end\<close>

theorem "map_potree id xs = xs"
  by (rule result_potree[unfolded P2_def])

end
