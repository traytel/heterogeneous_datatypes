theory HCodatatype_Mutual
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

lemma snoc: "snoc x xs = xs @ [x]"
  by (induct xs) auto

lemma rev_Cons: "rev (x # xs) = snoc x (rev xs)"
  by (auto simp: snoc)

lemma rev_snoc: "rev (snoc x xs) = x # rev xs"
  by (auto simp: snoc)

lemma vimage2p_eq_onp: "inj f \<Longrightarrow> BNF_Def.vimage2p f f (eq_onp S) = eq_onp (S o f)"
  unfolding vimage2p_def eq_onp_def inj_on_def by auto

lemma vimage2pD: "BNF_Def.vimage2p f g R x y \<Longrightarrow> R (f x) (g y)"
  unfolding vimage2p_def .

lemma eq_onpD: "eq_onp S x x \<Longrightarrow> S x"
  unfolding eq_onp_def by simp

lemma bi_unique_Grp: "bi_unique (BNF_Def.Grp A f) = inj_on f A"
  unfolding bi_unique_def Grp_def inj_on_def by auto

lemma bi_unique_eq_onp: "bi_unique (eq_onp P)"
  unfolding eq_onp_def bi_unique_def by simp

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto


section \<open>Input\<close>

declare [[bnf_internals, inductive_internals, typedef_overloaded]]

bnf_axiomatization ('a, 'b) F1A
bnf_axiomatization ('a, 'b) F1B
bnf_axiomatization ('a, 'b) F2A
bnf_axiomatization ('a, 'b) F2B
bnf_axiomatization ('a, 'b) E3A
bnf_axiomatization ('a, 'b) E3B

bnf_axiomatization ('a, 'b, 'x, 'y, 'z) GF [wits: "'b \<Rightarrow> ('a, 'b, 'x, 'y, 'z) GF"]
bnf_axiomatization ('a, 'b, 'x, 'y, 'z) GE [wits: "'b \<Rightarrow> 'y \<Rightarrow> ('a, 'b, 'x, 'y, 'z) GE"]

(*
datatype ('a, 'b) bt =
   L 'b | N "('a + 'a, 'b * 'b) bt" 'a "('a * 'a, 'b) bu"
     and ('a, 'b) bu = X 'a 'b | Y "('a list, 'b list) bt"

('a, 'b, 'x, 'y, 'z) GF = 'b + 'x \<times> 'a \<times> 'z
('a, 'b, 'x, 'y, 'z) GE = 'a \<times> 'b + 'y
('a, 'b) F1A = 'a + 'a
('a, 'b) F2A = 'a list
('a, 'b) F1B = 'b * 'b
('a, 'b) F2B = 'b list
('a, 'b) E3A = 'a * 'a
('a, 'b) E3B = 'b

specs = [(GF, [[F1A, F2A], [F1B, F2B]]), (GE, [[E3A], [E3B]])]
*)

section \<open>Raw Type\<close>

datatype (discs_sels) label = F1 | F2 | E3
type_synonym depth = "label list"
datatype ('a, 'b) shape3A = LeafA 'a
                         | Node1A "(('a, 'b) shape3A, ('a, 'b) shape3B) F1A"
                         | Node2A "(('a, 'b) shape3A, ('a, 'b) shape3B) F2A"
                         | Node3A "(('a, 'b) shape3A, ('a, 'b) shape3B) E3A"
     and ('a, 'b) shape3B = LeafB 'b
                         | Node1B "(('a, 'b) shape3A, ('a, 'b) shape3B) F1B"
                         | Node2B "(('a, 'b) shape3A, ('a, 'b) shape3B) F2B"
                         | Node3B "(('a, 'b) shape3A, ('a, 'b) shape3B) E3B"
codatatype ('a, 'b) rawF = ConsF (UnConsF: "(('a, 'b) shape3A, ('a, 'b) shape3B, ('a, 'b) rawF, ('a, 'b) rawF, ('a, 'b) rawE) GF")
     and ('a, 'b) rawE = ConsE (UnConsE: "(('a, 'b) shape3A, ('a, 'b) shape3B, ('a, 'b) rawF, ('a, 'b) rawF, ('a, 'b) rawE) GE")

abbreviation "un_LeafA u \<equiv> case u of LeafA x \<Rightarrow> x"
abbreviation "un_LeafB u \<equiv> case u of LeafB x \<Rightarrow> x"
abbreviation "un_Node1A u \<equiv> case u of Node1A x \<Rightarrow> x"
abbreviation "un_Node1B u \<equiv> case u of Node1B x \<Rightarrow> x"
abbreviation "un_Node2A u \<equiv> case u of Node2A x \<Rightarrow> x"
abbreviation "un_Node2B u \<equiv> case u of Node2B x \<Rightarrow> x"
abbreviation "un_Node3A u \<equiv> case u of Node3A x \<Rightarrow> x"
abbreviation "un_Node3B u \<equiv> case u of Node3B x \<Rightarrow> x"
abbreviation "un_ConsF t \<equiv> case t of ConsF x \<Rightarrow> x"
abbreviation "un_ConsE t \<equiv> case t of ConsE x \<Rightarrow> x"


section \<open>Invariant\<close>

primrec invar_shape3A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> bool" and invar_shape3B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> bool" where
  "invar_shape3A u0 (LeafA u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape3A u0 (Node1A fa) = (case u0 of label # u0 \<Rightarrow> if label = F1 then pred_F1A (invar_shape3A u0) (invar_shape3B u0) fa else False | _ \<Rightarrow> False)"
| "invar_shape3A u0 (Node2A fa) = (case u0 of label # u0 \<Rightarrow> if label = F2 then pred_F2A (invar_shape3A u0) (invar_shape3B u0) fa else False | _ \<Rightarrow> False)"
| "invar_shape3A u0 (Node3A fa) = (case u0 of label # u0 \<Rightarrow> if label = E3 then pred_E3A (invar_shape3A u0) (invar_shape3B u0) fa else False | _ \<Rightarrow> False)"
| "invar_shape3B u0 (LeafB u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shape3B u0 (Node1B fb) = (case u0 of label # u0 \<Rightarrow> if label = F1 then pred_F1B (invar_shape3A u0) (invar_shape3B u0) fb else False | _ \<Rightarrow> False)"
| "invar_shape3B u0 (Node2B fb) = (case u0 of label # u0 \<Rightarrow> if label = F2 then pred_F2B (invar_shape3A u0) (invar_shape3B u0) fb else False | _ \<Rightarrow> False)"
| "invar_shape3B u0 (Node3B fb) = (case u0 of label # u0 \<Rightarrow> if label = E3 then pred_E3B (invar_shape3A u0) (invar_shape3B u0) fb else False | _ \<Rightarrow> False)"

coinductive invar_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> bool" and invar_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> bool" where
  "pred_GF (invar_shape3A u0) (invar_shape3B u0) (invar_rawF (F1 # u0)) (invar_rawF (F2 # u0)) (invar_rawE (E3 # u0)) g \<Longrightarrow> invar_rawF u0 (ConsF g)"
| "pred_GE (invar_shape3A u0) (invar_shape3B u0) (invar_rawF (F1 # u0)) (invar_rawF (F2 # u0)) (invar_rawE (E3 # u0)) g \<Longrightarrow> invar_rawE u0 (ConsE g)"
monos GF.pred_mono GE.pred_mono

thm invar_rawF_invar_rawE.coinduct rawF_rawE.coinduct

lemmas invar_rawF_simps = invar_rawF.simps[of _ "ConsF _", unfolded ex_simps simp_thms(39,40) rawF.inject]
lemmas invar_rawE_simps = invar_rawE.simps[of _ "ConsE _", unfolded ex_simps simp_thms(39,40) rawE.inject]

section \<open>The Type\<close>

primrec mk_shape :: "depth \<Rightarrow> 'a \<times> 'b \<Rightarrow> ('a, 'b) shape3A \<times> ('a, 'b) shape3B" where
  "mk_shape [] xy = (LeafA (fst xy), LeafB (snd xy))"
| "mk_shape (l # u0) xy = (case l of
    F1 \<Rightarrow> (Node1A (map_F1A (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F1A)),
           Node1B (map_F1B (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F1B)))
  | F2 \<Rightarrow> (Node2A (map_F2A (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F2A)),
           Node2B (map_F2B (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) F2B)))
  | E3 \<Rightarrow> (Node3A (map_E3A (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) E3A)),
           Node3B (map_E3B (\<lambda>_. fst (mk_shape u0 xy)) (\<lambda>_. snd (mk_shape u0 xy)) (undefined :: (unit, unit) E3B))))"

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_Shape_mk_Shape_tac ctxt P u invar_simps mk_shape_simps pred_maps pred_Trues pred_congs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME [P, u]) @{thm list.induct})) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val simp = full_simp_tac (fold Simplifier.add_cong pred_congs
        (fold Splitter.add_split label_splits
        (ss_only (flat [invar_simps, mk_shape_simps, pred_maps, pred_Trues, IHs,
        @{thms simp_thms list.case fstI sndI if_True o_apply}]) ctxt)));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, simp])
    end) ctxt);
end
\<close>
lemma invar_shape3A_mk_shape_raw:
  "invar_shape3A u0 (fst (mk_shape u0 xy)) \<and>
   invar_shape3B u0 (snd (mk_shape u0 xy))"
  apply (tactic \<open>mk_invar_Shape_mk_Shape_tac @{context}
    @{cterm "\<lambda> u0. invar_shape3A u0 (fst (mk_shape u0 xy)) \<and> invar_shape3B u0 (snd (mk_shape u0 xy))"}
    @{cterm u0}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms mk_shape.simps}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms F1A.pred_True F2A.pred_True E3A.pred_True F1B.pred_True F2B.pred_True E3B.pred_True}
    @{thms F1A.pred_cong F2A.pred_cong E3A.pred_cong F1B.pred_cong F2B.pred_cong E3B.pred_cong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule list.induct[of "\<lambda>u0. invar_shape3A u0 (fst (mk_shape u0 xy)) \<and>
   invar_shape3B u0 (snd (mk_shape u0 xy))" u0])
  apply (auto simp:
    F1A.pred_map F2A.pred_map E3A.pred_map
    F1B.pred_map F2B.pred_map E3B.pred_map
    F1A.pred_True F2A.pred_True E3A.pred_True
    F1B.pred_True F2B.pred_True E3B.pred_True
    cong: F1A.pred_cong F2A.pred_cong E3A.pred_cong F1B.pred_cong F2B.pred_cong E3B.pred_cong
    split: label.splits)
  done*)

lemmas invar_shape3A_mk_shape =
  conjunct1[OF invar_shape3A_mk_shape_raw]
  conjunct2[OF invar_shape3A_mk_shape_raw]

definition "witF y = ConsF (wit_GF (snd (mk_shape [] (undefined, y))))"
definition "witE y = ConsE (wit_GE (snd (mk_shape [] (undefined, y)))
   (ConsF (wit_GF (snd (mk_shape [F2] (undefined, y))))))"

lemma invar_witF: "invar_rawF [] (witF y)"
  apply (auto simp only:
    invar_shape3A_mk_shape
    witF_def witE_def invar_rawF_simps invar_rawE_simps
    invar_shape3A.simps invar_shape3B.simps
    invar_shape3A_mk_shape_raw mk_shape.simps snd_conv fst_conv
    GF.pred_map GE.pred_map GF.pred_set GE.pred_set
    GF.set_map GE.set_map list.case dest: GE.wit GF.wit)
  done

lemma invar_witE: "invar_rawE [] (witE y)"
  apply (auto simp only:
    invar_shape3A_mk_shape
    witF_def witE_def invar_rawF_simps invar_rawE_simps
    invar_shape3A.simps invar_shape3B.simps
    invar_shape3A_mk_shape_raw mk_shape.simps snd_conv fst_conv
    GF.pred_map GE.pred_map GF.pred_set GE.pred_set
    GF.set_map GE.set_map list.case label.case refl if_True
    F2B.pred_map F2B.pred_True o_apply cong: F2B.pred_cong dest!: GE.wit GF.wit)
  done

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_typedef_tac ctxt wit_appl invar_wit =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt [NONE, SOME wit_appl] exI) THEN'
    rtac ctxt CollectI THEN' rtac ctxt invar_wit);

end
\<close>
typedef ('a ,'b) T = "{t :: ('a ,'b) rawF. invar_rawF [] t}"
  apply (tactic \<open>mk_typedef_tac @{context}
    @{cterm "witF undefined :: ('a ,'b) rawF"}
    @{thm invar_witF}\<close>)
  done


typedef ('a ,'b) S = "{t :: ('a ,'b) rawE. invar_rawE [] t}"
  apply (tactic \<open>mk_typedef_tac @{context}
    @{cterm "witE undefined :: ('a ,'b) rawE"}
    @{thm invar_witE}\<close>)
  done


section \<open>Flat and Unflat\<close>

primrec (transfer)
  flat_shape1A :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A \<Rightarrow> ('a, 'b) shape3A" and
  flat_shape1B :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B \<Rightarrow> ('a, 'b) shape3B" where
  "flat_shape1A (LeafA f) = Node1A (map_F1A LeafA LeafB f)"
| "flat_shape1A (Node1A fa) = Node1A (map_F1A flat_shape1A flat_shape1B fa)"
| "flat_shape1A (Node2A fa) = Node2A (map_F2A flat_shape1A flat_shape1B fa)"
| "flat_shape1A (Node3A fa) = Node3A (map_E3A flat_shape1A flat_shape1B fa)"
| "flat_shape1B (LeafB f) = Node1B (map_F1B LeafA LeafB f)"
| "flat_shape1B (Node1B fb) = Node1B (map_F1B flat_shape1A flat_shape1B fb)"
| "flat_shape1B (Node2B fb) = Node2B (map_F2B flat_shape1A flat_shape1B fb)"
| "flat_shape1B (Node3B fb) = Node3B (map_E3B flat_shape1A flat_shape1B fb)"

primrec (transfer)
  flat_shape2A :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A \<Rightarrow> ('a, 'b) shape3A" and
  flat_shape2B :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B \<Rightarrow> ('a, 'b) shape3B" where
  "flat_shape2A (LeafA f) = Node2A (map_F2A LeafA LeafB f)"
| "flat_shape2A (Node1A fa) = Node1A (map_F1A flat_shape2A flat_shape2B fa)"
| "flat_shape2A (Node2A fa) = Node2A (map_F2A flat_shape2A flat_shape2B fa)"
| "flat_shape2A (Node3A fa) = Node3A (map_E3A flat_shape2A flat_shape2B fa)"
| "flat_shape2B (LeafB f) = Node2B (map_F2B LeafA LeafB f)"
| "flat_shape2B (Node1B fb) = Node1B (map_F1B flat_shape2A flat_shape2B fb)"
| "flat_shape2B (Node2B fb) = Node2B (map_F2B flat_shape2A flat_shape2B fb)"
| "flat_shape2B (Node3B fb) = Node3B (map_E3B flat_shape2A flat_shape2B fb)"

primrec (transfer)
  flat_shape3A :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A \<Rightarrow> ('a, 'b) shape3A" and
  flat_shape3B :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B \<Rightarrow> ('a, 'b) shape3B" where
  "flat_shape3A (LeafA f) = Node3A (map_E3A LeafA LeafB f)"
| "flat_shape3A (Node1A fa) = Node1A (map_F1A flat_shape3A flat_shape3B fa)"
| "flat_shape3A (Node2A fa) = Node2A (map_F2A flat_shape3A flat_shape3B fa)"
| "flat_shape3A (Node3A fa) = Node3A (map_E3A flat_shape3A flat_shape3B fa)"
| "flat_shape3B (LeafB f) = Node3B (map_E3B LeafA LeafB f)"
| "flat_shape3B (Node1B fb) = Node1B (map_F1B flat_shape3A flat_shape3B fb)"
| "flat_shape3B (Node2B fb) = Node2B (map_F2B flat_shape3A flat_shape3B fb)"
| "flat_shape3B (Node3B fb) = Node3B (map_E3B flat_shape3A flat_shape3B fb)"

primrec (nonexhaustive)
   unflat_shape1A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) shape3A" and
   unflat_shape1B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) shape3B" where
  "unflat_shape1A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_F1A un_LeafA un_LeafB fa)
      | _ # u0 \<Rightarrow> Node1A (map_F1A (unflat_shape1A u0) (unflat_shape1B u0) fa))"
| "unflat_shape1A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2A (map_F2A (unflat_shape1A u0) (unflat_shape1B u0) fa))"
| "unflat_shape1A u0 (Node3A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node3A (map_E3A (unflat_shape1A u0) (unflat_shape1B u0) fa))"
| "unflat_shape1B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_F1B un_LeafA un_LeafB fb)
      | _ # u0 \<Rightarrow> Node1B (map_F1B (unflat_shape1A u0) (unflat_shape1B u0) fb))"
| "unflat_shape1B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2B (map_F2B (unflat_shape1A u0) (unflat_shape1B u0) fb))"
| "unflat_shape1B u0 (Node3B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node3B (map_E3B (unflat_shape1A u0) (unflat_shape1B u0) fb))"

primrec (nonexhaustive)
   unflat_shape2A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) shape3A" and
   unflat_shape2B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) shape3B" where
  "unflat_shape2A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1A (map_F1A (unflat_shape2A u0) (unflat_shape2B u0) fa))"
| "unflat_shape2A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_F2A un_LeafA un_LeafB fa)
      | _ # u0 \<Rightarrow> Node2A (map_F2A (unflat_shape2A u0) (unflat_shape2B u0) fa))"
| "unflat_shape2A u0 (Node3A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node3A (map_E3A (unflat_shape2A u0) (unflat_shape2B u0) fa))"
| "unflat_shape2B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1B (map_F1B (unflat_shape2A u0) (unflat_shape2B u0) fb))"
| "unflat_shape2B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_F2B un_LeafA un_LeafB fb)
      | _ # u0 \<Rightarrow> Node2B (map_F2B (unflat_shape2A u0) (unflat_shape2B u0) fb))"
| "unflat_shape2B u0 (Node3B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node3B (map_E3B (unflat_shape2A u0) (unflat_shape2B u0) fb))"

primrec (nonexhaustive)
   unflat_shape3A :: "depth \<Rightarrow> ('a, 'b) shape3A \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) shape3A" and
   unflat_shape3B :: "depth \<Rightarrow> ('a, 'b) shape3B \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) shape3B" where
  "unflat_shape3A u0 (Node1A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1A (map_F1A (unflat_shape3A u0) (unflat_shape3B u0) fa))"
| "unflat_shape3A u0 (Node2A fa) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2A (map_F2A (unflat_shape3A u0) (unflat_shape3B u0) fa))"
| "unflat_shape3A u0 (Node3A fa) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_E3A un_LeafA un_LeafB fa)
      | _ # u0 \<Rightarrow> Node3A (map_E3A (unflat_shape3A u0) (unflat_shape3B u0) fa))"
| "unflat_shape3B u0 (Node1B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node1B (map_F1B (unflat_shape3A u0) (unflat_shape3B u0) fb))"
| "unflat_shape3B u0 (Node2B fb) =
      (case u0 of
        [] \<Rightarrow> undefined
      | _ # u0 \<Rightarrow> Node2B (map_F2B (unflat_shape3A u0) (unflat_shape3B u0) fb))"
| "unflat_shape3B u0 (Node3B fb) =
      (case u0 of
        [] \<Rightarrow> LeafB (map_E3B un_LeafA un_LeafB fb)
      | _ # u0 \<Rightarrow> Node3B (map_E3B (unflat_shape3A u0) (unflat_shape3B u0) fb))"

primcorec (transfer) flat1_rawF :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF \<Rightarrow> ('a, 'b) rawF"
               and flat1_rawE :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE \<Rightarrow> ('a, 'b) rawE" where
  "flat1_rawF r = ConsF (map_GF flat_shape1A flat_shape1B flat1_rawF flat1_rawF flat1_rawE (un_ConsF r))"
| "flat1_rawE r = ConsE (map_GE flat_shape1A flat_shape1B flat1_rawF flat1_rawF flat1_rawE (un_ConsE r))"

primcorec (transfer) flat2_rawF :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF \<Rightarrow> ('a, 'b) rawF"
               and flat2_rawE :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE \<Rightarrow> ('a, 'b) rawE" where
  "flat2_rawF r = ConsF (map_GF flat_shape2A flat_shape2B flat2_rawF flat2_rawF flat2_rawE (un_ConsF r))"
| "flat2_rawE r = ConsE (map_GE flat_shape2A flat_shape2B flat2_rawF flat2_rawF flat2_rawE (un_ConsE r))"

primcorec (transfer) flat3_rawF :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF \<Rightarrow> ('a, 'b) rawF"
               and flat3_rawE :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE \<Rightarrow> ('a, 'b) rawE" where
  "flat3_rawF r = ConsF (map_GF flat_shape3A flat_shape3B flat3_rawF flat3_rawF flat3_rawE (un_ConsF r))"
| "flat3_rawE r = ConsE (map_GE flat_shape3A flat_shape3B flat3_rawF flat3_rawF flat3_rawE (un_ConsE r))"

lemmas flat1_simps = flat1_rawF.ctr[of "ConsF z" for z, unfolded rawF.sel] flat1_rawE.ctr[of "ConsE z" for z, unfolded rawE.sel]
lemmas flat2_simps = flat2_rawF.ctr[of "ConsF z" for z, unfolded rawF.sel] flat2_rawE.ctr[of "ConsE z" for z, unfolded rawE.sel]
lemmas flat3_simps = flat3_rawF.ctr[of "ConsF z" for z, unfolded rawF.sel] flat3_rawE.ctr[of "ConsE z" for z, unfolded rawE.sel]

primcorec unflat1_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) rawF"
    and unflat1_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> (('a, 'b) F1A, ('a, 'b) F1B) rawE" where
  "unflat1_rawF u0 r = ConsF (map_GF (unflat_shape1A u0) (unflat_shape1B u0) (unflat1_rawF (F1 # u0)) (unflat1_rawF (F2 # u0)) (unflat1_rawE (E3 # u0)) (un_ConsF r))"
| "unflat1_rawE u0 r = ConsE (map_GE (unflat_shape1A u0) (unflat_shape1B u0) (unflat1_rawF (F1 # u0)) (unflat1_rawF (F2 # u0)) (unflat1_rawE (E3 # u0)) (un_ConsE r))"

primcorec unflat2_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) rawF"
    and unflat2_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> (('a, 'b) F2A, ('a, 'b) F2B) rawE" where
  "unflat2_rawF u0 r = ConsF (map_GF (unflat_shape2A u0) (unflat_shape2B u0) (unflat2_rawF (F1 # u0)) (unflat2_rawF (F2 # u0)) (unflat2_rawE (E3 # u0)) (un_ConsF r))"
| "unflat2_rawE u0 r = ConsE (map_GE (unflat_shape2A u0) (unflat_shape2B u0) (unflat2_rawF (F1 # u0)) (unflat2_rawF (F2 # u0)) (unflat2_rawE (E3 # u0)) (un_ConsE r))"

primcorec unflat3_rawF :: "depth \<Rightarrow> ('a, 'b) rawF \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) rawF"
    and unflat3_rawE :: "depth \<Rightarrow> ('a, 'b) rawE \<Rightarrow> (('a, 'b) E3A, ('a, 'b) E3B) rawE" where
  "unflat3_rawF u0 r = ConsF (map_GF (unflat_shape3A u0) (unflat_shape3B u0) (unflat3_rawF (F1 # u0)) (unflat3_rawF (F2 # u0)) (unflat3_rawE (E3 # u0)) (un_ConsF r))"
| "unflat3_rawE u0 r = ConsE (map_GE (unflat_shape3A u0) (unflat_shape3B u0) (unflat3_rawF (F1 # u0)) (unflat3_rawF (F2 # u0)) (unflat3_rawE (E3 # u0)) (un_ConsE r))"

lemmas unflat1_simps = unflat1_rawF.ctr[of _ "ConsF z" for z, unfolded rawF.sel] unflat1_rawE.ctr[of _ "ConsE z" for z, unfolded rawE.sel]
lemmas unflat2_simps = unflat2_rawF.ctr[of _ "ConsF z" for z, unfolded rawF.sel] unflat2_rawE.ctr[of _ "ConsE z" for z, unfolded rawE.sel]
lemmas unflat3_simps = unflat3_rawF.ctr[of _ "ConsF z" for z, unfolded rawF.sel] unflat3_rawE.ctr[of _ "ConsE z" for z, unfolded rawE.sel]

section \<open>Constructor and Selector\<close>

definition T :: "('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GF \<Rightarrow>
   ('a, 'b) T" where
  "T g = Abs_T (ConsF (map_GF LeafA LeafB (flat1_rawF o Rep_T) (flat2_rawF o Rep_T) (flat3_rawE o Rep_S) g))"

definition un_T :: "('a, 'b) T \<Rightarrow>
  ('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GF" where
  "un_T t = map_GF un_LeafA un_LeafB (Abs_T o unflat1_rawF []) (Abs_T o unflat2_rawF []) (Abs_S o unflat3_rawE []) (un_ConsF (Rep_T t))"

definition S :: "('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GE \<Rightarrow>
   ('a, 'b) S" where
  "S g = Abs_S (ConsE (map_GE LeafA LeafB (flat1_rawF o Rep_T) (flat2_rawF o Rep_T) (flat3_rawE o Rep_S) g))"

definition un_S :: "('a, 'b) S \<Rightarrow>
  ('a, 'b, (('a, 'b) F1A, ('a, 'b) F1B) T, (('a, 'b) F2A, ('a, 'b) F2B) T, (('a, 'b) E3A, ('a, 'b) E3B) S) GE" where
  "un_S s = map_GE un_LeafA un_LeafB (Abs_T o unflat1_rawF []) (Abs_T o unflat2_rawF []) (Abs_S o unflat3_rawE []) (un_ConsE (Rep_S s))"


section \<open>BNF Instance\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_map_Shape_closed_tac ctxt induct Ps us
    Shape_map_thms invar_simps pred_maps pred_congs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI :: map (fn thm => refl RS thm) pred_congs);
      val elim = eresolve_tac ctxt [conjE, impE, exE];
      val simp = full_simp_tac
        (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits})
        (ss_only (flat [Shape_map_thms, invar_simps, pred_maps,
            @{thms o_apply simp_thms list.inject list.distinct}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma invar_shape_map_closed_raw:
  "(\<forall>u0. invar_shape3A u0 (map_shape3A f g ua) \<longleftrightarrow> invar_shape3A u0 ua) \<and>
   (\<forall>u0. invar_shape3B u0 (map_shape3B f g ub) \<longleftrightarrow> invar_shape3B u0 ub)"
  apply (tactic \<open>mk_invar_map_Shape_closed_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua. \<forall>u0. invar_shape3A u0 (map_shape3A f g ua) \<longleftrightarrow> invar_shape3A u0 ua"},
     @{cterm "\<lambda>ub. \<forall>u0. invar_shape3B u0 (map_shape3B f g ub) \<longleftrightarrow> invar_shape3B u0 ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms shape3A.map shape3B.map}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_cong F1B.pred_cong F2A.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 (map_shape3A f g ua) \<longleftrightarrow> invar_shape3A u0 ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 (map_shape3B f g ub) \<longleftrightarrow> invar_shape3B u0 ub" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits if_splits)
  done*)

lemmas invar_shape_map_closed =
  spec[OF conjunct1[OF invar_shape_map_closed_raw]]
  spec[OF conjunct2[OF invar_shape_map_closed_raw]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_map_raw_closed_tac ctxt induct Ps us
    map_thms invar_simps pred_maps invar_map_Shape_closed_thms pred_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI :: map (fn thm => refl RS thm) pred_congs);
      val simp = full_simp_tac
        (ss_only (flat [map_thms, invar_simps, pred_maps, invar_map_Shape_closed_thms, @{thms o_apply}])
          ctxt);
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma invar_map_closed_raw:
  "(invar_rawF u0 (map_rawF f g t) = invar_rawF u0 t) \<and>
   (invar_rawE u0 (map_rawE f g s) = invar_rawE u0 s)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_invar_map_raw_closed_tac' ctxt Ps Qs induct raw_exhausts raw_injects raw_maps
          invar_simps invar_shape_map_closed
          G_pred_maps G_pred_mono_strongs =
          let
            val n = length Ps;
          in
            HEADGOAL (EVERY' [rtac ctxt rev_mp,
              rtac ctxt (infer_instantiate' ctxt (map SOME Ps) induct)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 1)] exhaust;
                  val elim = eresolve_tac ctxt G_pred_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_injects, raw_maps, invar_simps,
                    invar_shape_map_closed, G_pred_maps, @{thms simp_thms ex_simps o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (EVERY' [rtac ctxt rev_mp,
              rtac ctxt (infer_instantiate' ctxt (map SOME Qs) induct)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (conjI :: exI :: disjI1 :: []);
                  val elim = eresolve_tac ctxt G_pred_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_maps, invar_simps,
                    invar_shape_map_closed, G_pred_maps, @{thms o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            unfold_tac ctxt @{thms ex_simps simp_thms(39,40)} THEN
            HEADGOAL (EVERY' [rtac ctxt impI, rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i),
                dtac ctxt (mk_conjunctN n i),
                rtac ctxt iffI,

                etac ctxt thin_rl,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt,

                dtac ctxt asm_rl,
                etac ctxt thin_rl,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)])
          end
      in
        mk_invar_map_raw_closed_tac' @{context}
          [@{cterm "\<lambda>x. \<lambda>y :: ('c, 'd) rawF. \<exists>u0 t. x = u0 \<and> y = t \<and> invar_rawF u0 (map_rawF f g t)"},
           @{cterm "\<lambda>x. \<lambda>y :: ('c, 'd) rawE. \<exists>u0 s. x = u0 \<and> y = s \<and> invar_rawE u0 (map_rawE f g s)"}]
          [@{cterm "\<lambda>x. \<lambda>y :: ('a, 'b) rawF. \<exists>u0 t. x = u0 \<and> y = map_rawF f g t \<and> invar_rawF u0 t"},
           @{cterm "\<lambda>x. \<lambda>y :: ('a, 'b) rawE. \<exists>u0 s. x = u0 \<and> y = map_rawE f g s \<and> invar_rawE u0 s"}]
          @{thm invar_rawF_invar_rawE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.inject rawE.inject}
          @{thms rawF.map rawE.map}
          @{thms invar_rawF_simps invar_rawE_simps}
          @{thms invar_shape_map_closed}
          @{thms GF.pred_map GE.pred_map}
          @{thms GF.pred_mono_strong GE.pred_mono_strong}
      end\<close>)

(*
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = u0 \<and> y = t \<and> invar_rawF u0 (map_rawF f g t)"
    "\<lambda>x y. \<exists>u0 s. x = u0 \<and> y = s \<and> invar_rawE u0 (map_rawE f g s)"])

  subgoal for _ s
  apply (rule rawF.exhaust[of s])
  apply (auto simp only: ex_simps simp_thms(39,40) o_apply rawF.inject rawF.map invar_rawF_simps
      GF.pred_map invar_shape_map_closed
    elim!: GF.pred_mono_strong)
  done

  subgoal for _ t
  apply (rule rawE.exhaust[of t])
  apply (auto simp only: ex_simps simp_thms(39,40) o_apply rawE.inject rawE.map invar_rawE_simps
      GE.pred_map invar_shape_map_closed
    elim!: GE.pred_mono_strong)
  done
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = u0 \<and> y = map_rawF f g t \<and> invar_rawF u0 t"
    "\<lambda>x y. \<exists>u0 s. x = u0 \<and> y = map_rawE f g s \<and> invar_rawE u0 s"])

  apply (erule conjE exE)+
  subgoal for _ _ _ t
  apply (rule rawF.exhaust[of t])
  apply (auto simp: invar_rawF_simps GF.pred_map invar_shape_map_closed
    elim!: GF.pred_mono_strong)
  done

  apply (erule conjE exE)+
  subgoal for _ _ _ s
  apply (rule rawE.exhaust[of s])
  apply (auto simp: invar_rawE_simps GE.pred_map invar_shape_map_closed
    elim!: GE.pred_mono_strong)
  done

  apply (unfold ex_simps simp_thms(39,40))
  apply (rule impI)
  apply (rule impI)
  apply (rule conjI)

  apply (drule conjunct1)
  apply (drule conjunct1)
  apply (rule iffI)

  apply (erule thin_rl)
  apply (erule allE mp)+
  apply (assumption)

  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (erule allE mp)+
  apply (rule exI conjI refl)+
  apply (assumption)

  apply (drule conjunct2)
  apply (drule conjunct2)
  apply (rule iffI)

  apply (erule thin_rl)
  apply (erule allE mp)+
  apply (assumption)

  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (erule allE mp)+
  apply (rule exI conjI refl)+
  apply (assumption)
  done

  apply (tactic \<open>mk_invar_map_raw_closed_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t. \<forall>u0. invar_rawF u0 (map_rawF f g t) \<longleftrightarrow> invar_rawF u0 t"},
     @{cterm "\<lambda>s. \<forall>u0. invar_rawE u0 (map_rawE f g s) \<longleftrightarrow> invar_rawE u0 s"}]
    [@{cterm t}, @{cterm s}]
    @{thms rawF.map rawE.map}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms GF.pred_map GE.pred_map}
    @{thms invar_shape_map_closed}
    @{thms GF.pred_cong GE.pred_cong}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 (map_rawF f g t) \<longleftrightarrow> invar_rawF u0 t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 (map_rawE f g s) \<longleftrightarrow> invar_rawE u0 s" t s])
  apply (simp only:
      rawF.map invar_rawF.simps rawE.map invar_rawE.simps id_apply o_apply
      GF.pred_map GE.pred_map invar_shape_map_closed |
    intro allI GF.pred_cong GE.pred_cong)+
  done*)

lemmas invar_map_closed =
  conjunct1[OF invar_map_closed_raw]
  conjunct2[OF invar_map_closed_raw]

ML \<open>
local
open Ctr_Sugar_Util
in
fun mk_lift_tac ctxt thm =
  HEADGOAL (full_simp_tac (ss_only (thm :: @{thms mem_Collect_eq}) ctxt));
(*call twice with invar_raw_map_closeds and nwits times with invar_wits*)
end
\<close>

lift_bnf ('a, 'b) T [wits: (*"witF :: 'b \<Rightarrow> ('a, 'b) rawF"*)]
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(1)}\<close>)
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(1)}\<close>)
(*
  apply (tactic \<open>mk_lift_tac @{context} @{thms invar_witF}\<close>)
  apply (auto simp only: witF_def witE_def rawF.set shape3B.simps mk_shape.simps snd_conv dest!: GF.wit) []
  apply (auto simp only: witF_def witE_def rawF.set shape3B.simps mk_shape.simps snd_conv dest!: GF.wit) []
*)
  done
(*  apply (simp only:
      invar_map_closed mem_Collect_eq)+
  done*)


lift_bnf ('a, 'b) S (*[wits: "witE :: 'b \<Rightarrow> ('a, 'b) rawE"]*)
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(2)}\<close>)
  apply (tactic \<open>mk_lift_tac @{context} @{thm invar_map_closed(2)}\<close>)
(*
  apply (tactic \<open>mk_lift_tac @{context} @{thms invar_witE}\<close>)
  apply (auto simp only: witF_def witE_def rawE.set rawF.set shape3A.simps shape3B.simps mk_shape.simps fst_conv snd_conv label.case F2B.set_map dest!: GF.wit GE.wit) []
  apply (auto simp only: witF_def witE_def rawE.set shape3B.simps mk_shape.simps snd_conv dest!: GE.wit) []*)
  done


section \<open>Lemmas about Flat, Unflat, Invar\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_Shape_depth_iff_leaf_tac ctxt exhaust P u invar_simps inject distinct =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME [u, P]) exhaust)) THEN
  ALLGOALS
    (let
      val simp = CHANGED o full_simp_tac
        (ss_only (flat [invar_simps, inject, distinct, @{thms simp_thms list.case}]) ctxt);
    in
      REPEAT_DETERM o FIRST' [hyp_subst_tac ctxt, simp]
    end);

fun mk_invar_Shape_depth_iff_node_tac ctxt exhaust P u invar_simps inject distinct
    pred_maps label_case label_distinct =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME [u, P]) exhaust)) THEN
  ALLGOALS
    (let
      val simp = CHANGED o full_simp_tac
        (ss_only (flat [invar_simps, inject, distinct, pred_maps, label_case,
        label_distinct, @{thms simp_thms list.case if_True if_False}]) ctxt);
    in
      REPEAT_DETERM o FIRST' [hyp_subst_tac ctxt, simp]
    end);
end
\<close>

lemma invar_shape3A_depth_iff:
  "invar_shape3A [] x = (\<exists>a. x = LeafA a)"
  "invar_shape3A (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3A (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3A (E3 # u0) x = (\<exists>y. x = Node3A y \<and> pred_E3A (invar_shape3A u0) (invar_shape3B u0) y)"
  apply (tactic \<open>mk_invar_Shape_depth_iff_leaf_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A [] x = (\<exists>a. x = LeafA a)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}\<close>)
  apply (tactic \<open>mk_invar_Shape_depth_iff_node_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shape3A u0) (invar_shape3B u0) y)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms label.case}
    @{thms label.distinct}\<close>)
  apply (tactic \<open>mk_invar_Shape_depth_iff_node_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shape3A u0) (invar_shape3B u0) y)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms label.case}
    @{thms label.distinct}\<close>)
  apply (tactic \<open>mk_invar_Shape_depth_iff_node_tac @{context}
    @{thm shape3A.exhaust}
    @{cterm "invar_shape3A (E3 # u0) x = (\<exists>y. x = Node3A y \<and> pred_E3A (invar_shape3A u0) (invar_shape3B u0) y)"}
    @{cterm x}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.distinct shape3B.distinct}
    @{thms F1A.pred_map F2A.pred_map E3A.pred_map F1B.pred_map F2B.pred_map E3B.pred_map}
    @{thms label.case}
    @{thms label.distinct}\<close>)
(*  apply (rule shape3A.exhaust[of x "invar_shape3A [] x = (\<exists>a. x = LeafA a)"];
    simp only: invar_shape3A.simps shape3A.inject shape3A.distinct simp_thms list.case)
  apply (rule shape3A.exhaust[of x "invar_shape3A (F1 # u0) x = (\<exists>y. x = Node1A y \<and> pred_F1A (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3A.simps F1A.pred_map shape3A.inject shape3A.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3A.exhaust[of x "invar_shape3A (F2 # u0) x = (\<exists>y. x = Node2A y \<and> pred_F2A (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3A.simps F2A.pred_map shape3A.inject shape3A.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3A.exhaust[of x "invar_shape3A (E3 # u0) x = (\<exists>y. x = Node3A y \<and> pred_E3A (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3A.simps E3A.pred_map shape3A.inject shape3A.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
 *) done

lemma invar_shape3B_depth_iff:
  "invar_shape3B [] x = (\<exists>b. x = LeafB b)"
  "invar_shape3B (F1 # u0) x = (\<exists>y. x = Node1B y \<and> pred_F1B (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3B (F2 # u0) x = (\<exists>y. x = Node2B y \<and> pred_F2B (invar_shape3A u0) (invar_shape3B u0) y)"
  "invar_shape3B (E3 # u0) x = (\<exists>y. x = Node3B y \<and> pred_E3B (invar_shape3A u0) (invar_shape3B u0) y)"
  apply (rule shape3B.exhaust[of x "invar_shape3B [] x = (\<exists>a. x = LeafB a)"];
    simp only: invar_shape3B.simps shape3B.inject shape3B.distinct simp_thms list.case)
  apply (rule shape3B.exhaust[of x "invar_shape3B (F1 # u0) x = (\<exists>y. x = Node1B y \<and> pred_F1B (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3B.simps F1B.pred_map shape3B.inject shape3B.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3B.exhaust[of x "invar_shape3B (F2 # u0) x = (\<exists>y. x = Node2B y \<and> pred_F2B (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3B.simps F2B.pred_map shape3B.inject shape3B.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  apply (rule shape3B.exhaust[of x "invar_shape3B (E3 # u0) x = (\<exists>y. x = Node3B y \<and> pred_E3B (invar_shape3A u0) (invar_shape3B u0) y)"];
    simp only: invar_shape3B.simps E3B.pred_map shape3B.inject shape3B.distinct label.case id_o simp_thms list.case label.distinct if_True if_splits)
  done

declare
  atomize_all[symmetric, rulify]
  atomize_imp[symmetric, rulify]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_flat_shape_unflat_shape_tac ctxt induct Ps us
    label_exhaust label_splits label_distincts shape_cases shape_injects invar_shape_depth_iffs
    unflat_simps flat_simps invar_simps
    pred_maps map_comps map_cong_preds map_idents pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val case_distinction_label = SOLVED'
        (rtac ctxt label_exhaust THEN_ALL_NEW
          (TRY o (etac ctxt notE THEN_ALL_NEW assume_tac ctxt)));
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) map_cong_preds map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: pred_mono_strongs);
      val simp = full_simp_tac (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits})
        (ss_only (flat [shape_cases, shape_injects, invar_shape_depth_iffs,
            unflat_simps, flat_simps, invar_simps, pred_maps, map_comps, label_distincts,
            @{thms snoc.simps snoc_neq_Nil id_apply o_apply simp_thms
              list.inject de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, case_distinction_label, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma flat_shape1_unflat_shape1_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> flat_shape1A (unflat_shape1A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> flat_shape1B (unflat_shape1B u0 ub) = ub)"
  apply (tactic \<open>mk_flat_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> flat_shape1A (unflat_shape1A u0 ua) = ua"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> flat_shape1B (unflat_shape1B u0 ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thm label.exhaust}
    @{thms label.splits}
    @{thms label.distinct}
    @{thms shape3A.case shape3B.case}
    @{thms shape3A.inject shape3B.inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms unflat_shape1A.simps unflat_shape1B.simps}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}\<close>)
  done
(*
  apply (auto simp only:
    unflat_shape1A.simps unflat_shape1B.simps flat_shape1A.simps flat_shape1B.simps
    invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
    shape3A.case shape3B.case invar_shape3A_depth_iff invar_shape3B_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits) [2]
  done
*)

lemma flat_shape2_unflat_shape2_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> flat_shape2A (unflat_shape2A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> flat_shape2B (unflat_shape2B u0 ub) = ub)"
  apply (tactic \<open>mk_flat_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> flat_shape2A (unflat_shape2A u0 ua) = ua"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> flat_shape2B (unflat_shape2B u0 ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thm label.exhaust}
    @{thms label.splits}
    @{thms label.distinct}
    @{thms shape3A.case shape3B.case}
    @{thms shape3A.inject shape3B.inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms unflat_shape2A.simps unflat_shape2B.simps}
    @{thms flat_shape2A.simps flat_shape2B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> flat_shape2A (unflat_shape2A u0 ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> flat_shape2B (unflat_shape2B u0 ub) = ub" ua ub])
  apply (auto simp only:
    unflat_shape2A.simps unflat_shape2B.simps flat_shape2A.simps flat_shape2B.simps
    invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
    shape3A.case shape3B.case invar_shape3A_depth_iff invar_shape3B_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits)
  done
*)

lemma flat_shape3_unflat_shape3_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> flat_shape3A (unflat_shape3A u0 ua) = ua) \<and>
   (\<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> flat_shape3B (unflat_shape3B u0 ub) = ub)"
  apply (tactic \<open>mk_flat_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> flat_shape3A (unflat_shape3A u0 ua) = ua"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> flat_shape3B (unflat_shape3B u0 ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thm label.exhaust}
    @{thms label.splits}
    @{thms label.distinct}
    @{thms shape3A.case shape3B.case}
    @{thms shape3A.inject shape3B.inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms unflat_shape3A.simps unflat_shape3B.simps}
    @{thms flat_shape3A.simps flat_shape3B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> flat_shape3A (unflat_shape3A u0 ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> flat_shape3B (unflat_shape3B u0 ub) = ub" ua ub])
  apply (auto simp only:
    unflat_shape3A.simps unflat_shape3B.simps flat_shape3A.simps flat_shape3B.simps
    invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
    F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
    F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
    shape3A.case shape3B.case invar_shape3A_depth_iff invar_shape3B_depth_iff
    id_apply o_apply
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits)
  done
*)

lemmas flat_shape_unflat_shape =
  mp[OF spec[OF conjunct1[OF flat_shape1_unflat_shape1_raw]]]
  mp[OF spec[OF conjunct2[OF flat_shape1_unflat_shape1_raw]]]
  mp[OF spec[OF conjunct1[OF flat_shape2_unflat_shape2_raw]]]
  mp[OF spec[OF conjunct2[OF flat_shape2_unflat_shape2_raw]]]
  mp[OF spec[OF conjunct1[OF flat_shape3_unflat_shape3_raw]]]
  mp[OF spec[OF conjunct2[OF flat_shape3_unflat_shape3_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in
fun mk_unflat_shape_flat_shape_tac ctxt induct Ps us
    shape_injects unflat_simps flat_simps invar_simps
    F_pred_maps F_map_comps F_pred_Trues Shape_case F_map_cong_preds F_map_idents
    F_pred_mono_strongs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) F_map_cong_preds F_map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: F_pred_mono_strongs);
      val simp = full_simp_tac
        (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits sum.splits})
        (ss_only (flat [Shape_case, shape_injects,
            unflat_simps, flat_simps, invar_simps, F_pred_maps, F_map_comps, F_pred_Trues,
            @{thms id_apply o_apply refl simp_thms list.simps
              list.inject de_Morgan_conj de_Morgan_disj triv_forall_equality thin_rl}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
        FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma unflat_shape1_flat_shape1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape1A u0 (flat_shape1A ua) = ua) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape1B u0 (flat_shape1B ub) = ub)"
  apply (tactic \<open>mk_unflat_shape_flat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: (('a, 'b) F1A, ('a, 'b) F1B) shape3A. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape1A u0 (flat_shape1A ua) = ua"},
     @{cterm "\<lambda>ub :: (('a, 'b) F1A, ('a, 'b) F1B) shape3B. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape1B u0 (flat_shape1B ub) = ub"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms shape3A.inject shape3B.inject}
    @{thms unflat_shape1A.simps unflat_shape1B.simps}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.pred_True F1B.pred_True F2A.pred_True F2B.pred_True E3A.pred_True E3B.pred_True}
    @{thms shape3A.case shape3B.case}
    @{thms F1A.map_cong_pred F1B.map_cong_pred F2A.map_cong_pred F2B.map_cong_pred E3A.map_cong_pred E3B.map_cong_pred}
    @{thms F1A.map_ident F1B.map_ident F2A.map_ident F2B.map_ident E3A.map_ident E3B.map_ident}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape1A u0 (flat_shape1A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape1B u0 (flat_shape1B ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shape1A.simps unflat_shape1B.simps flat_shape1A.simps flat_shape1B.simps
      invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      shape3A.case shape3B.case
      id_apply o_apply refl
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits)
  done*)

lemma unflat_shape2_flat_shape2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape2A u0 (flat_shape2A ua) = ua) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape2B u0 (flat_shape2B ub) = ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape2A u0 (flat_shape2A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape2B u0 (flat_shape2B ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shape2A.simps unflat_shape2B.simps flat_shape2A.simps flat_shape2B.simps invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      shape3A.case shape3B.case
      id_apply o_apply refl
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits)
  done

lemma unflat_shape3_flat_shape3_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape3A u0 (flat_shape3A ua) = ua) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape3B u0 (flat_shape3B ub) = ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> unflat_shape3A u0 (flat_shape3A ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> unflat_shape3B u0 (flat_shape3B ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shape3A.simps unflat_shape3B.simps flat_shape3A.simps flat_shape3B.simps invar_shape3A.simps invar_shape3B.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      shape3A.case shape3B.case label.distinct
      id_apply o_apply refl if_True
    intro!:
      trans[OF F1A.map_cong_pred F1A.map_ident] trans[OF F1B.map_cong_pred F1B.map_ident]
      trans[OF F2A.map_cong_pred F2A.map_ident] trans[OF F2B.map_cong_pred F2B.map_ident]
      trans[OF E3A.map_cong_pred E3A.map_ident] trans[OF E3B.map_cong_pred E3B.map_ident]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits)
  done

lemmas unflat_shape_flat_shape =
  mp[OF spec[OF conjunct1[OF unflat_shape1_flat_shape1_raw]]]
  mp[OF spec[OF conjunct2[OF unflat_shape1_flat_shape1_raw]]]
  mp[OF spec[OF conjunct1[OF unflat_shape2_flat_shape2_raw]]]
  mp[OF spec[OF conjunct2[OF unflat_shape2_flat_shape2_raw]]]
  mp[OF spec[OF conjunct1[OF unflat_shape3_flat_shape3_raw]]]
  mp[OF spec[OF conjunct2[OF unflat_shape3_flat_shape3_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_shape_flat_shape_tac ctxt induct Ps us flat_simps invar_simps F_pred_maps
    F_pred_Trues F_pred_mono_strongs label_splits F_pred_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        F_pred_mono_strongs);
      val simp = full_simp_tac (fold Simplifier.add_cong F_pred_congs
        (fold Splitter.add_split (label_splits @ @{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, invar_simps, F_pred_maps, F_pred_Trues,
            @{thms snoc.simps snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt)));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma invar_shape_flat_shape1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F1 u0) (flat_shape1A ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F1 u0) (flat_shape1B ub))"
  apply (tactic \<open>mk_invar_shape_flat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: (('a, 'b) F1A, ('a, 'b) F1B) shape3A. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F1 u0) (flat_shape1A ua)"},
     @{cterm "\<lambda>ub :: (('a, 'b) F1A, ('a, 'b) F1B) shape3B. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F1 u0) (flat_shape1B ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_True F1B.pred_True F2A.pred_True F2B.pred_True E3A.pred_True E3B.pred_True}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms F1A.pred_cong F1B.pred_cong F2A.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F1 u0) (flat_shape1A ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F1 u0) (flat_shape1B ub)" ua ub])
  apply (auto simp only:
      flat_shape1A.simps flat_shape1B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong)
  done*)

lemma invar_shape_flat_shape2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F2 u0) (flat_shape2A ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F2 u0) (flat_shape2B ub))"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc F2 u0) (flat_shape2A ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc F2 u0) (flat_shape2B ub)" ua ub])
  apply (auto simp only:
      flat_shape2A.simps flat_shape2B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong)
  done

lemma invar_shape_flat_shape3_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc E3 u0) (flat_shape3A ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc E3 u0) (flat_shape3B ub))"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A (snoc E3 u0) (flat_shape3A ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B (snoc E3 u0) (flat_shape3B ub)" ua ub])
  apply (auto simp only:
      flat_shape3A.simps flat_shape3B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      F1A.pred_True F2A.pred_True F1B.pred_True F2B.pred_True E3A.pred_True E3B.pred_True
      id_apply o_apply
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits sum.splits if_splits
    cong: F1A.pred_cong F2A.pred_cong F1B.pred_cong F2B.pred_cong E3A.pred_cong E3B.pred_cong)
  done

lemmas invar_shape_flat_shape =
  mp[OF spec[OF conjunct1[OF invar_shape_flat_shape1_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_flat_shape1_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_flat_shape2_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_flat_shape2_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_flat_shape3_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_flat_shape3_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_shape_unflat_shape_tac ctxt induct Ps us unflat_simps invar_simps
    F_pred_maps F_pred_mono_strongs label_splits label_distincts =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN print_tac ctxt "bla" THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: F_pred_mono_strongs);
      val simp = full_simp_tac (fold Splitter.add_split (label_splits @ @{thms list.splits sum.splits if_splits})
        (ss_only (flat [unflat_simps, invar_simps, F_pred_maps, label_distincts,
            @{thms snoc.simps snoc_neq_Nil id_apply o_apply simp_thms list.simps
              de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, elim, intro, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma invar_shape_unflat_shape1_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape1A u0 ua)) \<and>
   (\<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape1B u0 ub))"
  apply (tactic \<open>mk_invar_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape1A u0 ua)"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape1B u0 ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms unflat_shape1A.simps unflat_shape1B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms label.distinct}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A (snoc F1 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape1A u0 ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B (snoc F1 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape1B u0 ub)" ua ub])
  apply (auto simp only:
      unflat_shape1A.simps unflat_shape1B.simps invar_shape3A.simps invar_shape3B.simps snoc.simps snoc_neq_Nil
      F1A.pred_map F2A.pred_map F1B.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      id_apply o_apply refl
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    split: list.splits label.splits if_splits)
  done*)

lemma invar_shape_unflat_shape2_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape2A u0 ua)) \<and>
   (\<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape2B u0 ub))"
  apply (tactic \<open>mk_invar_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc F2 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape2A u0 ua)"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc F2 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape2B u0 ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms unflat_shape2A.simps unflat_shape2B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms label.distinct}\<close>)
  done

lemma invar_shape_unflat_shape3_raw:
  fixes ua :: "('a, 'b) shape3A" and ub :: "('a, 'b) shape3B"
  shows
  "(\<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape3A u0 ua)) \<and>
   (\<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape3B u0 ub))"
  apply (tactic \<open>mk_invar_shape_unflat_shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: ('a, 'b) shape3A. \<forall>u0. invar_shape3A (snoc E3 u0) ua \<longrightarrow> invar_shape3A u0 (unflat_shape3A u0 ua)"},
     @{cterm "\<lambda>ub :: ('a, 'b) shape3B. \<forall>u0. invar_shape3B (snoc E3 u0) ub \<longrightarrow> invar_shape3B u0 (unflat_shape3B u0 ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms unflat_shape3A.simps unflat_shape3B.simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map}
    @{thms F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong E3A.pred_mono_strong E3B.pred_mono_strong}
    @{thms label.splits}
    @{thms label.distinct}\<close>)
  done

lemmas invar_shape_unflat_shape =
  mp[OF spec[OF conjunct1[OF invar_shape_unflat_shape1_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_unflat_shape1_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_unflat_shape2_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_unflat_shape2_raw]]]
  mp[OF spec[OF conjunct1[OF invar_shape_unflat_shape3_raw]]]
  mp[OF spec[OF conjunct2[OF invar_shape_unflat_shape3_raw]]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_raw_flat_raw_tac ctxt induct Ps us flat_simps invar_simps invar_shape_flat_shapes
    G_pred_maps G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ invar_shape_flat_shapes));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, invar_simps, G_pred_maps,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma invar_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "(invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F1 u0) (flat1_rawF t)) \<and>
   (invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F1 u0) (flat1_rawE s))"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_invar_raw_flat_raw_tac' ctxt Ps induct raw_exhausts raw_injects raw_maps
          invar_simps flat_simps invar_shape_flat_shape
          G_pred_maps G_rel_mono_strongs =
          let
            val n = length Ps;
          in
            HEADGOAL (EVERY' [rtac ctxt rev_mp,
              rtac ctxt (infer_instantiate' ctxt (map SOME Ps) induct)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (conjI :: exI :: disjI1 :: []);
                  val elim = eresolve_tac ctxt G_rel_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_injects, raw_maps, invar_simps, flat_simps,
                    invar_shape_flat_shape, G_pred_maps, @{thms snoc.simps[symmetric] o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)])
          end
      in
        mk_invar_raw_flat_raw_tac' @{context}
          [@{cterm "\<lambda>x. \<lambda>y :: ('a, 'b) rawF. \<exists>u0 t. x = (snoc F1 u0) \<and> y = (flat1_rawF t) \<and> invar_rawF u0 t"},
           @{cterm "\<lambda>x. \<lambda>y :: ('a, 'b) rawE. \<exists>u0 s. x = (snoc F1 u0) \<and> y = (flat1_rawE s) \<and> invar_rawE u0 s"}]
          @{thm invar_rawF_invar_rawE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.inject rawE.inject}
          @{thms rawF.map rawE.map}
          @{thms invar_rawF_simps invar_rawE_simps}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms invar_shape_flat_shape}
          @{thms GF.pred_map GE.pred_map}
          @{thms GF.pred_mono_strong GE.pred_mono_strong}
      end\<close>)

(*
thm invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = (snoc F1 u0) \<and> y = (flat1_rawF t) \<and> invar_rawF u0 t"
    "\<lambda>x y. \<exists>u0 s. x = (snoc F1 u0) \<and> y = (flat1_rawE s) \<and> invar_rawE u0 s"]
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = (snoc F1 u0) \<and> y = (flat1_rawF t) \<and> invar_rawF u0 t"
    "\<lambda>x y. \<exists>u0 s. x = (snoc F1 u0) \<and> y = (flat1_rawE s) \<and> invar_rawE u0 s"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        flat1_simps flat2_simps flat3_simps
        invar_shape_flat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)+
    done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        flat1_simps flat2_simps flat3_simps
        invar_shape_flat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)+
    done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, rule impI, (erule allE mp)+, (rule exI conjI refl)+, assumption)
  apply (drule conjunct2, rule impI, (erule allE mp)+, (rule exI conjI refl)+, assumption)
  done

  apply (tactic \<open>mk_invar_raw_flat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F1 u0) (flat1_rawF t)"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F1 u0) (flat1_rawE s)"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms invar_shape_flat_shape}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F1 u0) (flat1_rawF t)"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F1 u0) (flat1_rawE s)" t s])
  apply (auto simp only:
      flat1_rawF.simps invar_rawF.simps flat1_rawE.simps invar_rawE.simps snoc.simps[symmetric] invar_shape_flat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma invar_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "(invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F2 u0) (flat2_rawF t)) \<and>
   (invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F2 u0) (flat2_rawE s))"
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = (snoc F2 u0) \<and> y = (flat2_rawF t) \<and> invar_rawF u0 t"
    "\<lambda>x y. \<exists>u0 s. x = (snoc F2 u0) \<and> y = (flat2_rawE s) \<and> invar_rawE u0 s"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        flat1_simps flat2_simps flat3_simps
        invar_shape_flat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)+
    done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        flat1_simps flat2_simps flat3_simps
        invar_shape_flat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)+
    done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, rule impI, (erule allE mp)+, (rule exI conjI refl)+, assumption)
  apply (drule conjunct2, rule impI, (erule allE mp)+, (rule exI conjI refl)+, assumption)
  done
(*
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc F2 u0) (flat2_rawF t)"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc F2 u0) (flat2_rawE s)" t s])
  apply (auto simp only:
      flat2_rawF.simps invar_rawF.simps flat2_rawE.simps invar_rawE.simps snoc.simps[symmetric] invar_shape_flat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma invar_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "(invar_rawF u0 t \<longrightarrow> invar_rawF (snoc E3 u0) (flat3_rawF t)) \<and>
   (invar_rawE u0 s \<longrightarrow> invar_rawE (snoc E3 u0) (flat3_rawE s))"
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = (snoc E3 u0) \<and> y = (flat3_rawF t) \<and> invar_rawF u0 t"
    "\<lambda>x y. \<exists>u0 s. x = (snoc E3 u0) \<and> y = (flat3_rawE s) \<and> invar_rawE u0 s"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        flat1_simps flat2_simps flat3_simps
        invar_shape_flat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)+
    done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        flat1_simps flat2_simps flat3_simps
        invar_shape_flat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)+
    done
  apply fast
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF (snoc E3 u0) (flat3_rawF t)"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE (snoc E3 u0) (flat3_rawE s)" t s])
  apply (auto simp only:
      flat3_rawF.simps invar_rawF.simps flat3_rawE.simps invar_rawE.simps snoc.simps[symmetric] invar_shape_flat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemmas invar_flat =
  mp[OF conjunct1[OF invar_flat1_raw]]
  mp[OF conjunct2[OF invar_flat1_raw]]
  mp[OF conjunct1[OF invar_flat2_raw]]
  mp[OF conjunct2[OF invar_flat2_raw]]
  mp[OF conjunct1[OF invar_flat3_raw]]
  mp[OF conjunct2[OF invar_flat3_raw]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_invar_raw_unflat_raw_tac ctxt induct Ps us unflat_simps invar_simps invar_shape_unflat_shapes
    G_pred_maps G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ invar_shape_unflat_shapes));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [unflat_simps, invar_simps, G_pred_maps,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma invar_unflat1_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
   "(invar_rawF (snoc F1 u0) t \<longrightarrow> invar_rawF u0 (unflat1_rawF u0 t)) \<and>
    (invar_rawE (snoc F1 u0) s \<longrightarrow> invar_rawE u0 (unflat1_rawE u0 s))"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_invar_raw_unflat_raw_tac' ctxt Ps induct raw_exhausts raw_injects raw_maps
          invar_simps unflat_simps invar_shape_unflat_shape
          G_pred_maps G_rel_mono_strongs =
          let
            val n = length Ps;
          in
            HEADGOAL (EVERY' [rtac ctxt rev_mp,
              rtac ctxt (infer_instantiate' ctxt (map SOME Ps) induct)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (conjI :: exI :: disjI1 :: []);
                  val elim = eresolve_tac ctxt G_rel_mono_strongs;
                  val simp = asm_full_simp_tac (ss_only (flat [raw_injects, raw_maps, invar_simps, unflat_simps,
                    invar_shape_unflat_shape, G_pred_maps, @{thms snoc.simps[symmetric] o_apply}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)])
          end
      in
        mk_invar_raw_unflat_raw_tac' @{context}
          [@{cterm "\<lambda>x. \<lambda>y :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<exists>u0 t. x = u0 \<and> y = (unflat1_rawF u0 t) \<and> invar_rawF (snoc F1 u0) t"},
           @{cterm "\<lambda>x. \<lambda>y :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<exists>u0 s. x = u0 \<and> y = (unflat1_rawE u0 s) \<and> invar_rawE (snoc F1 u0) s"}]
          @{thm invar_rawF_invar_rawE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.inject rawE.inject}
          @{thms rawF.map rawE.map}
          @{thms invar_rawF_simps invar_rawE_simps}
          @{thms unflat1_simps unflat2_simps unflat3_simps}
          @{thms invar_shape_unflat_shape}
          @{thms GF.pred_map GE.pred_map}
          @{thms GF.pred_mono_strong GE.pred_mono_strong}
      end\<close>)
(*
thm invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = u0 \<and> y = (unflat1_rawF u0 t) \<and> invar_rawF (snoc F1 u0) t"
    "\<lambda>x y. \<exists>u0 s. x = u0 \<and> y = (unflat1_rawE u0 s) \<and> invar_rawE (snoc F1 u0) s"]
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = u0 \<and> y = (unflat1_rawF u0 t) \<and> invar_rawF (snoc F1 u0) t"
    "\<lambda>x y. \<exists>u0 s. x = u0 \<and> y = (unflat1_rawE u0 s) \<and> invar_rawE (snoc F1 u0) s"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        unflat1_simps unflat2_simps unflat3_simps
        invar_shape_unflat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        unflat1_simps unflat2_simps unflat3_simps
        invar_shape_unflat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, rule impI, (erule allE mp)+, (rule exI conjI refl)+, assumption)
  apply (drule conjunct2, rule impI, (erule allE mp)+, (rule exI conjI refl)+, assumption)
  done

  apply (tactic \<open>mk_invar_raw_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: ('a, 'b) rawF. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> invar_rawF u0 (unflat1_rawF u0 t)"},
     @{cterm "\<lambda>s :: ('a, 'b) rawE. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> invar_rawE u0 (unflat1_rawE u0 s)"}]
    [@{cterm t}, @{cterm s}]
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms invar_shape_unflat_shape}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> invar_rawF u0 (unflat1_rawF u0 t)"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> invar_rawE u0 (unflat1_rawE u0 s)" t s])
  apply (auto simp only:
      unflat1_rawF.simps invar_rawF.simps unflat1_rawE.simps invar_rawE.simps snoc.simps invar_shape_unflat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma invar_unflat2_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
   "(invar_rawF (snoc F2 u0) t \<longrightarrow> invar_rawF u0 (unflat2_rawF u0 t)) \<and>
    (invar_rawE (snoc F2 u0) s \<longrightarrow> invar_rawE u0 (unflat2_rawE u0 s))"
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = u0 \<and> y = (unflat2_rawF u0 t) \<and> invar_rawF (snoc F2 u0) t"
    "\<lambda>x y. \<exists>u0 s. x = u0 \<and> y = (unflat2_rawE u0 s) \<and> invar_rawE (snoc F2 u0) s"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        unflat1_simps unflat2_simps unflat3_simps
        invar_shape_unflat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        unflat1_simps unflat2_simps unflat3_simps
        invar_shape_unflat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done
  apply fast
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F2 u0) t \<longrightarrow> invar_rawF u0 (unflat2_rawF u0 t)"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F2 u0) s \<longrightarrow> invar_rawE u0 (unflat2_rawE u0 s)" t s])
  apply (auto simp only:
      unflat2_rawF.simps invar_rawF.simps unflat2_rawE.simps invar_rawE.simps snoc.simps invar_shape_unflat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma invar_unflat3_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
   "(invar_rawF (snoc E3 u0) t \<longrightarrow> invar_rawF u0 (unflat3_rawF u0 t)) \<and>
    (invar_rawE (snoc E3 u0) s \<longrightarrow> invar_rawE u0 (unflat3_rawE u0 s))"
  apply (rule rev_mp)
  apply (rule invar_rawF_invar_rawE.coinduct[of
    "\<lambda>x y. \<exists>u0 t. x = u0 \<and> y = (unflat3_rawF u0 t) \<and> invar_rawF (snoc E3 u0) t"
    "\<lambda>x y. \<exists>u0 s. x = u0 \<and> y = (unflat3_rawE u0 s) \<and> invar_rawE (snoc E3 u0) s"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        unflat1_simps unflat2_simps unflat3_simps
        invar_shape_unflat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawF.inject rawE.inject rawF.map rawE.map
        invar_rawF_simps invar_rawE_simps
        unflat1_simps unflat2_simps unflat3_simps
        invar_shape_unflat_shape
        GF.pred_map GE.pred_map
        ex_simps simp_thms(39,40) snoc.simps[symmetric] id_apply o_apply
      elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done
  apply fast
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc E3 u0) t \<longrightarrow> invar_rawF u0 (unflat3_rawF u0 t)"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc E3 u0) s \<longrightarrow> invar_rawE u0 (unflat3_rawE u0 s)" t s])
  apply (auto simp only:
      unflat3_rawF.simps invar_rawF.simps unflat3_rawE.simps invar_rawE.simps snoc.simps invar_shape_unflat_shape
      id_apply o_apply GF.pred_map GE.pred_map
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemmas invar_unflat =
  mp[OF conjunct1[OF invar_unflat1_raw]]
  mp[OF conjunct2[OF invar_unflat1_raw]]
  mp[OF conjunct1[OF invar_unflat2_raw]]
  mp[OF conjunct2[OF invar_unflat2_raw]]
  mp[OF conjunct1[OF invar_unflat3_raw]]
  mp[OF conjunct2[OF invar_unflat3_raw]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_flat_raw_unflat_raw_tac ctxt induct Ps us flat_simps unflat_simps invar_simps raw_injects flat_shape_unflat_shape
    G_pred_maps G_map_comps G_map_cong_preds G_map_idents G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) G_map_cong_preds G_map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ flat_shape_unflat_shape));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, unflat_simps, invar_simps, G_pred_maps, G_map_comps, raw_injects,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma flat1_unflat1_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
  "(invar_rawF (snoc F1 u0) t \<longrightarrow> flat1_rawF (unflat1_rawF u0 t) = t) \<and>
   (invar_rawE (snoc F1 u0) s \<longrightarrow> flat1_rawE (unflat1_rawE u0 s) = s)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_flat_raw_unflat_raw_tac' ctxt Ps xs ys induct raw_exhausts raw_sels
          flat_simps unflat_simps invar_simps flat_shape_unflat_shapes
          G_rel_maps G_pred_rels G_rel_mono_strongs =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (rtac ctxt impI),
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            HEADGOAL (RANGE (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (allI :: impI :: conjI :: exI :: []);
                  val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) :: G_rel_mono_strongs);
                  val simp = asm_full_simp_tac (ss_only (flat [raw_sels, flat_simps, unflat_simps, invar_simps,
                    flat_shape_unflat_shapes, G_rel_maps, G_pred_rels, @{thms snoc.simps[symmetric] eq_onp_def
                    simp_thms snoc_neq_Nil}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts)) THEN
            HEADGOAL (case_mut (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)]) (K all_tac))
          end
      in
        mk_flat_raw_unflat_raw_tac' @{context}
          [@{cterm "\<lambda>l r :: ('a, 'b) rawF. \<exists>u0 t. l = flat1_rawF (unflat1_rawF u0 t) \<and> r = t \<and> invar_rawF (snoc F1 u0) t"},
          @{cterm "\<lambda>l r :: ('a, 'b) rawE. \<exists>u0 t. l = flat1_rawE (unflat1_rawE u0 t) \<and> r = t \<and> invar_rawE (snoc F1 u0) t"}]
          [@{cterm "flat1_rawF (unflat1_rawF u0 t)"}, @{cterm "flat1_rawE (unflat1_rawE u0 s)"}]
          [@{cterm t}, @{cterm s}]
          @{thm rawF_rawE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.sel rawE.sel}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms unflat1_simps unflat2_simps unflat3_simps}
          @{thms invar_rawF_simps invar_rawE_simps}
          @{thms flat_shape_unflat_shape}
          @{thms GF.rel_map GE.rel_map}
          @{thms GF.pred_rel GE.pred_rel}
          @{thms GF.rel_mono_strong GE.rel_mono_strong}
      end\<close>)
(*
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = flat1_rawF (unflat1_rawF u0 t) \<and> r = t \<and> invar_rawF (snoc F1 u0) t"
    "\<lambda>l r. \<exists>u0 t. l = flat1_rawE (unflat1_rawE u0 t) \<and> r = t \<and> invar_rawE (snoc F1 u0) t"
    "flat1_rawF (unflat1_rawF u0 t)" t "flat1_rawE (unflat1_rawE u0 s)" s])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.sel unflat1_simps flat1_simps GF.rel_map invar_rawF_simps GF.pred_rel
      flat_shape_unflat_shape snoc.simps[symmetric] eq_onp_def elim!: GF.rel_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawE.sel unflat1_simps flat1_simps GE.rel_map invar_rawE_simps GE.pred_rel
      flat_shape_unflat_shape snoc.simps[symmetric] eq_onp_def elim!: GE.rel_mono_strong)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (rule impI, drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (rule impI, drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done

  apply (tactic \<open>mk_flat_raw_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: ('a, 'b) rawF. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> flat1_rawF (unflat1_rawF u0 t) = t"},
     @{cterm "\<lambda>s :: ('a, 'b) rawE. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> flat1_rawE (unflat1_rawE u0 s) = s"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms rawF.inject rawE.inject}
    @{thms flat_shape_unflat_shape}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_cong_pred GE.map_cong_pred}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F1 u0) t \<longrightarrow> flat1_rawF (unflat1_rawF u0 t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F1 u0) s \<longrightarrow> flat1_rawE (unflat1_rawE u0 s) = s" t s])
  apply (auto simp only:
      unflat1_rawF.simps unflat1_rawE.simps flat1_rawF.simps flat1_rawE.simps invar_rawF_simps invar_rawE_simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply GF.pred_map GF.map_comp GE.pred_map GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma flat2_unflat2_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
  "(invar_rawF (snoc F2 u0) t \<longrightarrow> flat2_rawF (unflat2_rawF u0 t) = t) \<and>
   (invar_rawE (snoc F2 u0) s \<longrightarrow> flat2_rawE (unflat2_rawE u0 s) = s)"
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = flat2_rawF (unflat2_rawF u0 t) \<and> r = t \<and> invar_rawF (snoc F2 u0) t"
    "\<lambda>l r. \<exists>u0 t. l = flat2_rawE (unflat2_rawE u0 t) \<and> r = t \<and> invar_rawE (snoc F2 u0) t"
    "flat2_rawF (unflat2_rawF u0 t)" t "flat2_rawE (unflat2_rawE u0 s)" s])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.sel unflat2_simps flat2_simps GF.rel_map invar_rawF_simps GF.pred_rel
      flat_shape_unflat_shape snoc.simps[symmetric] eq_onp_def elim!: GF.rel_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawE.exhaust[of t])
    apply (auto simp only: rawE.sel unflat2_simps flat2_simps GE.rel_map invar_rawE_simps GE.pred_rel
      flat_shape_unflat_shape snoc.simps[symmetric] eq_onp_def elim!: GE.rel_mono_strong)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (rule impI, drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (rule impI, drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc F2 u0) t \<longrightarrow> flat2_rawF (unflat2_rawF u0 t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc F2 u0) s \<longrightarrow> flat2_rawE (unflat2_rawE u0 s) = s" t s])
  apply (auto simp only:
      unflat2_rawF.simps unflat2_rawE.simps flat2_rawF.simps flat2_rawE.simps invar_rawF_simps invar_rawE_simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply GF.pred_map GF.map_comp GE.pred_map GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma flat3_unflat3_raw:
  fixes t :: "('a, 'b) rawF" and s :: "('a, 'b) rawE"
  shows
  "(invar_rawF (snoc E3 u0) t \<longrightarrow> flat3_rawF (unflat3_rawF u0 t) = t) \<and>
   (invar_rawE (snoc E3 u0) s \<longrightarrow> flat3_rawE (unflat3_rawE u0 s) = s)"
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = flat3_rawF (unflat3_rawF u0 t) \<and> r = t \<and> invar_rawF (snoc E3 u0) t"
    "\<lambda>l r. \<exists>u0 t. l = flat3_rawE (unflat3_rawE u0 t) \<and> r = t \<and> invar_rawE (snoc E3 u0) t"
    "flat3_rawF (unflat3_rawF u0 t)" t "flat3_rawE (unflat3_rawE u0 s)" s])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.sel unflat3_simps flat3_simps GF.rel_map invar_rawF_simps GF.pred_rel
      flat_shape_unflat_shape snoc.simps[symmetric] eq_onp_def elim!: GF.rel_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawE.exhaust[of t])
    apply (auto simp only: rawE.sel unflat3_simps flat3_simps GE.rel_map invar_rawE_simps GE.pred_rel
      flat_shape_unflat_shape snoc.simps[symmetric] eq_onp_def elim!: GE.rel_mono_strong)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (rule impI, drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (rule impI, drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF (snoc E3 u0) t \<longrightarrow> flat3_rawF (unflat3_rawF u0 t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE (snoc E3 u0) s \<longrightarrow> flat3_rawE (unflat3_rawE u0 s) = s" t s])
  apply (auto simp only:
      unflat3_rawF.simps unflat3_rawE.simps flat3_rawF.simps flat3_rawE.simps invar_rawF_simps invar_rawE_simps snoc.simps
      flat_shape_unflat_shape id_apply o_apply GF.pred_map GF.map_comp GE.pred_map GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemmas flat_unflat =
  mp[OF conjunct1[OF flat1_unflat1_raw]]
  mp[OF conjunct2[OF flat1_unflat1_raw]]
  mp[OF conjunct1[OF flat2_unflat2_raw]]
  mp[OF conjunct2[OF flat2_unflat2_raw]]
  mp[OF conjunct1[OF flat3_unflat3_raw]]
  mp[OF conjunct2[OF flat3_unflat3_raw]]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_unflat_raw_flat_raw_tac ctxt induct Ps us flat_simps unflat_simps invar_simps raw_injects unflat_shape_flat_shape
    G_pred_maps G_map_comps G_map_cong_preds G_map_idents G_pred_mono_strongs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt (map (fn thm => thm RS spec RS mp) IHs) THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) G_map_cong_preds G_map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE :: not_sym :: (not_sym RS disjI1) ::
        (G_pred_mono_strongs @ unflat_shape_flat_shape));
      val simp = full_simp_tac (fold Splitter.add_split (@{thms list.splits sum.splits if_splits})
        (ss_only (flat [flat_simps, unflat_simps, invar_simps, G_pred_maps, G_map_comps, raw_injects,
            @{thms snoc.simps[symmetric] snoc_neq_Nil id_apply o_apply simp_thms
              list.inject list.distinct de_Morgan_conj de_Morgan_disj triv_forall_equality}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, intro, elim, apply_IHs, simp])
    end) ctxt);
end
\<close>
lemma unflat1_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "(invar_rawF u0 t \<longrightarrow> unflat1_rawF u0 (flat1_rawF t) = t) \<and>
   (invar_rawE u0 s \<longrightarrow> unflat1_rawE u0 (flat1_rawE s) = s)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_unflat_raw_flat_raw_tac' ctxt Ps xs ys induct raw_exhausts raw_sels
          flat_simps unflat_simps invar_simps flat_shape_unflat_shapes
          G_rel_maps G_pred_rels G_rel_mono_strongs =
          let
            val n = length Ps;
            fun case_mut a b = if n <> 1 then a else b;
          in
            HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (rtac ctxt impI),
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct),
              case_mut (K all_tac) (REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]) THEN'
              assume_tac ctxt)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (exI :: []);
                  val elim = eresolve_tac ctxt (conjE :: G_rel_mono_strongs);
                  val simp = asm_full_simp_tac (ss_only (flat [raw_sels, flat_simps, unflat_simps, invar_simps,
                    flat_shape_unflat_shapes, G_rel_maps, G_pred_rels, @{thms eq_onp_def}]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, elim, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), rtac ctxt impI,
                REPEAT_DETERM o (eresolve_tac ctxt [allE, mp]),
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl]),
                assume_tac ctxt
              ]) (1 upto n)])
          end
      in
        mk_unflat_raw_flat_raw_tac' @{context}
          [@{cterm "\<lambda>l r :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<exists>u0 t. l = unflat1_rawF u0 (flat1_rawF t) \<and> r = t \<and> invar_rawF u0 t"},
          @{cterm "\<lambda>l r :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<exists>u0 s. l = unflat1_rawE u0 (flat1_rawE s) \<and> r = s \<and> invar_rawE u0 s"}]
          [@{cterm "unflat1_rawF u0 (flat1_rawF t)"}, @{cterm "unflat1_rawE u0 (flat1_rawE s)"}]
          [@{cterm t}, @{cterm s}]
          @{thm rawF_rawE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.sel rawE.sel}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms unflat1_simps unflat2_simps unflat3_simps}
          @{thms invar_rawF_simps invar_rawE_simps}
          @{thms unflat_shape_flat_shape}
          @{thms GF.rel_map GE.rel_map}
          @{thms GF.pred_rel GE.pred_rel}
          @{thms GF.rel_mono_strong GE.rel_mono_strong}
      end\<close>)

(*
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = unflat1_rawF u0 (flat1_rawF t) \<and> r = t \<and> invar_rawF u0 t"
    "\<lambda>l r. \<exists>u0 s. l = unflat1_rawE u0 (flat1_rawE s) \<and> r = s \<and> invar_rawE u0 s"
    "unflat1_rawF u0 (flat1_rawF t)" t "unflat1_rawE u0 (flat1_rawE s)" s])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.sel unflat1_simps flat1_simps GF.rel_map invar_rawF_simps GF.pred_rel
      unflat_shape_flat_shape snoc.simps[symmetric] eq_onp_def elim!: GF.rel_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawE.sel unflat1_simps flat1_simps GE.rel_map invar_rawE_simps GE.pred_rel
      unflat_shape_flat_shape snoc.simps[symmetric] eq_onp_def elim!: GE.rel_mono_strong)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (rule impI, drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (rule impI, drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done

  apply (tactic \<open>mk_flat_raw_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat1_rawF u0 (flat1_rawF t) = t"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat1_rawE u0 (flat1_rawE s) = s"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms rawF.inject rawE.inject}
    @{thms unflat_shape_flat_shape}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_cong_pred GE.map_cong_pred}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat1_rawF u0 (flat1_rawF t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat1_rawE u0 (flat1_rawE s) = s"])
  apply (auto simp only:
      unflat1_rawF.simps unflat1_rawE.simps flat1_rawF.simps flat1_rawE.simps invar_rawF_simps invar_rawE_simps unflat_shape_flat_shape
      id_apply o_apply GF.pred_map GE.pred_map GF.map_comp GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma unflat2_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "(invar_rawF u0 t \<longrightarrow> unflat2_rawF u0 (flat2_rawF t) = t) \<and>
   (invar_rawE u0 s \<longrightarrow> unflat2_rawE u0 (flat2_rawE s) = s)"
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = unflat2_rawF u0 (flat2_rawF t) \<and> r = t \<and> invar_rawF u0 t"
    "\<lambda>l r. \<exists>u0 s. l = unflat2_rawE u0 (flat2_rawE s) \<and> r = s \<and> invar_rawE u0 s"
    "unflat2_rawF u0 (flat2_rawF t)" t "unflat2_rawE u0 (flat2_rawE s)" s])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.sel unflat2_simps flat2_simps GF.rel_map invar_rawF_simps GF.pred_rel
      unflat_shape_flat_shape snoc.simps[symmetric] eq_onp_def elim!: GF.rel_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawE.sel unflat2_simps flat2_simps GE.rel_map invar_rawE_simps GE.pred_rel
      unflat_shape_flat_shape snoc.simps[symmetric] eq_onp_def elim!: GE.rel_mono_strong)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (rule impI, drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (rule impI, drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat2_rawF u0 (flat2_rawF t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat2_rawE u0 (flat2_rawE s) = s"])
  apply (auto simp only:
      unflat2_rawF.simps unflat2_rawE.simps flat2_rawF.simps flat2_rawE.simps invar_rawF_simps invar_rawE_simps unflat_shape_flat_shape
      id_apply o_apply GF.pred_map GE.pred_map GF.map_comp GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemma unflat3_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "(invar_rawF u0 t \<longrightarrow> unflat3_rawF u0 (flat3_rawF t) = t) \<and>
   (invar_rawE u0 s \<longrightarrow> unflat3_rawE u0 (flat3_rawE s) = s)"
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = unflat3_rawF u0 (flat3_rawF t) \<and> r = t \<and> invar_rawF u0 t"
    "\<lambda>l r. \<exists>u0 s. l = unflat3_rawE u0 (flat3_rawE s) \<and> r = s \<and> invar_rawE u0 s"
    "unflat3_rawF u0 (flat3_rawF t)" t "unflat3_rawE u0 (flat3_rawE s)" s])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.sel unflat3_simps flat3_simps GF.rel_map invar_rawF_simps GF.pred_rel
      unflat_shape_flat_shape snoc.simps[symmetric] eq_onp_def elim!: GF.rel_mono_strong)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ u0 s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawE.sel unflat3_simps flat3_simps GE.rel_map invar_rawE_simps GE.pred_rel
      unflat_shape_flat_shape snoc.simps[symmetric] eq_onp_def elim!: GE.rel_mono_strong)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (rule impI, drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (rule impI, drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>u0. invar_rawF u0 t \<longrightarrow> unflat3_rawF u0 (flat3_rawF t) = t"
    "\<lambda>s. \<forall>u0. invar_rawE u0 s \<longrightarrow> unflat3_rawE u0 (flat3_rawE s) = s"])
  apply (auto simp only:
      unflat3_rawF.simps unflat3_rawE.simps flat3_rawF.simps flat3_rawE.simps invar_rawF_simps invar_rawE_simps unflat_shape_flat_shape
      id_apply o_apply GF.pred_map GE.pred_map GF.map_comp GE.map_comp
    intro!: trans[OF GF.map_cong_pred GF.map_ident] trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GF.pred_mono_strong GE.pred_mono_strong)
  done*)

lemmas unflat_flat =
  mp[OF conjunct1[OF unflat1_flat1_raw]]
  mp[OF conjunct2[OF unflat1_flat1_raw]]
  mp[OF conjunct1[OF unflat2_flat2_raw]]
  mp[OF conjunct2[OF unflat2_flat2_raw]]
  mp[OF conjunct1[OF unflat3_flat3_raw]]
  mp[OF conjunct2[OF unflat3_flat3_raw]]

section \<open>Constructor is Bijection\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_un_T_T_tac ctxt T_ctr_def T_dtr_def T_Abs_inverse T_Reps T_Rep_inverses
    invar_raw_simps raw_cases invar_Shape_simps Shape_cases
    invar_map_raw_closeds invar_flat_raws unflat_flat_raws
    G_pred_maps G_pred_Trues G_map_comps G_map_idents G_pred_congs G_map_congs =
  unfold_tac ctxt [T_ctr_def, T_dtr_def] THEN
  HEADGOAL (full_simp_tac (fold Simplifier.add_cong (G_pred_congs @ G_map_congs)
        (ss_only (flat [[T_Abs_inverse],
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, T_Rep_inverses,
            invar_raw_simps, raw_cases, invar_Shape_simps, Shape_cases,
            invar_map_raw_closeds, invar_flat_raws, unflat_flat_raws,
            G_pred_maps, G_pred_Trues, G_map_comps, G_map_idents,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt)));
end
\<close>
lemma un_T_T: "un_T (T x) = x"
  apply (tactic \<open>mk_un_T_T_tac @{context}
    @{thm T_def}
    @{thm un_T_def}
    @{thm Abs_T_inverse}
    @{thms Rep_T Rep_S}
    @{thms Rep_T_inverse Rep_S_inverse}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms rawF.case rawE.case}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.case shape3B.case}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms unflat_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.map_cong GE.map_cong}\<close>)
  done
(*
  unfolding T_def un_T_def
  apply (simp only:
      Abs_T_inverse Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawF.simps rawF.case
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_map_closed invar_flat unflat_flat
      GF.pred_map GF.pred_True GF.map_comp GF.map_ident
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong GF.map_cong)
  done*)

lemma un_S_S: "un_S (S x) = x"
  apply (tactic \<open>mk_un_T_T_tac @{context}
    @{thm S_def}
    @{thm un_S_def}
    @{thm Abs_S_inverse}
    @{thms Rep_T Rep_S}
    @{thms Rep_T_inverse Rep_S_inverse}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms rawF.case rawE.case}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.case shape3B.case}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms unflat_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.map_cong GE.map_cong}\<close>)
  done
(*
  unfolding S_def un_S_def
  apply (simp only:
      Abs_S_inverse Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawE.simps rawE.case
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_map_closed invar_flat unflat_flat
      GE.pred_map GE.pred_True GE.map_comp GE.map_ident
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong GE.map_cong)
  done*)

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_T_un_T_tac ctxt T_ctr_def T_dtr_def T_Rep t T_Rep_inject raw_exhaust T_Rep_t
    T_Abs_inverses T_Reps T_Rep_inverses invar_raw_simps raw_cases raw_injects
    invar_Shape_simps Shape_cases invar_map_raw_closeds invar_flat_raws
    invar_unflat_raws flat_unflat_raws pred_maps pred_Trues map_comps
    invar_Shape_depth_iffs map_cong_preds map_idents pred_congs pred_mono_strongs =

  unfold_tac ctxt [T_ctr_def, T_dtr_def] THEN
  HEADGOAL (EVERY' [rtac ctxt rev_mp,
    rtac ctxt (infer_instantiate' ctxt [SOME t] (unfold_thms ctxt @{thms mem_Collect_eq} T_Rep)),
    rtac ctxt impI, rtac ctxt (T_Rep_inject RS iffD1),
    rtac ctxt (infer_instantiate' ctxt [SOME T_Rep_t] raw_exhaust)]) THEN
  ALLGOALS
    (let
      val intro = resolve_tac ctxt (allI :: impI :: conjI ::
        map2 (fn cong => fn id => trans OF [cong, id]) map_cong_preds map_idents);
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE ::  pred_mono_strongs);
      val dest = dresolve_tac ctxt (map (fn thm => thm RS iffD1) invar_Shape_depth_iffs);
      val simp = asm_full_simp_tac (fold Simplifier.add_cong pred_congs
        (ss_only (flat [T_Abs_inverses,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, T_Rep_inverses,
            invar_raw_simps, raw_cases, raw_injects,
            invar_Shape_simps, Shape_cases, invar_map_raw_closeds, invar_flat_raws,
            invar_unflat_raws, flat_unflat_raws, pred_maps, pred_Trues, map_comps,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt));
    in
      REPEAT_DETERM o FIRST' [hyp_subst_tac ctxt, intro, elim, dest, simp]
    end);
end
\<close>

lemma T_un_T: "T (un_T x) = x"
  apply (tactic \<open>mk_T_un_T_tac @{context}
    @{thm T_def}
    @{thm un_T_def}
    @{thm Rep_T}
    @{cterm x}
    @{thm Rep_T_inject}
    @{thm rawF.exhaust}
    @{cterm "Rep_T x"}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms Rep_T Rep_S}
    @{thms Rep_T_inverse Rep_S_inverse}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms rawF.case rawE.case}
    @{thms rawF.inject rawE.inject}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms shape3A.case shape3B.case}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms invar_unflat}
    @{thms flat_unflat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms invar_shape3A_depth_iff(1) invar_shape3B_depth_iff(1)}
    @{thms GF.map_cong_pred GE.map_cong_pred}
    @{thms GF.map_ident GE.map_ident}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.pred_mono_strong GE.pred_mono_strong}\<close>)
  done
(*
  unfolding T_def un_T_def
  apply (rule rev_mp)
  apply (rule Rep_T[unfolded mem_Collect_eq, of x])
  apply (rule impI)
  apply (rule iffD1[OF Rep_T_inject])
  apply (rule rawF.exhaust[of "Rep_T x"])
  apply (auto simp only:
      Abs_T_inverse Abs_S_inverse
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawF.simps rawF.case rawF.inject
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_map_closed invar_flat invar_unflat flat_unflat
      GF.pred_map GF.pred_True GF.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    dest!: iffD1[OF invar_shape3A_depth_iff(1)] iffD1[OF invar_shape3B_depth_iff(1)]
    intro!: trans[OF GF.map_cong_pred GF.map_ident]
    elim!: GF.pred_mono_strong
    cong: GF.pred_cong)
  done*)

lemma S_un_S: "S (un_S x) = x"
  unfolding S_def un_S_def
  apply (rule rev_mp)
  apply (rule Rep_S[unfolded mem_Collect_eq, of x])
  apply (rule impI)
  apply (rule iffD1[OF Rep_S_inject])
  apply (rule rawE.exhaust[of "Rep_S x"])
  apply (auto simp only:
      Abs_T_inverse Abs_S_inverse
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq] Rep_T_inverse Rep_S_inverse
      invar_rawF_simps invar_rawE_simps rawE.case rawE.inject
      invar_shape3A.simps invar_shape3B.simps shape3A.case shape3B.case
      invar_map_closed invar_flat invar_unflat flat_unflat
      GE.pred_map GE.pred_True GE.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    dest!: iffD1[OF invar_shape3A_depth_iff(1)] iffD1[OF invar_shape3B_depth_iff(1)]
    intro!: trans[OF GE.map_cong_pred GE.map_ident]
    elim!: GE.pred_mono_strong
    cong: GE.pred_cong)
  done


section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_map_flat_Shape_tac ctxt induct Ps us Shape_injects Shape_maps flat_simps
    map_comps map_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong map_congs
        (ss_only (flat [Shape_maps, flat_simps, map_comps, Shape_injects, IHs, @{thms o_apply}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>
lemma map_flat_shape1_raw:
  "map_shape3A f g (flat_shape1A ua) = flat_shape1A (map_shape3A (map_F1A f g) (map_F1B f g) ua) \<and>
   map_shape3B f g (flat_shape1B ub) = flat_shape1B (map_shape3B (map_F1A f g) (map_F1B f g) ub)"
  apply (tactic \<open>mk_map_flat_Shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua. map_shape3A f g (flat_shape1A ua) = flat_shape1A (map_shape3A (map_F1A f g) (map_F1B f g) ua)"},
     @{cterm "\<lambda>ub. map_shape3B f g (flat_shape1B ub) = flat_shape1B (map_shape3B (map_F1A f g) (map_F1B f g) ub)"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms shape3A.inject shape3B.inject}
    @{thms shape3A.map shape3B.map}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp}
    @{thms F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. map_shape3A f g (flat_shape1A ua) = flat_shape1A (map_shape3A (map_F1A f g) (map_F1B f g) ua)"
    "\<lambda>ub. map_shape3B f g (flat_shape1B ub) = flat_shape1B (map_shape3B (map_F1A f g) (map_F1B f g) ub)" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map flat_shape1A.simps flat_shape1B.simps
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0)
  done*)

lemma map_flat_shape2_raw:
  "map_shape3A f g (flat_shape2A ua) = flat_shape2A (map_shape3A (map_F2A f g) (map_F2B f g) ua) \<and>
   map_shape3B f g (flat_shape2B ub) = flat_shape2B (map_shape3B (map_F2A f g) (map_F2B f g) ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. map_shape3A f g (flat_shape2A ua) = flat_shape2A (map_shape3A (map_F2A f g) (map_F2B f g) ua)"
    "\<lambda>ub. map_shape3B f g (flat_shape2B ub) = flat_shape2B (map_shape3B (map_F2A f g) (map_F2B f g) ub)" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map flat_shape2A.simps flat_shape2B.simps
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0)
  done

lemma map_flat_shape3_raw:
  "map_shape3A f g (flat_shape3A ua) = flat_shape3A (map_shape3A (map_E3A f g) (map_E3B f g) ua) \<and>
   map_shape3B f g (flat_shape3B ub) = flat_shape3B (map_shape3B (map_E3A f g) (map_E3B f g) ub)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. map_shape3A f g (flat_shape3A ua) = flat_shape3A (map_shape3A (map_E3A f g) (map_E3B f g) ua)"
    "\<lambda>ub. map_shape3B f g (flat_shape3B ub) = flat_shape3B (map_shape3B (map_E3A f g) (map_E3B f g) ub)" ua ub])
  apply (auto simp only:
      shape3A.map shape3B.map flat_shape3A.simps flat_shape3B.simps
      F1A.map_comp F2A.map_comp F1B.map_comp F2B.map_comp E3A.map_comp E3B.map_comp o_apply
    cong: F1A.map_cong0 F2A.map_cong0 F1B.map_cong0 F2B.map_cong0 E3A.map_cong0 E3B.map_cong0)
  done

lemmas map_flat_shape =
  conjunct1[OF map_flat_shape1_raw]
  conjunct2[OF map_flat_shape1_raw]
  conjunct1[OF map_flat_shape2_raw]
  conjunct2[OF map_flat_shape2_raw]
  conjunct1[OF map_flat_shape3_raw]
  conjunct2[OF map_flat_shape3_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_map_flat_raw_tac ctxt induct Ps us raw_maps flat_simps map_comps
    map_flat_shapes map_congs =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong map_congs
        (ss_only (flat [raw_maps, flat_simps, map_comps, map_flat_shapes, IHs, @{thms o_apply}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>

lemma map_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "map_rawF f g (flat1_rawF t) = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t) \<and>
   map_rawE f g (flat1_rawE s) = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
      fun mk_map_flat_raw_tac' ctxt Ps xs ys induct raw_exhausts raw_sels
        raw_maps flat_simps map_flat_shapes
        G_rel_maps G_rel_refl =
        let
          val n = length Ps
          val inst_cterms = Ps @ ([xs, ys] |> transpose |> flat);
        in
          HEADGOAL (EVERY' [rtac ctxt rev_mp,
            rtac ctxt (infer_instantiate' ctxt (map SOME inst_cterms) induct)]) THEN
          (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
            REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
            hyp_subst_tac ctxt,
            Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
              let
                val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 2)] exhaust;
                val intro = resolve_tac ctxt (exI :: G_rel_refl);
                val simp = asm_full_simp_tac (ss_only (flat [raw_sels, raw_maps, flat_simps,
                  map_flat_shapes, G_rel_maps, @{thms snoc.simps[symmetric] eq_onp_def}]) ctxt);
              in
                HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                  FIRST' [hyp_subst_tac ctxt, intro, simp]])
              end) ctxt])) raw_exhausts) THEN
          HEADGOAL (
            rtac ctxt impI THEN'
            CONJ_WRAP' (fn i => EVERY' [
              dtac ctxt (mk_conjunctN n i),
              etac ctxt mp,
              REPEAT_DETERM o FIRST' [
                resolve_tac ctxt [exI, conjI, refl],
                assume_tac ctxt]]) (1 upto n))
        end
      in
        mk_map_flat_raw_tac' @{context}
          [@{cterm "\<lambda>l r :: ('c, 'd) rawF. \<exists>t. l = map_rawF f g (flat1_rawF t) \<and> r = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"},
           @{cterm "\<lambda>l r :: ('c, 'd) rawE. \<exists>s. l = map_rawE f g (flat1_rawE s) \<and> r = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"}]
          [@{cterm "map_rawF f g (flat1_rawF t)"}, @{cterm "map_rawE f g (flat1_rawE s)"}]
          [@{cterm "flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"},
           @{cterm "flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"}]
          @{thm rawF_rawE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.sel rawE.sel}
          @{thms rawF.map rawE.map}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms map_flat_shape}
          @{thms GF.rel_map GE.rel_map}
          @{thms GF.rel_refl GE.rel_refl}
      end\<close>)
(*
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>t. l = map_rawF f g (flat1_rawF t) \<and> r = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"
    "\<lambda>l r. \<exists>s. l = map_rawE f g (flat1_rawE s) \<and> r = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"
    "map_rawF f g (flat1_rawF t)" "flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"
    "map_rawE f g (flat1_rawE s)" "flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: rawF.sel rawF.map flat1_simps GF.rel_map
      map_flat_shape intro!: GF.rel_refl)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: rawE.sel rawE.map flat1_simps GE.rel_map
      map_flat_shape intro!: GE.rel_refl)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done

  apply (tactic \<open>mk_map_flat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t. map_rawF f g (flat1_rawF t) = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"},
     @{cterm "\<lambda>s. map_rawE f g (flat1_rawE s) = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)"}]
    [@{cterm t}, @{cterm s}]
    @{thms rawF.map rawE.map}
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms GF.map_comp GE.map_comp}
    @{thms map_flat_shape}
    @{thms GF.map_cong0 GE.map_cong0}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. map_rawF f g (flat1_rawF t) = flat1_rawF (map_rawF (map_F1A f g) (map_F1B f g) t)"
    "\<lambda>s. map_rawE f g (flat1_rawE s) = flat1_rawE (map_rawE (map_F1A f g) (map_F1B f g) s)" t s])
  apply (simp_all only:
      rawF.map rawE.map flat1_rawF.simps flat1_rawE.simps GF.map_comp GE.map_comp map_flat_shape o_apply
    cong: GF.map_cong0 GE.map_cong0)
  done*)

lemma map_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "map_rawF f g (flat2_rawF t) = flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t) \<and>
   map_rawE f g (flat2_rawE s) = flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
      fun mk_map_flat_raw_tac' ctxt Ps xs ys induct raw_exhausts raw_sels
        raw_maps flat_simps map_flat_shapes
        G_rel_maps G_rel_refl =
        let
          val n = length Ps
          val inst_cterms = Ps @ ([xs, ys] |> transpose |> flat);
        in
          HEADGOAL (EVERY' [rtac ctxt rev_mp,
            rtac ctxt (infer_instantiate' ctxt (map SOME inst_cterms) induct)]) THEN
          (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
            REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
            hyp_subst_tac ctxt,
            Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
              let
                val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 2)] exhaust;
                val intro = resolve_tac ctxt (exI :: G_rel_refl);
                val simp = asm_full_simp_tac (ss_only (flat [raw_sels, raw_maps, flat_simps,
                  map_flat_shapes, G_rel_maps, @{thms snoc.simps[symmetric] eq_onp_def}]) ctxt);
              in
                HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                  FIRST' [hyp_subst_tac ctxt, intro, simp]])
              end) ctxt])) raw_exhausts) THEN
          HEADGOAL (
            rtac ctxt impI THEN'
            CONJ_WRAP' (fn i => EVERY' [
              dtac ctxt (mk_conjunctN n i),
              etac ctxt mp,
              REPEAT_DETERM o FIRST' [
                resolve_tac ctxt [exI, conjI, refl],
                assume_tac ctxt]]) (1 upto n))
        end
      in
        mk_map_flat_raw_tac' @{context}
          [@{cterm "\<lambda>l r :: ('c, 'd) rawF. \<exists>t. l = map_rawF f g (flat2_rawF t) \<and> r = flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t)"},
           @{cterm "\<lambda>l r :: ('c, 'd) rawE. \<exists>s. l = map_rawE f g (flat2_rawE s) \<and> r = flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)"}]
          [@{cterm "map_rawF f g (flat2_rawF t)"}, @{cterm "map_rawE f g (flat2_rawE s)"}]
          [@{cterm "flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t)"},
           @{cterm "flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)"}]
          @{thm rawF_rawE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.sel rawE.sel}
          @{thms rawF.map rawE.map}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms map_flat_shape}
          @{thms GF.rel_map GE.rel_map}
          @{thms GF.rel_refl GE.rel_refl}
      end\<close>)

(*
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>t. l = map_rawF f g (flat2_rawF t) \<and> r = flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t)"
    "\<lambda>l r. \<exists>s. l = map_rawE f g (flat2_rawE s) \<and> r = flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)"
    "map_rawF f g (flat2_rawF t)" "flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t)"
    "map_rawE f g (flat2_rawE s)" "flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ t
    apply (case_tac t)
    apply (auto simp only: rawF.sel rawF.map flat2_simps GF.rel_map
      map_flat_shape intro!: GF.rel_refl)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ s
    apply (case_tac s)
    apply (auto simp only: rawE.sel rawE.map flat2_simps GE.rel_map
      map_flat_shape intro!: GE.rel_refl)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. map_rawF f g (flat2_rawF t) = flat2_rawF (map_rawF (map_F2A f g) (map_F2B f g) t)"
    "\<lambda>s. map_rawE f g (flat2_rawE s) = flat2_rawE (map_rawE (map_F2A f g) (map_F2B f g) s)" t s])
  apply (simp_all only:
      rawF.map rawE.map flat2_rawF.simps flat2_rawE.simps GF.map_comp GE.map_comp map_flat_shape o_apply
    cong: GF.map_cong0 GE.map_cong0)
  done*)

lemma map_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "map_rawF f g (flat3_rawF t) = flat3_rawF (map_rawF (map_E3A f g) (map_E3B f g) t) \<and>
   map_rawE f g (flat3_rawE s) = flat3_rawE (map_rawE (map_E3A f g) (map_E3B f g) s)"
  apply (rule rev_mp)
  apply (rule rawF_rawE.coinduct[of
    "\<lambda>l r. \<exists>t. l = map_rawF f g (flat3_rawF t) \<and> r = flat3_rawF (map_rawF (map_E3A f g) (map_E3B f g) t)"
    "\<lambda>l r. \<exists>s. l = map_rawE f g (flat3_rawE s) \<and> r = flat3_rawE (map_rawE (map_E3A f g) (map_E3B f g) s)"
    "map_rawF f g (flat3_rawF t)" "flat3_rawF (map_rawF (map_E3A f g) (map_E3B f g) t)"
    "map_rawE f g (flat3_rawE s)" "flat3_rawE (map_rawE (map_E3A f g) (map_E3B f g) s)"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ t
    apply (case_tac t)
    apply (auto simp only: rawF.sel rawF.map flat3_simps GF.rel_map
      map_flat_shape intro!: GF.rel_refl)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ s
    apply (case_tac s)
    apply (auto simp only: rawE.sel rawE.map flat3_simps GE.rel_map
      map_flat_shape intro!: GE.rel_refl)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, erule mp, (rule exI conjI refl | assumption)+)
  apply (drule conjunct2, erule mp, (rule exI conjI refl | assumption)+)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. map_rawF f g (flat3_rawF t) = flat3_rawF (map_rawF (map_E3A f g) (map_E3B f g) t)"
    "\<lambda>s. map_rawE f g (flat3_rawE s) = flat3_rawE (map_rawE (map_E3A f g) (map_E3B f g) s)" t s])
  apply (simp_all only:
      rawF.map rawE.map flat3_rawF.simps flat3_rawE.simps GF.map_comp GE.map_comp map_flat_shape o_apply
    cong: GF.map_cong0 GE.map_cong0)
  done*)

lemmas map_flat =
  conjunct1[OF map_flat1_raw]
  conjunct2[OF map_flat1_raw]
  conjunct1[OF map_flat2_raw]
  conjunct2[OF map_flat2_raw]
  conjunct1[OF map_flat3_raw]
  conjunct2[OF map_flat3_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_map_T_eq_tac ctxt T_map_defs T_ctr_def T_Abs_inverses
        raw_maps Shape_maps map_flat_raws T_Reps invar_raw_simps
        invar_Shape_simps invar_map_raw_closeds invar_flat_raws
        pred_maps pred_Trues map_comps pred_congs map_congs =
  unfold_tac ctxt (T_ctr_def :: T_map_defs) THEN
  HEADGOAL (full_simp_tac (fold Simplifier.add_cong (pred_congs @ map_congs)
        (ss_only (flat [T_Abs_inverses, raw_maps, Shape_maps, map_flat_raws,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, invar_raw_simps,
            invar_Shape_simps, invar_map_raw_closeds, invar_flat_raws,
            pred_maps, pred_Trues, map_comps,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt)));
end
\<close>

lemma map_T: "map_T f g (T t) =
  T (map_GF f g (map_T (map_F1A f g) (map_F1B f g)) (map_T (map_F2A f g) (map_F2B f g)) (map_S (map_E3A f g) (map_E3B f g)) t)"
  apply (tactic \<open>mk_map_T_eq_tac @{context}
    @{thms map_T_def map_S_def}
    @{thm T_def}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms rawF.map rawE.map}
    @{thms shape3A.map shape3B.map}
    @{thms map_flat}
    @{thms Rep_T Rep_S}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.map_comp GE.map_comp}
    @{thms GF.pred_cong GE.pred_cong}
    @{thms GF.map_cong0 GE.map_cong0}\<close>)
  done
(*
  unfolding map_T_def map_S_def T_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawF.map rawE.map shape3A.map shape3B.map map_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF.simps
      invar_shape3A.simps invar_shape3B.simps
      invar_map_closed invar_flat
      GF.pred_map GF.pred_True GF.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong GF.map_cong)
  done*)

lemma map_S: "map_S f g (S t) =
  S (map_GE f g (map_T (map_F1A f g) (map_F1B f g)) (map_T (map_F2A f g) (map_F2B f g)) (map_S (map_E3A f g) (map_E3B f g)) t)"
  unfolding map_T_def map_S_def S_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawF.map rawE.map shape3A.map shape3B.map map_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF_simps invar_rawE_simps
      invar_shape3A.simps invar_shape3B.simps
      invar_map_closed invar_flat
      GE.pred_map GE.pred_True GE.map_comp
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong GE.map_cong)
  done

subsection \<open>set\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_set_flat_Shape_tac ctxt induct Ps us Shape_sets flat_simps set_maps =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong @{thms SUP_cong}
        (ss_only (flat [Shape_sets, flat_simps, set_maps, IHs,
        @{thms UN_simps UN_singleton UN_insert UN_empty UN_empty2
          UN_Un UN_Un_distrib Un_ac Un_empty_left}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>

lemma set1_flat_shape1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(set1_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set1_F1A \<union> UNION (set2_shape3A ua) set1_F1B) \<and>
   (set1_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set1_F1A \<union> UNION (set2_shape3B ub) set1_F1B)"
  apply (tactic \<open>mk_set_flat_Shape_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua :: (('a, 'b) F1A, ('a, 'b) F1B) shape3A. set1_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set1_F1A \<union> UNION (set2_shape3A ua) set1_F1B"},
     @{cterm "\<lambda>ub :: (('a, 'b) F1A, ('a, 'b) F1B) shape3B. set1_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set1_F1A \<union> UNION (set2_shape3B ub) set1_F1B"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms shape3A.set shape3B.set}
    @{thms F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set1_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set1_F1A \<union> UNION (set2_shape3A ua) set1_F1B"
    "\<lambda>ub. set1_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set1_F1A \<union> UNION (set2_shape3B ub) set1_F1B" ua ub])
  apply (simp_all only:
      flat_shape1A.simps flat_shape1B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done*)

lemma set1_flat_shape2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(set1_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set1_F2A \<union> UNION (set2_shape3A ua) set1_F2B) \<and>
   (set1_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set1_F2A \<union> UNION (set2_shape3B ub) set1_F2B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set1_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set1_F2A \<union> UNION (set2_shape3A ua) set1_F2B"
    "\<lambda>ub. set1_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set1_F2A \<union> UNION (set2_shape3B ub) set1_F2B" ua ub])
  apply (simp_all only:
      flat_shape2A.simps flat_shape2B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set1_flat_shape3_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(set1_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set1_E3A \<union> UNION (set2_shape3A ua) set1_E3B) \<and>
   (set1_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set1_E3A \<union> UNION (set2_shape3B ub) set1_E3B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set1_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set1_E3A \<union> UNION (set2_shape3A ua) set1_E3B"
    "\<lambda>ub. set1_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set1_E3A \<union> UNION (set2_shape3B ub) set1_E3B" ua ub])
  apply (simp_all only:
      flat_shape3A.simps flat_shape3B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set2_flat_shape1_raw:
  fixes ua :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3A" and ub :: "(('a, 'b) F1A, ('a, 'b) F1B) shape3B"
  shows
  "(set2_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set2_F1A \<union> UNION (set2_shape3A ua) set2_F1B) \<and>
   (set2_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set2_F1A \<union> UNION (set2_shape3B ub) set2_F1B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set2_shape3A (flat_shape1A ua) = UNION (set1_shape3A ua) set2_F1A \<union> UNION (set2_shape3A ua) set2_F1B"
    "\<lambda>ub. set2_shape3B (flat_shape1B ub) = UNION (set1_shape3B ub) set2_F1A \<union> UNION (set2_shape3B ub) set2_F1B" ua ub])
  apply (simp_all only:
      flat_shape1A.simps flat_shape1B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set2_flat_shape2_raw:
  fixes ua :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3A" and ub :: "(('a, 'b) F2A, ('a, 'b) F2B) shape3B"
  shows
  "(set2_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set2_F2A \<union> UNION (set2_shape3A ua) set2_F2B) \<and>
   (set2_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set2_F2A \<union> UNION (set2_shape3B ub) set2_F2B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set2_shape3A (flat_shape2A ua) = UNION (set1_shape3A ua) set2_F2A \<union> UNION (set2_shape3A ua) set2_F2B"
    "\<lambda>ub. set2_shape3B (flat_shape2B ub) = UNION (set1_shape3B ub) set2_F2A \<union> UNION (set2_shape3B ub) set2_F2B" ua ub])
  apply (simp_all only:
      flat_shape2A.simps flat_shape2B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma set2_flat_shape3_raw:
  fixes ua :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3A" and ub :: "(('a, 'b) E3A, ('a, 'b) E3B) shape3B"
  shows
  "(set2_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set2_E3A \<union> UNION (set2_shape3A ua) set2_E3B) \<and>
   (set2_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set2_E3A \<union> UNION (set2_shape3B ub) set2_E3B)"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. set2_shape3A (flat_shape3A ua) = UNION (set1_shape3A ua) set2_E3A \<union> UNION (set2_shape3A ua) set2_E3B"
    "\<lambda>ub. set2_shape3B (flat_shape3B ub) = UNION (set1_shape3B ub) set2_E3A \<union> UNION (set2_shape3B ub) set2_E3B" ua ub])
  apply (simp_all only:
      flat_shape3A.simps flat_shape3B.simps shape3A.set shape3B.set
      F1A.set_map F2A.set_map F1B.set_map F2B.set_map E3A.set_map E3B.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemmas set_flat_shape =
  conjunct1[OF set1_flat_shape1_raw]
  conjunct1[OF set2_flat_shape1_raw]
  conjunct2[OF set1_flat_shape1_raw]
  conjunct2[OF set2_flat_shape1_raw]
  conjunct1[OF set1_flat_shape2_raw]
  conjunct1[OF set2_flat_shape2_raw]
  conjunct2[OF set1_flat_shape2_raw]
  conjunct2[OF set2_flat_shape2_raw]
  conjunct1[OF set1_flat_shape3_raw]
  conjunct1[OF set2_flat_shape3_raw]
  conjunct2[OF set1_flat_shape3_raw]
  conjunct2[OF set2_flat_shape3_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_set_flat_raw_tac ctxt induct Ps us raw_sets flat_simps set_maps set_flat_Shapes =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val simp = full_simp_tac (fold Simplifier.add_cong @{thms SUP_cong}
        (ss_only (flat [raw_sets, flat_simps, set_maps, set_flat_Shapes, IHs,
        @{thms UN_simps UN_singleton UN_insert UN_empty UN_empty2
          UN_Un UN_Un_distrib Un_ac Un_empty_left}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, simp])
    end) ctxt);
end
\<close>

lemma set1_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "set1_rawF (flat1_rawF t) = UNION (set1_rawF t) set1_F1A \<union> UNION (set2_rawF t) set1_F1B \<and>
   set1_rawE (flat1_rawE s) = UNION (set1_rawE s) set1_F1A \<union> UNION (set2_rawE s) set1_F1B"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_Util open BNF_FP_Rec_Sugar_Util
        fun mk_set_flat_raw_cotac ctxt lemma Pss us inducts nr_occ raw_exhausts raw_injects raw_sets
          flat_simps set_flat_shapes G_set_maps =
          let
            val nr_par = (length Pss);
            val nr_Ts = length (hd Pss);
            val nr_lives = nr_par + nr_occ;
            fun case_mut a b = if nr_Ts <> 1 then a else b;
            val rep_raw_exhausts = map (replicate nr_lives) raw_exhausts |> flat;
            val UnI_pars = map (mk_UnIN nr_Ts) (1 upto nr_par);
            val UnI_Gs = map (mk_UnIN nr_lives) (1 upto nr_lives);
            val solve = EVERY' [rtac ctxt equalityI, rtac ctxt subsetI,
              etac ctxt allE, dtac ctxt bspec, assume_tac ctxt,
              etac ctxt allE, dtac ctxt mp, rtac ctxt refl, assume_tac ctxt];
          in
            HEADGOAL (EVERY' [rtac ctxt (infer_instantiate' ctxt [SOME lemma] rev_mp),
              REPEAT_DETERM_N nr_Ts o rtac ctxt allI,
              case_mut (rtac ctxt (hd inducts))
              (rtac ctxt ballI THEN' etac ctxt (hd inducts)) THEN_ALL_NEW
              REPEAT_DETERM o resolve_tac ctxt [allI, impI],
              RANGE (map (fn exhaust =>
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd o List.last) params] exhaust;
                in
                  HEADGOAL (rtac ctxt exhaust_inst)
                end) ctxt THEN' REPEAT_DETERM o
                let
                  val elim = eresolve_tac ctxt (imageE :: UnE :: @{thms UN_E});
                  val dest_IH = etac ctxt allE THEN' dtac ctxt mp THEN' rtac ctxt refl;
                  val solve = EVERY' [resolve_tac ctxt UnI_pars, etac ctxt @{thm UN_I[rotated]},
                    resolve_tac ctxt UnI_Gs, etac ctxt @{thm UN_I[rotated]}, assume_tac ctxt];
                  val simp = CHANGED o asm_full_simp_tac
                    (ss_only (flat [raw_sets, raw_injects, flat_simps, set_flat_shapes,
                      G_set_maps]) ctxt);
                in
                  FIRST' [hyp_subst_tac ctxt, elim, solve, dest_IH, simp]
                end) rep_raw_exhausts)]) THEN
            HEADGOAL (case_mut (K all_tac) (EVERY' [
              rtac ctxt impI,
              solve,
              rtac ctxt subsetI,
              etac ctxt @{thm UN_E}])) THEN
            HEADGOAL (EVERY' (map2 (fn induct => fn Ps =>
              EVERY' [rtac ctxt rev_mp,
                let
                  val instantiate_args = case_mut (map SOME (Ps @ us)) (NONE :: map SOME (us @ Ps));
                  val dest_IH = dtac ctxt bspec THEN' assume_tac ctxt;
                  val solve = EVERY' [resolve_tac ctxt UnI_Gs, etac ctxt @{thm UN_I}] THEN'
                    TRY o EVERY' [resolve_tac ctxt UnI_pars,
                    etac ctxt @{thm UN_I[rotated]}, assume_tac ctxt];
                  val intro = resolve_tac ctxt (ballI :: []);
                  val simp = CHANGED o asm_full_simp_tac (ss_only (flat [raw_sets, flat_simps,
                    set_flat_shapes, G_set_maps, @{thms UN_simps}]) ctxt);
                in
                  rtac ctxt (infer_instantiate' ctxt instantiate_args induct) THEN_ALL_NEW
                  REPEAT_DETERM o FIRST' [intro, solve, dest_IH, simp]
                end]) inducts Pss)) THEN
            case_mut (unfold_tac ctxt @{thms all_simps} THEN
            HEADGOAL (EVERY' [REPEAT_DETERM o rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN nr_Ts i),
                EVERY' (replicate nr_par (dtac ctxt (mk_conjunctN nr_Ts i))),
                solve,
                rtac ctxt subsetI,
                REPEAT_DETERM o etac ctxt UnE,
                EVERY' (replicate nr_par (EVERY' [
                  etac ctxt @{thm UN_E},
                  REPEAT_DETERM o (dtac ctxt bspec THEN' assume_tac ctxt),
                  assume_tac ctxt]))
              ]) (1 upto nr_Ts)])) (HEADGOAL (REPEAT_DETERM_N nr_par o EVERY' [
                rtac ctxt impI,
                REPEAT_DETERM o (dtac ctxt bspec THEN' assume_tac ctxt),
                assume_tac ctxt]))
          end
      in
        mk_set_flat_raw_cotac @{context}
          @{cterm "\<forall>t :: ('a, 'b) rawF. \<forall>s :: ('a, 'b) rawE.
            (\<forall>y \<in> set1_rawF t. \<forall>t'. flat1_rawF t' = t \<longrightarrow> y \<in> UNION (set1_rawF t') set1_F1A \<union> UNION (set2_rawF t') set1_F1B) \<and>
            (\<forall>y \<in> set1_rawE s. \<forall>s'. flat1_rawE s' = s \<longrightarrow> y \<in> UNION (set1_rawE s') set1_F1A \<union> UNION (set2_rawE s') set1_F1B)"}
          [[@{cterm "\<lambda>f :: ('a, 'b) F1A. \<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>a \<in> set1_F1A f. a \<in> set1_rawF (flat1_rawF t)"},
            @{cterm "\<lambda>f :: ('a, 'b) F1A. \<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>a \<in> set1_F1A f. a \<in> set1_rawE (flat1_rawE s)"}],
           [@{cterm "\<lambda>f :: ('a, 'b) F1B. \<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>a \<in> set1_F1B f. a \<in> set1_rawF (flat1_rawF t)"},
            @{cterm "\<lambda>f :: ('a, 'b) F1B. \<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>a \<in> set1_F1B f. a \<in> set1_rawE (flat1_rawE s)"}]]
          [@{cterm t}, @{cterm s}]
          @{thms rawF_rawE.set_induct}
          3
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.inject rawE.inject}
          @{thms rawF.set rawE.set}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms set_flat_shape}
          @{thms GF.set_map GE.set_map}
      end\<close>)
thm rawF_rawE.set_induct
lemma set1_flat1_raw':
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "set1_rawF (flat1_rawF t) = UNION (set1_rawF t) set1_F1A \<union> UNION (set2_rawF t) set1_F1B \<and>
   set1_rawE (flat1_rawE s) = UNION (set1_rawE s) set1_F1A \<union> UNION (set2_rawE s) set1_F1B"
  apply (rule rev_mp[of "\<forall>t :: ('a, 'b) rawF. \<forall>s :: ('a, 'b) rawE.
    (\<forall>y \<in> set1_rawF t. \<forall>t'. flat1_rawF t' = t \<longrightarrow> y \<in> UNION (set1_rawF t') set1_F1A \<union> UNION (set2_rawF t') set1_F1B) \<and>
    (\<forall>y \<in> set1_rawE s. \<forall>s'. flat1_rawE s' = s \<longrightarrow> y \<in> UNION (set1_rawE s') set1_F1A \<union> UNION (set2_rawE s') set1_F1B)"])
  apply (rule allI)
  apply (rule allI)
thm rawF_rawE.set_induct(1)[of
    "\<lambda>f. \<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>t'. flat1_rawF t' = t \<longrightarrow> f \<in> UNION (set1_rawF t') set1_F1A \<union> UNION (set2_rawF t') set1_F1B"
    "\<lambda>f. \<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>s'. flat1_rawE s' = s \<longrightarrow> f \<in> UNION (set1_rawE s') set1_F1A \<union> UNION (set2_rawE s') set1_F1B"
    "t:: (('a, 'b) F1A, ('a, 'b) F1B) rawF"
    "s:: (('a, 'b) F1A, ('a, 'b) F1B) rawE"]
  apply (rule rawF_rawE.set_induct(1); (rule allI impI)+)
  subgoal for _ _ _ _ _ t
    apply (rule rawF.exhaust[of t])
    apply (auto 0 4 simp only:
        flat1_simps rawF.set GF.set_map set_flat_shape
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
    apply (rule rawF.exhaust[of t])
    apply (auto 0 4 simp only:
        flat1_simps rawF.set GF.set_map set_flat_shape
        UN_simps UN_Un UN_Un_distrib Un_ac
      cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a s
  apply (rule rawE.exhaust[of s])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a s
  apply (rule rawE.exhaust[of s])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a s
  apply (rule rawE.exhaust[of s])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a s
  apply (rule rawE.exhaust[of s])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a s
  apply (rule rawE.exhaust[of s])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done

  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(1)[of
      "\<lambda>f t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>a \<in> set1_F1A f. a \<in> set1_rawF (flat1_rawF t)"
      "\<lambda>f s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>a \<in> set1_F1A f. a \<in> set1_rawE (flat1_rawE s)" t s];
    auto simp only: flat1_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(2)[of
      "\<lambda>f t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>a \<in> set1_F1B f. a \<in> set1_rawF (flat1_rawF t)"
      "\<lambda>f s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>a \<in> set1_F1B f. a \<in> set1_rawE (flat1_rawE s)" t s];
    auto simp only: flat1_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)

  apply (unfold all_simps)
  apply (rule impI)
  apply (rule impI)
  apply (rule impI)
  apply (rule conjI)

  apply (drule conjunct1)+
  apply (rule equalityI)
  apply (rule subsetI)
  apply (erule allE, drule bspec, assumption)
  apply (erule allE)
  apply (drule mp)
  apply (rule refl)
  apply assumption
  apply (rule subsetI)
  apply (erule UnE)+
   apply (erule UN_E)
   apply (drule bspec, assumption)+
   apply assumption
   apply (erule UN_E)
   apply (drule bspec, assumption)+
   apply assumption

  apply (drule conjunct2)+
  apply (rule equalityI)
  apply (rule subsetI)
  apply (erule allE, drule bspec, assumption)
  apply (erule allE)
  apply (drule mp)
  apply (rule refl)
  apply assumption
  apply (rule subsetI)
  apply (erule UnE)+
   apply (erule UN_E)
   apply (drule bspec, assumption)+
   apply assumption
   apply (erule UN_E)
   apply (drule bspec, assumption)+
   apply assumption
  done
(*
  apply (tactic \<open>mk_set_flat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. set1_rawF (flat1_rawF t) = UNION (set1_rawF t) set1_F1A \<union> UNION (set2_rawF t) set1_F1B"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. set1_rawE (flat1_rawE s) = UNION (set1_rawE s) set1_F1A \<union> UNION (set2_rawE s) set1_F1B"}]
    [@{cterm t}, @{cterm s}]
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms rawF.set rawE.set}
    @{thms GF.set_map GE.set_map}
    @{thms set_flat_shape}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set1_rawF (flat1_rawF t) = UNION (set1_rawF t) set1_F1A \<union> UNION (set2_rawF t) set1_F1B"
    "\<lambda>s. set1_rawE (flat1_rawE s) = UNION (set1_rawE s) set1_F1A \<union> UNION (set2_rawE s) set1_F1B" t s])
  apply (simp_all only: flat1_rawF.simps flat1_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done*)

lemma set2_flat1_raw:
  fixes t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "set2_rawF (flat1_rawF t) = UNION (set1_rawF t) set2_F1A \<union> UNION (set2_rawF t) set2_F1B \<and>
   set2_rawE (flat1_rawE s) = UNION (set1_rawE s) set2_F1A \<union> UNION (set2_rawE s) set2_F1B"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_Util open BNF_FP_Rec_Sugar_Util
        fun mk_set_flat_raw_cotac ctxt lemma Pss us inducts nr_occ raw_exhausts raw_injects raw_sets
          flat_simps set_flat_shapes G_set_maps =
          let
            val nr_par = (length Pss);
            val nr_Ts = length (hd Pss);
            val nr_lives = nr_par + nr_occ;
            fun case_mut a b = if nr_Ts <> 1 then a else b;
            val rep_raw_exhausts = map (replicate nr_lives) raw_exhausts |> flat;
            val UnI_pars = map (mk_UnIN nr_Ts) (1 upto nr_par);
            val UnI_Gs = map (mk_UnIN nr_lives) (1 upto nr_lives);
            val solve = EVERY' [rtac ctxt equalityI, rtac ctxt subsetI,
              etac ctxt allE, dtac ctxt bspec, assume_tac ctxt,
              etac ctxt allE, dtac ctxt mp, rtac ctxt refl, assume_tac ctxt];
          in
            HEADGOAL (EVERY' [rtac ctxt (infer_instantiate' ctxt [SOME lemma] rev_mp),
              REPEAT_DETERM_N nr_Ts o rtac ctxt allI,
              case_mut (rtac ctxt (hd (tl inducts)))
              (rtac ctxt ballI THEN' etac ctxt (hd inducts)) THEN_ALL_NEW
              REPEAT_DETERM o resolve_tac ctxt [allI, impI],
              RANGE (map (fn exhaust =>
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd o List.last) params] exhaust;
                in
                  HEADGOAL (rtac ctxt exhaust_inst)
                end) ctxt THEN' REPEAT_DETERM o
                let
                  val elim = eresolve_tac ctxt (imageE :: UnE :: @{thms UN_E});
                  val dest_IH = etac ctxt allE THEN' dtac ctxt mp THEN' rtac ctxt refl;
                  val solve = EVERY' [resolve_tac ctxt UnI_pars, etac ctxt @{thm UN_I[rotated]},
                    resolve_tac ctxt UnI_Gs, etac ctxt @{thm UN_I[rotated]}, assume_tac ctxt];
                  val simp = CHANGED o asm_full_simp_tac
                    (ss_only (flat [raw_sets, raw_injects, flat_simps, set_flat_shapes,
                      G_set_maps]) ctxt);
                in
                  FIRST' [hyp_subst_tac ctxt, elim, solve, dest_IH, simp]
                end) rep_raw_exhausts)]) THEN
            HEADGOAL (case_mut (K all_tac) (EVERY' [
              rtac ctxt impI,
              solve,
              rtac ctxt subsetI,
              etac ctxt @{thm UN_E}])) THEN
            HEADGOAL (EVERY' (map2 (fn induct => fn Ps =>
              EVERY' [rtac ctxt rev_mp,
                let
                  val instantiate_args = case_mut (map SOME (Ps @ us)) (NONE :: map SOME (us @ Ps));
                  val dest_IH = dtac ctxt bspec THEN' assume_tac ctxt;
                  val solve = EVERY' [resolve_tac ctxt UnI_Gs, etac ctxt @{thm UN_I}] THEN'
                    TRY o EVERY' [resolve_tac ctxt UnI_pars,
                    etac ctxt @{thm UN_I[rotated]}, assume_tac ctxt];
                  val intro = resolve_tac ctxt (ballI :: []);
                  val simp = CHANGED o asm_full_simp_tac (ss_only (flat [raw_sets, flat_simps,
                    set_flat_shapes, G_set_maps, @{thms UN_simps}]) ctxt);
                in
                  rtac ctxt (infer_instantiate' ctxt instantiate_args induct) THEN_ALL_NEW
                  REPEAT_DETERM o FIRST' [intro, solve, dest_IH, simp]
                end]) inducts Pss)) THEN
            case_mut (unfold_tac ctxt @{thms all_simps} THEN
            HEADGOAL (EVERY' [REPEAT_DETERM o rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN nr_Ts i),
                EVERY' (replicate nr_par (dtac ctxt (mk_conjunctN nr_Ts i))),
                solve,
                rtac ctxt subsetI,
                REPEAT_DETERM o etac ctxt UnE,
                EVERY' (replicate nr_par (EVERY' [
                  etac ctxt @{thm UN_E},
                  REPEAT_DETERM o (dtac ctxt bspec THEN' assume_tac ctxt),
                  assume_tac ctxt]))
              ]) (1 upto nr_Ts)])) (HEADGOAL (REPEAT_DETERM_N nr_par o EVERY' [
                rtac ctxt impI,
                REPEAT_DETERM o (dtac ctxt bspec THEN' assume_tac ctxt),
                assume_tac ctxt]))
          end
      in
        mk_set_flat_raw_cotac @{context}
          @{cterm "\<forall>t :: ('a, 'b) rawF. \<forall>s :: ('a, 'b) rawE.
            (\<forall>y \<in> set2_rawF t. \<forall>t'. flat1_rawF t' = t \<longrightarrow> y \<in> UNION (set1_rawF t') set2_F1A \<union> UNION (set2_rawF t') set2_F1B) \<and>
            (\<forall>y \<in> set2_rawE s. \<forall>s'. flat1_rawE s' = s \<longrightarrow> y \<in> UNION (set1_rawE s') set2_F1A \<union> UNION (set2_rawE s') set2_F1B)"}
          [[@{cterm "\<lambda>f :: ('a, 'b) F1A. \<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>a \<in> set2_F1A f. a \<in> set2_rawF (flat1_rawF t)"},
            @{cterm "\<lambda>f :: ('a, 'b) F1A. \<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>a \<in> set2_F1A f. a \<in> set2_rawE (flat1_rawE s)"}],
           [@{cterm "\<lambda>f :: ('a, 'b) F1B. \<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>a \<in> set2_F1B f. a \<in> set2_rawF (flat1_rawF t)"},
            @{cterm "\<lambda>f :: ('a, 'b) F1B. \<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>a \<in> set2_F1B f. a \<in> set2_rawE (flat1_rawE s)"}]]
          [@{cterm t}, @{cterm s}]
          @{thms rawF_rawE.set_induct}
          3
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.inject rawE.inject}
          @{thms rawF.set rawE.set}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms set_flat_shape}
          @{thms GF.set_map GE.set_map}
      end\<close>)
(*
  apply (rule rev_mp[of "\<forall>t' s'. (\<forall>y \<in> set2_rawF t'. \<forall>t. flat1_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set2_F1A \<union> UNION (set2_rawF t) set2_F1B) \<and>
                                 (\<forall>y \<in> set2_rawE s'. \<forall>s. flat1_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set2_F1A \<union> UNION (set2_rawE s) set2_F1B)"])
  apply (rule allI)
  apply (rule allI)
thm rawF_rawE.set_induct(2)[of
    "\<lambda>y t. \<forall>t. flat1_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set2_F1A \<union> UNION (set2_rawF t) set2_F1B"
    "\<lambda>y s. \<forall>s. flat1_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set2_F1A \<union> UNION (set2_rawE s) set2_F1B"
    "t':: (('a, 'b) F1A, ('a, 'b) F1B) rawF"  
    "s':: (('a, 'b) F1A, ('a, 'b) F1B) rawE"]
  apply (rule rawF_rawE.set_induct(2); (rule allI impI)+)
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat1_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done

  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(1)[of
      "\<lambda>x t. \<forall>a \<in> set2_F1A x. a \<in> set2_rawF (flat1_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set2_F1A x. a \<in> set2_rawE (flat1_rawE s)" t s];
    auto simp only: flat1_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(2)[of
      "\<lambda>x t. \<forall>a \<in> set2_F1B x. a \<in> set2_rawF (flat1_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set2_F1B x. a \<in> set2_rawE (flat1_rawE s)" t s];
    auto simp only: flat1_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply slow
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set2_rawF (flat1_rawF t) = UNION (set1_rawF t) set2_F1A \<union> UNION (set2_rawF t) set2_F1B"
    "\<lambda>s. set2_rawE (flat1_rawE s) = UNION (set1_rawE s) set2_F1A \<union> UNION (set2_rawE s) set2_F1B" t s])
  apply (simp_all only: flat1_rawF.simps flat1_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done*)

lemma set1_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "set1_rawF (flat2_rawF t) = UNION (set1_rawF t) set1_F2A \<union> UNION (set2_rawF t) set1_F2B \<and>
   set1_rawE (flat2_rawE s) = UNION (set1_rawE s) set1_F2A \<union> UNION (set2_rawE s) set1_F2B"
  apply (rule rev_mp[of "\<forall>t' s'. (\<forall>y \<in> set1_rawF t'. \<forall>t. flat2_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set1_F2A \<union> UNION (set2_rawF t) set1_F2B) \<and>
                                 (\<forall>y \<in> set1_rawE s'. \<forall>s. flat2_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set1_F2A \<union> UNION (set2_rawE s) set1_F2B)"])
  apply (rule allI)
  apply (rule allI)
thm rawF_rawE.set_induct(1)[of
    "\<lambda>y t. \<forall>t. flat2_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set1_F2A \<union> UNION (set2_rawF t) set1_F2B"
    "\<lambda>y s. \<forall>s. flat2_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set1_F2A \<union> UNION (set2_rawE s) set1_F2B"
    "t':: (('a, 'b) F2A, ('a, 'b) F2B) rawF"  
    "s':: (('a, 'b) F2A, ('a, 'b) F2B) rawE"]
  apply (rule rawF_rawE.set_induct(1); (rule allI impI)+)
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done

  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(1)[of
      "\<lambda>x t. \<forall>a \<in> set1_F2A x. a \<in> set1_rawF (flat2_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set1_F2A x. a \<in> set1_rawE (flat2_rawE s)" t s];
    auto simp only: flat2_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(2)[of
      "\<lambda>x t. \<forall>a \<in> set1_F2B x. a \<in> set1_rawF (flat2_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set1_F2B x. a \<in> set1_rawE (flat2_rawE s)" t s];
    auto simp only: flat2_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply slow
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set1_rawF (flat2_rawF t) = UNION (set1_rawF t) set1_F2A \<union> UNION (set2_rawF t) set1_F2B"
    "\<lambda>s. set1_rawE (flat2_rawE s) = UNION (set1_rawE s) set1_F2A \<union> UNION (set2_rawE s) set1_F2B" t s])
  apply (simp_all only: flat2_rawF.simps flat2_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done*)

lemma set2_flat2_raw:
  fixes t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "set2_rawF (flat2_rawF t) = UNION (set1_rawF t) set2_F2A \<union> UNION (set2_rawF t) set2_F2B \<and>
   set2_rawE (flat2_rawE s) = UNION (set1_rawE s) set2_F2A \<union> UNION (set2_rawE s) set2_F2B"
  apply (rule rev_mp[of "\<forall>t' s'. (\<forall>y \<in> set2_rawF t'. \<forall>t. flat2_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set2_F2A \<union> UNION (set2_rawF t) set2_F2B) \<and>
                                 (\<forall>y \<in> set2_rawE s'. \<forall>s. flat2_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set2_F2A \<union> UNION (set2_rawE s) set2_F2B)"])
  apply (rule allI)
  apply (rule allI)
thm rawF_rawE.set_induct(2)[of
    "\<lambda>y t. \<forall>t. flat2_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set2_F2A \<union> UNION (set2_rawF t) set2_F2B"
    "\<lambda>y s. \<forall>s. flat2_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set2_F2A \<union> UNION (set2_rawE s) set2_F2B"
    "t':: (('a, 'b) F2A, ('a, 'b) F2B) rawF"  
    "s':: (('a, 'b) F2A, ('a, 'b) F2B) rawE"]
  apply (rule rawF_rawE.set_induct(2); (rule allI impI)+)
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat2_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done

  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(1)[of
      "\<lambda>x t. \<forall>a \<in> set2_F2A x. a \<in> set2_rawF (flat2_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set2_F2A x. a \<in> set2_rawE (flat2_rawE s)" t s];
    auto simp only: flat2_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(2)[of
      "\<lambda>x t. \<forall>a \<in> set2_F2B x. a \<in> set2_rawF (flat2_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set2_F2B x. a \<in> set2_rawE (flat2_rawE s)" t s];
    auto simp only: flat2_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply slow
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set2_rawF (flat2_rawF t) = UNION (set1_rawF t) set2_F2A \<union> UNION (set2_rawF t) set2_F2B"
    "\<lambda>s. set2_rawE (flat2_rawE s) = UNION (set1_rawE s) set2_F2A \<union> UNION (set2_rawE s) set2_F2B" t s])
  apply (simp_all only: flat2_rawF.simps flat2_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done*)

lemma set1_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "set1_rawF (flat3_rawF t) = UNION (set1_rawF t) set1_E3A \<union> UNION (set2_rawF t) set1_E3B \<and>
   set1_rawE (flat3_rawE s) = UNION (set1_rawE s) set1_E3A \<union> UNION (set2_rawE s) set1_E3B"
  apply (rule rev_mp[of "\<forall>t' s'. (\<forall>y \<in> set1_rawF t'. \<forall>t. flat3_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set1_E3A \<union> UNION (set2_rawF t) set1_E3B) \<and>
                                 (\<forall>y \<in> set1_rawE s'. \<forall>s. flat3_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set1_E3A \<union> UNION (set2_rawE s) set1_E3B)"])
  apply (rule allI)
  apply (rule allI)
thm rawF_rawE.set_induct(1)[of
    "\<lambda>y t. \<forall>t. flat3_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set1_E3A \<union> UNION (set2_rawF t) set1_E3B"
    "\<lambda>y s. \<forall>s. flat3_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set1_E3A \<union> UNION (set2_rawE s) set1_E3B"
    "t':: (('a, 'b) E3A, ('a, 'b) E3B) rawF"  
    "s':: (('a, 'b) E3A, ('a, 'b) E3B) rawE"]
  apply (rule rawF_rawE.set_induct(1); (rule allI impI)+)
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done

  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(1)[of
      "\<lambda>x t. \<forall>a \<in> set1_E3A x. a \<in> set1_rawF (flat3_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set1_E3A x. a \<in> set1_rawE (flat3_rawE s)" t s];
    auto simp only: flat3_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(2)[of
      "\<lambda>x t. \<forall>a \<in> set1_E3B x. a \<in> set1_rawF (flat3_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set1_E3B x. a \<in> set1_rawE (flat3_rawE s)" t s];
    auto simp only: flat3_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply slow
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set1_rawF (flat3_rawF t) = UNION (set1_rawF t) set1_E3A \<union> UNION (set2_rawF t) set1_E3B"
    "\<lambda>s. set1_rawE (flat3_rawE s) = UNION (set1_rawE s) set1_E3A \<union> UNION (set2_rawE s) set1_E3B" t s])
  apply (simp_all only: flat3_rawF.simps flat3_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done*)

lemma set2_flat3_raw:
  fixes t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "set2_rawF (flat3_rawF t) = UNION (set1_rawF t) set2_E3A \<union> UNION (set2_rawF t) set2_E3B \<and>
   set2_rawE (flat3_rawE s) = UNION (set1_rawE s) set2_E3A \<union> UNION (set2_rawE s) set2_E3B"
  apply (rule rev_mp[of "\<forall>t' s'. (\<forall>y \<in> set2_rawF t'. \<forall>t. flat3_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set2_E3A \<union> UNION (set2_rawF t) set2_E3B) \<and>
                                 (\<forall>y \<in> set2_rawE s'. \<forall>s. flat3_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set2_E3A \<union> UNION (set2_rawE s) set2_E3B)"])
  apply (rule allI)
  apply (rule allI)
thm rawF_rawE.set_induct(2)[of
    "\<lambda>y t. \<forall>t. flat3_rawF t = t' \<longrightarrow> y \<in> UNION (set1_rawF t) set2_E3A \<union> UNION (set2_rawF t) set2_E3B"
    "\<lambda>y s. \<forall>s. flat3_rawE s = s' \<longrightarrow> y \<in> UNION (set1_rawE s) set2_E3A \<union> UNION (set2_rawE s) set2_E3B"
    "t':: (('a, 'b) E3A, ('a, 'b) E3B) rawF"  
    "s':: (('a, 'b) E3A, ('a, 'b) E3B) rawE"]
  apply (rule rawF_rawE.set_induct(2); (rule allI impI)+)
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawF.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawF.set shape3A.set GF.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done
  subgoal for _ _ g sh a t
  apply (rule rawE.exhaust[of t])
  apply (auto 0 4 simp only:
      flat3_simps rawE.set shape3A.set GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong) []
  done

  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(1)[of
      "\<lambda>x t. \<forall>a \<in> set2_E3A x. a \<in> set2_rawF (flat3_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set2_E3A x. a \<in> set2_rawE (flat3_rawE s)" t s];
    auto simp only: flat3_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply (rule rev_mp)
  apply (rule rawF_rawE.set_induct(2)[of
      "\<lambda>x t. \<forall>a \<in> set2_E3B x. a \<in> set2_rawF (flat3_rawF t)"
      "\<lambda>x s. \<forall>a \<in> set2_E3B x. a \<in> set2_rawE (flat3_rawE s)" t s];
    auto simp only: flat3_simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac)
  apply slow
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. set2_rawF (flat3_rawF t) = UNION (set1_rawF t) set2_E3A \<union> UNION (set2_rawF t) set2_E3B"
    "\<lambda>s. set2_rawE (flat3_rawE s) = UNION (set1_rawE s) set2_E3A \<union> UNION (set2_rawE s) set2_E3B" t s])
  apply (simp_all only: flat3_rawF.simps flat3_rawE.simps rawF.set rawE.set GF.set_map GE.set_map set_flat_shape
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done*)

lemmas set_flat =
  conjunct1[OF set1_flat1_raw]
  conjunct1[OF set2_flat1_raw]
  conjunct2[OF set1_flat1_raw]
  conjunct2[OF set2_flat1_raw]
  conjunct1[OF set1_flat2_raw]
  conjunct1[OF set2_flat2_raw]
  conjunct2[OF set1_flat2_raw]
  conjunct2[OF set2_flat2_raw]
  conjunct1[OF set1_flat3_raw]
  conjunct1[OF set2_flat3_raw]
  conjunct2[OF set1_flat3_raw]
  conjunct2[OF set2_flat3_raw]

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_set_T_eq_tac ctxt T_set_defs T_ctr_def T_Abs_inverses raw_sets Shape_sets
      set_flat_raw T_Reps invar_raw_simps invar_Shape_simps invar_flat_raws
      pred_maps pred_Trues set_maps pred_congs =
  unfold_tac ctxt (T_ctr_def :: T_set_defs) THEN
  ALLGOALS (full_simp_tac (fold Simplifier.add_cong pred_congs
        (ss_only (flat [T_Abs_inverses, raw_sets, Shape_sets, set_flat_raw,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps, invar_raw_simps,
            invar_Shape_simps, invar_flat_raws,
            pred_maps, pred_Trues, set_maps,
            @{thms UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
              list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]}])
          ctxt)));
end
\<close>
lemma set_T: "set1_T (T g) = set1_GF g \<union>
  (\<Union>(set1_F1A ` (\<Union>(set1_T ` (set3_GF g))))) \<union>
  (\<Union>(set1_F1B ` (\<Union>(set2_T ` (set3_GF g))))) \<union>
  (\<Union>(set1_F2A ` (\<Union>(set1_T ` (set4_GF g))))) \<union>
  (\<Union>(set1_F2B ` (\<Union>(set2_T ` (set4_GF g))))) \<union>
  (\<Union>(set1_E3A` (\<Union>(set1_S ` (set5_GF g))))) \<union>
  (\<Union>(set1_E3B` (\<Union>(set2_S ` (set5_GF g)))))"
  "set2_T (T g) = set2_GF g \<union>
  (\<Union>(set2_F1A ` (\<Union>(set1_T ` (set3_GF g))))) \<union>
  (\<Union>(set2_F1B ` (\<Union>(set2_T ` (set3_GF g))))) \<union>
  (\<Union>(set2_F2A ` (\<Union>(set1_T ` (set4_GF g))))) \<union>
  (\<Union>(set2_F2B ` (\<Union>(set2_T ` (set4_GF g))))) \<union>
  (\<Union>(set2_E3A` (\<Union>(set1_S ` (set5_GF g))))) \<union>
  (\<Union>(set2_E3B` (\<Union>(set2_S ` (set5_GF g)))))"
  apply (tactic \<open>mk_set_T_eq_tac @{context}
    @{thms set1_T_def set2_T_def set1_S_def set2_S_def}
    @{thm T_def}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms rawF.set rawE.set}
    @{thms shape3A.set shape3B.set}
    @{thms set_flat}
    @{thms Rep_T Rep_S}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms invar_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.set_map GE.set_map}
    @{thms GF.pred_cong GE.pred_cong}\<close>)
  done
(*
  unfolding set1_T_def set2_T_def set1_S_def set2_S_def T_def
  apply (simp_all only:
      Abs_T_inverse Abs_S_inverse
      rawF.set rawE.set shape3A.set shape3B.set set_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF.simps
      invar_shape3A.simps invar_shape3B.simps
      invar_flat
      GF.pred_map GF.pred_True GF.set_map
      UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong)
  done*)

lemma set_S: "set1_S (S g) = set1_GE g \<union>
  (\<Union>(set1_F1A ` (\<Union>(set1_T ` (set3_GE g))))) \<union>
  (\<Union>(set1_F1B ` (\<Union>(set2_T ` (set3_GE g))))) \<union>
  (\<Union>(set1_F2A ` (\<Union>(set1_T ` (set4_GE g))))) \<union>
  (\<Union>(set1_F2B ` (\<Union>(set2_T ` (set4_GE g))))) \<union>
  (\<Union>(set1_E3A` (\<Union>(set1_S ` (set5_GE g))))) \<union>
  (\<Union>(set1_E3B` (\<Union>(set2_S ` (set5_GE g)))))"
  "set2_S (S g) = set2_GE g \<union>
  (\<Union>(set2_F1A ` (\<Union>(set1_T ` (set3_GE g))))) \<union>
  (\<Union>(set2_F1B ` (\<Union>(set2_T ` (set3_GE g))))) \<union>
  (\<Union>(set2_F2A ` (\<Union>(set1_T ` (set4_GE g))))) \<union>
  (\<Union>(set2_F2B ` (\<Union>(set2_T ` (set4_GE g))))) \<union>
  (\<Union>(set2_E3A` (\<Union>(set1_S ` (set5_GE g))))) \<union>
  (\<Union>(set2_E3B` (\<Union>(set2_S ` (set5_GE g)))))"
  unfolding set1_T_def set2_T_def set1_S_def set2_S_def S_def
  apply (simp_all only:
      Abs_T_inverse Abs_S_inverse
      rawF.set rawE.set shape3A.set shape3B.set set_flat
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF_simps invar_rawE_simps
      invar_shape3A.simps invar_shape3B.simps
      invar_flat
      GE.pred_map GE.pred_True GE.set_map
      UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong)
  done


subsection \<open>rel\<close>

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_flat_Shape_raw_tac ctxt induct Ps us invar_shape_simps flat_simps rel_injects
        invar_Shape_depth_iffs rel_maps pred_sets rel_mono_strongs label_splits =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct)) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IH0s, ...} =>
    let
      val IHs = map (fn thm => thm RS spec RS spec RS mp RS mp RS mp) IH0s;
      val apply_IHs = resolve_tac ctxt IHs THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val dest = dtac ctxt bspec THEN' assume_tac ctxt;
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE ::  rel_mono_strongs);
      val simp = full_simp_tac (fold Splitter.add_split (label_splits @ @{thms list.splits if_splits})
        (ss_only (flat [invar_shape_simps, flat_simps, rel_injects, invar_Shape_depth_iffs,
        rel_maps, pred_sets, @{thms ball_simps id_apply}])
          ctxt));
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, apply_IHs, intro, elim, dest, simp])
    end) ctxt);
end
\<close>

lemma rel_flat_shape1_raw:
  fixes R Q
  shows
  "(\<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') \<longrightarrow>
     rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua') \<and>
   (\<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') \<longrightarrow>
     rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub')"
  apply (tactic \<open>mk_rel_flat_Shape_raw_tac @{context}
    @{thm shape3A_shape3B.induct}
    [@{cterm "\<lambda>ua. \<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow>
      rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') \<longrightarrow> rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua'"},
     @{cterm "\<lambda>ub. \<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow>
      rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') \<longrightarrow> rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub'"}]
    [@{cterm ua}, @{cterm ub}]
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms flat_shape1A.simps flat_shape1B.simps}
    @{thms shape3A.rel_inject shape3B.rel_inject}
    @{thms invar_shape3A_depth_iff invar_shape3B_depth_iff}
    @{thms F1A.rel_map F2A.rel_map F1B.rel_map F2B.rel_map E3A.rel_map E3B.rel_map}
    @{thms F1A.pred_set F2A.pred_set F1B.pred_set F2B.pred_set E3A.pred_set E3B.pred_set}
    @{thms F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong E3A.rel_mono_strong E3B.rel_mono_strong}
    @{thms label.splits}\<close>)
  done
(*
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') \<longrightarrow>
    rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua'"
    "\<lambda>ub. \<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') \<longrightarrow>
    rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shape3A.simps invar_shape3B.simps flat_shape1A.simps flat_shape1B.simps shape3A.rel_inject shape3B.rel_inject
      invar_shape3A_depth_iff invar_shape3B_depth_iff ball_simps id_apply
      F1A.rel_map F1A.pred_set
      F2A.rel_map F2A.pred_set
      E3A.rel_map E3A.pred_set
      F1B.rel_map F1B.pred_set
      F2B.rel_map F2B.pred_set
      E3B.rel_map E3B.pred_set
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
      E3A.rel_mono_strong E3B.rel_mono_strong
    split: list.splits label.splits if_splits)
  done*)

lemma rel_flat_shape2_raw:
  fixes R Q
  shows
  "(\<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape2A ua) (flat_shape2A ua') \<longrightarrow>
     rel_shape3A (rel_F2A R Q) (rel_F2B R Q) ua ua') \<and>
   (\<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape2B ub) (flat_shape2B ub') \<longrightarrow>
     rel_shape3B (rel_F2A R Q) (rel_F2B R Q) ub ub')"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0 ua'. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape2A ua) (flat_shape2A ua') \<longrightarrow>
    rel_shape3A (rel_F2A R Q) (rel_F2B R Q) ua ua'"
    "\<lambda>ub. \<forall>u0 ub'. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape2B ub) (flat_shape2B ub') \<longrightarrow>
    rel_shape3B (rel_F2A R Q) (rel_F2B R Q) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shape3A.simps invar_shape3B.simps flat_shape2A.simps flat_shape2B.simps shape3A.rel_inject shape3B.rel_inject
      invar_shape3A_depth_iff invar_shape3B_depth_iff ball_simps id_apply
      F1A.rel_map pred_F1A_def
      F2A.rel_map pred_F2A_def
      E3A.rel_map pred_E3A_def
      F1B.rel_map pred_F1B_def
      F2B.rel_map pred_F2B_def
      E3B.rel_map pred_E3B_def
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
      E3A.rel_mono_strong E3B.rel_mono_strong
    split: list.splits label.splits if_splits)
  done

lemma rel_flat_shape3_raw:
  fixes R Q
  shows
  "(\<forall>ua' u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape3A ua) (flat_shape3A ua') \<longrightarrow>
     rel_shape3A (rel_E3A R Q) (rel_E3B R Q) ua ua') \<and>
   (\<forall>ub' u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape3B ub) (flat_shape3B ub') \<longrightarrow>
     rel_shape3B (rel_E3A R Q) (rel_E3B R Q) ub ub')"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>ua' u0. invar_shape3A u0 ua \<longrightarrow> invar_shape3A u0 ua' \<longrightarrow> rel_shape3A R Q (flat_shape3A ua) (flat_shape3A ua') \<longrightarrow>
    rel_shape3A (rel_E3A R Q) (rel_E3B R Q) ua ua'"
    "\<lambda>ub. \<forall>ub' u0. invar_shape3B u0 ub \<longrightarrow> invar_shape3B u0 ub' \<longrightarrow> rel_shape3B R Q (flat_shape3B ub) (flat_shape3B ub') \<longrightarrow>
    rel_shape3B (rel_E3A R Q) (rel_E3B R Q) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shape3A.simps invar_shape3B.simps flat_shape3A.simps flat_shape3B.simps shape3A.rel_inject shape3B.rel_inject
      invar_shape3A_depth_iff invar_shape3B_depth_iff ball_simps id_apply
      F1A.rel_map pred_F1A_def
      F2A.rel_map pred_F2A_def
      E3A.rel_map pred_E3A_def
      F1B.rel_map pred_F1B_def
      F2B.rel_map pred_F2B_def
      E3B.rel_map pred_E3B_def
    elim!: F1A.rel_mono_strong F2A.rel_mono_strong F1B.rel_mono_strong F2B.rel_mono_strong
      E3A.rel_mono_strong E3B.rel_mono_strong
    split: list.splits label.splits if_splits)
  done

ML \<open>
local
open Ctr_Sugar_Util
in
fun mk_rel_final_tac ctxt raw_thm transfer =
  ALLGOALS (EVERY' [
    rtac ctxt (transfer RS @{thm rel_funD} RS @{thm iffI[rotated]}),
    assume_tac ctxt,
    rtac ctxt raw_thm] THEN_ALL_NEW
    assume_tac ctxt);
end
\<close>

lemmas rel_flat_shape1_raw1 =
  mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat_shape1_raw]]]]]]
lemmas rel_flat_shape1_raw2 =
  mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat_shape1_raw]]]]]]

lemma rel_flat_shape1A:
  fixes R Q
  shows
  "invar_shape3A u0 ua \<Longrightarrow> invar_shape3A u0 ua' \<Longrightarrow>
    rel_shape3A R Q (flat_shape1A ua) (flat_shape1A ua') = rel_shape3A (rel_F1A R Q) (rel_F1B R Q) ua ua'"
  apply (tactic \<open>mk_rel_final_tac @{context}
    @{thm rel_flat_shape1_raw1}
    @{thm flat_shape1A.transfer}\<close>)
  done
lemma rel_flat_shape1B:
  fixes R Q
  shows
  "invar_shape3B u0 ub \<Longrightarrow> invar_shape3B u0 ub' \<Longrightarrow>
    rel_shape3B R Q (flat_shape1B ub) (flat_shape1B ub') = rel_shape3B (rel_F1A R Q) (rel_F1B R Q) ub ub'"
  apply (tactic \<open>mk_rel_final_tac @{context}
    @{thm rel_flat_shape1_raw2}
    @{thm flat_shape1B.transfer}\<close>)
  done
(*  apply (rule iffI[rotated, OF rel_funD[OF flat_shape1A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat_shape1_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape1B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat_shape1_raw]]]]]]; assumption)
  done*)

lemma rel_flat_shape2:
  fixes R Q
  shows
  "invar_shape3A u0 ua \<Longrightarrow> invar_shape3A u0 ua' \<Longrightarrow>
    rel_shape3A R Q (flat_shape2A ua) (flat_shape2A ua') = rel_shape3A (rel_F2A R Q) (rel_F2B R Q) ua ua'"
  "invar_shape3B u0 ub \<Longrightarrow> invar_shape3B u0 ub' \<Longrightarrow>
    rel_shape3B R Q (flat_shape2B ub) (flat_shape2B ub') = rel_shape3B (rel_F2A R Q) (rel_F2B R Q) ub ub'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape2A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat_shape2_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape2B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat_shape2_raw]]]]]]; assumption)
  done

lemma rel_flat_shape3:
  fixes R Q
  shows
  "invar_shape3A u0 ua \<Longrightarrow> invar_shape3A u0 ua' \<Longrightarrow>
    rel_shape3A R Q (flat_shape3A ua) (flat_shape3A ua') = rel_shape3A (rel_E3A R Q) (rel_E3B R Q) ua ua'"
  "invar_shape3B u0 ub \<Longrightarrow> invar_shape3B u0 ub' \<Longrightarrow>
    rel_shape3B R Q (flat_shape3B ub) (flat_shape3B ub') = rel_shape3B (rel_E3A R Q) (rel_E3B R Q) ub ub'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape3A.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat_shape3_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat_shape3B.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat_shape3_raw]]]]]]; assumption)
  done

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_flat_raw_raw_tac ctxt induct Ps us raw_exhausts invar_raw_simps flat_simps rel_injects
        rel_maps pred_sets rel_mono_strongs rel_flat_Shapes =
  HEADGOAL (rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ us)) induct) THEN_ALL_NEW
   (EVERY' [
    rtac ctxt allI,
    Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thm = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           raw_exhausts;
        in HEADGOAL (resolve_tac ctxt thm) end) ctxt, hyp_subst_tac_thin true ctxt,
    Subgoal.FOCUS (fn {context = ctxt, prems = IH0s, ...} =>
    let
      val IHs = map (fn thm => thm RS spec RS spec RS mp RS mp RS mp) IH0s;
      val apply_IHs = resolve_tac ctxt IHs THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt [allI, impI, conjI];
      val dest = dtac ctxt bspec THEN' assume_tac ctxt;
      val elim = eresolve_tac ctxt (conjE :: disjE :: exE ::  rel_mono_strongs);
      val simp = asm_full_simp_tac (ss_only (flat [rel_flat_Shapes, invar_raw_simps, flat_simps,
        rel_injects, rel_maps, pred_sets, @{thms ball_simps id_apply}])
          ctxt);
    in
      HEADGOAL (REPEAT_DETERM o
       FIRST' [hyp_subst_tac ctxt, apply_IHs, intro, elim, dest, simp])
    end) ctxt]));
end
\<close>

lemma rel_flat1_raw:
  fixes R Q and t :: "(('a, 'b) F1A, ('a, 'b) F1B) rawF" and s :: "(('a, 'b) F1A, ('a, 'b) F1B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') \<longrightarrow> rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t') \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') \<longrightarrow> rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s')"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
      fun mk_rel_flat_raw_raw_cotac ctxt Ps Rs xs ys induct raw_exhausts raw_sels raw_rel_injects
        flat_simps invar_raw_simps rel_flat_shapes
        G_rel_maps G_pred_sets G_rel_refl G_rel_congs G_rel_mono_strongs =
        let
          val n = length Ps;
          fun case_mut a b = if n <> 1 then a else b;
          val infer_args = case_mut ((hd Ps :: Rs @ tl Ps) @ ([xs, ys] |> transpose |> flat))
            (hd Ps :: ([xs, ys] |> transpose |> flat) @ Rs);
        in
          HEADGOAL (EVERY' [case_mut (rtac ctxt rev_mp) (REPEAT_DETERM o (rtac ctxt impI)),
            rtac ctxt (infer_instantiate' ctxt (map SOME infer_args) induct),
            case_mut (K all_tac) (REPEAT_DETERM o FIRST' [resolve_tac ctxt [exI, conjI], assume_tac ctxt])]) THEN
          (HEADGOAL o RANGE) (map (fn exhaust => EVERY' [
            REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
            Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
              let
                val exhaust_tacs = map (fn param =>
                  rtac ctxt (infer_instantiate' ctxt ((single o SOME o snd) param) exhaust)) (take 2 params);
                val intro = resolve_tac ctxt (exI :: G_rel_refl);
                val elim = eresolve_tac ctxt (conjE :: G_rel_mono_strongs);
                val dest_bspec = dtac ctxt bspec THEN' assume_tac ctxt;
                val rel_flat_shapesI = map (fn thm => thm RS iffD1) rel_flat_shapes;
                val apply_rel_flat = resolve_tac ctxt rel_flat_shapesI THEN_ALL_NEW assume_tac ctxt;
                val simp = asm_full_simp_tac (fold Simplifier.add_cong G_rel_congs
                  (ss_only (flat [raw_sels, raw_rel_injects, flat_simps,
                    invar_raw_simps, G_rel_maps, G_pred_sets]) ctxt));
              in
                HEADGOAL (EVERY' exhaust_tacs THEN' REPEAT_DETERM o
                  FIRST' [hyp_subst_tac ctxt, intro, elim, dest_bspec, apply_rel_flat, simp])
              end) ctxt]) raw_exhausts) THEN
          HEADGOAL (case_mut (
            rtac ctxt impI THEN'
            CONJ_WRAP' (fn i => EVERY' [
              dtac ctxt (mk_conjunctN n i),
              REPEAT_DETERM o resolve_tac ctxt [allI, impI],
              etac ctxt mp,
              REPEAT_DETERM o FIRST' [
                resolve_tac ctxt [exI, conjI],
                assume_tac ctxt]]) (1 upto n)) (K all_tac))
        end
      in
        mk_rel_flat_raw_raw_cotac @{context}
          [@{cterm "\<lambda>t t'. \<exists>u0. invar_rawF u0 t \<and> invar_rawF u0 t' \<and> rel_rawF R Q (flat1_rawF t) (flat1_rawF t')"},
           @{cterm "\<lambda>s s'. \<exists>u0. invar_rawE u0 s \<and> invar_rawE u0 s' \<and> rel_rawE R Q (flat1_rawE s) (flat1_rawE s')"}]
          [@{cterm "rel_F1A R Q"}, @{cterm "rel_F1B R Q"}]
          [@{cterm "t"}, @{cterm "s"}]
          [@{cterm "t'"}, @{cterm "s'"}]
          @{thm rawF_rawE.rel_coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms rawF.sel rawE.sel}
          @{thms rawF.rel_inject rawE.rel_inject}
          @{thms flat1_simps flat2_simps flat3_simps}
          @{thms invar_rawF_simps invar_rawE_simps}
          @{thms rel_flat_shape1A rel_flat_shape1B rel_flat_shape2 rel_flat_shape3}
          @{thms GF.rel_map GE.rel_map}
          @{thms GF.pred_set GE.pred_set}
          @{thms GF.rel_refl GE.rel_refl}
          @{thms GF.rel_cong GE.rel_cong}
          @{thms GF.rel_mono_strong GE.rel_mono_strong}
      end\<close>)
(*
  apply (rule rev_mp)
  apply (rule rawF_rawE.rel_coinduct[of
    "\<lambda>t t'. \<exists>u0. invar_rawF u0 t \<and> invar_rawF u0 t' \<and> rel_rawF R Q (flat1_rawF t) (flat1_rawF t')"
    "rel_F1A R Q" "rel_F1B R Q"
    "\<lambda>s s'. \<exists>u0. invar_rawE u0 s \<and> invar_rawE u0 s' \<and> rel_rawE R Q (flat1_rawE s) (flat1_rawE s')"
    t t' s s'])

  subgoal for rawF rawF'
    apply (rule rawF.exhaust[of rawF])
    apply (rule rawF.exhaust[of rawF'])
    apply (auto simp only: rawF.sel rawE.sel rawF.rel_inject rawE.rel_inject
      rel_flat_shape1A rel_flat_shape1B rel_flat_shape2 rel_flat_shape3
      flat1_simps flat2_simps flat3_simps invar_rawF_simps invar_rawE_simps
      GF.rel_map GE.rel_map GF.pred_set GE.pred_set
      o_apply simp_thms ball_simps id_apply lambda_prs
      intro: GF.rel_refl GE.rel_refl
      cong: GF.rel_cong GE.rel_cong
      elim!: GF.rel_mono_strong GE.rel_mono_strong)
    done

  subgoal for rawE rawE'
    apply (rule rawE.exhaust[of rawE])
    apply (rule rawE.exhaust[of rawE'])
    apply (auto simp only: rawF.sel rawE.sel rawF.rel_inject rawE.rel_inject
      rel_flat_shape1A rel_flat_shape1B rel_flat_shape2 rel_flat_shape3 
      flat1_simps flat2_simps flat3_simps invar_rawF_simps invar_rawE_simps
      GF.rel_map GE.rel_map GF.pred_set GE.pred_set
      o_apply simp_thms ball_simps id_apply lambda_prs
      intro: GF.rel_refl GE.rel_refl
      cong: GF.rel_cong GE.rel_cong
      elim!: GF.rel_mono_strong GE.rel_mono_strong)
    done

  apply (rule impI conjI)+
  apply (erule conjE)
  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (rule allI impI)+
  apply (erule impE; (rule exI conjI | assumption)+)

  apply (erule conjE)
  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (rule allI impI)+
  apply (erule impE; (rule exI conjI | assumption)+)

(* or this way:
  apply (rule impI)
  apply (rule conjI)

  apply (drule conjunct1)
  apply (rule allI impI)+
  apply (erule mp)
  apply (rule exI conjI | assumption)+

  apply (drule conjunct2)
  apply (rule allI impI)+
  apply (erule mp)
  apply (rule exI conjI | assumption)+*)
  done



  apply (tactic \<open>mk_rel_flat_raw_raw_tac @{context}
    @{thm rawF_rawE.induct}
    [@{cterm "\<lambda>t :: (('a, 'b) F1A, ('a, 'b) F1B) rawF. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') \<longrightarrow> rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t'"},
     @{cterm "\<lambda>s :: (('a, 'b) F1A, ('a, 'b) F1B) rawE. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') \<longrightarrow> rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s'"}]
    [@{cterm t}, @{cterm s}]
    @{thms rawF.exhaust rawE.exhaust}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms flat1_rawF.simps flat1_rawE.simps}
    @{thms rawF.rel_inject rawE.rel_inject}
    @{thms GF.rel_map GE.rel_map}
    @{thms GF.pred_set GE.pred_set}
    @{thms GF.rel_mono_strong GE.rel_mono_strong}
    @{thms rel_flat_shape1A rel_flat_shape1B}\<close>)
  done

  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') \<longrightarrow> rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t'"
    "\<lambda>s. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') \<longrightarrow> rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s'" t s])
  apply (tactic \<open>
    let open Ctr_Sugar_Util in
      ALLGOALS (rtac @{context} allI THEN' Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thm = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           @{thms rawF.exhaust rawE.exhaust};
        in HEADGOAL (resolve_tac ctxt thm) end) @{context})
    end\<close>)
  apply (auto simp only: rel_flat_shape1A rel_flat_shape1B ball_simps id_apply
      invar_rawF.simps flat1_rawF.simps rawF.rel_inject GF.rel_map GF.pred_set
      invar_rawE.simps flat1_rawE.simps rawE.rel_inject GE.rel_map GE.pred_set
    elim!: GF.rel_mono_strong GE.rel_mono_strong)
  done*)

lemma rel_flat2_raw:
  fixes R Q and t :: "(('a, 'b) F2A, ('a, 'b) F2B) rawF" and s :: "(('a, 'b) F2A, ('a, 'b) F2B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat2_rawF t) (flat2_rawF t') \<longrightarrow> rel_rawF (rel_F2A R Q) (rel_F2B R Q) t t') \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat2_rawE s) (flat2_rawE s') \<longrightarrow> rel_rawE (rel_F2A R Q) (rel_F2B R Q) s s')"
  apply (rule rev_mp)
  apply (rule rawF_rawE.rel_coinduct[of
    "\<lambda>t t'. \<exists>u0. invar_rawF u0 t \<and> invar_rawF u0 t' \<and> rel_rawF R Q (flat2_rawF t) (flat2_rawF t')"
    "rel_F2A R Q" "rel_F2B R Q"
    "\<lambda>s s'. \<exists>u0. invar_rawE u0 s \<and> invar_rawE u0 s' \<and> rel_rawE R Q (flat2_rawE s) (flat2_rawE s')"
    t t' s s'])

  subgoal for rawF rawF'
    apply (rule rawF.exhaust[of rawF])
    apply (rule rawF.exhaust[of rawF'])
    apply (auto simp only: rawF.sel rawE.sel rawF.rel_inject rawE.rel_inject
      rel_flat_shape1A rel_flat_shape1B rel_flat_shape2 rel_flat_shape3
      flat1_simps flat2_simps flat3_simps invar_rawF_simps invar_rawE_simps
      GF.rel_map GE.rel_map GF.pred_set GE.pred_set
      o_apply simp_thms ball_simps id_apply lambda_prs
      intro: GF.rel_refl GE.rel_refl
      cong: GF.rel_cong GE.rel_cong
      elim!: GF.rel_mono_strong GE.rel_mono_strong)
    done

  subgoal for rawE rawE'
    apply (rule rawE.exhaust[of rawE])
    apply (rule rawE.exhaust[of rawE'])
    apply (auto simp only: rawF.sel rawE.sel rawF.rel_inject rawE.rel_inject
      rel_flat_shape1A rel_flat_shape1B rel_flat_shape2 rel_flat_shape3 
      flat1_simps flat2_simps flat3_simps invar_rawF_simps invar_rawE_simps
      GF.rel_map GE.rel_map GF.pred_set GE.pred_set
      o_apply simp_thms ball_simps id_apply lambda_prs
      intro: GF.rel_refl GE.rel_refl
      cong: GF.rel_cong GE.rel_cong
      elim!: GF.rel_mono_strong GE.rel_mono_strong)
    done

  apply (rule impI conjI)+
  apply (erule conjE)
  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (rule allI impI)+
  apply (erule impE; (rule exI conjI | assumption)+)

  apply (erule conjE)
  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (rule allI impI)+
  apply (erule impE; (rule exI conjI | assumption)+)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat2_rawF t) (flat2_rawF t') \<longrightarrow> rel_rawF (rel_F2A R Q) (rel_F2B R Q) t t'"
    "\<lambda>s. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat2_rawE s) (flat2_rawE s') \<longrightarrow> rel_rawE (rel_F2A R Q) (rel_F2B R Q) s s'" t s])
  apply (tactic \<open>
    let open Ctr_Sugar_Util in
      ALLGOALS (rtac @{context} allI THEN' Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thm = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           @{thms rawF.exhaust rawE.exhaust};
        in HEADGOAL (resolve_tac ctxt thm) end) @{context})
    end\<close>)
  apply (auto simp only: rel_flat_shape2 ball_simps id_apply
      invar_rawF.simps flat2_rawF.simps rawF.rel_inject GF.rel_map GF.pred_set
      invar_rawE.simps flat2_rawE.simps rawE.rel_inject GE.rel_map GE.pred_set
    elim!: GF.rel_mono_strong GE.rel_mono_strong)
  done*)

lemma rel_flat3_raw:
  fixes R Q and t :: "(('a, 'b) E3A, ('a, 'b) E3B) rawF" and s :: "(('a, 'b) E3A, ('a, 'b) E3B) rawE"
  shows
  "(\<forall>u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat3_rawF t) (flat3_rawF t') \<longrightarrow> rel_rawF (rel_E3A R Q) (rel_E3B R Q) t t') \<and>
   (\<forall>u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat3_rawE s) (flat3_rawE s') \<longrightarrow> rel_rawE (rel_E3A R Q) (rel_E3B R Q) s s')"
  apply (rule rev_mp)
  apply (rule rawF_rawE.rel_coinduct[of
    "\<lambda>t t'. \<exists>u0. invar_rawF u0 t \<and> invar_rawF u0 t' \<and> rel_rawF R Q (flat3_rawF t) (flat3_rawF t')"
    "rel_E3A R Q" "rel_E3B R Q"
    "\<lambda>s s'. \<exists>u0. invar_rawE u0 s \<and> invar_rawE u0 s' \<and> rel_rawE R Q (flat3_rawE s) (flat3_rawE s')"
    t t' s s'])

  subgoal for rawF rawF'
    apply (rule rawF.exhaust[of rawF])
    apply (rule rawF.exhaust[of rawF'])
    apply (auto simp only: rawF.sel rawE.sel rawF.rel_inject rawE.rel_inject
      rel_flat_shape1A rel_flat_shape1B rel_flat_shape2 rel_flat_shape3
      flat1_simps flat2_simps flat3_simps invar_rawF_simps invar_rawE_simps
      GF.rel_map GE.rel_map GF.pred_set GE.pred_set
      o_apply simp_thms ball_simps id_apply lambda_prs
      intro: GF.rel_refl GE.rel_refl
      cong: GF.rel_cong GE.rel_cong
      elim!: GF.rel_mono_strong GE.rel_mono_strong)
    done

  subgoal for rawE rawE'
    apply (rule rawE.exhaust[of rawE])
    apply (rule rawE.exhaust[of rawE'])
    apply (auto simp only: rawF.sel rawE.sel rawF.rel_inject rawE.rel_inject
      rel_flat_shape1A rel_flat_shape1B rel_flat_shape2 rel_flat_shape3 
      flat1_simps flat2_simps flat3_simps invar_rawF_simps invar_rawE_simps
      GF.rel_map GE.rel_map GF.pred_set GE.pred_set
      o_apply simp_thms ball_simps id_apply lambda_prs
      intro: GF.rel_refl GE.rel_refl
      cong: GF.rel_cong GE.rel_cong
      elim!: GF.rel_mono_strong GE.rel_mono_strong)
    done

  apply (rule impI conjI)+
  apply (erule conjE)
  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (rule allI impI)+
  apply (erule impE; (rule exI conjI | assumption)+)

  apply (erule conjE)
  apply (erule thin_rl)
  apply (drule asm_rl)
  apply (rule allI impI)+
  apply (erule impE; (rule exI conjI | assumption)+)
  done
(*
  apply (rule rawF_rawE.induct[of
    "\<lambda>t. \<forall>t' u0. invar_rawF u0 t \<longrightarrow> invar_rawF u0 t' \<longrightarrow>
   rel_rawF R Q (flat3_rawF t) (flat3_rawF t') \<longrightarrow> rel_rawF (rel_E3A R Q) (rel_E3B R Q) t t'"
    "\<lambda>s. \<forall>s' u0. invar_rawE u0 s \<longrightarrow> invar_rawE u0 s' \<longrightarrow>
   rel_rawE R Q (flat3_rawE s) (flat3_rawE s') \<longrightarrow> rel_rawE (rel_E3A R Q) (rel_E3B R Q) s s'" t s])
    apply (tactic \<open>
    let open Ctr_Sugar_Util in
      ALLGOALS (rtac @{context} allI THEN' Subgoal.FOCUS (fn {context = ctxt, params, ...} =>
        let val thms = map_filter (try (infer_instantiate' ctxt [SOME (snd (nth params 1))]))
           @{thms rawF.exhaust rawE.exhaust};
        in HEADGOAL (resolve_tac ctxt thms) end) @{context})
    end\<close>)
  apply (auto simp only: rel_flat_shape3 ball_simps id_apply
      invar_rawF.simps flat3_rawF.simps rawF.rel_inject GF.rel_map GF.pred_set
      invar_rawE.simps flat3_rawE.simps rawE.rel_inject GE.rel_map GE.pred_set
    elim!: GF.rel_mono_strong GE.rel_mono_strong)
  done*)
ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_flat_raw_tac ctxt transfer rel_flat_raw_raw conj =
  ALLGOALS (EVERY' [
    rtac ctxt (transfer RS @{thm rel_funD} RS @{thm iffI[rotated]}),
    assume_tac ctxt,
    rtac ctxt (rel_flat_raw_raw RS conj RS spec RS mp RS mp RS mp)] THEN_ALL_NEW
    assume_tac ctxt)
end
\<close>
lemma rel_flat1A:
  fixes R Q
  shows
  "invar_rawF u0 t \<Longrightarrow> invar_rawF u0 t' \<Longrightarrow>
   rel_rawF R Q (flat1_rawF t) (flat1_rawF t') = rel_rawF (rel_F1A R Q) (rel_F1B R Q) t t'"
  apply (tactic \<open>mk_rel_flat_raw_tac @{context}
    @{thm flat1_rawF.transfer}
    @{thm rel_flat1_raw}
    @{thm conjunct1}\<close>)
  done
(*
  apply (rule iffI[rotated, OF rel_funD[OF flat1_rawF.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF rel_flat1_raw]]]]]]; assumption)
  done*)
lemma rel_flat1B:
  fixes R Q
  shows
  "invar_rawE u0 s \<Longrightarrow> invar_rawE u0 s' \<Longrightarrow>
   rel_rawE R Q (flat1_rawE s) (flat1_rawE s') = rel_rawE (rel_F1A R Q) (rel_F1B R Q) s s'"
  apply (tactic \<open>mk_rel_flat_raw_tac @{context}
    @{thm flat1_rawE.transfer}
    @{thm rel_flat1_raw}
    @{thm conjunct2}\<close>)
  done
(*
  apply (rule iffI[rotated, OF rel_funD[OF flat1_rawE.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF rel_flat1_raw]]]]]]; assumption)
  done*)

lemma rel_flat2:
  fixes R Q
  shows
  "invar_rawF u0 t \<Longrightarrow> invar_rawF u0 t' \<Longrightarrow>
   rel_rawF R Q (flat2_rawF t) (flat2_rawF t') = rel_rawF (rel_F2A R Q) (rel_F2B R Q) t t'"
  "invar_rawE u0 s \<Longrightarrow> invar_rawE u0 s' \<Longrightarrow>
   rel_rawE R Q (flat2_rawE s) (flat2_rawE s') = rel_rawE (rel_F2A R Q) (rel_F2B R Q) s s'"
  apply (rule iffI[rotated, OF rel_funD[OF flat2_rawF.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF conjunct1[OF rel_flat2_raw]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat2_rawE.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF conjunct2[OF rel_flat2_raw]]]]]; assumption)
  done

lemma rel_flat3:
  fixes R Q
  shows
  "invar_rawF u0 t \<Longrightarrow> invar_rawF u0 t' \<Longrightarrow>
   rel_rawF R Q (flat3_rawF t) (flat3_rawF t') = rel_rawF (rel_E3A R Q) (rel_E3B R Q) t t'"
  "invar_rawE u0 s \<Longrightarrow> invar_rawE u0 s' \<Longrightarrow>
   rel_rawE R Q (flat3_rawE s) (flat3_rawE s') = rel_rawE (rel_E3A R Q) (rel_E3B R Q) s s'"
  apply (rule iffI[rotated, OF rel_funD[OF flat3_rawF.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF conjunct1[OF rel_flat3_raw]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat3_rawE.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF conjunct2[OF rel_flat3_raw]]]]]; assumption)
  done

lemmas rel_flat_rawF =
  rel_flat1A
  rel_flat2(1)
  rel_flat3(1)

lemmas rel_flat_rawE =
  rel_flat1B
  rel_flat2(2)
  rel_flat3(2)

ML \<open>
local
open Ctr_Sugar_Util
in

fun mk_rel_T_eq_tac ctxt T_rel_defs T_ctr_def T_Abs_inverses
      raw_rel_injects Shape_rel_injects rel_flat_rawss T_Reps invar_raw_simps
      invar_Shape_simps invar_map_raw_closeds invar_flat_raws
      pred_maps pred_Trues rel_maps pred_congs =
  unfold_tac ctxt (T_ctr_def :: @{thm vimage2p_def} :: T_rel_defs) THEN
  HEADGOAL (full_simp_tac (fold Simplifier.add_cong (pred_congs)
        (ss_only (flat [T_Abs_inverses, raw_rel_injects, Shape_rel_injects,
            map2 (fn thms => fn rep =>
              let
                val reps = replicate 2 (unfold_thms ctxt @{thms mem_Collect_eq} rep);
              in
                map (fn thm => thm OF reps) thms
              end) rel_flat_rawss T_Reps |> flat,
            map (unfold_thms ctxt @{thms mem_Collect_eq}) T_Reps,
            invar_raw_simps, invar_Shape_simps, invar_map_raw_closeds, invar_flat_raws,
            pred_maps, pred_Trues, rel_maps,
            @{thms snoc.simps(1)[symmetric] o_apply mem_Collect_eq list.case}])
          ctxt)));
end
\<close>
lemma rel_T: fixes R Q shows "rel_T R Q (T g) (T g') =
  rel_GF R Q
   (rel_T (rel_F1A R Q) (rel_F1B R Q))
   (rel_T (rel_F2A R Q) (rel_F2B R Q))
   (rel_S (rel_E3A R Q) (rel_E3B R Q)) g g'"
  apply (tactic \<open>mk_rel_T_eq_tac @{context}
    @{thms rel_T_def rel_S_def}
    @{thm T_def}
    @{thms Abs_T_inverse Abs_S_inverse}
    @{thms rawF.rel_inject rawE.rel_inject}
    @{thms shape3A.rel_inject shape3B.rel_inject}
    [@{thms rel_flat_rawF}, @{thms rel_flat_rawE}]
    @{thms Rep_T Rep_S}
    @{thms invar_rawF_simps invar_rawE_simps}
    @{thms invar_shape3A.simps invar_shape3B.simps}
    @{thms invar_map_closed}
    @{thms invar_flat}
    @{thms GF.pred_map GE.pred_map}
    @{thms GF.pred_True GE.pred_True}
    @{thms GF.rel_map GE.rel_map}
    @{thms GF.pred_cong GF.pred_cong}\<close>)
  done
(*
thm Rep_T[unfolded mem_Collect_eq]
thm rel_flat_rawF[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]]
  unfolding rel_T_def rel_S_def T_def vimage2p_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawF.rel_inject GF.rel_map shape3A.rel_inject shape3B.rel_inject
      rel_flat_rawF[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]]
      rel_flat_rawE[OF Rep_S[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]]
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF.simps invar_shape3A.simps invar_shape3B.simps
      invar_map_closed invar_flat
      GF.pred_map GF.pred_True
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GF.pred_cong)
  done*)

lemma rel_S: fixes R Q shows "rel_S R Q (S g) (S g') =
  rel_GE R Q
   (rel_T (rel_F1A R Q) (rel_F1B R Q))
   (rel_T (rel_F2A R Q) (rel_F2B R Q))
   (rel_S (rel_E3A R Q) (rel_E3B R Q)) g g'"
  unfolding rel_T_def rel_S_def S_def vimage2p_def
  apply (simp only:
      Abs_T_inverse Abs_S_inverse
      rawE.rel_inject GE.rel_map shape3A.rel_inject shape3B.rel_inject
      rel_flat_rawF[OF Rep_T[unfolded mem_Collect_eq] Rep_T[unfolded mem_Collect_eq]]
      rel_flat_rawE[OF Rep_S[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]]
      Rep_T[unfolded mem_Collect_eq] Rep_S[unfolded mem_Collect_eq]
      invar_rawF_simps invar_rawE_simps invar_shape3A.simps invar_shape3B.simps
      invar_map_closed invar_flat
      GE.pred_map GE.pred_True
      list.case o_apply mem_Collect_eq snoc.simps(1)[symmetric]
    cong: GE.pred_cong)
  done

thm arg_cong[OF map_T, of un_T _ _ "un_T g" for g, unfolded un_T_T T_un_T]
thm set_T[of "un_T g" for g, unfolded T_un_T]
thm rel_T[of _ _ "un_T g" "un_T g'" for g g', unfolded T_un_T]


section \<open>Size\<close>

(* The "recursion sizes" of elements of 'a raw -- on the second argument of G *)
codatatype szF = SConsF "(unit, unit, szF, szF, szE) GF"
     and szE = SConsE "(unit, unit, szF, szF, szE) GE"

primcorec sz_ofF :: "('a, 'b) rawF \<Rightarrow> szF"
    and sz_ofE :: "('a, 'b) rawE \<Rightarrow> szE" where
  "sz_ofF r = SConsF (map_GF (\<lambda>_. ()) (\<lambda>_. ()) sz_ofF sz_ofF sz_ofE (UnConsF r))"
| "sz_ofE r = SConsE (map_GE (\<lambda>_. ()) (\<lambda>_. ()) sz_ofF sz_ofF sz_ofE (UnConsE r))"

lemmas sz_of_simps = sz_ofF.ctr[of "ConsF z" for z, unfolded rawF.sel] sz_ofE.ctr[of "ConsE z" for z, unfolded rawE.sel]

lemma sz_of_unflat1_raw: 
  "(sz_ofF (unflat1_rawF u0 t) = sz_ofF (t :: ('a, 'b) rawF)) \<and>
   (sz_ofE (unflat1_rawE u0 s) = sz_ofE (s :: ('a, 'b) rawE))"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_sz_of_unflat_raw_cotac ctxt Ps xs ys induct raw_exhausts sz_sels
          unflat_simps sz_of_simps G_rel_maps G_rel_refls =
          let
            val n = length Ps;
          in
            HEADGOAL (EVERY' [rtac ctxt rev_mp,
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 3)] exhaust;
                  val intro = resolve_tac ctxt (exI :: G_rel_refls);
                  val simp = asm_full_simp_tac (ss_only (flat [sz_sels, unflat_simps, sz_of_simps, G_rel_maps]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), etac ctxt mp,
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl])
              ]) (1 upto n)])
          end
      in
        mk_sz_of_unflat_raw_cotac @{context}
          [@{cterm "\<lambda>l r. \<exists>u0 t. l = sz_ofF (unflat1_rawF u0 t) \<and> r = sz_ofF (t :: ('a, 'b) rawF)"},
           @{cterm "\<lambda>l r. \<exists>u0 s. l = sz_ofE (unflat1_rawE u0 s) \<and> r = sz_ofE (s :: ('a, 'b) rawE)"}]
          [@{cterm "sz_ofF (unflat1_rawF u0 t)"}, @{cterm "sz_ofE (unflat1_rawE u0 s)"}]
          [@{cterm "sz_ofF t"}, @{cterm "sz_ofE s"}]
          @{thm szF_szE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms szF.sel szE.sel}
          @{thms unflat1_simps unflat2_simps unflat3_simps}
          @{thms sz_of_simps}
          @{thms GF.rel_map GE.rel_map}
          @{thms GF.rel_refl GE.rel_refl}
      end\<close>)

(*
  apply (rule rev_mp)
  apply (rule szF_szE.coinduct[of
    "\<lambda>l r. \<exists>u0 t. l = sz_ofF (unflat1_rawF u0 t) \<and> r = sz_ofF (t :: ('a, 'b) rawF)"
    "\<lambda>l r. \<exists>u0 s. l = sz_ofE (unflat1_rawE u0 s) \<and> r = sz_ofE (s :: ('a, 'b) rawE)"
    "sz_ofF (unflat1_rawF u0 t)" "sz_ofF (t :: ('a, 'b) rawF)"
    "sz_ofE (unflat1_rawE u0 s)" "sz_ofE (s :: ('a, 'b) rawE)"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ _ t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: szF.sel szE.sel
        unflat1_simps unflat2_simps unflat3_simps
        sz_of_simps
        GF.rel_map GE.rel_map
        intro: GF.rel_refl GE.rel_refl)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ _ s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: szF.sel szE.sel
        unflat1_simps unflat2_simps unflat3_simps
        sz_of_simps
        GF.rel_map GE.rel_map
        intro: GF.rel_refl GE.rel_refl)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, erule mp, (rule exI conjI refl)+)
  apply (drule conjunct2, erule mp, (rule exI conjI refl)+)
  done

  apply (tactic \<open>mk_sz_of_unflat_raw_tac @{context}
    @{thm rawF_rawE.induct}
    @{thms unflat1_rawF.simps unflat1_rawE.simps}
    @{thms szF.inject szE.inject}
    @{thms sz_of_simps}
    @{thms GF.map_cong GE.map_cong}
    @{thms GF.map_comp GE.map_comp}
  \<close>)
  done

  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: unflat1_rawF.simps unflat1_rawE.simps sz_ofF.simps sz_ofE.simps
      GF.map_comp GE.map_comp o_apply)
  done
*)

lemmas sz_of_unflat1 =
  conjunct1[OF sz_of_unflat1_raw]
  conjunct2[OF sz_of_unflat1_raw]

lemma sz_of_unflat2_raw: 
  "(\<forall>ul. sz_ofF (unflat2_rawF ul r) = sz_ofF (r :: ('a, 'b) rawF)) \<and>
   (\<forall>ul. sz_ofE (unflat2_rawE ul s) = sz_ofE (s :: ('a, 'b) rawE))"
  sorry
(*
  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: unflat2_rawF.simps unflat2_rawE.simps sz_of_simps
      GF.map_comp GE.map_comp o_apply)
  done*)

lemmas sz_of_unflat2 =
  spec[OF conjunct1[OF sz_of_unflat2_raw]]
  spec[OF conjunct2[OF sz_of_unflat2_raw]]

lemma sz_of_unflat3_raw: 
  "(\<forall>ul. sz_ofF (unflat3_rawF ul r) = sz_ofF (r :: ('a, 'b) rawF)) \<and>
   (\<forall>ul. sz_ofE (unflat3_rawE ul s) = sz_ofE (s :: ('a, 'b) rawE))"
  sorry
(*
  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: unflat3_rawF.simps unflat3_rawE.simps sz_of_simps
      GF.map_comp GE.map_comp o_apply)
  done*)

lemmas sz_of_unflat3 =
  spec[OF conjunct1[OF sz_of_unflat3_raw]]
  spec[OF conjunct2[OF sz_of_unflat3_raw]]


ML \<open>
local
open Ctr_Sugar_Util
in

  fun mk_sz_of_map_raw_tac ctxt raw_induct raw_maps sz_injects sz_of_simps
    G_map_congs G_map_comps =
  HEADGOAL (rtac ctxt raw_induct) THEN
  ALLGOALS (Subgoal.FOCUS (fn {context = ctxt, prems = IHs, ...} =>
    let
      val apply_IHs =
        resolve_tac ctxt IHs THEN_ALL_NEW assume_tac ctxt;
      val intro = resolve_tac ctxt G_map_congs;
      val simp = full_simp_tac
        (ss_only (flat [raw_maps, sz_of_simps, G_map_comps, sz_injects,
            @{thms o_apply}]) ctxt);
    in
      HEADGOAL (REPEAT_DETERM o
        FIRST' [hyp_subst_tac ctxt, intro, apply_IHs, simp])
    end) ctxt);
end
\<close>

lemma sz_of_map_raw_raw: 
  "sz_ofF (map_rawF f g t) = sz_ofF t \<and>
   sz_ofE (map_rawE f g s) = sz_ofE s"
  by (tactic \<open>let open Ctr_Sugar_Util open BNF_FP_Rec_Sugar_Util
        fun mk_sz_of_map_raw_cotac ctxt Ps xs ys induct raw_exhausts sz_sels
          raw_maps sz_of_simps G_rel_maps G_rel_refls =
          let
            val n = length Ps;
          in
            HEADGOAL (EVERY' [rtac ctxt rev_mp,
              rtac ctxt (infer_instantiate' ctxt (map SOME (Ps @ ([xs, ys] |> transpose |> flat))) induct)]) THEN
            (HEADGOAL o RANGE) (map (fn exhaust =>  (EVERY' [
              REPEAT_DETERM o (eresolve_tac ctxt [exE, conjE]),
              hyp_subst_tac ctxt,
              Subgoal.FOCUS_PARAMS (fn {context = ctxt, params, ...} =>
                let
                  val exhaust_inst = infer_instantiate' ctxt [(SOME o snd) (nth params 2)] exhaust;
                  val intro = resolve_tac ctxt (exI :: G_rel_refls);
                  val simp = asm_full_simp_tac (ss_only (flat [sz_sels, raw_maps, sz_of_simps, G_rel_maps]) ctxt);
                in
                  HEADGOAL (EVERY' [rtac ctxt exhaust_inst, REPEAT_DETERM o
                    FIRST' [hyp_subst_tac ctxt, intro, simp]])
                end) ctxt])) raw_exhausts) THEN
            HEADGOAL (EVERY' [rtac ctxt impI,
              CONJ_WRAP' (fn i => EVERY' [
                dtac ctxt (mk_conjunctN n i), etac ctxt mp,
                REPEAT_DETERM o (resolve_tac ctxt [exI, conjI, refl])
              ]) (1 upto n)])
          end
      in
        mk_sz_of_map_raw_cotac @{context}
          [@{cterm "\<lambda>l r. \<exists>t. l = sz_ofF (map_rawF f g t) \<and> r = sz_ofF t"},
           @{cterm "\<lambda>l r. \<exists>s. l = sz_ofE (map_rawE f g s) \<and> r = sz_ofE s"}]
          [@{cterm "sz_ofF (map_rawF f g t)"}, @{cterm "sz_ofE (map_rawE f g s)"}]
          [@{cterm "sz_ofF t"}, @{cterm "sz_ofE s"}]
          @{thm szF_szE.coinduct}
          @{thms rawF.exhaust rawE.exhaust}
          @{thms szF.sel szE.sel}
          @{thms rawF.map rawE.map}
          @{thms sz_of_simps}
          @{thms GF.rel_map GE.rel_map}
          @{thms GF.rel_refl GE.rel_refl}
      end\<close>)
(*
  apply (rule rev_mp)
  apply (rule szF_szE.coinduct[of
    "\<lambda>l r. \<exists>t. l = sz_ofF (map_rawF f g t) \<and> r = sz_ofF t"
    "\<lambda>l r. \<exists>s. l = sz_ofE (map_rawE f g s) \<and> r = sz_ofE s"
    "sz_ofF (map_rawF f g t)" "sz_ofF t"
    "sz_ofE (map_rawE f g s)" "sz_ofE s"])
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ t
    apply (rule rawF.exhaust[of t])
    apply (auto simp only: szF.sel szE.sel
        rawF.map rawE.map
        sz_of_simps
        GF.rel_map GE.rel_map
        intro: GF.rel_refl GE.rel_refl)
  done
  apply (erule exE conjE)+
  apply hypsubst
  subgoal for _ _ s
    apply (rule rawE.exhaust[of s])
    apply (auto simp only: szF.sel szE.sel
        rawF.map rawE.map
        sz_of_simps
        GF.rel_map GE.rel_map
        intro: GF.rel_refl GE.rel_refl)
  done
  apply (rule impI)
  apply (rule conjI)
  apply (drule conjunct1, erule mp, (rule exI conjI refl)+)
  apply (drule conjunct2, erule mp, (rule exI conjI refl)+)
  done

  apply (tactic \<open>mk_sz_of_map_raw_tac @{context}
    @{thm rawF_rawE.induct}
    @{thms rawF.map rawE.map}
    @{thms szF.inject szE.inject}
    @{thms sz_of_simps}
    @{thms GF.map_cong GE.map_cong}
    @{thms GF.map_comp GE.map_comp}
  \<close>)
  done

  apply (rule rawF_rawE.induct)
  apply (auto intro: GF.map_cong GE.map_cong
    simp only: rawF.map rawE.map sz_ofF.simps sz_ofE.simps GF.map_comp GE.map_comp o_apply)
  done
*)
lemmas sz_of_map_raw = conjunct1[OF sz_of_map_raw_raw] conjunct2[OF sz_of_map_raw_raw]


ML \<open>
local
open Ctr_Sugar_Util
open BNF_FP_Rec_Sugar_Util
in

  fun mk_raw_induct_sz_tac ctxt IHs reduce_ctrm sz_induct induct_ctrms sz_cases =
    let
      val n = length (IHs |> map (tap (tracing o Thm.string_of_thm ctxt)));
    in
      HEADGOAL (EVERY' [rtac ctxt (infer_instantiate' ctxt [SOME reduce_ctrm] rev_mp),
        REPEAT_DETERM_N n o rtac ctxt allI,
        rtac ctxt (infer_instantiate' ctxt (map SOME induct_ctrms) sz_induct) THEN_ALL_NEW
        Subgoal.FOCUS (fn {context = ctxt, prems = raw_IHs', ...} =>
        let
          val IHs' = map (fn thm => thm RS spec RS mp) raw_IHs';
          val apply_IHs = resolve_tac ctxt IHs THEN'
            EVERY' (map (fn IH' =>
              rtac ctxt IH' THEN_ALL_NEW asm_full_simp_tac (ss_only sz_cases ctxt)) IHs');
          val intro = resolve_tac ctxt [allI, impI];
        in
          HEADGOAL (REPEAT_DETERM o FIRST' [intro, apply_IHs])
        end) ctxt,
        EVERY' [rtac ctxt impI, REPEAT_DETERM_N n o etac ctxt allE,
          CONJ_WRAP' (fn i => EVERY' [dtac ctxt (mk_conjunctN n i), dtac ctxt spec, etac ctxt mp,
            rtac ctxt refl]) (1 upto n)]])
    end;
end
\<close>
lemma raw_induct_sz: 
  assumes "\<And> t :: ('a, 'b) rawF.
    (\<And>t'. sz_ofF t' \<in> set3_GF (case sz_ofF r of SConsF x \<Rightarrow> x) \<Longrightarrow> PP t') \<Longrightarrow>
    (\<And>t'. sz_ofF t' \<in> set4_GF (case sz_ofF r of SConsF x \<Rightarrow> x) \<Longrightarrow> PP t') \<Longrightarrow>
    (\<And>s'. sz_ofE s' \<in> set5_GF (case sz_ofF r of SConsF x \<Rightarrow> x) \<Longrightarrow> QQ s') \<Longrightarrow> PP t"
    "\<And> s :: ('a, 'b) rawE.
    (\<And>t'. sz_ofF t' \<in> set3_GE (case sz_ofE s of SConsE x \<Rightarrow> x) \<Longrightarrow> PP t') \<Longrightarrow>
    (\<And>t'. sz_ofF t' \<in> set4_GE (case sz_ofE s of SConsE x \<Rightarrow> x) \<Longrightarrow> PP t') \<Longrightarrow>
    (\<And>s'. sz_ofE s' \<in> set5_GE (case sz_ofE s of SConsE x \<Rightarrow> x) \<Longrightarrow> QQ s') \<Longrightarrow> QQ s"
  shows "PP t \<and> QQ s"
  sorry
(*
  by (tactic \<open>mk_raw_induct_sz_tac @{context}
    @{thms assms}
    @{cterm "\<forall>szF szE. (\<forall>s. sz_ofF s = szF \<longrightarrow> PP s) \<and> (\<forall>s. sz_ofE s = szE \<longrightarrow> QQ s)"}
    @{thm szF_szE.induct}
    [@{cterm "\<lambda>szF. \<forall>r. sz_ofF r = szF \<longrightarrow> PP r"}, @{cterm "\<lambda>szE. \<forall>s. sz_ofE s = szE \<longrightarrow> QQ s"}]
    @{thms szF.case szE.case}
  \<close>)*)


section \<open>Recursion\<close>

(* MIND-FUCK.THY *)

(*normal datatype recursor
  (('b, 'a) G \<Rightarrow> 'a) \<Rightarrow>
  'b T \<Rightarrow> 'a
*)

(*generalized recursor
  (\<forall>'a. ('a L, 'a F K) G \<Rightarrow> 'a K) \<Rightarrow>
  (\<forall>'a. 'a L F \<Rightarrow> 'a F L) \<Rightarrow>
  'b L T \<Rightarrow> 'b K
*)

bnf_axiomatization ('a, 'b, 'c, 'd) RF
bnf_axiomatization ('a, 'b, 'c, 'd) RE
bnf_axiomatization ('e, 'f, 'g) DA
bnf_axiomatization ('h, 'i, 'j) DB

consts defobjF :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB,
  (('a, 'c) F1A, ('b, 'd) F1A, ('a, 'c) F1B, ('b, 'd) F1B) RF,
  (('a, 'c) F2A, ('b, 'd) F2A, ('a, 'c) F2B, ('b, 'd) F2B) RF,
  (('a, 'c) E3A, ('b, 'd) E3A, ('a, 'c) E3B, ('b, 'd) E3B) RE) GF \<Rightarrow> ('a, 'b, 'c, 'd) RF"
consts defobjE :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB,
  (('a, 'c) F1A, ('b, 'd) F1A, ('a, 'c) F1B, ('b, 'd) F1B) RF,
  (('a, 'c) F2A, ('b, 'd) F2A, ('a, 'c) F2B, ('b, 'd) F2B) RF,
  (('a, 'c) E3A, ('b, 'd) E3A, ('a, 'c) E3B, ('b, 'd) E3B) RE) GE \<Rightarrow> ('a, 'b, 'c, 'd) RE"
consts argobj1A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1A \<Rightarrow> (('e, 'h) F1A, ('f, 'i) F1A, ('g, 'j) F1A) DA"
consts argobj1B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1B \<Rightarrow> (('e, 'h) F1B, ('f, 'i) F1B, ('g, 'j) F1B) DB"
consts argobj2A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2A \<Rightarrow> (('e, 'h) F2A, ('f, 'i) F2A, ('g, 'j) F2A) DA"
consts argobj2B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2B \<Rightarrow> (('e, 'h) F2B, ('f, 'i) F2B, ('g, 'j) F2B) DB"
consts argobj3A :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3A \<Rightarrow> (('e, 'h) E3A, ('f, 'i) E3A, ('g, 'j) E3A) DA"
consts argobj3B :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3B \<Rightarrow> (('e, 'h) E3B, ('f, 'i) E3B, ('g, 'j) E3B) DB"

axiomatization where
  defobjF_transfer: "\<And>RA RB RC RD RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    bi_unique RA \<Longrightarrow> bi_unique RB \<Longrightarrow> bi_unique RC \<Longrightarrow> bi_unique RD \<Longrightarrow>
    rel_fun (rel_GF (rel_DA RE RF RG) (rel_DB RH RI RJ) (rel_RF (rel_F1A RA RC) (rel_F1A RB RD) (rel_F1B RA RC) (rel_F1B RB RD))
    (rel_RF (rel_F2A RA RC) (rel_F2A RB RD) (rel_F2B RA RC) (rel_F2B RB RD)) (rel_RE (rel_E3A RA RC) (rel_E3A RB RD) (rel_E3B RA RC) (rel_E3B RB RD)))
    (rel_RF RA RB RC RD) defobjF defobjF" and
  defobjE_transfer: "\<And>RA RB RC RD RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    bi_unique RA \<Longrightarrow> bi_unique RB \<Longrightarrow> bi_unique RC \<Longrightarrow> bi_unique RD \<Longrightarrow>
    rel_fun (rel_GE (rel_DA RE RF RG) (rel_DB RH RI RJ) (rel_RF (rel_F1A RA RC) (rel_F1A RB RD) (rel_F1B RA RC) (rel_F1B RB RD))
    (rel_RF (rel_F2A RA RC) (rel_F2A RB RD) (rel_F2B RA RC) (rel_F2B RB RD)) (rel_RE (rel_E3A RA RC) (rel_E3A RB RD) (rel_E3B RA RC) (rel_E3B RB RD)))
    (rel_RE RA RB RC RD) defobjE defobjE" and
  argobj1A_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F1A (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DA (rel_F1A RE RH) (rel_F1A RF RI) (rel_F1A RG RJ)) argobj1A argobj1A" and
  argobj1B_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F1B (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DB (rel_F1B RE RH) (rel_F1B RF RI) (rel_F1B RG RJ)) argobj1B argobj1B" and
  argobj2A_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F2A (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DA (rel_F2A RE RH) (rel_F2A RF RI) (rel_F2A RG RJ)) argobj2A argobj2A" and
  argobj2B_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_F2B (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DB (rel_F2B RE RH) (rel_F2B RF RI) (rel_F2B RG RJ)) argobj2B argobj2B" and
  argobj3A_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_E3A (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DA (rel_E3A RE RH) (rel_E3A RF RI) (rel_E3A RG RJ)) argobj3A argobj3A" and
  argobj3B_transfer: "\<And>RE RF RG RH RI RJ. bi_unique RE \<Longrightarrow> bi_unique RF \<Longrightarrow> bi_unique RG \<Longrightarrow>
    bi_unique RH \<Longrightarrow> bi_unique RI \<Longrightarrow> bi_unique RJ \<Longrightarrow>
    rel_fun (rel_E3B (rel_DA RE RF RG) (rel_DB RH RI RJ))
      (rel_DB (rel_E3B RE RH) (rel_E3B RF RI) (rel_E3B RG RJ)) argobj3B argobj3B"

lemma defobjF_invar: "pred_fun (pred_GF (pred_DA RE RF RG) (pred_DB RH RI RJ)
  (pred_RF (pred_F1A RA RC) (pred_F1A RB RD) (pred_F1B RA RC) (pred_F1B RB RD))
  (pred_RF (pred_F2A RA RC) (pred_F2A RB RD) (pred_F2B RA RC) (pred_F2B RB RD))
  (pred_RE (pred_E3A RA RC) (pred_E3A RB RD) (pred_E3B RA RC) (pred_E3B RB RD))) (pred_RF RA RB RC RD) defobjF"
  unfolding fun_pred_rel GF.rel_eq_onp[symmetric]
    F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric] F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric] E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] RF.rel_eq_onp[symmetric] RE.rel_eq_onp[symmetric]
  apply (rule defobjF_transfer) sorry
lemma defobjE_invar: "pred_fun (pred_GE (pred_DA RE RF RG) (pred_DB RH RI RJ)
  (pred_RF (pred_F1A RA RC) (pred_F1A RB RD) (pred_F1B RA RC) (pred_F1B RB RD))
  (pred_RF (pred_F2A RA RC) (pred_F2A RB RD) (pred_F2B RA RC) (pred_F2B RB RD))
  (pred_RE (pred_E3A RA RC) (pred_E3A RB RD) (pred_E3B RA RC) (pred_E3B RB RD))) (pred_RE RA RB RC RD) defobjE"
  unfolding fun_pred_rel GE.rel_eq_onp[symmetric]
    F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric] F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric] E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] RF.rel_eq_onp[symmetric] RE.rel_eq_onp[symmetric]
  apply (rule defobjE_transfer) sorry

lemma argobj1A_invar: "pred_fun (pred_F1A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_F1A RE RH) (pred_F1A RF RI) (pred_F1A RG RJ)) argobj1A"
  unfolding fun_pred_rel F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj1A_transfer) sorry
lemma argobj1B_invar: "pred_fun (pred_F1B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_F1B RE RH) (pred_F1B RF RI) (pred_F1B RG RJ)) argobj1B"
  unfolding fun_pred_rel F1A.rel_eq_onp[symmetric] F1B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj1B_transfer) sorry
lemma argobj2A_invar: "pred_fun (pred_F2A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_F2A RE RH) (pred_F2A RF RI) (pred_F2A RG RJ)) argobj2A"
  unfolding fun_pred_rel F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj2A_transfer) sorry
lemma argobj2B_invar: "pred_fun (pred_F2B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_F2B RE RH) (pred_F2B RF RI) (pred_F2B RG RJ)) argobj2B"
  unfolding fun_pred_rel F2A.rel_eq_onp[symmetric] F2B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj2B_transfer) sorry
lemma argobj3A_invar: "pred_fun (pred_E3A (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_E3A RE RH) (pred_E3A RF RI) (pred_E3A RG RJ)) argobj3A"
  unfolding fun_pred_rel E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj3A_transfer) sorry
lemma argobj3B_invar: "pred_fun (pred_E3B (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_E3B RE RH) (pred_E3B RF RI) (pred_E3B RG RJ)) argobj3B"
  unfolding fun_pred_rel E3A.rel_eq_onp[symmetric] E3B.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] apply (rule argobj3B_transfer) sorry

lemma GrpD: "BNF_Def.Grp A f x y \<Longrightarrow> f x = y"
  by (rule GrpE)

lemma defobjF_natural:
  assumes
  "inj_on fe (\<Union>(set1_DA ` set1_GF g))" 
  "inj_on ff (\<Union>(set2_DA ` set1_GF g))"
  "inj_on fg (\<Union>(set3_DA ` set1_GF g))"
  "inj_on fh (\<Union>(set1_DB ` set2_GF g))" 
  "inj_on fi (\<Union>(set2_DB ` set2_GF g))"
  "inj_on fj (\<Union>(set3_DB ` set2_GF g))"
  "inj_on fa (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GF g)))"
  "inj_on fb (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GF g)))"
  "inj_on fc (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GF g)))"
  "inj_on fd (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GF g)))"
  shows "defobjF (map_GF (map_DA fe ff fg) (map_DB fh fi fj)
   (map_RF (map_F1A fa fc) (map_F1A fb fd) (map_F1B fa fc) (map_F1B fb fd))
   (map_RF (map_F2A fa fc) (map_F2A fb fd) (map_F2B fa fc) (map_F2B fb fd))
   (map_RE (map_E3A fa fc) (map_E3A fb fd) (map_E3B fa fc) (map_E3B fb fd)) g) = map_RF fa fb fc fd (defobjF g)"
  apply (rule rel_funD[OF defobjF_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_GF g)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_GF g)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_GF g)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_GF g)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_GF g)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_GF g)) fj"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GF g))) fa"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GF g))) fb"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GF g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GF g))) fc"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GF g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GF g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GF g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GF g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GF g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GF g))) fd",
    unfolded bi_unique_Grp, OF assms, unfolded
      F1A.rel_Grp F1B.rel_Grp F2A.rel_Grp F2B.rel_Grp E3A.rel_Grp E3B.rel_Grp
      RF.rel_Grp RE.rel_Grp DA.rel_Grp DB.rel_Grp GF.rel_Grp GE.rel_Grp, THEN GrpD, THEN sym])
  apply (rule GrpI[OF refl])
  apply (blast 12)
  done
lemma defobjE_natural:
  assumes
  "inj_on fe (\<Union>(set1_DA ` set1_GE g))" 
  "inj_on ff (\<Union>(set2_DA ` set1_GE g))"
  "inj_on fg (\<Union>(set3_DA ` set1_GE g))"
  "inj_on fh (\<Union>(set1_DB ` set2_GE g))" 
  "inj_on fi (\<Union>(set2_DB ` set2_GE g))"
  "inj_on fj (\<Union>(set3_DB ` set2_GE g))"
  "inj_on fa (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GE g)))"
  "inj_on fb (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
              \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GE g)))"
  "inj_on fc (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GE g)))"
  "inj_on fd (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
              \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GE g)))"
  shows  "defobjE (map_GE (map_DA fe ff fg) (map_DB fh fi fj)
   (map_RF (map_F1A fa fc) (map_F1A fb fd) (map_F1B fa fc) (map_F1B fb fd))
   (map_RF (map_F2A fa fc) (map_F2A fb fd) (map_F2B fa fc) (map_F2B fb fd))
   (map_RE (map_E3A fa fc) (map_E3A fb fd) (map_E3B fa fc) (map_E3B fb fd)) g) = map_RE fa fb fc fd (defobjE g)"
  apply (rule rel_funD[OF defobjE_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_GE g)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_GE g)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_GE g)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_GE g)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_GE g)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_GE g)) fj"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set3_RE ` set5_GE g))) fa"
    "BNF_Def.Grp (\<Union>(set1_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set1_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set1_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
                  \<Union>(set1_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set1_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set1_E3B ` \<Union>(set4_RE ` set5_GE g))) fb"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set1_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set1_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set1_RE ` set5_GE g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set3_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set3_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set3_RE ` set5_GE g))) fc"
    "BNF_Def.Grp (\<Union>(set2_F1A ` \<Union>(set2_RF ` set3_GE g)) \<union> \<Union>(set2_F2A ` \<Union>(set2_RF ` set4_GE g)) \<union> \<Union>(set2_E3A ` \<Union>(set2_RE ` set5_GE g)) \<union>
                  \<Union>(set2_F1B ` \<Union>(set4_RF ` set3_GE g)) \<union> \<Union>(set2_F2B ` \<Union>(set4_RF ` set4_GE g)) \<union> \<Union>(set2_E3B ` \<Union>(set4_RE ` set5_GE g))) fd",
    unfolded bi_unique_Grp, OF assms, unfolded
      F1A.rel_Grp F1B.rel_Grp F2A.rel_Grp F2B.rel_Grp E3A.rel_Grp E3B.rel_Grp
      RF.rel_Grp RE.rel_Grp DA.rel_Grp DB.rel_Grp GF.rel_Grp GE.rel_Grp, THEN GrpD, THEN sym])
  apply (rule GrpI[OF refl])
  apply (blast 12)
  done

lemma argobj1A_natural: "inj_on fe (\<Union>(set1_DA ` set1_F1A f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F1A f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F1A f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F1A f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F1A f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F1A f)) \<Longrightarrow>
   argobj1A (map_F1A (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DA (map_F1A fe fh) (map_F1A ff fi) (map_F1A fg fj) (argobj1A f)"
  using rel_funD[OF argobj1A_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F1A f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F1A f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F1A f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F1A f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F1A f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F1A f)) fj"]
  unfolding F1A.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj1B_natural: "inj_on fe (\<Union>(set1_DA ` set1_F1B f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F1B f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F1B f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F1B f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F1B f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F1B f)) \<Longrightarrow>
   argobj1B (map_F1B (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DB (map_F1B fe fh) (map_F1B ff fi) (map_F1B fg fj) (argobj1B f)"
  using rel_funD[OF argobj1B_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F1B f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F1B f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F1B f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F1B f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F1B f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F1B f)) fj"]
  unfolding F1B.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj2A_natural: "inj_on fe (\<Union>(set1_DA ` set1_F2A f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F2A f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F2A f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F2A f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F2A f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F2A f)) \<Longrightarrow>
   argobj2A (map_F2A (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DA (map_F2A fe fh) (map_F2A ff fi) (map_F2A fg fj) (argobj2A f)"
  using rel_funD[OF argobj2A_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F2A f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F2A f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F2A f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F2A f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F2A f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F2A f)) fj"]
  unfolding F2A.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj2B_natural: "inj_on fe (\<Union>(set1_DA ` set1_F2B f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_F2B f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_F2B f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_F2B f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_F2B f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_F2B f)) \<Longrightarrow>
   argobj2B (map_F2B (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DB (map_F2B fe fh) (map_F2B ff fi) (map_F2B fg fj) (argobj2B f)"
  using rel_funD[OF argobj2B_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_F2B f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_F2B f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_F2B f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_F2B f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_F2B f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_F2B f)) fj"]
  unfolding F2B.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj3A_natural: "inj_on fe (\<Union>(set1_DA ` set1_E3A f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_E3A f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_E3A f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_E3A f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_E3A f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_E3A f)) \<Longrightarrow>
   argobj3A (map_E3A (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DA (map_E3A fe fh) (map_E3A ff fi) (map_E3A fg fj) (argobj3A f)"
  using rel_funD[OF argobj3A_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_E3A f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_E3A f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_E3A f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_E3A f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_E3A f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_E3A f)) fj"]
  unfolding E3A.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)
lemma argobj3B_natural: "inj_on fe (\<Union>(set1_DA ` set1_E3B f)) \<Longrightarrow>
   inj_on ff (\<Union>(set2_DA ` set1_E3B f)) \<Longrightarrow>
   inj_on fg (\<Union>(set3_DA ` set1_E3B f)) \<Longrightarrow>
   inj_on fh (\<Union>(set1_DB ` set2_E3B f)) \<Longrightarrow>
   inj_on fi (\<Union>(set2_DB ` set2_E3B f)) \<Longrightarrow>
   inj_on fj (\<Union>(set3_DB ` set2_E3B f)) \<Longrightarrow>
   argobj3B (map_E3B (map_DA fe ff fg) (map_DB fh fi fj) f) = map_DB (map_E3B fe fh) (map_E3B ff fi) (map_E3B fg fj) (argobj3B f)"
  using rel_funD[OF argobj3B_transfer, of
    "BNF_Def.Grp (\<Union>(set1_DA ` set1_E3B f)) fe" "BNF_Def.Grp (\<Union>(set2_DA ` set1_E3B f)) ff" "BNF_Def.Grp (\<Union>(set3_DA ` set1_E3B f)) fg"
    "BNF_Def.Grp (\<Union>(set1_DB ` set2_E3B f)) fh" "BNF_Def.Grp (\<Union>(set2_DB ` set2_E3B f)) fi" "BNF_Def.Grp (\<Union>(set3_DB ` set2_E3B f)) fj"]
  unfolding E3B.rel_Grp DA.rel_Grp DB.rel_Grp bi_unique_Grp by (auto simp: Grp_def)


lemma defobjF_flat1_natural:fixes
    u0 :: "label list" and
    gf :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
            ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) gf \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B)
    (map_RF (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B))
    (map_RF (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B))
    (map_RE (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B)) gf) =
    map_RF flat_shape1A flat_shape1A flat_shape1B flat_shape1B (defobjF gf)"
  sorry
lemma defobjF_flat2_natural:fixes
    u0 :: "label list" and
    gf :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
            ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) gf \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B)
    (map_RF (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B))
    (map_RF (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B))
    (map_RE (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B)) gf) =
    map_RF flat_shape2A flat_shape2A flat_shape2B flat_shape2B (defobjF gf)"
  sorry
lemma defobjF_flat3_natural:fixes
    u0 :: "label list" and
    gf :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
            ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) gf \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B)
    (map_RF (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B))
    (map_RF (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B))
    (map_RE (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B)) gf) =
    map_RF flat_shape3A flat_shape3A flat_shape3B flat_shape3B (defobjF gf)"
  sorry
lemma defobjE_flat1_natural:fixes
    u0 :: "label list" and
    ge :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
            ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F1B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) F2B) RF,
            (((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3A,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B,
             ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) ge \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B)
    (map_RF (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B))
    (map_RF (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B))
    (map_RE (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B)) ge) =
    map_RF flat_shape1A flat_shape1A flat_shape1B flat_shape1B (defobjF ge)"
  sorry
lemma defobjE_flat2_natural:fixes
    u0 :: "label list" and
    ge :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
            ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F1B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) F2B) RF,
            (((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3A,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B,
             ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) ge \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B)
    (map_RF (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B))
    (map_RF (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B))
    (map_RE (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B)) ge) =
    map_RF flat_shape2A flat_shape2A flat_shape2B flat_shape2B (defobjF ge)"
  sorry
lemma defobjE_flat3_natural:fixes
    u0 :: "label list" and
    ge :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
            ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F1B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) F2B) RF,
            (((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3A,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B,
             ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) E3B) RE) GF"
  shows
    "pred_GF
      (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
      (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) 
      (pred_RF (pred_F1A (invar_shape3A u0) (invar_shape3B u0)) (pred_F1A  (invar_shape3A u0) (invar_shape3B u0))
               (pred_F1B (invar_shape3A u0) (invar_shape3B u0)) (pred_F1B  (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RF (pred_F2A (invar_shape3A u0) (invar_shape3B u0)) (pred_F2A (invar_shape3A u0) (invar_shape3B u0))
               (pred_F2B (invar_shape3A u0) (invar_shape3B u0)) (pred_F2B (invar_shape3A u0) (invar_shape3B u0)))
      (pred_RE (pred_E3A (invar_shape3A u0) (invar_shape3B u0)) (pred_E3A (invar_shape3A u0) (invar_shape3B u0))
               (pred_E3B (invar_shape3A u0) (invar_shape3B u0)) (pred_E3B (invar_shape3A u0) (invar_shape3B u0))) ge \<Longrightarrow>
    defobjF (map_GF (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B)
    (map_RF (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B))
    (map_RF (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B))
    (map_RE (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B)) ge) =
    map_RF flat_shape3A flat_shape3A flat_shape3B flat_shape3B (defobjF ge)"
  sorry


lemma argobj1A_flat1_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F1A"
  shows
    "pred_F1A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1A (map_F1A (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DA (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (map_F1A flat_shape1A flat_shape1B) (argobj1A x)"
  apply (rule argobj1A_natural; rule inj_onI)
  apply(rule box_equals[OF _ unflat_shape_flat_shape(1)[of u] unflat_shape_flat_shape(1)[of u]])
  apply (auto simp only: unflat_shape_flat_shape F1A.pred_set F1B.pred_set F2A.pred_set F2B.pred_set E3A.pred_set E3B.pred_set DA.pred_set DB.pred_set)
  sorry
lemma argobj1A_flat2_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F1A"
  shows
    "pred_F1A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1A (map_F1A (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DA (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (map_F1A flat_shape2A flat_shape2B) (argobj1A x)"
  sorry
lemma argobj1A_flat3_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F1A"
  shows
    "pred_F1A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1A (map_F1A (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DA (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (map_F1A flat_shape3A flat_shape3B) (argobj1A x)"
  sorry
lemma argobj1B_flat1_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F1B"
  shows
    "pred_F1B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1B (map_F1B (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DB (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (map_F1B flat_shape1A flat_shape1B) (argobj1B x)"
  sorry
lemma argobj1B_flat2_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F1B"
  shows
    "pred_F1B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1B (map_F1B (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DB (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (map_F1B flat_shape2A flat_shape2B) (argobj1B x)"
  sorry
lemma argobj1B_flat3_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F1B"
  shows
    "pred_F1B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj1B (map_F1B (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DB (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (map_F1B flat_shape3A flat_shape3B) (argobj1B x)"
  sorry
lemma argobj2A_flat1_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F2A"
  shows
    "pred_F2A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2A (map_F2A (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DA (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (map_F2A flat_shape1A flat_shape1B) (argobj2A x)"
  sorry
lemma argobj2A_flat2_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F2A"
  shows
    "pred_F2A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2A (map_F2A (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DA (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (map_F2A flat_shape2A flat_shape2B) (argobj2A x)"
  sorry
lemma argobj2A_flat3_natural: fixes
    u0 :: "label list" and
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F2A"
  shows
    "pred_F2A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2A (map_F2A (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DA (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (map_F2A flat_shape3A flat_shape3B) (argobj2A x)"
  sorry
lemma argobj2B_flat1_natural: fixes
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) F2B"
  shows
    "pred_F2B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2B (map_F2B (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DB (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (map_F2B flat_shape1A flat_shape1B) (argobj2B x)"
  sorry
lemma argobj2B_flat2_natural: fixes
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) F2B"
  shows
    "pred_F2B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2B (map_F2B (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DB (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (map_F2B flat_shape2A flat_shape2B) (argobj2B x)"
  sorry
lemma argobj2B_flat3_natural: fixes
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) F2B"
  shows
    "pred_F2B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj2B (map_F2B (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DB (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (map_F2B flat_shape3A flat_shape3B) (argobj2B x)"
  sorry
lemma argobj3A_flat1_natural: fixes
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) E3A"
  shows
    "pred_E3A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3A (map_E3A (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DA (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (map_E3A flat_shape1A flat_shape1B) (argobj3A x)"
  sorry
lemma argobj3A_flat2_natural: fixes
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) E3A"
  shows
    "pred_E3A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3A (map_E3A (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DA (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (map_E3A flat_shape2A flat_shape2B) (argobj3A x)"
  sorry
lemma argobj3A_flat3_natural: fixes
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) E3A"
  shows
    "pred_E3A (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3A (map_E3A (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DA (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (map_E3A flat_shape3A flat_shape3B) (argobj3A x)"
  sorry
lemma argobj3B_flat1_natural: fixes
    x :: "(((('a, 'b) F1A, ('a, 'b) F1B) shape3A, (('c, 'd) F1A, ('c, 'd) F1B) shape3A, (('e, 'f) F1A, ('e, 'f) F1B) shape3A) DA,
           ((('a, 'b) F1A, ('a, 'b) F1B) shape3B, (('c, 'd) F1A, ('c, 'd) F1B) shape3B, (('e, 'f) F1A, ('e, 'f) F1B) shape3B) DB) E3B"
  shows
    "pred_E3B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3B (map_E3B (map_DA flat_shape1A flat_shape1A flat_shape1A) (map_DB flat_shape1B flat_shape1B flat_shape1B) x) =
    map_DB (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (map_E3B flat_shape1A flat_shape1B) (argobj3B x)"
  sorry
lemma argobj3B_flat2_natural: fixes
    x :: "(((('a, 'b) F2A, ('a, 'b) F2B) shape3A, (('c, 'd) F2A, ('c, 'd) F2B) shape3A, (('e, 'f) F2A, ('e, 'f) F2B) shape3A) DA,
           ((('a, 'b) F2A, ('a, 'b) F2B) shape3B, (('c, 'd) F2A, ('c, 'd) F2B) shape3B, (('e, 'f) F2A, ('e, 'f) F2B) shape3B) DB) E3B"
  shows
    "pred_E3B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3B (map_E3B (map_DA flat_shape2A flat_shape2A flat_shape2A) (map_DB flat_shape2B flat_shape2B flat_shape2B) x) =
    map_DB (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (map_E3B flat_shape2A flat_shape2B) (argobj3B x)"
  sorry
lemma argobj3B_flat3_natural: fixes
    x :: "(((('a, 'b) E3A, ('a, 'b) E3B) shape3A, (('c, 'd) E3A, ('c, 'd) E3B) shape3A, (('e, 'f) E3A, ('e, 'f) E3B) shape3A) DA,
           ((('a, 'b) E3A, ('a, 'b) E3B) shape3B, (('c, 'd) E3A, ('c, 'd) E3B) shape3B, (('e, 'f) E3A, ('e, 'f) E3B) shape3B) DB) E3B"
  shows
    "pred_E3B (pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0))
              (pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0)) x \<Longrightarrow>
    argobj3B (map_E3B (map_DA flat_shape3A flat_shape3A flat_shape3A) (map_DB flat_shape3B flat_shape3B flat_shape3B) x) =
    map_DB (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (map_E3B flat_shape3A flat_shape3B) (argobj3B x)"
  sorry


(*
lemma argobj_flat_shape_natural: "pred_F (pred_D (invar_shape u) (invar_shape u) (invar_shape u)) x \<Longrightarrow>
  argobj (map_F (map_D flat_shape flat_shape flat_shape) x) =
  map_D (map_F flat_shape) (map_F flat_shape) (map_F flat_shape) (argobj x)"
  apply (rule argobj_natural; rule inj_onI;
    rule box_equals[OF _ unflat_shape_flat_shape[of u] unflat_shape_flat_shape[of u]])
  apply (auto simp only: F.pred_set D.pred_set)
  done*)


primrec f_shapeA :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shape3A \<Rightarrow> (('e, 'h) shape3A, ('f, 'i) shape3A, ('g, 'j) shape3A) DA"
    and f_shapeB :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shape3B \<Rightarrow> (('e, 'h) shape3B, ('f, 'i) shape3B, ('g, 'j) shape3B) DB" where
  "f_shapeA (LeafA a) = map_DA LeafA LeafA LeafA a"
| "f_shapeA (Node1A fa) = map_DA Node1A Node1A Node1A (argobj1A (map_F1A f_shapeA f_shapeB fa))"
| "f_shapeA (Node2A fa) = map_DA Node2A Node2A Node2A (argobj2A (map_F2A f_shapeA f_shapeB fa))"
| "f_shapeA (Node3A fa) = map_DA Node3A Node3A Node3A (argobj3A (map_E3A f_shapeA f_shapeB fa))"
| "f_shapeB (LeafB b) = map_DB LeafB LeafB LeafB b"
| "f_shapeB (Node1B fb) = map_DB Node1B Node1B Node1B (argobj1B (map_F1B f_shapeA f_shapeB fb))"
| "f_shapeB (Node2B fb) = map_DB Node2B Node2B Node2B (argobj2B (map_F2B f_shapeA f_shapeB fb))"
| "f_shapeB (Node3B fb) = map_DB Node3B Node3B Node3B (argobj3B (map_E3B f_shapeA f_shapeB fb))"
(*
primcorec f_rawF :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) rawF \<Rightarrow>
  (('a, 'c) shape3A, ('b, 'd) shape3A, ('a, 'c) shape3B, ('b, 'd) shape3B) RF" and
  f_rawE :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) rawE \<Rightarrow>
  (('a, 'c) shape3A, ('b, 'd) shape3A, ('a, 'c) shape3B, ('b, 'd) shape3B) RE" where
  "f_rawF r = defobjF (map_GF f_shapeA f_shapeB ((map_RF un_Node1A un_Node1A un_Node1B un_Node1B) o f_rawF)
    ((map_RF un_Node2A un_Node2A un_Node2B un_Node2B) o f_rawF) ((map_RE un_Node3A un_Node3A un_Node3B un_Node3B) o f_rawE) (un_ConsF r))"
| "f_rawE r = defobjE (map_GE f_shapeA f_shapeB ((map_RF un_Node1A un_Node1A un_Node1B un_Node1B) o f_rawF)
    ((map_RF un_Node2A un_Node2A un_Node2B un_Node2B) o f_rawF) ((map_RE un_Node3A un_Node3A un_Node3B un_Node3B) o f_rawE) (un_ConsE g))"

definition f_T :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) T \<Rightarrow> ('a, 'b, 'c, 'd) RF" where
  "f_T t = map_RF un_LeafA un_LeafA un_LeafB un_LeafB (f_rawF (Rep_T t))"
definition f_S :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) S \<Rightarrow> ('a, 'b, 'c, 'd) RE" where
  "f_S t = map_RE un_LeafA un_LeafA un_LeafB un_LeafB (f_rawE (Rep_S t))"

lemma invar_shape_f_shape: fixes ua :: "(('a, 'b, 'c) DA, ('d, 'e, 'f) DB) shape3A" and ub :: "(('a, 'b, 'c) DA, ('d, 'e, 'f) DB) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0) (f_shapeA ua)) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0) (f_shapeB ub))"
  apply (rule shape3A_shape3B.induct[of
    "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> pred_DA (invar_shape3A u0) (invar_shape3A u0) (invar_shape3A u0) (f_shapeA ua)"
    "\<lambda>ub. \<forall>u0. invar_shape3B u0 ub \<longrightarrow> pred_DB (invar_shape3B u0) (invar_shape3B u0) (invar_shape3B u0) (f_shapeB ub)"
    ua ub])
  apply (auto simp only:
      invar_shape3A.simps invar_shape3B.simps
      f_shapeA.simps f_shapeB.simps
      DA.pred_map DB.pred_map DA.pred_True DB.pred_True
      F1A.pred_map F1B.pred_map F2A.pred_map F2B.pred_map E3A.pred_map E3B.pred_map
      o_apply simp_thms all_simps True_implies_equals imp_conjL list.inject list.distinct
    intro!: pred_funD[OF argobj1A_invar] pred_funD[OF argobj2A_invar] pred_funD[OF argobj3A_invar]
      pred_funD[OF argobj1B_invar] pred_funD[OF argobj2B_invar] pred_funD[OF argobj3B_invar]
    elim!: F1A.pred_mono_strong F1B.pred_mono_strong F2A.pred_mono_strong F2B.pred_mono_strong
      E3A.pred_mono_strong E3B.pred_mono_strong
    cong: DA.pred_cong DB.pred_cong imp_cong
    split: list.splits label.splits if_splits)
  done

lemma f_shape_flat1_shape: fixes
      ua :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) shape3A" and 
      ub :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> f_shapeA (flat_shape1A ua) = map_DA flat_shape1A flat_shape1A flat_shape1A (f_shapeA (map_shape3A argobj1A argobj1B ua))) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> f_shapeB (flat_shape1B ub) = map_DB flat_shape1B flat_shape1B flat_shape1B (f_shapeB (map_shape3B argobj1A argobj1B ub)))"
  apply (rule shape3A_shape3B.induct[of
        "\<lambda>ua. \<forall>u0. invar_shape3A u0 ua \<longrightarrow> f_shapeA (flat_shape1A ua) = map_DA flat_shape1A flat_shape1A flat_shape1A (f_shapeA (map_shape3A argobj1A argobj1B ua))"
        "\<lambda>ub.\<forall>u0. invar_shape3B u0 ub \<longrightarrow> f_shapeB (flat_shape1B ub) = map_DB flat_shape1B flat_shape1B flat_shape1B (f_shapeB (map_shape3B argobj1A argobj1B ub))"
        "ua :: ((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) shape3A"
        "ub :: ((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) shape3B"])
  apply (simp only:
      flat_shape1A.simps flat_shape1B.simps
      f_shapeA.simps f_shapeB.simps
      shape3A.map shape3B.map
      DA.map_comp DB.map_comp
      F1A.map_comp F1B.map_comp F2A.map_comp F2B.map_comp E3A.map_comp E3B.map_comp
      argobj1A_natural argobj1B_natural argobj2A_natural argobj2B_natural argobj3A_natural argobj3B_natural
      shape3A.inject shape3B.inject
      inj_on_def o_apply simp_thms Ball_def o_def

    cong: F1A.map_cong F1B.map_cong F2A.map_cong F2B.map_cong E3A.map_cong E3B.map_cong
      DA.map_cong DB.map_cong DA.pred_cong DB.pred_cong imp_cong)
  apply (rule allI)
  subgoal premises IH for f u0
    apply (unfold flat_shape1A.simps f_shapeA.simps shape3A.map DA.map_comp o_def)
    apply (auto simp only: DA.map_comp[unfolded o_def, symmetric]
        F1A.set_map F1A.map_comp F1A.pred_set invar_shape3A.simps list.sel o_apply
        invar_shape_map_closed invar_shape_f_shape f_shapeA.simps
      intro!: arg_cong[of _ _ argobj1A] DA.map_cong F1A.map_cong
         IH[THEN spec, THEN mp, of _ "tl u0"]
         trans[OF _ argobj1A_flat1_natural[of "tl u0"]]
      elim!: F1A.pred_mono_strong
      split: list.splits label.splits if_splits)
    done

  apply (rule allI)
  subgoal premises IH for f u0
    apply (unfold flat_shape1A.simps f_shapeA.simps shape3A.map DA.map_comp o_def)
    apply (auto simp only: DA.map_comp[unfolded o_def, symmetric]
        F2A.set_map F2A.map_comp F2A.pred_set invar_shape3A.simps list.sel o_apply
        invar_shape_map_closed invar_shape_f_shape f_shapeA.simps
      intro!: arg_cong[of _ _ argobj2A] DA.map_cong F2A.map_cong
         IH[THEN spec, THEN mp, of _ "tl u0"]
         trans[OF _ argobj2A_flat1_natural[of "tl u0"]]
      elim!: F2A.pred_mono_strong
      split: list.splits label.splits if_splits)
    done
  done




lemma f_shape_flat2_shape: fixes
      ua :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) shape3A" and 
      ub :: "((('g, 'h, 'i) DA, ('j, 'k, 'l) DB) F2A, (('g, 'h, 'i) DA, ('j, 'k, 'l) DB) F2B) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> f_shapeA (flat_shape2A ua) = map_DA flat_shape2A flat_shape2A flat_shape2A (f_shapeA (map_shape3A argobj2A argobj2B ua))) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> f_shapeB (flat_shape2B ub) = map_DB flat_shape2B flat_shape2B flat_shape2B (f_shapeB (map_shape3B argobj2A argobj2B ub)))"
  sorry
lemma f_shape_flat3_shape: fixes
      ua :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) shape3A" and 
      ub :: "((('g, 'h, 'i) DA, ('j, 'k, 'l) DB) E3A, (('g, 'h, 'i) DA, ('j, 'k, 'l) DB) E3B) shape3B"
    shows
  "(\<forall>u0. invar_shape3A u0 ua \<longrightarrow> f_shapeA (flat_shape3A ua) = map_DA flat_shape3A flat_shape3A flat_shape3A (f_shapeA (map_shape3A argobj3A argobj3B ua))) \<and>
   (\<forall>u0. invar_shape3B u0 ub \<longrightarrow> f_shapeB (flat_shape3B ub) = map_DB flat_shape3B flat_shape3B flat_shape3B (f_shapeB (map_shape3B argobj3A argobj3B ub)))"
  sorry

lemma invar_f_raw1: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> pred_RF (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawF ::
  ((('a, 'd) F1A, ('b, 'e) F1A, ('c, 'f) F1A) DA, (('a, 'd) F1B, ('b, 'e) F1B, ('c, 'f) F1B) DB) rawF
   \<Rightarrow> ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3A,
       (('g, 'h) F1A, ('g, 'h) F1B) shape3B, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) RF) (map_rawF argobj1A argobj1B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> pred_RE (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawE ::
  ((('a, 'd) F1A, ('b, 'e) F1A, ('c, 'f) F1A) DA, (('a, 'd) F1B, ('b, 'e) F1B, ('c, 'f) F1B) DB) rawE
   \<Rightarrow> ((('g, 'h) F1A, ('g, 'h) F1B) shape3A, (('i, 'j) F1A, ('i, 'j) F1B) shape3A,
       (('g, 'h) F1A, ('g, 'h) F1B) shape3B, (('i, 'j) F1A, ('i, 'j) F1B) shape3B) RE) (map_rawE argobj1A argobj1B s)))"
  sorry
lemma invar_f_raw2: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> pred_RF (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawF ::
  ((('a, 'd) F2A, ('b, 'e) F2A, ('c, 'f) F2A) DA, (('a, 'd) F2B, ('b, 'e) F2B, ('c, 'f) F2B) DB) rawF
   \<Rightarrow> ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3A,
       (('g, 'h) F2A, ('g, 'h) F2B) shape3B, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) RF) (map_rawF argobj2A argobj2B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> pred_RE (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawE ::
  ((('a, 'd) F2A, ('b, 'e) F2A, ('c, 'f) F2A) DA, (('a, 'd) F2B, ('b, 'e) F2B, ('c, 'f) F2B) DB) rawE
   \<Rightarrow> ((('g, 'h) F2A, ('g, 'h) F2B) shape3A, (('i, 'j) F2A, ('i, 'j) F2B) shape3A,
       (('g, 'h) F2A, ('g, 'h) F2B) shape3B, (('i, 'j) F2A, ('i, 'j) F2B) shape3B) RE) (map_rawE argobj2A argobj2B s)))"
  sorry
lemma invar_f_raw3: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> pred_RF (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawF ::
  ((('a, 'd) E3A, ('b, 'e) E3A, ('c, 'f) E3A) DA, (('a, 'd) E3B, ('b, 'e) E3B, ('c, 'f) E3B) DB) rawF
   \<Rightarrow> ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3A,
       (('g, 'h) E3A, ('g, 'h) E3B) shape3B, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) RF) (map_rawF argobj3A argobj3B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> pred_RE (invar_shape3A u0) (invar_shape3A u0) (invar_shape3B u0) (invar_shape3B u0) ((f_rawE ::
  ((('a, 'd) E3A, ('b, 'e) E3A, ('c, 'f) E3A) DA, (('a, 'd) E3B, ('b, 'e) E3B, ('c, 'f) E3B) DB) rawE
   \<Rightarrow> ((('g, 'h) E3A, ('g, 'h) E3B) shape3A, (('i, 'j) E3A, ('i, 'j) E3B) shape3A,
       (('g, 'h) E3A, ('g, 'h) E3B) shape3B, (('i, 'j) E3A, ('i, 'j) E3B) shape3B) RE) (map_rawE argobj3A argobj3B s)))"
  sorry

lemma f_raw_flat1: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F1B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> f_rawF (flat1_rawF t) = map_RF flat_shape1A flat_shape1A flat_shape1B flat_shape1B (f_rawF (map_rawF argobj1A argobj1B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> f_rawE (flat1_rawE s) = map_RE flat_shape1A flat_shape1A flat_shape1B flat_shape1B (f_rawE (map_rawE argobj1A argobj1B s)))"
  sorry
lemma f_raw_flat2: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) F2B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> f_rawF (flat2_rawF t) = map_RF flat_shape2A flat_shape2A flat_shape2B flat_shape2B (f_rawF (map_rawF argobj2A argobj2B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> f_rawE (flat2_rawE s) = map_RE flat_shape2A flat_shape2A flat_shape2B flat_shape2B (f_rawE (map_rawE argobj2A argobj2B s)))"
  sorry
lemma f_raw_flat3: fixes
      t :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawF" and
      s :: "((('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3A, (('a, 'b, 'c) DA, ('d, 'e, 'f) DB) E3B) rawE"
    shows
 "(\<forall>u0. invar_rawF u0 t \<longrightarrow> f_rawF (flat3_rawF t) = map_RF flat_shape3A flat_shape3A flat_shape3B flat_shape3B (f_rawF (map_rawF argobj3A argobj3B t))) \<and>
  (\<forall>u0. invar_rawE u0 s \<longrightarrow> f_rawE (flat3_rawE s) = map_RE flat_shape3A flat_shape3A flat_shape3B flat_shape3B (f_rawE (map_rawE argobj3A argobj3B s)))"
  sorry

lemma fixes
      g :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB,
        ((('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1A,
         (('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F1B) T,
        ((('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2A,
         (('e, 'f, 'g) DA, ('h, 'i, 'j) DB) F2B) T,
        ((('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3A,
         (('e, 'f, 'g) DA, ('h, 'i, 'j) DB) E3B) S) GF" and
      h :: "(('o, 'p, 'q) DA, ('r, 's, 't) DB,
        ((('o, 'p, 'q) DA, ('r, 's, 't) DB) F1A,
         (('o, 'p, 'q) DA, ('r, 's, 't) DB) F1B) T,
        ((('o, 'p, 'q) DA, ('r, 's, 't) DB) F2A,
         (('o, 'p, 'q) DA, ('r, 's, 't) DB) F2B) T,
        ((('o, 'p, 'q) DA, ('r, 's, 't) DB) E3A,
         (('o, 'p, 'q) DA, ('r, 's, 't) DB) E3B) S) GE"
    shows
 "f_T (T g) = defobjF (map_GF id id (f_T o map_T argobj1A argobj1B) (f_T o map_T argobj2A argobj2B) (f_S o map_S argobj3A argobj3B) g) \<and>
  f_S (S h) = defobjE (map_GE id id (f_T o map_T argobj1A argobj1B) (f_T o map_T argobj2A argobj2B) (f_S o map_S argobj3A argobj3B) h)"
  sorry
*)
section \<open>Coinduction\<close>

abbreviation "immerse1F ul \<equiv> map_rawF Node1A Node1B o unflat1_rawF (rev ul)"
abbreviation "immerse2F ul \<equiv> map_rawF Node2A Node2B o unflat2_rawF (rev ul)"
abbreviation "immerse3F ul \<equiv> map_rawF Node3A Node3B o unflat3_rawF (rev ul)"
abbreviation "immerse1E ul \<equiv> map_rawE Node1A Node1B o unflat1_rawE (rev ul)"
abbreviation "immerse2E ul \<equiv> map_rawE Node2A Node2B o unflat2_rawE (rev ul)"
abbreviation "immerse3E ul \<equiv> map_rawE Node3A Node3B o unflat3_rawE (rev ul)"
  (* cumulative immerse: *)
primrec cimmerse_shape3A where 
  "cimmerse_shape3A [] sh = sh"
| "cimmerse_shape3A (u # ul) sh = cimmerse_shape3A ul
   ((case u of
     F1 \<Rightarrow> \<lambda>ul. map_shape3A Node1A Node1B o unflat_shape1A (rev ul)
   | F2 \<Rightarrow> \<lambda>ul. map_shape3A Node2A Node2B o unflat_shape2A (rev ul)
   | E3 \<Rightarrow> \<lambda>ul. map_shape3A Node3A Node3B o unflat_shape3A (rev ul)) ul sh)"
primrec cimmerse_shape3B where 
  "cimmerse_shape3B [] sh = sh"
| "cimmerse_shape3B (u # ul) sh = cimmerse_shape3B ul
   ((case u of
     F1 \<Rightarrow> \<lambda>ul. map_shape3B Node1A Node1B o unflat_shape1B (rev ul)
   | F2 \<Rightarrow> \<lambda>ul. map_shape3B Node2A Node2B o unflat_shape2B (rev ul)
   | E3 \<Rightarrow> \<lambda>ul. map_shape3B Node3A Node3B o unflat_shape3B (rev ul)) ul sh)"

primrec cimmerseF where 
  "cimmerseF [] r = r"
| "cimmerseF (u # ul) r = cimmerseF ul
   (case u of
     F1 \<Rightarrow> immerse1F ul r
   | F2 \<Rightarrow> immerse2F ul r
   | E3 \<Rightarrow> immerse3F ul r)"
  
primrec cimmerseE where 
  "cimmerseE [] r = r"
| "cimmerseE (u # ul) r = cimmerseE ul
   ((case u of
     F1 \<Rightarrow> immerse1E
   | F2 \<Rightarrow> immerse2E
   | E3 \<Rightarrow> immerse3E) ul r)"

lemma cimmerse_shape3A_inj[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp]:
  "\<forall>sh1 sh2. invar_shape3A (rev ul) sh1 \<longrightarrow> invar_shape3A (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3A ul sh1 = cimmerse_shape3A ul sh2 \<longrightarrow> sh1 = sh2"
  apply (rule list.induct[of "\<lambda>ul. \<forall>sh1 sh2. invar_shape3A (rev ul) sh1 \<longrightarrow> invar_shape3A (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3A ul sh1 = cimmerse_shape3A ul sh2 \<longrightarrow> sh1 = sh2" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerse_shape3A.simps(1) rev.simps(1) id_apply)
  subgoal premises prems for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
    apply (drule prems[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp, rotated -1]
        shape3A.inj_map_strong[rotated -1]
        arg_cong[of _ _ flat_shape1A]
        arg_cong[of _ _ flat_shape2A]
        arg_cong[of _ _ flat_shape3A] |
      simp only: cimmerse_shape3A.simps o_apply rev_Cons shape3A.inject shape3B.inject label.case
        invar_shape_map_closed invar_shape_unflat_shape flat_shape_unflat_shape)+
    done
  done

lemma cimmerse_shape3B_inj[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp]:
  "\<forall>sh1 sh2. invar_shape3B (rev ul) sh1 \<longrightarrow> invar_shape3B (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3B ul sh1 = cimmerse_shape3B ul sh2 \<longrightarrow> sh1 = sh2"
  apply (rule list.induct[of "\<lambda>ul. \<forall>sh1 sh2. invar_shape3B (rev ul) sh1 \<longrightarrow> invar_shape3B (rev ul) sh2 \<longrightarrow>
    cimmerse_shape3B ul sh1 = cimmerse_shape3B ul sh2 \<longrightarrow> sh1 = sh2" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerse_shape3B.simps(1) rev.simps(1) id_apply)
  subgoal premises prems for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
    apply (drule prems[THEN spec, THEN spec, THEN mp, THEN mp, THEN mp, rotated -1]
        shape3B.inj_map_strong[rotated -1]
        arg_cong[of _ _ flat_shape1B]
        arg_cong[of _ _ flat_shape2B]
        arg_cong[of _ _ flat_shape3B] |
      simp only: cimmerse_shape3B.simps o_apply rev_Cons shape3A.inject shape3B.inject label.case
        invar_shape_map_closed invar_shape_unflat_shape flat_shape_unflat_shape)+
    done
  done

lemma invar_cimmerseF[THEN spec, THEN mp]:
  "\<forall>r. invar_rawF (rev ul) r \<longrightarrow> invar_rawF [] (cimmerseF ul r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r. invar_rawF (rev ul) r \<longrightarrow> invar_rawF [] (cimmerseF ul r)" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerseF.simps(1) rev.simps(1) id_apply)
  subgoal for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
      apply (simp only: cimmerseF.simps(2) rev_Cons o_apply invar_map_closed invar_unflat
        label.case)+
    done
  done

lemma invar_cimmerseE[THEN spec, THEN mp]:
  "\<forall>r. invar_rawE (rev ul) r \<longrightarrow> invar_rawE [] (cimmerseE ul r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r. invar_rawE (rev ul) r \<longrightarrow> invar_rawE [] (cimmerseE ul r)" ul])
   apply (rule allI impI)+
   apply (simp only: cimmerseE.simps(1) rev.simps(1) id_apply)
  subgoal for u ul
    apply (rule label.exhaust[of u]; (rule allI impI)+)
      apply (simp only: cimmerseE.simps(2) rev_Cons o_apply invar_map_closed invar_unflat
        label.case)+
    done
  done

lemma un_Cons_cimmerseF[THEN spec]:
  "\<forall>r. map_GF id id (immerse1F []) (immerse2F []) (immerse3E []) (un_ConsF (cimmerseF ul r)) = 
   map_GF (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsF r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r.  map_GF id id (immerse1F []) (immerse2F []) (immerse3E ( [])) (un_ConsF (cimmerseF ul r)) = 
   map_GF (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsF r)" ul])
   apply (rule allI)
   apply (simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps snoc.simps(1) rev.simps(1) id_apply
     label.case
     cong: GF.map_cong)
  apply (rule allI)
  unfolding cimmerseF.simps
  apply (drule spec, erule trans)
  apply (simp only: split: rawF.split label.splits)+
  apply (rule allI conjI impI | hypsubst | simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps rev_snoc
    snoc.simps o_apply id_apply GF.map_comp unflat1_simps unflat2_simps unflat3_simps rawF.map
    label.case rawF.inject rawE.inject label.distinct
    cong: GF.map_cong)+
  done

lemma un_Cons_cimmerseE[THEN spec]:
  "\<forall>r. map_GE id id (immerse1F []) (immerse2F []) (immerse3E []) (un_ConsE (cimmerseE ul r)) = 
   map_GE (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsE r)"
  apply (rule list.induct[of "\<lambda>ul. \<forall>r.  map_GE id id (immerse1F []) (immerse2F []) (immerse3E []) (un_ConsE (cimmerseE ul r)) = 
   map_GE (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsE r)" ul])
   apply (rule allI)
   apply (simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps snoc.simps(1) rev.simps(1) id_apply
     label.case
     cong: GE.map_cong)
  apply (rule allI)
  apply (auto simp only: cimmerseF.simps cimmerseE.simps cimmerse_shape3A.simps cimmerse_shape3B.simps rev_snoc rev.simps
    snoc.simps o_apply id_apply id_o GE.map_comp unflat1_simps unflat2_simps unflat3_simps rawE.map
    label.case
    cong: GE.map_cong split: rawE.splits label.splits)
  done

    (*  THE ASSUMPTIONS: *)
consts QF :: "('a, 'b) T \<Rightarrow> ('a, 'b) T \<Rightarrow> bool"
consts QE :: "('a, 'b) S \<Rightarrow> ('a, 'b) S \<Rightarrow> bool"
axiomatization where 
  QF_param: "\<And>(R :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (S :: 'b \<Rightarrow> 'b' \<Rightarrow> bool). bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_T R S) (rel_fun (rel_T R S) (=)) QF QF" and
  QE_param: "\<And>(R :: 'a \<Rightarrow> 'a' \<Rightarrow> bool) (S :: 'b \<Rightarrow> 'b' \<Rightarrow> bool). bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_S R S) (rel_fun (rel_S R S) (=)) QE QE" and
  QF_coind: "QF \<le> BNF_Def.vimage2p un_T un_T (rel_GF (=) (=) QF QF QE)" and
  QE_coind: "QE \<le> BNF_Def.vimage2p un_S un_S (rel_GE (=) (=) QF QF QE)"

lemma QF_map_T_inj:
  assumes "inj f" "inj g"
  shows "QF (map_T f g t1) (map_T f g t2) \<longleftrightarrow> QF t1 t2"
  using assms QF_param[of "BNF_Def.Grp UNIV f" "BNF_Def.Grp UNIV g"] unfolding bi_unique_Grp 
  unfolding rel_fun_def T.rel_Grp  unfolding Grp_def by auto

lemma QE_map_T_inj:
  assumes "inj f" "inj g"
  shows "QE (map_S f g t1) (map_S f g t2) \<longleftrightarrow> QE t1 t2"
  using assms QE_param[of "BNF_Def.Grp UNIV f" "BNF_Def.Grp UNIV g"] unfolding bi_unique_Grp 
  unfolding rel_fun_def S.rel_Grp  unfolding Grp_def by auto
  
abbreviation QFrr where 
  "QFrr ul r1 r2 \<equiv> QF (Abs_T (cimmerseF ul r1)) (Abs_T (cimmerseF ul r2))"
abbreviation QErr where 
  "QErr ul r1 r2 \<equiv> QE (Abs_S (cimmerseE ul r1)) (Abs_S (cimmerseE ul r2))"

theorem QFr_un_Cons:
  "invar_rawF (rev ul) r1 \<Longrightarrow> invar_rawF (rev ul) r2 \<Longrightarrow> QFrr ul r1 r2 \<Longrightarrow>
    rel_GF (=) (=) (\<lambda>x y. QF (Abs_T x) (Abs_T y)) (\<lambda>x y. QF (Abs_T x) (Abs_T y)) (\<lambda>x y. QE (Abs_S x) (Abs_S y))
    (map_GF (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsF r1))
    (map_GF (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsF r2))"
  apply (unfold un_Cons_cimmerseF[symmetric] rev.simps(1))
  apply (drule vimage2pD[OF predicate2D[OF QF_coind]])
  apply (drule invar_cimmerseF)
  apply (drule invar_cimmerseF)
  apply (hypsubst_thin |
    drule iffD2[OF QF_map_T_inj[of Node1A Node1B], rotated -1] |
    drule iffD2[OF QF_map_T_inj[of Node2A Node2B], rotated -1] |
    drule iffD2[OF QE_map_T_inj[of Node3A Node3B], rotated -1] |
    drule bspec, assumption |
    erule GF.rel_mono_strong conjE exE |
    rule allI impI ballI |
    simp only:
      GF.rel_map un_T_def un_S_def Abs_T_inverse Abs_S_inverse map_T_def map_S_def invar_cimmerseF
      mem_Collect_eq o_apply id_apply snoc.simps inj_on_def
      invar_unflat rawF.inject shape3A.inject shape3B.inject shape3A.case shape3B.case
      invar_rawF_simps GF.pred_set invar_shape3A_depth_iff invar_shape3B_depth_iff
      split: rawF.splits)+
  done

theorem QEr_un_Cons:
  "invar_rawE (rev ul) r1 \<Longrightarrow> invar_rawE (rev ul) r2 \<Longrightarrow> QErr ul r1 r2 \<Longrightarrow>
    rel_GE (=) (=) (\<lambda>x y. QF (Abs_T x) (Abs_T y)) (\<lambda>x y. QF (Abs_T x) (Abs_T y)) (\<lambda>x y. QE (Abs_S x) (Abs_S y))
    (map_GE (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsE r1))
    (map_GE (cimmerse_shape3A ul) (cimmerse_shape3B ul) (cimmerseF (snoc F1 ul)) (cimmerseF (snoc F2 ul)) (cimmerseE (snoc E3 ul)) (un_ConsE r2))"
  apply (unfold un_Cons_cimmerseE[symmetric] rev.simps(1))
  apply (drule vimage2pD[OF predicate2D[OF QE_coind]])
  apply (drule invar_cimmerseE)
  apply (drule invar_cimmerseE)
  apply (hypsubst_thin |
    drule iffD2[OF QF_map_T_inj[of Node1A Node1B], rotated -1] |
    drule iffD2[OF QF_map_T_inj[of Node2A Node2B], rotated -1] |
    drule iffD2[OF QE_map_T_inj[of Node3A Node3B], rotated -1] |
    drule bspec, assumption |
    erule GE.rel_mono_strong conjE exE |
    rule allI impI ballI |
    simp only:
      GE.rel_map un_T_def un_S_def Abs_T_inverse Abs_S_inverse map_T_def map_S_def invar_cimmerseE
      mem_Collect_eq o_apply id_apply snoc.simps inj_on_def
      invar_unflat rawE.inject shape3A.inject shape3B.inject shape3A.case shape3B.case
      invar_rawE_simps GE.pred_set invar_shape3A_depth_iff invar_shape3B_depth_iff
      split: rawE.splits)+
  done

theorem Qrr:
  fixes r1 r2 :: "(('a, 'b) shape3A, ('a, 'b) shape3B) rawF"
    and s1 s2 :: "(('a, 'b) shape3A, ('a, 'b) shape3B) rawE"
  shows "((\<exists>u0. invar_rawF (rev u0) r1 \<and> invar_rawF (rev u0) r2 \<and> QFrr u0 r1 r2) \<longrightarrow> r1 = r2) \<and>
         ((\<exists>u0. invar_rawE (rev u0) s1 \<and> invar_rawE (rev u0) s2 \<and> QErr u0 s1 s2) \<longrightarrow> s1 = s2)"
  apply (rule rawF_rawE.coinduct[of "\<lambda>r1 r2. \<exists>u0. invar_rawF (rev u0) r1 \<and> invar_rawF (rev u0) r2 &  QFrr u0 r1 r2" "\<lambda>s1 s2. \<exists>u0. invar_rawE (rev u0) s1 \<and> invar_rawE (rev u0) s2 \<and> QErr u0 s1 s2"], unfold UnConsF_def UnConsE_def)
  apply (erule exE conjE)+
  apply (drule QFr_un_Cons[rotated -1], assumption, assumption)
  apply ((hypsubst_thin |
      erule GF.rel_mono_strong cimmerse_shape3A_inj[rotated -1] cimmerse_shape3B_inj[rotated -1] |
      rule allI impI |
      drule bspec, assumption |
      simp only: GF.rel_map id_apply invar_rawF_simps GF.pred_set rawF.inject rev_snoc split: rawF.splits |
      rule exI, (rule conjI[rotated])+, assumption)+) []

  apply (erule exE conjE)+
  apply (drule QEr_un_Cons[rotated -1], assumption, assumption)
  apply ((hypsubst_thin |
      erule GE.rel_mono_strong cimmerse_shape3A_inj[rotated -1] cimmerse_shape3B_inj[rotated -1] conjE |
      rule allI impI |
      drule bspec, assumption |
      simp only: GE.rel_map id_apply invar_rawE_simps GE.pred_set rawE.inject rev_snoc split: rawE.splits |
      rule exI, (rule conjI[rotated])+, assumption)+) []
  done

theorem T_coind: "QF \<le> (=)"
  apply (rule predicate2I)
  subgoal for x y
    apply (rule Abs_T_cases[of x])
    apply (rule Abs_T_cases[of y])
    apply hypsubst_thin
    apply (rule iffD2[OF Abs_T_inject])
      apply assumption
     apply assumption
    apply (rule rawF.inj_map_strong[of _ _ LeafA LeafA LeafB LeafB];
      (simp only: shape3A.inject(1) shape3B.inject(1))?)
    apply (rule conjunct1[OF Qrr, THEN mp])
    apply (drule iffD2[OF QF_map_T_inj[of LeafA LeafB], rotated -1];
      (simp only: inj_on_def shape3A.inject shape3B.inject simp_thms ball_simps)?)
    apply (rule exI[of _ "[]"])
     apply (simp only: invar_map_closed cimmerseF.simps
        map_T_def Abs_T_inverse o_apply mem_Collect_eq rev.simps(1))
    done
  done

theorem S_coind: "QE \<le> (=)"
  apply (rule predicate2I)
  subgoal for x y
    apply (rule Abs_S_cases[of x])
    apply (rule Abs_S_cases[of y])
    apply hypsubst_thin
    apply (rule iffD2[OF Abs_S_inject])
      apply assumption
     apply assumption
    apply (rule rawE.inj_map_strong[of _ _ LeafA LeafA LeafB LeafB];
      (simp only: shape3A.inject(1) shape3B.inject(1))?)
    apply (rule conjunct2[OF Qrr, THEN mp])
    apply (drule iffD2[OF QE_map_T_inj[of LeafA LeafB], rotated -1];
      (simp only: inj_on_def shape3A.inject shape3B.inject simp_thms ball_simps)?)
    apply (rule exI[of _ "[]"])
     apply (simp only: invar_map_closed cimmerseE.simps
        map_S_def Abs_S_inverse o_apply mem_Collect_eq rev.simps(1))
    done
  done

end