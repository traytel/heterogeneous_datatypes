theory Wit_Example
imports Main
begin

ML \<open>
local
open BNF_Def
open Ctr_Sugar_Util
in
fun mk_ctor_witss decode f m Dss As bnfs ctors lthy =
  let
    val ((ys, ys'), _) = lthy |> mk_Frees' "z" (take m As)
    val witss = map2 (fn Ds => fn bnf => mk_wits_of_bnf
      (replicate (nwits_of_bnf bnf) Ds)
      (replicate (nwits_of_bnf bnf) As) bnf) Dss bnfs;
    fun close_wit (I, wit) = fold_rev Term.absfree (map (nth ys') I) wit;
    fun wit_apply (arg_I, arg_wit) (fun_I, fun_wit) =
      (union ((=)) arg_I fun_I, fun_wit $ arg_wit);

    fun gen_arg support js i =
      if i < m then [([i], f js ys i)]
      else
        let
          val j = i - m;
          val j' = decode j;
        in
          maps (mk_wit support (nth ctors j') (j :: js) j') (nth support j')
        end
    and mk_wit support ctor js i (I, wit) =
      let
        val args = map (gen_arg (nth_map i (remove ((=)) (I, wit)) support) js) I;
      in
        (args, [([], wit)])
        |-> fold (map_product wit_apply)
        |> map (apsnd (fn t => ctor $ t))
        |> minimize_wits
      end;
  in
    @{map 3} (fn ctor => fn i => map close_wit o minimize_wits o maps (mk_wit witss ctor [] i))
      ctors (0 upto length ctors - 1) witss
  end;
end
\<close>

local_setup \<open> fn lthy =>
let
  open BNF_Def
  open BNF_Util
  open BNF_Tactics
  open BNF_Comp
  open BNF_FP_Util
  open BNF_FP_Def_Sugar
  open BNF_LFP
  open BNF_LFP_Util
  open BNF_LFP_Tactics
  open BNF_LFP_Rec_Sugar
  open Ctr_Sugar
  open BNF_Tactics

  (* ----- Debug data generation ----- *)
  fun composition_bnf lthy (inline_opt, (b, str)) =
    let
      val T = Syntax.read_typ lthy str;
      fun qualify b' = if length (Binding.prefix_of b') <= 2 then b
        else Binding.qualify true (Binding.name_of b) b';
      val inline = (case inline_opt of true => BNF_Def.Dont_Inline | _ => BNF_Def.Do_Inline);
      val ((bnf, (Ds, _)), ((_, unfolds), lthy')) =
        BNF_Comp.bnf_of_typ true inline qualify (distinct (=) o flat) [] [] T
          ((BNF_Comp.empty_comp_cache, BNF_Comp.empty_unfolds), lthy)
    in
      seal_bnf I unfolds (Binding.qualify false "seal" b) true Ds Ds bnf lthy'
      |> apfst #1
    end;

  val (F1, lthy) = composition_bnf lthy (true, (Binding.name "F1A", "'a + 'a"));
  val (F2, lthy) = composition_bnf lthy (true, (Binding.name "F2A", "'a list"));
  val (F3, lthy) = composition_bnf lthy (true, (Binding.name "F1B", "'b * 'b"));
  val (F4, lthy) = composition_bnf lthy (true, (Binding.name "F2B", "'b list"));
  val (E1, lthy) = composition_bnf lthy (true, (Binding.name "E3A", "'a * 'a"));
  val (E2, lthy) = composition_bnf lthy (true, (Binding.name "E3B", "'b"));

  val (G1, lthy) = composition_bnf lthy (true, (Binding.name "G1", "'b + 'x * 'a * 'z"));
  val (G2, lthy) = composition_bnf lthy (true, (Binding.name "G2", "'a * 'b + 'y"));
  
  val BNFs = [F1, F2, F3, F4, E1, E2];
  val ([a, b, x, y, z], _) = mk_TFrees 5 lthy;

  val Ass = [[a], [a], [b], [b], [a], [b]];
  val Xs = [a, b];
  val Ds = [];
  fun norm_qualify i = Binding.qualify true (nth ["a", "b", "c", "d", "e", "f"] (Int.max (0, i - 1)))
        #> Binding.concealed;
  val (_, (BNFs, (_,lthy))) = normalize_bnfs norm_qualify Ass Ds (K Xs) BNFs
            ((empty_comp_cache, empty_unfolds), lthy) ;
  val Fss = [[nth BNFs 0, nth BNFs 1], [nth BNFs 2, nth BNFs 3]];
  val Ess = [[nth BNFs 4], [nth BNFs 5]];

  val Ass = [[b, x, a, z], [a, b, y]];
  val Xs = [a, b, x, y, z];

  val (_, ([G1, G2], (_,lthy))) = normalize_bnfs norm_qualify Ass Ds (K Xs) [G1, G2]
            ((empty_comp_cache, empty_unfolds), lthy);

  val _ =
    mk_ctor_witss (fn i => if i < 2 then 0 else 1) (fn xs => (@{print} xs; nth)) 2 [[], []]
      [@{typ 'a}, @{typ 'b}, @{typ "'a list"}, @{typ "'a list"}, @{typ "'a option"}]
      [G1, G2]
      [@{term "c :: 'a \<Rightarrow> 'b"},
       @{term "d :: 'a \<Rightarrow> 'b"}] @{context}
    |> map (map (warning o Syntax.string_of_term @{context}))
in

lthy

end
\<close>

end