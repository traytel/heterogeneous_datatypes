theory Het_Data2
imports Main
begin

  declare Basic_BNFs.setl.simps[termination_simp]
  declare Basic_BNFs.setr.simps[termination_simp]

  lemma elim_refl_vars[simp]: 
    "(\<And>a. x=a \<Longrightarrow> PROP (P a)) \<equiv> (PROP (P x))"
    "(\<And>a. a=x \<Longrightarrow> PROP (P a)) \<equiv> (PROP (P x))"
    apply rule
    apply (drule meta_spec[where x=x])
    apply simp_all[2]
    apply rule
    apply (drule meta_spec[where x=x])
    apply simp_all[2]
    done

  (*
    S :: 'a T
    D :: 'a \<Rightarrow> ('a+bool) T \<Rightarrow> 'a T
  *)

  (* Idea:
    'a base: Every data that can be constructed by repeated
      application of the recursion functors, in this case only one,
      namely Sum: 'a \<mapsto> 'a+bool
      Moreover, we need the  occurrence of 'a, which is "Base"

    type:   
      Types that can  be constructed by the recursion functors, represented as terms.
  *)




  datatype 'a base = Base 'a | Sum "'a base + bool"
  datatype type = TBase | TSum type

  (* Relates term-represented types with 'a-base terms *)
  inductive isBT :: "type \<Rightarrow> 'a base \<Rightarrow> bool" where
    "isBT TBase (Base _)"
  | "isBT a x \<Longrightarrow> isBT (TSum a) (Sum (Inl x))"
  | "isBT (TSum a) (Sum (Inr x))"

  fun flat_sum :: "('a+bool) base \<Rightarrow> 'a base" where
    "flat_sum (Base (Inl a)) = Sum (Inl (Base a))"
  | "flat_sum (Base (Inr b)) = Sum (Inr b)"
  | "flat_sum (Sum x) = Sum (map_sum flat_sum id x)"

  fun base_induct2_scheme :: "'a base \<Rightarrow> 'a base \<Rightarrow> unit" where
    "base_induct2_scheme (Base _) (Base _) = ()"
  | "base_induct2_scheme (Sum (Inl x)) (Sum (Inl x')) = (base_induct2_scheme x x')"  
  | "base_induct2_scheme (Sum (Inr x)) (Sum (Inr x')) = ()"  
  | "base_induct2_scheme _ _ = ()"
    
  lemma flat_sum_inject:
    assumes "flat_sum t1 = flat_sum t2" "isBT t t1" "isBT t t2"
    shows "t1 = t2"
    using assms
    apply (induct t1 t2 arbitrary: t rule: base_induct2_scheme.induct)
    apply simp_all
    apply (auto 4 4 elim: isBT.cases)
    apply (rename_tac x y t)
    apply (case_tac x; case_tac y; simp)
    done

  lemma flat_sum_BT:
    assumes "isBT a x"
    shows "isBT (TSum a) (flat_sum x)"
    using assms
    apply induction
    apply (rename_tac x; case_tac x; auto intro: isBT.intros)
    apply (auto intro: isBT.intros)
    done

  lemma isBT_unflat:
    assumes "isBT (TSum a) x"
    shows "\<exists>x'. x = flat_sum x' \<and> isBT a x'"
    using assms
    apply (induction "TSum a" x arbitrary: a rule: isBT.induct)
    apply (erule isBT.cases; simp)

    apply (rename_tac a x u)
    apply (rule_tac x="Base (Inl u)" in exI)
    apply (simp add: isBT.intros)

    apply (clarsimp)
    apply (rule_tac x="Sum (Inl x')" in exI)
    apply (simp add: isBT.intros)

    apply (clarsimp)
    apply (rule_tac x="Sum (Inl x')" in exI)
    apply (simp add: isBT.intros)
    
    apply (case_tac a)
    apply (rule_tac x="Base (Inr x)" in exI)
    apply (simp add: isBT.intros)

    apply (rule_tac x="Sum (Inr x)" in exI)
    apply (simp add: isBT.intros)
    done  


  (* Terms that can be constructed from the constructors *)
  datatype 'a univ = rS | rD 'a  "'a univ"

  (* The type-invariant on these terms *)
  inductive isT :: "type \<Rightarrow> 'a base univ \<Rightarrow> bool" where
    "isT a rS"
  | "isBT a x1 \<Longrightarrow> isT (TSum a) x2 \<Longrightarrow> isT a (rD x1 x2)"  

  fun flat_univ :: "('a base \<Rightarrow> 'b base) \<Rightarrow> 'a base univ \<Rightarrow> 'b base univ" where
    "flat_univ _ rS = rS"
  | "flat_univ f (rD x y) = rD (f x) (flat_univ f y)"  

  fun univ_induct2_scheme :: "'a univ \<Rightarrow> 'a univ \<Rightarrow> unit" where
    "univ_induct2_scheme rS rS = ()"
  | "univ_induct2_scheme (rD x y) (rD x' y') = (univ_induct2_scheme y y')"  
  | "univ_induct2_scheme _ _ = ()"

  lemma flat_univ_sum_inject:
    assumes "flat_univ flat_sum t1 = flat_univ flat_sum t2" "isT t t1" "isT t t2"
    shows "t1 = t2"
    using assms
    apply (induct t1 t2 arbitrary: t rule: univ_induct2_scheme.induct)
    apply (auto simp: flat_sum_inject elim: isT.cases)
    apply (auto dest: flat_sum_inject elim: isT.cases)
    done

  lemma flat_sum_T:
    assumes "isT a x"
    shows "isT (TSum a) (flat_univ flat_sum x)"  
    using assms
    apply (induction)
    apply (auto intro: isT.intros flat_sum_BT)
    done    

  lemma isT_unflat:
    assumes "isT (TSum a) x"
    shows "\<exists>x'. x = flat_univ flat_sum x' \<and> isT a x'"  
    using assms
    apply (induction "TSum a" x arbitrary: a rule: isT.induct)
    apply (rule exI[where x="rS"])
    apply (simp add: isT.intros)
    apply (frule isBT_unflat)
    apply clarsimp
    apply (rename_tac a x2 x1)
    apply (rule_tac x="(rD x1 x2)" in exI)
    apply (simp add: isT.intros)
    done




  typedef 'a T = "{ x . isT TBase (x::'a base univ) }"
    by (auto intro: isT.intros)

  definition S :: "'a T" where "S \<equiv> Abs_T rS"
  definition D :: "'a \<Rightarrow> ('a+bool) T \<Rightarrow> 'a T" where 
    "D x1 x2 \<equiv> Abs_T (rD (Base x1) (flat_univ flat_sum (Rep_T x2)))"

  lemma D_inject: "D x1 x2 = D x1' x2' \<Longrightarrow> x1=x1' \<and> x2=x2'"  
    unfolding D_def
    apply (subst (asm) Abs_T_inject)
    prefer 3
    apply clarsimp
    apply (drule flat_univ_sum_inject)
    using Rep_T apply simp apply assumption
    using Rep_T apply simp apply assumption
    apply (simp add: Rep_T_inject)
    apply simp_all

    apply (rule isT.intros)
    apply (simp add: isBT.intros)
    apply (rule flat_sum_T)
    using Rep_T apply (simp) apply assumption
    
    apply (rule isT.intros)
    apply (simp add: isBT.intros)
    apply (rule flat_sum_T)
    using Rep_T apply (simp) apply assumption
    done

  lemma D_ne: "D x1 x2 \<noteq> S"  
    unfolding D_def S_def
    apply safe
    apply (subst (asm) Abs_T_inject)
    apply simp_all
    
    apply (rule isT.intros)
    apply (simp add: isBT.intros)
    apply (rule flat_sum_T)
    using Rep_T apply (simp) apply assumption

    apply (rule isT.intros)
    done

  lemma D_exhaust: "(x = S) \<or> (\<exists>x1 x2. x=D x1 x2)"  
    apply (cases x; simp)
    apply (simp_all add: S_def D_def)
    apply (subst Abs_T_inject)
    apply simp
    apply (simp add: isT.intros)
    apply (subst Abs_T_inject)
    apply simp
    apply simp
    
    apply (rule isT.intros)
    apply (simp add: isBT.intros)
    apply (rule flat_sum_T)
    using Rep_T apply (simp) apply assumption

    apply (erule isT.cases)
    apply (auto elim: isBT.cases)
    apply (drule isT_unflat)
    apply auto
    apply (rule_tac x="Abs_T x'" in exI)
    apply (simp add: Abs_T_inverse)
    done
    

  (*
    Primitive recursion equations:
      (f::'a t \<Rightarrow> 'r) S = F1
      (f::'a t \<Rightarrow> 'r) (D a t) = F2 a ((f::('a+bool) t \<Rightarrow> 'r) t)
 
    for example, map :: 'a T \<Rightarrow> ('a \<Rightarrow> 'b) \<Rightarrow> 'b T
      map S = \<lambda>f. S
      map (D a t) = \<lambda>f. D (f a) (map t (map_sum f))

    set:
      set S = {}
      set (D a t) = insert a (\<Union>(setl ` set t))

    snoc :: "'a T \<Rightarrow> 'a \<Rightarrow> 'a T"
      snoc S a = D a S
      snoc (D b t) a = D b (snoc t (Inl a))
  
    sum_aux :: "'a T \<Rightarrow> ('a\<Rightarrow>int) \<Rightarrow> int"  
      sum_aux S f = 0
      sum_aux (D x t) f = f x + sum_aux t (case_sum f (\<lambda>_. 0))
    sum :: "int T \<Rightarrow> int"
      sum t = sum_aux t id
    (* \<rightarrow> See generalized folds! [?,Peterson:?, 1999, FAoC] *)

    Gives rise to recursion equation on raw datatype:  

    ? How to transition from 'a to 'a Base ?

    map :: 'a rT \<Rightarrow> ('a Base \<Rightarrow> 'b) \<Rightarrow> 'b T  
      map rS f = S
      map (rD a t) f = D (f a) (map t ( \<down>(mapsum \<up>f) ))




  *)

  consts map :: "'a T \<Rightarrow> ('a\<Rightarrow>'b) \<Rightarrow> 'b T" 
  axiomatization where
        map_S: "map S f = S"
    and map_D: "map (D x t) f = D (f x) (map t (map_sum f id))"

  primrec raw_map :: "'a base univ \<Rightarrow> ('a base \<Rightarrow> 'b) \<Rightarrow> 'b T" where
    "raw_map rS f = S"
  | "raw_map (rD x t) f = D (f x) (undefined (raw_map t (undefined (map_sum (undefined f) id))))"  
  (*                               ^^ Why that?    *)

  primrec m :: "('a \<Rightarrow> 'a) \<Rightarrow> 'a list \<Rightarrow> 'a list" where
    "m f [] = []"
  | "m f (x#xs) = f x # m (f o f) xs"








end
