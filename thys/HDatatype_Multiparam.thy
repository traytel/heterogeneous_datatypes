theory HDatatype_Multiparam
imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

primrec snoc where
  "snoc x [] = [x]"
| "snoc x (y # xs) = y # snoc x xs"

lemma snoc_neq_Nil: "snoc x xs \<noteq> []"
  by (cases xs; simp)

section \<open>Input\<close>

declare [[bnf_internals, typedef_overloaded]]

(*
datatype ('a, 'b) l = N 'b | C 'a "('a * 'a, 'a + 'b) l"

('a, 'b, 'x) G = 'b + 'a * 'x
('a, 'b) FA = 'a * 'a
('a, 'b) FB = 'a + 'b

specs = [(G, [[FA, FB]])]
*)

bnf_axiomatization ('a, 'b) FA
bnf_axiomatization ('a, 'b) FB

bnf_axiomatization ('a, 'b, 'x) G [wits: "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b, 'x) G"]


section \<open>Raw Type\<close>

datatype label = F
type_synonym depth = "label list"
datatype ('a, 'b) shapeA = LeafA 'a | NodeA "(('a, 'b) shapeA, ('a, 'b) shapeB) FA"
     and ('a, 'b) shapeB = LeafB 'b | NodeB "(('a, 'b) shapeA, ('a, 'b) shapeB) FB"
datatype ('a, 'b) raw = Cons "(('a, 'b) shapeA, ('a, 'b) shapeB, ('a, 'b) raw) G"

abbreviation "un_LeafA u \<equiv> case u of LeafA x \<Rightarrow> x"
abbreviation "un_LeafB u \<equiv> case u of LeafB x \<Rightarrow> x"
abbreviation "un_NodeA u \<equiv> case u of NodeA x \<Rightarrow> x"
abbreviation "un_NodeB u \<equiv> case u of NodeB x \<Rightarrow> x"
abbreviation "un_Cons t \<equiv> case t of Cons x \<Rightarrow> x"


section \<open>Invariant\<close>

primrec invar_shapeA :: "depth \<Rightarrow> ('a, 'b) shapeA \<Rightarrow> bool" and invar_shapeB :: "depth \<Rightarrow> ('a, 'b) shapeB \<Rightarrow> bool" where
  "invar_shapeA u0 (LeafA u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shapeA u0 (NodeA fa) = (case u0 of F # u0 \<Rightarrow> pred_FA (invar_shapeA u0) (invar_shapeB u0) fa | _ \<Rightarrow> False)"
| "invar_shapeB u0 (LeafB u) = (case u0 of [] \<Rightarrow> True | _ \<Rightarrow> False)"
| "invar_shapeB u0 (NodeB fb) = (case u0 of F # u0 \<Rightarrow> pred_FB (invar_shapeA u0) (invar_shapeB u0) fb | _ \<Rightarrow> False)"

primrec invar :: "depth \<Rightarrow> ('a, 'b) raw \<Rightarrow> bool" where
  "invar u0 (Cons g) = pred_G (invar_shapeA u0) (invar_shapeB u0) (invar (F # u0)) g"


section \<open>The Type\<close>

definition "wit x y = Cons (wit_G (LeafA x) (LeafB y))"

lemma invar_wit: "invar [] (wit x y)"
  by (auto simp only: wit_def invar.simps invar_shapeA.simps invar_shapeB.simps G.pred_map o_def id_apply pred_G_def
    G.set_map list.case dest: G.wit)

typedef ('a ,'b) T = "{t :: ('a ,'b) raw. invar [] t}"
  by (rule exI[of _ "wit undefined undefined"]) (auto simp only: invar_wit)


section \<open>Flat and Unflat\<close>

primrec (transfer)
  flat_shapeA :: "(('a, 'b) FA, ('a, 'b) FB) shapeA \<Rightarrow> ('a, 'b) shapeA" and
  flat_shapeB :: "(('a, 'b) FA, ('a, 'b) FB) shapeB \<Rightarrow> ('a, 'b) shapeB" where
  "flat_shapeA (LeafA f1) = NodeA (map_FA LeafA LeafB f1)"
| "flat_shapeA (NodeA f1) = NodeA (map_FA flat_shapeA flat_shapeB f1)"
| "flat_shapeB (LeafB f2) = NodeB (map_FB LeafA LeafB f2)"
| "flat_shapeB (NodeB f2) = NodeB (map_FB flat_shapeA flat_shapeB f2)"

primrec (nonexhaustive)
   unflat_shapeA :: "depth \<Rightarrow> ('a, 'b) shapeA \<Rightarrow> (('a, 'b) FA, ('a, 'b) FB) shapeA" and
   unflat_shapeB :: "depth \<Rightarrow> ('a, 'b) shapeB \<Rightarrow> (('a, 'b) FA, ('a, 'b) FB) shapeB" where
  "unflat_shapeA u0 (NodeA f1) =
      (case u0 of
        [] \<Rightarrow> LeafA (map_FA un_LeafA un_LeafB f1)
      | F # u0 \<Rightarrow> NodeA (map_FA (unflat_shapeA u0) (unflat_shapeB u0) f1))"
| "unflat_shapeB u0 (NodeB f2) = 
      (case u0 of
        [] \<Rightarrow> LeafB (map_FB un_LeafA un_LeafB f2)
      | F # u0 \<Rightarrow> NodeB (map_FB (unflat_shapeA u0) (unflat_shapeB u0) f2))"

primrec (transfer) flat :: "(('a, 'b) FA, ('a, 'b) FB) raw \<Rightarrow> ('a, 'b) raw" where
  "flat (Cons g) = Cons (map_G flat_shapeA flat_shapeB flat g)"

primrec unflat :: "depth \<Rightarrow> ('a, 'b) raw \<Rightarrow> (('a, 'b) FA, ('a, 'b) FB) raw" where
  "unflat u0 (Cons g) = Cons (map_G (unflat_shapeA u0) (unflat_shapeB u0) (unflat (F # u0)) g)"


section \<open>Constructor and Selector\<close>

definition T :: "('a, 'b, (('a, 'b) FA, ('a, 'b) FB) T) G \<Rightarrow> ('a, 'b) T" where
  "T g = Abs_T (Cons (map_G LeafA LeafB (flat o Rep_T) g))"

definition un_T :: "('a, 'b) T \<Rightarrow> ('a, 'b, (('a, 'b) FA, ('a, 'b) FB) T) G" where
  "un_T t = map_G un_LeafA un_LeafB (Abs_T o unflat []) (un_Cons (Rep_T t))"


section \<open>BNF Instance\<close>

lemma invarU_map_closed_raw:
  "(\<forall>u0. invar_shapeA u0 (map_shapeA f g ua) \<longleftrightarrow> invar_shapeA u0 ua) \<and>
   (\<forall>u0. invar_shapeB u0 (map_shapeB f g ub) \<longleftrightarrow> invar_shapeB u0 ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 (map_shapeA f g ua) \<longleftrightarrow> invar_shapeA u0 ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 (map_shapeB f g ub) \<longleftrightarrow> invar_shapeB u0 ub" ua ub])
  apply (auto simp only:
      shapeA.map shapeB.map invar_shapeA.simps invar_shapeB.simps
      FA.pred_map FB.pred_map
      o_apply
    elim!: FA.pred_mono_strong FB.pred_mono_strong
    split: list.split label.splits)
  done

lemmas invarU_map_closed =
  spec[OF conjunct1[OF invarU_map_closed_raw]]
  spec[OF conjunct2[OF invarU_map_closed_raw]]

lemma invar_map_closed_raw:
  "\<forall>u0. invar u0 (map_raw f g t) \<longleftrightarrow> invar u0 t"
  apply (induct t)
  apply (auto simp only:
      raw.map invar.simps id_apply o_apply
      G.pred_map invarU_map_closed
    elim!: G.pred_mono_strong)
  done

lemmas invar_map_closed = spec[OF invar_map_closed_raw]

lift_bnf ('a, 'b) T
  apply (auto simp only:
      invar_map_closed)
  done


section \<open>Lemmas about Flat, Unflat, Invar\<close>

lemma invar_shapeA_depth_iff:
  "invar_shapeA [] x = (\<exists>a. x = LeafA a)"
  "invar_shapeA (F # u0) x = (\<exists>y. x = NodeA y \<and> pred_FA (invar_shapeA u0) (invar_shapeB u0) y)"
  by (cases x; simp add: FA.pred_map)+

lemma invar_shapeB_depth_iff:
  "invar_shapeB [] x = (\<exists>b. x = LeafB b)"
  "invar_shapeB (F # u0) x = (\<exists>y. x = NodeB y \<and> pred_FB (invar_shapeA u0) (invar_shapeB u0) y)"
  by (cases x; simp add: FB.pred_map)+

lemma flatU_unflatU_raw:
  fixes ua :: "('a, 'b) shapeA" and ub :: "('a, 'b) shapeB"
  shows
  "(\<forall>u0. invar_shapeA (snoc F u0) ua \<longrightarrow> flat_shapeA (unflat_shapeA u0 ua) = ua) \<and>
   (\<forall>u0. invar_shapeB (snoc F u0) ub \<longrightarrow> flat_shapeB (unflat_shapeB u0 ub) = ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA (snoc F u0) ua \<longrightarrow> flat_shapeA (unflat_shapeA u0 ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB (snoc F u0) ub \<longrightarrow> flat_shapeB (unflat_shapeB u0 ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shapeA.simps unflat_shapeB.simps flat_shapeA.simps flat_shapeB.simps
      invar_shapeA.simps invar_shapeB.simps FA.pred_map FB.pred_map FA.map_comp FB.map_comp
      shapeA.case shapeB.case invar_shapeA_depth_iff invar_shapeB_depth_iff snoc.simps snoc_neq_Nil
      id_apply o_apply
    intro!: trans[OF FA.map_cong_pred FA.map_ident] trans[OF FB.map_cong_pred FB.map_ident]
    elim!: FA.pred_mono_strong FB.pred_mono_strong
    split: list.split label.splits)
  done

lemmas flatU_unflatU =
  mp[OF spec[OF conjunct1[OF flatU_unflatU_raw]]]
  mp[OF spec[OF conjunct2[OF flatU_unflatU_raw]]]

lemma unflatU_flatU_raw:
  fixes ua :: "(('a, 'b) FA, ('a, 'b) FB) shapeA" and ub :: "(('a, 'b) FA, ('a, 'b) FB) shapeB"
  shows
  "(\<forall>u0. invar_shapeA u0 ua \<longrightarrow> unflat_shapeA u0 (flat_shapeA ua) = ua) \<and>
   (\<forall>u0. invar_shapeB u0 ub \<longrightarrow> unflat_shapeB u0 (flat_shapeB ub) = ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 ua \<longrightarrow> unflat_shapeA u0 (flat_shapeA ua) = ua"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 ub \<longrightarrow> unflat_shapeB u0 (flat_shapeB ub) = ub" ua ub])
  apply (auto simp only:
      unflat_shapeA.simps unflat_shapeB.simps flat_shapeA.simps flat_shapeB.simps invar_shapeA.simps invar_shapeB.simps
      FA.pred_map FB.pred_map FA.map_comp FB.map_comp FA.pred_True FB.pred_True
      shapeA.case shapeB.case
      id_apply o_apply refl
    intro!: trans[OF FA.map_cong_pred FA.map_ident] trans[OF FB.map_cong_pred FB.map_ident]
    elim!: FA.pred_mono_strong FB.pred_mono_strong
    split: list.split label.splits)
  done

lemmas unflatU_flatU =
  mp[OF spec[OF conjunct1[OF unflatU_flatU_raw]]]
  mp[OF spec[OF conjunct2[OF unflatU_flatU_raw]]]

lemma invarU_flatU_raw:
  fixes ua :: "(('a, 'b) FA, ('a, 'b) FB) shapeA" and ub :: "(('a, 'b) FA, ('a, 'b) FB) shapeB"
  shows
  "(\<forall>u0. invar_shapeA u0 ua \<longrightarrow> invar_shapeA (snoc F u0) (flat_shapeA ua)) \<and>
   (\<forall>u0. invar_shapeB u0 ub \<longrightarrow> invar_shapeB (snoc F u0) (flat_shapeB ub))"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA u0 ua \<longrightarrow> invar_shapeA (snoc F u0) (flat_shapeA ua)"
    "\<lambda>ub. \<forall>u0. invar_shapeB u0 ub \<longrightarrow> invar_shapeB (snoc F u0) (flat_shapeB ub)" ua ub])
  apply (auto simp only:
      flat_shapeA.simps flat_shapeB.simps invar_shapeA.simps invar_shapeB.simps snoc.simps
      FA.pred_map FB.pred_map FA.pred_True FB.pred_True
      id_apply o_apply
    elim!: FA.pred_mono_strong FB.pred_mono_strong
    split: list.split label.splits
    cong: FA.pred_cong FB.pred_cong)
  done

lemmas invarU_flatU =
  mp[OF spec[OF conjunct1[OF invarU_flatU_raw]]]
  mp[OF spec[OF conjunct2[OF invarU_flatU_raw]]]

lemma invar_flat_raw: "\<forall>u0. invar u0 t \<longrightarrow> invar (snoc F u0) (flat t)"
  apply (induct t)
  apply (auto simp only:
      flat.simps invar.simps snoc.simps[symmetric] invarU_flatU id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemmas invar_flat = mp[OF spec[OF invar_flat_raw]]

lemma invarU_unflatU_raw:
  fixes ua :: "('a, 'b) shapeA" and ub :: "('a, 'b) shapeB"
  shows
  "(\<forall>u0. invar_shapeA (snoc F u0) ua \<longrightarrow> invar_shapeA u0 (unflat_shapeA u0 ua)) \<and>
   (\<forall>u0. invar_shapeB (snoc F u0) ub \<longrightarrow> invar_shapeB u0 (unflat_shapeB u0 ub))"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0. invar_shapeA (snoc F u0) ua \<longrightarrow> invar_shapeA u0 (unflat_shapeA u0 ua)"
    "\<lambda>ub. \<forall>u0. invar_shapeB (snoc F u0) ub \<longrightarrow> invar_shapeB u0 (unflat_shapeB u0 ub)" ua ub])
  apply (auto simp only:
      unflat_shapeA.simps unflat_shapeB.simps invar_shapeA.simps invar_shapeB.simps snoc.simps snoc_neq_Nil
      FA.pred_map FB.pred_map
      id_apply o_apply refl
    elim!: FA.pred_mono_strong FB.pred_mono_strong
    split: list.split label.splits)
  done

lemmas invarU_unflatU =
  mp[OF spec[OF conjunct1[OF invarU_unflatU_raw]]]
  mp[OF spec[OF conjunct2[OF invarU_unflatU_raw]]]

lemma invar_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> invar u0 (unflat u0 t)"
  apply (induct t)
  apply (auto simp only:
      unflat.simps invar.simps snoc.simps invarU_unflatU id_apply o_apply G.pred_map
    elim!: G.pred_mono_strong)
  done

lemmas invar_unflat = mp[OF spec[OF invar_unflat_raw]]

lemma flat_unflat_raw: "\<forall>u0. invar (snoc F u0) t \<longrightarrow> flat (unflat u0 t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar.simps snoc.simps
      flatU_unflatU id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done

lemmas flat_unflat = mp[OF spec[OF flat_unflat_raw]]

lemma unflat_flat_raw: "\<forall>u0. invar u0 t \<longrightarrow> unflat u0 (flat t) = t"
  apply (induct t)
  apply (auto simp only:
      unflat.simps flat.simps invar.simps unflatU_flatU id_apply o_apply G.pred_map G.map_comp
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong)
  done

lemmas unflat_flat = mp[OF spec[OF unflat_flat_raw]]


section \<open>Constructor is Bijection\<close>

lemma un_T_T: "un_T (T x) = x"
  unfolding T_def un_T_def
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) []
  apply (auto simp only:
      raw.case shapeA.case shapeB.case Rep_T_inverse o_apply
      G.map_comp G.map_ident Rep_T[simplified] unflat_flat
    cong: G.map_cong) []
  done

lemma T_un_T: "T (un_T x) = x"
  unfolding T_def un_T_def G.map_comp o_def
  apply (rule iffD1[OF Rep_T_inject])
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) []
  apply (insert Rep_T[simplified, of x])
  apply (rule raw.exhaust[of "Rep_T x"])
  apply (auto simp only:
      raw.case shapeA.case shapeB.case invar.simps invar_shapeA_depth_iff invar_shapeB_depth_iff
       snoc.simps(1)[of F, symmetric]
      G.pred_map Abs_T_inverse invar_unflat flat_unflat id_apply o_apply mem_Collect_eq
    intro!: trans[OF G.map_cong_pred G.map_ident]
    elim!: G.pred_mono_strong) []
  done


section \<open>Characteristic Theorems\<close>

subsection \<open>map\<close>

lemma flatU_map_raw:
  "map_shapeA f g (flat_shapeA ua) = flat_shapeA (map_shapeA (map_FA f g) (map_FB f g) ua) \<and>
   map_shapeB f g (flat_shapeB ub) = flat_shapeB (map_shapeB (map_FA f g) (map_FB f g) ub)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. map_shapeA f g (flat_shapeA ua) = flat_shapeA (map_shapeA (map_FA f g) (map_FB f g) ua)"
    "\<lambda>ub. map_shapeB f g (flat_shapeB ub) = flat_shapeB (map_shapeB (map_FA f g) (map_FB f g) ub)" ua ub])
  apply (simp_all only:
      shapeA.map shapeB.map flat_shapeA.simps flat_shapeB.simps FA.map_comp FB.map_comp o_apply
    cong: FA.map_cong0 FB.map_cong0)
  done

lemmas flatU_map =
  conjunct1[OF flatU_map_raw]
  conjunct2[OF flatU_map_raw]

lemma map_raw_flat: "map_raw f g (flat t) = flat (map_raw (map_FA f g) (map_FB f g) t)"
  apply (induct t)
  apply (simp only:
      raw.map flat.simps G.map_comp flatU_map o_apply
    cong: G.map_cong0)
  done

lemma map_T: "map_T f g (T t) = T (map_G f g (map_T (map_FA f g) (map_FB f g)) t)"
  unfolding map_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) []
  apply (auto simp only:
      raw.map shapeA.map shapeB.map G.map_comp o_apply mem_Collect_eq
     invar_map_closed Abs_T_inverse Rep_T[simplified] map_raw_flat
    cong: G.map_cong) []
  done


subsection \<open>set\<close>

lemma flatU_set1_raw:
  fixes ua :: "(('a, 'b) FA, ('a, 'b) FB) shapeA" and ub :: "(('a, 'b) FA, ('a, 'b) FB) shapeB"
  shows
  "(set1_shapeA (flat_shapeA ua) = UNION (set1_shapeA ua) set1_FA \<union> UNION (set2_shapeA ua) set1_FB) \<and>
   (set1_shapeB (flat_shapeB ub) = UNION (set1_shapeB ub) set1_FA \<union> UNION (set2_shapeB ub) set1_FB)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. set1_shapeA (flat_shapeA ua) = UNION (set1_shapeA ua) set1_FA \<union> UNION (set2_shapeA ua) set1_FB"
    "\<lambda>ub. set1_shapeB (flat_shapeB ub) = UNION (set1_shapeB ub) set1_FA \<union> UNION (set2_shapeB ub) set1_FB" ua ub])
  apply (simp_all only:
      flat_shapeA.simps flat_shapeB.simps shapeA.set shapeB.set FA.set_map FB.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemma flatU_set2_raw:
  fixes ua :: "(('a, 'b) FA, ('a, 'b) FB) shapeA" and ub :: "(('a, 'b) FA, ('a, 'b) FB) shapeB"
  shows
  "(set2_shapeA (flat_shapeA ua) = UNION (set1_shapeA ua) set2_FA \<union> UNION (set2_shapeA ua) set2_FB) \<and>
   (set2_shapeB (flat_shapeB ub) = UNION (set1_shapeB ub) set2_FA \<union> UNION (set2_shapeB ub) set2_FB)"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. set2_shapeA (flat_shapeA ua) = UNION (set1_shapeA ua) set2_FA \<union> UNION (set2_shapeA ua) set2_FB"
    "\<lambda>ub. set2_shapeB (flat_shapeB ub) = UNION (set1_shapeB ub) set2_FA \<union> UNION (set2_shapeB ub) set2_FB" ua ub])
  apply (simp_all only:
      flat_shapeA.simps flat_shapeB.simps shapeA.set shapeB.set FA.set_map FB.set_map
      UN_simps UN_singleton UN_insert UN_empty UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong)
  done

lemmas flatU_set =
  conjunct1[OF flatU_set1_raw]
  conjunct1[OF flatU_set2_raw]
  conjunct2[OF flatU_set1_raw]
  conjunct2[OF flatU_set2_raw]

lemma set_raw_flat:
  "set1_raw (flat t) = UNION (set1_raw t) set1_FA \<union> UNION (set2_raw t) set1_FB"
  "set2_raw (flat t) = UNION (set1_raw t) set2_FA \<union> UNION (set2_raw t) set2_FB"
  apply (induct t)
  apply (simp_all only: flat.simps raw.set shapeA.set G.set_map flatU_set
      UN_simps UN_Un UN_Un_distrib Un_ac
    cong: SUP_cong)
  done

lemma set_T: "set1_T (T g) = set1_G g \<union>
  (\<Union>(set1_FA ` (\<Union>(set1_T ` (set3_G g))))) \<union>
  (\<Union>(set1_FB ` (\<Union>(set2_T ` (set3_G g)))))"
  "set2_T (T g) = set2_G g \<union>
  (\<Union>(set2_FA ` (\<Union>(set1_T ` (set3_G g))))) \<union>
  (\<Union>(set2_FB ` (\<Union>(set2_T ` (set3_G g)))))"
  unfolding set1_T_def set2_T_def T_def o_apply
sorry(*
  apply -
  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) []
  apply (simp_all only:
      raw.set shapeA.set shapeB.set G.set_map set_raw_flat o_apply
      Sup_image_eq UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong) []

  apply (subst Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) []
  apply (simp_all only:
      raw.set shapeA.set shapeB.set G.set_map set_raw_flat o_apply
      Sup_image_eq UN_simps UN_singleton UN_empty2 UN_Un UN_Un_distrib Un_ac Un_empty_left
    cong: SUP_cong) []
  done*)


subsection \<open>rel\<close>

lemma flatU_rel_raw:
  "(\<forall>u0 ua'. invar_shapeA u0 ua \<longrightarrow> invar_shapeA u0 ua' \<longrightarrow> rel_shapeA R S (flat_shapeA ua) (flat_shapeA ua') \<longrightarrow>
     rel_shapeA (rel_FA R S) (rel_FB R S) ua ua') \<and>
   (\<forall>u0 ub'. invar_shapeB u0 ub \<longrightarrow> invar_shapeB u0 ub' \<longrightarrow> rel_shapeB R S (flat_shapeB ub) (flat_shapeB ub') \<longrightarrow>
     rel_shapeB (rel_FA R S) (rel_FB R S) ub ub')"
  apply (rule shapeA_shapeB.induct[of
    "\<lambda>ua. \<forall>u0 ua'. invar_shapeA u0 ua \<longrightarrow> invar_shapeA u0 ua' \<longrightarrow> rel_shapeA R S (flat_shapeA ua) (flat_shapeA ua') \<longrightarrow>
    rel_shapeA (rel_FA R S) (rel_FB R S) ua ua'"
    "\<lambda>ub. \<forall>u0 ub'. invar_shapeB u0 ub \<longrightarrow> invar_shapeB u0 ub' \<longrightarrow> rel_shapeB R S (flat_shapeB ub) (flat_shapeB ub') \<longrightarrow>
    rel_shapeB (rel_FA R S) (rel_FB R S) ub ub'"
    ua ub])
  apply (auto 0 4 simp only:
      invar_shapeA.simps invar_shapeB.simps flat_shapeA.simps flat_shapeB.simps shapeA.rel_inject shapeB.rel_inject
      invar_shapeA_depth_iff invar_shapeB_depth_iff ball_simps id_apply
      FA.rel_map pred_FA_def FA.set_map FB.rel_map pred_FB_def FB.set_map
    elim!: FA.rel_mono_strong FB.rel_mono_strong
    split: list.split label.splits)
  done

lemma flatU_rel:
  "invar_shapeA u0 ua \<Longrightarrow> invar_shapeA u0 ua' \<Longrightarrow>
    rel_shapeA R S (flat_shapeA ua) (flat_shapeA ua') = rel_shapeA (rel_FA R S) (rel_FB R S) ua ua'"
  "invar_shapeB u0 ub \<Longrightarrow> invar_shapeB u0 ub' \<Longrightarrow>
    rel_shapeB R S (flat_shapeB ub) (flat_shapeB ub') = rel_shapeB (rel_FA R S) (rel_FB R S) ub ub'"
  apply (rule iffI[rotated, OF rel_funD[OF flat_shapeA.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct1[OF flatU_rel_raw]]]]]]; assumption)
  apply (rule iffI[rotated, OF rel_funD[OF flat_shapeB.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF conjunct2[OF flatU_rel_raw]]]]]]; assumption)
  done

lemma rel_raw_flat_raw:
  "\<forall>t' u0. invar u0 t \<longrightarrow> invar u0 t' \<longrightarrow>
   rel_raw R S (flat t) (flat t') \<longrightarrow> rel_raw (rel_FA R S) (rel_FB R S) t t'"
  apply (induct t)
  apply (rule allI)
  apply (case_tac t')
  apply (auto simp only:
      invar.simps flat.simps raw.rel_inject G.rel_map pred_G_def flatU_rel G.set_map ball_simps id_apply
    elim!: G.rel_mono_strong)
  done

lemma rel_raw_flat:
  "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
   rel_raw R S (flat t) (flat t') = rel_raw (rel_FA R S) (rel_FB R S) t t'"
  apply (rule iffI[rotated, OF rel_funD[OF flat.transfer]], assumption)
  apply (rule mp[OF mp[OF mp[OF spec[OF spec[OF rel_raw_flat_raw]]]]]; assumption)
  done

lemma rel_T: "rel_T R S (T g) (T g') = rel_G R S (rel_T (rel_FA R S) (rel_FB R S)) g g'"
  unfolding rel_T_def T_def vimage2p_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only:
       invar.simps invar_shapeA.simps invar_shapeB.simps list.case id_apply o_apply
       snoc.simps(1)[of F, symmetric]
       G.pred_map G.pred_True G.map_comp Rep_T[simplified] invar_flat
     cong: G.pred_cong) [2]
  apply (simp only:
    raw.rel_inject G.rel_map shapeA.rel_inject shapeB.rel_inject o_apply
    rel_raw_flat[OF Rep_T[simplified] Rep_T[simplified]])
  done

(*
bnf_axiomatization ('a, 'b) FA
bnf_axiomatization ('a, 'b) FB

bnf_axiomatization ('a, 'b, 'x) G [wits: "'a \<Rightarrow> 'b \<Rightarrow> ('a, 'b, 'x) G"]


section \<open>Raw Type\<close>

datatype label = F
type_synonym depth = "label list"
datatype ('a, 'b) shapeA = LeafA 'a | NodeA "(('a, 'b) shapeA, ('a, 'b) shapeB) FA"
     and ('a, 'b) shapeB = LeafB 'b | NodeB "(('a, 'b) shapeA, ('a, 'b) shapeB) FB"
datatype ('a, 'b) raw = Cons "(('a, 'b) shapeA, ('a, 'b) shapeB, ('a, 'b) raw) G"
*)
bnf_axiomatization ('a, 'b, 'c, 'd) R
bnf_axiomatization ('e, 'f, 'g) DA
bnf_axiomatization ('h, 'i, 'j) DB

consts defobj :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB, (('a, 'c) FA, ('b, 'd) FA, ('a, 'c) FB, ('b, 'd) FB) R) G \<Rightarrow> ('a, 'b, 'c, 'd) R"
consts argobjA :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) FA \<Rightarrow> (('e, 'h) FA, ('f, 'i) FA, ('g, 'j) FA) DA"
consts argobjB :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) FB \<Rightarrow> (('e, 'h) FB, ('f, 'i) FB, ('g, 'j) FB) DB"

axiomatization where
  defobj_transfer: "rel_fun (rel_G (rel_DA RE RF RG) (rel_DB RH RI RJ) (rel_R (rel_FA RA RC) (rel_FA RB RD) (rel_FB RA RC) (rel_FB RB RD))) (rel_R RA RB RC RD) defobj defobj" and
  argobjA_transfer: "rel_fun (rel_FA (rel_DA RE RF RG) (rel_DB RH RI RJ)) (rel_DA (rel_FA RE RH) (rel_FA RF RI) (rel_FA RG RJ)) argobjA argobjA" and
  argobjB_transfer: "rel_fun (rel_FB (rel_DA RE RF RG) (rel_DB RH RI RJ)) (rel_DB (rel_FB RE RH) (rel_FB RF RI) (rel_FB RG RJ)) argobjB argobjB"

lemma fun_pred_rel: "pred_fun A B x = rel_fun (eq_onp A) (eq_onp B) x x"
  unfolding pred_fun_def rel_fun_def eq_onp_def by auto

lemma pred_funD: "pred_fun A B f \<Longrightarrow> A x \<Longrightarrow> B (f x)"
  unfolding pred_fun_def by auto

lemma R_pred_mono: "P1 \<le> Q1 \<Longrightarrow> P2 \<le> Q2 \<Longrightarrow> P3 \<le> Q3 \<Longrightarrow> P4 \<le> Q4 \<Longrightarrow> pred_R P1 P2 P3 P4 \<le> pred_R Q1 Q2 Q3 Q4"
  using R.pred_mono_strong[of P1 P2 P3 P4 _ Q1 Q2 Q3 Q4] sorry

lemma defobj_invar: "pred_fun (pred_G (pred_DA RE RF RG) (pred_DB RH RI RJ) (pred_R (pred_FA RA RC) (pred_FA RB RD) (pred_FB RA RC) (pred_FB RB RD))) (pred_R RA RB RC RD) defobj"
  unfolding fun_pred_rel G.rel_eq_onp[symmetric] FA.rel_eq_onp[symmetric] FB.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] R.rel_eq_onp[symmetric] by (rule defobj_transfer)

lemma argobjA_invar: "pred_fun (pred_FA (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DA (pred_FA RE RH) (pred_FA RF RI) (pred_FA RG RJ)) argobjA"
  unfolding fun_pred_rel FA.rel_eq_onp[symmetric] FB.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] by (rule argobjA_transfer)
lemma argobjB_invar: "pred_fun (pred_FB (pred_DA RE RF RG) (pred_DB RH RI RJ)) (pred_DB (pred_FB RE RH) (pred_FB RF RI) (pred_FB RG RJ)) argobjB"
  unfolding fun_pred_rel FA.rel_eq_onp[symmetric] FB.rel_eq_onp[symmetric]
    DA.rel_eq_onp[symmetric] DB.rel_eq_onp[symmetric] by (rule argobjB_transfer)


lemma defobj_natural: "defobj (map_G (map_DA fe ff fg) (map_DB fh fi fj) (map_R (map_FA fa fc) (map_FA fb fd) (map_FB fa fc) (map_FB fb fd)) x) = map_R fa fb fc fd (defobj x)"
  using rel_funD[OF defobj_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"
    "BNF_Def.Grp UNIV fa" "BNF_Def.Grp UNIV fc" "BNF_Def.Grp UNIV fb" "BNF_Def.Grp UNIV fd"]
  unfolding FA.rel_Grp FB.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp G.rel_Grp by (auto simp: Grp_def)

lemma argobjA_natural: "argobjA (map_FA (map_DA fe ff fg) (map_DB fh fi fj) x) = map_DA (map_FA fe fh) (map_FA ff fi) (map_FA fg fj) (argobjA x)"
  using rel_funD[OF argobjA_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"]
  unfolding FA.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp by (auto simp: Grp_def)

lemma argobjB_natural: "argobjB (map_FB (map_DA fe ff fg) (map_DB fh fi fj) x) = map_DB (map_FB fe fh) (map_FB ff fi) (map_FB fg fj) (argobjB x)"
  using rel_funD[OF argobjB_transfer, of "BNF_Def.Grp UNIV fe" "BNF_Def.Grp UNIV ff" "BNF_Def.Grp UNIV fg"
    "BNF_Def.Grp UNIV fh" "BNF_Def.Grp UNIV fi" "BNF_Def.Grp UNIV fj"]
  unfolding FB.rel_Grp R.rel_Grp DA.rel_Grp DB.rel_Grp by (auto simp: Grp_def)

primrec f_shapeA :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shapeA \<Rightarrow> (('e, 'h) shapeA, ('f, 'i) shapeA, ('g, 'j) shapeA) DA"
    and f_shapeB :: "(('e, 'f, 'g) DA, ('h, 'i, 'j)DB) shapeB \<Rightarrow> (('e, 'h) shapeB, ('f, 'i) shapeB, ('g, 'j) shapeB) DB" where
  "f_shapeA (LeafA a) = map_DA LeafA LeafA LeafA a"
| "f_shapeA (NodeA fa) = map_DA NodeA NodeA NodeA (argobjA (map_FA f_shapeA f_shapeB fa))"
| "f_shapeB (LeafB b) = map_DB LeafB LeafB LeafB b"
| "f_shapeB (NodeB fb) = map_DB NodeB NodeB NodeB (argobjB (map_FB f_shapeA f_shapeB fb))"

primrec f_raw :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) raw \<Rightarrow>
  (('a, 'c) shapeA, ('b, 'd) shapeA, ('a, 'c) shapeB, ('b, 'd) shapeB) R" where
  "f_raw (Cons g) = defobj (map_G f_shapeA f_shapeB ((map_R un_NodeA un_NodeA un_NodeB un_NodeB) o f_raw) g)"

definition f :: "(('e, 'f, 'g) DA, ('h, 'i, 'j) DB) T \<Rightarrow> ('a, 'b, 'c, 'd) R" where
  "f t = map_R un_LeafA un_LeafA un_LeafB un_LeafB (f_raw (Rep_T t))"

lemma f_shapeA_flat_shapeA: "f_shapeA (flat_shapeA x) = map_DA flat_shapeA flat_shapeA flat_shapeA (f_shapeA (map_shapeA argobjA argobjB x))"
  sorry
lemma f_shapeB_flat_shapeB: "f_shapeB (flat_shapeB x) = map_DB flat_shapeB flat_shapeB flat_shapeB (f_shapeB (map_shapeB argobjA argobjB x))"
  sorry

lemma invar_shapeA_f_shapeA: "invar_shapeA u0 u \<Longrightarrow> pred_DA (invar_shapeA u0) (invar_shapeA u0) (invar_shapeA u0) (f_shapeA u)"
  sorry
lemma invar_shapeB_f_shapeB: "invar_shapeB u0 u \<Longrightarrow> pred_DB (invar_shapeB u0) (invar_shapeB u0) (invar_shapeB u0) (f_shapeB u)"
  sorry

lemma f_raw_flat: "invar u t \<Longrightarrow> f_raw (flat t) = map_R flat_shapeA flat_shapeA flat_shapeB flat_shapeB (f_raw (map_raw argobjA argobjB t))"
  sorry

lemma invar_f_raw: "invar u0 x \<Longrightarrow> pred_R (invar_shapeA u0) (invar_shapeA u0) (invar_shapeB u0) (invar_shapeB u0) (f_raw (map_raw argobjA argobjB x))"
  sorry

lemma "f (T g) = defobj (map_G id id (f o map_T argobjA argobjB) g)"
  unfolding f_def[abs_def] map_T_def T_def o_def
  sorry

hide_const (open) f

section \<open>Induction\<close>

(* Preliminaries *)

lemma bi_unique_Grp: "bi_unique (BNF_Def.Grp A f) = inj_on f A"
  unfolding bi_unique_def Grp_def inj_on_def by auto

(* The "recursion sizes" of elements of 'a raw -- on the second argument of G *)
datatype sz = SCons "(unit, unit, sz) G"

primrec size :: "('a, 'b) raw \<Rightarrow> sz" where
  "size (Cons g) = SCons (map_G (\<lambda>_. ()) (\<lambda>_. ()) size g)"

lemma size_unflat: 
  "size (unflat ul r) = size (r :: ('a, 'b) raw)"
  apply (induct r arbitrary: ul)
  apply (auto intro: G.map_cong simp only: unflat.simps size.simps G.map_comp o_apply)
  done

lemma size_map_raw: 
  "size (map_raw f g r) = size r"
  apply (induct r)
  apply (auto intro: G.map_cong simp only: raw.map size.simps G.map_comp o_apply)
  done

lemma raw_induct_size: 
  assumes "\<And> r :: ('a, 'b) raw. (\<And>r'. size r' \<in> set3_G (case size r of SCons x \<Rightarrow> x) \<Longrightarrow> PP r') \<Longrightarrow> PP r"
  shows "PP (r :: ('a, 'b) raw)"
  apply (induct "size r" arbitrary: r)
  apply (rule assms)
  apply (drule sym)
  apply (simp only: sz.case)
  done


(*  THE ASSUMPTIONS: *)

consts P :: "('a, 'b) T \<Rightarrow> bool"

axiomatization where 
P_ind: "\<And>t. (\<And> t'. t' \<in> set3_G t \<Longrightarrow> P t') \<Longrightarrow> P (T t :: ('a, 'b) T)" and
P_param: "\<And>R S :: 'a \<Rightarrow> 'b \<Rightarrow> bool. bi_unique R \<Longrightarrow> bi_unique S \<Longrightarrow> rel_fun (rel_T R S) (=) P P"

lemma P_mono: "inj_on f (set1_T a) \<Longrightarrow> inj_on g (set2_T a) \<Longrightarrow> P (map_T f g a) \<Longrightarrow> P a"
  using P_param[of "BNF_Def.Grp (set1_T a) f" "BNF_Def.Grp (set2_T a) g"]
  unfolding T.rel_Grp rel_fun_def bi_unique_Grp unfolding Grp_def
  apply (auto simp only:
    simp_thms mem_Collect_eq True_implies_equals implies_True_equals all_simps ex_simps)
  done

lemma P_raw: 
  fixes r :: "(('a, 'b) shapeA, ('a, 'b) shapeB) raw"
  shows "invar [] r \<longrightarrow> P (Abs_T r)"
  apply (rule raw_induct_size)
  apply (rule impI)
  apply (rule iffD1[OF arg_cong[of _ _ P, OF T_un_T]])
  apply (rule P_ind)
  apply (simp only: un_T_def G.set_map Abs_T_inverse mem_Collect_eq)
  apply (erule imageE)
  apply hypsubst_thin
  apply (unfold o_apply)
  apply (rule P_mono[of NodeA _ NodeB])
   apply (simp only: inj_on_def shapeA.inject simp_thms Ball_def)
   apply (simp only: inj_on_def shapeB.inject simp_thms Ball_def)
  apply (rule raw.exhaust)
  apply (frule iffD1[OF arg_cong[of _ _ "invar []"]])
   apply assumption
  apply (simp only: invar.simps raw.case G.pred_set map_T_def o_apply Abs_T_inverse mem_Collect_eq
    invar_unflat snoc.simps)
  apply (drule meta_spec)
  apply (erule meta_mp[THEN mp])
   apply (auto simp only: size.simps sz.case G.set_map size_map_raw size_unflat) []
  apply (simp only: invar_map_closed invar_unflat snoc.simps)
  done

lemma P_end_product: 
  fixes t :: "('a, 'b) T"
  shows "P t"
  apply (rule P_mono[of LeafA _ LeafB])
   apply (simp only: inj_on_def shapeA.inject simp_thms Ball_def P_raw)
   apply (simp only: inj_on_def shapeB.inject simp_thms Ball_def P_raw)
  unfolding map_T_def o_apply
  apply (rule mp[OF P_raw])
  apply (simp only: invar_map_closed Rep_T[unfolded mem_Collect_eq])
  done

end