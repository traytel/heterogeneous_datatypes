theory HDatatype2
  imports "~~/src/HOL/Library/BNF_Axiomatization"
begin

declare [[bnf_internals, typedef_overloaded]]

bnf_axiomatization ('a, 'x) F

bnf_axiomatization ('a, 'x) G [wits: "'a \<Rightarrow> ('a, 'x) G"]

lemma F_pred_map: "pred_F P1 P2 (map_F f1 f2 x) = pred_F (P1 o f1) (P2 o f2) x"
  unfolding pred_F_def F.set_map ball_simps o_def ..

lemma F_pred_True: "pred_F (\<lambda>_. True) (\<lambda>_. True) = (\<lambda>_. True)"
  unfolding pred_F_def F.set_map by blast

lemma F_pred_rel: "rel_F (eq_onp P1) (eq_onp P2) x x = pred_F P1 P2 x"
  unfolding pred_F_def F.in_rel
  by (force simp: F.set_map F.map_comp F.map_ident eq_onp_def o_def
    intro!: exI[of _ "map_F (\<lambda>x. (x, x)) (\<lambda>x. (x, x)) x"])

lemma F_pred_cong[fundef_cong]: "x = y \<Longrightarrow>
  ((\<And>x. x \<in> set1_F y \<Longrightarrow> P1 x = Q1 x)) \<Longrightarrow>
   (\<And>x. x \<in> set2_F y \<Longrightarrow> P2 x = Q2 x) \<Longrightarrow> pred_F P1 P2 x = pred_F Q1 Q2 y"
  unfolding pred_F_def by simp

lemma F_map_cong_pred: "pred_F (\<lambda>x. f1 x = g1 x)  (\<lambda>x. f2 x = g2 x) x \<Longrightarrow> map_F f1 f2 x = map_F g1 g2 x"
  unfolding pred_F_def by (auto intro: F.map_cong0)

lemma F_pred_mono_strong: "pred_F P1 P2 x \<Longrightarrow> (\<And>z. z \<in> set1_F x \<Longrightarrow> P1 z \<Longrightarrow> Q1 z) \<Longrightarrow>
  (\<And>z. z \<in> set2_F x \<Longrightarrow> P2 z \<Longrightarrow> Q2 z) \<Longrightarrow> pred_F Q1 Q2 x"
  unfolding F_pred_rel[symmetric] by (erule F.rel_mono_strong, (auto simp: eq_onp_def))

lemma F_pred_mono_strong2:
  "pred_F P1 P2 x \<Longrightarrow> pred_F (\<lambda>x. P1 x \<longrightarrow> Q1 x) (\<lambda>x. P2 x \<longrightarrow> Q2 x) x \<Longrightarrow> pred_F Q1 Q2 x"
  by (erule F_pred_mono_strong, auto simp: pred_F_def)

lemma G_pred_map: "pred_G P1 P2 (map_G f1 f2 x) = pred_G (P1 o f1) (P2 o f2) x"
  unfolding pred_G_def G.set_map ball_simps o_def ..

lemma G_pred_cong[fundef_cong]: "x = y \<Longrightarrow>
  ((\<And>x. x \<in> set1_G y \<Longrightarrow> P1 x = Q1 x)) \<Longrightarrow>
   (\<And>x. x \<in> set2_G y \<Longrightarrow> P2 x = Q2 x) \<Longrightarrow> pred_G P1 P2 x = pred_G Q1 Q2 y"
  unfolding pred_G_def by simp

lemma G_pred_True: "pred_G (\<lambda>_. True) (\<lambda>_. True) = (\<lambda>_. True)"
  unfolding pred_G_def G.set_map by blast

lemma G_pred_rel: "rel_G (eq_onp P1) (eq_onp P2) x x = pred_G P1 P2 x"
  unfolding pred_G_def G.in_rel
  by (force simp: G.set_map G.map_comp G.map_ident eq_onp_def o_def
    intro!: exI[of _ "map_G (\<lambda>x. (x, x)) (\<lambda>x. (x, x)) x"])

lemma G_pred_mono_strong: "pred_G P1 P2 x \<Longrightarrow> (\<And>z. z \<in> set1_G x \<Longrightarrow> P1 z \<Longrightarrow> Q1 z) \<Longrightarrow>
  (\<And>z. z \<in> set2_G x \<Longrightarrow> P2 z \<Longrightarrow> Q2 z) \<Longrightarrow> pred_G Q1 Q2 x"
  unfolding G_pred_rel[symmetric] by (erule G.rel_mono_strong, (auto simp: eq_onp_def))

lemma G_map_cong_pred: "pred_G (\<lambda>x. f1 x = g1 x)  (\<lambda>x. f2 x = g2 x) x \<Longrightarrow> map_G f1 f2 x = map_G g1 g2 x"
  unfolding pred_G_def by (auto intro: G.map_cong0)

(*datatype 'a T = T "('a, ('a, 'a T) F T) G"*)

datatype (discs_sels) U0 = A0 | U0 U0
datatype (discs_sels) ('a, 'x) U = A 'a | U "(('a, 'x) U, 'x) F"
datatype (discs_sels) 'a T' = T' "(('a, 'a T') U, 'a T') G"

primrec invarU :: "(U0 \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> U0 \<Rightarrow> ('a, 'b) U \<Rightarrow> bool" where
  "invarU b A0 u = (case u of A x \<Rightarrow> True | U _ \<Rightarrow> False)"
| "invarU b (U0 u0) u = (case u of A _ \<Rightarrow> False | U f \<Rightarrow> pred_F id id (map_F (invarU b u0) (b u0) f))"

primrec invar :: "U0 \<Rightarrow> 'a T' \<Rightarrow> bool" where
  "invar u (T' g) = pred_G (invarU (\<lambda>u f. f u) u) id (map_G (map_U id ((\<lambda>x u. invar u x))) (invar (U0 u)) g)"

primrec (transfer) flatU :: "('y \<Rightarrow> 'x) \<Rightarrow> (('a, 'x) F, 'y) U \<Rightarrow> ('a, 'x) U" where
  "flatU flat (A f) = U (map_F A id f)"
| "flatU flat (U f) = U (map_F (flatU flat) flat f)"

primrec unflatU :: "(U0 \<Rightarrow> 'x \<Rightarrow> 'y) \<Rightarrow> U0 \<Rightarrow> ('a, 'x) U \<Rightarrow> (('a, 'x) F, 'y) U" where
  "unflatU unflat A0 u = (case u of U f \<Rightarrow> A (map_F un_A id f))"
| "unflatU unflat (U0 u0) u =
    (case u of U f \<Rightarrow> U (map_F (unflatU unflat u0) (unflat u0) f))"

primrec (transfer) flat :: "('a, 'a T') F T' \<Rightarrow> 'a T'" where
  "flat (T' g) = T' (map_G (flatU id) id (map_G (map_U id flat) flat g))"

primrec unflat :: "U0 \<Rightarrow> 'a T' \<Rightarrow> ('a, 'a T') F T'" where
  "unflat u0 (T' g) = T'
    (map_G (map_U (map_F id (\<lambda>f. flat (f (U0 u0)))) id o unflatU (\<lambda>u0 f. f u0) u0) id
    (map_G (map_U id (\<lambda>x u0. unflat u0 x)) (unflat (U0 u0)) g))"

lemmas invarU_simps = invarU.simps[unfolded F_pred_map id_o]

lemma invarU_cong: "invarU b u0 u \<Longrightarrow>
  pred_U (\<lambda>x. True) (\<lambda>x. \<forall>u0. b u0 x \<longrightarrow> b' u0 x) u \<Longrightarrow> invarU b' u0 u"
  apply (induct u0 arbitrary: u)
  apply (auto simp only: invarU_simps U.pred_inject split: U.splits)
  apply (rotate_tac -1)
  apply (erule F_pred_mono_strong2)
  apply (erule F_pred_mono_strong)
  apply auto
  done

lemma invarU_map: "invarU b u0 (map_U f g u) = invarU (\<lambda>u. b u o g) u0 u"
  apply (induct u0 arbitrary: u)
  apply (auto simp only: invarU_simps U.map F_pred_map F_pred_True o_apply
    split: U.splits elim!: F_pred_mono_strong)
  done

lemmas invar_simps = invar.simps[unfolded G_pred_map o_def id_apply invarU_map]

lemma invar_map: "invar u0 (map_T' f t) = invar u0 t"
  apply (induct t arbitrary: u0)
  apply (auto simp only: invar_simps T'.map G_pred_map invarU_map o_apply id_apply
    elim!: G_pred_mono_strong invarU_cong simp: pred_U_def)
  done

definition "wit_T x = T' (wit_G (A x))"

lemma invar_wit_T: "invar A0 (wit_T x)"
  by (auto simp only: wit_T_def invar.simps invarU.simps G_pred_map o_def id_apply invarU_map pred_G_def
    G.set_map U.case dest!: G.wit)

typedef 'a T = "{t :: 'a T'. invar A0 t}"
  by (rule exI[of _ "wit_T undefined"]) (simp only: invar_wit_T mem_Collect_eq)

lemma flatU_map: "flatU g (map_U (map_F f g) h u) = map_U f g (flatU h u)"
  apply (induct u)
  apply (auto simp only: flatU.simps U.map F.map_comp o_def id_apply intro: F.map_cong0)
  done

lemmas flat_simps = flat.simps[unfolded G.map_comp o_def id_apply
  flatU_map[of id id flat, unfolded F.map_id0] U.map_id]

lemma invarU_flatU: "invarU (\<lambda>u0. b (U0 u0) o f) u0 u \<Longrightarrow>
  pred_U (pred_F (\<lambda>x. True) (b A0)) (\<lambda>x. True) u \<Longrightarrow>
  invarU b (U0 u0) (flatU f u)"
  apply (induct u0 arbitrary: u)
  apply (fastforce simp only: invarU.simps flatU.simps F_pred_map o_def id_apply
    U.pred_inject elim!: F_pred_mono_strong2 split: U.splits
    intro: iffD1[OF F_pred_cong, OF refl _ _ iffD2[OF fun_cong[OF F_pred_True] TrueI]])+
  done

lemma invar_flat:
  "invar u0 t \<Longrightarrow> pred_T' (pred_F (\<lambda>x. True) (invar A0)) t \<Longrightarrow> invar (U0 u0) (flat t)"
  apply (induct t arbitrary: u0)
  apply (auto simp only: flat_simps invar_simps G_pred_map o_apply pred_U_def
    elim!: G_pred_mono_strong invarU_cong intro!: invarU_flatU)
  apply (auto simp: pred_G_def pred_U_def)
  done

lemma unflatU_map:
  "invarU b (U0 u0) u \<Longrightarrow> unflatU (\<lambda>u0 f. f u0) u0 (map_U f (\<lambda>x u0. un u0 x) u) =
     map_U (map_F f (\<lambda>x u0. un u0 x)) id (unflatU un u0 u)"
  apply (induct u0 arbitrary: u)
  apply (auto simp only: unflatU.simps invarU_simps F.map_comp U.map U.sel(1) o_apply id_apply
    intro!: F_map_cong_pred elim!: F_pred_mono_strong split: U.splits)
  done

lemma flatU_unflatU: "invarU b (U0 u0) u \<Longrightarrow>
  pred_U (\<lambda>x. True) (\<lambda>x. f (g u0 x) = x) u \<Longrightarrow> flatU f (unflatU g u0 u) = u"
  apply (induct u0 arbitrary: u)
  apply (auto simp only: unflatU.simps flatU.simps invarU.simps F_pred_map F.map_comp o_def
    id_apply U.sel(1) intro!: trans[OF F_map_cong_pred F.map_ident] elim!: F_pred_mono_strong split: U.splits)
  apply (case_tac z; simp)
  apply (case_tac z; simp)
  apply (erule notE)
  apply (drule meta_spec)
  apply (drule meta_mp)
  prefer 2
  apply (erule meta_mp)
  apply (auto elim!: F_pred_mono_strong)
  apply (auto simp: pred_F_def pred_U_def) sledgehammer
  apply (case_tac u0; auto simp add: F.map_comp pred_F_def F.set_map o_def
    intro!: trans[OF F_map_cong_pred F.map_ident] split: U.splits)
  apply (case_tac x; auto)
  done

lemma "invar (U0 u0) t \<Longrightarrow> flat (unflat u0 t) = t"
  apply (induct t arbitrary: u0)
  apply (auto simp only: unflat.simps flat_simps invar_simps[unfolded pred_G_def]
    G.map_comp o_def id_apply
    intro!: trans[OF G.map_cong0 G.map_ident])
  apply (subst unflatU_map)
  apply (erule bspec)
  apply assumption
  apply (auto simp only: U.map_comp F.map_comp o_def id_apply)
  apply (rule trans[OF arg_cong[of _ _ "flatU flat"]])
  apply (rule trans[OF U.map_cong0 U.map_ident])
  apply (rule trans[OF F.map_cong0 F.map_ident])
  apply (rule refl)
  apply auto [2]
  defer
  apply (rule flatU_unflatU)
  apply (erule bspec)
  apply assumption
  apply (drule meta_spec2)
  apply (erule thin_rl)
  apply (drule meta_spec)
  apply (drule meta_mp)
  apply assumption
  apply (drule meta_mp)
  prefer 2
  apply (erule meta_mp)
  apply assumption
  sledgehammer
  apply (case_tac z1)
  apply auto
thm flatU_map
  apply (subst flatU_map)

lemma unflat_simps: "invar (U0 u) (T' g) \<Longrightarrow>
  unflat u (T' g) = T' (map_G (unflatU unflat u) (unflat (U0 u)) g)"
  apply (auto simp only: invar_simps unflat.simps G.map_comp o_def id_apply
    U.map_comp F.map_comp unflatU_map
    intro!: G_map_cong_pred trans[OF U.map_cong0 U.map_ident] trans[OF F.map_cong0 F.map_ident]
    elim!: G_pred_mono_strong)
  apply (case_tac u)
  apply (auto simp: F_pred_map F.set_map split: U.splits)
apply (case_tac z)
  apply simp
  apply simp
  apply (frule zzz)
  apply assumption
  apply assumption
  apply (drule invarU_b)
  apply (auto split: U.splits)
  done

lemma "invar (U0 u0) t \<Longrightarrow> flat (unflat u0 t) = t"
  apply (induct t arbitrary: u0)
  apply (auto simp only: unflat.simps flat_simps invar_simps[unfolded pred_G_def]
    G.map_comp o_def id_apply
    intro!: trans[OF G.map_cong0 G.map_ident])
  apply (subst unflatU_map)
  apply (erule bspec)
  apply assumption
  apply (auto simp only: U.map_comp F.map_comp o_def id_apply)
  apply (rule trans[OF arg_cong[of _ _ "flatU flat"]])
  apply (rule trans[OF U.map_cong0 U.map_ident])
  apply (rule trans[OF F.map_cong0 F.map_ident])
  apply (rule refl)
  apply auto [2]
  defer
  apply (rule flatU_unflatU)
  apply (erule bspec)
  apply assumption
  apply (drule meta_spec2)
  apply (erule thin_rl)
  apply auto
thm flatU_map
  apply (subst flatU_map)



lemma "invar b (U0 u0) t \<Longrightarrow> flat (unflat u0 t) = t"
  apply (induct t arbitrary: u0)
  apply (auto simp only: unflat.simps flat_simps invar_simps[unfolded pred_G_def]
    G.map_comp o_def id_apply
    intro!: trans[OF G.map_cong0 G.map_ident])
  apply (subst unflatU_map)
  apply (erule bspec)
  apply assumption
  apply (auto simp only: U.map_comp F.map_comp o_def id_apply)
  apply (rule trans[OF arg_cong[of _ _ "flatU flat"]])
  apply (rule trans[OF U.map_cong0 U.map_ident])
  apply (rule trans[OF F.map_cong0 F.map_ident])
  apply (rule refl)
  apply auto [2]
  defer
  apply (rule flatU_unflatU)
  apply (erule bspec)
  apply assumption
  apply (drule meta_spec2)
  apply (erule thin_rl)
  apply auto
thm flatU_map
  apply (subst flatU_map)

lemma unflat_simps: "invar a (U0 u) (T' g) \<Longrightarrow>
  unflat u (T' g) = T' (map_G (unflatU unflat u) (unflat (U0 u)) g)"
  apply (auto simp only: invar_simps unflat.simps G.map_comp o_def id_apply unflatU_map
    U.map_comp F.map_comp
    intro!: G_map_cong_pred U.map_cong trans[OF F.map_cong0 F.map_ident] elim!: G_pred_mono_strong)
  apply (auto split: U.splits simp: F_pred_map)
  apply (case_tac u; auto simp add: F.map_comp o_def intro!: trans[OF F.map_cong0 F.map_ident])
  apply simp
  apply (frule zzz)
  apply assumption
  apply assumption
  apply (drule invarU_b)
  apply (auto split: U.splits)
  done

lemma invarU_flatU:
  "invarU (pred_F a (b u0)) b u0 u \<Longrightarrow> invarU a b (U0 u0) (flatU xxx u)"
  apply (induct u0 arbitrary: u)
  apply (auto simp only: flatU.simps invarU_simps F_pred_map o_apply id_apply
    elim!: F_pred_mono_strong split: U.splits)
  done

lemma invarU_unflatU: "invarU a b (U0 u0) u \<Longrightarrow> invarU (pred_F a b) b u0 (unflatU u0 u)"
  apply (induct u0 arbitrary: u)
  apply (auto simp only: unflatU.simps invarU_simps F_pred_map U.sel o_apply id_apply
    elim!: F_pred_mono_strong split: U.splits)
  done

lemma unflatU_flatU: "invarU (pred_F a b) b u0 u \<Longrightarrow> unflatU u0 (flatU u) = u"
  apply (induct u0 arbitrary: u)
  apply (auto simp only: unflatU.simps flatU.simps invarU_simps F.map_comp F_pred_True
    o_apply id_apply U.sel(1) intro!: trans[OF F_map_cong_pred F.map_ident]
    elim!: F_pred_mono_strong split: U.splits)
  done

lemma invarU_A_iff: "invarU a b u0 (A v) \<longleftrightarrow> u0 = A0 \<and> a v"
  by (cases u0) auto simp

lemma invarU_b: "invarU a b u0 x \<Longrightarrow> \<forall>z \<in> set2_U x. b z"
  apply (induct u0 arbitrary: x)
  apply (auto simp only: invarU_simps U.set pred_F_def split: U.splits)
  done

lemma flat_unflat: "invar a (U0 u0) t \<Longrightarrow> flat (unflat u0 t) = t"
  apply (induct t arbitrary: u0)
  apply (auto simp only: unflat.simps G.map_comp flat_simps o_def U.map_comp id_apply
    invar_simps flatU_map
    intro!: trans[OF G_map_cong_pred G.map_ident] elim!: G_pred_mono_strong)
  apply (subst flatU_unflatU)
  apply (subst invarU_map)
  apply (auto simp only: o_def id_apply pred_U_def U.map_comp elim!: invarU_cong
    intro!: trans[OF U.map_cong U.map_ident])
  apply (auto simp only: invarU_simps split: U.splits) []
  apply (drule meta_spec)
  apply (drule asm_rl)
  apply (erule thin_rl)
  apply (drule meta_spec)
  apply (drule meta_spec)
  apply (drule meta_mp)
  apply assumption
  apply (drule meta_mp)
  prefer 2
  apply (erule meta_mp)
  apply (auto simp add: pred_F_def)
  apply (drule bspec)
  apply assumption
  apply (case_tac u0)
  apply (auto simp only: pred_F_def invarU_simps U.set split: U.splits dest: invarU_b)
  done

lemma zzz: "invarU a b (U0 u) z \<Longrightarrow> z1 \<in> set1_U (unflatU u z) \<Longrightarrow> z2 \<in> set2_F z1 \<Longrightarrow> z2 \<in> set2_U z"
  apply (induct u arbitrary: z)
  apply (auto simp only: unflatU.simps invarU_simps U.set F.set_map id_apply split: U.splits)
  apply (erule notE)
  apply (drule meta_spec)+
  apply (drule meta_mp)
  prefer 2
  apply (drule meta_mp)
  apply assumption
  apply (auto simp:  split: U.splits)
  apply (meson U0.simps(3) invarU_A_iff pred_F_def)
  apply (auto simp add: pred_F_def)
  apply (drule bspec)
  apply assumption
  apply (auto simp: F_pred_map)
  apply (case_tac u)
  apply (auto simp: F.set_map pred_F_def split: U.splits)
  done

lemma unflat_simps: "invar a (U0 u) (T' g) \<Longrightarrow>
  unflat u (T' g) = T' (map_G (map_U id (unflat u) o unflatU u) (unflat (U0 u)) g)"
  apply (auto simp only: invar_simps unflat.simps G.map_comp o_apply id_apply unflatU_map
    U.map_comp F.map_comp dest: invarU_b
    intro!: G_map_cong_pred U.map_cong trans[OF F.map_cong0 F.map_ident] flat_unflat elim!: G_pred_mono_strong)
  apply (frule zzz)
  apply assumption
  apply assumption
  apply (drule invarU_b)
  apply (auto split: U.splits)
  done

lemma invar_flat: "invar (\<lambda>u0. pred_F (a (U0 u0)) (invar a u0)) u0 t \<Longrightarrow>
  invar a (U0 u0) (flat t)"
  apply (induct t arbitrary: u0)
  apply (unfold flat_simps invar_simps G_pred_map o_def)
  apply (erule G_pred_mono_strong)
  apply (rule invarU_flatU)
  apply (simp only: invarU_map o_def id_apply)
  apply (erule invarU_cong)
  apply (auto simp only: flat.simps invar.simps invarU_map
    G_pred_map o_def id_apply elim!: G_pred_mono_strong intro!: invarU_flatU
    elim!: invarU_cong simp: pred_U_def) []
  apply (erule thin_rl)
  apply (drule meta_spec)+
  apply (drule meta_mp)
  apply assumption
  apply (erule meta_mp)
  apply assumption
  done

lemma unflat_flat: "invar (\<lambda>u0. pred_F (a (U0 u0)) (invar a u0)) u0 t \<Longrightarrow> unflat u0 (flat t) = t"
  apply (induct t arbitrary: u0)
  apply (unfold flat.simps)
  apply (subst unflat_simps)
  apply (unfold invar_simps G_pred_map o_def id_apply)
  apply (erule G_pred_mono_strong)
  apply (rule invarU_flatU)
  apply (rule iffD2[OF invarU_map])
  apply (erule invarU_cong)
  apply (auto simp only: pred_U_def o_apply id_apply invar_flat) []
  apply (erule invar_flat)
  apply (auto simp only: T'.inject G.map_comp o_apply id_apply pred_G_def
    intro!: trans[OF G.map_cong0 G.map_ident])
  apply (drule bspec, assumption)
  apply (subst unflatU_flatU)
  apply (simp only: invarU_map)
  apply (erule invarU_cong)
  apply (auto simp only: pred_U_def o_apply id_apply elim: invar_flat) []
  apply (auto simp only: U.map_comp o_apply id_apply dest: invarU_b
    intro!: trans[OF U.map_cong0 U.map_ident])
  done

lemma zzzz: "invarU a b (U0 u) z \<Longrightarrow> z1 \<in> set2_U (unflatU u z) \<Longrightarrow> z1 \<in> set2_U z"
  apply (induct u arbitrary: z)
  apply (auto simp only: unflatU.simps invarU_simps U.set F.set_map id_apply split: U.splits)
  apply (erule notE)
  apply (drule meta_spec)+
  apply (drule meta_mp)
  prefer 2
  apply (drule meta_mp)
  apply assumption
  apply (auto simp:  split: U.splits)
  apply (meson U0.simps(3) invarU_A_iff pred_F_def)
  apply (auto simp add: pred_F_def)
  apply (drule bspec)
  apply assumption
  apply (auto simp: F_pred_map)
  apply (case_tac u)
  apply (auto simp: F.set_map pred_F_def split: U.splits)
  done

lemma invar_unflat: "invar a (U0 u0) t \<Longrightarrow>
  invar (\<lambda>u0. pred_F (a (U0 u0)) (invar a u0)) u0 (unflat u0 t)"
  apply (induct t arbitrary: u0)
  apply  (auto simp only: unflat.simps invar.simps invarU_unflatU U.map_comp o_def
    unflatU_map invarU_map F_pred_map ball_simps id_apply pred_G_def G.set_map)
  apply (subst unflatU_map)
  apply (erule bspec)
  apply assumption
  apply (auto simp only: invarU_map o_def F_pred_map id_apply)
  apply (rule invarU_cong)
  apply (rule invarU_unflatU)
  apply (erule bspec)
  apply assumption
  apply (auto simp only: pred_U_def flat_unflat elim!: F_pred_mono_strong)
  apply (drule meta_spec)
  apply (erule thin_rl)
  apply (drule meta_spec)+
  apply (drule meta_mp)
  apply assumption
  apply (drule meta_mp)
  defer
  apply (erule meta_mp)
  apply assumption
  apply (auto simp only: dest: zzzz[rotated])
  done

definition T :: "('a, ('a, 'a T) F T) G \<Rightarrow> 'a T" where
  "T g = Abs_T (T' (map_G A (flat o map_T' (map_F id Rep_T) o Rep_T) g))"

definition un_T :: "'a T \<Rightarrow> ('a, ('a, 'a T) F T) G" where
  "un_T t = map_G un_A (Abs_T o map_T' (map_F id Abs_T) o unflat A0) (un_T' (Rep_T t))"

lemma un_T_T: "un_T (T x) = x"
  unfolding T_def un_T_def G.map_comp o_def
  apply (subst Abs_T_inverse)
   apply (auto simp only: invar.simps invarU.simps U.map U.case
     G_pred_map G_pred_True F_pred_map F_pred_True
     invar_flat invar_map  Rep_T[simplified] id_apply o_def) []
  apply (rule G_pred_mono_strong[OF iffD2[OF fun_cong[OF G_pred_True] TrueI]], rule TrueI)
  apply (rule invar_flat)
  apply (auto simp only: invar_map F_pred_map o_def Rep_T[simplified])
  defer
  apply (auto simp only: T'.sel U.sel(1)
    G.map_comp G.map_ident T'.map_comp T'.map_ident F.map_comp F.map_ident F_pred_map F_pred_True
    invar_map Rep_T_inverse Rep_T[simplified] id_def id_apply o_def) []
  apply (subst unflat_flat)
  apply (auto simp only: invar_map F_pred_map o_def Rep_T[simplified])
  done

lemma T_un_T: "T (un_T x) = x"
  unfolding T_def un_T_def G.map_comp o_def
  apply (cases x; simp)
  apply (subst Abs_T_inject[simplified])
  apply (auto simp add: G_pred_map)
  apply (subst (2) Abs_T_inverse)
  apply simp
  apply (case_tac y)
  apply (simp add: G_pred_map)
  apply (erule G_pred_mono_strong; simp)
  apply (subst invar_flat)
  apply (auto simp: invar_map o_def F_pred_map Rep_T[simplified] F_pred_True)
  apply (subst (2) Abs_T_inverse)
  apply simp
  apply (case_tac y)
  apply (simp add: G_pred_map o_def)
  apply (rule trans[OF G.map_cong0 G.map_ident])
  apply (metis U.case_eq_if U.collapse(1) U.map_disc_iff pred_G_def)
  apply (subst Abs_T_inverse)
  apply (simp add: invar_map o_def)
  apply (rule invar_unflat)
 
 using Rep_T[simplified, of x]
  apply (subst trans[OF G.map_cong0 G.map_ident])
  apply (cases "Rep_T x"; auto simp only: pred_G_def G.set_map
    T'.sel invar.simps ball_simps id_apply U.disc(1) intro!: U.collapse(1))
  apply (cases "Rep_T x"; auto simp only: Abs_T_inverse invar_unflat
    flat_unflat pred_G_def G.set_map T'.sel invar.simps ball_simps id_apply
    mem_Collect_eq)
  apply (simp only: T'.collapse Rep_T_inverse)
  done

lemma invarU_map_closed:
  "invarU u0 (map_U f u) \<longleftrightarrow> invarU u0 u"
  by (induct u0 arbitrary: u)
    (auto simp only: invarU.simps U.map F_pred_map id_o o_apply
      elim!: F_pred_mono_strong split: U.splits)

lemma invar_map_closed:
  "map_T' f t \<in> Collect (invar u0) \<longleftrightarrow> t \<in> Collect (invar u0)"
  by (induct t arbitrary: u0)
    (auto simp only: mem_Collect_eq T'.map invar.simps id_apply ball_simps
      pred_G_def G.set_map invarU_map_closed)

lemma invarU_zip_closed:
  "invarU u0 (map_U fst z) \<Longrightarrow> invarU u0 (map_U snd z) \<Longrightarrow> invarU u0 z"
  by (induct u0 arbitrary: z)
    (auto simp only: invarU.simps U.map id_o o_apply pred_F_def F.set_map id_apply ball_simps
      split: U.splits)

lemma invar_zip_closed:
  "map_T' fst z \<in> Collect (invar u0) \<Longrightarrow> map_T' snd z \<in> Collect (invar u0) \<Longrightarrow> z \<in> Collect (invar u0)"
  by (induct z arbitrary: u0)
    (auto simp only: invar.simps T'.map pred_G_def G.set_map id_apply ball_simps
      invarU_zip_closed mem_Collect_eq)

lift_bnf 'a T
  apply (erule iffD2[OF invar_map_closed])
  apply (erule (1) invar_zip_closed)
  done

lemma map_U_flatU: "map_U f (flatU u) = flatU (map_U (map_F f) u)"
  by (induct u) (auto simp only: U.map flatU.simps F.map_comp o_def cong: F.map_cong0)

lemma map_T'_flat: "map_T' f (flat t) = flat (map_T' (map_F f) t)"
  by (induct t) (simp only: G.map_comp T'.map flat.simps map_U_flatU o_def cong: G.map_cong0)

lemma map_T: "map_T f (T g) = T (map_G f (map_T (map_F f)) g)"
  unfolding map_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only: invar.simps invarU.simps id_apply o_apply
     G.map_comp pred_G_def G.set_map Rep_T[simplified] invar_flat U.case) []
  apply (auto simp only: G.map_comp o_def Abs_T_inverse T'.map U.map mem_Collect_eq
     invar_map_closed[simplified] Rep_T[simplified] invar_flat map_T'_flat)
  done

lemma set_U_flatU: "set_U (flatU u) = UNION (set_U u) set_F"
  by (induct u) (auto simp only: flatU.simps U.set UN_simps F.set_map)

lemma set_T'_flat: "set_T' (flat t) = UNION (set_T' t) set_F"
  by (induct t)
    (auto 0 3 simp only: flat.simps T'.set U.set UN_simps G.set_map set_U_flatU)

lemma set_T: "set_T (T g) = set1_G g \<union> (\<Union>(set_F ` (\<Union>(set_T ` (set2_G g)))))"
  unfolding set_T_def T_def o_apply
  apply (subst Abs_T_inverse)
   apply (auto simp only: invar.simps invarU.simps id_apply o_apply
     G.map_comp pred_G_def G.set_map Rep_T[simplified] invar_flat U.case) []
  apply (auto simp only: T'.set G.set_map o_apply set_T'_flat U.set
    Sup_image_eq UN_simps)
  done

lemma rel_U_flatU: "invarU u0 u \<Longrightarrow> invarU u0 u' \<Longrightarrow>
  rel_U R (flatU u) (flatU u') = rel_U (rel_F R) u u'"
  apply (rule iffI[rotated, OF rel_funD[OF flatU.transfer]], assumption)
  apply (induct u arbitrary: u' u0)
  apply (case_tac u')
  apply (auto simp: F.rel_map invarU_A_iff) [2]
  apply (case_tac u'; case_tac u0)
  apply (auto simp only: flatU.simps U.rel_inject U.rel_distinct invarU.simps U.case
    F.rel_map invarU_A_iff F.set_map pred_F_def ball_simps id_apply elim!: F.rel_mono_strong) [4]
  done

lemma rel_T'_flat: "invar u0 t \<Longrightarrow> invar u0 t' \<Longrightarrow>
  rel_T' R (flat t) (flat t') = rel_T' (rel_F R) t t'"
proof (rule iffI[rotated, OF rel_funD[OF flat.transfer]], assumption, rotate_tac -1,
  induct "flat t" "flat t'" arbitrary: t t' u0 rule: T'.rel_induct)
  case (T' g g')
  then show ?case
    by (cases t t' rule: T'.exhaust[case_product T'.exhaust])
      (auto simp: G.rel_map rel_U_flatU pred_G_def G.set_map elim!: G.rel_mono_strong)
qed

lemma rel_T: "rel_T R (T g) (T g') = rel_G R (rel_T (rel_F R)) g g'"
  unfolding rel_T_def T_def vimage2p_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only: invar.simps invarU.simps id_apply o_apply
     G.map_comp pred_G_def G.set_map Rep_T[simplified] invar_flat U.case) [2]
  apply (simp only: G.rel_map T'.rel_inject o_apply U.rel_inject
    rel_T'_flat[OF Rep_T[simplified] Rep_T[simplified]])
  done

(*generalized recursor
  (\<forall>'a. ('a L, 'a F K) G \<Rightarrow> 'a K) \<Rightarrow>
  (\<forall>'a. 'a L F \<Rightarrow> 'a F L) \<Rightarrow>
  'b L T \<Rightarrow> 'b K 
*)
bnf_axiomatization 'a K
bnf_axiomatization 'a L

lemma L_pred_map: "pred_L P (map_L f x) = pred_L (P o f) x"
  unfolding pred_L_def L.set_map ball_simps o_def ..

lemma L_pred_True: "pred_L (\<lambda>_. True) = (\<lambda>_. True)"
  unfolding pred_L_def L.set_map by blast

lemma L_pred_rel: "rel_L (eq_onp P) x x = pred_L P x"
  unfolding pred_L_def L.in_rel
  by (force simp: L.set_map L.map_comp L.map_ident eq_onp_def o_def
    intro!: exI[of _ "map_L (\<lambda>x. (x, x)) x"])

lemma K_pred_map: "pred_K P (map_K f x) = pred_K (P o f) x"
  unfolding pred_K_def K.set_map ball_simps o_def ..

lemma K_pred_True: "pred_K (\<lambda>_. True) = (\<lambda>_. True)"
  unfolding pred_K_def K.set_map by blast

lemma K_pred_rel: "rel_K (eq_onp P) x x = pred_K P x"
  unfolding pred_K_def K.in_rel
  by (force simp: K.set_map K.map_comp K.map_ident eq_onp_def o_def
    intro!: exI[of _ "map_K (\<lambda>x. (x, x)) x"])

consts F :: "('a L, 'a F K) G \<Rightarrow> 'a K"
consts H :: "'a L F \<Rightarrow> 'a F L"

axiomatization where
  F_transfer: "rel_fun (rel_G (rel_L R) (rel_K (rel_F R))) (rel_K R) F F" and
  H_transfer: "rel_fun (rel_F (rel_L R)) (rel_L (rel_F R)) H H"

lemma F_natural: "F (map_G (map_L f) (map_K (map_F f)) x) = map_K f (F x)"
  using rel_funD[OF F_transfer, of "BNF_Def.Grp UNIV f"]
  unfolding F.rel_Grp K.rel_Grp L.rel_Grp G.rel_Grp by (auto simp: Grp_def)

lemma H_natural: "H (map_F (map_L f) x) = map_L (map_F f) (H x)"
  using rel_funD[OF H_transfer, of "BNF_Def.Grp UNIV f"]
  unfolding F.rel_Grp K.rel_Grp L.rel_Grp by (auto simp: Grp_def)

primrec fU :: "'a L U \<Rightarrow> 'a U L" where
  "fU (A a) = map_L A a"
| "fU (U f) = map_L U (H (map_F fU f))"

primrec f' :: "'a L T' \<Rightarrow> 'a U K" where
  "f' (T' g) = F (map_G fU (map_K un_U o f') g)"

definition f :: "'a L T \<Rightarrow> 'a K" where
  "f t = map_K un_A (f' (Rep_T t))"

lemma fU_flatU: "fU (flatU x) = map_L flatU (fU (map_U H x))"
  apply (induct x)
   apply (simp only: flatU.simps fU.simps U.map L.map_comp F.map_comp H_natural
     o_apply cong: F.map_cong L.map_cong)
  apply (simp only: flatU.simps fU.simps U.map F.map_comp L.map_comp o_apply
    cong: F.map_cong L.map_cong)
  apply (simp only: L.map_comp[unfolded o_def, of U "map_F flatU", symmetric]
    H_natural[symmetric] F.map_comp o_def)
  done

lemma invarU_fU: "invarU u0 u \<Longrightarrow> pred_L (invarU u0) (fU u)"
  apply (induct u arbitrary: u0)
  apply auto
  apply (auto simp add: L_pred_map o_def L_pred_True invarU_A_iff)
  apply (case_tac u0)
  apply (auto simp add: L_pred_rel[symmetric] F.rel_eq_onp[symmetric] F_pred_map)
  apply (rule rel_funD[OF H_transfer])
  apply (auto simp: L.rel_eq_onp F_pred_rel F_pred_map o_def L_pred_rel[symmetric])
  apply (auto simp: pred_F_def)
  done

lemma invar_f': "invar u0 x \<Longrightarrow> pred_K (invarU u0) (f' (map_T' H x))"
  apply (induct x arbitrary: u0)
  apply (auto simp: pred_G_def G.set_map)
  apply (auto simp add: K_pred_rel[symmetric])
  apply (rule rel_funD[OF F_transfer])
  apply (auto simp: G.set_map o_def G.map_comp K.set_map
    K.rel_eq_onp F.rel_eq_onp L.rel_eq_onp G_pred_rel G_pred_map K_pred_map)
  apply (auto simp: pred_G_def invarU_fU invarU_map_closed eq_onp_same_args)
  apply (drule meta_spec)+
  apply (drule meta_mp)
  apply assumption
  apply (drule meta_mp)
  apply (erule bspec)
  apply assumption
  apply (auto simp: pred_K_def F_pred_map split: U.splits)
  apply (case_tac xb)
  apply (auto simp: pred_F_def)
  done

lemma f'_flat: "invar u t \<Longrightarrow> f' (flat t) = map_K flatU (f' (map_T' H t))"
  apply (induct t arbitrary: u)
  apply (auto simp: G.map_comp F_natural[symmetric] o_def K.map_comp fU_flatU
    pred_G_def G.set_map
    intro!: arg_cong[of _ _ F] G.map_cong cong: K.map_cong G.map_cong)
    apply (drule meta_spec)+
    apply (drule meta_mp)
    apply assumption
    apply (drule meta_mp)
    apply (erule bspec)
    apply assumption
    apply (simp add: K.map_comp o_def)
    apply (rule K.map_cong0)
    apply (drule bspec[OF invar_f'[unfolded pred_K_def], rotated -1])
    apply (erule bspec)
    apply assumption
    apply (case_tac z)
    apply auto
    done

lemma "f (T g) = F (map_G id (f o map_T H) g)"
  unfolding f_def[abs_def] map_T_def T_def o_def
  apply (subst (1 2) Abs_T_inverse)
   apply (auto simp only: invar.simps invarU.simps id_apply o_apply U.case
     G.map_comp pred_G_def G.set_map Rep_T[simplified] invar_flat invar_map_closed[simplified]) [2]
  apply (simp only: f'.simps G.map_comp o_def
    F_natural[symmetric] fU.simps L.map_comp U.sel(1) L.map_id id_def[symmetric]
    K.map_comp f'_flat[OF Rep_T[simplified]])
  apply (rule arg_cong[of _ _ F])
  apply (rule G.map_cong0)
  apply simp
  apply (rule K.map_cong0)
  apply (drule bspec[OF invar_f'[unfolded pred_K_def], rotated -1])
  apply (rule Rep_T[simplified])
  apply (auto simp add: invarU_A0_iff F.map_comp o_def F.map_ident split: U.splits)
  done

end